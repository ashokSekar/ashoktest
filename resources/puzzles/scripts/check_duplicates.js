module.exports = function () {
  'use strict';

  var util = require('./util'),
    _ = require('lodash'),
    fs = require('fs'),
    path = require('path'),
    files = util.readdir(),
    hashes = {},
    puzzles = {};

  _.each(files, function (file) {
    var data = JSON.parse(fs.readFileSync(path.join(__dirname, '..', file))),
      id = data.id,
      puzzle_writable;

    if (_.has(hashes, id)) {
      console.log('Duplicate id: ' + hashes[id] + ' ' + file + " ----" + id);
      console.log('Removing id for : ' + file);
      data.id = '';
      puzzle_writable = fs.createWriteStream('./' + file);
      puzzle_writable.write(JSON.stringify(data, null, 2));
      puzzle_writable.end();
    } else {
      hashes[id] = file;
    }

    delete data.id;
    puzzles[file] = data;
  });

  _.each(puzzles, function (data1, key1) {
    _.each(puzzles, function (data2, key2) {
      if (key1 !== key2 && _.isEqual(data1, data2)) {
        console.log('Duplicate data', key1, key2);
      }
    });
  });
};
