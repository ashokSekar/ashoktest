// Mahjong trails files are in the format (z,y,x)
// We are interpreting it as (z,x,y) to flip it as all the levels are in landscape
// We need portrait
// You can run like this example: node scripts/mahjong_trails_convert.js 721 19 ~/Downloads/levels
// It will convert 19 levels from 721, the puzzle data will take from ~/Downloads/levels
var fs = require('fs'),
 child_process = require("child_process"),
 i = 0,
 args = process.argv,
 start = args[2], // change this value to new ms starts from
 no_of_levels = args[3],
 layout_path = args[4],
 alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
 kmahjongg = [],
 puzzle, id, default_start, remaining,
 len, contents, positions, max_x, max_y, max_z, output, file_part, file, pick;

 function convertoToQuest() {
    for (i = 1; i <= no_of_levels; i++) {
      positions = getPositions(contents);
      //console.log(positions);
      len = positions.length;
      max_x = 0;
      max_y = 0;
      max_z = 0;
      output = [];
      for(j = 0; j < len; j++) {
        z = positions[j][0];
        x = positions[j][1];
        y = positions[j][2];
    
        if(x > max_x) {
          max_x = x;
        }
    
        if(y > max_y) {
          max_y = y;
        }
    
        if(z > max_z) {
          max_z = z;
        }
    
        output[j] = [x, y, z];
      }
      file_part = "../layouts/" + max_x + "_" + max_y + "_" + max_z + "_" + len;
      file = file_part;
      j=1;
      
      while(fs.existsSync(file + ".json")) {
        file = file_part + "_r" + j++;
      }
      console.log('creating file ' + file);
      fs.writeFileSync(file + ".json", JSON.stringify(output, null, 4), {encoding: "utf8", flags: "w"});
    
      puzzle = {};
      puzzle.layout =  max_x + "_" + max_y + "_" + max_z + "_" + len;
      puzzle.total_tiles = len;
      id = "";
      for(j=0;j<4;j++) {
        id += alphabet[getRandomInclusive(0,25)];
      }
      puzzle.id = id;
      // pairs = tiles/2, so dividing by 2
      default_start = Math.floor(len/2/4);
      remaining = len/2 - 3 * default_start;
      puzzle.families = [
        {
          'pairs': default_start,
          'types': null
        }, {
          'pairs': default_start,
          'types': null
        }, {
          'pairs': default_start,
          'types': null
        }, {
          'pairs': 0,
          'types': null
        }, {
          'pairs': 0,
          'types': null
        }, {
          'pairs': 0,
          'types': null
        }, {
          'pairs': 0,
          'types': null
        }
      ];
      puzzle.time = 1200;
    
      for(j=0;j<remaining; j++) {
        pick = getRandomInclusive(3,6);
        puzzle.families[pick].pairs += 1;
      }
    
      console.log(puzzle);
      fs.writeFileSync("../" + start++ + ".json", JSON.stringify(puzzle, null, 4), {encoding: "utf8", flags: "w"});
    }
}

function createDefaulArray() {
  for (j=0;j<480;j++) {
    kmahjongg[j]=[];
    for(i=0;i<20;i++) {
      kmahjongg[j][i] = "."; 
    }
  }
}

function convertoToKMahjongg() {
  var i, k, out = "";
  for (i = 81; i <= 81; i++) {
    positions = getPositions(contents, i);
    len = positions.length;

    createDefaulArray();
    out = "";
    for(j = 0; j < len; j++) {
      z = positions[j][0];
      x = positions[j][1];
      y = positions[j][2];

      _y = 32*z + y;
      kmahjongg[_y][x] = "1";
      kmahjongg[_y][x+1] = "2";
      kmahjongg[_y+1][x] = "4";
      kmahjongg[_y+1][x+1] = "3";
    }
    out += "kmahjongg-layout-v1.1\n";
    out += "w20\nh32\nd15\n";
    for(k=0;k<480;k++) {
      out += kmahjongg[k].join("") + "\n";
    }
    fs.writeFileSync("trails/" + i + ".layout", out, {encoding: "utf8", flags: "w"});
    
    /*out = "[KMahjonggLayout]\n";
    out += "Name=Trails " + i + "\n";
    out += "Description=Trails Level " + i + "\n";
    out += "VersionFormat=1\n";
    out += "Author=HC\n";
    out += "AuthorEmail=support@hashcube.com\n";
    out += "FileName="+i+".layout\n";
    out += "\n";
    out += "[Desktop Entry]\n";
    out += "Name[en_IN]="+i+".desktop";
    fs.writeFileSync("trails/" + i + ".desktop.desktop", out, {encoding: "utf8", flags: "w"});    
    //console.log(out);
    */
  }
}

function convertFromKMahjongg () {
  var limit = parseInt(start) + parseInt(no_of_levels);
  for(f = start; f < limit; f++) {
    contents = fs.readFileSync(layout_path + '/' + f + ".layout", "utf8"); // you can change this to new puzzle location
    out = contents.split(/\r?\n/);
    out.splice(0, 4); //remove headers
    positions = [];
    max_x = 0;
    max_y = 0;
    max_z = 0;
    len = 0;
    for(i=0; i<out.length; i++) {
      z = Math.floor(i/32);
      for (j=0;j<out[i].length; j++) {
        y = i%32;
        if (out[i][j] === "1") {
          x = j;
          positions.push([x, y, z]);
  
          if (x > max_x) {
            max_x = x;
          }
  
          if (y > max_y) {
            max_y = y;
          }
  
          if(z > max_z) {
            max_z = z;
          }
          len++;
        }
      }
    }
    file_part = "layouts/" + max_x + "_" + max_y + "_" + max_z + "_" + len;
    file = file_part;
    j=0;
    r_part = "";
    
    while(fs.existsSync(file + ".json")) {
      file = file_part + "_r" + ++j;
      r_part = "_r" + j;
    }
    console.log('creating file ' + file);
    fs.writeFileSync(file + ".json", JSON.stringify(positions, null, 4), {encoding: "utf8", flags: "w"});
  
    puzzle = {};
    puzzle.layout =  max_x + "_" + max_y + "_" + max_z + "_" + len + r_part;
    puzzle.total_tiles = len;
    id = "";
    for(j=0;j<4;j++) {
      id += alphabet[getRandomInclusive(0,25)];
    }
    puzzle.id = id;
    //pairs = tiles/2, so dividing by 2
    default_start = Math.floor(len/2/4);
    remaining = len/2 - 3 * default_start;
    puzzle.families = [
      {
        'pairs': default_start,
        'types': null
      }, {
        'pairs': default_start,
        'types': null
      }, {
        'pairs': default_start,
        'types': null
      }, {
        'pairs': 0,
        'types': null
      }, {
        'pairs': 0,
        'types': null
      }, {
        'pairs': 0,
        'types': null
      }, {
        'pairs': 0,
        'types': null
      }
    ];
    puzzle.init = [];
    puzzle.time = Math.round(0.9 * len);
    puzzle.file = f;
  
    for(j=0;j<remaining; j++) {
      pick = getRandomInclusive(3,6);
      puzzle.families[pick].pairs += 1;
    }
  
    fs.writeFileSync(f + ".json", JSON.stringify(puzzle, null, 4), {encoding: "utf8", flags: "w"});
  }

  runScript('scripts/build.js', function (err) {
    if (err) {
      console.log('Error Occured');
      throw err;
    } else {
      console.log('Script executed successfuly');
    }
  });
}

function runScript(scriptPath, callback) {
  // keep track of whether callback has been invoked to prevent multiple invocations
  var invoked = false,
    process = child_process.fork(scriptPath);

  // listen for errors as they may prevent the exit event from firing
  process.on('error', function (err) {
    if (invoked) return;
      invoked = true;
      callback(err);
  });

  // execute the callback once the process has finished running
  process.on('exit', function (code) {
    if (invoked) return;
    invoked = true;
    var err = code === 0 ? null : new Error('exit code ' + code);
    callback(err);
  });

}

function getRandomInclusive(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getPositions(contents, i) {
  contents = fs.readFileSync("mahjong_trails/level_" + i + ".json", "utf8");
  contents = JSON.parse(contents);
  positions = contents.positions;
  return positions;
}

//convertoToQuest();
//convertoToKMahjongg();
convertFromKMahjongg();
