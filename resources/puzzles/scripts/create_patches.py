import git
import unicodedata
import ConfigParser
import json
import os.path
import re

# get parent directory
path = os.path.abspath(__file__).rsplit(os.path.dirname(__file__), 1)[0][:-1]
repo = git.Repo(path)

config = ConfigParser.ConfigParser()
config.read(path + '/scripts/puzzle_config')

def isNumber(s):
    try:
        float(s)
        return True
    except ValueError:
        pass

    try:
        unicodedata.numeric(s)
        return True
    except (TypeError, ValueError):
        pass

    return False

# versions 0.4.8 and above have no timers stopping during matching animation
# except versions 0.4.11 and 0.4.12
def vercmp(version1, version2):
    if (version1 == '0.4.11' or version1 == '0.4.12'):
      return -1
    def normalize(v):
        return [int(x) for x in re.sub(r'(\.0+)*$','', v).split(".")]
    return cmp(normalize(version1), normalize(version2))

def getDiff(old_data, new_data, ignore_tap):
    ret_data = {}

    for attr in new_data:
        if (old_data.get(attr) is None):
            ret_data[attr] = new_data[attr]
        elif (isinstance(new_data[attr], dict)):
            ret_data[attr] = getDiff(old_data[attr], new_data[attr], ignore_tap)
            if (not ret_data[attr]):
                ret_data.pop(attr, None)
        elif (old_data[attr] != new_data[attr]):
            ret_data[attr] = new_data[attr]
    if ignore_tap is True:
        ret_data.pop("tap", None)
    return ret_data


def createIndexFile(tag, d_ver):
    modified_data = {}
    print('\nCREATING DESIGN_VERSION FILE FOR: ')
    print(tag)
    old = repo.commit(tag)
    head = repo.commit('HEAD')
    git = repo.git
    counter = 0
    old_data = {}
    new_data = {}
    ignore_tap = False
    version = tag.name[1:]

    # For the versions before 0.6.71 the patch with tap has already been generated . 
    # Only after 0.6.71 Bonus level maps were introduced . 
    if(version >= '0.6.71' and version < '0.6.81'):
        ignore_tap = True

    diffs = head.diff(old, create_patch=True).iter_change_type('M')

    for diff in diffs:
        diff_str = diff.diff
        changed_id = diff_str.find('-  "id":')
        old_file = diff.a_path.split('.')[0]
        new_file = diff.b_path.split('.')[0]

        if(changed_id == -1 and old_file == new_file and isNumber(old_file)):
            counter = counter + 1
            with open(path + '/' + old_file + '.json') as data_file:
                new_data[old_file] = json.load(data_file)

    ret = git.stash()
    git.checkout(old)
    for file_name in new_data:
        with open(path + '/' + file_name + '.json') as data_file:
            old_data[file_name] = json.load(data_file)

    git.checkout(head)
    if ('Saved working directory' in ret):
        git.stash('pop')

    modified_data = getDiff(old_data, new_data, ignore_tap)

    if (modified_data != {}):
        app_version = tag.name[1:]
        modified_data['version'] = d_ver
        modified_data['app_version'] = app_version
        with open(path + '/patches/' + app_version + '.json', 'w') as f_out:
            json.dump(modified_data, f_out, indent=2, separators=(',', ': '))

    print('JSON DUMP COMPLETED, No. of puzzles: ', counter)


def getTagNames():
    with open(path + '/config.json', 'r') as f_read:
        config_data = json.load(f_read)

    d_ver = int(config_data['version']) + 1
    min_version = config.get('misc', 'min_version')

    for tag in repo.tags:
        name = tag.name
        if name[:1] == 'D':
          print name + ' ignored'
          continue

        name = tag.name[1:]

        if(vercmp(name, min_version) == 1):
            createIndexFile(tag, d_ver)

    print('\n\nUPDATING TO DESIGN VERSION:' + str(d_ver))
    config_data['version'] = int(d_ver)

    with open(path + '/config.json', 'w') as f_write:
        json.dump(config_data, f_write, indent=2, separators=(',', ': '))

if __name__ == '__main__':
    if repo.is_dirty():
        print('Git directory not clean. Please commit/stash before running these changes')
    else:
        getTagNames()
