#! /usr/bin/env node
'use strict';

var util = require('./util'),
  jsio = require('jsio'),
  fs = require('fs'),
  _ = require('lodash'),
  sample = require('../sample.json'),
  files = util.readdir(),
  sample_keys = _.keys(sample),
  minify = require('../config.json').minify,
  checkDuplicates = require('./check_duplicates'),
  deleteLayouts = require('./del_layout'),
  ids = {},

  generateID = function () {
    return (Math.random() * Math.pow(36, 4) << 0)
      .toString(36);
  },

  getID = function () {
    var id = generateID();

    while (ids[id]) {
      id = generateID();
    }
    return id;
  },

  funcs = {
    time: function (puzzle, param) {
      var time_fix_ms = 660;

      if (parseInt(this.file.split('.json')[0]) <= time_fix_ms) {
        return this.time;
      }

      return Math.max(Math.round(0.9 * this['total_tiles']), 60);
    },

    tap: function (val, param) {
      var tap_fix_ms = 660;

      if (parseInt(this.file.split('.json')[0]) <= tap_fix_ms) {
        return this.tap;
      }
      return this.total_tiles * 4;
    },

    init: function (data, param) {
      var face_cords = _.groupBy(data, function (curr) {
          return curr[3];
        }),
        file = this.file;

      _.forEach(face_cords, function (cords, face) {
        if (cords.length % 2 !== 0) {
          console.log('Wrong init value for puzzle: ', file);
        }
      });
    },

    families: function (data, param) {
      var max_tiles = game_config.max_tiles,
        file = this.file;

      _.each(data, function (curr, idx) {
        if (curr && curr.types > max_tiles[idx]) {
          console.log('Reseting Tile types for family: %s for file: %s',
            idx + 1, file);
          curr.types = max_tiles[idx];
        }
     });
    }
  };

jsio.path.add('../');
/* jsio: if */
jsio('import data.config as game_config');
/* jsio: fi */

// preload id to avoid possible duplicates
_.each(files, function (file) {
  var id = require('../' + file).id;

  if (id && id.length <= 4) {
    ids[id] = true;
  }
});

_.each(files, function (file) {
  var data = require('../' + file);

  // Merge keys that are absent - give prior to data
  data = _.extend({}, sample, data);
  data.file = file;

  // if a function is found run function and update key value
  _.each(funcs, function (val, key) {
    var res;

    if (!_.has(data, key)) {
      return;
    }

    res = funcs[key].call(data, data[key], key);

    if (res) {
      data[key] = res;
    }
  });

  // pick out un wanted keys
  data = _.pick(data, function (val, key) {
    return sample_keys.indexOf(key) > -1;
  });

  // bled gives hash now
  if (!data.id || data.id.length > 4 || data.id.length === 0) {
    data.id = getID();
  }
  ids[data.id] = true;

  fs.writeFileSync(file, JSON.stringify(data, null, minify ? null: 2));
});

checkDuplicates();
deleteLayouts();
