var fs = require('fs'),
  puzzles = '../';
  csv = require('csv'),
  change_limits = function (level_no, limit_value) {

    fs.readFile(puzzles + level_no + '.json', 'utf8', function (err,data) {
      if (err) {
        return console.log(err);
      }
      data = JSON.parse(data);
      data[limit] = parseInt(limit_value , 10);
      fs.writeFile(puzzles + level_no + '.json', JSON.stringify(data,null, 2), 'utf8', function () {
        console.log("level_no", level_no, "written");
      });

    });

  },
  read_limits = function (file) {
    fs.readFile(file, 'utf8', function (err,data) {
      if (err) {
        return console.log(err);
      }
      csv.parse(data, function(err, data) {
        headers = false;
        i=0;
        k=1;
        data.forEach(function(levels) {
          if(headers) {
            console.log(levels[0], i++)
            if(levels[1] !== "") {
              change_limits(levels[0], levels[1])
              console.log('writing' + limit , levels[0], k++);
            }
          }
        headers = true
        });
      });
    });
  };

var argv = process.argv.slice(2),
  limit = "time";
if (argv[1]) {
  limit = argv[1];
}
if (limit === "time" || limit === "tap") {
  read_limits(argv[0]);
} else {
  console.log("Only time or tap can be given as argument");
}

