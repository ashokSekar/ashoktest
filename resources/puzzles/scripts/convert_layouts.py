import ast
import copy
import os.path

# get parent directory
path = os.path.dirname(os.path.abspath(__file__)).rsplit('/', 1)[0]

tile_array = []

def processPuzzles(file_path):
    f = open(file_path, 'r')
    r = 0
    c = 0
    l = 0
    row = ['.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.']
    overall_puzzle = []

    data = f.read()
    tile_array = ast.literal_eval(data)

    while l < 15:
        curr_layer = []
        second_row = row
        r = 0
        while r < 32:
            first_row = list(second_row)
            second_row = list(row)
            c = 0
            while c < 19:
                flag = 0
                for tile in tile_array:
                    if tile[0] == c and tile[1] == r and tile[2] == l:
                        first_row[c] = '1'
                        first_row[c+1] = '2'
                        second_row[c+1] = '3'
                        second_row[c] = '4'
                        tile_array.remove(tile)
                c+=1
                flag = 0

            curr_layer.append(''.join(first_row) + '\n')
            r+=1
        l+=1
        overall_puzzle.append(''.join(curr_layer))

        final_puzzle = 'kmahjongg-layout-v1.1\nw20\nh32\nd15\n' + ''.join(overall_puzzle)
        print('final_puzzle', final_puzzle)

    return final_puzzle


def convertFiles():
    src_path = path + '/layouts'
    dest_path = path + '/scripts/converted_layouts'

    for filename in os.listdir(src_path):
        if filename.endswith(".json"):
            print(os.path.join(src_path, filename))
            new_file = processPuzzles(os.path.join(src_path, filename))

            f = open(os.path.join(dest_path, filename.rsplit('.')[0] + '.layout'), 'w')
            f.write(new_file)


if __name__ == '__main__':
    convertFiles()
