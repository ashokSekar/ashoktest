var fs = require('fs'),
  _ = require('lodash'),
  config_files = require('../config.json').ignore,
  ext = 'json';

module.exports = {
  /* reads file list from current dir ignores if not JSON files and
    ignores fils in config.json */
  readdir: function (path) {
    'use strict';

    var all_files = fs.readdirSync(path || '.'),
      filtered;

    filtered = _.filter(all_files, function (file) {
      return file.indexOf(ext, file.length - ext.length) !== -1 &&
        (config_files.indexOf(file) < 0);
    });

    return _.sortBy(filtered, function (name) {
      return parseInt(name.split('.')[0], 10);
    });
  }
};
