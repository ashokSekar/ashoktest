
module.exports = function () {
  'use strict';

  var util = require('./util'),
    fs = require('fs'),
    levs = util.readdir(),
    lays = util.readdir('./layouts'),
    _ = require('lodash');

  _.each(levs, function (file) {
    var layout = require('../' + file).layout;

    _.remove(lays, function (curr) {
      return curr === layout + '.json';
    });
  });

  console.log('Deleting Unused layouts: ', lays);
  _.each(lays, function (layout_file) {
    fs.unlink('./layouts/'+ layout_file);
  });
};

