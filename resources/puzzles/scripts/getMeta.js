var fs = require('fs'),
  path = "./"
  start=601, end=669;

function getPairs(families) {
  return families? families.pairs: 0;
}

function getTypes(families) {
  var types;

  types = families? families.types: 0;
  return types?types: 0;
}

for(i=start;i<=end;i++) {
  var data = fs.readFileSync(path+i+".json"),
    fp = [],
    ft = [],
    data_print = [],
    rows, cols, layers, time, tiles, family, families;

  data = JSON.parse(data);
  layout = data.layout;

  families = data.families.length;
  tiles = data.total_tiles;
  family = data.families;
  time = data.time;
  family_count = 0;
  for(j=0;j<7;j++) {
    fp[j] = getPairs(family[j]);
    ft[j] = getTypes(family[j]);
    if(fp[j] !== 0) {
      family_count++;
    }
  }
  layout_data = layout.split("_");
  rows = layout_data[0];
  cols = layout_data[1];
  layers = layout_data[2];

  data_print.push(i);
  data_print.push(layout);
  data_print.push(rows);
  data_print.push(cols);
  data_print.push(layers);
  data_print.push(tiles);
  data_print.push(family_count);
  for(j=0;j<7;j++) {
    data_print.push(fp[j]);
  }
  for(j=0;j<7;j++) {
    data_print.push(ft[j]);
  }
  data_print.push(time);
  console.log(data_print.join(","));
}
