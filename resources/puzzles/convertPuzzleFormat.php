<?php

class convertPuzzleFormat
{
  public function __construct()
  {

  }

  /*
    Reads a solver comaptible format and
    converts it back to design
    Applicable for gnome-mahjong type arrangement
  */
  private function solToDesign1Tile($file_name)
  {
    if(empty($file_name))
    {
      echo "No file mentioned\n";
      return ;
    }

    $pattern = array();

    $data = file_get_contents($file_name);
    $lines = explode("\n", $data);

    $tile = null;

    foreach ($lines as $idx => $row) {
      if($idx === 0) {
        $tile = (int) $row;
      } else if(strlen($row) > 0) {
        $items = explode(" ", $row);
        $r = (int) $items[0];
        $c = (int) $items[1];
        if(isset($pattern[$r][$c])) {
          $pattern[$r][$c] += 1;
        } else {
          $pattern[$r][$c] = 1;
        }
      }

    }

    // Get max row and column value
    $row_count = 0;
    $column_count = 0;

    foreach ($pattern as $idx => $column) {
      foreach ($column as $key => $value) {
        if($key > $column_count) {
          $column_count = $key;
        }
      }
      if($idx > $row_count) {
        $row_count = $idx;
      }
    }
    $column_count += 1;
    $row_count += 1;

    for ($i = 0; $i < $row_count; $i++) {
      for ($j = 0; $j < $column_count; $j++) {
        if(!isset($pattern[$i][$j])) {
          $pattern[$i][$j] = 0;
        }
      }
    }

    $data = "";
    for ($i = 0; $i < $row_count; $i++) {
      for ($j = 0; $j < $column_count; $j++) {
        $data = $data.$pattern[$i][$j];
      }
      $data = $data."\n";
    }
    echo $data;
  }

  /*
  Reads a design and output a format which the solver can read

  Compatiblity: gnome-mahjong
                Cannot read overlap designs
  Ex:    10201  Output: 0 0 0
         12321          0 3 0
         10201          ....
  */
  private function designToSol1Tile($file_name)
  {
    if(empty($file_name)) {
      print_r("No input file !!\n");
      return;
    }

    // Split into layers
    $data = file_get_contents($file_name);
    $lines = explode("\n", $data);
    $pattern = array();
    $max_layer = 0;
    $rows = 0;
    $columns = 0;
    $layers = array();
    $ctr = 0;

    // Put it into array
    // Compute max for layer
    foreach ($lines as $idx => $row) {
      // Make sure to ignore comment lines
      echo $row."\n";
      if(strlen($row) > 0 && $row[0] !== "#") {
        $rows += 1;
        $columns = strlen($row);
        $pattern[$ctr] = str_split($row);

        for($j = 0; $j < $columns; $j++) {
          $pattern[$ctr][$j] = (int) $pattern[$ctr][$j];
        }

        if($max_layer < max($pattern[$ctr])) {
          $max_layer = max($pattern[$ctr]);
        }

        $ctr ++;
      }

    }

    for($i = 0; $i < $rows; $i ++) {
      for($j = 0; $j < $columns; $j ++) {
        $temp = $pattern[$i][$j];
        while($temp > 0) {
          $layers[$temp - 1][$i][$j] = 1;
          $temp--;
        }
      }
    }

    $op = "";

    // Fill missing layers
    $ctr = 0;
    for($k = 0 ; $k < $max_layer; $k ++) {
      for($i = 0; $i < $rows; $i ++) {
        for($j = 0; $j < $columns; $j ++) {
          if(!isset($layers[$k][$i][$j])) {
            $layers[$k][$i][$j] = 0;
          } else {
            $op = $op.($i)." ".($j)." ".$k."\n";
            $ctr ++;
          }


        }
      }
    }

    $ctr /= 4;
    $op = (int) $ctr."\n".$op;

    echo "--------\n";
    print_r($op);
    echo "\n--------\n";

    echo "Data written to file output.txt\n";
    file_put_contents("output.txt", $op);
    return 1;
  }

  /*
  Reads a design and output a format which the solver can read

  Compatiblity: kde-mahjong
                READS overlapping designs
  */
  private function KDEFormatDesignToSolution($input_file_name, $output_file_name)
  {

    $config = file_exists("config") ?
      parse_ini_file("config", TRUE)["convertPuzzleFormat"] : "";

    if(empty($config)) {
      echo "WARN config not present !!!\n";
      return;
    }

    if(empty($input_file_name) || !file_exists($input_file_name)) {
      echo "Empty file_name or File not present. Please enter properly !!!\n";
      return;
    }

    if(empty($output_file_name)) {
      echo "WARN : no output file name described\n";
      $output_file_name = "output.txt";
    }

    $data = file_get_contents($input_file_name);

    // Aim to read width, height, depth
    $data = explode("\n", $data);

    foreach ($data as $idx => $row) {
      $contents[$idx] = str_split($row);
    }

    $dimension = $this->getDimension($contents, $config);
    $layers = $this->getLayers($contents, $dimension);
    if(!$layers) {
      echo "Reading format error\n";
      return;
    }

    // iterate through each layer add coordinates
    $line = 0;
    $op = "";
    $final = [];
    for($i = 0 ; $i < $dimension['d']; $i++)
    {
      for ($j=0; $j < $dimension['h']; $j++) {
        for ($k=0; $k < $dimension['w']; $k++) {
          $lay = [];
          if($layers[$i][$j][$k] === '1') {
            array_push($lay, $k, $j, $i);
            array_push($final, $lay);
            $op = $op."$k $j $i\n";
            $line += 1;
          }
        }
      }
    }

    $op = json_encode($final, JSON_PRETTY_PRINT); 
    file_put_contents($output_file_name, $op);

    //echo $input_file_name." exported to ".$output_file_name;
  }

  private function getLayers($contents, $dimension)
  {
    if(!$dimension) {
      return false;
    }
    $layers = array();
    $ctr = 0;
    for($i = 0; $i < count($contents); $i++) {
      $row = $contents[$i];
      $str_join = join($row);
      if(strlen($str_join) && $row[0] !== "#" && $i > 0
        && !in_array($row[0], array('h','w','d'))) {
        $d_layer = null;
        // Break into row groups
        for($j = $i; $j < $i + $dimension['h']; $j ++) {
          $d_layer[] = $contents[$j];
        }
        if(!is_null($d_layer)) {
          array_push($layers, $d_layer);
          $i = $i + $dimension['h'] - 1;
        }
      }
    }
    return $layers;
  }

  private function getDimension($data, $config)
  {
    foreach ($data as $idx => $current) {
      if(in_array($current[0], array('h', 'w', 'd')))
      {
        $temp = join($current);
        $dimension[$current[0]] = (int) substr($temp, 1);
      }
    }
    if(isset($dimension) && count($dimension) === 3) {
      return $dimension;
    } else {
      return $config;
    }
    return false;
  }

  public function init($arg_1, $arg_2, $arg_3)
  {
    switch ($arg_1) {
      case 1:
        $this->solToDesign1Tile($arg_2);
        break;

      case 2:
        $this->designToSol1Tile($arg_2);
        break;

      case 3:
        $this->KDEFormatDesignToSolution($arg_2, $arg_3);
        break;

      default:
        print_r("Menu:\n
        1. Solution (mjsol) to puzzle (one above another)
        2. Puzzle Design to Solution (one above another)
        3. KDE type design layout to solver readable format
        \n");
        break;
    }
  }
}

$obj = new convertPuzzleFormat();
// Menu option
$arg_1 = isset($argv[1]) ? (int) $argv[1] : 0;
// Input file
$arg_2 = isset($argv[2]) ? $argv[2] : "";
// ouptut file
$arg_3 = isset($argv[3]) ? $argv[3] : "";

// Init function
$obj->init($arg_1, $arg_2, $arg_3);
?>
