This will have layouts and designs along with the scripts to populate those layouts


Setup :
1. Do npm install
2. do pip install -r requirements.txt


Steps to add/remove levels
1. Copy files at thier appropriate folders .
2. change tag version in package.json ( dont change version in config.json) .
2. Run node scripts/build.js .
3. run python scripts/create_patches.py ( it will change design version in config.json) and send Pull request.
4. get merge pull request and create tag from master (same version as step 2).
4. after that create tag , push it.
5. ms_populate.py is now automated with cron jobs in the mahjong analytics script.Version number is optional in ms_populate.py,if version is not specified(for design versions),script will update patches else it will insert data into ms_ids from root json file as well as updating patches
