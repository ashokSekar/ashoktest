import subprocess
import os
import json
import sys
import glob

new_created_file_name = '';
layout_path = sys.argv[1] if len(sys.argv) > 1 else false
layout = layout_path.split('/')[-1]

def generateCustomLayout():
    if (layout):
      argument_input = layout_path
      argument_output = 'layouts/' + layout
      subprocess.call(['php', 'convertPuzzleFormat.php', '3', argument_input, argument_output])
    

def execShell(language, script_name, params):
  cmd = language + " " + script_name + " " + params
  proc = subprocess.Popen(cmd, shell=True,
                                  stdout=subprocess.PIPE)
  resp = proc.stdout.read()

def renameLayoutFile(layout_data, layout_name):
  max_x = max_y = max_z = 0
  
  for data in layout_data:
    max_x = data[0] if data[0] > max_x else max_x
    max_y = data[1] if data[1] > max_y else max_y
    max_z = data[2] if data[2] > max_z else max_z
  coordinates = str(max_x) + '_' + str(max_y) + '_' + str(max_z) + '_' + str(len(layout_data)) 
  existing_same_layouts = glob.glob('layouts/' + coordinates + '*')
  existing_same_layouts_count = len(existing_same_layouts)
  if existing_same_layouts_count:
    new_created_file_name = coordinates + '_r' + str(existing_same_layouts_count) + '.json'
  else:
    new_created_file_name = coordinates + '.json'
  path = 'layouts/'
  execShell('mv', path + layout_name  + '.layout', path + new_created_file_name)
  print new_created_file_name


def setCustomLayoutName():
  generateCustomLayout()
  
  glob.glob('layouts/'+ layout)
  for fn in os.listdir('layouts'):
    if fn.endswith('.layout') and fn == layout:
      with open('layouts/' + fn, 'r') as layout_data:
        json_data = json.load(layout_data)
        renameLayoutFile(json_data, fn.split('.')[0])

if __name__ == "__main__":
  setCustomLayoutName()
