exports = [
  {
    puzzle_id: 4,
    position: 'left'
  },
  {
    puzzle_id: 6,
    position: 'right'
  },
  {
    puzzle_id: 7,
    position: 'left'
  },
  {
    puzzle_id: 8,
    position: 'left'
  },
  {
    puzzle_id: 9,
    position: 'left'
  },
  {
    puzzle_id: 10,
    position: 'left'
  },
  {
    puzzle_id: 11,
    position: 'left'
  },
  {
    puzzle_id: 12,
    position: 'right'
  },
  {
    puzzle_id: 13,
    position: 'right'
  },
  {
    puzzle_id: 14,
    position: 'left'
  },
  {
    puzzle_id: 16,
    position: 'left'
  },
  {
    puzzle_id: 17,
    position: 'left'
  },
  {
    puzzle_id: 19,
    position: 'left'
  },
  {
    puzzle_id: 22,
    position: 'left',
    offset: -125
  },
  {
    puzzle_id: 23,
    position: 'left'
  },
  {
    puzzle_id: 49,
    position: 'right'
  },
  {
    puzzle_id: 64,
    position: 'left',
    offset: -125
  },
  {
    puzzle_id: 73,
    position: 'right'
  },
  {
    puzzle_id: 32,
    position: 'right'
  },
  {
    puzzle_id: 67,
    position: 'right'
  },
  {
    puzzle_id: 31,
    position: 'right'
  },
  {
    puzzle_id: 80,
    position: 'left'
  },
  {
    puzzle_id: 38,
    position: 'right'
  },
  {
    puzzle_id: 47,
    position: 'right'
  },
  {
    puzzle_id: 36,
    position: 'right'
  },
  {
    puzzle_id: 48,
    position: 'left'
  },
  {
    puzzle_id: 61,
    position: 'right'
  },
  {
    puzzle_id: 46,
    position: 'left'
  },
  {
    puzzle_id: 66,
    position: 'right'
  },
  {
    puzzle_id: 57,
    position: 'right'
  },
  {
    puzzle_id: 42,
    position: 'left'
  },
  {
    puzzle_id: 58,
    position: 'right'
  },
  {
    puzzle_id: 68,
    position: 'left'
  },
  {
    puzzle_id: 79,
    position: 'right'
  },
  {
    puzzle_id: 70,
    position: 'left'
  },
  {
    puzzle_id: 75,
    position: 'right'
  },
  {
    puzzle_id: 81,
    position: 'right'
  },
  {
    puzzle_id: 53,
    position: 'right'
  },
  {
    puzzle_id: 54,
    position: 'left'
  },
  {
    puzzle_id: 71,
    position: 'right'
  },
  {
    puzzle_id: 37,
    position: 'left'
  },
  {
    puzzle_id: 86,
    position: 'right'
  },
  {
    puzzle_id: 41,
    position: 'right'
  },
  {
    puzzle_id: 44,
    position: 'left',
    offset: -100
  },
  {
    puzzle_id: 78,
    position: 'right'
  }
];
