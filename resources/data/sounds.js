exports = {
  music: {
    map: {
      path: 'music',
      volume: 0.4,
      background: true,
      loop: true
    },
    puzzle_default: {
      path: 'music',
      volume: 0.5,
      background: true,
      loop: true
    }
  },
  effect: {
    button_primary: {
      path: 'effect',
      volume: 0.5
    },
    level_won: {
      path: 'effect',
      volume: 0.5
    },
    level_fail: {
      path: 'effect',
      volume: 0.5
    },
    time: {
      path: 'effect',
      volume: 0.5
    },
    cash_purchase: {
      path: 'effect',
      volume: 1.0
    },
    tile_match: {
      path: 'effect',
      volume: 1.0
    },
    star_0: {
      path: 'effect',
      volume: 1
    },
    star_1: {
      path: 'effect',
      volume: 1
    },
    star_2: {
      path: 'effect',
      volume: 1
    },
    substitute: {
      path: 'effect',
      volume: 1
    },
    swap_tap: {
      path: 'effect',
      volume: 1
    },
    swap: {
      path: 'effect',
      volume: 1
    },
    flip: {
      path: 'effect',
      volume: 1
    },
    open_slot: {
      path: 'effect',
      volume: 1
    },
    joker_match: {
      path: 'effect',
      volume: 0.5
    },
    hint: {
      path: 'effect',
      volume: 1
    },
    shuffle: {
      path: 'effect',
      volume: 0.8
    },
    half_hint: {
      path: 'effect',
      volume: 0.5
    },
    tap: {
      path: 'effect',
      volume: 0.5
    }
  }
};
