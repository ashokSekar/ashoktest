var lives_gen = 1800000;

exports = {
  server: 'http://oogway.hashcube.com:3009',
  img_url: 'https://s3.amazonaws.com/hashcube/images/mahjong',
  board_data: {
    rows: 10,
    columns: 10,
    depth: 5
  },
  game_name: 'Mahjong quest',
  facebook_app_id: '1777829205830622',
  disable_facebook: false,
  disable_google: false,
  fb_objects: {
    life: '1668240663286754'
  },

  STYLE: {
    orientation: 'portrait',
    bound_width: 640,
    bound_height: 960,
    tablet: 0.8,
    phablet: 0.9
  },

  lives: {
    max: 5,
    cost_for_max: 10,
    gen: lives_gen,
    ad: 1,
    fb: 1
  },

  sale_notif_delay: 14400,

  puzzle_load: {
    retry: 64,
    info_timeout: 3,
    track: 5,
    time_multiplier: 0.2
  },

  family_score: [100, 150, 200, 300, 250, 400, 350],
  max_tiles: [9, 9, 9, 3, 4, 4, 4],
  fb_coin_award: 100,
  fb_auth_url: 'https://www.facebook.com/v3.0/dialog/oauth',
  facebook_app_name: 'oogway_mq_desktop',
  fb_scopes: [
    'public_profile',
    'user_friends'
  ],
  why_fb_ms: 20,
  powerups: {
    ingame: {
      shuffle: true,
      hint: true,
      half_hint: true,
      swoosh: true,
      swap: false,
      substitute: true,
      flip: false
    },
    pregame: {
      joker: true,
      open_slot: true,
      auto_hint: true
    }
  },
  auto_hint_interval: 5000,
  award_powerup_count: {
    flip: 3,
    hint: 3,
    half_hint: 3,
    auto_hint: 3,
    swoosh: 3,
    substitute: 3,
    open_slot: 3,
    swap: 3,
    joker: 3
  },
  unlockables: {
    shuffle: 5,
    hint: 12,
    half_hint: 14,
    swoosh: 23,
    flip: 27,
    joker: 28,
    swap: 32,
    substitute: 40,
    open_slot: 44,
    auto_hint: 52
  },
  goals_prop: {
    clear: 'tile_count',
    golden: 'golden_collected',
    time: 'time',
    tap: 'tap',
    jewels: 'jewels',
    lock: 'lock_keys'
  },
  swoosh_pairs: 5,
  flip_count: 3,
  rating_min_ms: 18,

  // Max ms upto which analytics level based events to send
  ms_event_max: 20,
  app_reengage_uris: ['/play', '/testplay'],

  app_url: {
    android: 'market://details?id=com.hashcube.mahjongfree',
    ios: 'itms-apps://itunes.apple.com/app/id1227443330',
    kindle: 'amzn://apps/android?p=com.hashcube.mahjongfree'
  },
  support_mail: 'support@hashcube.com',

  no_of_hints: 42,
  no_of_motivation_msg: 36,
  jokers_per_level: 2,
  special_faces: {
    joker: 'f_joker',
    golden: 'f_golden',
    lock: ['f_lock_0', 'f_lock_1', 'f_lock_2']
  },
  touch3d: {
    play: {
      icon: 'play'
    },
    messages: {
      icon: 'message',
      facebook: true
    },
    invites: {
      icon: 'add',
      facebook: true
    }
  },
  max_type: 9,
  variations: {
    classic: {
      id: 'classic',
      powerups_exclude: ['flip'],
      goals: ['clear', 'time']
    },
    memory: {
      id: 'memory',
      powerups_exclude: ['shuffle', 'half_hint',
        'swap', 'open_slot', 'auto_hint'],
      goals: ['clear', 'tap']
    },
    untimed: {
      id: 'untimed',
      powerups_exclude: ['flip'],
      goals: ['clear']
    },
    golden: {
      id: 'golden',
      powerups_exclude: ['flip'],
      goals: ['clear', 'time', 'golden']
    },
    jewels: {
      id: 'jewels',
      powerups_exclude: ['flip'],
      goals: ['clear', 'time', 'jewels']
    },
    three_match: {
      id: 'three_match',
      powerups_exclude: ['flip'],
      goals: ['clear', 'time']
    },
    lock_key: {
      id: 'lock_key',
      powerups_exclude: ['flip'],
      goals: ['clear', 'time', 'lock']
    },
    partial_memory: {
      id: 'partial_memory',
      powerups_exclude: ['flip'],
      goals: ['clear', 'time']
    }
  },
  quick_rate_min_version: {
    ios: 10.3
  },
  partial_memory_ratio: 0.1,
  bonus_level_def: [[7, 15], [13, 30], [20, 40]],
  bonus_level_offset: 1,
  ms_per_chapter: 20,
  jewels: ['triangle', 'square', 'circle'],
  key_bonus_map: 'bonus_map',
  reward_notif_interval: 2,
  rewards_key: 'rewards_given',
  key_prefix: 'key_',
  lock_prefix: 'f_lock',
  unlock_prefix: 'f_unlock',
  ab_test_defaults: {
    lives_gen: lives_gen / 60000,
    map_skip_max_level: 0
  },
  tutorials_firebase: ['welcome_to_mahjong', 'match_pair'],
  tutorials_facebook: ['welcome_to_mahjong', 'match_pair'],
  max_retry: 2
};
