/* global _, config */

/* jshint ignore:start */
import util.underscore as _;
import .config as config;
/* jshint ignore:end */

exports = (function () {
  'use strict';

  var obj = {
    game_intro: {
      1: [
        {
          id: 'welcome_to_mahjong',
          always_show: true,
          message: 'tap_to_continue',
          msg_text: 'default',
          message_offset: -100,
          offsetY: -200
        },
        {
          id: 'match_pair',
          always_show: true,
          msg_text: 'default',
          message: 'tap_to_continue',
          message_offset: -100,
          offsetY: -200
        }
      ],
      2: [
        {
          id: 'match_only_free',
          always_show: true,
          msg_text: 'default',
          message: 'tap_to_continue',
          message_offset: -100,
          offsetY: -200
        }
      ],
      3: [
        {
          id: 'non_free_unmatch',
          always_show: true,
          msg_text: 'default',
          message: 'tap_to_continue',
          message_offset: -40,
          offsetY: -200
        }
      ],
      4: [
        {
          id: 'timer_intro',
          msg_text: 'default',
          message: 'tap_to_continue',
          message_offset: -100,
          offsetY: -200
        },
        {
          id: 'goal_intro',
          msg_text: 'default',
          message: 'tap_to_continue',
          message_offset: -100,
          offsetY: -200
        }
      ]
    },
    bonus_level_intro: [{
      id: 'bonus_level_intro',
      msg_text: 'default',
      offsetY: -200
    }],
    unlock_hint: [{
      id: 'unlock_hint',
      text: 'hint',
      center: true
    }],
    unlock_half_hint: [{
      id: 'unlock_half_hint',
      text: 'half_hint',
      center: true
    }],
    unlock_shuffle: [{
      id: 'unlock_shuffle',
      text: 'shuffle',
      center: true
    }],
    unlock_swoosh: [{
      id: 'unlock_swoosh',
      text: 'swoosh',
      center: true
    }],
    unlock_substitute: [{
      id: 'unlock_substitute',
      text: 'substitute',
      center: true
    }],
    unlock_open_slot: [{
      id: 'unlock_open_slot',
      text: 'open_slot',
      center: true
    }],
    unlock_swap: [{
      id: 'unlock_swap',
      text: 'swap',
      center: true
    }],
    unlock_auto_hint: [{
      id: 'unlock_auto_hint',
      text: 'auto_hint',
      center: true
    }],
    unlock_flip: [{
      id: 'unlock_flip',
      text: 'flip',
      center: true
    }],
    unlock_joker: [{
      id: 'unlock_joker',
      text: 'joker',
      center: true
    }],
    hint_three_match: [{
      id: 'hint_three_match',
      msg_text: 'default',
      message: 'tap_to_continue',
      message_offset: -100,
      offsetY: -200
    }],
    golden_tile_unlock: [{
      id: 'golden_tile_unlock',
      msg_text: 'default',
      message: 'tap_to_continue',
      offsetY: 300,
      message_offset: 100
    }]
  };

  _.each(_.pick(config.unlockables, _.keys(config.powerups.ingame)),
    function (ms, type) {
      obj.game_intro[ms] = [
      {
        id: type + '_intro',
        msg_text: 'default',
        message: type === 'shuffle' ? 'tap_to_continue' :
          'tap_powerup_continue',
        message_offset: -100,
        group: type,
        stick_to_lightbox: true,
        powerup_count: config.award_powerup_count[type]
      }
    ];

      if (type === 'swap') {
        obj.game_intro[ms].push({
          id: 'swap_select_tile_one',
          msg_text: 'default',
          group: type,
          stick_to_lightbox: true
        },
        {
          id: 'swap_select_tiles',
          msg_text: 'default',
          group: type,
          stick_to_lightbox: true
        });
      } else if (type === 'flip') {
        obj.game_intro[ms].push({
          id: 'flip_uncover',
          msg_text: 'default',
          group: type,
          message: 'tap_to_continue',
          info_image: true,
          offsetY: 200,
          message_offset: 50,
          extra: {
            info_image: {
              offsetY: -450
            }
          }
        });
      }
    });

  obj.game_intro.powerup_info = [{
    id: 'powerup_info',
    always_show: true,
    msg_text: 'default',
    message: 'tap_to_continue',
    message_offset: -100,
    center: true
  }];

  obj.game_intro.jewels = [{
    id: 'jewels_hud',
    message: 'tap_to_continue',
    msg_text: 'default',
    center: true,
    offsetY: -200,
    message_offset: -100
  },
  {
    id: 'jewels_match1',
    message: 'tap_to_continue',
    msg_text: 'default',
    info_image: true,
    offsetY: 200,
    message_offset: 0,
    extra: {
      info_image: {
        offsetY: -300
      }
    }
  },
  {
    id: 'jewels_continue',
    message: 'tap_to_continue',
    msg_text: 'default',
    center: true,
    offsetY: -200,
    message_offset: -100
  }];

  obj.game_intro.lock_key = [{
    id: 'lock_key1',
    message: 'tap_to_continue',
    msg_text: 'default',
    info_image: true,
    offsetY: 200,
    message_offset: 50,
    extra: {
      info_image: {
        offsetY: -400
      }
    }
  },
  {
    id: 'lock_key2',
    message: 'tap_to_continue',
    msg_text: 'default',
    info_image: true,
    offsetY: 200,
    message_offset: 50,
    extra: {
      info_image: {
        offsetY: -400
      }
    }
  },
  {
    id: 'lock_key3',
    message: 'tap_to_continue',
    msg_text: 'default',
    center: true,
    offsetY: -200,
    message_offset: -100
  }];

  obj.game_intro.memory = [{
    id: 'memory_match_intro',
    message: 'tap_to_continue',
    msg_text: 'default',
    center: true,
    offsetY: -200,
    message_offset: -100
  },
  {
    id: 'memory_match_time',
    message: 'tap_to_continue',
    msg_text: 'default',
    center: true,
    offsetY: -200,
    message_offset: -100
  },
  {
    id: 'memory_match_mismatch',
    message: 'tap_to_continue',
    msg_text: 'default',
    center: true,
    offsetY: -200,
    message_offset: -100
  }];

  obj.game_intro.partial_memory = [{
    id: 'partial_memory',
    msg_text: 'default',
    message_offset: -100,
    group: 'partial_memory_intro',
    info_image: true,
    offsetY: 100,
    extra: {
      info_image: {
        offsetY: -300
      }
    }
  }];

  obj.game_intro.three_match = [{
    id: 'three_match',
    message: 'tap_to_continue',
    msg_text: 'default',
    message_offset: -100,
    group: 'three_match_intro',
    info_image: true,
    offsetY: 100,
    extra: {
      info_image: {
        offsetY: -300
      }
    }
  }, {
    id: 'three_match_end',
    message: 'tap_to_continue',
    group: 'three_match_intro',
    msg_text: 'default',
    center: true,
    message_offset: -100
  }];

  obj.game_intro.golden_tile = [{
    id: 'golden_tile',
    message: 'tap_to_continue',
    msg_text: 'default',
    group: 'golden_tile_intro',
    info_image: true,
    offsetY: 100,
    extra: {
      info_image: {
        offsetY: -300
      }
    }
  }];

  obj.game_intro.untimed = [{
    id: 'untimed1',
    message: 'tap_to_continue',
    msg_text: 'default',
    group: 'untimed_intro',
    message_offset: -100,
    offsetY: -200
  }, {
    id: 'untimed2',
    message: 'tap_to_continue',
    msg_text: 'default',
    group: 'untimed_intro',
    message_offset: -100,
    offsetY: -200
  }];

  obj.play_popup = {};
  _.each(_.pick(config.unlockables, _.keys(config.powerups.pregame)),
    function (ms, type) {
      obj.play_popup[ms] = [
        {
          id: 'enable_' + type,
          finish_immediate: true,
          msg_text: 'box',
          offsetY: -150,
          powerup_count: config.award_powerup_count[type],
          extra: {
            main_text: {
              offsetX: 0,
              offsetY: 0
            }
          }
        },
        {
          id: 'enable_' + type + '_play',
          msg_text: 'box',
          offsetY: -150,
          extra: {
            main_text: {
              offsetX: 0,
              offsetY: 0
            }
          }
        }
      ];

      switch (type) {
        case 'open_slot':
          obj.game_intro[ms] = [
            {
              id: 'open_slot_intro',
              msg_text: 'default',
              group: 'open_slot_intro',
              message: 'tap_to_continue',
              stick_to_lightbox: true,
              message_offset: -100
            },
            {
              id: 'open_slot_drag',
              msg_text: 'default',
              group: 'open_slot_intro',
              stick_to_lightbox: true
            },
            {
              id: 'open_slot_after',
              msg_text: 'default',
              group: 'open_slot_intro',
              message: 'tap_to_continue',
              stick_to_lightbox: true,
              message_offset: -100
            }
          ];
          break;

        case 'joker':
          obj.game_intro[ms] = [
            {
              id: 'joker_intro',
              msg_text: 'default',
              group: 'joker_intro',
              stick_to_lightbox: true
            },
            {
              id: 'joker_match',
              msg_text: 'default',
              group: 'joker_intro',
              stick_to_lightbox: true
            }
          ];
          break;

        case 'auto_hint':
          obj.game_intro[ms] = [
            {
              id: 'auto_hint_intro',
              msg_text: 'default',
              message: 'tap_to_continue',
              message_offset: -100,
              center: true
            }
          ];
          break;
      }
    }
  );

  obj.play_popup.powerup_info = [{
    id: 'powerup_info',
    always_show: true,
    message: 'tap_to_continue',
    msg_text: 'default',
    offsetY: 350,
    message_offset: -500
  }];

  return obj;
})();
