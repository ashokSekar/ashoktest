exports = {
  currency: {
    cash7: {
      cost: 2,
      quantity: 200
    },
    cash2: {
      cost: 5,
      quantity: 525,
      more: 5
    },
    cash3: {
      cost: 10,
      quantity: 1100,
      more: 10,
      tag: 'popular'
    },
    cash4: {
      cost: 25,
      quantity: 2850,
      more: 15
    },
    cash5: {
      cost: 50,
      quantity: 6000,
      more: 20
    },
    cash6: {
      cost: 100,
      quantity: 12500,
      more: 25,
      tag: 'best_value'
    }
  },
  lives: {
    lives1: {
      cost: 0,
      desc: 'ask_friends',
      request: 'ask_friends',
      quantity: 1
    },
    lives2: {
      cost: 149,
      desc: 'full_set',
      quantity: 5,
      inventory: true,
      fill: true,
      name: 'lives'
    },
    lives3: {
      cost: 499,
      desc: 'one_day_unlimited',
      inventory: true,
      infinite: 86400,
      quantity: 1
    },
    lives4: {
      cost: 999,
      desc: 'three_day_unlimited',
      discount: 30,
      tag: 'popular',
      inventory: true,
      infinite: 259200,
      quantity: 1
    },
    lives5: {
      cost: 1999,
      desc: 'seven_day_unlimited',
      discount: 40,
      inventory: true,
      infinite: 604800,
      quantity: 1
    },
    lives6: {
      cost: 3999,
      desc: 'thirty_day_unlimited',
      discount: 70,
      tag: 'best_value',
      inventory: true,
      infinite: 2592000,
      quantity: 1
    }
  },
  time: {
    cost: 100,
    quantity: 30
  },
  tap: {
    cost: 100,
    quantity: 30
  },
  shuffle: {
    cost: 50,
    inventory: true,
    quantity: 1
  },
  hint: {
    cost: 50,
    inventory: true,
    quantity: 1
  },
  half_hint: {
    cost: 25,
    inventory: true,
    quantity: 1
  },
  magnet: {
    cost: 80,
    inventory: true,
    quantity: 1
  },
  swoosh: {
    cost: 150,
    inventory: true,
    quantity: 1
  },
  substitute: {
    cost: 150,
    inventory: true,
    quantity: 1
  },
  swap: {
    cost: 50,
    inventory: true,
    quantity: 1
  },
  flip: {
    cost: 100,
    inventory: true,
    quantity: 1
  },
  auto_hint: {
    cost: 150,
    inventory: true,
    quantity: 1
  },
  open_slot: {
    cost: 150,
    inventory: true,
    quantity: 1
  },
  joker: {
    cost: 100,
    inventory: true,
    quantity: 1
  },
  gifts: {
    13: {
      type: 'cash',
      quantity: 300,
      bonus: 'first_time_bonus'
    }
  },
  tile0: {
    name: 'classic',
    cost: 0,
    quantity: 1
  },
  tile1: {
    name: 'winter',
    cost: 9999,
    quantity: 1
  },
  tile2: {
    name: 'summer',
    cost: 9999,
    quantity: 1
  },
  tile3: {
    name: 'halloween',
    cost: 9999,
    quantity: 1
  }
};
