exports = {
  unlock_shuffle: 'CgkIt6-q4KgXEAIQDw',
  unlock_hint: 'CgkIt6-q4KgXEAIQEA',
  unlock_swoosh: 'CgkIt6-q4KgXEAIQEQ',
  top_scorer: 'CgkIu-aP1M8HEAIQFQ',
  maps: {
    60: 'CgkIt6-q4KgXEAIQBQ',
    120: 'CgkIt6-q4KgXEAIQBg',
    180: 'CgkIt6-q4KgXEAIQBw',
    240: 'CgkIt6-q4KgXEAIQCA',
    300: 'CgkIt6-q4KgXEAIQCQ',
    360: 'CgkIt6-q4KgXEAIQCg',
    420: 'CgkIt6-q4KgXEAIQCw',
    480: 'CgkIt6-q4KgXEAIQDA',
    540: 'CgkIt6-q4KgXEAIQDQ',
    600: 'CgkIt6-q4KgXEAIQDg'
  },
  solved: {
    10: 'CgkIt6-q4KgXEAIQAA',
    50: 'CgkIt6-q4KgXEAIQAQ',
    100: 'CgkIt6-q4KgXEAIQAg',
    250: 'CgkIt6-q4KgXEAIQAw',
    500: 'CgkIt6-q4KgXEAIQBA'
  }
};
