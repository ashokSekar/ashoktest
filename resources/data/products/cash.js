
module.exports = {
  cash7disc50: {
    no_of_cash: 200,
    l10n: [{
      cost: 1,
      currency: 'USD'
    }]
  },
  cash1: {
    no_of_cash: 100,
    l10n: [{
      cost: 1,
      currency: 'USD'
    }]
  },
  cash7: {
    no_of_cash: 200,
    l10n: [{
      cost: 2,
      currency: 'USD'
    }]
  },
  cash2: {
    no_of_cash: 525,
    l10n: [{
      cost: 5,
      currency: 'USD'
    }]
  },
  cash3: {
    no_of_cash: 1100,
    l10n: [{
      cost: 10,
      currency: 'USD'
    }]
  },
  cash4: {
    no_of_cash: 2850,
    l10n: [{
      cost: 25,
      currency: 'USD'
    }]
  },
  cash5: {
    no_of_cash: 6000,
    l10n: [{
      cost: 50,
      currency: 'USD'
    }]
  },
  cash6: {
    no_of_cash: 12500,
    l10n: [{
      cost: 100,
      currency: 'USD'
    }]
  }
};
