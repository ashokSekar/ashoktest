
module.exports = {
  puzzle: {
    is_array: true,
    id: {type: 'string', required: true},
    timestamp: {type: 'date'},
    time: {type: 'int'},
    tap: {type: 'int'},
    type: {type: 'string', canbe_null: true},
    score: {type: 'int', required: true},
    stars: {type: 'int', max: 3},
    progress: {type: 'int', max: 100},
    count_clear: {type: 'int'},
    app_version: {type: 'string'},
    design_version: {type: 'string'},
    instance_id: {type: 'int'},
    load_attempts: {type: 'int'},
    tiles_placings: {type: 'int'},
    loading_elapsed: {type: 'int'},
    hint: {type: 'int'},
    shuffle: {type: 'int'},
    magnet: {type: 'int'},
    swoosh: {type: 'int'},
    substitute: {type: 'int'},
    swap: {type: 'int'},
    half_hint: {type: 'int'},
    open_slot: {type: 'bool'},
    joker: {type: 'bool'},
    auto_hint: {type: 'bool'},
    free_shuffle: {type: 'int'},
    undo: {type: 'int'},
    tile_set: {type: 'string', canbe_null: true}
  }
};
