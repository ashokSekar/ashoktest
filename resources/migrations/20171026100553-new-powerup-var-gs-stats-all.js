exports.up = function(db, callback) {
  'use strict';

  db.runSql('ALTER TABLE user_gs_stats_all ' +
    'ADD COLUMN substitute tinyint, ' +
    'ADD COLUMN swap tinyint, ' +
    'ADD COLUMN half_hint tinyint, ' +
    'ADD COLUMN open_slot tinyint, ' +
    'ADD COLUMN joker tinyint, ' +
    'ADD COLUMN auto_hint tinyint, ' +
    'ADD COLUMN taps_taken tinyint unsigned DEFAULT 0, ' +
    'ADD COLUMN type varchar(10) DEFAULT NULL, ' +
    'ADD COLUMN tile_set varchar(15) NOT NULL DEFAULT \'classic\'', callback);
};

exports.down = function (db, callback) {
  'use strict';

  db.removeColumn('user_gs_stats_all', 'substitute', function () {
    db.removeColumn('user_gs_stats_all', 'swap', function () {
      db.removeColumn('user_gs_stats_all', 'half_hint', function () {
        db.removeColumn('user_gs_stats_all', 'open_slot', function () {
          db.removeColumn('user_gs_stats_all', 'joker', function () {
            db.removeColumn('user_gs_stats_all', 'auto_hint', function () {
              db.removeColumn('user_gs_stats_all', 'taps_taken', function () {
                db.removeColumn('user_gs_stats_all', 'type', function () {
                  db.removeColumn('user_gs_stats_all', 'tile_set', callback);
                });
              });
            });
          });
        });
      });
    });
  });
};
