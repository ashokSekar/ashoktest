exports.up = function (db, callback) {
  'use strict';

  db.addColumn('fb_user_game', 'magnet', {
    type: 'smallint',
    notNull: true,
    unsigned: true,
    defaultValue: 0
  }, function () {
    db.addColumn('fb_user_game', 'swoosh', {
      type: 'smallint',
      notNull: true,
      unsigned: true,
      defaultValue: 0
    }, callback);
  });
};

exports.down = function (db, callback) {
  'use strict';

  db.removeColumn('fb_user_game', 'magnet', function () {
    db.removeColumn('fb_user_game', 'swoosh', callback);
  });
};
