exports.up = function (db, callback) {
  'use strict';

  db.addColumn('user_game', 'magnet', {
    type: 'smallint',
    notNull: true,
    unsigned:  true,
    defaultValue: 0
  }, function () {
    db.addColumn('user_game', 'swoosh', {
      type: 'smallint',
      notNull: true,
      unsigned:  true,
      defaultValue: 0
    }, callback);
  });
};

exports.down = function (db, callback) {
  'use strict';

  db.removeColumn('user_game', 'magnet', function () {
    db.removeColumn('user_game', 'swoosh', callback);
  });
};

