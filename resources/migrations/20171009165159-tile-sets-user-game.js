'use strict';

exports.up = function(db, callback) {
  db.runSql('ALTER TABLE user_game ' +
    'ADD COLUMN tile_sets VARCHAR(200) DEFAULT NULL', callback);
};

exports.down = function(db, callback) {
  db.removeColumn('user_game', 'tile_sets', callback);
};
