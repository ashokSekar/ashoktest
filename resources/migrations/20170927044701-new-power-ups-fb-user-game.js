exports.up = function(db, callback) {
  'use strict';

  db.runSql('ALTER TABLE fb_user_game ' +
    'ADD COLUMN substitute smallint UNSIGNED DEFAULT 0, ' +
    'ADD COLUMN swap smallint UNSIGNED DEFAULT 0, ' +
    'ADD COLUMN half_hint smallint UNSIGNED DEFAULT 0, ' +
    'ADD COLUMN open_slot smallint UNSIGNED DEFAULT 0, ' +
    'ADD COLUMN joker smallint UNSIGNED DEFAULT 0, ' +
    'ADD COLUMN auto_hint smallint UNSIGNED DEFAULT 0', callback);
};

exports.down = function (db, callback) {
  'use strict';

  db.removeColumn('fb_user_game', 'substitute', function () {
    db.removeColumn('fb_user_game', 'swap', function () {
      db.removeColumn('fb_user_game', 'half_hint', function () {
        db.removeColumn('fb_user_game', 'open_slot', function () {
          db.removeColumn('fb_user_game', 'joker', function () {
            db.removeColumn('fb_user_game', 'auto_hint', callback);
          });
        });
      });
    });
  });
};
