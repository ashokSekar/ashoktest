exports.up = function (db, callback) {
  'use strict';

  db.addColumn('fb_user_game', 'shuffle', {
    type: 'smallint',
    notNull: true,
    unsigned: true,
    defaultValue: 0
  }, function () {
    db.addColumn('fb_user_game', 'hint', {
      type: 'smallint',
      notNull: true,
      unsigned: true,
      defaultValue: 0
    }, callback);
  });
};

exports.down = function (db, callback) {
  'use strict';

  db.removeColumn('fb_user_game', 'shuffle', function () {
    db.removeColumn('fb_user_game', 'hint', callback);
  });
};
