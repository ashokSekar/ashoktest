'use strict';

exports.up = function(db, callback) {
  db.changeColumn('user_gs_stats_all', 'type', {
      type: 'string',
      length: 15
    }, function () {
      db.runSql('CREATE TABLE user_gs_stats_all_exp LIKE user_gs_stats_all',
        function () {
          db.addColumn('user_gs_stats_all_exp', 'reduced_family', {
            type: 'decimal',
            length: '8, 2',
            unsigned: true
          }, callback);
        });
    });
};

exports.down = function(db, callback) {
  db.dropTable('user_gs_stats_all_exp', callback);
};
