'use strict';

exports.up = function(db, callback) {
  db.runSql('ALTER TABLE user_gs_stats_all_exp ' +
    'ADD COLUMN limit_given smallint(5) UNSIGNED NOT NULL DEFAULT 0,' +
    'ADD COLUMN event_id varchar(30) DEFAULT NULL', callback);
};

exports.down = function(db, callback) {
  db.removeColumn('user_gs_stats_all_exp', 'limit_given', function () {
    db.removeColumn('user_gs_stats_all_exp', 'event_id', callback);
  });
};
