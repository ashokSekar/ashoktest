exports.up = function(db, callback) {
  'use strict';

  // Number of tiles that were placed
  db.addColumn('user_gs_stats_all', 'tiles_placings', {
    type: 'smallint',
    unsigned:  true,
    defaultValue: 0
  }, function () {
    // Number of puzzles was loaded
    db.addColumn('user_gs_stats_all', 'load_attempts', {
      type: 'smallint',
      unsigned:  true,
      defaultValue: 0
    }, function () {
      // Time it took to load puzzle
      db.addColumn('user_gs_stats_all', 'loading_elapsed', {
        type: 'smallint',
        unsigned:  true,
        defaultValue: 0
      }, callback);
    });
  });
};

exports.down = function (db, callback) {
  'use strict';

  db.removeColumn('user_gs_stats_all', 'tiles_placings', function () {
    db.removeColumn('user_gs_stats_all', 'load_attempts', function () {
      db.removeColumn('user_gs_stats_all', 'loading_elapsed', callback);
    });
  });
};
