'use strict';

exports.up = function(db, callback) {
  db.addColumn('user_gs_stats_all', 'free_shuffle', {
    type: 'smallint',
    notNull: true,
    unsigned: true,
    defaultValue: 0
  }, function () {
    db.addColumn('user_gs_stats_all', 'undo', {
      type: 'smallint',
      notNull: true,
      unsigned: true,
      defaultValue: 0
    }, callback);
  });
};

exports.down = function(db, callback) {
  db.removeColumn('user_gs_stats_all', 'free_shuffle', function () {
    db.removeColumn('user_gs_stats_all', 'undo', callback);
  });
};
