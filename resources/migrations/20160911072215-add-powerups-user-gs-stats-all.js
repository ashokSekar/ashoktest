exports.up = function (db, callback) {
  'use strict';

  db.addColumn('user_gs_stats_all', 'shuffle', {
    type: 'smallint',
    notNull: true,
    unsigned: true,
    defaultValue: 0
  }, function () {
    db.addColumn('user_gs_stats_all', 'hint', {
      type: 'smallint',
      notNull: true,
      unsigned: true,
      defaultValue: 0
    }, callback);
  });
};

exports.down = function (db, callback) {
  'use strict';

  db.removeColumn('user_gs_stats_all', 'shuffle', function () {
    db.removeColumn('user_gs_stats_all', 'hint', callback);
  });
};

