
exports.up = function(db, callback) {
  'use strict';

  db.runSql('ALTER TABLE user_gs_stats_all MODIFY COLUMN taps_taken ' +
    'smallint unsigned', function () {
      db.runSql('ALTER TABLE user_gs_stats_all_exp MODIFY COLUMN taps_taken ' +
        'smallint unsigned', function () {
          callback();
        });
    });
};

exports.down = function(db, callback) {
  'use strict';

  db.runSql('ALTER TABLE user_gs_stats_all MODIFY COLUMN taps_taken tinyint ' +
    'unsigned', function () {
      db.runSql('ALTER TABLE user_gs_stats_all_exp MODIFY COLUMN taps_taken ' +
        'tinyint unsigned', function () {
          callback();
        });
    });
};
