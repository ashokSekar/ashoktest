exports.up = function (db, callback) {
  'use strict';

  db.addColumn('user_game', 'shuffle', {
    type: 'smallint',
    notNull: true,
    unsigned:  true,
    defaultValue: 0
  }, function () {
    db.addColumn('user_game', 'hint', {
      type: 'smallint',
      notNull: true,
      unsigned:  true,
      defaultValue: 0
    }, callback);
  });
};

exports.down = function (db, callback) {
  'use strict';

  db.removeColumn('user_game', 'shuffle', function () {
    db.removeColumn('user_game', 'hint', callback);
  });
};

