module.exports = {
  access_control_prod: {
    origin: 'https://apps-180709025702831.apps.fbsbx.com',
    headers: 'Origin, X-Requested-With, Content-Type, Accept'
  },
  powerups: {
    pregame: ['open_slot', 'joker', 'auto_hint'],
    ingame: ['hint', 'shuffle', 'magnet', 'swoosh', 'swap', 'substitute',
      'half_hint']
  },
  cross_promotion: {
    id: 'sudokuquest',
    universal_link: 'https://sudokuquest.com/play',
    min_ms: 30,
    android: {
      package: 'com.hashcube.sudokuquest',
      name: 'Sudoku Quest',
      url: 'https://play.google.com/store/apps/details?' +
        'id=com.hashcube.sudokuquest&referrer=utm_source%' +
        '3Dmajong_quest_android%26utm_medium%3Dcross_promotion%' +
        '26utm_content%3Dmajong_quest%26utm_campaign%3DMajong%' +
        '2520Quest%2520Cross%2520Promotion',
      cash: 100,
      min_version_fb_user: '0.4.6'
    },
    ios: {
      package: 'com.hashcube.sudokuquest',
      name: 'Sudoku Quest',
      url: 'itms-apps://itunes.apple.com/app/id671755288',
      cash: 100,
      min_version_fb_user: '0.4.7'
    },
    kindle: {
      package: 'com.hashcube.sudokuquest',
      name: 'Sudoku Quest',
      url: 'https://www.amazon.com/gp/product/B00ECIKGYG/ref=' +
        'mas_pm_Sudoku%20Quest',
      cash: 100,
      min_version_fb_user: '0.5.0'
    }
  },
  rewards_min_version: '0.7.11',
  rewards: [{
    id: 'starter_pack',
    min_ms: 30,
    type: 'discount',
    product_id: 'cash7',
    paid_user: false,
    amount: 50,
    validity: 48
  }],

  //This config to push starterpack again to filter out bug in client side
  starterpack_bug: {
    id: 'starter_pack',
    versions: ['0.7.10', '0.7.11'],
    new_id: 'starter_pack_01',
    validity: 192 // 8 Days
  },
  purchase_validation_url: {
    amazon: 'https://appstore-sdk.amazon.com/version/1.0/' +
      'verifyReceiptId/developer/'
  },

  update_available_path: 'resources/updates.json',
  bonus_map_data_path: 'resources/bonus_map.json',

  user_resp_prop: ['tile_sets'],
  sync_resp_prop: ['bonus_map'],
  force_update_versions: ['0.6.71'],
  bonus_map_min_version: {
    golden: '0.6.90',
    jewels: '0.7.10'
  }
};
