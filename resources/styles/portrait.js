exports = {
  tile_size: {

    // heigh and width of tile
    width: 98,
    height: 130,

    // Value in pixels of the gradient of the asset
    gradient: {
      x: 10,
      y: 10
    }
  },
  tile_face: {
    autoFontSize: true,
    color: 'black'
  },
  tile_tag: {
    layout: 'box',
    width: 30,
    height: 30,
    left: 5,
    top: 5
  },
  screen_puzzle: {
    layout: 'linear',
    justifyContent: 'start'
  },
  hud: {
    layout: 'linear',
    centerX: true,
    order: 1
  },
  pause: {
    layout: 'box',
    inLayout: false,
    right: -2,
    top: 2
  },
  undo: {
    layout: 'box',
    inLayout: false,
    top: 2,
    left: -5,
    zIndex: 1
  },
  goals_hud: {
    layout: 'linear',
    width: 500,
    height: 122,
    direction: 'horizontal',
    justifyContent: 'space-outside',
    centerX: true
  },
  goals_hud_left: {
    layout: 'box',
    order: 0
  },
  goals_hud_right: {
    layout: 'box',
    order: 2
  },
  limit: {
    width: 114,
    height: 100,
    offsetX: -3,
    bottom: 0,
    spacing: 0,
    centerX: true,
    characterData: {
      0: {image: 'resources/images/puzzlescreen/timer_0.png'},
      1: {image: 'resources/images/puzzlescreen/timer_1.png'},
      2: {image: 'resources/images/puzzlescreen/timer_2.png'},
      3: {image: 'resources/images/puzzlescreen/timer_3.png'},
      4: {image: 'resources/images/puzzlescreen/timer_4.png'},
      5: {image: 'resources/images/puzzlescreen/timer_5.png'},
      6: {image: 'resources/images/puzzlescreen/timer_6.png'},
      7: {image: 'resources/images/puzzlescreen/timer_7.png'},
      8: {image: 'resources/images/puzzlescreen/timer_8.png'},
      9: {image: 'resources/images/puzzlescreen/timer_9.png'},
      ':': {image: 'resources/images/puzzlescreen/timer_colon.png'}
    }
  },
  tap_limit: {
    width: 90,
    height: 50,
    bottom: 35
  },
  screen_start_layout: {
    x: 0,
    y: 0,
    tag: 'start',
    layout: 'linear',
    direction: 'vertical',
    scaleMethod: 'stretch',
    justifyContent: 'center'
  },
  screen_start_version: {
    layout: 'box',
    inLayout: false,
    width: 150,
    height: 50,
    right: 10,
    bottom: 20,
    color: '#00c3b7',
    fontFamily: 'Troika',
    size: 33
  },
  screen_start_logo: {
    layout: 'box',
    top: -350,
    centerX: true,
    order: 1
  },
  screen_start_button: {
    layout: 'box',
    centerX: true
  },
  screen_start_play: {
    extend: 'screen_start_button',
    top: 81,
    text: {
      height: 88,
      width: 306,
      size: 52,
      color: '#FFFFFF',
      offsetY: -4,
      shadowColor: '#074201',
      shadowWidth: 4,
      shadowDx: 0,
      shadowDy: 5,
      strokeColor: '#074201',
      strokeWidth: 3,
      fontFamily: 'Troika'
    }
  },
  screen_start_google: {
    extend: 'screen_start_button',
    left: 20,
    bottom: 20,
    inLayout: false
  },
  friends_map: {
    width: 150,
    height: 50,
    offsetY: 121
  },
  player_map: {
    layout: 'box',
    width: 62,
    height: 62
  },
  fb_img_player: {
    width: 69,
    height: 69
  },
  fb_img_friend: {
    width: 49,
    height: 49
  },

  screen_start_facebook: {
    top: 70,
    text: {
      color: '#FFFFFF',
      fontFamily: 'Troika',
      strokeWidth: 2,
      strokeColor: '#001e75',
      shadowColor: '#001e75',
      shadowDy: 4,
      shadowDx: 0,
      padding: '0 0 0 11',
      size: 33
    }
  },
  screen_start_elements: {
    layout: 'linear',
    direction: 'vertical',
    centerX: true,
    height: 400,
    inLayout: false,
    bottom: 0,
    justifyContent: 'start',
    order: 2
  },
  loading_screen: {
    layout: 'linear',
    inLayout: false,
    scaleMethod: 'stretch',
    justifyContent: 'center'
  },
  loading_icon: {
    layout: 'box',
    centerX: true
  },
  loading_msg: {
    layout: 'box',
    size: 80,
    height: 100,
    width: 290,
    color: '#FFFFFF',
    centerX: true,
    strokeColor: '#f79500',
    fontFamily: 'Troika'
  },
  puzzle_screen_bg: {
    layout: 'linear',
    inLayout: false,
    justifyContent: 'start',
    zIndex: -1
  },
  puzzle_bg: {
    layout: 'linear',
    direction: 'horizontal',
    scaleMethod: 'stretch',
    justifyContent: 'space'
  },
  toast_text: {
    color: '#FFFFFF',
    fontfamily: 'Sansita Bold',
    offsetY: 8,
    autoFontSize: true,
    wrap: true,
    autoSize: false
  },
  toast_tool_tip_head: {
    layout: 'box',
    centerX: true,
    centerY: true,
    color: '#FFF545',
    width: 600,
    height: 100,
    size: 35,
    fontFamily: 'Troika',
    offsetY: -35
  },
  toast_tool_tip_content: {
    layout: 'box',
    centerX: true,
    color: '#FFFFFF',
    width: 600,
    height: 100,
    size: 30,
    fontFamily: 'Troika',
    offsetY: 45,
    wrap: true
  },
  toast_magnet: {
    x: 0,
    y: 0,
    offsetX: -30,
    offsetY: 60
  },
  border_left: {
    layout: 'linear',
    offsetY: -34
  },
  border_right: {
    layout: 'linear',
    offsetY: -56
  },
  border_left_tile: {
    scaleMethod: 'tile'
  },
  border_right_tile: {
    scaleMethod: 'tile'
  },
  grid: {
    order: 2,
    layout: 'box',
    width: 560,
    top: 30,
    padding: 80,
    centerX: true,
    centerY: true
  },
  grid_container: {
    layout: 'box',
    centerY: true,
    centerX: true,
    zIndex: 1
  },
  powerups: {
    layout: 'linear',
    direction: 'horizontal',
    justifyContent: 'space-outside',
    width: 610,
    height: 99,
    centerX: true
  },
  powerup_count_holder_ingame: {
    layout: 'box',
    left: 0,
    top: 4
  },
  powerup_count_holder_pregame: {
    layout: 'box',
    left: 12,
    top: 4
  },
  powerup_count_ingame: {
    layout: 'box',
    width: 33,
    height: 33,
    size: 18,
    top: -2,
    color: '#ffffff',
    fontFamily: 'Troika',
    centerX: true
  },
  powerup_count_pregame: {
    layout: 'box',
    width: 48,
    height: 48,
    size: 30,
    color: '#A1612C',
    fontFamily: 'Troika',
    centerX: true
  },
  powerup_name: {
    layout: 'box',
    width: 105,
    height: 30,
    size: 30,
    bottom: 30,
    centerX: true,
    offsetX: -2,
    autoSize: true,
    color: '#FFFFFF',
    fontFamily: 'Troika'
  },
  powerup_level: {
    layout: 'box',
    color: '#A1612C',
    fontFamily: 'Troika',
    centerX: true
  },
  powerup_level_ingame: {
    extend: 'powerup_level',
    size: 25,
    top: 28,
    width: 50,
    height: 32
  },
  powerup_level_pregame: {
    extend: 'powerup_level',
    size: 38,
    width: 63,
    height: 40,
    top: 43,
    color: '#A1612C'
  },
  powerup_cost: {
    size: 22,
    autoSize: false,
    color: '#973100',
    fontFamily: 'Troika',
    verticalAlign: 'bottom',
    offsetY: 4,
    offsetX: 2
  },
  powerup_cost_pregame: {
    extend: 'powerup_cost',
    padding: '0 0 6 8'
  },
  powerup_cost_ingame: {
    extend: 'powerup_cost',
    padding: '0 0 9 8'
  },
  powerup_unlock_name: {
    layout: 'box',
    centerX: true,
    centerAnchor: true,
    height: 70,
    bottom: 58,
    width: 360,
    fontFamily: 'Troika',
    color: '#DF942E',
    shadowColor: '#F7EF44',
    shadowWidth: 2,
    shadowDx: 0,
    shadowDy: 5,
    strokeColor: '#F7EF44',
    strokeWidth: 2,
    size: 70
  },
  powerup_unlock_title: {
    layout: 'box',
    centerX: true,
    centerAnchor: true,
    height: 80,
    width: 500,
    lineHeight: 1,
    fontFamily: 'Troika',
    color: '#FF7930',
    shadowColor: '#FFFFFF',
    shadowWidth: 2,
    shadowDx: 0,
    shadowDy: 5,
    strokeColor: '#FFFFFF',
    strokeWidth: 4,
    size: 80,
    wrap: true,
    autoSize: false,
    autoFontSize: false,
    offsetY: -95
  },
  powerup_unlock_action: {
    layout: 'box',
    bottom: -110,
    height: 80,
    width: 600,
    fontFamily: 'Troika',
    color: '#FFFFFF',
    size: 38
  },
  powerup_unlock_powerup: {
    layout: 'box',
    centerAnchor: true,
    centerX: true,
    centerY: true
  },
  popup_goals: {
    frame: {
      direction: 'vertical'
    },
    title: {
      size: 40,
      offsetY: 5,
      fontFamily: 'Troika',
      color: '#FFFFFF',
      width: 220,
      shadowColor: '#b20000',
      shadowDx: 0,
      shadowDy: 4,
      strokeColor: '#b20000'
    },
    head: {
      height: 80,
      width: 353
    },
    body: {
      height: 225,
      width: 320,
      padding: 10
    },
    footer: {}
  },
  popup_store: {
    frame: {
      direction: 'vertical'
    },
    close: {
      x: 542,
      y: 23
    },
    title: {
      size: 52,
      offsetY: 2,
      width: 278,
      fontFamily: 'Troika',
      strokeColor: '#b20000',
      shadowColor: '#b20000',
      shadowDx: 0,
      shadowDy: 7,
      color: '#fefdff'
    },
    article: {
      padding: '28 0 0 0'
    },
    head: {
      height: 75,
      width: 600,
      top: -15
    },
    body: {
      height: 680,
      width: 532,
      top: 32
    }
  },
  gameover_level_no: {
    layout: 'box',
    width: 445,
    height: 60,
    order: 1,
    color: '#fff7d0',
    strokeColor: '#520045',
    shadowColor: '#520045',
    shadowDx: 0,
    shadowDy: 4,
    size: 50,
    offsetY: -305,
    centerX: true,
    fontFamily: 'Troika'
  },
  gameover_button: {
    width: 180,
    left: 10,
    height: 60,
    text: {
      size: 30
    }
  },
  btn_next_gameover: {
    extend: 'gameover_button',
    offsetY: -10,
    order: 2,
    text: {
      size: 35,
      fontFamily: 'Troika',
      color: '#FFFFFF',
      strokeWidth: 8,
      strokeColor: '#6CAE29',
      padding: '0 0 17 30'
    }
  },
  btn_play_gameover: {
    extend: 'gameover_button',
    text: {
      size: 45,
      fontFamily: 'Troika',
      color: '#FFFFFF',
      strokeWidth: 8,
      centerX: true,
      strokeColor: '#589A0D',
      padding: '0 0 5 50'
    }
  },
  btn_replay_lose_gameover: {
    extend: 'gameover_button',
    centerY: true,
    offsetY: -12,
    text: {
      size: 30,
      fontFamily: 'Troika',
      horizontalAlign: 'right',
      color: '#FFFFFF',
      padding: '0 26 18 0',
      height: 45,
      width: 140
    }
  },
  btn_replay_won_gameover: {
    extend: 'gameover_button',
    order: 3
  },
  btn_goto_map_gameover: {
    extend: 'gameover_button',
    order: 1,
    text: {
      size: 35,
      fontFamily: 'Troika',
      color: '#FFFFFF',
      strokeWidth: 8,
      strokeColor: '#D34D14',
      padding: '0 0 17 30'
    }
  },
  goals_list: {
    layout: 'linear',
    direction: 'horizontal',
    justifyContent: 'center',
    width: 320,
    height: 75,
    centerX: true
  },
  goal_content: {
    layout: 'linear',
    direction: 'vertical',
    justifyContent: 'center',
    centerX: true,
    centerY: true
  },
  goals_text: {
    layout: 'box',
    wrap: true,
    size: 28,
    width: 240,
    height: 50,
    fontFamily: 'Troika',
    color: '#fefdff'
  },
  gameover_score: {
    size: 33,
    offsetY: 15,
    width: 460,
    height: 100,
    fontFamily: 'Troika',
    color: '#FFFFFF',
    shadowColor: '#b20000',
    shadowWidth: 2,
    shadowDx: 0,
    shadowDy: 5,
    strokeColor: '#b20000',
    strokeWidth: 4
  },
  gameover_high_score: {
    extend: 'gameover_score',
    offsetY: -25,
    height: 60,
    size: 33
  },
  popup_gameover: {
    centerX: true,
    frame: {
      direction: 'vertical',
      offsetY: -220
    },
    title: {
      size: 48,
      horizontalAlign: 'center',
      fontFamily: 'Troika',
      width: 270,
      height: 70,
      color: '#FFFFFF',
      strokeColor: '#b20000',
      shadowColor: '#b20000',
      shadowDy: 4,
      shadowDx: 0,
      offsetY: 4
    },
    close: {
      right: 75,
      offsetY: 220
    },
    head: {
      top: 75,
      offsetY: 130,
      height: 80,
      width: 460,
      bottom: 0
    },
    body: {
      top: 12,
      height: 150,
      width: 460,
      centerX: true,
      offsetY: 105,
      layout: 'linear'
    },
    footer: {
      height: 110,
      width: 460,
      top: 90,
      justifyContent: 'space-outside'
    }
  },
  popup_play: {
    frame: {
      direction: 'vertical',
      offsetY: 0
    },
    title: {
      size: 50,
      offsetY: 74,
      horizontalAlign: 'center',
      verticalAlign: 'center',
      fontFamily: 'Troika',
      width: 270,
      height: 70,
      color: '#FFFFFF',
      strokeColor: '#8e0077',
      shadowColor: '#8e0077',
      shadowDx: 0,
      shadowDy: 7
    },
    close: {
      x: 520,
      y: 130
    },
    head: {
      top: 50,
      height: 60,
      width: 510
    },
    body: {
      top: 12,
      height: 431,
      width: 622
    },
    footer: {
      height: 110,
      width: 550,
      top: 20,
      justifyContent: 'space-outside'
    }
  },
  play_popup_text: {
    height: 120,
    width: 622,
    offsetX: 0,
    offsetY: 70,
    size: 38,
    fontFamily: 'Troika',
    wrap: true,
    color: '#FFFFFF',
    strokeColor: '#b20000',
    shadowColor: '#b20000',
    shadowDx: 0,
    shadowDy: 4
  },
  play_popup_motivation: {
    size: 32,
    inLayout: false,
    autoSize: false,
    width: 550,
    height: 120,
    fontFamily: 'Troika',
    color: '#E58316',
    wrap: true,
    offsetY: 340,
    offsetX: 35,
    centerX: true,
    bottom: 0
  },
  popup_bottom: {
    scaleMethod: 'cover'
  },
  popup_bottom_fb: {
    scaleMethod: 'tile'
  },
  popup_bottom_nonfb: {
    scaleMethod: 'tile'
  },
  pregame_powerup_container: {
    layout: 'linear',
    direction: 'horizontal',
    justifyContent: 'space-outside',
    centerX: true,
    centerY: true,
    offsetY: 60,
    height: 162,
    width: 560
  },
  /* START: BUY LIVES POPUP */
  popup_buy_lives: {
    frame: {
      direction: 'vertical'
    },
    title: {
      size: 48,
      width: 257,
      offsetY: -5,
      offsetX: -3,
      color: '#FFFFFF',
      strokeColor: '#b20000',
      shadowColor: '#b20000',
      shadowWidth: 10,
      shadowDx: 0,
      shadowDy: 7,
      fontFamily: 'Troika'
    },
    close: {
      x: 500,
      y: 20
    },
    head: {
      height: 90,
      width: 542
    },
    body: {
      height: 290,
      width: 500
    }
  },
  popup_small: {
    frame: {
      direction: 'vertical'
    },
    article: {
      padding: '30 0 0 0'
    },
    head: {
      height: 60,
      width: 475
    },
    body: {
      height: 255,
      width: 407,
      size: 36,
      fontFamily: 'Troika',
      color: '#FFFFFF',
      strokeColor: '#b20000',
      shadowColor: '#b20000',
      shadowDy: 4,
      shadowDx: 0,
      wrap: true
    },
    footer: {
      height: 70,
      width: 550,
      top: -15
    }
  },
  popup_limit: {
    frame: {
      direction: 'vertical'
    },
    head: {
      height: 82,
      width: 300,
      offsetY: 10
    },
    title: {
      size: 52,
      fontFamily: 'Troika',
      color: '#FFFFFF',
      strokeColor: '#b20000',
      shadowColor: '#b20000',
      shadowDx: 0,
      shadowDy: 7,
      height: 40,
      width: 300,
      offsetY: 8
    },
    body: {
      height: 280,
      width: 558
    },
    close: {
      x: 500,
      y: 44
    },
    footer: {}
  },
  goal_extra_text_left: {
    layout: 'box',
    width: 100,
    height: 25,
    size: 30,
    bottom: 12,
    centerX: true,
    color: '#FFFFFF',
    fontFamily: 'Troika'
  },
  goal_extra_text_right: {
    layout: 'box',
    width: 100,
    height: 35,
    size: 45,
    bottom: 8,
    centerX: true,
    color: '#FFFFFF',
    fontFamily: 'Troika',
    strokeColor: '#000000',
    strokeWidth: 5
  },
  limit_illustration: {
    inLayout: false,
    layout: 'box',
    x: 0,
    y: 300
  },
  outof_limit_content: {
    layout: 'linear',
    direction: 'vertical',
    centerX: true,
    offsetY: 55,
    width: 400,
    justifyContent: 'center'
  },
  outof_limit_subhead: {
    width: 400,
    height: 90,
    wrap: true,
    color: '#FFFFFF',
    fontFamily: 'Troika',
    strokeColor: '#b20000',
    shadowColor: '#b20000',
    shadowDx: 0,
    shadowDy: 4,
    layout: 'box',
    centerX: true,
    size: 42
  },
  outof_limit_buy_bg: {
    centerX: true,
    layout: 'box'
  },
  outof_limit_buy: {
    centerX: true,
    centerY: true,
    text: {
      size: 45,
      width: 250,
      height: 98,
      color: '#FFFFFF',
      shadowColor: '#074201',
      strokeColor: '#074201',
      shadowDy: 4,
      shadowDx: 0,
      fontFamily: 'Troika',
      offsetX: 32,
      centerY: true
    }
  },
  outof_limit_progress_bar: {
    layout: 'box',
    centerY: true,
    centerX: false,
    scaleMethod: '3slice',
    sourceSlices: {
      horizontal: {
        left: 4,
        center: 8,
        right: 4
      }
    },
    x: 8
  },
  outof_limit_progress_bg: {
    layout: 'box',
    centerX: true,
    offsetY: 7
  },
  outof_limit_progress_text: {
    layout: 'box',
    size: 28,
    width: 350,
    height: 45,
    color: '#FFFFFF',
    strokeColor: '#074201',
    fontFamily: 'Troika',
    centerX: true
  },
  popup_new_variation: {
    frame: {
      direction: 'vertical'
    },
    article: {
      padding: '30 0 0 0'
    },
    title: {
      size: 72,
      color: '#FFFFFF',
      fontFamily: 'Troika',
      width: 452,
      offsetY: 50,
      offsetX: 2,
      wrap: true
    },
    head: {
      height: 85,
      width: 475
    },
    body: {
      height: 255,
      width: 407,
      size: 32,
      fontFamily: 'Troika',
      color: '#FFFFFF',
      strokeColor: '#b20000',
      shadowColor: '#b20000',
      shadowDy: 4,
      shadowDx: 0
    }
  },
  popup_gameplay: {
    frame: {
      direction: 'vertical'
    },
    article: {
      padding: '30 0 0 0'
    },
    head: {
      height: 60,
      width: 475
    },
    body: {
      height: 255,
      width: 407,
      size: 36,
      fontFamily: 'Troika',
      color: '#FFFFFF',
      strokeColor: '#b20000',
      shadowColor: '#b20000',
      shadowDy: 4,
      shadowDx: 0,
      wrap: true
    },
    footer: {
      height: 70,
      width: 550,
      top: -15
    }
  },
  gameplay_btn: {
    centerX: true,
    text: {
      height: 70,
      width: 200,
      fontFamily: 'Troika',
      color: '#FFFFFF',
      shadowColor: '#001e75',
      strokeColor: '#001e75',
      shadowDy: 5,
      shadowDx: 0,
      shadowWidth: 7,
      size: 35,
      offsetX: 20,
      centerY: -5
    }
  },
  popup_rate_us: {
    frame: {
      direction: 'vertical'
    },
    title: {
      size: 48,
      fontFamily: 'Troika',
      color: '#FFFFFF',
      width: 510,
      shadowColor: '#b20000',
      shadowDx: 0,
      shadowDy: 4,
      strokeColor: '#b20000'
    },
    head: {
      height: 80,
      width: 353
    },
    body: {
      height: 345,
      width: 507
    },
    close: {
      x: 490,
      y: 20
    },
    footer: {}
  },
  rate_star_container: {
    layout: 'linear',
    layoutWidth: '90%',
    direction: 'horizontal',
    justifyContent: 'space-outside',
    centerX: true,
    top: 80
  },

  rate_star: {
    layout: 'box',
    toggleSelected: true,
    centerX: true,
    top: 0,
    left: 5,
    offsetY: 0,
    offsetX: 0
  },

  rate_check_container: {
    height: 80,
    width: 300,
    centerX: true,
    bottom: 50,
    layout: 'linear',
    direction: 'horizontal',
    justifyContent: 'center'
  },

  rate_check_btn: {
    toggleSelected: true,
    layout: 'box',
    centerY: true
  },

  rate_check_text: {
    layout: 'box',
    centerY: true,
    width: 300,
    height: 50,
    size: 30,
    wrap: true,
    offsetX: 10,
    color: '#FFFFFF',
    horizontalAlign: 'left',
    fontFamily: 'Troika',
    strokeColor: '#b20000',
    shadowColor: '#b20000',
    shadowDx: 0,
    shadowDy: 4,
    verticalAlign: 'middle'
  },
  rate_confirm_btn: {
    visible: false,
    layout: 'box',
    centerX: true,
    top: 50,
    text: {
      size: 38,
      fontFamily: 'Troika',
      color: '#FFFFFF',
      shadowColor: '#074201',
      strokeColor: '#074201',
      shadowDy: 4,
      shadowDx: 0,
      shadowWidth: 4,
      height: 74
    }
  },
  rate_us_layout: {
    layout: 'linear',
    direction: 'vertical',
    centerX: true,
    justifyContent: 'start'
  },

  buy_lives_content: {
    layout: 'linear',
    centerX: true,
    justifyContent: 'space-outside'
  },
  buy_lives_life_text: {
    layout: 'box',
    centerX: true,
    color: '#FFFFFF',
    fontFamily: 'Troika',
    shadowColor: '#520045',
    strokeColor: '#520045',
    shadowDy: 4,
    shadowDx: 0,
    shadowWidth: 4,
    width: 500,
    height: 290,
    size: 35,
    wrap: true
  },
  buy_lives_timer: {
    layout: 'box',
    x: 135,
    y: 266,
    inLayout: false
  },
  buy_lives_time_txt: {
    layout: 'box',
    offsetX: 47,
    offsetY: 28,
    color: '#FFFFFF',
    strokeColor: '#520045',
    shadowColor: '#520045',
    shadowDy: 7,
    shadowDx: 0,
    shadowWidth: 7,
    fontFamily: 'Troika',
    width: 160,
    height: 50,
    size: 50
  },
  btn_life_main: {
    layout: 'box',
    centerX: true,
    text: {
      layout: 'box',
      centerX: true,
      size: 40,
      height: 70,
      width: 300,
      color: '#FFFFFF'
    }
  },
  btn_life_count: {
    centerY: true,
    layout: 'box',
    x: 12,
    r: -0.61,
    offsetX: -10,
    offsetY: 19,
    width: 80,
    height: 75,
    size: 55,
    color: '#FFFFFF',
    strokeColor: '#ca0042',
    strokeWidth: 8,
    fontFamily: 'Troika'
  },
  btn_life_amount: {
    extend: 'text_large',
    layout: 'box',
    centerY: true,
    offsetX: -10,
    offsetY: 24,
    height: 70,
    width: 80,
    right: 0,
    size: 55,
    color: '#FFFFFF',
    strokeColor: '#c30e00',
    strokeWidth: 8,
    fontFamily: 'Troika'
  },
  btn_life_facebook: {
    offsetY: -23,
    offsetX: -12,
    text: {
      centerY: true,
      offsetX: 15,
      height: 70,
      width: 200,
      size: 52,
      fontFamily: 'Troika',
      shadowColor: '#001e75',
      strokeColor: '#001e75',
      shadowDy: 5,
      shadowDx: 0,
      shadowWidth: 7
    }
  },
  btn_life_buy: {
    offsetY: -10,
    text: {
      centerY: true,
      offsetX: 6,
      offsetY: 7,
      height: 70,
      width: 249,
      size: 48,
      fontFamily: 'Troika',
      shadowColor: '#074201',
      strokeColor: '#074201',
      shadowDy: 7,
      shadowDx: 0,
      shadowWidth: 7
    }
  },
  btn_life_count_facebook: {
    x: 4
  },
  btn_life_count_buy: {
    x: 12
  },
  /* END: BUY LIVES POPUP */

  /* START: BUY POWERUP (INGAME) POPUP */
  popup_buy_powerup: {
    frame: {
      direction: 'vertical'
    },
    article: {
      padding: '25 0 0 0'
    },
    close: {
      x: 490,
      y: 12
    },
    head: {
      height: 0,
      width: 0
    },
    body: {
      height: 710,
      width: 540
    }
  },
  buy_powerups_title: {
    layout: 'box',
    height: 40,
    width: 500,
    centerX: true,
    top: -20,
    color: '#FFF262',
    shadowColor: '#FFBB18',
    shadowWidth: 2,
    shadowDx: 0,
    shadowDy: 7,
    strokeColor: '#FC6B0A',
    strokeWidth: 3,
    fontFamily: 'Troika',
    size: 55
  },
  buy_powerups_content: {
    layout: 'linear',
    direction: 'vertical',
    centerX: true,
    offsetY: -40,
    centerY: true,
    justifyContent: 'space-outside'
  },
  buy_powerups_description: {
    layout: 'box',
    height: 32,
    width: 499,
    centerX: true,
    fontFamily: 'Troika',
    color: '#FD5171',
    size: 25
  },
  buy_powerups_currency_bg: {
    layout: 'box',
    inLayout: false,
    x: 290,
    y: -134
  },
  buy_powerups_currency_text: {
    layout: 'box',
    height: 36,
    width: 142,
    x: 75,
    y: 30,
    centerX: true,
    fontFamily: 'Troika',
    color: '#FD5171',
    size: 30
  },
  buy_powerups_container: {
    layout: 'linear',
    direction: 'horizontal',
    justifyContent: 'space-outside',
    height: 89,
    centerX: true
  },
  buy_powerups_container_icon: {
    layout: 'box',
    centerY: true
  },
  buy_powerups_container_count: {
    layout: 'box',
    width: 89,
    height: 71,
    offsetX: -30,
    offsetY: -4,
    centerY: true,
    color: '#FD5171',
    size: 50,
    strokeWidth: 8,
    strokeColor: '#FFFFFF',
    fontFamily: 'Troika'
  },
  buy_powerups_container_buy: {
    centerY: true,
    layout: 'box',
    offsetX: -4,
    text: {
      color: '#FFFFFF',
      size: 25,
      fontFamily: 'Troika',
      strokeWidth: 5,
      strokeColor: '#FD5171',
      padding: '0 0 13 0'
    }
  },
  /* END: BUY POWERUP (INGAME) POPUP */

  /* START: WHY FB POPUP */
  popup_why_fb: {
    frame: {
      direction: 'vertical'
    },
    title: {
      size: 60,
      offsetY: 17,
      fontFamily: 'Troika',
      color: '#FFFFFF',
      width: 360
    },
    article: {
      padding: '0 0 0 0'
    },
    close: {
      x: 550,
      y: 70
    },
    head: {
      height: 140,
      width: 640
    },
    body: {
      height: 700,
      width: 640
    }
  },
  why_fb_content: {
    layout: 'box',
    centerX: true,
    height: 600,
    width: 500,
    justifyContent: 'space-outside'
  },
  why_fb_bg: {
    layout: 'linear',
    direction: 'vertical',
    centerX: true,
    height: 700,
    width: 500,
    offsetY: -40,
    justifyContent: 'space'
  },
  why_fb_text: {
    layout: 'box',
    centerX: true,
    centerY: true,
    wrap: true,
    fontFamily: 'Troika',
    height: 200,
    width: 400,
    offsetY: 20,
    color: '#FFFFFF',
    size: 40
  },
  why_fb_social_panel: {
    layout: 'box',
    centerX: true,
    centerY: true
  },
  why_fb_coin_text: {
    layout: 'box',
    width: 500,
    height: 100,
    centerY: true,
    color: '#FFFFFF',
    size: 40,
    fontFamily: 'Troika'
  },
  why_fb_coin: {
    layout: 'box',
    centerY: true,
    centerX: true,
    offsetY: -70
  },
  why_fb_connect: {
    centerY: true,
    centerX: true,
    layout: 'box',
    offsetY: -100,
    text: {
      left: 13,
      top: 5,
      color: '#FFFFFF',
      size: 35,
      fontFamily: 'Troika',
      strokeWidth: 5,
      strokeColor: '#263391'
    }
  },
  why_fb_connect_start: {
    text: {
      left: 15,
      top: 5,
      offsetX: 8,
      color: '#FFFFFF',
      size: 21,
      fontFamily: 'Troika',
      strokeWidth: 3,
      strokeColor: '#263391'
    }
  },
  /* END: WHY FB POPUP */

  fb_btn_coins: {
    layout: 'box',
    top: -40,
    right: -50,
    width: 200,
    height: 100,
    color: '#FFFFFF',
    size: 45,
    strokeWidth: 5,
    strokeColor: '#8c3605',
    fontFamily: 'Troika'
  },

  /* START: OVERTAKE POPUP */
  popup_overtake: {
    frame: {
      direction: 'vertical'
    },
    title: {
      size: 48,
      fontFamily: 'Troika',
      color: '#FFFFFF',
      strokeColor: '#000000',
      strokeWidth: 2,
      centerX: true,
      height: 30,
      width: 500
    },
    head: {
      height: 80,
      width: 500
    },
    body: {
      height: 400,
      width: 500
    },
    close: {
      x: 490,
      y: 30
    },
    footer: {}
  },
  overtake_content: {
    layout: 'linear',
    direction: 'vertical',
    centerX: true,
    height: 600,
    width: 500
  },
  overtake_bg: {
    centerX: true
  },
  overtake_fb_rank: {
    layout: 'box',
    inLayout: false
  },
  overtake_title: {
    layout: 'box',
    size: 42,
    fontFamily: 'Troika',
    color: '#FFFFFF',
    strokeColor: '#000000',
    strokeWidth: 2,
    centerX: true,
    height: 50,
    width: 400
  },
  overtake_player_pic: {
    layout: 'box',
    centerX: true,
    centerY: true,
    inLayout: false,
    offsetX: 75,
    offsetY: -10,
    height: 94,
    width: 94
  },
  overtake_player_frame: {
    centerX: true,
    centerY: true,
    offsetY: -47,
    offsetX: -12,
    height: 152,
    width: 118
  },
  overtake_player_arrow: {
    offsetX: 110,
    height: 90,
    width: 70
  },
  overtake_player_name: {
    size: 37,
    fontFamily: 'Troika',
    color: '#FFFFFF',
    strokeColor: '#000000',
    strokeWidth: 2,
    centerX: true,
    centerY: true,
    offsetY: 100,
    height: 35,
    width: 90
  },
  overtake_player_score: {
    size: 42,
    fontFamily: 'Troika',
    color: '#FDFF3B',
    strokeColor: '#000000',
    strokeWidth: 2,
    centerX: true,
    centerY: true,
    offsetY: 130,
    height: 35,
    width: 90
  },
  overtake_friend_pic: {
    layout: 'box',
    centerX: true,
    centerY: true,
    inLayout: false,
    offsetX: -90,
    offsetY: 80,
    height: 78,
    width: 78
  },
  overtake_friend_frame: {
    centerX: true,
    centerY: true,
    offsetX: -12,
    offsetY: -12,
    height: 103,
    width: 103
  },
  overtake_friend_arrow: {
    offsetX: -65,
    height: 90,
    width: 70
  },
  overtake_friend_name: {
    size: 37,
    fontFamily: 'Troika',
    color: '#FFFFFF',
    strokeColor: '#000000',
    strokeWidth: 2,
    centerX: true,
    centerY: true,
    offsetY: -70,
    height: 35,
    width: 90
  },
  overtake_friend_score: {
    size: 42,
    fontFamily: 'Troika',
    color: '#56DFF8',
    strokeColor: '#000000',
    strokeWidth: 2,
    centerX: true,
    centerY: true,
    offsetY: -40,
    height: 35,
    width: 90
  },
  btn_overtake_share: {
    layout: 'box',
    extend: 'button',
    centerX: true,
    text: {
      size: 45,
      fontFamily: 'Troika',
      color: '#FFFFFF',
      strokeColor: '#000000',
      strokeWidth: 2,
      height: 45,
      width: 140,
      offsetX: 90,
      offsetY: 20
    }
  },
  /* END: OVERTAKE POPUP */

  /* START: MESSAGE POPUP */
  popup_messages: {
    frame: {
      direction: 'vertical'
    },
    title: {
      size: 45,
      offsetY: 14,
      fontFamily: 'Troika',
      color: '#FFFFFF',
      strokeColor: '#b20000',
      shadowColor: '#b20000',
      shadowDy: 4,
      shadowDx: 0,
      width: 270
    },
    article: {
      padding: 0
    },
    close: {
      x: 552,
      y: 68
    },
    head: {
      height: 150,
      width: 608
    },
    body: {
      height: 660,
      width: 546,
      offsetY: 10,
      zIndex: -1
    },
    footer: {
      top: -32,
      height: 140,
      width: 608,
      justifyContent: 'space',
      verticalAlign: 'bottom'
    }
  },

  messages_layout: {
    centerX: true,
    scrollX: false,
    scrollY: true,
    bounce: true,
    direction: 'vertical',
    useLayoutBounds: true,
    layout: 'linear'
  },

  text_no_message: {
    extend: 'text_medium',
    layout: 'box',
    fontFamily: 'Troika',
    size: 55,
    color: '#FFFFFF',
    strokeColor: '#520045',
    shadowColor: '#520045',
    shadowDx: 0,
    shadowDy: 4,
    centerX: true,
    height: 540,
    width: 500
  },

  btn_send_life_messages: {
    extend: 'button',
    bottom: 0,
    order: 1,
    text: {
      size: 37,
      height: 60,
      width: 140,
      wrap: true,
      fontFamily: 'Troika',
      color: '#FFFFFF',
      strokeColor: '#ca0042',
      shadowColor: '#ca0042',
      shadowDx: 0,
      autoFontSize: true,
      shadowDy: 4,
      offsetX: 26,
      offsetY: 19
    }
  },

  btn_invite_messages: {
    extend: 'button',
    bottom: 0,
    order: 2,
    text: {
      size: 37,
      fontFamily: 'Troika',
      color: '#FFFFFF',
      strokeColor: '#ca0042',
      shadowColor: '#ca0042',
      shadowDx: 0,
      shadowDy: 4,
      height: 45,
      width: 140,
      offsetX: -20,
      offsetY: 10
    }
  },

  message_layout: {
    layout: 'box',
    justifyContent: 'space',
    height: 89,
    width: 500,
    centerX: true,
    top: 5
  },

  message_bg: {
    layout: 'linear',
    direction: 'horizontal',
    justifyContent: 'start',
    height: 89,
    centerX: true
  },

  message_fb_user: {
    order: 1,
    left: 20,
    centerY: true
  },

  message_msg: {
    extend: 'text_medium',
    layout: 'box',
    fontFamily: 'Troika',
    size: 30,
    color: '#FFFFFF',
    strokeColor: '#520045',
    shadowColor: '#520045',
    shadowDy: 4,
    shadowDx: 0,
    width: 300,
    height: 40,
    order: 2,
    left: 30,
    top: 10,
    centerX: true
  },

  message_action: {
    extend: 'text_medium',
    layout: 'box',
    fontFamily: 'Troika',
    size: 30,
    color: '#9738BF',
    width: 300,
    height: 40,
    order: 1,
    offsetY: 30,
    centerX: true
  },

  /* END: RETRY POPUP */

  popup_retry_puzzle_load: {
    frame: {
      direction: 'vertical'
    },
    article: {
      padding: '30 0 0 0'
    },
    head: {
      height: 60,
      width: 475,
      offsetY: 40
    },
    body: {
      height: 255,
      width: 407,
      size: 36,
      fontFamily: 'Troika',
      color: '#FFFFFF',
      strokeColor: '#b20000',
      shadowColor: '#b20000',
      shadowDy: 4,
      shadowDx: 0,
      wrap: true
    },
    footer: {
      height: 70,
      width: 550,
      top: -15
    }
  },

  retry_actions: {
    layout: 'linear',
    direction: 'horizontal',
    justifyContent: 'space-outside',
    width: 407,
    height: 99,
    offsetY: -80,
    centerX: true
  },

  puzzle_load_fail_content: {
    layout: 'linear',
    direction: 'vertical',
    justifyContent: 'space-outside',
    centerX: true,
    width: 600,
    height: 600
  },

  puzzle_load_fail_message: {
    extend: 'text_medium',
    layout: 'box',
    fontFamily: 'Troika',
    size: 34,
    color: '#FFFFFF',
    strokeColor: '#520045',
    shadowColor: '#520045',
    shadowDy: 4,
    shadowDx: 0,
    width: 360,
    height: 140,
    order: 2,
    left: 30,
    top: 10,
    centerX: true,
    wrap: true
  },

  puzzle_load_fail_message_small: {
    extend: 'text_medium',
    layout: 'box',
    fontFamily: 'Troika',
    size: 30,
    color: '#FFFFFF',
    strokeColor: '#520045',
    shadowColor: '#520045',
    shadowDy: 4,
    shadowDx: 0,
    width: 380,
    height: 150,
    order: 2,
    centerX: true,
    wrap: true,
    offsetY: -70
  },

  btn_retry_actions: {
    layout: 'box',
    centerY: true,
    centerX: true,
    text: {
      size: 38,
      wrap: true,
      offsetY: -4,
      strokeWidth: 2,
      strokeColor: '#074201',
      shadowColor: '#074201',
      shadowDx: 0,
      shadowDy: 4,
      color: '#ffffff',
      fontFamily: 'Troika'
    }
  },

  /* END: RETRY POPUP */

  /* START: SETTINGS POPUP */
  popup_settings: {
    frame: {
      direction: 'vertical'
    },
    title: {
      size: 50,
      color: '#FFFFFF',
      strokeColor: '#b20000',
      shadowColor: '#b20000',
      shadowWidth: 10,
      shadowDx: 0,
      shadowDy: 7,
      fontFamily: 'Troika',
      width: 452,
      offsetY: -3,
      offsetX: 2

    },
    head: {
      height: 92,
      width: 452
    },
    close: {
      x: 400,
      y: 28
    },
    body: {
      height: 485,
      width: 402
    }
  },
  popup_pause: {
    frame: {
      direction: 'vertical'
    },
    title: {
      size: 50,
      color: '#FFFFFF',
      strokeColor: '#b20000',
      shadowColor: '#b20000',
      shadowWidth: 10,
      shadowDx: 0,
      shadowDy: 7,
      fontFamily: 'Troika',
      width: 452,
      offsetY: -3,
      offsetX: 2

    },
    head: {
      height: 92,
      width: 452,
      top: 68
    },
    close: {
      x: 400,
      y: 100
    },
    body: {
      height: 485,
      width: 402
    }
  },

  pause_level_no_text: {
    layout: 'box',
    width: 402,
    height: 60,
    order: 1,
    color: '#fff7d0',
    strokeColor: '#520045',
    shadowColor: '#520045',
    shadowDx: 0,
    shadowDy: 4,
    size: 50,
    offsetY: -153,
    inLayout: false,
    fontFamily: 'Troika'
  },

  settings_layout: {
    layout: 'linear',
    direction: 'vertical',
    justifyContent: 'space-outside',
    centerX: true
  },

  settings_control: {
    layout: 'linear',
    direction: 'horizontal',
    justifyContent: 'space-outside',
    layoutHeight: 'wrapContent',
    width: 402,
    centerX: true
  },

  settings_social: {
    layout: 'linear',
    direction: 'vertical',
    justifyContent: 'space-outside',
    width: 402,
    height: 275,
    centerX: true
  },

  settings_app_version_no_text: {
    layout: 'box',
    right: 35,
    bottom: -6,
    width: 150,
    height: 35,
    order: 4,
    color: '#fee330',
    fontFamily: 'Troika',
    size: 25
  },

  settings_btn: {
    centerX: true,
    text: {
      height: 70,
      width: 200,
      fontFamily: 'Troika',
      color: '#FFFFFF',
      shadowColor: '#001e75',
      strokeColor: '#001e75',
      shadowDy: 5,
      shadowDx: 0,
      shadowWidth: 7,
      size: 35,
      offsetX: -1
    }
  },
  settings_btn_resume: {
    text: {
      height: 70,
      width: 200,
      fontFamily: 'Troika',
      color: '#FFFFFF',
      shadowColor: '#074201',
      strokeColor: '#074201',
      shadowDy: 5,
      shadowDx: 0,
      shadowWidth: 7,
      size: 35,
      offsetX: 2
    }
  },
  settings_btn_help: {
    text: {
      height: 70,
      width: 200,
      fontFamily: 'Troika',
      color: '#FFFFFF',
      shadowColor: '#ca0042',
      strokeColor: '#ca0042',
      shadowDy: 5,
      shadowDx: 0,
      shadowWidth: 7,
      size: 35,
      offsetX: 2,
      offsetY: 2
    }
  },
  settings_btn_feedback: {
    text: {
      height: 70,
      width: 200,
      fontFamily: 'Troika',
      color: '#FFFFFF',
      shadowColor: '#ca0042',
      strokeColor: '#ca0042',
      shadowDy: 5,
      shadowDx: 0,
      shadowWidth: 7,
      size: 35,
      offsetX: 0,
      offsetY: 2
    }
  },

  settings_helper: {
    layout: 'linear',
    direction: 'horizontal',
    justifyContent: 'space-outside',
    order: 3,
    width: 402,
    layoutHeight: 'wrapContent',
    centerX: true
  },

  settings_helper_maphelp: {
    centerX: true,
    text: {
      color: '#FFFFFF',
      fontFamily: 'Troika',
      shadowColor: '#ca0042',
      strokeColor: '#ca0042',
      shadowDy: 4,
      shadowDx: 0,
      shadowWidth: 4,
      size: 30
    }
  },
  settings_helper_mapfeedback: {
    centerX: true,
    text: {
      color: '#FFFFFF',
      fontFamily: 'Troika',
      shadowColor: '#ca0042',
      strokeColor: '#ca0042',
      shadowDy: 4,
      shadowDx: 0,
      shadowWidth: 4,
      size: 27
    }
  },

  /* END: SETTINGS POPUP */

  /* START: CROSS PROMOTION POPUP */
  popup_cross_promo: {
    frame: {
      direction: 'vertical'
    },
    title: {
      size: 60,
      offsetY: 17,
      fontFamily: 'Troika',
      color: '#FFFFFF',
      width: 360
    },
    article: {
      padding: '0 0 0 0'
    },
    close: {
      x: 550,
      y: 70
    },
    head: {
      height: 140,
      width: 640
    },
    body: {
      height: 710,
      width: 540
    }
  },
  /* END: CROSS PROMOTION POPUP */

  store_init: {
    layout: 'linear',
    direction: 'vertical',
    centerX: true,
    justifyContent: 'start'
  },

  store_container_viewpool: {
    layout: 'linear',
    direction: 'horizontal',
    justifyContent: 'space',
    centerX: true,
    offsetY: 20
  },

  store_sale_tag: {
    layout: 'box',
    r: 44.1,
    inLayout: false,
    offsetX: -63,
    offsetY: -182,
    zIndex: 1
  },

  store_sale_tag_text: {
    layout: 'box',
    width: 97,
    height: 84,
    offsetX: 46,
    offsetY: 12,
    r: 0.23,
    color: '#FFAC2A',
    fontFamily: 'Troika',
    size: 60,
    wrap: true
  },

  store_sale_timer: {
    layout: 'box',
    inLayout: false,
    zIndex: 1,
    offsetY: 145,
    bottom: 0,
    centerX: true
  },

  store_sale_timer_text: {
    layout: 'box',
    width: 333,
    height: 60,
    zIndex: 1,
    color: '#C65A1A',
    fontFamily: 'Troika',
    size: 25
  },

  store_tabs: {
    layout: 'linear',
    direction: 'horizontal',
    justifyContent: 'space',
    centerX: true,
    centerY: true,
    top: -3
  },

  btn_store_tab: {
    text: {
      size: 38,
      fontFamily: 'Troika',
      strokeColor: '#96651C',
      color: '#FFFFFF'
    }
  },

  btn_store_tab_selected: {
    text: {
      extend: 'btn_store_tab',
      strokeWidth: 2,
      color: '#FFFFFF'
    }
  },

  btn_store_tab_unselected: {
    text: {
      extend: 'btn_store_tab',
      strokeWidth: 0,
      color: '#96651C'
    }
  },

  transaction_loading_sprite: {
    layout: 'box',
    width: 178,
    height: 178,
    centerX: true,
    top: -65,
    order: 0
  },

  transaction_loading_text: {
    centerX: true,
    layout: 'box',
    layoutWidth: '80%',
    height: 50,
    order: 1,
    color: '#fff7d0',
    strokeColor: '#520045',
    shadowColor: '#520045',
    shadowDx: 0,
    shadowDy: 4,
    size: 40,
    fontFamily: 'Troika'
  },

  product_container_quantity_container: {
    layout: 'linear',
    direction: 'vertical',
    centerY: true,
    left: 25,
    offsetY: -10,
    height: 104,
    width: 150
  },

  product_container_quantity_base: {
    layout: 'box',
    color: '#ffffc6',
    strokeColor: '#b20000',
    horizontalAlign: 'center',
    verticalAlign: 'bottom',
    wrap: true,
    height: 70,
    width: 150,
    extend: 'text'
  },

  product_container_quantity: {
    fontFamily: 'Troika',
    horizontalAlign: 'center',
    verticalAlign: 'bottom',
    size: 40,
    extend: 'product_container_quantity_base'
  },

  product_container_quantity_sale: {
    wrap: true,
    inLayout: false,
    left: 24,
    top: -10,
    fontFamily: 'Troika',
    size: 45,
    extend: 'product_container_quantity_base'
  },

  product_container_strike: {
    layout: 'box',
    inLayout: false,
    zIndex: 1,
    top: 70,
    r: 0.06,
    left: 60,
    offsetY: -10
  },

  product_container_more: {
    extend: 'text',
    layout: 'box',
    size: 22,
    height: 34,
    width: 90,
    centerX: true,
    verticalAlign: 'top',
    fontFamily: 'Troika',
    color: '#fdff1f'
  },

  product_container_discount: {
    centerY: true,
    layout: 'box',
    x: 12,
    r: -0.3,
    centerX: true,
    offsetX: -50,
    offsetY: -15,
    width: 80,
    height: 75,
    size: 55,
    color: '#FFFFFF',
    strokeColor: '#593C1F',
    strokeWidth: 5,
    fontFamily: 'Troika',
    shadowColor: '#FFFFFF'
  },

  product_container_decs: {
    layout: 'box',
    left: 25,
    height: 70,
    width: 150,
    wrap: true,
    fontFamily: 'Troika',
    centerY: true,
    size: 28,
    color: '#ffffff',
    lineHeight: 1,
    strokeColor: '#b20000',
    extend: 'text'
  },

  product_container_tag_text: {
    layout: 'box',
    width: 250,
    height: 40,
    centerY: true,
    centerX: true,
    offsetY: 39,
    inLayout: false,
    size: 20,
    fontFamily: 'Troika',
    color: '#C65A1A',
    strokeWidth: 2,
    lineHeight: 1.2
  },
  product_container_buy_btn: {
    layout: 'box',
    centerY: true,
    offsetY: -5,
    right: 30,
    text: {
      size: 27,
      color: '#017c12',
      fontFamily: 'Troika'
    }
  },

  social_panel: {
    padding: '25 20 0 20',
    height: 171,
    width: 640,
    layout: 'box',
    centerX: true
  },

  social_panel_friends_stat: {
    layout: 'linear',
    direction: 'horizontal',
    centerX: true,
    scrollY: false,
    width: 640,
    height: 171
  },

  social_panel_buttons: {
    layout: 'linear',
    direction: 'horizontal',
    justifyContent: 'space-outside',
    offsetY: -30,
    strokeWidth: 4,
    layoutWidth: '100%',
    centerX: true,
    height: 80
  },

  social_panel_btn_fb: {
    text: {
      layout: 'box',
      strokeWidth: 4,
      left: null,
      right: null,
      wrap: true,
      size: 27,
      offsetX: 10,
      fontFamily: 'Troika',
      color: '#FFFFFF',
      strokeColor: '#001e75',
      shadowColor: '#001e75',
      shadowDx: 0,
      shadowDy: 5
    }
  },

  social_panel_msg: {
    layout: 'box',
    width: 300,
    height: 30,
    centerX: true,
    bottom: 2,
    fontFamily: 'Troika',
    size: 28,
    color: '#8B4513'
  },

  social_friend: {
    layout: 'box',
    width: 111,
    height: 134,
    left: 25,
    offsetY: 25
  },

  social_friend_frame: {
    layout: 'linear',
    width: 111,
    height: 134,
    direction: 'vertical',
    justifyContent: 'space-outside',
    zIndex: 2
  },

  social_friend_name: {
    extend: 'text_medium',
    layout: 'box',
    height: 18,
    size: 25,
    offsetY: 12,
    centerX: true,
    fontFamily: 'Troika',
    wrap: false,
    width: 80,
    order: 1,
    top: 70,
    color: '#FFFFFF'
  },

  social_friend_score: {
    extend: 'text_medium',
    layout: 'box',
    height: 20,
    size: 18,
    offsetY: 4,
    fontFamily: 'Troika',
    centerX: true,
    layoutWidth: '80%',
    order: 2,
    color: '#000000'
  },

  social_friend_rank: {
    inLayout: false,
    layout: 'box',
    visible: false,
    x: 0,
    y: -25
  },

  social_friend_rank_text: {
    extend: 'text_medium',
    centerX: true,
    fontFamily: 'Troika',
    height: 19,
    width: 34,
    strokeWidth: 5,
    y: -2,
    layout: 'box',
    color: '#FFFFFF',
    offsetY: 15,
    autoSize: true
  },

  social_friend_heart: {
    offsetX: 70,
    superview: this.frame,
    inLayout: false,
    layout: 'box',
    centerY: true
  },

  social_user_pic: {
    clip: true,
    centerX: true,
    zIndex: 1,
    offsetY: -17
  },

  social_user: {
    height: 90,
    width: 90
  },
  social_fb_user_overlay: {
    visible: false,
    zIndex: 1
  },
  star_box: {
    layout: 'linear',
    justifyContent: 'space-outside',
    direction: 'horizontal',
    width: 445,
    height: 134,
    offsetY: -225,
    centerX: true,
    centerAnchor: true,
    order: 1,
    zIndex: 2,
    visible: true
  },
  star_box_gameover: {
    extend: 'star_box',
    offsetY: -450
  },
  star_box_play: {
    extend: 'star_box',
    width: 500,
    offsetY: -280,
    centerY: true
  },
  btn_ok_small: {
    layout: 'box',
    centerY: true,
    text: {
      size: 38,
      wrap: true,
      offsetY: -4,
      strokeWidth: 2,
      strokeColor: '#074201',
      shadowColor: '#074201',
      shadowDx: 0,
      shadowDy: 4,
      color: '#ffffff',
      fontFamily: 'Troika'
    }
  },

  text_large: {
    color: '#FFFFFF'
  },

  btn_play_play: {
    layout: 'box',
    centerX: true,
    text: {
      layout: 'box',
      centerX: true,
      color: '#FFFFFF',
      fontFamily: 'Troika',
      strokeColor: '#074201',
      shadowColor: '#074201',
      shadowDx: 0,
      shadowDy: 5,
      width: 500,
      offsetY: -5,
      height: 290,
      size: 45
    }
  },
  hud_lives: {
    x: 75,
    width: 45,
    size: 35,
    color: '#973100',
    fontFamily: 'Troika',
    offsetY: 2
  },
  hud_lives_infinite: {
    x: 75,
    width: 100,
    size: 35,
    color: '#973100',
    fontFamily: 'Troika',
    offsetY: 2
  },
  hud_currency: {
    x: 65,
    width: 103,
    size: 35,
    offsetY: 4,
    color: '#973100',
    fontFamily: 'Troika'
  },

  hud_map: {
    layout: 'linear',
    direction: 'horizontal',
    centerX: true,
    justifyContent: 'space',
    layoutHeight: 'wrapContent'
  },
  hud_text: {
    layout: 'box',
    centerY: true,
    centerX: true,
    offsetY: 5,
    offsetX: 10,
    characterData: {
      0: {image: 'resources/images/map_screen/text_0.png'},
      1: {image: 'resources/images/map_screen/text_1.png'},
      2: {image: 'resources/images/map_screen/text_2.png'},
      3: {image: 'resources/images/map_screen/text_3.png'},
      4: {image: 'resources/images/map_screen/text_4.png'},
      5: {image: 'resources/images/map_screen/text_5.png'},
      6: {image: 'resources/images/map_screen/text_6.png'},
      7: {image: 'resources/images/map_screen/text_7.png'},
      8: {image: 'resources/images/map_screen/text_8.png'},
      9: {image: 'resources/images/map_screen/text_9.png'},
      D: {image: 'resources/images/map_screen/text_D.png'},
      a: {image: 'resources/images/map_screen/text_A.png'},
      y: {image: 'resources/images/map_screen/text_Y.png'},
      s: {image: 'resources/images/map_screen/text_S.png'},
      ':': {image: 'resources/images/map_screen/text_colon.png'},
      ' ': {image: 'resources/images/map_screen/text_empty.png'}
    }
  },

  hud_text_lives: {
    extend: 'hud_text',
    height: 30,
    width: 80
  },

  hud_text_lives_infinite: {
    extend: 'hud_text',
    height: 30,
    width: 200
  },

  hud_text_currency: {
    extend: 'hud_text',
    height: 30,
    width: 80
  },

  hud_count: {
    layout: 'box',
    centerY: true,
    centerX: false,
    height: 30,
    width: 80,
    offsetX: -5,
    characterData: {
      0: {image: 'resources/images/map_screen/lives_0.png'},
      1: {image: 'resources/images/map_screen/lives_1.png'},
      2: {image: 'resources/images/map_screen/lives_2.png'},
      3: {image: 'resources/images/map_screen/lives_3.png'},
      4: {image: 'resources/images/map_screen/lives_4.png'},
      5: {image: 'resources/images/map_screen/lives_5.png'},
      '∞': {image: 'resources/images/map_screen/lives_infinite.png'}
    }
  },

  hud_info_text: {
    layout: 'box',
    centerX: true,
    centerY: true,
    offsetY: 4,
    offsetX: 10,
    height: 50,
    width: 103,
    size: 38,
    color: '#973100',
    fontFamily: 'Troika'
  },

  /* START: MAP SCREEN */

  map_achievements: {
    bottom: 15,
    centerX: true
  },
  map_sale_info: {
    layout: 'box',
    inLayout: false,
    top: 50,
    right: 25,
    zIndex: -1
  },
  map_sale_text: {
    layout: 'box',
    size: 18,
    width: 160,
    height: 50,
    offsetY: 20,
    color: '#FFFFFF',
    fontFamily: 'Troika',
    centerX: true
  },
  map_sale_timer_text: {
    layout: 'box',
    width: 110,
    height: 20,
    bottom: 7,
    size: 11,
    color: '#FFFFFF',
    fontFamily: 'Troika',
    centerX: true
  },

  map_messages: {
    bottom: 15,
    right: 10
  },

  map_message_notif: {
    layout: 'box',
    right: 0,
    top: -9,
    inLayout: false
  },

  map_msg_count: {
    layout: 'box',
    centerX: true,
    centerY: true,
    size: 22,
    offsetY: -3,
    height: 25,
    width: 25,
    fontFamily: 'Troika',
    color: '#FFFFFF'
  },

  map_settings_btn: {
    bottom: 15,
    left: 10,
    layout: 'box',
    visible: true,
    centerX: true
  },

  /* END: MAP SCREEN */
  popup_none: {
    frame: {
      direction: 'vertical'
    },
    title: {
      size: 60,
      offsetY: 17,
      fontFamily: 'Troika',
      color: '#FFFFFF',
      width: 360
    },
    article: {
      padding: '0 0 0 0'
    },
    close: {
      x: 550,
      y: 70
    },
    head: {
      height: 140,
      width: 640
    },
    body: {
      height: 700,
      width: 640
    }
  },
  new_variation_content: {
    layout: 'linear',
    direction: 'vertical',
    justifyContent: 'space-outside',
    centerX: true,
    width: 600,
    height: 600
  },
  shuffle_content: {
    layout: 'linear',
    direction: 'vertical',
    justifyContent: 'space-outside',
    centerX: true,
    width: 600,
    height: 600
  },
  shuffle_btn_container: {
    layout: 'linear',
    direction: 'horizontal',
    justifyContent: 'space-outside',
    centerX: true,
    width: 600,
    height: 90
  },
  shuffle_icon: {
    layout: 'box',
    centerX: true
  },
  shuffle_btn_buy: {
    layout: 'box',
    centerX: true,
    text: {
      layout: 'box',
      centerX: true,
      centerY: true,
      padding: '0 0 10 0',
      color: '#FFFFFF',
      strokeColor: '#074201',
      shadowColor: '#074201',
      shadowDx: 0,
      shadowDy: 4,
      fontFamily: 'Troika',
      width: 240,
      left: 40,
      height: 117,
      size: 37,
      offsetY: 4,
      offsetX: 17
    }
  },
  shuffle_btn_give_up: {
    extend: 'shuffle_btn_buy',
    text: {
      layout: 'box',
      centerX: true,
      centerY: true,
      padding: '0 0 10 0',
      color: '#FFFFFF',
      strokeColor: '#ca0042',
      shadowColor: '#ca0042',
      shadowDx: 0,
      shadowDy: 4,
      strokeWidth: 2,
      fontFamily: 'Troika',
      width: 270,
      height: 82,
      size: 35,
      offsetY: 2
    }
  },
  shuffle_msg: {
    layout: 'box',
    top: -50,
    width: 500,
    height: 60,
    centerX: true,
    fontFamily: 'Troika',
    size: 56,
    color: '#fdeb3f'
  },
  shuffle_free_msg: {
    layout: 'box',
    width: 600,
    height: 60,
    centerX: true,
    fontFamily: 'Troika',
    size: 56,
    color: '#FFFFFF'
  },
  shuffle_buy_count: {
    layout: 'box',
    width: 100,
    height: 60,
    right: 4,
    bottom: 25,
    centerX: true,
    strokeColor: '#c30e00',
    shadowColor: '#ca0042',
    shadowDx: 0,
    shadowDy: 7,
    strokeWidth: 4,
    fontFamily: 'Troika',
    size: 50,
    color: '#FFFFFF'
  },

  tutorial_simple_text: {
    visible: false,
    layout: 'box',
    centerY: true,
    centerX: true,
    height: 60,
    width: 520,
    color: '#FFFFFF',
    fontFamily: 'DK Sugary Pancake',
    size: 33
  },
  tutorial_msg_text: {
    visible: false,
    layout: 'box',
    centerY: true,
    centerX: true,
    height: 230,
    width: 430,
    offsetX: 85,
    offsetY: 58,
    color: '#FFFFFF',
    fontFamily: 'DK Sugary Pancake',
    wrap: true,
    size: 28
  },
  tutorial_msg_text_extra: {
    visible: false,
    layout: 'box',
    height: 45,
    centerY: true,
    offsetY: 36,
    centerX: true,
    width: 465,
    color: '#FFFFFF',
    fontFamily: 'DK Sugary Pancake',
    size: 33,
    horizontalAlign: 'right'
  },
  tutorial_info_text: {
    visible: false,
    layout: 'box',
    height: 55,
    centerY: true,
    centerX: true,
    offsetY: 28,
    width: 465,
    color: '#FFFFFF',
    fontFamily: 'DK Sugary Pancake',
    size: 33
  },
  powerup_step_viewpool: {
    width: 608,
    layout: 'linear',
    justifyContent: 'start',
    centerX: true,
    direction: 'vertical',
    padding: '0 10 0 10'
  },
  powerup_step_no_image: {
    layout: 'box',
    zIndex: 1,
    inLayout: false,
    x: 50,
    y: 50
  },
  powerup_step_image: {
    layout: 'box',
    zIndex: 0,
    centerX: true,
    horizontalAlign: 'center'
  },
  popup_powerup_info: {
    frame: {
      direction: 'vertical'
    },
    title: {
      size: 40,
      color: '#FFFFFF',
      strokeColor: '#b20000',
      shadowColor: '#b20000',
      shadowWidth: 10,
      shadowDx: 0,
      shadowDy: 7,
      fontFamily: 'Troika',
      width: 452,
      offsetY: 37,
      offsetX: 2
    },
    head: {
      height: 92,
      width: 452
    },
    close: {
      x: 530,
      y: 45
    },
    body: {
      height: 485,
      width: 608,
      offsetY: 60
    }
  },
  powerup_info_content: {
    layout: 'linear',
    direction: 'vertical',
    centerX: true,
    justifyContent: 'start'
  },
  popup_powerup_info_text: {
    layout: 'box',
    size: 25,
    width: 452,
    height: 50,
    autoSize: false,
    fontFamily: 'Troika',
    color: '#FFFFFF',
    strokeWidth: 8,
    centerX: true,
    wrap: true,
    bottom: 0
  },

  popup_gifting: {
    frame: {
      direction: 'vertical'
    },
    title: {
      size: 35,
      color: '#FFFFFF',
      strokeColor: '#b20000',
      shadowColor: '#b20000',
      shadowWidth: 10,
      shadowDx: 0,
      shadowDy: 7,
      fontFamily: 'Troika',
      width: 452,
      offsetY: -3,
      offsetX: 2

    },
    head: {
      height: 92,
      width: 400
    },
    body: {
      height: 381,
      width: 400
    },
    footer: {
      height: 110,
      width: 400,
      justifyContent: 'space-outside'
    }
  },

  gifting_content: {
    layout: 'linear',
    direction: 'vertical',
    justifyContent: 'space-outside',
    centerX: true,
    width: 600,
    height: 600
  },
  gifting_body_text: {
    layout: 'box',
    order: 1,
    centerX: true,
    color: '#FFFFFF',
    fontFamily: 'Troika',
    shadowColor: '#520045',
    strokeColor: '#520045',
    shadowDy: 4,
    shadowDx: 0,
    shadowWidth: 4,
    width: 300,
    height: 130,
    top: 30,
    size: 40,
    wrap: true
  },
  gifting_info_text: {
    extend: 'gifting_body_text',
    order: 2,
    top: -10,
    size: 35
  },
  btn_accept_gifting: {
    layout: 'box',
    text: {
      layout: 'box',
      centerX: true,
      centerY: true,
      padding: '0 0 10 0',
      color: '#FFFFFF',
      strokeColor: '#074201',
      shadowColor: '#074201',
      shadowDx: 0,
      shadowDy: 4,
      fontFamily: 'Troika',
      width: 240,
      height: 117,
      size: 37,
      offsetY: 2
    }
  },
  btn_close_gifting: {
    layout: 'box',
    text: {
      layout: 'box',
      centerX: true,
      centerY: true,
      padding: '0 0 10 0',
      color: '#FFFFFF',
      strokeColor: '#074201',
      shadowColor: '#074201',
      shadowDx: 0,
      shadowDy: 4,
      fontFamily: 'Troika',
      width: 240,
      height: 117,
      size: 37,
      offsetY: 2
    }
  },
  popup_update: {
    frame: {
      direction: 'vertical'
    },
    title: {
      size: 38,
      color: '#FFFFFF',
      strokeColor: '#b20000',
      shadowColor: '#b20000',
      shadowWidth: 10,
      shadowDx: 0,
      shadowDy: 7,
      fontFamily: 'Troika',
      width: 452,
      offsetY: 38,
      offsetX: 2
    },
    close: {
      x: 550,
      y: 60
    },
    head: {
      height: 92,
      width: 400
    },
    body: {
      height: 950,
      width: 400
    },
    footer: {
      height: 150,
      width: 400,
      top: 90,
      justifyContent: 'space-outside'
    }
  },
  btn_update: {
    centerX: true,
    text: {
      size: 45,
      fontFamily: 'Troika',
      color: '#FFFFFF',
      strokeWidth: 8,
      centerX: true,
      strokeColor: '#589A0D',
      offsetY: -4
    }
  },
  update_popup_content: {
    layout: 'linear',
    direction: 'vertical',
    justifyContent: 'space-outside',
    centerX: true,
    width: 600,
    height: 600
  },
  update_popup_text: {
    layout: 'box',
    centerX: true,
    color: '#FFFFFF',
    fontFamily: 'Troika',
    shadowColor: '#520045',
    strokeColor: '#520045',
    shadowDy: 4,
    shadowDx: 0,
    shadowWidth: 4,
    width: 300,
    height: 130,
    top: 30,
    size: 40,
    wrap: true
  },
  cross_promo_content: {
    layout: 'linear',
    direction: 'vertical',
    justifyContent: 'space-outside',
    centerX: true,
    width: 600,
    height: 600
  },
  cross_promo_info: {
    fontFamily: 'Troika',
    size: 36,
    color: '#FFFFFF',
    wrap: true,
    width: 540
  },
  cross_promo_currency: {
    fontFamily: 'Troika',
    color: '#FFFFFF',
    strokeWidth: 3,
    strokeColor: '#8A4014',
    size: 44,
    width: 100,
    height: 80
  },
  cross_promo_btn: {
    layout: 'box',
    centerX: true,
    text: {
      layout: 'box',
      centerX: true,
      centerY: true,
      padding: '0 0 10 0',
      color: '#FFFFFF',
      strokeColor: '#074201',
      shadowColor: '#074201',
      shadowDx: 0,
      shadowDy: 4,
      fontFamily: 'Troika',
      width: 240,
      height: 117,
      size: 37
    }
  },
  cross_promo_pulldown_tag: {
    inLayout: false,
    x: 70,
    y: 0
  },
  cross_promo_map_btn: {
    layout: 'box',
    right: 20,
    top: 200,
    text: {
      layout: 'box',
      centerY: true,
      right: -8,
      horizontalAlign: 'right',
      color: '#FFFFFF',
      strokeColor: '#8A4014',
      fontFamily: 'Troika',
      width: 100,
      height: 50,
      size: 25
    }
  },
  toast_puzzle_load: {
    layout: 'box',
    centerX: true,
    centerY: true,
    color: '#FFFFFF',
    width: 600,
    height: 100,
    size: 60,
    fontFamily: 'Troika'
  },
  toast_shuffle_progress: {
    layout: 'box',
    centerX: true,
    centerY: true,
    color: '#FFFFFF',
    width: 600,
    height: 200,
    size: 60,
    wrap: true,
    fontFamily: 'Troika',
    offsetY: 0
  },
  toast_motivation_msg: {
    layout: 'box',
    centerX: true,
    centerY: true,
    color: '#FFFFFF',
    width: 600,
    height: 200,
    size: 36,
    wrap: true,
    fontFamily: 'Troika',
    offsetY: 10
  },
  toast_substitute: {
    layout: 'box',
    centerX: true,
    centerY: true,
    offsetY: 60,
    color: '#FFFFFF',
    width: 600,
    height: 100,
    size: 30,
    fontFamily: 'Troika'
  },
  toast_tile_face_left: {
    layout: 'box',
    centerX: true,
    centerY: true,
    offsetX: -120,
    offsetY: -20
  },
  toast_tile_face_right: {
    layout: 'box',
    centerX: true,
    centerY: true,
    offsetX: 120,
    offsetY: -20
  },
  toast_substitute_arrow: {
    layout: 'box',
    centerX: true,
    centerY: true,
    height: 100,
    width: 100,
    offsetY: -20
  },
  bonus_level_star_info: {
    layout: 'box',
    size: 30,
    color: '#FFFFFF',
    centerX: true,
    centerY: true,
    offsetY: 36,
    offsetX: -5,
    fontFamily: 'Troika',
    zIndex: 2
  },
  play_bonus_star: {
    layout: 'box',
    centerX: true,
    y: -25
  },
  play_bonus_info: {
    size: 80,
    autoSize: false,
    width: 622,
    height: 220,
    fontFamily: 'Troika',
    color: '#FFFFFF',
    wrap: true,
    strokeWidth: 8,
    strokeColor: '#A85F0B',
    offsetY: -250
  },
  /* START: SALE POPUP */
  popup_sale: {
    frame: {
      direction: 'vertical'
    },
    title: {
      size: 44,
      offsetY: 17,
      fontFamily: 'Troika',
      color: '#FFFFFF',
      width: 360
    },
    article: {
      padding: '0 0 0 0'
    },
    close: {
      x: 560,
      y: 60
    },
    head: {
      height: 140,
      width: 640
    },
    body: {
      height: 710,
      width: 540
    }
  },
  sale_content: {
    layout: 'linear',
    direction: 'vertical',
    justifyContent: 'space-outside',
    centerX: true,
    offsetY: -25,
    width: 600,
    height: 600,
    order: 0
  },
  store_btn: {
    layout: 'box',
    centerX: true,
    offsetY: -120,
    text: {
      layout: 'box',
      centerX: true,
      centerY: true,
      padding: '0 0 10 0',
      color: '#FFFFFF',
      strokeColor: '#074201',
      shadowColor: '#074201',
      shadowDx: 0,
      shadowDy: 4,
      fontFamily: 'Troika',
      width: 240,
      height: 117,
      size: 54
    }
  },
  sale_info: {
    fontFamily: 'Troika',
    size: 48,
    color: '#FFFFFF',
    offsetY: -350,
    wrap: true,
    width: 540,
    height: 100,
    zIndex: 9000,
    order: 4
  },
  sale_end_info: {
    fontFamily: 'Troika',
    size: 30,
    color: '#FFFFFF',
    offsetY: -235,
    wrap: true,
    width: 540,
    height: 100,
    zIndex: 9000,
    order: 4
  },
  /* END: SALE POPUP */
  gameover_bonus_star: {
    inLayout: false,
    x: 168,
    y: -170
  },
  map_bottom_hud: {
    layout: 'linear',
    direction: 'horizontal',
    centerX: true,
    bottom: -1,
    justifyContent: 'space-outside'
  },
  bottom_hud_btn: {
    centerX: true,
    centerY: true,
    offsetY: 13
  },
  /* START: TILE STORE POPUP */
  tile_store_init: {
    layout: 'linear',
    direction: 'vertical',
    centerX: true,
    justifyContent: 'space-outside'
  },
  popup_tile_store: {
    frame: {
      direction: 'vertical'
    },
    title: {
      size: 44,
      offsetY: 17,
      fontFamily: 'Troika',
      color: '#FFFFFF',
      width: 360
    },
    article: {
      padding: '0 0 0 0'
    },
    close: {
      x: 560,
      y: 60
    },
    head: {
      height: 140,
      width: 640
    },
    body: {
      height: 710,
      width: 540
    }
  },
  /* END: TILE STORE POPUP */
  tile_tab: {
    layout: 'linear',
    direction: 'horizontal',
    justifyContent: 'space-outside',
    width: 540,
    centerX: true,
    top: 15,
    order: 1,
    scaleMethod: '3slice',
    sourceSlices: {
      horizontal: {
        left: 5,
        center: 31,
        right: 5
      }
    },
    destSlices: {
      horizontal: {
        left: 5,
        center: 520,
        right: 5
      }
    }
  },
  buy_tile_btn: {
    layout: 'box',
    centerX: true,
    centerY: true,
    offsetY: -75,
    text: {
      layout: 'box',
      centerX: true,
      centerY: true,
      padding: '0 0 10 0',
      color: '#FFFFFF',
      strokeColor: '#074201',
      shadowColor: '#074201',
      shadowDx: 0,
      shadowDy: 4,
      fontFamily: 'Troika',
      width: 240,
      height: 117,
      size: 34
    }
  },
  tile_set_btn: {
    layout: 'box',
    centerY: true,
    offsetY: 14,
    text: {
      fontFamily: 'Troika',
      size: 24,
      color: '#be8825',
      centerX: true,
      offsetX: 10,
      centerY: true,
      offsetY: -40,
      width: 110,
      height: 80
    }
  },
  tile_set_display: {
    layout: 'box',
    centerX: true,
    centerY: true,
    offsetY: -10
  },
  popup_bonus_desc: {
    layout: 'box',
    size: 32,
    width: 450,
    height: 125,
    offsetY: 35,
    fontFamily: 'Troika',
    color: '#FFFFFF',
    centerX: true,
    wrap: true
  },
  bonus_locked: {
    centerX: true,
    layout: 'box'
  },
  bonus_locked_info: {
    layout: 'box',
    size: 52,
    width: 445,
    height: 60,
    fontFamily: 'Troika',
    color: '#FFFFFF',
    strokeColor: '#B20000',
    centerX: true,
    centerY: true,
    offsetY: 95
  },
  bonus_locked_msg: {
    extend: 'bonus_locked_info',
    size: 36,
    height: 80,
    offsetY: 155,
    lineHeight: 1,
    autoFontSize: true,
    wrap: true,
    autoSize: false
  },
  popup_bonus_locked: {
    frame: {
      direction: 'vertical'
    },
    title: {
      size: 48,
      fontFamily: 'Troika',
      color: '#FFFFFF',
      width: 510,
      shadowColor: '#b20000',
      shadowDx: 0,
      shadowDy: 4,
      strokeColor: '#b20000'
    },
    head: {
      height: 80,
      width: 353
    },
    body: {
      height: 249,
      width: 473
    },
    close: {
      x: 490,
      y: 20
    },
    footer: {}
  },
  gifting_img: {
    zIndex: 3,
    x: 100,
    y: 250,
    centerAnchor: true
  },
  map_icon: {
    layout: 'box',
    right: 20,
    centerY: true,
    offsetY: 200,
    text: {
      layout: 'box',
      centerX: true,
      centerY: true,
      offsetY: 30,
      lineHeight: 1,
      padding: '0 0 0 0',
      color: '#074201',
      fontFamily: 'Troika',
      width: 75,
      height: 50,
      size: 34,
      wrap: true
    }
  },
  progress_info: {
    layout: 'linear',
    direction: 'horizontal',
    justifyContent: 'space-outside',
    inLayout: false,
    zIndex: 1,
    y: 27,
    x: 227
  },
  /* START: REWARD POPUP */
  popup_rewards: {
    frame: {
      direction: 'vertical'
    },
    title: {
      size: 32,
      color: '#FFFFFF',
      strokeColor: '#b20000',
      shadowColor: '#b20000',
      shadowWidth: 10,
      shadowDx: 0,
      shadowDy: 7,
      fontFamily: 'Troika',
      width: 452,
      offsetY: -3,
      offsetX: 2
    },
    head: {
      height: 92,
      width: 452
    },
    close: {
      x: 400,
      y: 28
    },
    body: {
      height: 485,
      width: 402
    }
  },
  reward_popup_content: {
    layout: 'linear',
    direction: 'vertical',
    justifyContent: 'space-outside',
    centerX: true
  },
  reward_popup_offer: {
    height: 100,
    width: 402,
    fontFamily: 'Troika',
    color: '#FF0A00',
    size: 60,
    strokeColor: '#FFFFFF',
    zIndex: 9999,
    strokeWidth: 8,
    offsetY: 10
  },
  reward_popup_quantity: {
    height: 80,
    width: 402,
    fontFamily: 'Troika',
    size: 42,
    offsetY: 100,
    color: '#9A538D'
  },
  reward_popup_prev_price: {
    layout: 'box',
    height: 30,
    width: 402,
    fontFamily: 'Troika',
    size: 34,
    color: '#E19412',
    offsetY: 70,
    centerX: true
  },
  reward_popup_strike: {
    layout: 'box',
    centerX: true,
    offsetY: 16
  },
  reward_popup_new_price: {
    height: 60,
    width: 402,
    fontFamily: 'Troika',
    size: 54,
    color: '#E19412',
    offsetY: 50,
    strokeColor: '#FFFFFF',
    strokeWidth: 4
  },
  reward_popup_time_remining: {
    height: 30,
    width: 402,
    fontFamily: 'Troika',
    size: 30,
    offsetY: 65,
    color: '#FFFFFF'
  },
  reward_popup_get_now_btn: {
    width: 402,
    height: 60,
    offsetY: 60,
    centerX: true,
    text: {
      size: 32,
      fontFamily: 'Troika',
      color: '#FFFFFF',
      strokeWidth: 8,
      centerX: true,
      strokeColor: '#589A0D'
    }
  },
  /* END: REWARD POPUP */

  reward_banner: {
    layout: 'linear',
    direction: 'vertical',
    inLayout: false,
    centerX: true,
    centerY: true,
    zIndex: 9999
  },
  reward_banner_prev_price: {
    height: 40,
    width: 350,
    layout: 'box',
    top: 20,
    centerX: true,
    offsetX: -150,
    fontFamily: 'Troika',
    size: 54,
    color: '#FFFFFF',
    strokeColor: '#006400',
    strokeWidth: 4
  },
  reward_banner_strike: {
    layout: 'box',
    centerX: true,
    offsetY: 20
  },
  reward_banner_new_price: {
    height: 120,
    width: 350,
    top: -35,
    centerX: true,
    layout: 'box',
    offsetX: -150,
    fontFamily: 'Troika',
    size: 48,
    color: '#ff0a00',
    strokeColor: '#FFFFFF',
    strokeWidth: 4
  },
  reward_banner_get_now_btn: {
    offsetY: 80,
    centerX: true,
    text: {
      size: 24,
      fontFamily: 'Troika',
      color: '#FFFFFF',
      strokeWidth: 8,
      height: 50,
      width: 130,
      strokeColor: '#589A0D'
    }
  },
  reward_banner_time_remining: {
    layout: 'box',
    height: 28,
    width: 450,
    fontFamily: 'Troika',
    size: 30,
    centerX: true,
    top: 30,
    color: '#e19412'
  },
  reward_banner_quantity: {
    layout: 'box',
    height: 60,
    width: 200,
    fontFamily: 'Troika',
    size: 54,
    color: '#9a538d',
    strokeColor: '#FFFFFF',
    strokeWidth: 6,
    centerX: true,
    offsetX: 170,
    top: -90
  },
  reward_btn: {
    layout: 'linear',
    direction: 'vertical',
    centerX: true,
    left: 20,
    top: 200
  },
  reward_btn_rays: {
    centerX: true,
    offsetY: -50,
    offsetX: 15
  },
  reward_btn_coins: {
    centerX: true,
    centerY: true,
    offsetX: -10,
    offsetY: 20
  },
  reward_btn_prev_price: {
    layout: 'box',
    height: 25,
    width: 167,
    centerX: true,
    offsetY: -75,
    fontFamily: 'Troika',
    size: 28,
    color: '#FFFFFF',
    strokeColor: '#E19412',
    strokeWidth: 4
  },
  reward_btn_strike: {
    layout: 'box',
    centerX: true,
    offsetY: 12
  },
  reward_btn_new_price: {
    height: 30,
    width: 167,
    centerX: true,
    offsetY: -78,
    fontFamily: 'Troika',
    size: 32,
    color: '#ff0a00',
    strokeColor: '#FFFFFF',
    strokeWidth: 4
  },
  reward_btn_remining_time: {
    height: 20,
    width: 167,
    centerX: true,
    offsetY: -77,
    size: 24,
    fontFamily: 'Troika',
    color: '#e19412'
  },
  reward_btn_stike: {
    offsetY: 12
  },
  event_popup_content: {
    layout: 'linear',
    direction: 'vertical',
    justifyContent: 'space-outside',
    centerX: true
  },
  popup_events: {
    frame: {
      direction: 'vertical'
    },
    title: {
      size: 42,
      offsetY: 17,
      fontFamily: 'Troika',
      color: '#FFFFFF',
      width: 360
    },
    article: {
      padding: '0 0 0 0'
    },
    close: {
      x: 550,
      y: 70
    },
    head: {
      height: 140,
      width: 640
    },
    body: {
      height: 700,
      width: 640
    }
  },
  event_bg: {
    layout: 'box',
    centerX: true,
    inLayout: false
  },
  event_name: {
    layout: 'box',
    height: 200,
    width: 550,
    centerX: true,
    wrap: true,
    fontFamily: 'Troika',
    size: 72,
    lineHeight: 0.8,
    offsetY: -35,
    color: '#FFFFFF',
    strokeWidth: 12,
    strokeColor: '#A52A2A'
  },
  event_text_is: {
    extend: 'event_name',
    height: 100,
    size: 54,
    offsetY: -90
  },
  event_text_here: {
    layout: 'box',
    height: 200,
    lineHeight: 0.4,
    width: 550,
    centerX: true,
    wrap: true,
    fontFamily: 'Troika',
    size: 90,
    offsetY: -175,
    color: '#FFFF00',
    strokeWidth: 12,
    strokeColor: '#A52A2A'
  },
  event_end_info: {
    layout: 'box',
    height: 150,
    offsetY: -224,
    lineHeight: 0.8,
    width: 550,
    centerX: true,
    wrap: true,
    fontFamily: 'Troika',
    size: 46,
    color: '#FFFFFF',
    strokeWidth: 6,
    strokeColor: '#FFA500'
  },
  event_desc: {
    layout: 'box',
    height: 300,
    lineHeight: 0.8,
    offsetY: -410,
    width: 550,
    centerX: true,
    wrap: true,
    fontFamily: 'Troika',
    size: 32,
    color: '#A52A2A'
  },
  event_play_btn: {
    width: 402,
    height: 60,
    offsetY: -315,
    centerX: true,
    text: {
      size: 38,
      fontFamily: 'Troika',
      color: '#FFFFFF',
      strokeWidth: 8,
      centerX: true,
      strokeColor: '#589A0D'
    }
  },
  event_ends_in: {
    layout: 'box',
    height: 100,
    offsetY: -290,
    width: 550,
    centerX: true,
    wrap: true,
    fontFamily: 'Troika',
    size: 36,
    color: '#d25e1f'
  },
  event_hurry: {
    layout: 'box',
    height: 100,
    lineHeight: 0.8,
    offsetY: -250,
    width: 550,
    centerX: true,
    fontFamily: 'Troika',
    size: 80,
    color: '#FFFFFF',
    strokeWidth: 6,
    strokeColor: '#FFA500'
  },
  jewel_style: {
    zIndex: 1,
    centerX: true,
    centerY: true,
    centerAnchor: true,
    offsetY: 10
  },
  jewel_glow_style: {
    zIndex: 0,
    opacity: 0,
    centerY: true,
    centerX: true,
    centerAnchor: true,
    offsetY: 10
  },
  event_finish_content: {
    layout: 'linear',
    direction: 'vertical',
    justifyContent: 'space-outside',
    centerX: true,
    width: 600,
    height: 600
  },
  event_finish_desc: {
    layout: 'box',
    size: 26,
    width: 450,
    height: 125,
    fontFamily: 'Troika',
    color: '#FFFFFF',
    centerX: true,
    wrap: true
  },
  popup_event_finish: {
    frame: {
      direction: 'vertical'
    },
    article: {
      padding: '30 0 0 0'
    },
    title: {
      size: 72,
      color: '#FFFFFF',
      fontFamily: 'Troika',
      width: 452,
      offsetY: 60,
      offsetX: 2,
      wrap: true
    },
    head: {
      height: 120,
      width: 475
    },
    body: {
      height: 200,
      width: 407,
      size: 32,
      fontFamily: 'Troika',
      color: '#FFFFFF',
      strokeColor: '#b20000',
      shadowColor: '#b20000',
      shadowDy: 4,
      shadowDx: 0,
      offsetY: 20
    }
  },
  btn_ok_event_finish: {
    layout: 'box',
    offsetY: -5,
    text: {
      layout: 'box',
      centerX: true,
      centerY: true,
      padding: '0 0 10 0',
      color: '#FFFFFF',
      strokeWidth: 4,
      strokeColor: '#228B22',
      fontFamily: 'Troika',
      width: 240,
      height: 117,
      size: 34,
      offsetY: 2
    }
  }
};
