/* jshint node:true */
"use strict";

var eson = require("eson"),
  device = process.argv[2],
  type = process.argv[3] || "develop",
  file = device === "amazon" || type === "production" ?
    type + "_" + device : type,
  mod = require("../../manifest/" + file),
  fs = require("fs"),
  replace = function (obj) {
    return function (key, val) {
      if (typeof val !== "string") {
        return val;
      }

      val = val.replace(/[{}]/g, "");
      return obj[val] !== undefined ? obj[val] : val;
    };
  },

  data = eson()
    .use(eson.include)
    .use(replace(mod))
    .read("../manifest/base");

console.log("Generating manifest for", file);

fs.writeFileSync("../manifest.json", JSON.stringify(data, null, 2));
