#!/bin/bash
if [ $# -lt 1 ]
then
    echo "Usage : $0 sudoku/gummy/mahjongg test/release android/ios path(optional)"
    exit
fi

target="/vol/projects/downloads/"${1}"/"
root=`git rev-parse --show-toplevel`

folders=(${root}/build/release/native-*/)

if [ ! -z "$3" ]; then
    platform=$3
elif [ ${#folders[@]} = 1 ]; then
    platform=${folders[0]#*release/native-}
else
    printf "Select platform:\n"
    select platform in "${folders[@]#*release/native-}"; do test -n "$platform" && break; echo ">>> Invalid Selection"; done
fi

if [ "$platform" == "amazon" ]; then
    amazon=true;
    platform='android';
fi

platform=${platform%*/}

# echo ""

packagename=""
buildpath=""
version=""
ext=""
sub_folder=""

ios() {
    today=`date +"%Y-%m-%d"`
    folders=(~/Desktop/TeaLeafIOS\ $today*/)
    ipatool=`dirname $0`/ipatool.sh

    if [ ! -z "$1" ]; then
        buildpath=$1
    elif [ ${#folders[@]} = 1 ]; then
        buildpath=${folders[0]}
        buildpath+="TeaLeafIOS.ipa"
    else
        printf "Select archive folder:\n"
        select buildpath in "${folders[@]}"; do test -n "$buildpath" && break; echo ">>> Invalid Selection"; done
        buildpath+="TeaLeafIOS.ipa"
    fi

    version=`${ipatool} "${buildpath}" version`
    name=`${ipatool} "${buildpath}" identifier`
    packagename=${name##*.}
    ext=".ipa"
}

android() {
    files=(${root}/build/release/native-android/*.apk)

    if [ ! -z "$1" ]; then
        buildpath=$1
    elif [ ${#files[@]} = 1 ]; then
        buildpath=${files[0]}
    else
        printf "Select apk:\n"
        select buildpath in "${files[@]}"; do test -n "$buildpath" && break; echo ">>> Invalid Selection"; done
    fi

    version=`aapt d badging ${buildpath} | grep package | awk -F "'" '{print  $6 }'`
    name=`aapt d badging ${buildpath} | grep package | awk -F "'" '{print  $2 }'`
    packagename=${name##*.}
    ext=".apk"
}

eval $platform $4

# printf ""

# swap back platform to copy files to oogway
if [ "$amazon" == true ]; then
    platform='amazon';
fi

filename="${packagename}_v${version}"

case "$2" in
    release)
        target+="release/${platform}/"
        sub_folder = "release/"
    ;;
    test)
        target+="${platform}/"
        timestamp=`date +%Y%m%d_%H%M%S`
        filename+="_${timestamp}"
    ;;
esac

filename+=${ext}

echo "build: ${buildpath}"
echo "copying... ${target}${filename}"
scp "${buildpath}" panda@oogway.hashcube.com:${target}${filename}

echo "Link: oogway.hashcube.com/downloads/${1}/${sub_folder}${platform}/${filename}"
