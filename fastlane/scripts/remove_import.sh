unamestr=`uname`

if [[ "$unamestr" == 'Darwin' ]]; then
  SED=gsed
else
  SED=sed
fi

#cd $(git rev-parse --show-toplevel)
path="src/"
path_quest="modules/quest/src"

if [ -d "$path_quest" ]; then
  path+=" $path_quest"
fi

for module in "$@"
do
  echo "removing ${module} imports"
  for file in $(find ${path} -name '*.js');
  do
    $SED -i '/import '${module}'/d' $file
  done
done
