exports.resolve = function () {
  'use strict';

  return [
    'modules.quest.src.modules.firebase',
    'modules.quest.src.modules.flurry',
    'modules.quest.src.modules.achievements',
    'modules.quest.src.modules.facebook',
    'modules.quest.src.modules.freshchat',
    'modules.quest.src.modules.kochava',
    'modules.quest.src.modules.onesignal',
    'modules.quest.src.modules.crashlytics'
  ];
};
