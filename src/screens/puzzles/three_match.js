/* global BonusScreen, ThreeMatchModel, tutorial_view */

/* jshint ignore:start */
import src.screens.puzzles.bonus as BonusScreen;
import src.views.tutorial as tutorial_view;
import src.models.puzzles.three_match as ThreeMatchModel;
/* jshint ignore:end */

exports = Class(BonusScreen, function (supr) {
  'use strict';

  this.init = function (opts) {
    supr(this, 'init', [opts]);

    this.model = new ThreeMatchModel();
  };

  this.onChangeSelected = function () {
    supr(this, 'onChangeSelected');

    this.loadThirdMatchTutorial();
  };

  this.loadThirdMatchTutorial = function () {
    var type = 'puzzle',
      tutorial = GC.app.tutorial,
      model = this.model,
      grid = this.grid,
      selected_tiles = model.get('tile_selected') || [];

    if (!tutorial.isCompleted('hint_three_match', {
        type: type
      }) && selected_tiles.length === 2) {
      tutorial.build({
        superview: this,
        view: tutorial_view,
        type: type,
        positions: {
          hint_three_match: {
            context: this.hud.hud_info,
            before: function () {
              model.pauseTimers();
              grid.setHandleEvents(false, true);
            },
            after: function () {
              model.resumeTimers();
              grid.setHandleEvents(true, false);
            },
            view: {
              index: 0,
              timeout: 100,
              params: {
                overlay: 'hud_info'
              }
            }
          }
        }
      });

      tutorial.add('hint_three_match', true);
    }
  };

  this.gameOver = function (won, cb) {
    supr(this, 'gameOver', [won, cb, true]);
  };
});
