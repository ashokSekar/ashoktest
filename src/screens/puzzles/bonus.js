/* global _, PuzzleScreen, BonusModel, puzzle_module,
   tutorial_view */

/* jshint ignore:start */
import util.underscore as _;

import src.screens.puzzle as PuzzleScreen;
import src.views.tutorial as tutorial_view;
import src.models.puzzles.bonus as BonusModel;
import quest.modules.puzzle as puzzle_module;
/* jshint ignore:end */

exports = Class(PuzzleScreen, function (supr) {
  'use strict';

  this.init = function (opts) {
    supr(this, 'init', [opts]);

    this.model = new BonusModel();

    _.bindAll(this, 'loadGoldenTileUnlockTutorial');
  };

  this.loadTutorial = function () {
    var type = puzzle_module.getBonusMapType(),
      tutorial_data = this.getTutorial(type),
      tutorial = GC.app.tutorial;

    if (tutorial_data) {
      tutorial.build(tutorial_data);
    }

    if (type === 'golden' && !tutorial.isCompleted('golden_tile_unlock', {
        type: 'puzzle'
      })) {
      this.model.on('tiles-update', this.loadGoldenTileUnlockTutorial);
    }
  };

  this.loadGoldenTileUnlockTutorial = function (tile_array) {
    var puzzle = this.model,
      tutorial = GC.app.tutorial,
      grid = this.grid,
      position, tile_model,
      checkForTutorial = function (item) {
        tile_model = _.isArray(item) ? puzzle.getTile(item) : item;
        if (tile_model && tile_model.isGoldenTile() && tile_model.isFree()) {
          position = tile_model.getCoordinates();

          tutorial.build({
            superview: this,
            view: tutorial_view,
            type: 'puzzle',
            positions: {
              golden_tile_unlock: {
                context: grid.tiles[position[0]][position[1]][position[2]],
                before: function () {
                  puzzle.pauseTimers();
                  grid.setHandleEvents(false, true);
                },
                after: function () {
                  puzzle.resumeTimers();
                  grid.setHandleEvents(true, false);
                },
                view: {
                  index: 0,
                  timeout: 100,
                  params: {
                    overlay: 'tile'
                  }
                }
              }
            }
          });
          puzzle.removeListener('change:ongoing',
            this.loadGoldenTileUnlockTutorial);
          if (!tile_array && puzzle.get('ongoing') > 0) {
            puzzle.on('change:ongoing', this.loadGoldenTileUnlockTutorial);
            return true;
          }
          tutorial.add('golden_tile_unlock', true);
          puzzle.removeListener('tiles-update',
            this.loadGoldenTileUnlockTutorial);
          return true;
        }
      };

    if (tile_array) {
      _.find(tile_array, bind(this, checkForTutorial));
    } else {
      puzzle.eachCell(bind(this, checkForTutorial));
    }
  };

  this.updateGoldenTileForTutorial = function () {
    var open_tiles = [],
      model = this.model,
      matchable_faces = [],
      tile_per_match = model.get('tile_per_match'),
      matching_tile, golden_tile, face, is_golden;

    this.model.eachCell(bind(this, function (tile) {
      face = tile.get('face');

      if (tile.isFree()) {
        if (!_.has(open_tiles, face)) {
          open_tiles[face] = [];
        }
        open_tiles[face].push(tile);

        if (open_tiles[face].length >= tile_per_match) {
          matchable_faces.push(face);
        }
      }

      is_golden = tile.isGoldenTile();
      if (is_golden) {
        if (!golden_tile || tile.isVisible()) {
          golden_tile = tile;
        }
      }
    }));

    model.eachTileFromTop(function (tile) {
      if (!_.contains(matchable_faces, tile.get('face'))) {
        matching_tile = tile;
        return true;
      }
    });

    if (golden_tile && matching_tile) {
      model.doSwap(golden_tile, matching_tile, true);
      golden_tile.emit('change-face');
      matching_tile.emit('change-face');
    }
  };

  this.getTutorial = function (type) {
    var model = this.model,
      grid = this.grid,
      beforeTutorial = function () {
        model.pauseTimers();
        grid.setHandleEvents(false, true);
      },
      afterTutorial = function () {
        model.resumeTimers();
        grid.setHandleEvents(true, false);
      },
      tutorials = {
        golden: {
          superview: this,
          type: 'game_intro',
          milestone: 'golden_tile',
          view: [tutorial_view],
          positions: {
            golden_tile: {
              x: 0,
              y: 0,
              view: {
                index: 0,
                timeout: 100
              },
              before: bind(this, function () {
                this.updateGoldenTileForTutorial();
                beforeTutorial();
              }),
              after: bind(this, function () {
                afterTutorial();
                this.loadGoldenTileUnlockTutorial();
              })
            }
          }
        },
        partial_memory: {
          superview: this,
          type: 'game_intro',
          milestone: 'partial_memory',
          view: [tutorial_view],
          positions: {
            partial_memory: {
              x: 0,
              y: 0,
              view: {
                index: 0,
                timeout: 100
              },
              before: beforeTutorial,
              after: afterTutorial
            }
          }
        },
        three_match: {
          superview: this,
          type: 'game_intro',
          milestone: 'three_match',
          view: [tutorial_view],
          positions: {
            three_match: {
              x: 0,
              y: 0,
              view: {
                index: 0,
                timeout: 100
              },
              before: beforeTutorial,
              after: afterTutorial
            },
            three_match_end: {
              x: 0,
              y: 0,
              view: {
                index: 0,
                timeout: 100
              },
              before: beforeTutorial,
              after: afterTutorial
            }
          }
        },
        jewels: {
          superview: this,
          type: 'game_intro',
          milestone: 'jewels',
          view: [tutorial_view],
          positions: {
            jewels_hud: {
              context: this.hud.hud_info,
              x: 0,
              y: 0,
              view: {
                index: 0,
                timeout: 100,
                params: {
                  overlay: 'hud_info'
                }
              },
              before: beforeTutorial,
              after: afterTutorial
            },
            jewels_match1: {
              x: 0,
              y: 0,
              view: {
                index: 0,
                timeout: 100
              },
              before: beforeTutorial,
              after: afterTutorial
            },
            jewels_continue: {
              x: 0,
              y: 0,
              view: {
                index: 0,
                timeout: 100
              },
              before: beforeTutorial,
              after: afterTutorial
            }
          }
        },
        lock_key: {
          superview: this,
          type: 'game_intro',
          milestone: 'lock_key',
          view: [tutorial_view],
          positions: {
            lock_key1: {
              x: 0,
              y: 0,
              view: {
                index: 0,
                timeout: 100
              },
              before: beforeTutorial,
              after: afterTutorial
            },
            lock_key2: {
              x: 0,
              y: 0,
              view: {
                index: 0,
                timeout: 100
              },
              before: beforeTutorial,
              after: afterTutorial
            },
            lock_key3: {
              x: 0,
              y: 0,
              view: {
                index: 0,
                timeout: 100
              },
              before: beforeTutorial,
              after: afterTutorial
            }
          }
        },
        untimed: {
          superview: this,
          type: 'game_intro',
          milestone: 'untimed',
          view: [tutorial_view],
          positions: {
            untimed1: {
              context: this.hud.limit_view,
              view: {
                index: 0,
                timeout: 100,
                params: {
                  overlay: 'timer'
                }
              },
              before: beforeTutorial,
              after: afterTutorial
            },
            untimed2: {
              x: 0,
              y: 0,
              view: {
                index: 0,
                timeout: 100
              },
              before: beforeTutorial,
              after: afterTutorial
            }
          }
        }
      };

    return tutorials[type];
  };

  this.gameOver = function (won, cb) {
    supr(this, 'gameOver', [won, cb, true]);
  };
});
