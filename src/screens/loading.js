/* global image, style, TextView, ImageView, i18n  */

/* jshint ignore:start */
import ui.ImageView as ImageView;
import ui.TextView as TextView;
import quest.modules.image as image;

import DevkitHelper.style as style;
import DevkitHelper.i18n as i18n;
/* jshint ignore:end */

exports = function (supr) {
  'use strict';

  this.init = function () {
    supr(this, 'init');

    this.icon = new ImageView(style.get('loading_icon', {
      superview: this
    }));

    this.msg = new TextView(style.get('loading_msg', {
      superview: this
    }));
  };

  this.build = function () {
    var icon = image.get('loading_screen/loading');

    supr(this, 'build');
    this.icon.updateOpts({
      image: icon,
      width: icon.getWidth(),
      height: icon.getHeight()
    });

    this.msg.setText(i18n('transaction_loading_title'));
    return this;
  };
};
