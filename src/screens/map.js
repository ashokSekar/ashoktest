/* global TextPromptView, ButtonView, cutscene, powerup_unlock, image, style,
  popup,  retry_popup, Callback, TileStore, puzzle_module, tutorial_view,
  i18n, string, timer, ImageView,
  util, gifting, ImageScaleView, effects, animate */

/* jshint ignore:start */
import event.Callback as Callback;

import DevkitHelper.style as style;
import DevkitHelper.i18n as i18n;

import animate as animate;
import effects as effects;
import util.underscore as _;
import DevkitHelper.timer as timer;
import ui.TextPromptView as TextPromptView;
import ui.ImageView as ImageView;
import ui.widget.ButtonView as ButtonView;
import src.modules.cutscene as cutscene;
import quest.views.powerup_unlock as powerup_unlock;
import quest.modules.gifting as gifting;
import src.views.tutorial as tutorial_view;
import ui.ImageScaleView as ImageScaleView;

/* jsio: if */
import src.modules.util as util;
/* jsio: else */
import quest.modules.util as util;
/* jsio: fi */

/* jsio: if */
import src.modules.popup_manager as popup;
/* jsio: else */
import quest.modules.popup_manager as popup;
/* jsio: fi */

import quest.modules.puzzle as puzzle_module;
import quest.modules.image as image;
import quest.utils.string as string;

import src.views.popups.retry_puzzle_load as retry_popup;
import src.views.popups.tile_store as TileStore;
/* jshint ignore:end */

exports = function (supr) {
  'use strict';

  this.init = function (opts) {
    supr(this, 'init', [opts]);

    this.tile_store = new TileStore();

    this.map_icon = new ButtonView(style.get('map_icon', {
      superview: this
    }));

    this.gifting_image = new ImageView({
      superview: this
    });

    this.overlay = new ImageScaleView({
      superview: this,
      scaleMethod: 'stretch',
      centerAnchor: true
    });

    this.frame_pos = 1;
    _.bindAll(this, 'refresh', 'onTileStore', 'loadMap', 'updateMapTimer',
      'showEventPopup', 'showGiftingPopup');
  };

  this.build = function (map_icon) {
    var start_ms,
      user = GC.app.user,
      overlay = image.get('popup/bg'),
      btn_img = map_icon || image.get('map_screen/bonus_map');

    if (!user.get('screen_bonus')) {
      start_ms = user.getMs('max');
    }

    this.gifting_image.updateOpts(style.get('gifting_img'));

    this.overlay.updateOpts({
      image: overlay,
      width: style.base_width,
      height: style.base_height
    });

    this.gifting_image.on('InputSelect', bind(this, 'showGiftingPopup', false));
    this.overlay.hide();
    supr(this, 'build', [start_ms]);

    this.addCustomLoad();
    this.map_icon.updateOpts({
      height: btn_img.getHeight(),
      width: btn_img.getWidth(),
      images: {
        up: btn_img
      },
      on: {
        up: this.loadMap
      }
    });

    this.map_icon.setState(ButtonView.states.UP);
  };

  this.checkGiftingIcon = function (ms) {
    var user = GC.app.user,
      max_ms = user.getMs('max'),
      gifting_ms = util.getProducts().gifts[ms],
      gifted_ms = user.get('gifted_ms');

    /* Show icon only if didn't got
      the gift and max_ms is within that ms
    */

    return max_ms <= ms &&
      gifting_ms && !_.contains(gifted_ms, ms);
  };

  this.showGiftingPopup = function (ms_id, tag_click) {
    var img,
      ms = ms_id || GC.app.user.getMs('curr'),
      gifting_img = this.gifting_image;

    if (gifting.giftAvailable(ms)) {
      this.overlay.show();
      img = image.get('map_screen/gift/gift-tap-' + this.frame_pos);
      gifting_img.updateOpts({
        image: img,
        width: img.getWidth(),
        height: img.getHeight()
      });
      if (this.frame_pos < 4) {
        gifting_img.show();
        if (this.frame_pos === 1) {
          effects.hover(gifting_img, {
            duration: 500,
            delay: 250,
            scale: 5,
            loop: false
          });
        } else {
          effects.shake(gifting_img);
        }
        this.frame_pos++;
      } else {
        effects.commit(gifting_img, 'shake');
        animate(gifting_img).now({
          scale: 2,
          opacity: 0,
          duration: 1250
        });
        timer.timeout('show_gifting_popup', bind(this, function () {
          this.map.getModel().removeTagById(ms, 'GiftingLevel');
          this.frame_pos = 1;
          this.gifting_image.hide();
          this.overlay.hide();
          supr(this, 'showGiftingPopup', [ms, tag_click]);
        }), 750);
      }
      return true;
    } else {
      return supr(this, 'showGiftingPopup', [ms, tag_click]);
    }
  };

  this.refresh = function (ms) {
    supr(this, 'refresh', [ms]);

    this.loadMapIcon();
  };

  this.loadMapIcon = function () {
    this.map_icon.hide();
    puzzle_module.on('bonus-map-time', this.updateMapTimer);
  };

  this.updateMapTimer = function (time_left) {
    if (time_left > 0) {
      this.map_icon.setTitle(i18n('map_timer_info',
        [string.convertSeconds(time_left)]));
      this.map_icon.show();
    } else {
      this.map_icon.hide();
    }
  };

  this.loadMap = function (bonus) {
    var callback = new Callback();

    if (bonus !== false) {
      bonus = true;
    }
    callback.run(bind(GC.app, GC.app.emit, 'load-map', bonus));
    this.close(callback);
  };

  this.showPlayPopup = function (ms, cb, extra, hide_social) {
    supr(this, 'showPlayPopup', [ms, cb, 'layout', hide_social]);
  };

  this.renderPuzzle = function (data) {
    supr(this, 'renderPuzzle', [data, 'layout']);
  };

  this.puzzleComplete = function (next, bonus_level) {
    supr(this, 'puzzleComplete', [next, bonus_level, 'layout']);
  };

  this.chainStartMs = function (ms, show_play) {
    var user = GC.app.user,
      is_bonus_map = GC.app.user.get('screen_bonus');

    if (user.getMs('max') === user.getMs('curr') && !is_bonus_map) {
      cutscene.register(this, 'map', ms);
    }

    if (!show_play && !is_bonus_map) {
      this.chain('start-ms', this.showEventPopup);
    }

    supr(this, 'chainStartMs', [ms, show_play]);
  };

  this.showEventPopup = function (ms, cb) {
    if (puzzle_module.showEventPopup()) {
      popup.on('events:close', cb);
      popup.once('events:play', bind(this, function () {
        popup.removeListener('events:close', cb);
        popup.close();
        this.loadMap();
      }));
    } else {
      cb();
    }
  };

  this.focusMilestone = function (ms, start, bonus, is_3dtouch) {
    if (!this.buildBonusLevelTutorial()) {
      supr(this, 'focusMilestone', [ms, start, bonus, is_3dtouch]);
    }
  };

  this.buildBonusLevelTutorial = function () {
    var app = GC.app,
      user = app.user,
      last_bonus_level = puzzle_module
        .getLastUnlockedBonus(user.getMs('curr'), 5),
      tutorial = app.tutorial,
      map = this.map,
      node = map.getModel().getNodesById()[last_bonus_level] || {},
      shown = false,
      ctx, bonus_view;

    if (last_bonus_level && !tutorial.isCompleted('bonus_level_intro', {
        type: 'map'
      }) && node.itemViews &&
      node.itemViews.BonusLevel && popup.getStack().length <= 0) {
      bonus_view = node.itemViews.BonusLevel;

      map.focusNodeById(last_bonus_level, function () {
        ctx = {
          onInputSelect: function () {
            bonus_view.emit('InputSelect');
          }
        };

        tutorial.build({
          superview: app.getCurrentView(),
          view: tutorial_view,
          type: 'map',
          on_cancel: function (cb) {
            tutorial.clean();

            if (cb && cb.fire) {
              cb.fire();
            }
          },
          positions: {
            bonus_level_intro: {
              context: bonus_view,
              action: {
                context: ctx
              },
              view: {
                index: 0,
                timeout: 100,
                params: {
                  overlay: 'bonus_icon'
                }
              }
            }
          }
        });

        tutorial.add('bonus_level_intro', true);
      });

      shown = true;
    }

    return shown;
  };

  this.mapEventAvailable = function (ms_current) {
    return !cutscene.isCompleted('map', ms_current) ||
      supr(this, 'mapEventAvailable', [ms_current]);
  };

  this.startMilestone = function (ms, bonus, is_3dtouch) {
    var user = GC.app.user,
      unlock_powerup_view_opts = {
        x: 0,
        y: 0,
        view: {
          index: 0,
          timeout: 0,
          params: {}
        }
      };

    if (user.isLifeAvailable() &&
      user.getMs('max') === user.getMs('curr')) {
      GC.app.tutorial.build({
        superview: this,
        type: 'map',
        milestone: ms,
        disable: false,
        view: [powerup_unlock],
        positions: {
          unlock_hint: unlock_powerup_view_opts,
          unlock_shuffle: unlock_powerup_view_opts,
          unlock_swoosh: unlock_powerup_view_opts,
          unlock_substitute: unlock_powerup_view_opts,
          unlock_open_slot: unlock_powerup_view_opts,
          unlock_joker: unlock_powerup_view_opts,
          unlock_flip: unlock_powerup_view_opts,
          unlock_swap: unlock_powerup_view_opts,
          unlock_half_hint: unlock_powerup_view_opts,
          unlock_auto_hint: unlock_powerup_view_opts
        }
      });
    }

    supr(this, 'startMilestone', [ms, bonus, is_3dtouch]);
  };

  this.loadPuzzle = function (data) {
    var view = this.puzzle_screen.build();

    view.load(data)
      .then(bind(this, function () {
        view.on('puzzle-complete', this.checkMapInStack);
        GC.app.push(view, true);
      }))
      .catch(bind(this, function () {
        this.showRetryPopup(data);
      }));
  };

  /* istanbul ignore next */
  this.addCustomLoad = function () {
    new ButtonView({
      superview: this,
      width: 50,
      height: 50,
      title: 'Load',
      backgroundColor: '#ff5722',
      layout: 'box',
      zIndex: 999,
      right: 0,
      bottom: 0,
      on: {
        up: bind(this, function () {
          var textprompt = new TextPromptView({
            prompt: 'enter id'
          });

          textprompt.on('Submit', bind(this, function (ms) {
            this.renderPuzzle(parseInt(ms, 10));
          }));
          textprompt.showPrompt();
        })
      }
    });
  };

  this.showRetryPopup = function (data) {
    var bonus_level = !!data.bonus_level,
      ms = bonus_level ? GC.app.user.getMs('curr') : data.number;

    retry_popup.build('gameload');
    popup.once('retry-gameload', bind(this, this.startMilestone, ms,
      bonus_level));
  };

  this.showPopup = function (type, is_3dtouch) {
    type = type === 'invites' ? 'messages' : type;
    supr(this, 'showPopup', [type, is_3dtouch]);
  };

  this.onTileStore = function () {
    this.tile_store.build();
  };

  this.on3DTouch = function (tag, origin) {
    var callback = new Callback();

    GC.app.tutorial.clean();
    callback.run(this, function () {
      _.defer(bind(this, this.showPopup, tag, true));
    });

    if (origin && popup.getStack().length > 0) {
      this.removeAllListeners('start-ms');
      popup.closeAll(callback);
    } else {
      callback.fire();
    }
  };

  this.clean = function (cb) {
    puzzle_module.removeListener('bonus-map-time', this.updateMapTimer);
    supr(this, 'clean', [cb]);
    this.gifting_image.hide();
    this.frame_pos = 1;
    this.gifting_image.removeAllListeners('InputSelect');
  };
};
