/* global PuzzleScreen, PuzzleBg, Hud, style, dh_timer, config, shuffle,
  sounds, effects, game_intro, animate, Toast, popup, retry_popup, ViewPool,
  history, SubstituteToast, Callback, device, ImageView, loading, settings,
  Promise, image, test, GoalsPopup, event_manager
*/

/* jshint ignore:start */
import quest.lib.bluebird as Promise;
import event.Callback as Callback;

import util.underscore as _;

import quest.screens.puzzle as PuzzleScreen;
import quest.modules.sounds as sounds;
import DevkitHelper.test as test;
import DevkitHelper.event_manager as event_manager;

import resources.data.config as config;

import DevkitHelper.style as style;
import DevkitHelper.timer as dh_timer;
import DevkitHelper.history as history;

import src.views.puzzle_bg as PuzzleBg;
import src.views.toast as Toast;
import src.views.substitute_toast as SubstituteToast;
import src.views.hud as Hud;
import src.views.popups.shuffle as shuffle;
import src.views.popups.goals as GoalsPopup;

import src.modules.game_intro as game_intro;
import effects;
import animate;
import device;

import quest.modules.popup_manager as popup;

import src.views.popups.retry_puzzle_load as retry_popup;
import quest.views.popups.settings as settings;

import ui.ViewPool as ViewPool;
import ui.ImageView as ImageView;
import quest.modules.image as image;

import DevkitHelper.loading as loading;
/* jshint ignore:end */

exports = Class(PuzzleScreen, function (supr) {
  'use strict';

  var lock_key_timer_ids = [],
    shuffle_count = 0;

  this.init = function (opts) {
    supr(this, 'init', [opts]);

    this.puzzle_bg = new PuzzleBg();
    this.addSubview(this.puzzle_bg);

    this.substitute_toast = new SubstituteToast();

    this.toast_viewpool = new ViewPool({
      ctor: Toast,
      initCount: 3
    });

    this.jewels = new ViewPool({
      ctor: ImageView,
      initCount: 3
    });

    this.keys = new ViewPool({
      ctor: ImageView,
      initCount: 2
    });

    this.hud = new Hud();
    this.addSubview(this.hud);
    _.bindAll(this, 'outOfLimit', 'shuffle', 'onChangeSelected', 'onLoad',
      'onLoadDelay', 'onUILoad', 'autoShuffle', 'handleLoadFail', 'enableGrid',
      'onShuffleFailed', 'substituteOverlay',
      'onRetryShuffle', 'afterLoading', 'animateKey', 'afterGoalClose');

    test.prepare(this, {
      setLockKeyTimerIds: function (val) {
        lock_key_timer_ids = val;
      },
      getShuffleCount: function () {
        return shuffle_count;
      }
    });
  };

  this.build = function () {
    var model = this.model;

    this.puzzle_bg.build();
    this.hud.build(this.model);
    this.hud.addSubview(this.pause_btn);
    model.on('change:ongoing', this.toggleHandleEvents);
    model.on('out-of-limit', this.outOfLimit);
    model.on('auto-shuffle', this.autoShuffle);
    model.on('tiles-matched', this.onTilesMatched);
    model.on('change:tile_selected', this.onChangeSelected);
    model.on('puzzle-loaded', this.onLoad);
    model.once('resize-complete', this.onUILoad);
    model.on('shuffle', this.shuffle);
    model.on('substitute-overlay', this.substituteOverlay);
    model.on('load-failed', this.handleLoadFail);
    model.on('change:limit', this.enableGrid);
    model.on('shuffle-complete', this.onShuffleComplete);

    model.on('key-matched', this.animateKey);

    loading.once('hide', this.afterLoading);

    this.grid.setHeight(this.puzzle_bg.puzzle.style.height -
      style.get('grid').padding);

    return supr(this, 'build', []);
  };

  this.enableGrid = function () {
    this.toggleHandleEvents(null, true);
  };

  this.onChangeSelected = function () {
    this.commitEffects();
  };

  this.afterLoading = function () {
    var classic_id = config.variations.classic.id;

    this.toast_viewpool.releaseAllViews();
    /* Locking back button for blocking
      pause popup within loading */
    if (this.model.get('type') !== classic_id) {
      history.setBusy();
    }
  };

  this.load = function (data) {
    if (data.init.length === 0) {
      this.showTip();
    }
    this.model.on('change:loading_elapsed', this.onLoadDelay);
    return supr(this, 'load', [data]);
  };

  this.onLoadDelay = function (secs) {
    var load_def = config.puzzle_load,
      loading_toast;

    if (secs >= load_def.info_timeout) {
      this.model.removeListener('change:loading_elapsed', this.onLoadDelay);
      loading_toast = this.toast_viewpool.obtainView();
      loading_toast.build(GC.app, 'puzzle_load', {
        text_type: 'puzzle_load',
        offsetY: -250
      });
    }
  };

  this.onLoad = function () {
    this.model.removeListener('change:loading_elapsed', this.onLoadDelay);
    this.toast_viewpool.releaseAllViews();
  };

  this.afterGoalClose = function () {
    // Enabling back button again after close
    history.resetBusy();
    this.model.resumeTimers();
    this.loadTutorial();
  };

  this.buildGoalPopup = function () {
    if (!this.goal_popup) {
      this.goal_popup = new GoalsPopup();
    }
    this.model.pauseTimers();
    this.goal_popup.build(this.model.get('type'));
    popup.once('goals:close', this.afterGoalClose);
  };

  this.onUILoad = function () {
    var classic_id = config.variations.classic.id;

    this.toggleHandleEvents(null, true);
    if (this.model.get('type') !== classic_id) {
      this.buildGoalPopup();
    } else {
      this.loadTutorial();
    }
  };

  this.loadTutorial = function () {
    var golden_id = config.variations.golden.id;

    if (this.model.get('type') !== golden_id) {
      game_intro.register(this);
    }
  };

  this.on3DTouch = function (tag) {
    var callback = new Callback(),
      model = this.model,
      clean = true,
      clear_popup_his = true,
      updateClean = function () {
        clean = false;
      },
      cleanPuzzle = bind(this, function () {
        // Clearing popup history since close with callback wont pop it.
        if (clear_popup_his) {
          history.pop();
        }

        // if gameover popup, clean will be done on closing the popup
        if (clean) {
          // Pop history added by puzzle screen.
          history.pop();
          this.clean();
        }
      });

    model.once('clean-start', updateClean);
    model.increment('ongoing');
    GC.app.set3DTouchPending(tag);
    callback.run(cleanPuzzle);

    if (popup.getStack().length > 0) {
      popup.closeAll(callback);
    } else {
      clear_popup_his = false;
      callback.fire();
    }
  };

  this.gameOver = function (won, cb, hide_social) {
    if (GC.app.user.getMs('curr') === 1) {
      event_manager.emit('level-first-after', 'complete');
    }

    // Hiding social panel for first few levels
    supr(this, 'gameOver', [won, cb, hide_social]);
  };

  this.pause = function (cb, force) {
    var anim = animate.getGroup('match_animate');

    // swoosh match animate has delay of 200 ms
    // b/w two swoosh match animate puzzle
    // if puzzle paused in it b/w calling anim.pause()
    // will have no effect, setting puzzle_paused
    // which will be check during next animation
    // and animation will be paused when its true
    // and resumed when called anim.resume and its
    // value as false
    if (this.model.get('mode') === 'swap') {
      if (cb && cb.fire) {
        cb.fire();
      }

      history.add(this.pause);
      this.model.savePuzzleData('pause');
      return;
    }
    this.model.set('puzzle_paused', true);
    if (anim) {
      anim.pause();
    }

    this.model.increment('ongoing');
    if (this.model.get('type') !== config.variations.memory.id) {
      this.commitEffects();
    }
    GC.app.tutorial.pause();
    supr(this, 'pause', [cb, force]);
    if (dh_timer.hasTimeout('outoflimit_build')) {
      dh_timer.callImmediate('outoflimit_build');
    }

    if (dh_timer.hasTimeout('auto_shuffle')) {
      dh_timer.callImmediate('auto_shuffle');
    }

    if (dh_timer.hasTimeout('substitute')) {
      dh_timer.callImmediate('substitute');
    }
    this.model.pauseTimers();
  };

  this.showSettings = function () {
    settings.build({
      origin: 'puzzle',
      bonus_level: this.model.get('bonus_level'),
      level_no: this.model.get('ms_no')
    });
  };

  this.resume = function (cb) {
    var anim = animate.getGroup('match_animate');

    this.model.set('puzzle_paused', false);
    if (anim) {
      anim.resume();
    }

    this.model.decrement('ongoing');
    this.model.resumeTimers();
    supr(this, 'resume', [cb]);
    GC.app.tutorial.resume();
  };

  this.outOfLimit = function () {
    this.toggleHandleEvents();
    history.setBusy();
    dh_timer.timeout('outoflimit_build', bind(this, function () {
      history.resetBusy();
      this.out_of_limit.build(this.model);
      this.toggleHandleEvents(null, true);
    }), 800);
  };

  this.toggleHandleEvents = function (ongoing, enable) {
    var mode = this.model.get('mode'),
      immediate = mode ? config.powerups.ingame[mode] : true,
      time_remaining = this.model.get(this.model.get('limit_type')) <
        this.model.get('limit');

    if (time_remaining && (enable || ongoing === 0)) {
      this.hud.setHandleEvents(true, false);
      enable = true;
    } else {
      this.hud.setHandleEvents(false, true);
    }
    supr(this, 'toggleHandleEvents',
      [ongoing, time_remaining && (this.model.get('pending_matches') > 0 ||
        !immediate || enable)]);
  };

  this.clearAnimations = function () {
    var anim = animate.getGroup('lock_key');

    if (anim) {
      anim.commit();
    }

    // Animations for lock and key are done using timer also
    // So should unregister timers also to commit animation
    dh_timer.unregister(lock_key_timer_ids);

    lock_key_timer_ids = [];
  };

  this.shuffle = function () {
    var loading_toast = this.toast_viewpool.obtainView(),
      afterShuffle = function () {
        loading_toast.clean(250, 300);
        history.resetBusy();
        sounds.stop('shuffle');
      };

    history.setBusy();

    sounds.play('shuffle');

    this.clearAnimations();

    loading_toast.build(GC.app, 'puzzle_shuffle', {
      text_type: 'shuffle_progress',
      bg: 'puzzlescreen/shuffle_overlay',
      offsetY: 75
    });
    this.model.once('tiles-update', afterShuffle);
  };

  this.substituteOverlay = function (face_one, face_two) {
    sounds.play('substitute');
    this.substitute_toast.build(GC.app, face_one, face_two);
    this.model.once('powerup-applied',
      bind(this.substitute_toast, this.substitute_toast.clean, 300, 200));
  };

  this.autoShuffle = function () {
    if (dh_timer.hasTimeout('outoflimit_build')) {
      return false;
    }
    this.model.pauseTimers();
    shuffle.build(this.model);
  };

  this.commitEffects = function () {
    effects.commit(null, 'hover');
  };

  this.onTilesMatched = function (type) {
    sounds.play(type ? type + '_match' : 'tile_match');
  };

  this.handleLoadFail = function (is_shuffle) {
    if (is_shuffle) {
      this.toast_viewpool.releaseAllViews();
      this.onShuffleFailed();
    } else {
      this.cleanViews();
    }
  };

  this.onShuffleFailed = function () {
    retry_popup.build('shuffle', ++shuffle_count);
    sounds.stop('shuffle');
    popup.once('retry-shuffle', this.onRetryShuffle);
    popup.once('retry-home', bind(this, this.onRetryHome));
  };

  this.onRetryHome = function () {
    shuffle_count = 0;
    popup.removeAllListeners('retry-shuffle');
    this.exit(false, 'retry_puzzle_load');
    if (this.model.get('solved').length > 0) {
      GC.app.user.incrementLives();
    }
  };

  this.onRetryShuffle = function () {
    popup.removeAllListeners('retry-home');
    this.shuffle();
    this.model.shuffleAsync();
  };

  this.getContextForTutorial = function (curr_ms) {
    var x, y, z, ytiles, xtiles,
      tiles = this.grid.tiles,
      model = this.model,
      exceptions = exceptions || [],
      contexts = [],
      powerup = _.findKey(config.unlockables, function (ms) {
        return ms === curr_ms;
      }),
      addToContext = function (tile) {
        var ret = false,
          model, prev_model, x_diff;

        if (tile) {
          model = tile.model;
          if (model.isFree() && contexts.length === 0) {
            contexts.push(tile);
          } else if (_.contains(['joker', 'swap'], powerup) &&
            contexts.length === 1) {
            prev_model = contexts[0].model;
            x_diff = Math.abs(prev_model.getCoordinates()[0] -
              model.getCoordinates()[0]);

            if (powerup === 'swap' &&
              prev_model.get('face') !== model.get('face') &&
              x_diff > 2 && !model.isFree() && model.isVisibleCompletely()) {
              contexts.push(tile);
              ret = true;
            } else if (powerup === 'joker' && !model.isJoker() &&
              model.isFree()) {
              contexts.push(tile);
              ret = true;
            }
          } else if (contexts.length === 1) {
            ret = true;
          }
        }
        return ret;
      },
      extra_rows = device.isTablet ? 6 : 2,
      max_y = Math.floor((model.get('rows') - extra_rows) / 2);

    // Getting top most tiles
    for (z = tiles.length; z >= 0; z--) {
      ytiles = tiles[z] || [];

      // Getting upper half tiles
      for (y = 0; y <= max_y; y++) {
        xtiles = ytiles[y] || [];
        for (x = 0; x <= xtiles.length; x++) {
          if (addToContext(xtiles[x])) {
            return contexts;
          }
        }
      }
    }

    return contexts;
  };

  this.animateKey = function (args) {
    var type = args[0],
      hud_info = this.hud.hud_info,
      anim_key, unlock_img, face_img, puzzle, anim_id,
      tile_model, orig_pos, is_done, position,
      tile_view, orig_face_zIndex, tile_style_anim,
      tile_style, orig_scale, orig_zIndex, tile_position, key_img, tile_set,
      magnifyTile = function (scale, zIndex, limit, direction, id) {
        return new Promise(function (resolve) {
          var mag_scale, mag_zIndex;

          id = 'move_lock_' + id;
          lock_key_timer_ids.push(id);

          dh_timer.register(id, function () {
            tile_style_anim = tile_view.style;
            mag_scale = tile_style_anim.scale + scale;
            mag_zIndex = tile_style_anim.zIndex + zIndex;
            is_done = direction === 'up' ? mag_zIndex >= limit :
              mag_zIndex <= limit;

            if (is_done && direction === 'down') {
              mag_scale = orig_scale;
              mag_zIndex = orig_zIndex;
            }
            tile_view.updateOpts({
              scale: mag_scale,
              zIndex: mag_zIndex
            });
            if (is_done) {
              lock_key_timer_ids.splice(lock_key_timer_ids.indexOf(id), 1);
              dh_timer.unregister(id);
              resolve(true);
            }
          }, 10);
        });
      };

    tile_set = GC.app.user.get('curr_tile_set');
    puzzle = this.model;
    puzzle.increment('ongoing');
    anim_id = 'lock_key';
    tile_model = args[1];
    orig_pos = hud_info.getPosition(this);
    position = tile_model.getCoordinates();
    tile_view = this.grid.tiles[position[0]][position[1]][position[2]];
    tile_style = tile_view.style;
    orig_scale = tile_style.scale;
    orig_zIndex = tile_style.zIndex;
    orig_face_zIndex = tile_view.face.style.zIndex;
    tile_position = tile_view.getPosition(this);

    unlock_img = image.get('puzzle/faces/' +
      tile_set + '/' + config.unlock_prefix + '_' + type);
    face_img = image.get('puzzle/faces/' +
      tile_set + '/' + tile_model.get('face'));
    key_img = image.get('puzzle/hud_' + config.key_prefix + type);
    anim_key = this.keys.obtainView({
      superview: this,
      inLayout: false,
      image: key_img,
      width: key_img.getWidth(),
      height: key_img.getHeight(),
      x: orig_pos.x,
      y: orig_pos.y
    });
    animate(anim_key, anim_id)
      .then({
        x: tile_position.x + 10,
        y: tile_position.y + 10
      }, 900)
      .wait(100)
      .then(bind(this, function () {
        this.keys.releaseView(anim_key);
        return animate(tile_view, anim_id)
          .then(function () {
            return magnifyTile(0.004, 1.5, 90, 'up', Date.now());
          })
          .then(function () {
            return animate(tile_view.face, anim_id)
              .wait(500)
              .then(function () {
                tile_view.face.updateOpts({
                  opacity: 1,
                  image: unlock_img,
                  zIndex: orig_face_zIndex + 100
                });
              }, 300)
              .then({
                opacity: 0
              }, 300)
              .then(function () {
                tile_view.face.updateOpts({
                  image: face_img,
                  zIndex: orig_face_zIndex
                });
              })
              .then({
                opacity: 1
              }, 300)
              .wait(100)
              .then(function () {
                return magnifyTile(-0.004, -1.5,
                  orig_zIndex, 'down', Date.now());
              })
              .wait(500)
              .then(function () {
                tile_view.updateOpts({
                  scale: orig_scale,
                  zIndex: orig_zIndex
                });
                puzzle.decrement('ongoing');
              });
          });
      }));
  };

  this.clean = function (next, cb) {
    this.model.emit('clean-start');
    GC.app.tutorial.clean();
    supr(this, 'clean', [next, cb]);
  };

  this.cleanViews = function () {
    this.toast_viewpool.releaseAllViews();
    this.keys.releaseAllViews();
    supr(this, 'cleanViews');
  };

  this.showTip = function () {
    var user = GC.app.user,
      tooltip_index = user.increment('tooltip_index'),
      tip_toast = this.toast_viewpool.obtainView();

    if (config.no_of_hints < tooltip_index) {
      tooltip_index = 0;
      user.set('tooltip_index', tooltip_index);
    }

    tip_toast.build(GC.app, 'tool_tip_head', {
      text_type: 'tool_tip_head',
      content: 'tooltip_' + tooltip_index,
      content_type: 'tool_tip_content',
      bg: 'toast/tip_bg.png',
      offsetY: 400
    });
  };

  this.onShuffleComplete = function () {
    shuffle_count = 0;
  };
});
