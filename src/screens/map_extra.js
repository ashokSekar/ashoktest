/* global _, Map, grid_settings, tile_settings, tiles_data, map_data,
 ThreeMatchScreen, config, image, puzzle_module, loading, event_manager,
 BonusPuzzleScreen, BonusMapPopup, popup, EventFinish, test */

/* jshint ignore:start */
import util.underscore as _;

import DevkitHelper.test as test;
import quest.screens.map as Map;
import quest.modules.image as image;
import resources.data.config as config;
import src.adventuremap.extra.grid as grid_settings;
import src.adventuremap.extra.tile as tile_settings;
import DevkitHelper.loading as loading;
import DevkitHelper.event_manager as event_manager;

import resources.data.extra_map_tiles as tiles_data;
import resources.data.extra_maps as map_data;
import quest.modules.image as image;
import quest.modules.puzzle as puzzle_module;
import src.screens.puzzles.three_match as ThreeMatchScreen;
import src.screens.puzzles.bonus as BonusPuzzleScreen;
import src.views.popups.bonus_map as BonusMapPopup;
import src.views.popups.event_finish as EventFinish;

/* jsio: if */
import src.modules.popup_manager as popup;
/* jsio: else */
import quest.modules.popup_manager as popup;
/* jsio: fi */
/* jshint ignore:end */

exports = Class(Map, function (supr) {
  'use strict';

  this.init = function () {
    var puzzle_screens = {
      default: new BonusPuzzleScreen()
    };

    supr(this, 'init', [{
      grid_settings: grid_settings,
      tile_settings: tile_settings,
      tiles_data: tiles_data,
      map_data: map_data
    }]);

    this.bonus_map = new BonusMapPopup();

    this.event_finish = new EventFinish();

    puzzle_screens[config.variations.three_match.id] = new ThreeMatchScreen();

    this.puzzle_screens = puzzle_screens;

    test.prepare(this, {
      setLastMs: function (ms) {
        map_data.last_ms_no = ms;
      }
    });

    _.bindAll(this, 'loadMap', 'renderPuzzle', 'showBonusMapPopup');
  };

  this.loadPuzzleScreen = function () {
    var curr_type = puzzle_module.getBonusMapType(),
      all_screens = this.puzzle_screens;

    this.puzzle_screen = all_screens[curr_type] || all_screens.default;
  };

  this.build = function () {
    this.loadPuzzleScreen();
    this.once('ViewDidAppear', this.showBonusMapPopup);

    supr(this, 'build', [image.get('map_screen/main_map')]);
  };

  this.showBonusMapPopup = function () {
    var variation_type = puzzle_module.getBonusMapId(),
      user = GC.app.user,
      launched_bonus_map = user.get('launched_bonus_map');

    if (!_.contains(launched_bonus_map, variation_type)) {
      launched_bonus_map.push(variation_type);
      user.set('launched_bonus_map', launched_bonus_map);
      this.bonus_map.build(puzzle_module.getBonusMapType());
    }
  };

  this.renderNodes = function () {
    var model = this.map.getModel(),
      nodes = model.getNodesByTag('milestone'),
      user_puzzle = puzzle_module.getUserBonusPuzzle(),
      max_ms = user_puzzle.max_ms,
      ms_id, start_count;

    _.each(nodes, function (node) {
      ms_id = parseInt(node.id, 10);

      if (ms_id <= max_ms) {
        start_count = user_puzzle[ms_id] ? user_puzzle[ms_id].stars : 0;
        node.node = 2 + start_count;
        model.removeTagById(ms_id, 'locked');
      } else {
        node.node = 1;
        model.addTagById(ms_id, 'locked');
      }
    });
  };

  this.loadMapIcon = function () {
    this.map_icon.show();
  };

  this.loadMap = function () {
    supr(this, 'loadMap', [false]);
  };

  this.rateUsAvailable = function () {
    return false;
  };

  this.canOvertake = function () {
    return false;
  };

  this.facebookAvailable = function () {
    return false;
  };

  this.renderPuzzle = function (data) {
    loading.show('puzzle', bind(this, function () {
      if (_.isNumber(data)) {
        puzzle_module.loadBonusMapPuzzle(data)
          .then(this.loadPuzzle);
      } else {
        this.loadPuzzle(data);
      }
    }));
  };

  this.focusMilestone = function (ms, start) {
    var map = this.map,
      ms_last = map_data.last_ms_no,
      model = map.getModel(),
      user_puzzle = puzzle_module.getUserBonusPuzzle(),
      ms_focus = Math.min(ms_last, ms);

    ms = ms || user_puzzle.curr_ms;
    user_puzzle.curr_ms = ms;
    user_puzzle.played = true;

    puzzle_module.setUserBonusPuzzle(user_puzzle);

    model.removeTag('Player');
    model.addTagById(ms, 'Player');
    model.update();
    map.focusNodeById(ms);

    if (start && ms_focus === ms) {
      event_manager.emit('milestone-selected', {
        milestone: ms
      });
      this.startMilestone(ms, false, false);
    }
  };

  this.showEventFinish = function (ms_current, next) {
    var last_ms = map_data.last_ms_no,
      is_last_level = last_ms < ms_current || !next && ms_current === last_ms;

    if (GC.app.user.get('last_played_won') && is_last_level) {
      loading.once('hide', bind(this, function () {
        this.event_finish.build();
        popup.on('event-finish:ok', this.loadMap);
        popup.on('event-finish:close', function () {
          popup.removeAllListeners('event-finish:ok');
          popup.removeAllListeners('event-finish:close');
        });
      }));
      return true;
    }
    return false;
  };

  this.renderCrossPromoBtn = function () {
    this.cross_promo_btn.hide();
  };

  this.showPopup = function () {
  };

  this.on3DTouch = function () {
  };

  this.getPuzzleDataToLoad = function (ms, bonus, extra) {
    var puzzle;

    return puzzle_module.loadBonusMapPuzzle(ms, false, extra)
      .then(function (data) {
        puzzle = puzzle_module.getUserBonusPuzzle()[ms];
        data.stars = puzzle ? puzzle.stars : 0;

        return data;
      });
  };

  this.showPlayPopup = function (ms, cb, extra) {
    supr(this, 'showPlayPopup', [ms, cb, extra, true]);
  };

  this.puzzleComplete = function (next) {
    var user_puzzle = puzzle_module.getUserBonusPuzzle(),
      ms_current = user_puzzle.curr_ms,
      focus_ms = this.canFocusMs();

    if (next) {
      ++ms_current;
      focus_ms = focus_ms || this.mapEventAvailable(ms_current);
    }

    this.refresh();
    this.startSound();
    if (!this.showEventFinish(ms_current, next) && _.isBoolean(next)) {
      loading.once('hide', bind(this, function () {
        this.focusMilestone(ms_current, focus_ms, false);
        if (!focus_ms) {
          puzzle_module.load(ms_current, this.renderPuzzle);
        }
      }));
    }
  };
});
