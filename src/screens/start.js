/* global i18n, popup, Callback, popup, BonusMap, loading */

/* jshint ignore:start */
import event.Callback as Callback;
import util.underscore as _;

import DevkitHelper.loading as loading;
import DevkitHelper.i18n as i18n;

import quest.modules.popup_manager as popup;
import src.screens.map_extra as BonusMap;

/* jshint ignore:end */

exports = function (supr) {
  'use strict';

  this.init = function (opts) {
    supr(this, 'init', [opts]);

    _.bindAll(this, 'loadMap');
  };

  this.build = function () {
    supr(this, 'build', [{}]);

    this.play_btn.setTitle(i18n('start_screen_play'));

    GC.app.on('load-map', this.loadMap);
  };

  this.on3DTouch = function (tag) {
    var callback = new Callback();

    callback.run(bind(this, this.showMap));
    GC.app.set3DTouchPending(tag);

    if (popup.getStack().length > 0) {
      popup.closeAll(callback);
    } else {
      callback.fire();
    }
  };

  this.getBonusMap = function () {
    var map = this.bonus_map;

    if (!map) {
      this.bonus_map = map = new BonusMap();
    }

    return map;
  };

  this.loadMap = function (bonus) {
    var map;

    if (bonus === true) {
      GC.app.user.set('screen_bonus', true);
      map = this.getBonusMap();
      loading.show('map_1', bind(this, function () {
        map.build();
        GC.app.push(map, true);
      }));
    } else {
      GC.app.user.set('screen_bonus', false);
      this.showMap();
    }
  };
};
