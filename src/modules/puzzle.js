/* global _, timer, config, test, storage, Promise,
  EventsPopup, image */

/* jshint ignore:start */
import util.underscore as _;

import DevkitHelper.timer as timer;
import DevkitHelper.test as test;
import quest.lib.bluebird as Promise;
import DevkitHelper.storage as storage;
import resources.data.config as config;
import src.views.popups.events as EventsPopup;
import quest.modules.image as image;
/* jshint ignore:end */

exports = function (supr) {
  'use strict';

  var bonus_map_data = null,
    duration = 0,
    pause_time = 0,
    timer_id = 'bonus_map_timer',
    event_popup_show = false,
    popup_bg = null,
    resetBonusMap = function () {
      popup_bg = null;
      duration = 0;
      pause_time = 0;
      timer.unregister(timer_id);
    },
    loadPopupBg = function (data) {
      var url = data.popup_bg ||
        config.img_url + '/event_popup_' + data.type + '.png';

      image.getAsync(url, 'anonymous')
        .then(function (img) {
          popup_bg = img[0];
        });
    };

  this.updateBonusData = function (id, data) {
    supr(this, 'updateBonusData', [id, data, {
      limit_type: 'tap',
      type: 'memory'
    }]);
  };

  this.setBonusMapData = function (data) {
    var curr_data = bonus_map_data || {},
      key = config.key_bonus_map,
      bonus_data = storage.get(key) || {},
      puzzle_types = _.pluck(config.variations, 'id'),
      user_puzzle;

    // Avoiding setting bonus map data in the puzzle
    if (GC.app.user.get('screen_bonus')) {
      return;
    }
    if (data && _.contains(puzzle_types, data.type) && data.time_left > 0 &&
       !_.isEqual(_.omit(data, 'time_left'), _.omit(curr_data, 'time_left'))) {
      bonus_map_data = data;
      user_puzzle = bonus_data[data.id] || {};
      this.updateBonusMapInterval(data.time_left);
      loadPopupBg(bonus_map_data);
      if (!user_puzzle.max_ms) {
        user_puzzle.max_ms = 1;
        user_puzzle.curr_ms = 1;

        bonus_data[data.id] = user_puzzle;
        storage.set(key, bonus_data);
      }
    } else if (!data) {
      resetBonusMap();
    }
    test.prepare(this, {
      bonus_map_data: bonus_map_data,
      duration: duration,
      getPopupBg: function () {
        return popup_bg;
      },
      loadPopupBg: loadPopupBg
    });
  };

  this.getUserBonusPuzzle = function () {
    return storage.get(config.key_bonus_map)[bonus_map_data.id];
  };

  this.getBonusMapId = function () {
    return bonus_map_data.id;
  };

  this.getBonusMapType = function () {
    return bonus_map_data.type;
  };

  this.setUserBonusPuzzle = function (user_puzzle) {
    var key = config.key_bonus_map,
      data = storage.get(key);

    data[bonus_map_data.id] = user_puzzle;
    storage.set(key, data);
  };

  this.updateBonusMapInterval = function (seconds) {
    if (!timer.has(timer_id)) {
      timer.register(timer_id, bind(this, function () {
        var diff = Math.floor((Date.now() - pause_time) / 1000);

        if (pause_time > 0 && diff > 1) {
          duration = duration - diff;
          pause_time = Date.now();
        } else {
          --duration;
        }

        if (duration <= 1) {
          resetBonusMap();
        }
        this.emit('bonus-map-time', duration);
      }), 1000);
    }

    pause_time = Date.now();
    duration = seconds;
  };

  this.loadBonusMapPuzzle = function (ms_no) {
    /* istanbul ignore next */
    var levels = bonus_map_data ? bonus_map_data.levels : [],
      id = levels[ms_no - 1],
      reduced_family = 2 / 3,
      custom_data = {};

    if (_.isArray(id)) {
      reduced_family = id[1];
      custom_data = _.isObject(id[2]) ? id[2] : custom_data;
      custom_data = _.pick(custom_data, 'time', 'tap', 'lock_keys');
      _.each(custom_data, function (curr, key) {
        if (_.isObject(curr)) {
          custom_data[key] = JSON.parse(JSON.stringify(curr));
        }
      });
      id = id[0];
    }

    return this.load(id, false, 'layout')
      .then(function (data) {
        data.type = bonus_map_data.type;
        data.number = ms_no;
        data.reduced_family = reduced_family;
        data.bonus_map = bonus_map_data.id;
        if (data.type === 'golden') {
          data.goal = {
            golden: 1
          };
        }

        data = merge(custom_data, data);

        return Promise.resolve(data);
      })
      .catch(function () {
        return Promise.reject('No Puzzle');
      });
  };

  this.saveHighScore = function (data) {
    if (!data.bonus_map) {
      supr(this, 'saveHighScore', [data]);
    }
  };

  this.showEventPopup = function () {
    var popup;

    if (bonus_map_data && !this.getUserBonusPuzzle().played &&
      popup_bg && !event_popup_show) {
      popup = new EventsPopup();
      popup.build(bonus_map_data, popup_bg);
      event_popup_show = true;
      return true;
    }

    return false;
  };
};
