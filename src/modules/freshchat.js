/* global freshchat, util*/

/* jshint ignore:start */
import freshchat;
import quest.modules.util as util;
/* jshint ignore:end */

exports = function (supr) {
  'use strict';

  // Tracking Custom Data
  this.showSupport = function (type) {
    var user = GC.app.user,
      str = JSON.stringify(user.get('puzzle_loads'));

    freshchat.addMetaData('puzzle_loads', str);
    freshchat.addMetaData('appversion', util.getDeviceInfo('appversion'));
    freshchat.addMetaData('os', util.getDeviceInfo('os'));
    freshchat.addMetaData('device', util.getDeviceInfo('name'));

    supr(this, 'showSupport', [type]);
  };
};
