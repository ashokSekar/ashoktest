/* global firebase, config, _ */

/* jshint ignore:start */
import firebase as firebase;
import resources.data.config as config;
import util.underscore as _;
/* jshint ignore:end */

exports = function (supr) {
  'use strict';

  this.tutorialAction = function (data) {
    if (GC.app.user.getMs('max') === 1) {
      if (_.contains(config.tutorials_firebase, data.data.tut_id)) {
        data.id = data.data.tut_id;
      }
    }
    supr(this, 'tutorialAction', [data]);
  };

  this.firstTap = function () {
    firebase.logEvent('first_tap');
  };

  this.firstPairMatch = function () {
    firebase.logEvent('first_pair_match');
  };

  this.outOfLives = function () {
    firebase.logEvent('out_of_lives');
  };
};
