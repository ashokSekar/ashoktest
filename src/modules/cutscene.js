/* global config, test, event_manager, string */

/* jshint ignore:start */
import util.underscore as _;

import resources.data.config as config;
import quest.utils.string as string;
import DevkitHelper.test as test;
import DevkitHelper.event_manager as event_manager;
/* jshint ignore:end */

exports = (function () {
  'use strict';

  var obj = {},
    data = {
      map: {}
    },
    showTutorial = function (id, callback) {
      return function (ms, cb) {
        var tutorial = GC.app.tutorial,
          opts = {
            on_cancel: function (hcb) {
              // hcb is history callback
              tutorial.clean();
              if (hcb && hcb.fire) {
                hcb.fire();
              }

              // cb for chaining
              if (cb) {
                cb(true);
              }
            }
          },
          shown = tutorial.add(id, true, cb, opts);

        // this is to emit event, line 55
        if (shown && callback) {
          callback.apply(this);
        }

        // cb for chaning
        if (!shown && cb) {
          cb();
        }
      };
    };

  _.each(config.unlockables, function (ms, unlockable) {
    var id = 'unlock_' + unlockable,
      func = string.toCamel(id);

    obj[func] = showTutorial(id, function () {
      event_manager.emit('powerup-unlocked', id);
    });
    data.map[ms] = {
      id: id,
      fn: func,
      events: ['start-ms']
    };
  });

  test.prepare(obj, {
    showTutorial: showTutorial,
    data: data
  });

  obj.register = function (model, type, ms_no) {
    var ms = ms_no || GC.app.user.get('curr_ms'),
      current = data[type][ms];

    if (obj.isCompleted(type, ms)) {
      return;
    }

    _.each(current.events, function (evnt) {
      model.chain(evnt, bind(model, obj[current.fn]));
    });
  };

  obj.isCompleted = function (type, ms_no) {
    var app = GC.app,
      user = app.user,
      ms = ms_no || user.get('curr_ms'),
      current = data[type][ms];

    return !current ||
      app.tutorial.isCompleted(current.id, {
        milestone: ms,
        type: type
      });
  };

  return obj;
})();
