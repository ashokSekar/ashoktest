/* global _, FB, config
*/
/* jshint ignore:start */
import util.underscore as _;
import facebook as FB;
import resources.data.config as config;
/* jshint ignore:end */

exports = function (supr) {
  'use strict';

  this.firstTap = function () {
    FB.AppEvents.logEvent('first_tap');
  };

  this.firstPairMatch = function () {
    FB.AppEvents.logEvent('first_pair_match');
  };

  this.tutorialAction = function (data) {
    if (GC.app.user.getMs('max') === 1 &&
      _.contains(config.tutorials_facebook, data.data.tut_id)) {
      data.id = data.data.tut_id;
      FB.AppEvents.logEvent('tutorial_' + data.id);
    } else {
      supr(this, 'tutorialAction', [data]);
    }
  };

  this.levelFirstAfter = function (data) {
    FB.AppEvents.logEvent('level_first_' + data);
  };
};
