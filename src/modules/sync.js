/* global puzzle_module */

/* jshint ignore:start */
import quest.modules.puzzle as puzzle_module;
/* jshint ignore:end */

exports = function (supr) {
  'use strict';

  this.onSync = function (callback, error, response) {
    if (response && !error) {
      puzzle_module.setBonusMapData(response.bonus_map);
    }

    supr(this, 'onSync', [callback, error, response]);
  };
};
