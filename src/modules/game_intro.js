/* global tutorial_view, test, config, tutorial_data, _, util */

/* jshint ignore:start */
import quest.modules.util as util;
import src.views.tutorial as tutorial_view;
import resources.data.config as config;
import DevkitHelper.test as test;
import resources.data.tutorial as tutorial_data;
import util.underscore as _;
/* jshint ignore:end */

exports = (function () {
  'use strict';

  var obj = {},
    puzzle;

  obj.register = function (puzzle_data) {
    var old_ms, curr_ms, type,
      powerup, powerup_info;

    puzzle = puzzle_data;
    type = puzzle.model.get('type');
    if (obj.isCompleted()) {
      old_ms = obj.getOldTutorialMs(type);
      if (!old_ms) {
        powerup_info = GC.app.user.getPowerupForInfo('ingame');
        if (powerup_info) {
          old_ms = 'powerup_info';
        } else {
          return false;
        }
      }
    }
    curr_ms = old_ms || obj.getTutorialKey();
    powerup = _.findKey(config.unlockables, function (ms) {
      return ms === curr_ms;
    });
    if (powerup && !obj.powerupCheck(powerup, type)) {
      return false;
    }
    obj.build(curr_ms);
    return true;
  };

  obj.getUnlockables = function (type) {
    return _.omit(config.unlockables,
      config.variations[type].powerups_exclude);
  };

  obj.powerupCheck = function (curr_powerup, type) {
    var pregame_powerups = _.keys(util.getPowerups('pregame')),
      unlocked_powerups = GC.app.user.get('unlocked_powerups');

    if (_.contains(unlocked_powerups, curr_powerup)) {
      return false;
    } else if (_.contains(pregame_powerups, curr_powerup) &&
      !puzzle.model.get(curr_powerup)) {
      return false;
    } else if (_.contains(config.variations[type].powerups_exclude,
        curr_powerup)) {
      return false;
    }

    return true;
  };

  obj.build = function (ms) {
    var app = GC.app,
      grid = puzzle.grid,
      powerups = puzzle.hud.powerups,
      model = puzzle.model,
      user = GC.app.user,
      powerup_info = user.get('last_powerup_info')[1],
      powerup = _.findKey(config.unlockables, function (ms_no) {
        return ms_no === ms;
      }),
      powerups_dynamic = {
        joker: 2,
        swap: 2,
        open_slot: 1
      },
      getContext = function (x, y, z, model) {
        var tiles = grid.tiles,
          ctx = null;

        if (tiles[x] && tiles[x][y] && tiles[x][y][z]) {
          ctx = tiles[x][y][z];
        }

        return model && ctx ? ctx.model : ctx;
      },
      contexts = puzzle.getContextForTutorial(ms),
      common_opts = {
        before: function () {
          model.pauseTimers();
          grid.setHandleEvents(false, true);
        },
        after: function () {
          model.resumeTimers();
          grid.setHandleEvents(true, false);
          if (powerup) {
            user.push('unlocked_powerups', powerup);
          }
        }
      },
      beforeTut = function (powerup) {
        return function () {
          var user = app.user;

          common_opts.before();
          if (user.get('inventory_' + powerup) === 0) {
            user.addFreeGoods(powerup, config.award_powerup_count[powerup],
              'first_time_bonus', model.get('instance_id'));
            powerups['btn_' + powerup].setCount();
          }
        };
      };

    if (powerups_dynamic[powerup] &&
      contexts.length < powerups_dynamic[powerup]) {
      return false;
    }

    app.tutorial.build({
      superview: puzzle,
      type: 'game_intro',
      milestone: ms,
      view: [tutorial_view],
      on_resume: function () {
        model.pauseTimers();
        grid.setHandleEvents(false, true);
      },
      positions: {
        welcome_to_mahjong: merge({
          x: 0,
          y: 0,
          view: {
            index: 0,
            timeout: 100
          }
        }, common_opts),
        match_pair: merge({
          context: getContext(7, 15, 0),
          view: {
            index: 0,
            timeout: 500,
            params: {
              overlay: 'match_pair'
            }
          }
        }, common_opts),
        match_only_free: merge({
          context: getContext(5, 16, 0),
          view: {
            index: 0,
            timeout: 100,
            params: {
              overlay: 'match_only_free'
            }
          }
        }, common_opts),
        non_free_unmatch: merge({
          context: getContext(5, 18, 1),
          view: {
            index: 0,
            timeout: 100,
            params: {
              overlay: 'non_free_unmatch'
            }
          }
        }, common_opts),
        timer_intro: merge({
          context: puzzle.hud.goals_hud.left_view,
          view: {
            index: 0,
            timeout: 100,
            params: {
              overlay: 'timer'
            }
          }
        }, common_opts),
        goal_intro: merge({
          context: puzzle.hud.goals_hud.right_view,
          view: {
            index: 0,
            timeout: 100,
            params: {
              overlay: 'goal'
            }
          }
        }, common_opts),
        shuffle_intro: merge({
          context: powerups.btn_shuffle,
          view: {
            index: 0,
            timeout: 100,
            params: {
              overlay: 'powerup',
              offsetY: -100
            }
          }
        }, common_opts),
        hint_intro: merge({
          context: powerups.btn_hint,
          view: {
            index: 0,
            timeout: 100,
            params: {
              overlay: 'powerup'
            }
          },
          action: {
            context: powerups.btn_hint
          },
          before: beforeTut('hint')
        }, common_opts),
        half_hint_intro: merge({
          context: powerups.btn_half_hint,
          view: {
            index: 0,
            timeout: 100,
            params: {
              overlay: 'powerup'
            }
          },
          action: {
            context: powerups.btn_half_hint
          },
          before: beforeTut('half_hint')
        }, common_opts),
        flip_intro: merge({
          context: powerups.btn_flip,
          view: {
            index: 0,
            timeout: 100,
            params: {
              overlay: 'powerup'
            }
          },
          action: {
            context: powerups.btn_flip
          },
          before: beforeTut('flip')
        }, common_opts),
        flip_uncover: merge({
          context: grid,
          view: {
            index: 0,
            timeout: 100
          },
          before: beforeTut('flip')
        }, common_opts),
        swoosh_intro: merge({
          context: powerups.btn_swoosh,
          view: {
            index: 0,
            timeout: 100,
            params: {
              overlay: 'powerup'
            }
          },
          action: {
            context: powerups.btn_swoosh
          },
          before: beforeTut('swoosh')
        }, common_opts),
        substitute_intro: merge({
          context: powerups.btn_substitute,
          view: {
            index: 0,
            timeout: 100,
            params: {
              overlay: 'powerup'
            }
          },
          action: {
            context: powerups.btn_substitute
          },
          before: beforeTut('substitute')
        }, common_opts),
        open_slot_drag: merge({
          context: contexts[0],
          custom: {
            drag_target: puzzle.grid.open_slot
          },
          view: {
            index: 0,
            timeout: 100,
            params: {
              overlay: 'tile'
            }
          },
          action: {
            evt: 'InputDrag',
            context: contexts[0],
            func: 'updateOpenSlot'
          }
        }, common_opts),
        open_slot_intro: merge({
          context: puzzle.grid.open_slot,
          view: {
            index: 0,
            timeout: 100,
            params: {
              overlay: 'tile'
            }
          }
        }, common_opts),
        open_slot_after: merge({
          context: puzzle.grid.open_slot,
          view: {
            index: 0,
            timeout: 200,
            params: {
              overlay: 'tile'
            }
          }
        }, common_opts),
        swap_intro: merge({
          context: powerups.btn_swap,
          view: {
            index: 0,
            timeout: 100,
            params: {
              overlay: 'powerup'
            }
          },
          action: {
            context: powerups.btn_swap
          },
          before: beforeTut('swap')
        }, common_opts),
        swap_select_tile_one: merge({
          context: contexts[0],
          view: {
            index: 0,
            timeout: 100,
            params: {
              overlay: 'tile'
            }
          },
          action: {
            context: contexts[0] ? contexts[0].model : null,
            func: 'pick'
          }
        }, common_opts),

        // keeping the same name to avoid repeatation for old users
        swap_select_tiles: merge({
          context: contexts[1],
          view: {
            index: 0,
            timeout: 100,
            params: {
              overlay: 'tile'
            }
          },
          action: {
            context: contexts[1] ? contexts[1].model : null,
            func: 'pick'
          },
          before: function () {
            model.emit('before-tutorial', 'swap_select_tiles',
              contexts[1]);
            common_opts.before();
          },
          after: function () {
            grid.setHandleEvents(true, false);
            model.emit('after-tutorial', 'swap_select_tiles',
              contexts[1]);
          }
        }, common_opts),
        auto_hint_intro: merge({
          x: 0,
          y: 0,
          view: {
            index: 0,
            timeout: 100
          }
        }, common_opts),
        joker_intro: merge({
          context: contexts[0],
          view: {
            index: 0,
            timeout: 100,
            params: {
              overlay: 'tile'
            }
          },
          action: {
            context: contexts[0] ? contexts[0].model : null,
            func: 'pick'
          },
          before: function () {
            model.emit('before-tutorial', 'joker_intro',
              contexts[0]);
            common_opts.before();
          }
        }, common_opts),
        joker_match: merge({
          context: contexts[1],
          view: {
            index: 0,
            timeout: 100,
            params: {
              overlay: 'tile'
            }
          },
          action: {
            context: contexts[1] ? contexts[1].model : null,
            func: 'pick'
          }
        }, common_opts),
        memory_match_intro: merge({
          context: grid,
          view: {
            index: 0,
            timeout: 100
          }
        }, common_opts),
        memory_match_time: merge({
          context: puzzle.hud.goals_hud.left_view,
          view: {
            index: 0,
            timeout: 100,
            params: {
              overlay: 'timer'
            }
          }
        }, common_opts),
        memory_match_mismatch: merge({
          context: puzzle.hud.goals_hud.left_view,
          view: {
            index: 0,
            timeout: 100
          }
        }, common_opts),
        powerup_info: merge({
          context: powerups['btn_' + powerup_info],
          view: {
            index: 0,
            timeout: 100,
            params: {
              overlay: 'powerup'
            }
          }
        }, common_opts)
      }
    });

    test.prepare(obj, {
      common: common_opts,
      getContext: getContext
    });
  };

  obj.isBonus = function () {
    return puzzle.model.get('bonus_level') > 0;
  };

  obj.getTutorialKey = function () {
    var last_tut_id,
      type = puzzle.model.get('type'),
      user = GC.app.user,
      curr_ms = user.getMs('curr'),
      max_ms = user.getMs('max'),
      old_ms = obj.getOldTutorialMs(type, max_ms);

    if (obj.isBonus()) {
      /* Checking if bonus tutorial is completed,if it is
         then return return max_ms
      */
      last_tut_id = obj.getLastTutorialData(type);
      if (last_tut_id && !GC.app.tutorial.isCompleted(last_tut_id, {
        type: 'game_intro',
        milestone: type
      })) {
        return type;
      } else if (old_ms) {
        return old_ms;
      } else {
        return max_ms;
      }
    }
    return curr_ms;
  };

  obj.getLastTutorialData = function (ms) {
    var game_intro = tutorial_data.game_intro;

    if (game_intro[ms]) {
      return _.last(game_intro[ms]).id;
    } else {
      return false;
    }
  };

  obj.isCompleted = function () {
    var app = GC.app,
      curr_ms = obj.getTutorialKey(),
      last_tut_id = obj.getLastTutorialData(curr_ms);

    return !last_tut_id || app.tutorial.isCompleted(last_tut_id, {
        type: 'game_intro',
        milestone: curr_ms
      });
  };

  // TODO: move this function to util as repeating in play popup
  obj.getOldTutorialMs = function (type, ms) {
    var game_intro = tutorial_data.game_intro,
      curr_ms = ms || obj.getTutorialKey(),
      unlockables = obj.getUnlockables(type);

    return _.find(unlockables, function (ms, powerup) {
      if (ms < curr_ms &&
        !_.contains(GC.app.user.get('unlocked_powerups'), powerup) &&
        game_intro[ms]) {
        return true;
      }
    });
  };

  return obj;
})();
