/* global Application, event_manager, Tutorial, tutorial_data, config, _,
  i18n, error_handler
*/

/* jshint ignore:start */
import util.underscore as _;

import resources.data.config as config;

import quest.Application as Application;

import DevkitHelper.error_handler as error_handler;
import DevkitHelper.event_manager as event_manager;
import DevkitHelper.tutorial as Tutorial;
import DevkitHelper.i18n as i18n;

import resources.data.tutorial as tutorial_data;
/* jshint ignore:end */

exports = Class(Application, function (supr) {
  'use strict';

  var shortcut_pending = null;

  this.initUI = function () {
    event_manager.register('quest.modules', ['flurry', 'firebase', 'facebook',
      'freshchat', 'kochava', 'onesignal', 'achievements', 'crashlytics']);

    this.tutorial = new Tutorial({
      data: tutorial_data
    });

    _.bindAll(this, 'update3DTouch', 'check3DTouch');

    error_handler.register({
      url: config.server + '/error'
    });

    supr(this, 'initUI');

    this.on('StackChanged', this.check3DTouch);
  };

  this.startGame = function () {
    var user = GC.app.user;

    user.on('change:max_ms', this.update3DTouch);
    user.on('change:social_facebook', this.update3DTouch);
    user.set('screen_bonus', false);
    return supr(this, 'startGame');
  };

  this.update3DTouch = function () {
    var data = config.touch3d,
      user = this.user,
      max_ms = user.getMs('max'),
      fb_connected = user.get('social_facebook'),
      res = {};

    if (max_ms > 1) {
      _.each(data, function (curr, tag) {
        if (!curr.facebook || curr.facebook && fb_connected) {
          res[tag] = {
            title: i18n('shortcut_' + tag, [max_ms]),
            icon: curr.icon
          };
        }
      });

      supr(this, 'update3DTouch', [res]);
    }
  };

  this.set3DTouchPending = function (tag) {
    shortcut_pending = tag;
  };

  this.check3DTouch = function () {
    var tag = shortcut_pending;

    if (tag) {
      shortcut_pending = null;
      this.on3DTouch(tag, false);
    }
  };
});
