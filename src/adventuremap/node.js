/* global PlayerView, CharacterSettings, AdView, Viewpool, FriendsView,
  BonusLevelView, GiftingIcon
*/

/* jshint ignore:start */
import ui.ViewPool as Viewpool;

/* jsio: if */
import src.views.player as PlayerView;
/* jsio: else */
import quest.views.player as PlayerView;
/* jsio: fi */

/* jsio: if */
import src.views.friend as FriendsView;
/* jsio: else */
import quest.views.friend as FriendsView;
/* jsio: fi */

/* jsio: if */
import src.views.bonus_level as BonusLevelView;
/* jsio: else */
import quest.views.bonus_level as BonusLevelView;
/* jsio: fi */

/* jsio: if */
import src.views.video_tag as AdView;
/* jsio: else */
import quest.views.video_tag as AdView;
/* jsio: fi */

/* jsio: if */
import src.views.gifting_icon as GiftingIcon;
/* jsio: else */
import quest.views.gifting_icon as GiftingIcon;
/* jsio: fi */

/* jsio: if */
import src.adventuremap.character as CharacterSettings;
/* jsio: else */
import quest.adventuremap.character as CharacterSettings;
/* jsio: fi */
/* jshint ignore: end */

var charsettings = {
  height: 25,
  width: 30,
  x: 10,
  y: 45,
  data: CharacterSettings.numbers
};

exports = {
  nodes: [
    {
      image: 'resources/images/map_screen/mslock.png',
      width: 105,
      height: 119,
      characterSettings: charsettings
    },
    {
      image: 'resources/images/map_screen/msicon.png',
      width: 105,
      height: 119,
      characterSettings: charsettings
    },
    {
      image: 'resources/images/map_screen/ms1star.png',
      width: 105,
      height: 119,
      characterSettings: charsettings
    },
    {
      image: 'resources/images/map_screen/ms2star.png',
      width: 105,
      height: 119,
      characterSettings: charsettings
    },
    {
      image: 'resources/images/map_screen/ms3star.png',
      width: 105,
      height: 119,
      characterSettings: charsettings
    }
  ],
  itemCtors: {
    Player: PlayerView,
    AdButton: AdView,
    GiftingLevel: GiftingIcon,
    Friends: new Viewpool({
      ctor: FriendsView,
      initCount: 10
    }),
    BonusLevel: new Viewpool({
      ctor: BonusLevelView,
      initCount: 10
    })
  }
};
