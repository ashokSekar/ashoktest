exports = {
  width: 10,
  height: 40,
  tags: ['milestone', 'locked'],
  rotate: ['left', 'reset', 'right']
};
