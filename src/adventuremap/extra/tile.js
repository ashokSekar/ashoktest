/* jshint ignore:start */
import util.underscore as _;
/* jshint ignore:end */

var maps = [
    {
      range: [
        0,
        39
      ],
      length: 20,
      folder: 'map6'
    }
  ],
  getMapData = function (pos) {
    'use strict';

    return _.find(maps, function (val) {
      return pos >= val.range[0] && pos <= val.range[1];
    });
  };

exports = {
  doodads: [],
  tileWidth: 64,
  tileHeight: 100,
  defaultTile: 0,
  friendsX: 0,
  friendsY: 0,
  directory: function (tileX, tileY) {
    'use strict';

    var data = getMapData(tileY),
      y = tileY - data.range[0];

    /* istanbul ignore else */
    if (data.length) {
      y %= data.length;
    }

    return 'resources/images/tiles/' +
      data.folder + '/' + y + '_' + tileX + '.png';
  }
};
