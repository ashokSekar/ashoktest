/* jshint ignore:start */
import util.underscore as _;
/* jshint ignore:end */

var maps = [
  {
    range: [
      0,
      4
    ],
    folder: 'comingsoon'
  },
  {
    range: [
      5,
      44
    ],
    folder: 'map7',
    length: 20
  },
  {
    range: [
      45,
      47
    ],
    folder: 'bridge6'
  },
  {
    range: [
      48,
      87
    ],
    folder: 'map6',
    length: 20
  },
  {
    range: [
      88,
      90
    ],
    folder: 'bridge5'
  },
  {
    range: [
      91,
      130
    ],
    folder: 'map5',
    length: 20
  },
  {
    range: [
      131,
      133
    ],
    folder: 'bridge4'
  },
  {
    range: [
      134,
      173
    ],
    folder: 'map4',
    length: 20
  },
  {
    range: [
      174,
      176
    ],
    folder: 'bridge3'
  },
  {
    range: [
      177,
      216
    ],
    folder: 'map3',
    length: 20
  },
  {
    range: [
      217,
      219
    ],
    folder: 'bridge2'
  },
  {
    range: [
      220,
      259
    ],
    folder: 'map2',
    length: 20
  },
  {
    range: [
      260,
      262
    ],
    folder: 'bridge1'
  },
  {
    range: [
      263,
      302
    ],
    folder: 'map1',
    length: 20
  },
  {
    range: [
      303,
      305
    ],
    folder: 'bridge9'
  },
  {
    range: [
      306,
      345
    ],
    folder: 'map9',
    length: 20
  },
  {
    range: [
      346,
      348
    ],
    folder: 'bridge8'
  },
  {
    range: [
      349,
      388
    ],
    folder: 'map8',
    length: 20
  },
  {
    range: [
      389,
      391
    ],
    folder: 'bridge7'
  },
  {
    range: [
      392,
      431
    ],
    folder: 'map7',
    length: 20
  },
  {
    range: [
      432,
      434
    ],
    folder: 'bridge6'
  },
  {
    range: [
      435,
      474
    ],
    folder: 'map6',
    length: 20
  },
  {
    range: [
      475,
      477
    ],
    folder: 'bridge5'
  },
  {
    range: [
      478,
      517
    ],
    folder: 'map5',
    length: 20
  },
  {
    range: [
      518,
      520
    ],
    folder: 'bridge4'
  },
  {
    range: [
      521,
      560
    ],
    folder: 'map4',
    length: 20
  },
  {
    range: [
      561,
      563
    ],
    folder: 'bridge3'
  },
  {
    range: [
      564,
      603
    ],
    folder: 'map3',
    length: 20
  },
  {
    range: [
      604,
      606
    ],
    folder: 'bridge2'
  },
  {
    range: [
      607,
      646
    ],
    folder: 'map2',
    length: 20
  },
  {
    range: [
      647,
      649
    ],
    folder: 'bridge1'
  },
  {
    range: [
      650,
      689
    ],
    folder: 'map1',
    length: 20
  },
  {
    range: [
      690,
      692
    ],
    folder: 'bridge9'
  },
  {
    range: [
      693,
      732
    ],
    folder: 'map9',
    length: 20
  },
  {
    range: [
      733,
      735
    ],
    folder: 'bridge8'
  },
  {
    range: [
      736,
      775
    ],
    folder: 'map8',
    length: 20
  },
  {
    range: [
      776,
      778
    ],
    folder: 'bridge7'
  },
  {
    range: [
      779,
      818
    ],
    folder: 'map7',
    length: 20
  },
  {
    range: [
      819,
      821
    ],
    folder: 'bridge6'
  },
  {
    range: [
      822,
      861
    ],
    folder: 'map6',
    length: 20
  },
  {
    range: [
      862,
      864
    ],
    folder: 'bridge5'
  },
  {
    range: [
      865,
      904
    ],
    folder: 'map5',
    length: 20
  },
  {
    range: [
      905,
      907
    ],
    folder: 'bridge4'
  },
  {
    range: [
      908,
      947
    ],
    folder: 'map4',
    length: 20
  },
  {
    range: [
      948,
      950
    ],
    folder: 'bridge3'
  },
  {
    range: [
      951,
      990
    ],
    folder: 'map3',
    length: 20
  },
  {
    range: [
      991,
      993
    ],
    folder: 'bridge2'
  },
  {
    range: [
      994,
      1033
    ],
    folder: 'map2',
    length: 20
  },
  {
    range: [
      1034,
      1036
    ],
    folder: 'bridge1'
  },
  {
    range: [
      1037,
      1076
    ],
    folder: 'map1',
    length: 20
  },
  {
    range: [
      1077,
      1079
    ],
    folder: 'bridge9'
  },
  {
    range: [
      1080,
      1119
    ],
    folder: 'map9',
    length: 20
  },
  {
    range: [
      1120,
      1122
    ],
    folder: 'bridge8'
  },
  {
    range: [
      1123,
      1162
    ],
    folder: 'map8',
    length: 20
  },
  {
    range: [
      1163,
      1165
    ],
    folder: 'bridge7'
  },
  {
    range: [
      1166,
      1205
    ],
    folder: 'map7',
    length: 20
  },
  {
    range: [
      1206,
      1208
    ],
    folder: 'bridge6'
  },
  {
    range: [
      1209,
      1248
    ],
    folder: 'map6',
    length: 20
  },
  {
    range: [
      1249,
      1251
    ],
    folder: 'bridge5'
  },
  {
    range: [
      1252,
      1291
    ],
    folder: 'map5',
    length: 20
  },
  {
    range: [
      1292,
      1294
    ],
    folder: 'bridge4'
  },
  {
    range: [
      1295,
      1334
    ],
    folder: 'map4',
    length: 20
  },
  {
    range: [
      1335,
      1337
    ],
    folder: 'bridge3'
  },
  {
    range: [
      1338,
      1377
    ],
    folder: 'map3',
    length: 20
  },
  {
    range: [
      1378,
      1380
    ],
    folder: 'bridge2'
  },
  {
    range: [
      1381,
      1420
    ],
    folder: 'map2',
    length: 20
  },
  {
    range: [
      1421,
      1423
    ],
    folder: 'bridge1'
  },
  {
    range: [
      1424,
      1463
    ],
    folder: 'map1',
    length: 20
  },
  {
    range: [
      1464,
      1466
    ],
    folder: 'bridge9'
  },
  {
    range: [
      1467,
      1506
    ],
    folder: 'map9',
    length: 20
  },
  {
    range: [
      1507,
      1509
    ],
    folder: 'bridge8'
  },
  {
    range: [
      1510,
      1549
    ],
    folder: 'map8',
    length: 20
  },
  {
    range: [
      1550,
      1552
    ],
    folder: 'bridge7'
  },
  {
    range: [
      1553,
      1592
    ],
    folder: 'map7',
    length: 20
  },
  {
    range: [
      1593,
      1595
    ],
    folder: 'bridge6'
  },
  {
    range: [
      1596,
      1635
    ],
    folder: 'map6',
    length: 20
  },
  {
    range: [
      1636,
      1638
    ],
    folder: 'bridge5'
  },
  {
    range: [
      1639,
      1678
    ],
    folder: 'map5',
    length: 20
  },
  {
    range: [
      1679,
      1681
    ],
    folder: 'bridge4'
  },
  {
    range: [
      1682,
      1721
    ],
    folder: 'map4',
    length: 20
  },
  {
    range: [
      1722,
      1724
    ],
    folder: 'bridge3'
  },
  {
    range: [
      1725,
      1764
    ],
    folder: 'map3',
    length: 20
  },
  {
    range: [
      1765,
      1767
    ],
    folder: 'bridge2'
  },
  {
    range: [
      1768,
      1807
    ],
    folder: 'map2',
    length: 20
  },
  {
    range: [
      1808,
      1810
    ],
    folder: 'bridge1'
  },
  {
    range: [
      1811,
      1850
    ],
    folder: 'map1',
    length: 20
  }
],
  getMapData = function (pos) {
    'use strict';

    return _.find(maps, function (val) {
      return pos >= val.range[0] && pos <= val.range[1];
    });
  };

exports = {
  doodads: [],
  tileWidth: 64,
  tileHeight: 100,
  defaultTile: 0,
  friendsX: 0,
  friendsY: 0,
  directory: function (tileX, tileY) {
    'use strict';

    var data = getMapData(tileY),
      y = tileY - data.range[0];

    if (data.length) {
      y %= data.length;
    }

    return 'resources/images/tiles/' +
      data.folder + '/' + y + '_' + tileX + '.png';
  }
};
