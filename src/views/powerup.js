/* global timer, Powerup, powerup_info */

/* jshint ignore:start */

import util.underscore as _;

import DevkitHelper.timer as timer;

import quest.views.powerup as Powerup;
import src.views.popups.powerup_info as powerup_info;

/* jshint ignore:end */

exports = Class(Powerup, function (supr) {
  'use strict';

  this.build = function (powerup, model) {
    var unlocked;

    supr(this, 'build', [powerup]);
    unlocked = GC.app.user.isUnlocked(powerup);

    if (unlocked) {
      this.on('InputStart', function () {
        timer.timeout('touch', function () {
          powerup_info.build(powerup, model);
        }, 500);
      });
      this.on('InputOut', function () {
        timer.clearTimeout('touch');
      });
    }
  };

  this.onRelease = function () {
    supr(this, 'onRelease');
    this.removeAllListeners('InputStart');
    this.removeAllListeners('InputOut');
  };
});
