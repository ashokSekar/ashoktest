/* global ImageScaleView, ImageView, View, style, image, utils */

/* jshint ignore:start */
import ui.View as View;
import ui.ImageView as ImageView;
import ui.ImageScaleView as ImageScaleView;

import quest.modules.image as image;
import DevkitHelper.style as style;

/* jsio: if */
import src.modules.util as utils;
/* jsio: else */
import quest.modules.util as utils;
/* jsio: fi */

/* jshint ignore:end */

exports = Class(View, function (supr) {
  'use strict';

  this.init = function () {
    var opts = style.get('puzzle_screen_bg');

    supr(this, 'init', [opts]);

    this.hud = new ImageScaleView({
      superview: this
    });

    this.puzzle = new ImageScaleView(
      style.get('puzzle_bg', {
        superview: this
      })
    );

    this.border_left = new View(style.get('border_left', {
      superview: this.puzzle
    }));

    this.border_left_fixed = new ImageView({
      superview: this.border_left
    });

    this.border_left_tile = new ImageScaleView({
      superview: this.border_left
    });

    this.border_right = new View(style.get('border_right', {
      superview: this.puzzle
    }));

    this.border_right_fixed = new ImageView({
      superview: this.border_right
    });

    this.border_right_tile = new ImageScaleView({
      superview: this.border_right
    });
  };

  this.build = function () {
    var hud_bg = image.get('puzzlescreen/hud_bg'),
      hud_img = image.get('puzzlescreen/hud'),
      curr_tile_set = GC.app.user.get('curr_tile_set'),
      puzzle = image.get('puzzle/faces/' + curr_tile_set + '/bg'),
      left_fixed = image.get('puzzlescreen/left_fixed'),
      right_fixed = image.get('puzzlescreen/right_fixed'),
      left_tile = image.get('puzzlescreen/left_tile'),
      right_tile = image.get('puzzlescreen/right_tile'),
      hud_height = hud_img.getHeight(),
      left_fixed_width = left_fixed.getWidth(),
      right_fixed_width = right_fixed.getWidth(),
      left_fixed_height = left_fixed.getHeight(),
      right_fixed_height = right_fixed.getHeight(),
      left_tile_height = left_tile.getHeight(),
      right_tile_height = right_tile.getHeight(),
      left_fixed_offset = this.border_left._opts.offsetY,
      right_fixed_offset = this.border_right._opts.offsetY,
      puzzle_height = style.base_height - hud_height,
      left_tile_rows = Math.ceil((puzzle_height - left_fixed_height -
        left_fixed_offset) / left_tile_height),
      right_tile_rows = Math.ceil((puzzle_height - right_fixed_height -
        right_fixed_offset) / right_tile_height);

    this.hud.updateOpts({
      image: hud_bg,
      offsetY: utils.getSafeArea().top,
      scaleMethod: '2slice',
      width: hud_bg.getWidth(),
      height: hud_height,
      sourceSlices: {
        vertical: {
          top: 44,
          bottom: 1
        }
      },
      destSlices: {
        vertical: {
          top: 44
        }
      }
    });

    this.puzzle.updateOpts({
      image: puzzle,
      width: puzzle.getWidth(),
      height: puzzle_height
    });

    this.border_left.updateOpts({
      width: left_fixed_width
    });

    this.border_right.updateOpts({
      width: right_fixed_width
    });

    this.border_left_fixed.updateOpts({
      image: left_fixed,
      width: left_fixed_width,
      height: left_fixed_height
    });

    this.border_left_tile.updateOpts(style.get('border_left_tile', {
      image: left_tile,
      rows: left_tile_rows,
      width: left_tile.getWidth(),
      height: left_tile_height * left_tile_rows
    }));

    this.border_right_fixed.updateOpts({
      image: right_fixed,
      width: right_fixed_width,
      height: right_fixed_height
    });

    this.border_right_tile.updateOpts(style.get('border_right_tile', {
      image: right_tile,
      rows: right_tile_rows,
      width: right_tile.getWidth(),
      height: right_tile_height * right_tile_rows
    }));
  };
});
