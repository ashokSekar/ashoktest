/* global _, ImageView, style, config, animate, util, image, effects */

/* jshint ignore:start */
import animate;
import effects;
import util.underscore as _;
import quest.modules.util as util;
import ui.ImageView as ImageView;

import DevkitHelper.style as style;
import resources.data.config as config;

import quest.modules.image as image;
/* jshint ignore:end */

exports = Class(ImageView, function (supr) {
  'use strict';

  this.init = function (opts) {
    supr(this, 'init', [merge({}, opts)]);

    _.bindAll(this, 'clean', 'highlight', 'changeImage', 'matchAnimate',
      'wrongMatchAnimate', 'changeFace', 'shuffleAnimate',
      'changeTag', 'onChangeFace', 'substituteAnimate', 'updateJokerFace',
      'swapAnimate', 'afterSwapAnimate', 'onChangeFaceVisibility');

    this.face = new ImageView(style.get('tile_face', {
      superview: this
    }));

    this.tag = new ImageView(style.get('tile_tag', {
      superview: this
    }));

    this.overlay = new ImageView(style.get('tile_overlay', {
      superview: this,
      visible: false
    }));
  };

  this.build = function (model, no_drag) {
    var img = image.get('puzzle/tile_overlay'),
      is_web = util.getDeviceInfo('store') === 'web',
      puzzle;

    this.model = model;
    puzzle = model.get('puzzle');
    this.on('InputStart', model.pick);
    if (!no_drag && model.get('puzzle').get('open_slot')) {
      this.on(is_web ? 'InputStart' : 'InputMove', this.registerDrag);
    }

    model.on('shuffle-animate', this.shuffleAnimate);
    model.on('highlight', this.highlight);
    model.on('match-animate', this.matchAnimate);
    model.on('wrong-match-animate', this.wrongMatchAnimate);
    model.on('change:state', this.changeImage);
    model.on('clean', this.clean);

    // Registering listener only if golden tile given
    if (puzzle.get('given_golden') > 0) {
      model.on('change:face', this.onChangeFace);
    }
    model.on('change-face', this.changeFace);
    model.on('substitute-animate', this.substituteAnimate);
    model.on('swap-animate', this.swapAnimate);
    model.on('after-swap-animate', this.afterSwapAnimate);
    model.on('change:face_visibility', this.onChangeFaceVisibility);
    model.on('change:tag', this.changeTag);
    model.on('update-joker-face', this.updateJokerFace);

    this.overlay.updateOpts({
      image: img,
      height: img.getHeight(),
      width: img.getWidth()
    });

    this.onChangeFaceVisibility(model.get('face_visibility'), false);
    this.changeImage();
    this.changeFace();
    this.changeTag();
  };

  this.registerDrag = function (evt) {
    var model = this.model,
      puzzle = model.get('puzzle');

    if (puzzle.get('mode') === null && puzzle.get('slot_tile') === null &&
      puzzle.get('pending_matches') === 0 &&
      model.get('state') === 2 && model.isFree()) {
      this.startDrag({
        inputStartEvt: evt,
        radius: 0
      });
    }
  };

  this.onChangeFace = function () {
    var face = this.model.get('face'),
      prev_face = this.model.getPrevious('face'),
      golden_face = config.special_faces.golden;

    if (face === golden_face || prev_face === golden_face) {
      this.changeImage();
    }
  };

  this.onDragStart = function () {
    this.model.get('puzzle').set('mode', 'open_slot');
    this.startPt = {
      x: this.style.x,
      y: this.style.y,
      zIndex: this.style.zIndex
    };
    this.style.zIndex = 20000;
  };

  this.onDrag = function (start_evt, drag_evt) {
    var src_pt = this.startPt;

    this.style.x = src_pt.x + drag_evt.srcPt.x - start_evt.srcPt.x;
    this.style.y = src_pt.y + drag_evt.srcPt.y - start_evt.srcPt.y;
  };

  this.onDragStop = function (start_evt, drag_evt) {
    var src_pt = this.startPt;

    this.model.get('puzzle').set('mode', null);

    if ((Math.abs(drag_evt.srcPt.y - start_evt.srcPt.y) > 50 ||
      Math.abs(drag_evt.srcPt.x - start_evt.srcPt.x) > 50) &&
      !this.model.isLock()) {
      this.updateOpenSlot();
    } else {
      this.style.zIndex = src_pt.zIndex;
      this.getSuperview().reflow();
    }
  };

  this.updateOpenSlot = function () {
    var model = this.model;

    this.setHandleEvents(false);
    model.get('puzzle').emit('open-slot', model);
  };

  this.shuffleAnimate = function () {
    var model = this.model,
      tile_size = style.get('tile_size'),
      y_orig = this.style.y,
      x_orig = this.style.x,
      offsetX = this.style.offsetX,
      offsetY = this.style.offsetY,
      grid_style = this.getSuperview().style,
      center_x = grid_style.anchorX - offsetX - tile_size.width / 2,
      center_y = grid_style.anchorY - offsetY - tile_size.height / 2;

    // To avoid shuffle animation for slot tile
    if (model.isSlot()) {
      center_x = x_orig;
      center_y = y_orig;
    }

    model.get('puzzle').increment('ongoing');

    // once shuffle completes ,animate back them to orignal postion
    model.get('puzzle').once('shuffle-complete',
      bind(this, this.shuffleAnimComplete, x_orig, y_orig));

    // animating tile to center first
    animate(this, 'anim_move')
      .now({
        x: center_x,
        y: center_y
      }, 250);
  };

  this.shuffleAnimComplete = function (x_orig, y_orig) {
    var anim_move = animate(this, 'anim_move');

    // adding wait beacuse some time shuffle completes
    // too much fast and even tiles are not reached at center
    anim_move
      .wait(250)
      .then({
        x: x_orig,
        y: y_orig
      }, 250)
      .then(bind(this, function () {
        this.model.get('puzzle').decrement('ongoing');
      }));
  };

  this.highlight = function () {
    this.model.showHint();
    this.prev_zIndex = this.style.zIndex;
    this.prev_scale = this.style.scale;
    this.updateOpts({
      scale: 1.2,
      zIndex: 999
    });

    effects.hover(this);
  };

  this.explode = function () {
    effects.explode(this, {
      scale: 1.2,
      blend: false,
      duration: 400,
      images: ['resources/images/puzzlescreen/particle_smoke.png']
    });
  };

  this.matchAnimate = function (cb) {
    var anim = animate(this, 'match_animate'),
      puzzle = this.model.get('puzzle');

    this.setHandleEvents(false, true);
    this.updateOpts({zIndex: 9999});
    puzzle.increment('ongoing');
    anim
      .now({
        scale: 1.2
      }, 200, animate.easeOut)
      .wait(60)
      .then({opacity: 0}, 240, animate.easeIn)
      .then(bind(this, function () {
        this.explode();
        puzzle.decrement('ongoing');

        // swoosh match animate has delay of 200 ms
        // b/w two swoosh match animate
        // if puzzle paused in it b/w by calling anim.pause()
        // will have no effect, so pausing it after that
        if (puzzle.get('puzzle_paused')) {
          anim.pause();
        }
      }))
      .then(function () {
        cb();
      });
  };

  this.swapAnimate = function (cb) {
    var anim = animate(this, 'swap_animate'),
      puzzle = this.model.get('puzzle');

    this.setHandleEvents(false, true);
    this.orig_z = this.style.zIndex;
    puzzle.increment('ongoing');
    anim
      .now({
        zIndex: 9999,
        scale: 1.3
      }, 300, animate.easeOut)
      .then(function () {
        puzzle.decrement('ongoing');
        if (cb) {
          cb();
        }
      });
  };

  this.afterSwapAnimate = function (cb) {
    var anim = animate(this, 'after_swap_animate'),
      face_animate = animate(this.face, 'face_transition'),
      model = this.model,
      puzzle = this.model.get('puzzle');

    this.setHandleEvents(false, true);
    puzzle.increment('ongoing');
    face_animate
      .now({
        opacity: 0
      }, 300)
      .then(bind(this, function () {
        model.emit('change-face');
      }))
      .then({
        opacity: 1
      }, 300)
      .then(bind(this, function () {
        anim
          .now({
            zIndex: this.orig_z,
            scale: 1
          }, 300, animate.easeOut)
          .then(bind(this, function () {
            puzzle.decrement('ongoing');
            this.setHandleEvents(true, false);
            cb();
          }));
      }));
  };

  this.wrongMatchAnimate = function (cb) {
    var wrong_match = animate(this, 'wrong_match'),
      orig_x = this.style.x;

    wrong_match
      .then({
        x: orig_x + 10
      }, 50)
      .then({
        x: orig_x - 10
      }, 50)
      .then({
        x: orig_x
      }, 50)
      .then(function () {
        if (cb) {
          cb();
        }
      });
  };

  /*
    Toggle b/w various tile images
      0 -> Tile which cannot be selected (blocked)
      1 -> Normal open tiles
      2 -> Tile picked by user
      3 -> Tile hinted
  */
  this.changeImage = function () {
    var state = this.model.get('state'),
        state_type = this.model.isGoldenTile() ? 'gold' : 'tile',
        style = {
          image: image.get('puzzle/' + state_type + '_' + state)
        },
        prev_state = this.model.getPrevious('state');

    if (prev_state === 3) {
      style.scale = this.prev_scale;
      style.zIndex = this.prev_zIndex;
    }
    this.updateOpts(style);

    if (state === 0) {
      this.overlay.show();
    } else {
      this.overlay.hide();
    }
  };

  this.changeFace = function (face, visibility) {
    var img,
      model = this.model;

    face = face || model.get('face');

    if (face === config.special_faces.joker &&
      model.get('puzzle').get('type') === config.variations.memory.id) {
      face = this.getJokerFace(face, !!visibility);
    }

    img = image.get('puzzle/faces/' +
      GC.app.user.get('curr_tile_set') + '/' + face);

    this.face.updateOpts(style.get('tile_face', {
      image: img,
      height: img.getHeight(),
      width: img.getWidth()
    }));
  };

  this.substituteAnimate = function (cb) {
    effects.explode(this, {
      scale: 1.2,
      blend: false,
      duration: 600,
      images: ['resources/images/puzzlescreen/substitute_smoke.png']
    });
    cb();
  };

  this.onChangeFaceVisibility = function (visible, do_animate) {
    var visibility, face_animate;

    visibility = visible ? 1 : 0;
    if (do_animate === false) {
      this.face.updateOpts({
        opacity: visibility
      });
    } else {
      face_animate = animate(this.face, 'face_transition');
      face_animate
        .now({
          opacity: visibility
        }, 250);
    }
  };

  this.changeTag = function () {
    var tag_img,
      tag = this.model.get('tag');

    if (tag) {
      tag_img = image.get('puzzle/' + tag);
      this.tag.updateOpts({
        image: tag_img
      });
      this.tag.show();
    } else {
      this.tag.hide();
    }
  };

  this.getJokerFace = function (face, visibility) {
    var type = this.model.get('puzzle').get('type');

    if (!visibility) {
      return type + '_' + face;
    }

    return face;
  };

  this.updateJokerFace = function (visibility) {
    this.changeFace(this.model.get('face'), visibility);
  };

  this.clean = function () {
    animate(this).clear();
    this.setHandleEvents(true, false);
    this.removeAllListeners('InputStart');
    this.removeAllListeners('InputMove');
    if (this.releaseFromPool) {
      this.releaseFromPool();
    }
  };
});
