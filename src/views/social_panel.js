/* global TextView, style, i18n
*/

/* jshint ignore:start */
import ui.TextView as TextView;
import DevkitHelper.style as style;
import DevkitHelper.i18n as i18n;
/* jshint ignore:end */

exports = function (supr) {
  'use strict';

  this.prepare = function () {
    supr(this, 'prepare', []);
    this.message = new TextView(style.get('social_panel_msg', {
      superview: this
    }));
  };

  this.build = function (puzzle, opts) {
    supr(this, 'build', [puzzle, opts]);
    if (!GC.app.user.get('social_facebook')) {
      this.message.updateOpts({
        visible: true,
        text: i18n('social_panel_msg')
      });
    }
    return this;
  };

  this.clean = function () {
    /* istanbul ignore else */
    if (this.message) {
      this.message.hide();
    }

    supr(this, 'clean', []);
  };
};
