/* global ImageScaleView, ViewPool, style, image,
  ButtonView, util, i18n, test */

/* jshint ignore:start */
import util.underscore as _;
import ui.ViewPool as ViewPool;
import quest.ui.ButtonView as ButtonView;
import quest.modules.image as image;
import ui.ImageScaleView as ImageScaleView;
import DevkitHelper.i18n as i18n;

import DevkitHelper.style as style;
import quest.modules.util as util;
import DevkitHelper.test as test;
/* jshint ignore:end */

exports = Class(ImageScaleView, function (supr) {
  'use strict';

  var tile_sets = {},
    fetchTileProducts = function () {
      _.each(util.getProducts(), function (val, key) {
        if (key.search(/tile/) !== -1) {
          tile_sets[key] = val;
        }
      });
    };

  this.init = function () {
    supr(this, 'init', [style.get('tile_tab')]);

    fetchTileProducts();

    this.tile_sets = new ViewPool({
      ctor: ButtonView,
      initCount: _.size(tile_sets),
      initOpts: style.get('tile_set_btn')
    });

    _.bindAll(this, 'clean', 'createButton', 'tileSetSelected');
  };

  this.build = function (popup) {
    var bg_img = image.get('popup/tile_store/tile_tab_bg');

    this.popup = popup;

    this.updateOpts({
      image: bg_img,
      height: bg_img.getHeight()
    });

    this.renderTileSets();
  };

  this.renderTileSets = function () {
    var selected_tile,
      user = GC.app.user,
      tile_keys = _.keys(tile_sets);

    _.each(tile_keys, this.createButton);

    selected_tile = _.find(tile_keys, function (tile) {
      return !_.contains(user.get('tile_sets'), tile_sets[tile].name);
    });
    this.tileSetSelected(selected_tile || tile_keys[0]);
  };

  this.createButton = function (item, order) {
    var item_name = tile_sets[item].name,
      img = image.button('popup/tile_store/' + item_name, true),
      btn = this.tile_sets.obtainView({
        superview: this,
        order: order,
        images: img,
        toggleSelected: true,
        width: img.unselected.getWidth(),
        height: img.unselected.getHeight(),
        on: {
          selected: bind(this, this.tileSetSelected, item)
        }
      });

    btn.setTitle(i18n(item_name));

    this['btn_' + item_name] = btn;
    return btn;
  };

  this.tileSetSelected = function (item) {
    var tile_set_list = _.without(_.keys(tile_sets), item),
      name;

    this['btn_' + tile_sets[item].name].setHandleEvents(false, true);
    this['btn_' + tile_sets[item].name].setState(ButtonView.states.SELECTED);
    _.each(tile_set_list, bind(this, function (tile_set_btn) {
      name = tile_sets[tile_set_btn].name;
      this['btn_' + name].setState(ButtonView.states.UNSELECTED);
      this['btn_' + name].setHandleEvents(true, false);
    }));

    this.emit('tile-set-select', item);
  };

  this.clean = function () {
    this.tile_sets.releaseAllViews();
  };

  test.prepare(this, {
    tile_sets: tile_sets,
    fetchTileProducts: fetchTileProducts
  });
});
