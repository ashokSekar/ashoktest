/* global image, style, ImageView, _, ButtonView, Powerups,
   HudInfo, GoalsHUD, utils */

/* jshint ignore:start */
import ui.ImageView as ImageView;

import util.underscore as _;
import DevkitHelper.style as style;
import quest.ui.ButtonView as ButtonView;
import quest.modules.image as image;
import src.views.powerups as Powerups;
import src.views.hud_info as HudInfo;
import src.views.goals_hud as GoalsHUD;

/* jsio: if */
import src.modules.util as utils;
/* jsio: else */
import quest.modules.util as utils;
/* jsio: fi */

/* jshint ignore:end */

exports = Class(ImageView, function (supr) {
  'use strict';

  this.init = function () {
    var opts = style.get('hud');

    supr(this, 'init', [opts]);

    this.undo_btn = new ButtonView(style.get('undo'));
    this.powerups = new Powerups();
    this.hud_info = new HudInfo();
    this.goals_hud = new GoalsHUD();
    this.addSubview(this.hud_info);
    this.addSubview(this.goals_hud);
    this.addSubview(this.powerups);
    this.addSubview(this.undo_btn);
    _.bindAll(this, 'undo', 'clean', 'toggleUndo');
  };

  this.build = function (model) {
    var hud_bg = image.get('puzzlescreen/hud'),
      undo_img = image.button('puzzlescreen/undo', false, true),
      undo_btn = this.undo_btn;

    this.updateOpts({
      image: hud_bg,
      width: hud_bg.getWidth(),
      height: hud_bg.getHeight(),
      offsetY: utils.getSafeArea().top,
      top: 0
    });

    undo_btn.updateOpts({
      images: undo_img,
      width: undo_img.up.getWidth(),
      height: undo_img.up.getHeight(),
      on: {
        up: this.undo
      }
    });
    this.toggleUndo();

    this.model = model;
    model.on('clean', this.clean);
    model.on('change:last_match', this.toggleUndo);
    this.powerups.build(model);
    this.hud_info.build(model);
    this.goals_hud.build(model);

    return this;
  };

  this.toggleUndo = function (unlocked) {
    this.undo_btn.setState(!_.isEmpty(unlocked) ?
      ButtonView.states.UP : ButtonView.states.DISABLED);
  };

  this.undo = function () {
    this.model.emit('undo');
  };

  this.clean = function () {
    this.undo_btn.onRelease();
    this.hud_info.clean();
  };
});
