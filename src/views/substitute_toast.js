/* global Toast, style, i18n, image, ui, Tile, TextView,
  ImageView */

/* jshint ignore:start */
import src.views.toast as Toast;
import DevkitHelper.style as style;
import DevkitHelper.i18n as i18n;
import quest.modules.image as image;
import DevkitHelper.style as ui;
import ui.ImageView as ImageView;
import ui.TextView as TextView;
import src.views.tile as Tile;

/* jshint ignore:end */

exports = Class(Toast, function (supr) { // jshint ignore:line
  'use strict';

  this.init = function () {
    supr(this, 'init');
  };

  this.prepare = function () {
    var arrow_img = image.get('puzzlescreen/arrow');

    this.tile_one = new Tile();
    this.tile_two = new Tile();

    this.toast_title = new TextView({
      superview: this
    });

    this.arrow = new ImageView(style.get('toast_substitute_arrow', {
      superview: this,
      image: arrow_img
    }));
  };

  this.build = function (parent, face_one, face_two) {
    var bg_img = image.get('puzzlescreen/shuffle_overlay'),
      img = image.get('puzzle/tile_1');

    if (this.getSubviews().length === 0) {
      this.prepare();
    }

    this.tile_one.changeFace(face_one);
    this.tile_two.changeFace(face_two);
    this.tile_one.updateOpts(style.get('toast_tile_face_left', {
      superview: this,
      image: img,
      height: img.getHeight(),
      width: img.getWidth()
    }));
    this.tile_two.updateOpts(style.get('toast_tile_face_right', {
      superview: this,
      image: img,
      height: img.getHeight(),
      width: img.getWidth()
    }));

    this.toast_title.updateOpts(style.get('toast_substitute', {
      text: i18n('toast_text_substitute')
    }));

    this.updateOpts({
      centerY: true,
      superview: parent,
      image: bg_img,
      opacity: 1,
      height: bg_img.getHeight(),
      width: ui.base_width
    });
    this.show();
  };
});
