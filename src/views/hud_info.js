/* global ImageView, ViewPool, image, string, _,
   timer, style, JewelHud*/

/* jshint ignore:start */
import util.underscore as _;

import ui.ImageView as ImageView;
import ui.ViewPool as ViewPool;

import quest.modules.image as image;
import quest.utils.string as string;

import DevkitHelper.style as style;
import DevkitHelper.timer as timer;
import src.views.jewel as JewelHud;
/* jshint ignore:end */

exports = Class(ImageView, function (supr) {
  'use strict';

  this.init = function () {
    supr(this, 'init', [style.get('progress_info')]);

    this.img_pool = new ViewPool({
      ctor: ImageView,
      initCount: 3,
      initOpts: {
        layout: 'box',
        centerY: true
      }
    });

    this.cross = new ImageView({
      superview: this
    });

    this.jewels_pool = new ViewPool({
      ctor: JewelHud,
      initCount: 3
    });
    this.arrows_pool = new ViewPool({
      ctor: ImageView,
      initCount: 2
    });

    this.loaded_imgs = [];
    this.jewel_list = [];
    this.arrows = [];
    this.glows = [];
    this.model = null;

    _.bindAll(this, 'highlight', 'build', 'changeJewels', 'buildJewels');
  };

  this.build = function (model) {
    var type = model.get('type'),
      func = this[string.toCamel('build_' + type)],
      img = image.get('puzzlescreen/progress_bg');

    this.model = model;

    if (func) {
      this.updateOpts({
        image: img,
        height: img.getHeight(),
        width: img.getWidth()
      });

      this.show();

      func.call(this);
    } else {
      model.once('puzzle-loaded', this.build);
      this.hide();
    }
  };

  this.buildJewels = function () {
    var img_jewel,
      jewels = this.model.get('jewels'),
      img_arrow = image.get('puzzle/arrow');

    _.each(jewels, bind(this, function (jewel, index) {
      var i = index,
        curr_jewel = this.jewels_pool.obtainView({
          superview: this,
          order: i,
          image: img_jewel
        });

      img_jewel = image.get('puzzle/' + jewel + '_inactive');
      curr_jewel.build(jewel);
      this.jewel_list.push(curr_jewel);
      if (index < jewels.length - 1) {
        this.arrows.push(this.arrows_pool.obtainView({
          superview: this,
          image: img_arrow,
          order: i + 1,
          offsetY: 12,
          width: img_arrow.getWidth(),
          height: img_arrow.getHeight()
        }));
      }
    }));
    this.model.on('change:jewel_order', this.changeJewels);
  };

  this.changeJewels = function () {
    var jewel_order = this.model.get('jewel_order'),
      jewels = this.model.get('jewels'),
      wrong_order_flag = false;

    _.each(jewels, bind(this, function (jewel, index) {
      var jewel_view = this.jewel_list[index],
        jewel_list = this.jewel_list;

      if (jewel === jewel_order[index]) {
        // Correct order
        if (jewel_view.isInactive()) {
          jewel_view.changeImage('active');

          if (index === jewels.length - 1 && !wrong_order_flag) {
            _.each(jewel_list, function (jewel, view_idx) {
              timer.timeout('animate_all_jewels_' + view_idx, function () {
                jewel.animateJewels();
              }, 200 * (view_idx + 1));
            });
          } else {
            jewel_view.animateJewels();
          }
        }
      } else if (jewel_order[index]) {
        // Incorrect order
        wrong_order_flag = true;
        jewel_view.wrongAnimate();
      } else {
        jewel_view.changeImage('inactive');
      }
    }));
    if (this.model.get('tile_count') === 0) {
      if (!this.model.isJewelNotInOrder()) {
        this.model.set('goal_completed', true);
        this.model.checkComplete();
      }
    }
  };

  this.buildThreeMatch = function () {
    _.times(3, bind(this, function () {
      this.loaded_imgs.push(this.img_pool.obtainView({
        superview: this
      }));
    }));

    this.highlight();
    this.model.on('change:tile_selected', this.highlight);
  };

  this.highlight = function (is_timer) {
    var selected = this.model.get('tile_selected'),
      prev_selected = this.model.getPrevious('tile_selected') || [],
      sel_length = _.isArray(selected) ? selected.length : 0,
      img_disable = image.get('puzzlescreen/tile_unselected'),
      img_enable = image.get('puzzlescreen/tile_selected'),
      loaded_imgs = this.loaded_imgs,
      timeout_id = 'selected_highlight';

    // This condition make sure that 3rd tile select highlight will be visible,
    // else it will get cleared fast
    if (is_timer !== true && sel_length === 0 && prev_selected.length === 3) {
      return timer.timeout(timeout_id, bind(this, this.highlight, true), 400);
    }

    timer.clearTimeout(timeout_id);

    _.times(sel_length, function (idx) {
      loaded_imgs[idx].updateOpts({
        image: img_enable,
        height: img_enable.getHeight(),
        width: img_enable.getWidth()
      });
    });

    while (sel_length < loaded_imgs.length) {
      loaded_imgs[sel_length++].updateOpts({
        image: img_disable,
        height: img_disable.getHeight(),
        width: img_disable.getWidth()
      });
    }
  };

  this.clean = function () {
    this.img_pool.releaseAllViews();
    this.arrows_pool.releaseAllViews();
    this.jewels_pool.releaseAllViews();
    this.jewel_list = [];
    this.loaded_imgs = [];
  };
});
