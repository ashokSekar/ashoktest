/* global View, animate, image, style, ImageView */

/* jshint ignore:start */
import ui.View as View;
import ui.ImageView as ImageView;
import animate;
import quest.modules.image as image;
import DevkitHelper.style as style;
/* jshint ignore:end */

exports = Class(View, function (supr) {
  'use strict';

  this.init = function () {
    supr(this, 'init', [{
      layout: 'box'
    }]);

    this.jewel = new ImageView(style.get('jewel_style', {
      superview: this,
      layout: 'box'
    }));

    this.glow = new ImageView(style.get('jewel_glow_style', {
      superview: this,
      layout: 'box'
    }));
  };

  this.build = function (type) {
    var glow_img = image.get('puzzlescreen/glow');

    this.type = type;
    this.changeImage();
    this.glow.updateOpts({
      image: glow_img,
      width: glow_img.getWidth(),
      height: glow_img.getHeight()
    });
    this.status = 'inactive';
  };

  this.changeImage = function (status) {
    var path,
      jewel_type = this.type,
      jewel_image, jewel_width, jewel_height;

    this.status = status;

    if (status === 'wrong') {
      path = 'puzzlescreen/cross';
    } else if (status === 'active') {
      path = 'puzzle/' + jewel_type;
    } else {
      path = 'puzzle/' + jewel_type + '_inactive';
    }
    jewel_image = image.get(path);
    jewel_width = jewel_image.getHeight();
    jewel_height = jewel_image.getHeight();
    this.jewel.updateOpts({
      image: jewel_image,
      width: jewel_width,
      height: jewel_height
    });

    this.updateOpts({
      width: jewel_width,
      height: jewel_height
    });
  };

  this.isInactive = function () {
    if (this.status === 'inactive') {
      return true;
    }
  };

  this.wrongAnimate = function () {
    this.changeImage('wrong');

    animate(this.jewel)
      .now({
        scale: 3.5
      }, 0)
      .then({
        scale: 1.75
      }, 700)
      .then({
        scale: 1
      })
      .then(bind(this, function () {
        this.changeImage('inactive');
      }));
  };

  this.animateJewels = function () {
    animate(this.glow, 'glow')
      .now({
        opacity: 1,
        scale: 1
      }, 30, animate.easeOutElastic)
      .then({
        opacity: 0
      });

    animate(this.jewel, 'scale')
      .now({
        scale: 1.4
      }, 5, animate.easeOutElastic)
      .then({
        scale: 1
      });
  };
});
