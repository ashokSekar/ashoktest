/* global style, i18n, image, View, TextView, ImageView  */

/* jshint ignore:start */

import DevkitHelper.style as style;
import DevkitHelper.i18n as i18n;
import quest.modules.image as image;

import ui.View as View;
import ui.TextView as TextView;
import ui.ImageView as ImageView;

/* jshint ignore:end */

exports = Class(View, function (supr) {
  'use strict';

  this.init = function () {
    var opts = style.get('powerup_step_viewpool');

    supr(this, 'init', [opts]);
    this.step_no = new ImageView(style.get('powerup_step_no_image', {
      superview: this
    }));
    this.step = new ImageView(style.get('powerup_step_image', {
      superview: this
    }));
    this.step_text = new TextView(style.get('popup_powerup_info_text', {
      superview: this
    }));
  };

  this.build = function (powerup, step) {
    var steps_img = image.get('tutorial/' + powerup + '_' + step),
      steps_no_img = image.get('tutorial/step_' + step);

    this.step_no.updateOpts({
      image: steps_no_img,
      height: steps_no_img.getHeight(),
      width: steps_no_img.getWidth()
    });

    this.step.updateOpts({
      image: steps_img,
      height: steps_img.getHeight(),
      width: steps_img.getWidth()
    });
    this.step_text.setText(i18n('powerup_info_' + powerup + '_step_' + step));
    this.updateOpts({
      order: step,
      height: this.step.style.height + this.step_text.style.height
    });
  };
});
