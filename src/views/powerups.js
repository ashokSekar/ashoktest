/* global Powerup */

/* jshint ignore:start */

import quest.views.powerups as Powerup;

/* jshint ignore:end */

exports = Class(Powerup, function (supr) {
  'use strict';

  this.powerupSelected = function (powerup) {
    var free_shuffle = this.model.get('free_shuffle');

    if (free_shuffle === 0 && powerup === 'shuffle') {
      this.model.pauseTimers();
      this.applyPowerup(powerup, false);
    } else {
      supr(this, 'powerupSelected', [powerup]);
    }
  };
});
