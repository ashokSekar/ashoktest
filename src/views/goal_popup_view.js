/* global View, TextView, TextView, ImageView,
  style, image, i18n */

/* jshint ignore:start */
import util.underscore as _;

import DevkitHelper.style as style;
import DevkitHelper.i18n as i18n;

import quest.modules.image as image;

import ui.View as View;
import ui.TextView as TextView;
import ui.ImageView as ImageView;

/* jshint ignore:end */

exports = Class(View, function (supr) {
  'use strict';

  this.init = function () {
    supr(this, 'init');
    this.goals = new TextView(style.get('goals_text', {
      superview: this,
      order: 1
    }));
    this.check = new ImageView({
      superview: this,
      order: 0
    });
  };

  this.build = function (goal) {
    var check_view,
      img = image.get('popup/goals/tick');

    check_view = this.check.updateOpts({
      image: img,
      width: img.getWidth(),
      height: img.getHeight(),
      centerY: true
    });
    this.goals.setText(i18n('goal_' + goal));
    return this;
  };
});
