/* global image, ImageView, _, i18n, GoalHUDView,
  string, View, config, style */

/* jshint ignore:start */
import ui.View as View;
import ui.ImageView as ImageView;

import util.underscore as _;
import quest.modules.image as image;
import quest.utils.string as string;
import src.views.goal_hud_view as GoalHUDView;
import DevkitHelper.i18n as i18n;
import DevkitHelper.style as style;

import resources.data.config as config;

/* jshint ignore:end */

exports = Class(View, function (supr) {
  'use strict';

  this.init = function () {
    supr(this, 'init', [style.get('goals_hud')]);

    this.face = new ImageView({
      superview: this
    });
    this.left_view = new GoalHUDView();
    this.right_view = new GoalHUDView();
    this.addSubview(this.left_view);
    this.addSubview(this.right_view);
    _.bindAll(this, 'onLoad', 'updateLimit',
      'updateGoal');
  };

  this.build = function (model) {
    var face_img = image.get('puzzlescreen/face_hud');

    // Updating HUD on the base of puzzle type
    model.on('puzzle-loaded', this.onLoad);
    this.model = model;
    this.face.updateOpts({
      order: 1,
      image: face_img,
      width: face_img.getWidth(),
      height: face_img.getHeight()
    });
    this.left_view.build('left');
    this.right_view.build('right');
    return this;
  };

  this.onLoad = function () {
    var type = this.model.get('type');

    this.face.updateOpts({
      opacity: _.contains(['jewels', 'three_match'], type) ? 0 : 1
    });
    this.right_view.setType(type);
    this.registerLimits(type);
  };

  this.registerLimits = function (type) {
    var model = this.model,
      goals = config.variations[type].goals;

    model.on('change:limit', this.updateLimit);
    _.each(goals, bind(this, function (goal) {
      // Converting goals into puzzle property
      goal = config.goals_prop[goal];
      if (_.contains(['tap', 'time'], goal)) {
        model.on('change:' + goal, this.updateLimit);
      }
      if (_.contains(['tile_count', 'golden_collected', 'lock_keys'], goal)) {
        model.on('change:' + goal, this.updateGoal);
      }
    }));
    this.updateLimit();
    this.updateGoal();
  };

  this.updateGoal = function () {
    var goal_count,
      model = this.model,
      type = model.get('type');

    switch (type) {
      case 'golden': goal_count = model.get('goal').golden * 2 -
        model.get('collected_golden') * 2;
        break;

      case 'lock_key': goal_count = model.getNoOfLocks();
        break;

      default: goal_count = this.model.get('tile_count');
        break;
    }
    this.right_view.updateGoal(goal_count);
  };

  this.updateLimit = function () {
    var model = this.model,
      left_view = this.left_view,
      limit_info = left_view.limit_info,
      limit_view = left_view.limit_view,
      limit_type = model.get('limit_type'),
      limit = model.get('limit'),
      limit_info_visible = limit_type === 'tap',
      left_view_visible = true,
      limit_text;

    switch (limit_type) {
      case 'tap': limit_text = limit - model.get(limit_type);
        limit_info.setText(i18n('taps'));
        limit_view.updateOpts(style.get('tap_limit'));
        break;

      case 'untimed': limit_text = '';
        left_view_visible = false;
        break;

      default: limit_text = string.convertSeconds(limit -
      model.get(model.get('limit_type')));
        break;
    }
    this.left_view.updateOpts({
      visible: left_view_visible
    });
    this.left_view.limit_info.updateOpts({
      visible: limit_info_visible
    });
    this.left_view.limit_view.setText(limit_text);
  };
});
