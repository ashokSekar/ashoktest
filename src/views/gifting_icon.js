/* global image, SpriteView */

/* jshint ignore:start */
import quest.modules.image as image;
import ui.SpriteView as SpriteView;

/* jshint ignore:end */

exports = Class(SpriteView, function (supr) {
  'use strict';

  this.init = function (opts) {
    var icon_view = image.get('map_screen/gift/gift-idle-01.png'),
      icon_width = icon_view.getWidth(),
      icon_height = icon_view.getHeight();

    opts = merge(opts, {
      width: icon_width,
      height: icon_height,
      offsetX: -100,
      offsetY: 160,
      url: 'resources/images/map_screen/gift/gift',
      frameRate: 12,
      loop: true,
      defaultAnimation: 'idle',
      autoStart: true
    });
    supr(this, 'init', [opts]);
  };
});

