/* global _, style, ButtonView, View,
  util, TextView */

/* jshint ignore:start */
import ui.View as View;
import ui.TextView as TextView;

import util.underscore as _;

import quest.modules.util as util;
import DevkitHelper.style as style;
import quest.ui.ButtonView as ButtonView;
/* jshint ignore:end */

exports = function (supr) {
  'use strict';

  var helper_buttons = {
      map: {
        title: true,
        btns: ['maphelp', 'mapfeedback']
      },
      puzzle: {
        title: false,
        btns: ['map', 'restart']
      }
    },
    button_elements = {
      map: [],
      puzzle: ['resume', 'help', 'feedback']
    };

  this.prepare = function () {
    if (supr(this, 'prepare', [7])) {
      this.level_no = new TextView(style.get('pause_level_no_text', {
        superview: this
      }));

      this.helper_view = new View(style.get('settings_helper', {
        superview: this
      }));

      _.bindAll(this, 'onMaphelp', 'onMapfeedback');
    }
  };

  this.build = function (opts) {
    var origin = opts.origin,
      level_no = opts.level_no,
      bonus_level = opts.bonus_level,
      helper_btns = helper_buttons[origin],
      is_web = util.getDeviceInfo('store') === 'web',
      btn;

    this.prepare();

    this.level_no.updateOpts({
      visible: origin === 'puzzle' && !bonus_level,
      text: level_no ? level_no : GC.app.user.getMs('curr')
    });

    if (is_web) {
      button_elements.puzzle = _.without(button_elements.puzzle, 'help');
      helper_btns.btns = _.without(helper_btns.btns, 'maphelp');
    }
    this.setButtonElements(button_elements);

    _.each(helper_btns.btns, function (key) {
      btn = this.getButton(key, false, helper_btns.title, true,
        style.get('settings_helper_' + key));
      this.helper_view.addSubview(btn);
      btn.setState(ButtonView.states.UP);
    }, this);

    return supr(this, 'build', [{
      origin: origin,
      type: origin === 'puzzle' ? 'pause' : 'settings',
      popup_bg: origin === 'map' ? 'medium' : null
    }]);
  };

  this.onMaphelp = function () {
    this.onHelp();
  };

  this.onMapfeedback = function () {
    supr(this, 'onFeedback');
    return true;
  };

  this.clean = function () {
    this.helper_view.removeAllSubviews();
    supr(this, 'clean');
  };
};
