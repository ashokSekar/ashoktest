/* global popup, config, ImageView, image,
  TextView, popup, style, i18n, View */

/* jshint ignore:start */

import DevkitHelper.style as style;
import DevkitHelper.i18n as i18n;

import ui.TextView as TextView;
import ui.ImageView as ImageView;
import ui.View as View;

import resources.data.config as config;
import quest.modules.image as image;

/* jsio: if */
import src.modules.popup_manager as popup;
/* jsio: else */
import quest.modules.popup_manager as popup;
/* jsio: fi */
/* jshint ignore:end */

exports = Class(View, function (supr) { // jshint ignore:line
  'use strict';

  this.init = function () {
    supr(this, 'init', [style.get('new_variation_content')]);
    this.content = new TextView(style.get('popup_bonus_desc', {
      superview: this
    }));
    this.variation_img = new ImageView({
      superview: this,
      offsetX: 62
    });
  };

  this.build = function (variation) {
    var img,
      variation_img = this.variation_img,
      show_img = variation === config.variations.jewels.id;

    this.content.setText(i18n(variation + '_desc'));

    if (show_img) {
      img = image.get('popup/new_variation/' + variation);
      this.content.updateOpts({
        offsetY: 25,
        height: 85
      });
      variation_img.updateOpts({
        image: img,
        width: img.getWidth(),
        height: img.getHeight()
      });
      variation_img.show();
    } else {
      variation_img.hide();
      this.content.updateOpts(style.get('popup_bonus_desc'));
    }

    // TODO: Duplicate file for bg
    popup.add({
      id: 'new_variation',
      body: this,
      title: i18n(variation + '_title'),
      close: true,
      popup_bg: 'small_no_head',
      type: 'new_variation',
      actions: []
    });
    return popup;
  };

  this.clean = function () {
    this.removeFromSuperview();
    popup.close();
  };
});
