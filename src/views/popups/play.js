/* global style, TextView, Play, star_view, i18n, puzzle_module, View,
 ViewPool, PregamePowerup, util, popup, history, config, tutorial_view,
 _, ImageView, image */

/* jshint ignore:start */
import ui.TextView as TextView;
import ui.View as View;
import ui.ImageView as ImageView;
import ui.ViewPool as ViewPool;

import util.underscore as _;

import DevkitHelper.style as style;
import DevkitHelper.i18n as i18n;
import DevkitHelper.history as history;

import quest.views.popups.play as Play;
import quest.modules.puzzle as puzzle_module;
import quest.modules.util as util;
import quest.modules.image as image;
import quest.modules.popup_manager as popup;

import src.views.star_view as star_view;
import quest.views.powerup_pregame as PregamePowerup;
import src.views.tutorial as tutorial_view;

import resources.data.config as config;
/* jshint ignore:end */

exports = Class(Play, function (supr) {
  'use strict';

  this.init = function () {
    supr(this, 'init', []);
    this.popup_text = new TextView(style.get('play_popup_text', {
      superview: this
    }));

    this.motivation_text = new TextView(style.get('play_popup_motivation', {
      superview: this
    }));

    this.powerup_container = new View(style.get('pregame_powerup_container', {
      superview: this
    }));

    this.bonus_star = new ImageView(style.get('play_bonus_star', {
      superview: this
    }));

    this.bonus_info = new TextView(style.get('play_bonus_info', {
      superview: this
    }));

    this.btn_pool = new ViewPool({
      ctor: PregamePowerup,
      initCount: _.size(util.getPowerups('pregame')),
      initOpts: {
        layout: 'box'
      }
    });
  };

  this.build = function (puzzle, hide_social) {
    var opts = {
        type: 'play',
        popup_bg: 'puzzle_large_rays',
        title: puzzle.number,
        extra: GC.app.user.get('social_facebook') ? 'fb' : 'nonfb'
      },
      bonus_puzzle = puzzle.bonus_level,
      limit_type = puzzle.limit_type || 'time',
      level_stars = _.isNumber(puzzle.stars) ?
        puzzle.stars : puzzle_module.getScore(puzzle.id).stars,
      limit = puzzle[limit_type],
      limit_text = limit_type,
      powerups = _.keys(util.getPowerups('pregame')),
      limit_extra, bonus_star_img;

    if (puzzle.type === 'untimed') {
      limit = puzzle.layout.length;
      limit_text = 'untimed_play';
      this.popup_text.updateOpts({
        size: 32
      });
    } else if (limit_type === 'time') {
      limit = Math.floor(limit / 60);
      limit_extra = puzzle.time - limit * 60;
      limit_text = limit_extra === 0 ? 'time' : 'time_secs';
      this.popup_text.updateOpts(style.get('play_popup_text'));
    }

    if (bonus_puzzle) {
      bonus_star_img = image.get('popup/star');

      delete opts.extra;
      delete opts.title;

      powerups = _.difference(powerups,
        config.variations[puzzle.type].powerups_exclude);
      this.bonus_star.updateOpts({
        visible: true,
        image: bonus_star_img,
        height: bonus_star_img.getHeight(),
        width: bonus_star_img.getWidth()
      });
      this.bonus_info.updateOpts({
        visible: true,
        text: i18n('play_bonus_info', [puzzle.bonus_level])
      });
    } else {
      this.bonus_star.hide();
      this.bonus_info.hide();

      if (!puzzle.bonus_map) {
        popup.on('play-popup:appear', bind(this, function () {
          this.loadPowerupTutorial(puzzle.type);
        }));
      }
      star_view.populateStars(level_stars, this, opts.type);
    }

    _.each(powerups, bind(this, function (curr, order) {
      var powerup = this.btn_pool.obtainView({
        order: order
      });

      powerup.build(curr, puzzle);
      this.powerup_container.addSubview(powerup);
    }));

    this.popup_text.setText(i18n(limit_text, [limit, limit_extra]));
    this.motivation_text.setText(i18n('play_motivation' +
      (bonus_puzzle ? '_bonus' : '')));

    supr(this, 'build', [puzzle, opts, hide_social]);
  };

  this.loadPowerupTutorial = function (puzzle_type) {
    var app = GC.app,
      game_type = puzzle_type || 'classic',
      powerups = util.getPowerups('pregame'),
      unlockables = _.omit(config.unlockables,
        config.variations[game_type].powerups_exclude),
      user = app.user,
      curr_ms = user.getMs('curr'),

      // TODO: move this function to util as repeating in game_intro
      getOldPowerup = function () {
        return _.findKey(unlockables, function (ms, powerup) {
          if (ms < curr_ms &&
            !_.contains(user.get('unlocked_powerups'), powerup)) {
            return true;
          }
        });
      },
      curr_powerup = _.invert(unlockables)[curr_ms],
      play_btn = popup.getCurrentView().foot.getSubviews()[0],
      positions = {},
      tutorial = app.tutorial,
      context, type, powerup_info,
      powerup_container = this.powerup_container,
      setContext = function (curr_powerup) {
        return _.each(powerup_container.getSubviews(), function (powerup) {
          if (powerup.name === curr_powerup) {
            context = powerup;
          }
        });
      };

    if (!curr_powerup) {
      curr_powerup = getOldPowerup();
      curr_ms = unlockables[curr_powerup];
    }

    type = curr_powerup ? 'enable_' + curr_powerup : null;

    if (_.contains(_.keys(powerups), curr_powerup) &&
      !tutorial.isCompleted(type, {
        milestone: curr_ms,
        type: 'play_popup'
      }) && !_.contains(user.get('unlocked_powerups'), curr_powerup)) {
      setContext(curr_powerup);

      user.addFreeGoods(curr_powerup, config.award_powerup_count[curr_powerup],
        'first_time_bonus', this.puzzle.instance_id);

      positions[type] = {
        view: {
          params: {
            overlay: 'pregame_powerup'
          },
          timeout: 50
        },
        context: context,
        action: {
          context: context
        },
        before: history.setBusy,
        after: history.resetBusy
      };

      positions[type + '_play'] = {
        view: {
          timeout: 150,
          params: {
            overlay: 'play'
          }
        },
        context: play_btn,
        action: {
          context: play_btn,
          func: 'onInputSelect'
        },
        before: history.setBusy,
        after: history.resetBusy
      };
    } else {
      powerup_info = user.getPowerupForInfo('pregame');
      if (powerup_info) {
        setContext(powerup_info);
        positions.powerup_info = {
          view: {
            params: {
              overlay: 'pregame_powerup'
            },
            timeout: 50
          },
          context: context,
          before: history.setBusy,
          after: history.resetBusy
        };
      }
      curr_ms = 'powerup_info';
    }

    if (!_.isEmpty(positions)) {
      tutorial.build({
        superview: app.getCurrentView(),
        type: 'play_popup',
        milestone: curr_ms,
        view: tutorial_view,
        positions: positions
      });
    }
  };

  this.clean = function () {
    supr(this, 'clean', []);
    popup.removeAllListeners('play-popup:appear');
    star_view.clean();
    this.btn_pool.releaseAllViews();
  };
});
