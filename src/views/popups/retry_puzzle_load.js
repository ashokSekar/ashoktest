/* global i18n, TextView, popup, style, View,
  ButtonView, image, history, config
*/

/* jshint ignore:start */
import ui.View as View;
import ui.TextView as TextView;
import DevkitHelper.style as style;
import DevkitHelper.i18n as i18n;
import util.underscore as _;
import quest.modules.popup_manager as popup;
import quest.modules.image as image;
import quest.ui.ButtonView as ButtonView;
import DevkitHelper.history as history;
import resources.data.config as config;

/* jshint ignore:end */

exports = new (Class(View, function (supr) { // jshint ignore:line
  'use strict';

  this.prepare = function () {
    supr(this, 'init', [style.get('puzzle_load_fail_content')]);

    _.bindAll(this, 'retry');

    this.retry_content = new TextView(style.get('puzzle_load_fail_message', {
      superview: this,
      order: 1
    }));

    this.retry_content_small = new TextView(style.get(
      'puzzle_load_fail_message_small',
      {
        superview: this,
        order: 2
      }
    ));

    this.btn_container = new View(style.get('retry_actions', {
      superview: this,
      order: 3
    }));

    this.btn_home = new ButtonView(style.get('btn_retry_actions', {
      superview: this.btn_container
    }));

    this.btn_retry = new ButtonView(style.get('btn_retry_actions', {
      superview: this.btn_container
    }));
  };

  this.build = function (type, count) {
    var btn_img = image.button('popup/retry_puzzle_load/btn_large'),
      content_opacity = 0,
      content_offsetY = 50;

    if (this.getSubviews().length === 0) {
      this.prepare();
    }

    this.type = type;

    this.retry_content.setText(i18n('retry_' + type + '_content'));
    if (type === 'gameload') {
      this.retry_content_small.setText(
        i18n('retry_' + type + '_content_small')
      );
      content_opacity = 1;
      content_offsetY = -10;
    }

    this.retry_content_small.updateOpts({
      opacity: content_opacity
    });
    this.retry_content.updateOpts({
      offsetY: content_offsetY
    });

    if (count > config.max_retry) {
      btn_img = image.button('popup/retry_puzzle_load/btn_small');
      this.btn_home.updateOpts({
        images: btn_img,
        opacity: 1,
        width: btn_img.up.getWidth(),
        height: btn_img.up.getHeight(),
        on: {
          up: this.home
        }
      });
      this.btn_home.setState(ButtonView.states.UP);
      this.btn_home.setTitle(i18n('retry_home'));
      this.btn_home.show();
    } else {
      this.btn_home.hide();
    }

    this.btn_retry.updateOpts({
      images: btn_img,
      width: btn_img.up.getWidth(),
      height: btn_img.up.getHeight(),
      on: {
        up: this.retry
      }
    });

    this.btn_retry.setState(ButtonView.states.UP);
    this.btn_retry.setTitle(i18n('retry_btn'));

    popup.add({
      id: 'retry_puzzle_load',
      type: 'retry_puzzle_load',
      popup_bg: 'small_red',
      title: i18n('retry_title'),
      body: this,
      close: false,
      order: 2,
      actions: []
    });

    history.setBusy();
    return popup;
  };

  this.home = function () {
    popup.emit('retry-home');
    popup.close();
  };

  this.retry = function () {
    history.resetBusy();
    popup.close();
    popup.emit('retry-' + this.type);
  };
}))();
