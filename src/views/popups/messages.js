/* global Messages */

/* jshint ignore:start */
import quest.views.popups.messages as Messages;
/* jshint ignore:end */

exports = Class(Messages, function (supr) {
  'use strict';

  this.build = function () {
    supr(this, 'build', [{scale: true}]);
  };
});
