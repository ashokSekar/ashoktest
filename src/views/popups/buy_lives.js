/* global config, products, BuyLives */

/* jshint ignore:start */
import resources.data.config as config;
import resources.data.products as products;
import quest.views.popups.buy_lives as BuyLives;
/* jshint ignore:end */

exports = Class(BuyLives, function (supr) {
  'use strict';

  this.init = function () {
    var button_types = [{
      type: 'buy',
      amount: products.lives.cost,
      lives: config.lives.max
    }, {
      type: 'facebook'
    }];

    this.setButtonType(button_types);
    supr(this, 'init');
  };

  this.build = function (source) {
    supr(this, 'build', [source, {align_bottom: true}]);
  };
});
