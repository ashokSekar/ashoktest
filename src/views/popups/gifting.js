/* global ImageView, image */

/* jshint ignore:start */
import ui.ImageView as ImageView;
import quest.modules.image as image;
/* jshint ignore:end */

exports = function (supr) {
  'use strict';

  this.prepare = function () {
    supr(this, 'prepare');

    this.img = new ImageView({
      superview: this,
      layout: 'box',
      order: 3,
      top: -50,
      centerX: true
    });
  };

  this.build = function (gift, cb) {
    var img = image.get('popup/gifting/' + gift.type);

    supr(this, 'build', [gift, cb]);

    this.img.updateOpts({
      image: img,
      height: img.getHeight(),
      width: img.getWidth()
    });
    if (gift.type === "not_yet") {
      this.img.updateOpts({
        top: -90
      });
    }
  };
};
