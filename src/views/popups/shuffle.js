/* global i18n, TextView, ImageView, ButtonView, util,
  popup, style, View, timer, image, inventory_manager, history
*/

/* jshint ignore:start */
import ui.View as View;
import ui.TextView as TextView;
import ui.ImageView as ImageView;
import DevkitHelper.style as style;
import DevkitHelper.i18n as i18n;
import DevkitHelper.timer as timer;
import DevkitHelper.history as history;
import quest.modules.image as image;
import quest.ui.ButtonView as ButtonView;
import util.underscore as _;
import quest.modules.util as util;
import quest.modules.popup_manager as popup;
import quest.modules.inventory_manager as inventory_manager;

/* jshint ignore:end */

exports = new (Class(View, function (supr) { // jshint ignore:line
  'use strict';

  this.prepare = function () {
    var btn_container;

    supr(this, 'init', [style.get('shuffle_content')]);
    this.shuffle = new ImageView(style.get('shuffle_icon', {
      superview: this
    }));
    this.shuffle_msg = new TextView(style.get('shuffle_msg', {
      superview: this
    }));

    this.button_buy = new ButtonView(style.get('shuffle_btn_buy', {
      superview: this
    }));

    btn_container = new View(style.get('shuffle_btn_container', {
      superview: this
    }));

    this.button_give_up = new ButtonView(style.get('shuffle_btn_give_up', {
      superview: btn_container
    }));

    this.buy_count = new TextView(style.get('shuffle_buy_count', {
      superview: this.button_buy
    }));
    _.bindAll(this, 'buy', 'use', 'closePopup', 'applyShuffle', 'resumeTimer');
    this.createButton();
  };

  this.build = function (model) {
    var shuffle_icon = image.get('popup/shuffle/shuffle.png'),
      free_shuffle = model.get('free_shuffle'),
      inventory_shuffle = GC.app.user.get('inventory_shuffle'),
      btn_action = inventory_shuffle > 0 ? 'use' : 'buy',
      btn_buy = image.button('popup/shuffle/btn_' + btn_action);

    this.model = model;
    model.pauseTimers();
    if (this.getSubviews().length === 0) {
      this.prepare();
    }

    this.shuffle.updateOpts({
      image: shuffle_icon,
      width: shuffle_icon.getWidth(),
      height: shuffle_icon.getHeight()
    });

    if (free_shuffle > 0) {
      this.button_buy.updateOpts({
        images: btn_buy,
        width: btn_buy.up.getWidth(),
        height: btn_buy.up.getHeight(),
        on: {
          up: this[btn_action]
        }
      });
      this.button_buy.setTitle(i18n('shuffle_btn_buy'));
      this.button_buy.setState(ButtonView.states.UP);

      this.shuffle_msg.setText(i18n('shuffle_msg'));
      if (inventory_shuffle > 0) {
        this.buy_count.hide();
      } else {
        this.buy_count.show();
      }
      this.button_give_up.show();
      this.button_buy.show();
    } else {
      this.button_buy.hide();
      this.button_give_up.hide();
      this.shuffle_msg.setText(i18n('shuffle_free_msg'));
      timer.timeout('auto_shuffle', this.applyShuffle, 1500);
    }

    popup.add({
      id: 'shuffle',
      body: this,
      type: 'none',
      group: 'store',
      title: null,
      extra: null,
      actions: [],
      close: false
    });
    history.setBusy();
    return popup;
  };

  this.applyShuffle = function (success, powerup, isPurchased) {
    var model = this.model;

    if (success === false) {
      history.setBusy();
      return;
    }
    this.model.increment('ongoing');
    history.resetBusy();
    this.model.on('change:ongoing', this.resumeTimer);

    // Todo handle shuffle impossible case
    model.emit('shuffle');
    if (success) {
      if (!isPurchased) {
        popup.close();
      }
      this.model.increment('shuffle');
      GC.app.user.decrement('inventory_shuffle');
    } else {
      popup.close();
    }
  };

  this.resumeTimer = function (ongoing) {
    if (ongoing === 0) {
      this.model.resumeTimers();
      this.model.removeListener('change:ongoing',
        this.resumeTimer);
    }
  };

  this.createButton = function () {
    var btn_small = image.button('popup/shuffle/small');

    this.button_give_up.updateOpts({
      images: btn_small,
      width: btn_small.up.getWidth(),
      height: btn_small.up.getHeight(),
      on: {
        up: this.closePopup
      }
    });
    this.button_give_up.setTitle(i18n('shuffle_btn_give_up'));
    this.button_give_up.setState(ButtonView.states.UP);
    this.buy_count.setText(i18n('shuffle_buy_count',
      [util.getProducts().shuffle.cost]));
  };

  this.buy = function () {
    var instance_id = this.model.get('instance_id');

    history.resetBusy();
    inventory_manager.buy('shuffle', this.applyShuffle, null, instance_id);
  };

  this.closePopup = function () {
    history.resetBusy();
    popup.close();
    this.model.emit('game-over', false);
  };

  this.clean = function () {
    if (timer.hasTimeout('auto_shuffle')) {
      timer.clear('auto_shuffle');
    }
  };

  this.use = function () {
    this.applyShuffle(true);
  };
}))();
