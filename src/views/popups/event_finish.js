/* global View, TextView, i18n, style, popup*/

/* jshint ignore:start */
import ui.View as View;
import ui.TextView as TextView;

import DevkitHelper.i18n as i18n;
import DevkitHelper.style as style;

/* jsio: if */
import src.modules.popup_manager as popup;
/* jsio: else */
import quest.modules.popup_manager as popup;
/* jsio: fi */

/* jshint ignore:end */

exports = Class(View, function (supr) {
  'use strict';

  this.init = function () {
    supr(this, 'init', [style.get('event_finish_content')]);

    this.desc = new TextView(style.get('event_finish_desc', {
      superview: this
    }));
  };

  this.build = function () {
    this.desc.setText(i18n('event_finish_desc'));

    popup.add({
      id: 'event-finish',
      type: 'event_finish',
      title: i18n('event_finish_head'),
      close: false,
      popup_bg: 'small_no_head',
      body: this,
      actions: [{
        id: 'ok',
        style: 'ok',
        title: 'event_finish_ok',
        close: true
      }]
    });
  };
});
