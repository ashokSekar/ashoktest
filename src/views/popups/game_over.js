/* global ImageView, style, image, TextView, GameOver,
  i18n, star_view, social_panel, Toast, config */

/* jshint ignore:start */
import ui.TextView as TextView;
import quest.modules.image as image;
import ui.ImageView as ImageView;
import src.views.star_view as star_view;
import quest.views.social_panel as social_panel;
import DevkitHelper.style as style;
import DevkitHelper.i18n as i18n;
import quest.views.popups.game_over as GameOver;
import src.views.toast as Toast;
import resources.data.config as config;
/* jshint ignore:end */

exports = Class(GameOver, function (supr) {
  'use strict';

  this.init = function () {
    supr(this, 'init', []);

    this.level_no = new TextView(style.get('gameover_level_no', {
      superview: this
    }));

    this.score = new TextView(style.get('gameover_score', {
      superview: this,
      layout: 'box'
    }));

    this.high_score = new TextView(style.get('gameover_high_score', {
      superview: this,
      layout: 'box'
    }));

    this.illustration = new ImageView({
      layout: 'box',
      centerX: true,
      zIndex: -1,
      bottom: 141
    });

    this.bonus_star = new ImageView(style.get('gameover_bonus_star', {
      superview: this
    }));

    this.toast = new Toast();
  };

  this.build = function (model, won, hide_social, opts) {
    var type = 'gameover',
      img_path = 'popup/gameover/',
      level_stars = model.get('stars'),
      score = model.get('score'),
      bonus = model.get('bonus_level'),
      img, best_score, info_type;

    opts = _.merge({
        type: type,
        extra: GC.app.user.get('social_facebook') ? 'fb' : 'nonfb',
        popup_bg: 'puzzle_small' + (won ? '_rays' : ''),
        no_bgm: bonus ? false : won
      }, opts || {});

    if (bonus) {
      img = image.get('popup/star');
      info_type = won ? 'won' : 'fail';

      delete opts.extra;
      opts.offsetY = -100;

      this.bonus_star.updateOpts({
        visible: true,
        image: img,
        height: img.getHeight(),
        width: img.getWidth()
      });
      this.level_no.hide();
      this.illustration.hide();
      opts.title = i18n('gameover_bonus_title', [bonus]);
      this.score.setText(i18n('gameover_' + info_type + '_bonus_info_1'));
      this.high_score.setText(i18n('gameover_' + info_type + '_bonus_info_2'));
    } else {
      opts.offsetY = 0;
      best_score = model.getBestScore();
      img = image.get(img_path + 'illustration_' + (won ? 'win' : 'lose'));

      this.level_no.updateOpts({
        text: model.get('ms_no'),
        visible: true
      });
      this.score.setText(i18n('puzzle_score', [score]));
      this.high_score.setText(i18n('puzzle_high_score', [best_score]));

      star_view.populateStars(level_stars, this, type);
      this.bonus_star.hide();

      if (!hide_social) {
        this.illustration.updateOpts({
          visible: true,
          superview: social_panel,
          width: img.getWidth(),
          height: img.getHeight(),
          image: img
        });
        opts.offsetY = -150;
        if (won) {
          this.showMotivationMsg();
        }
      }
    }

    supr(this, 'build', [model, won, opts, hide_social]);
  };

  this.showMotivationMsg = function () {
    var user = GC.app.user,
      motivation_msg_index = user.increment('motivation_msg_index');

    if (config.no_of_motivation_msg < motivation_msg_index) {
      motivation_msg_index = 0;
      user.set('motivation_msg_index', motivation_msg_index);
    }

    this.toast.build(social_panel, 'motivation_msg_' + motivation_msg_index, {
      text_type: 'motivation_msg',
      bg: 'toast/motivation_bg',
      offsetY: -25,
      y: -25,
      width: 840
    });
    this.toast.clean(1250, 1500, {
      y: this.toast.style.offsetY + this.toast.style.height
    });
  };

  this.clean = function () {
    supr(this, 'clean', []);
    this.illustration.removeFromSuperview();
    this.toast.removeFromSuperview();
    star_view.clean();
    this.toast.clean();
  };
});
