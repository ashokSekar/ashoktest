/* global _, style, View, popup, i18n,
    ViewPool, config, dh_timer, GoalView */

/* jshint ignore:start */
import ui.View as View;
import ui.ViewPool as ViewPool;
import DevkitHelper.i18n as i18n;
import DevkitHelper.timer as dh_timer;

import util.underscore as _;

import resources.data.config as config;

import src.views.goal_popup_view as GoalView;
import DevkitHelper.style as style;
import quest.modules.popup_manager as popup;
/* jshint ignore:end */

exports = Class(View, function (supr) {
  'use strict';

  var timer = 'close_goal_popup';

  this.init = function () {
    this.goals_pool = new ViewPool({
      ctor: GoalView,
      initCount: 5
    });
    this.goal_view = new GoalView();
    supr(this, 'init', [style.get('goal_content')]);
  };

  this.build = function (puzzle_type) {
    var goals_container,
      type = puzzle_type ? puzzle_type : 'classic',
        goals = config.variations[type].goals;

    _.each(goals, bind(this, function (goal, index) {
      goals_container = this.goals_pool.obtainView(style.get('goals_list', {
        order: index
      }));
      goals_container.build(goal, index);
      this.addSubview(goals_container);
    }));
    popup.add({
      id: 'goals',
      type: 'goals',
      title: i18n('goals'),
      body: this,
      close: false,
      actions: []
    });
    dh_timer.timeout(timer, function () {
      popup.close();
    }, 2500);
  };

  this.clean = function () {
    dh_timer.unregister(timer);
    this.removeAllSubviews();
    this.goals_pool.releaseAllViews();
  };
});
