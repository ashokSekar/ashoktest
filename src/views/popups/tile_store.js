/* global _, View, ImageView, ButtonView, popup, style,
  i18n, image, TileTab, util, inventory_manager
*/

/* jshint ignore:start */
import util.underscore as _;
import ui.View as View;
import ui.ImageView as ImageView;

import DevkitHelper.i18n as i18n;
import DevkitHelper.style as style;

import quest.ui.ButtonView as ButtonView;
import quest.modules.image as image;
import quest.modules.popup_manager as popup;
import quest.modules.util as util;

import src.views.tile_tab as TileTab;
import quest.modules.inventory_manager as inventory_manager;
/* jshint ignore:end */

exports = Class(View, function (supr) {
  'use strict';

  var selected_item;

  this.init = function () {
    supr(this, 'init', [style.get('tile_store_init')]);

    this.tile_tab = new TileTab();

    this.tile_set_display = new ImageView(style.get('tile_set_display', {
      superview: this,
      order: 2
    }));

    this.buy_btn = new ButtonView(style.get('buy_tile_btn', {
      superview: this,
      order: 3
    }));

    _.bindAll(this, 'updateView', 'onSelectTile', 'onBuy');
  };

  this.build = function () {
    var btn_imgs = image.button('popup/tile_store/use_btn');

    this.buy_btn.updateOpts({
      images: btn_imgs,
      width: btn_imgs.up.getWidth(),
      height: btn_imgs.up.getHeight(),
      on: {
        up: this.onSelectTile
      },
      opacity: 0
    });

    this.buy_btn.setTitle(i18n('tile_set_use'));
    this.buy_btn.setState(ButtonView.states.UP);
    this.buy_btn.setHandleEvents(false, true);

    this.tile_tab.on('tile-set-select', this.updateView);
    this.tile_tab.build();
    this.addSubview(this.tile_tab);

    popup.add({
      id: 'sale',
      type: 'sale',
      title: i18n('tile_store'),
      close: true,
      group: 'store',
      popup_bg: 'large',
      body: this
    });
  };

  this.onSelectTile = function () {
    var user = GC.app.user,
      name = util.getProducts()[selected_item].name;

    if (_.contains(user.get('tile_sets'), name)) {
      user.set('curr_tile_set', name);
      popup.close();
    } else {
      inventory_manager.buy(selected_item, this.onBuy);
    }
  };

  this.onBuy = function (success, product, is_purchased) {
    var user = GC.app.user,
      name;

    if (success) {
      if (!is_purchased) {
        popup.close();
      }
      name = util.getProducts()[selected_item].name;
      user.push('tile_sets', name);
      user.set('curr_tile_set', name);
      user.save();
    }
  };

  this.updateView = function (tile_set) {
    var cost, img,
      action = 'use',
      user = GC.app.user,
      product = util.getProducts()[tile_set],
      name = product.name,
      btn_visibile = user.get('curr_tile_set') !== name,
      img_tile = image.get('popup/tile_store/' + name);

    selected_item = tile_set;

    this.tile_set_display.updateOpts({
      image: img_tile,
      width: img_tile.getWidth(),
      height: img_tile.getHeight()
    });

    if (!_.contains(user.get('tile_sets'), name)) {
      cost = product.cost;
      action = 'buy';
    }

    img = image.button('popup/tile_store/' + action + '_btn');
    this.buy_btn.updateOpts({
      opacity: btn_visibile ? 1 : 0,
      images: img,
      width: img.up.getWidth(),
      height: img.up.getHeight(),
      text: {
        offsetX: action === 'buy' ? -30 : 0
      }
    });
    this.buy_btn.setTitle(i18n('tile_set_' + action, [cost]));
    this.buy_btn.setState(ButtonView.states.UP);
    this.buy_btn.setHandleEvents(btn_visibile, !btn_visibile);
  };

  this.clean = function () {
    this.tile_tab.removeListener('tile-set-select', this.updateView);
    this.tile_tab.clean();
  };
});
