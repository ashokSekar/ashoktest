/* global Overtake, i18n */

/* jshint ignore:start */
import DevkitHelper.i18n as i18n;

import quest.views.popups.overtake as Overtake;
/* jshint ignore:end */

exports = Class(Overtake, function (supr) {
  'use strict';

  this.build = function (data) {
    var type = data.type;

    data.popup_opts = {
      popup_bg: 'small' + (type === 'score' ? '_red' : ''),
      title: type === 'ms' ? i18n('overtake_title') : null
    };

    supr(this, 'build', [data]);
    this.share_btn.updateOpts({
      offsetY: type === 'ms' ? -20 : 10
    });
  };
});
