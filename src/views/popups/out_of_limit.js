/* global LimitPopup, ImageScaleView, ImageView, TextView, image, style,
  i18n, animate, popup */

/* jshint ignore:start */
import animate;
import ui.ImageScaleView as ImageScaleView;
import ui.ImageView as ImageView;
import ui.TextView as TextView;

import DevkitHelper.style as style;
import DevkitHelper.i18n as i18n;

import quest.modules.popup_manager as popup;
import quest.views.popups.out_of_limit as LimitPopup;
import quest.modules.image as image;
/* jshint ignore:end */

exports = Class(LimitPopup, function (supr) {
  'use strict';

  this.init = function (opts) {
    supr(this, 'init', [opts]);
    this.progress_bg = new ImageView(style.get('outof_limit_progress_bg', {
      superview: this
    }));

    this.progress_bar = new ImageScaleView(style.get(
      'outof_limit_progress_bar', {
      superview: this.progress_bg
    }));

    this.progress_text = new TextView(style.get(
      'outof_limit_progress_text', {
      superview: this
    }));

    this.illustration = new ImageView(style.get('limit_illustration', {
      superview: this
    }));
  };

  this.build = function (model) {
    var limit = model.getLimit(),
      progress_bg_img = image.get('popup/limit/progress_bg'),
      progress_bar_img = image.get('popup/limit/progress_bar'),
      illustration_img = image.get('popup/limit/' + limit),
      max_width = progress_bg_img.getWidth() - 2 * this.progress_bar.style.x,
      cleared_tiles = model.get('solved').length * model.get('tile_per_match'),
      total_tiles = cleared_tiles + model.get('tile_count'),
      progress_bar_width = max_width * cleared_tiles / total_tiles;

    this.progress_bg.updateOpts({
      image: progress_bg_img,
      height: progress_bg_img.getHeight(),
      width: progress_bg_img.getWidth()
    });

    this.progress_bar.updateOpts({
      image: progress_bar_img,
      height: progress_bar_img.getHeight(),
      width: 0
    });

    this.progress_text.setText(i18n('out_of_limit_progress',
      [cleared_tiles, total_tiles]));

    popup.once('out-of-limit:appear', bind(this, this.animateProgress,
      progress_bar_width));

    this.illustration.updateOpts({
      image: illustration_img,
      height: illustration_img.getHeight(),
      width: illustration_img.getWidth()
    });

    supr(this, 'build', [model, limit, {align_bottom: true}]);
  };

  this.animateProgress = function (target_width) {
    var anim_progress = animate(this.progress_bar);

    anim_progress.then({
        width: target_width
      }, 300, animate.easeOutCubic)
      .then(bind(anim_progress, anim_progress.clear));
  };
});
