/* global _, View, ImageView, ButtonView, popup, style,
  i18n, image, TextView, puzzle_module, string */

/* jshint ignore:start */
import util.underscore as _;
import ui.View as View;
import ui.ImageView as ImageView;

import DevkitHelper.i18n as i18n;
import DevkitHelper.style as style;
import quest.ui.ButtonView as ButtonView;
import quest.modules.image as image;
import quest.modules.popup_manager as popup;
import quest.utils.string as string;
import ui.TextView as TextView;
/* jshint ignore:end */

exports = Class(View, function (supr) {
  'use strict';

  this.init = function () {
    supr(this, 'init', [style.get('event_popup_content')]);

    this.bg_img = new ImageView(style.get('event_bg', {
      superview: this,
      order: 1
    }));

    this.event_name = new TextView(style.get('event_name', {
      superview: this,
      order: 2
    }));

    this.text_is = new TextView(style.get('event_text_is', {
      superview: this,
      order: 3
    }));

    this.text_here = new TextView(style.get('event_text_here', {
      superview: this,
      order: 4
    }));

    this.event_end_info = new TextView(style.get('event_end_info', {
      superview: this,
      order: 5
    }));

    this.event_hurry = new TextView(style.get('event_hurry', {
      superview: this,
      order: 6
    }));

    this.text_end_in = new TextView(style.get('event_ends_in', {
      superview: this,
      order: 7
    }));

    this.play_btn = new ButtonView(style.get('event_play_btn', {
      superview: this,
      order: 8
    }));

    jsio('import quest.modules.puzzle as puzzle_module');

    _.bindAll(this, 'updateTime');
  };
  this.build = function (opts, bg_img) {
    var type = opts.type,
      btn_imgs = image.button('popup/events/btn_play');

    this.bg_img.updateOpts({
      image: bg_img,
      height: bg_img.getHeight(),
      width: bg_img.getWidth()
    });

    this.event_name.setText(i18n(type));
    this.text_is.setText(i18n('event_text_is'));
    this.text_here.setText(i18n('event_text_here'));

    this.event_end_info.setText(i18n('event_end_info'));
    this.event_hurry.setText(i18n('event_hurry'));
    this.play_btn.updateOpts({
      images: btn_imgs,
      height: btn_imgs.up.getHeight(),
      width: btn_imgs.up.getWidth(),
      on: {
        up: bind(this, function () {
          popup.emit('events:play');
        })
      }
    });

    this.play_btn.setState(ButtonView.states.UP);
    this.play_btn.setTitle(i18n('event_play_btn'));

    this.updateTime(opts.time_left);

    puzzle_module.on('bonus-map-time', this.updateTime);

    popup.add({
      id: 'events',
      type: 'events',
      title: i18n('event_popup_title'),
      close: true,
      group: 'store',
      popup_bg: 'large',
      body: this
    });
  };

  this.updateTime = function (time_left) {
    if (time_left >= 0) {
      this.text_end_in.setText(i18n('event_ends_in',
        [string.convertSeconds(time_left)]));
    } else {
      this.clean();
      popup.close();
    }
  };

  this.clean = function () {
    puzzle_module.removeListener('bonus-map-time', this.updateTime);
  };
});
