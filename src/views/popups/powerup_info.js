/* global style, ViewPool, Step, View, popup, i18n, config
*/

/* jshint ignore:start */
import ui.View as View;
import ui.ViewPool as ViewPool;
import src.views.step_powerup_info as Step;

import util.underscore as _;

/* jsio: if */
import src.modules.popup_manager as popup;
/* jsio: else */
import quest.modules.popup_manager as popup;
/* jsio: fi */

import DevkitHelper.style as style;
import DevkitHelper.i18n as i18n;
import resources.data.config as config;

/* jshint ignore:end */

exports = new (Class(View, function (supr) { // jshint ignore:line
  'use strict';

  this.prepare = function () {
    var opts = style.get('powerup_info_content');

    supr(this, 'init', [opts]);
    this.steps = new ViewPool({
      ctor: Step,
      initCount: 3
    });
  };

  this.build = function (powerup, model) {
    var step,
      is_ingame = model && _.has(config.powerups.ingame, powerup);

    this.prepare();
    _.each([1, 2, 3], bind(this, function (count) {
      step = this.steps.obtainView();
      step.build(powerup, count);
      this.addSubview(step);
    }));
    popup.add({
      id: 'powerup_info',
      popup_bg: 'large',
      type: 'powerup_info',
      close: true,
      title: i18n('powerup_' + powerup),
      body: this
    });
    if (is_ingame) {
      model.pauseTimers();
    }
    popup.next();
    if (is_ingame) {
      popup.on('powerup_info:close', function () {
        model.resumeTimers();
      });
    }
    return popup;
  };

  this.clean = function () {
    this.steps.releaseAllViews();
  };
}))();
