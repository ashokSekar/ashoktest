/* global ViewPool, ImageView, View, image, style,
  sounds, animate, test, timer */

/* jshint ignore:start */
import ui.ViewPool as ViewPool;
import quest.modules.image as image;
import ui.View as View;
import ui.ImageView as ImageView;
import DevkitHelper.style as style;
import util.underscore as _;
import animate;
import quest.modules.sounds as sounds;
import DevkitHelper.test as test;
import DevkitHelper.timer as timer;
/* jshint ignore:end */

exports = new (Class(View, function () { // jshint ignore: line
  'use strict';

  var max_level_stars = 3,
    is_prepared = false,
    no_of_stars = 0;

  this.prepare = function () {
    this.stars = new ViewPool({
      ctor: ImageView,
      initOpts: {
        centerAnchor: true
      },
      initCount: max_level_stars
    });

    this.inactive_stars = new ViewPool({
      ctor: ImageView,
      initCount: max_level_stars
    });
    is_prepared = true;
  };

  this.populateStars = function (level_stars, parent, type) {
    var base, star_view, star_inactive, star_active,
      star_width, star_height, cb,
      img_path = 'popup/',
      star_array = [],
      order = [1, 3, 2],
      anim_time = 300,
      max_delay = 800,
      delay_reducer = 1.5,
      showStar = function (index) {
        sounds.play('star_' + index);
        animate(star_array[index], 'start_animation')
          .now({
            scale: 1.5
          }, anim_time, animate.easeOut)
          .then({
            scale: 1
          }, anim_time, animate.easeIn)
          .then(cb);
      },
      jumpStars = function () {
        sounds.play('level_won');
        if (no_of_stars === max_level_stars) {
          _.times(max_level_stars, function (i) {
            timer.timeout('jump_stars_' + i, function () {
              animate(star_array[order[i] - 1], 'start_animation')
                .now({
                  y: -100
                }, anim_time, animate.easeInOutBounce)
                .then({
                  y: 0
                });
            }, 200 * i / delay_reducer);
          });
        }
      };

    no_of_stars = level_stars;
    cb = _.after(no_of_stars, bind(this, jumpStars));

    if (!is_prepared) {
      this.prepare();
    }
    this.updateOpts(style.get('star_box_' + type, {
      superview: parent
    }));
    _.times(max_level_stars, bind(this, function (i) {
      star_inactive = image.get(img_path + 'star-base');
      star_active = image.get(img_path + 'star');
      star_width = star_inactive.getWidth();
      star_height = star_inactive.getHeight();

      base = this.inactive_stars.obtainView({
        superview: this,
        image: star_inactive,
        width: star_width,
        height: star_height,
        anchorX: true,
        anchorY: true,
        scale: i !== 2 ? 0.8 : 1,
        x: 210 - star_width / 2,
        offsetY: i === 2 ? -55 : 0,
        order: order[i]
      });
      if (level_stars > 0) {
        star_view = this.stars.obtainView({
          superview: base,
          image: star_active,
          width: star_width,
          height: star_height,
          centerAnchor: true,
          x: 0,
          y: 0,
          scale: 0
        });
        star_array.push(star_view);
        if (type === 'gameover') {
          timer.timeout('show_stars_' + i, bind(null, showStar, i),
            max_delay * i / delay_reducer);
        } else {
          star_view.updateOpts({
            scale: 1
          });
        }
        level_stars -= 1;
      }
    }));

    test.prepare(this, {
      showStar: showStar,
      jumpStars: jumpStars,
      star_array: star_array
    });
  };

  this.clean = function () {
    var timeout_to_clear = [],
      anim_group = animate.getGroup('start_animation');

    if (anim_group) {
      anim_group.clear();
    }

    _.times(no_of_stars, bind(this, function (i) {
      timeout_to_clear.push('show_stars_' + i);
      timeout_to_clear.push('jump_stars_' + i);
    }));

    sounds.stop('level_won');
    timer.clearTimeout(timeout_to_clear);

    /* istanbul ignore else */
    if (is_prepared) {
      this.inactive_stars.releaseAllViews();
      this.stars.releaseAllViews();
      this.removeFromSuperview();
    }
  };
}))();
