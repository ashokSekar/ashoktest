/* global _, ImageView, image, BottomHudMap, test, utils*/

/* jshint ignore:start */
import util.underscore as _;

import ui.ImageView as ImageView;

import quest.modules.image as image;
import quest.views.bottom_hud_map as BottomHudMap;
import DevkitHelper.test as test;

/* jsio: if */
import src.modules.util as utils;
/* jsio: else */
import quest.modules.util as utils;
/* jsio: fi */

/* jshint ignore:end */

exports = Class(BottomHudMap, function (supr) {
  'use strict';

  var hud_items = ['settings', 'achievements', 'tile_store', 'messages'];

  this.init = function () {
    if (utils.getDeviceInfo('store') === 'web') {
      hud_items = _.without(hud_items, 'achievements');
    }
    supr(this, 'init', [hud_items]);
    this.notif_icon = new ImageView({
      superView: this
    });
    _.bindAll(this, 'build', 'resetNotif');
  };

  this.build = function () {
    var user = GC.app.user,
      notif_img = image.get('map_screen/notif');

    supr(this, 'build');
    this.updateOpts({
      offsetY: utils.getSafeArea().bottom
    });
    if (!user.get('tile_notif_opened')) {
      this.notif_icon.updateOpts({
        offsetY: 3,
        image: notif_img,
        width: notif_img.getWidth(),
        height: notif_img.getHeight(),
        visible: true
      });
      this.btn_tile_store.addSubview(this.notif_icon);
      this.on('hud-item-selected', this.resetNotif);
    } else {
      this.notif_icon.hide();
    }
  };

  this.resetNotif = function (item) {
    if (item === 'tile_store') {
      GC.app.user.set('tile_notif_opened', true);
      /* istanbul ignore else */
      if (this.notif_icon) {
        this.notif_icon.hide();
      }
    }
  };

  test.prepare(this, {
    hud_items: hud_items
  });
});
