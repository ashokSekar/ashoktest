/* global _, View, GridView, config_data, image,
  style, test, ViewPool, utils, Tile, animate, ImageView
*/

/* jshint ignore:start */
import util.underscore as _;
import ui.View as View;
import animate;
import ui.widget.GridView as GridView;
import ui.ViewPool as ViewPool;
import ui.ImageView as ImageView;

import resources.data.config as config_data;
import DevkitHelper.style as style;
import DevkitHelper.test as test;
import quest.modules.image as image;

/* jsio: if */
import src.modules.util as utils;
/* jsio: else */
import quest.modules.util as utils;
/* jsio: fi */

import src.views.tile as Tile;
/* jshint ignore:end */

exports = Class(View, function (supr) {
  'use strict';

  var tiles,
    config = config_data.board_data,
    getTile = function () {
      if (!tiles) {
        tiles = new ViewPool({
          ctor: Tile,
          initCount: config.rows * config.columns *
            config.depth
        });
      }
      return tiles;
    };

  test.prepare(this, {
    getTile: getTile,
    setTiles: function (val) {
      tiles = val;
    }
  });

  this.init = function () {
    supr(this, 'init', [style.get('grid', {
      width: style.base_width,
      padding: 0
    })]);

    _.bindAll(this, 'load', 'renderAllTiles',
      'renderTile', 'resize', 'updateSlot',
      'changeTileViews', 'updateTilePosition');

    /* TODO: Adding wrapper since resize breaks on increasing
     * grid width. Need to modify to calculate grid_view props in resize
     * and remove wrapper view.
     */
    this.wrapper = new View(style.get('grid', {
      superview: this,
      centerX: true,
      top: 0
    }));

    this.grid_view = new GridView({
      superview: this.wrapper,
      autoCellSize: false
    });

    this.tile_pool = getTile();

    this.open_slot = new ImageView({
      superview: this,
      inLayout: false,
      x: 500,
      y: -40
    });

    this.open_tile = new Tile(style.get('tile_size', {
      x: 10,
      y: 10,
      centerAnchor: true
    }));
    this.open_slot.addSubview(this.open_tile);
  };

  this.build = function (model) {
    var open_slot_img = image.get('puzzlescreen/open_slot'),
      slot_width = open_slot_img.getWidth();

    this.model = model;
    this.tiles = [];

    model.on('puzzle-loaded', this.load);

    // Render all tile corresponding to tile models
    model.on('render-tiles', this.renderAllTiles);

    // Render a tile for which model is passed
    model.on('render-tile', this.renderTile);
    model.on('tiles-update', this.changeTileViews);
    model.on('change:last_match', this.resize);
    model.on('layout-changed', this.updateTilePosition);
    model.on('change:slot_tile', this.updateSlot);
    this.open_slot.updateOpts({
      image: open_slot_img,
      height: open_slot_img.getHeight(),
      x: style.base_width - slot_width,
      y: utils.isiPhoneX() ? 20 : 0,
      width: slot_width,
      anchorX: slot_width,
      scale: 0.65
    });
    this.open_tile.hide();

    return this;
  };

  this.load = function () {
    var grid = this.grid_view,
      model = this.model,
      x = model.get('columns'),
      y = model.get('rows'),
      tile_prop = style.get('tile_size');

    if (model.get('open_slot')) {
      this.open_slot.show();
    } else {
      this.open_slot.hide();
    }

    grid.updateOpts({
      scale: 1,
      horizontalMargin: 0,
      verticalMargin: 0,
      width: x * tile_prop.width / 2,
      height: y * tile_prop.height / 2
    });

    grid.setCols(x);
    grid.setRows(y);
    grid.reflow();

    this.resize();
  };

  this.updateSlot = function (tile) {
    var coords = tile ? tile.getCoordinates() : null,
      open_slot = this.open_slot,
      open_tile = this.open_tile,
      def_scale = open_slot.style.scale,
      curr_view, view_pos, slot_pos;

    if (tile !== null) {
      curr_view = this.tiles[coords[0]][coords[1]][coords[2]];
      view_pos = curr_view.getPosition(this);
      slot_pos = open_tile.getPosition(this);

      curr_view.updateOpts({
        superview: this,
        x: view_pos.x,
        y: view_pos.y,
        offsetX: 0,
        offsetY: 0
      });

      animate(open_slot)
        .now({
          scale: 1
        }, 200, animate.linear);

      animate(curr_view)
        .now({
          x: slot_pos.x,
          y: slot_pos.y
        }, 200, animate.easeOut)
        .then(function () {
          tile.emit('clean');
          tile.removeAllListeners();

          open_slot.style.scale = def_scale;
          open_tile.build(tile, true);
          open_tile.updateOpts({
            visible: true,
            scale: 1,
            opacity: 1
          });
        });
    } else {
      open_tile.hide();
    }
  };

  this.getTilePosition = function (pos) {
    return this.tiles[pos[0]][pos[1]][pos[2]].getPosition(this.grid_view);
  };

  this.resize = function () {
    var model = this.model,
      pos = model.getOccupiedColumnsRows(),
      scale, center_x, center_y,
      grid_width = this.wrapper.style.width,
      grid_height = this.wrapper.style.height,
      scale_down = utils.isiPhoneX() ? 0.05 : 0,
      tile_prop = style.get('tile_size'),

      left_tile = pos.x.min.getCoordinates(),
      right_tile = pos.x.max.getCoordinates(),
      top_tile = pos.y.min.getCoordinates(),
      bottom_tile = pos.y.max.getCoordinates(),

      min_x = this.getTilePosition(left_tile).x,
      max_y = this.getTilePosition(top_tile).y,

      visible_width = tile_prop.width + this.getTilePosition(right_tile).x -
        this.getTilePosition(left_tile).x,
      visible_height = tile_prop.height + this.getTilePosition(bottom_tile).y -
        this.getTilePosition(top_tile).y;

    if (model.get('pending_matches') > 0) {
      return;
    }

    // min of scale x, scale y and 1
    scale = _.min([1, grid_width / visible_width - scale_down,
      grid_height / visible_height - scale_down]);

    center_x = visible_width / 2 + min_x;
    center_y = visible_height / 2 + max_y;

    this.grid_view.reflow();
    animate(this.grid_view).now({
        anchorX: center_x,
        anchorY: center_y,
        x: grid_width / 2 - center_x,
        y: grid_height / 2 - center_y,
        scale: scale
      }, 400, animate.linear)
      .then(function () {
        model.emit('resize-complete');
      });
  };

  this.renderAllTiles = function () {
    this.model.eachCell(this.renderTile);

    // Reset Touch Events
    this.setHandleEvents(true, false);
  };

  this.renderTile = function (model) {
    var coord = model.getCoordinates(),
      tile = this.tile_pool.obtainView(this.getTileProps(model));

    this.setTile(coord[0], coord[1], coord[2], tile);
    tile.build(model);
    this.changeTileViews([model]);
  };

  this.setTile = function (x, y, z, tile_view) {
    var tiles = this.tiles;

    if (!tiles[x]) {
      tiles[x] = [];
    }

    if (!tiles[x][y]) {
      tiles[x][y] = [];
    }

    tiles[x][y][z] = tile_view;
  };

  this.changeTileViews = function (tile_arr, keep_selected) {
    var puzzle = this.model,
      setState = function (item) {
        var model = _.isArray(item) ? puzzle.getTile(item) : item,
          state = 0;

        if (model) {
          state = keep_selected && model.get('state') === 2 ?
            2 : model.isFree() | 0;
          model.set('state', state);
        }
      };

    if (!tile_arr) {
      puzzle.eachCell(setState);
    } else {
      _.each(tile_arr, setState);
    }
  };

  this.updateTilePosition = function (resize) {
    var tiles = this.tiles;

    this.tiles = [];

    this.model.eachCell(bind(this, function (model, x, y, z) {
      var prev_pos = model.getPrevCordinates(),
        view = tiles[prev_pos[0]]
          [prev_pos[1]]
          [prev_pos[2]];

      view.updateOpts(this.getTileProps(model));
      this.setTile(x, y, z, view);
    }));

    // if last match not empty then change of last match
    // will automatically invoke resize
    if (resize) {
      this.resize();
    }
  };

  this.getTileProps = function (model) {
    var depth = this.model.get('depth'),
      x = model.get('x'),
      y = model.get('y'),
      z = model.get('z'),
      tile_prop = style.get('tile_size'),

      // so we adding some offset to each tile to make sure
      // tiles dont go out of bounds
      // model.getOffset('x') will give negative value and maximum offset
      // of left/top most tile would be equal to max_offset_x or y
      max_offset_x = (depth - 1) * tile_prop.gradient.x,
      max_offset_y = (depth - 1) * tile_prop.gradient.y;

    /*
      zIndex: tiles from left to right should have
        decreasing zIndex across the same layer,
        however going from top to bottom across the
        same layer zIndex should increase
        Across the layers the zIndex should increase
        from layer 0 to layer top

      An asset contains a 3D view of tile, with
      observer looking from bottom left w.r.t tile.

      offsetX: Along the positive x-axis, the image
        should shift left, an amount based on
        gradient-shadow of the asset which would
        be cumulatively added across tiles in a row
      offsetY: Similar to offsetX
    */

    return {
      superview: this.grid_view,
      height: tile_prop.height,
      width: tile_prop.width,
      row: y,
      col: x,
      centerAnchor: true,
      opacity: 1,
      scale: 1,
      zIndex: this.model.get('columns') * (z + 1) + y + x,
      offsetX: max_offset_x + model.getOffset('x'),
      offsetY: max_offset_y + model.getOffset('y')
    };
  };

  this.setHeight = function (val) {
    this.style.height = val;
    this.wrapper.style.height = val;
  };

  this.clean = function () {
    animate(this.grid_view).clear();
  };
});
