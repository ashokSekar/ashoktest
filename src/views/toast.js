/* global TextView, ImageScaleView, style, i18n, image, animate, ui */

/* jshint ignore:start */
import animate;

import ui.ImageScaleView as ImageScaleView;
import ui.TextView as TextView;
import DevkitHelper.style as style;
import DevkitHelper.i18n as i18n;
import quest.modules.image as image;
import DevkitHelper.style as ui;
/* jshint ignore:end */

exports = Class(ImageScaleView, function (supr) { // jshint ignore:line
  'use strict';

  this.init = function () {
    supr(this, 'init', [{
      layout: 'box',
      width: ui.base_width,
      height: ui.base_height,
      centerX: true,
      centerY: true,
      zIndex: 20,
      visible: false
    }]);
  };

  this.prepare = function () {
    this.toast_title = new TextView({
      superview: this
    });

    this.toast_content = new TextView({
      superview: this
    });
  };

  this.build = function (parent, text, opts) {
    var bg_img, toast_title, toast_content, bg_opts,
      content = opts ? opts.content : null;

    opts = opts || {};
    bg_img = opts.bg ? image.get(opts.bg) : null;

    if (this.getSubviews().length === 0) {
      this.prepare();
    }

    toast_title = this.toast_title;

    toast_title.updateOpts(style.get('toast_' + (opts.text_type || 'text'), {
      text: i18n('toast_' + text)
    }));

    if (content) {
      toast_content = this.toast_content;
      toast_content.updateOpts(
        style.get('toast_' + opts.content_type, {
          visible: true,
          text: i18n(content)
        }
      ));
    } else {
      this.toast_content.hide();
    }

    bg_opts = {
      centerY: true,
      superview: parent,
      image: bg_img,
      opacity: 1,
      height: bg_img ? bg_img.getHeight() : toast_title._opts.height,
      width: opts.width || ui.base_width,
      offsetX: opts.offsetX ? opts.offsetX * style.tablet_scale : 0,
      offsetY: opts.offsetY ? opts.offsetY * style.tablet_scale : 0
    };

    if (opts.y) {
      bg_opts.y = opts.y;
    }

    this.updateOpts(bg_opts);
    this.show();
  };

  this.onRelease = function () {
    this.setImage(null);
  };

  this.clean = function (timeout, delay, style) {
    animate(this, 'disappear')
      .wait(delay || 0)
      .then(style || {
        opacity: 0
      }, timeout || 0, animate.easeInCubic)
      .then(bind(this, function () {
        this.onRelease();
        this.removeFromSuperview();
      }));
  };
});
