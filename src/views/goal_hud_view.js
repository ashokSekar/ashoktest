/* global TextView, ScoreView, TextView, ImageView,
  style, i18n, image*/

/* jshint ignore:start */
import util.underscore as _;

import DevkitHelper.style as style;

import quest.modules.image as image;

import DevkitHelper.i18n as i18n;
import ui.ScoreView as ScoreView;
import ui.TextView as TextView;
import ui.ImageView as ImageView;

/* jshint ignore:end */

exports = Class(ImageView, function (supr) {
  'use strict';

  this.init = function () {
    supr(this, 'init');
    this.limit_view = new ScoreView({
      superview: this,
      layout: 'box'
    });

    this.tile = new ImageView({
      superview: this
    });

    this.limit_info = new TextView({
      superview: this
    });
  };

  this.build = function (type) {
    var limit_style = style.get('limit'),
      bg = image.get('puzzlescreen/goals_' + type);

    this.updateOpts(style.get('goals_hud_' + type, {
      image: bg,
      width: bg.getWidth(),
      height: bg.getHeight()
    }));
    this.limit_info.updateOpts(style.get('goal_extra_text_' + type));
    if (type === 'left') {
      this.limit_view.updateOpts(limit_style);
      this.limit_view.setCharacterData(limit_style.characterData);
    } else {
      this.tile.updateOpts({
        layout: 'box',
        right: 40,
        bottom: 15
      });
    }
    return this;
  };

  this.setType = function (type) {
    var tile_bg = _.find(['golden', 'lock_key'], function (variation) {
        return type === variation;
      }),
      face_path = tile_bg ? tile_bg + '_hud' : 'tile_hud',
      tile_img = image.get('puzzlescreen/' + face_path);

    this.tile.updateOpts({
      image: tile_img,
      width: tile_img.getWidth(),
      height: tile_img.getHeight()
    });
  };

  this.updateGoal = function (goal) {
    this.limit_info.setText(i18n('times_number_hud', [goal]));
  };
});
