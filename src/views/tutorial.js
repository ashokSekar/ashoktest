/* global image, ImageView, ImageScaleView, animate, style, i18n, View,
  ViewPool, ButtonView, device, timer, event_manager, TextView */

/* jshint ignore:start */
import util.underscore as _;

import ui.View as View;
import ui.TextView as TextView;
import ui.ImageView as ImageView;
import ui.ImageScaleView as ImageScaleView;
import ui.ViewPool as ViewPool;
import quest.ui.ButtonView as ButtonView;
import animate;
import device;

import DevkitHelper.style as style;
import quest.modules.image as image;

import DevkitHelper.i18n as i18n;
import DevkitHelper.timer as timer;
import DevkitHelper.event_manager as event_manager;
/* jshint ignore:end */

exports = new (Class(View, function (supr) { // jshint ignore:line
  'use strict';

  // size of middle part in the 9 slice image
  var center_data = {
      default: {
        width: 3,
        height: 3,
        left: 1,
        right: 1,
        top: 1,
        bottom: 1,
        x_offset: 0,
        y_offset: 0
      },
      plain: {
        width: 75,
        height: 75,
        left: 5,
        right: 5,
        top: 5,
        bottom: 5,
        x_offset: 0,
        y_offset: 0
      },
      match_pair: {
        width: 280,
        height: 180,
        left: 5,
        right: 5,
        top: 10,
        bottom: 10,
        x_offset: -20,
        y_offset: -20
      },
      match_only_free: {
        width: 405,
        height: 170,
        left: 15,
        right: 15,
        top: 10,
        bottom: 10,
        x_offset: -10,
        y_offset: -10
      },
      non_free_unmatch: {
        width: 428,
        height: 170,
        left: 12,
        right: 20,
        top: 5,
        bottom: 10,
        x_offset: -1,
        y_offset: -15
      },
      timer: {
        width: 196,
        height: 140,
        left: 5,
        right: 5,
        top: 5,
        bottom: 2,
        x_offset: -18,
        y_offset: -10
      },
      goal: {
        width: 196,
        height: 140,
        left: 5,
        right: 5,
        top: 5,
        bottom: 2,
        x_offset: -20,
        y_offset: -10
      },
      powerup: {
        width: 210,
        height: 190,
        left: 10,
        right: 10,
        top: 5,
        bottom: 5,
        x_offset: -50,
        y_offset: -40
      },
      pregame_powerup: {
        width: 175,
        height: 178,
        left: 6,
        right: 6,
        top: 4,
        bottom: 4,
        x_offset: -5,
        y_offset: -15
      },
      tile: {
        width: 140,
        height: 174,
        left: 12,
        right: 12,
        top: 5,
        bottom: 5,
        x_offset: -7,
        y_offset: -15
      },
      play: {
        width: 328,
        height: 122,
        left: 10,
        right: 10,
        top: 12,
        bottom: 12,
        x_offset: -10,
        y_offset: -8
      },
      two_tile: {
        width: 321,
        height: 238,
        left: 8,
        right: 8,
        top: 8,
        bottom: 8,
        x_offset: -15,
        y_offset: -20
      },
      bonus_icon: {
        width: 183,
        height: 181,
        left: 5,
        right: 5,
        top: 5,
        bottom: 5,
        x_offset: -18,
        y_offset: -18
      },
      hud_info: {
        width: 242,
        height: 105,
        left: 15,
        right: 15,
        top: 15,
        bottom: 15,
        x_offset: -12,
        y_offset: -12
      }
    },
    base_height, base_width, center, overlay_img;

  this.prepare = function () {
    _.bindAll(this, 'clean', 'onPause', 'onResume');

    base_height = style.base_height;
    base_width = style.base_width;

    this.updateOpts({
      layout: 'box',
      inLayout: false,
      scale: style.tablet_scale,
      zIndex: 1
    });

    this.overlay = new ImageScaleView({
      inLayout: false,
      opacity: 0,
      scaleMethod: '9slice'
    });

    this.image_main = new ImageView({
      superview: this,
      layout: 'box',
      centerX: true,
      centerY: true,
      scale: style.tablet_scale
    });

    this.image_info = new ImageView({
      superview: this,
      visible: false,
      layout: 'box',
      centerX: true,
      centerY: true
    });

    this.simple_message = new TextView(style.get('tutorial_simple_text', {
      superview: this,
      width: 510,
      centerX: true,
      height: 135
    }));

    this.main_text = new TextView(style.get('tutorial_msg_text', {
      superview: this.image_main
    }));

    this.main_text_extra = new TextView(style.get('tutorial_msg_text_extra', {
      superview: this.image_main
    }));

    this.main_text_img = new ImageView({
      superview: this.image_main,
      layout: 'box',
      centerX: true,
      centerY: true
    });

    this.info_text = new TextView(style.get('tutorial_info_text', {
      superview: this.image_info
    }));

    this.clickHandler = new View({
      superview: this.overlay,
      layout: 'box',
      centerX: true,
      centerY: true
    });

    this.button_pool = new ViewPool({
      ctor: ButtonView,
      initCount: 2,
      initOpts: {
        left: 5,
        centerY: true
      }
    });

    this.buttons = new View({
      superview: this.image_main,
      layout: 'linear',
      justifyContent: 'start',
      centerX: true,
      bottom: -57,
      scale: style.tablet_scale
    });

    this.hand = new ImageView({
      superview: this.overlay,
      layout: 'box',
      zIndex: 1000,
      visible: false,
      centerX: true,
      centerY: true
    });

    this.animate_hand = animate(this.hand);
  };

  this.setup = function () {
    _.bindAll(this, 'clean', 'onPause', 'onResume');

    this.overlay = new ImageScaleView({
      inLayout: false,
      opacity: 0,
      scaleMethod: '9slice'
    });

    this.clickHandler.updateOpts({
      superview: this.overlay
    });

    this.hand = new ImageView({
      superview: this.overlay
    });

    this.animate_hand = animate(this.hand);

    this.image_main.updateOpts({
      centerX: true,
      centerY: true
    });

    this.image_info.updateOpts({
      visible: false,
      centerX: true,
      centerY: true
    });

    this.simple_message.updateOpts(style.get('tutorial_simple_text'));

    this.main_text.updateOpts(style.get('tutorial_msg_text', {
      superview: this.image_main
    }));

    this.main_text_extra.updateOpts(style.get('tutorial_msg_text_extra', {
      superview: this.image_main,
      offsetX: 0,
      offsetY: 0
    }));

    this.info_text.updateOpts(style.get('tutorial_info_text', {
      superview: this.image_info,
      offsetX: 0,
      offsetY: 0
    }));
  };

  this.build = function (params) {
    var opts = params || {};

    overlay_img = opts.overlay || 'default';
    center = center_data[overlay_img];

    if (this.getSubviews().length === 0) {
      this.prepare();
    } else {
      this.setup();
    }

    return this;
  };

  this.show = function (opts, timeout) {
    var tutorial = GC.app.tutorial;

    if (timeout) {
      timer.timeout('tutorialview_show', bind(this, 'show', opts), timeout);
      return;
    }

    supr(this, 'show');
    if (!opts) {
      return;
    }

    this.setupOverlay(opts);
    this.setupView(opts);
    tutorial.on('pause', this.onPause);
    tutorial.on('resume', this.onResume);
  };

  this.setupOverlay = function (opts) {
    var v_scale = 1,
      view = opts.view,
      parent = opts.superview,
      action = opts.action || {},
      action_context = action.context instanceof View ? action.context : null,
      overlay_image = overlay_img === 'plain' ? null :
        'resources/images/tutorial/overlay_' + overlay_img + '.png',
      getSize = function (max, dimen) {
        return max - center[dimen] * v_scale / 2;
      };

    // If overlay is disabled
    if (opts.disable_overlay) {
      return;
    }

    /*  If there is action associated with the lightbox we need to
        setup clickHandler for the action
    */
    if (opts.action) {
      this.setupClickHandler(opts);
    }

    /*  If we are highlighting a view we need to scale the lightbox
        according to the view
    */
    if (view && view.getPosition) {
      v_scale = view.getPosition(GC.app.view).scale;
    }

    this.overlay.updateOpts({
      superview: parent,
      image: overlay_image,
      x: -base_width + (opts.x + center.width / 2 - center.left +
        center.x_offset) * v_scale,
      y: -base_height + (opts.y + center.height / 2 - center.top +
        center.y_offset) * v_scale,
      width: base_width * 2,
      height: base_height * 2,
      sourceSlices: {
        horizontal: {
          left: center.left,
          center: center.width,
          right: center.right
        },
        vertical: {
          top: center.top,
          middle: center.height,
          bottom: center.bottom
        }
      },
      destSlices: {
        horizontal: {
          left: getSize(base_width, 'width'),
          center: center.width * v_scale,
          right: getSize(base_width, 'width')
        },
        vertical: {
          top: getSize(base_height, 'height'),
          middle: center.height * v_scale,
          bottom: getSize(base_height, 'height')
        }
      }
    });

    this.clickHandler.updateOpts({
      width: action_context && action_context.getWidth ?
        action_context.getWidth() + 5 : center.width,
      height: action_context && action_context.getHeight ?
        action_context.getHeight() + 5 : center.height
    });
  };

  this.setupClickHandler = function (opts) {
    var clickHandler = this.clickHandler,
      action = opts.action,

      // InputDrag is a custom event and not part of GameClosure
      evt_type = _.contains(['InputSelect', 'InputDrag'], action.evt) ?
        action.evt : 'InputSelect',
      evt_listen = evt_type === 'InputDrag' ? 'InputStart' : evt_type,
      onAction = bind(this, function (func, parameters) {
        // Close the tutorial and apply the function and emit next
        // to show next tutorial
        this.clean();
        this.emit('next');
        action.context[func].apply(action.context, parameters);
      });

    clickHandler.on(evt_listen, function (evt) {
      var func = action.func || 'onInputSelect',
        parameters = action.parameters || [];

      event_manager.emit('tutorial-action', {
        id: 'center',
        data: {
          tut_id: opts.id
        }
      });

      if (evt_type === 'InputDrag') {
        clickHandler.startDrag({
          inputStartEvt: evt,
          radius: 10
        });
        clickHandler.onDragStop = function () {
          onAction(func, parameters);
        };
      } else {
        onAction(func, parameters);
      }
    });

    /*  Disable events initially
        Should be enabled after all the animation is complete
    */
    clickHandler.setHandleEvents(false);
    clickHandler.show();
  };

  // jshint maxcomplexity:21
  this.setupView = function (opts) {
    var msg_bg = opts.msg_text ? image.get('tutorial/text_bg_' +
        opts.msg_text) : null,
      info_img = opts.info_image ? image.get('tutorial/' + opts.id) : null,
      main_text_img = opts.main_text_img ?
        image.get('tutorial/' + opts.main_text_img) : null,
      extra = opts.extra || {},
      custom = opts.custom || {},
      main_text_opts = extra.main_text || {},
      main_text_extra_opts = extra.main_text_extra || {},
      info_text_opts = extra.info_text || {},
      info_img_opts = extra.info_image || {},
      main_text_img_opts = extra.main_text_img,
      main_img_width = msg_bg ? msg_bg.getWidth() : 0,
      main_img_height = msg_bg ? msg_bg.getHeight() : 0,
      hand = custom.drag_target ? image.get('tutorial/overlay_hand') : null,
      target_style;

    this.tut_id = opts.id;
    if (opts.no_image) {
      this.updateOpts({
        scale: style.tablet_scale,
        width: base_width,
        height: 0
      });
    } else if (opts.msg_text) {
      this.updateOpts({
        scale: 1,
        width: main_img_width,
        height: main_img_height
      });

      this.image_main.updateOpts({
        visible: true,
        image: msg_bg,
        scale: extra.scale === false ? 1 : style.tablet_scale,
        width: main_img_width,
        height: main_img_height
      });

      this.main_text.updateOpts(merge({
        visible: true,
        text: opts.powerup_count ?
          i18n('tutorial_' + opts.id, [opts.powerup_count]) :
          i18n('tutorial_' + opts.id)
      }, main_text_opts));

      this.main_text_extra.updateOpts(merge({
        visible: !!opts.text_extra,
        text: i18n('tutorial_' + opts.text_extra)
      }, main_text_extra_opts));

      if (main_text_img) {
        this.main_text_img.updateOpts(merge({
          visible: true,
          image: main_text_img,
          height: main_text_img.getHeight(),
          width: main_text_img.getWidth()
        }, main_text_img_opts));
      }

      if (info_img) {
        this.image_info.updateOpts(merge({
          visible: true,
          image: info_img,
          width: info_img.getWidth(),
          height: info_img.getHeight(),
          scale: style.tablet_scale
        }, info_img_opts));
      }

      this.info_text.updateOpts(merge({
        visible: !!opts.text_info,
        text: i18n('tutorial_info_' + opts.text_info)
      }, info_text_opts));
    }
    if (hand) {
      this.hand.updateOpts({
        image: hand,
        width: hand.getWidth(),
        height: hand.getHeight(),
        visible: true
      });
      target_style = opts.custom.drag_target.getPosition();
      target_style.x = target_style.x / target_style.scale;
      target_style.y = target_style.y / target_style.scale;
      target_style.width = target_style.width / target_style.scale;
      target_style.height = target_style.height / target_style.scale;
      opts.custom.drag_target = target_style;
      this.animateHand(opts);
    }
    this.startAnimation(opts);
  };

  this.startAnimation = function (opts) {
    this.animateMainImage(opts);
    if (!opts.disable_overlay) {
      animate(this.overlay)
        .now({
          opacity: 1
        }, 500);
    }
  };

  this.animateMainImage = function (opts) {
    var clickHandler = this.clickHandler,
      pool = this.button_pool,
      button_actions = opts.actions,
      img_style = this.image_main.style,
      img_height = img_style.height,
      parent = opts.superview,
      message_id = opts.message,
      current_scale = this.style.scale,
      y = opts.y / style.tablet_scale,
      height = opts.height / style.tablet_scale,
      buttons = this.buttons,
      buttons_height = 10,
      buttons_width = 0,
      extra = opts.extra || {},
      message_opts = extra.message_text,
      info_style = this.image_info.style,
      info_y, info_height, parent_y, img_y, msg_y;

    if (info_style.visible) {
      info_y = info_style.offsetY;
      info_height = info_style.height;
    }

    if (button_actions) {
      _.each(button_actions, bind(this, function (action) {
        var btn_imgs = image.button('tutorial/' + (action.style || 'default')),
          title = action.title,
          up_btn = btn_imgs.up,
          width = up_btn.getWidth(),
          height = up_btn.getHeight(),
          btn_style = style.get(this.id + '_' + action.id),
          text = merge(btn_style.text, {
            layout: 'box',
            centerX: true,
            width: width,
            height: height,
            size: 40,
            fontFamily: 'sansita-italic',
            color: '#FFFFFF',
            offsetY: -5,
            strokeColor: '#60A710',
            strokeWidth: 8
          }),
          btn;

        buttons_height += height;
        buttons_width = Math.max(buttons_width, width);
        delete action.style;
        delete action.title;
        btn = pool.obtainView(merge(btn_style, {
          superview: this.buttons,
          images: btn_imgs,
          width: width,
          centerX: true,
          height: height,
          text: text,
          on: {
            up: bind(this, this.onAction, action)
          }
        }));

        btn.setState(ButtonView.states.UP);
        btn.setTitle(i18n('story_' + title));
      }));
    } else if (!opts.action && !opts.disable_overlay) {
      timer.timeout('tutorialview_events', bind(this, function () {
        this.overlay.once('InputSelect', bind(this, this.onAction, false));
        this.once('InputSelect', bind(this, this.onAction, false));
      }), opts.min_time || 0);

      if (opts.max_time) {
        timer.timeout('tutorialview_action', bind(this, this.onAction, false),
          opts.max_time);
      }
    }

    buttons.updateOpts({
      visible: !!button_actions,
      width: buttons_width,
      height: buttons_height
    });

    if (opts.stick_to_lightbox) {
      img_y = opts.y + opts.height;
    } else if (opts.center) {
      img_y = base_height / 2 - img_height / 2;
    } else if (opts.offsetY) {
      img_y = base_height / 2 - img_height / 2 + opts.offsetY;
    } else if (opts.no_image) {
      img_y = 0;
    } else {
      img_y = y + height >= base_height / 2 ?
        150 : base_height - 150 - img_height;
    }

    parent_y = img_y < 0 ? 0 : img_y + img_height > base_height ?
       base_height - (device.isTablet ? img_height + 10 : 400) : img_y;

    // If info image is going out of screen, Repositioning it to bottom of
    // screen. Expecting y position of info image set using offsetY.
    if (info_y && parent_y + info_y + info_height > base_height) {
      this.image_info.updateOpts({
        offsetY: base_height - parent_y - info_height - 30
      });
    }

    this.repositionImageInfo(parent_y);

    this.updateOpts({
      superview: parent,
      y: parent_y,
      x: -base_width
    });

    animate(this)
      .now({x: base_width / 2 - this.style.width * current_scale / 2}, 300)
      .then(bind(this, function () {
        var min_yoffset = device.isTablet ?
          this.simple_message.style.height * style.scale_height : 250;

        msg_y = (this.simple_message.style.height + img_y) *
            style.scale_height * style.tablet_scale;
        this.simple_message.updateOpts(merge(message_opts, {
          text: i18n(message_id),
          visible: !!message_id,
          y: base_height - (msg_y > min_yoffset ? msg_y : min_yoffset) +
            (opts.message_offset || 0)
        }));
      }))
      .then(bind(this, function () {
        // Enabling events after the animation is complete
        clickHandler.setHandleEvents(true, false);
        this.overlay.setHandleEvents(true, false);
        this.setHandleEvents(true, false);
      }));
  };

  this.animateHand = function (opts) {
    var click_handler_style = this.clickHandler.style,
      target_style = opts.custom.drag_target,
      style_x = click_handler_style.x + click_handler_style.width / 2,
      style_y = click_handler_style.y + click_handler_style.height / 2,
      x_diff = target_style.x - click_handler_style.x + target_style.width / 2,
      y_diff = target_style.y - click_handler_style.y + target_style.height / 2;

    // TODO: Getting target style position using absolute positions on screen
    this.animate_hand
    .now({
      x: style_x,
      y: style_y
    }, 1000, animate.easeInOut)
    .then({
      x: style_x + x_diff,
      y: style_y + y_diff
    }, 750)
    .then(this.animateHand.bind(this, opts));
  };

  this.repositionImageInfo = function (main_img_y) {
    var img_info_style = this.image_info.style,
      img_info_offset = img_info_style.offsetY,
      img_info_height = img_info_style.height;

    // Repositioning image_info to bottom of screen if it goes beyond
    if (img_info_offset && main_img_y + img_info_offset +
      img_info_height > base_height) {
      img_info_style.offsetY = base_height - main_img_y - img_info_height - 10;
    }
  };

  this.onAction = function (data) {
    if (timer.hasTimeout('tutorialview_action')) {
      timer.clearTimeout('tutorialview_action');
    }

    this.setHandleEvents(false, true);
    this.overlay.setHandleEvents(false, true);
    this.clickHandler.setHandleEvents(false, true);
    event_manager.emit('tutorial-action', {
      id: 'story_clicked',
      data: {
        tut_id: this.tut_id
      },
      param: data
    });

    if (!data) {
      this.clean();
      this.emit('next');
    } else {
      GC.app.tutorial.emit(this.id + ':' + data.id);
      if (data.close) {
        this.clean();
        this.emit('next');
      }
    }
  };

  this.onPause = function () {
    var overlay = this.overlay;

    this.hide();
    overlay.hide();
  };

  this.onResume = function () {
    var overlay = this.overlay;

    this.show();
    overlay.show();
  };

  this.finish = function (view, cb) {
    if (view) {
      view.setHandleEvents(true, false);
    }
    if (cb) {
      cb();
    }
  };

  this.clean = function () {
    var tutorial = GC.app.tutorial;

    if (this.getSubviews().length === 0) {
      return;
    }

    if (timer.hasTimeout('tutorialview_action')) {
      timer.clearTimeout('tutorialview_action');
    }

    tutorial.removeListener('resume', this.onResume);
    tutorial.removeListener('pause', this.onPause);

    this.image_main.hide();
    this.image_info.hide();
    this.main_text.hide();
    this.main_text_img.hide();
    this.hand.hide();
    this.animate_hand.clear();
    this.button_pool.releaseAllViews();

    this.removeFromSuperview();

    this.overlay.removeAllSubviews();
    this.overlay.removeFromSuperview();
    this.overlay.removeAllListeners('InputSelect');
    this.clickHandler.removeAllListeners('InputSelect');
    this.clickHandler.removeAllListeners('InputStart');
    this.removeAllListeners('InputSelect');
    this.clickHandler.removeAllListeners('InputMove');
  };

  // resetting
  // jshint maxcomplexity:15
}))();
