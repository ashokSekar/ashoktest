/* global _, PowerupView */

/* jshint ignore:start */
import util.underscore as _;
import src.views.powerup as PowerupView;

/* jshint ignore:end */

exports = Class(PowerupView, function (supr) {
  'use strict';

  this.init = function (opts) {
    supr(this, 'init', [opts]);
    _.bindAll(this, 'setCount');
  };

  this.build = function (powerup, model) {
    this.model = model;
    if (powerup === 'shuffle') {
      model.on('change:free_shuffle', this.setCount);
    }
    supr(this, 'build', [powerup, model]);
  };

  this.setCount = function () {
    var count = GC.app.user.get('inventory_' + this.name);

    if (this.name === 'shuffle') {
      count = count + (this.model.get('free_shuffle') > 0 ? 0 : 1);
    }
    supr(this, 'setCount', [count]);
  };

  this.onRelease = function () {
    if (this.name === 'shuffle') {
      this.model.removeListener('change:free_shuffle', this.setCount);
    }
    supr(this, 'onRelease');
  };
});
