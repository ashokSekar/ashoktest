/* global util, _, config_data, ab_testing */

/* jshint ignore:start */
import util.underscore as _;

import quest.modules.util as util;

import resources.data.config as config_data;
import quest.modules.ab_testing as ab_testing;
/* jshint ignore:end */

exports = function (supr) {
  'use strict';

  this.init = function () {
    supr(this, 'init', [merge({
      puzzle_loads: [],
      tooltip_index: 0,
      motivation_msg_index: 0,
      tile_sets: ['classic'],
      curr_tile_set: 'classic',
      tile_notif_opened: false,
      unlocked_powerups: [],
      last_powerup_info: [0, ''],
      screen_bonus: false,
      launched_bonus_map: [],
      last_played_won: false // currenlty using in bonus map only
    }, _.reduce(util.getPowerups(), function (res, val, pow) {
      res['inventory_' + pow] = 0;

      return res;
    }, {}))]);
  };

  this.onSync = function (resp) {
    var user_resp = resp.user;

    if (this.get('social_facebook') && user_resp) {
      this.set('tile_sets', _.union(this.get('tile_sets'),
        resp.user.tile_sets));
      this.set('unlocked_powerups', _.union(this.get('unlocked_powerups'),
        resp.user.unlocked_powerups));
    }

    supr(this, 'onSync', [resp]);
  };

  this.getPowerupForInfo = function (type) {
    var user = this,
      curr_ms = user.getMs('curr'),
      unlocked_powerups = user.get('unlocked_powerups'),
      powerup_info = user.get('last_powerup_info'),
      last_unlocked = unlocked_powerups[unlocked_powerups.length - 1];

    if (last_unlocked && last_unlocked !== powerup_info[1] &&
      _.contains(_.keys(util.getPowerups(type)), last_unlocked) &&
      curr_ms - powerup_info[0] > 5) {
      user.set('last_powerup_info', [curr_ms, last_unlocked]);
      return last_unlocked;
    }
  };

  this.getLivesGenTime = function () {
    var remote_val = ab_testing.get('lives_gen'),
      lives_gen = remote_val ? parseInt(remote_val, 10) * 60000 :
        config_data.lives.gen;

    return lives_gen;
  };
};
