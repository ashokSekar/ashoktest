/* global Model, util, _, style, config */

/* jshint ignore:start */

import resources.data.config as config;

import DevkitHelper.model as Model;
import quest.modules.util as util;
import util.underscore as _;
import DevkitHelper.style as style;
/* jshint ignore:end */

exports = Class(Model, function (supr) {
  'use strict';

  this.init = function () {
    supr(this, 'init');

    _.bindAll(this, 'pick', 'showHint');
  };

  this.onObtain = function () {
    var obj = {
      x: null,
      y: null,
      z: null,
      face: null,
      free: false,
      real_face: null,
      prev_face: null,
      /*
          hidden = true + face_visibilty = true- Tapping
            again to make visible is possible
          hidden = true + face_visibilty = false- Tapping
            again to make invisible is possible
          hidden = false + face_visibilty =
            true/false- Toggling visibility not possible
      */

      face_visibility: true,
      hidden: false,
      tag: null,
      /*
        type
          0 - blocked tile
          1 - open tile
          2 - tile currently selected,
          3 - tile hinted
      */
      state: 0
    };

    this.set(obj, true);
  };

  this.build = function (opts) {
    this.set(opts);
  };

  this.getCoordinates = function () {
    return [this.get('x'),
            this.get('y'),
            this.get('z')];
  };

  this.getPrevCordinates = function () {
    var props = ['x', 'y', 'z'],
      cords = [];

    _.each(props, bind(this, function (cord) {
      var val = this.getPrevious(cord);

      cords.push(_.isNull(val) ? this.get(cord) : val);
    }));

    return cords;
  };

  this.getNeighbours = function (position_exclude) {
    /*
      wrt any tile at position (x,y,z), its neighbour can be
        left -> just immediate left or left shifted down by on position,
          so its coordinate will be so adjusted,
        right -> same a above,
        above -> four neighbours are possible
          1. just above,
          2. above and shifted coordinate to the right,
          3, above and shifted coordinate to the right and
              another position to down in THAT same layer,
          4. above and shifted one position below the org tile
        below -> same as above, but for a change in z-axis
    */
    var adjs = {
        left: [[-2, -1, 0], [-2, 0, 0], [-2, 1, 0]],
        right: [[2, -1, 0], [2, 0, 0], [2, 1, 0]],
        above: [[-1, -1, 1], [0, -1, 1], [1, -1, 1],
                [-1, 0, 1], [0, 0, 1], [1, 0, 1],
                [-1, 1, 1], [0, 1, 1], [1, 1, 1]],
        below: [[-1, -1, -1], [0, -1, -1], [1, -1, -1],
                [-1, 0, -1], [0, 0, -1], [1, 0, -1],
                [-1, 1, -1], [0, 1, -1], [1, 1, -1]]
      },
      ret_obj = {},
      position = this.getCoordinates();

    _.mapObject(adjs, function (val, key) {
      var ret = [];

      if (_.indexOf(position_exclude, key) >= 0) {
        return;
      }
      _.each(val, function (coord) {
        ret.push(util.getOffsetPosition(position, coord));
      });

      ret_obj[key] = ret;
    });
    return ret_obj;
  };

  this.isFilled = function () {
    return this.get('face') ? true : false;
  };

  this.match = function (model) {
    // will return undefined if model not passed
    return model && _.isEqual(this.get('face'),
      model.get('face')) && !this.isJoker();
  };

  this.getJokerFace = function () {
    var joker_face = config.special_faces.joker,
      type = this.get('puzzle').get('type');

    return this.get('hidden') && !this.get('face_visibility') ?
      type + '_' + joker_face : joker_face;
  };

  this.isJoker = function () {
    return this.get('face') === this.getJokerFace();
  };

  this.isGoldenTile = function () {
    return this.get('face') === config.special_faces.golden;
  };

  this.isLock = function () {
    var face = this.get('face');

    return _.isString(face) && _.contains(config.special_faces.lock, face);
  };

  this.isKey = function () {
    var tag = this.get('tag');

    return _.isString(tag) && tag.search(config.key_prefix) !== -1;
  };

  this.showHint = function (show) {
    this.set('state', show === false ? 1 : 3);
  };

  this.getOffset = function (type) {
    /*
      Given an x or y (type). Compute offset by this formula
      (z + type/2) * gradient

      For eg:
        if gradient is 10 px (for x & y), and a tile has been
      placed [0,0,0]. A tile is to be placed at beside it
      at [2,0,0], it should be shifted by gradient pixels to
      the left of its original placement to compensate for the
      fact that a tile occupies 2 rows/ 2 columns.
    */
    return -(this.get('z') + this.get(type) / 2) *
      style.get('tile_size').gradient[type];
  };

  this.isFree = function () {
    return this.get('puzzle').isFreeTile(this);
  };

  this.isSlot = function () {
    return this.get('puzzle').isOpenSlotTile(this);
  };

  this.getActualFace = function () {
    var face = this.get('face');

    return _.contains(config.special_faces, face) ?
      this.get('real_face') : face;
  };

  this.isVisible = function () {
    var puzzle = this.get('puzzle'),
      cooridnates = this.getCoordinates(),
      hiding_coord = [cooridnates[0], cooridnates[1], cooridnates[2] + 1];

    if (puzzle.tileAt(hiding_coord)) {
      return false;
    }
    return true;
  };

  this.isVisibleCompletely = function () {
    var puzzle = this.get('puzzle'),
      cooridnates = this.getCoordinates(),
      x_cord = cooridnates[0],
      y_cord = cooridnates[1],
      neighbours = puzzle.getNeighbours(this, ['left', 'right', 'below'], true);

    return !_.find(neighbours, function (tile) {
      return Math.abs(tile[0] - x_cord) < 2 && Math.abs(tile[1] - y_cord) < 2;
    });
  };

  this.pick = function () {
    var puzzle = this.get('puzzle');

    // This checking is to avoid tap going to negative
    // When taping multiple tile together
    if (puzzle.get('limit_type') === 'tap' &&
      puzzle.get('limit') <= puzzle.get('tap')) {
      return true;
    }

    puzzle.pickTile(this);
  };

  this.onRelease = function () {
    this.emit('clean');

    supr(this, 'onRelease', []);
  };
});
