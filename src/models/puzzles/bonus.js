/* global PuzzleModel, puzzle_module */

/*jshint loopfunc: true */

/* jshint ignore:start */
import util.underscore as _;
import src.models.puzzle as PuzzleModel;
import quest.modules.puzzle as puzzle_module;
/* jshint ignore:end */
exports = Class(PuzzleModel, function (supr) {
  'use strict';

  this.init = function () {
    supr(this, 'init');
    _.bindAll(this, 'gameOver');
  };

  this.initializeData = function (data) {
    var free_shuffle = 0;

    if (data.type === 'untimed') {
      data.limit_type = 'untimed';
      data.untimed = data.total_tiles;
      this.set({
        untimed: 0
      });
      free_shuffle = 1;
    }
    supr(this, 'initializeData', [data]);
    this.set({
      limit_given: this.get('limit'),
      free_shuffle: free_shuffle
    }, true);
  };

  this.gameOver = function (type, cb) {
    var user_puzzle = puzzle_module.getUserBonusPuzzle(),
      won = type === true,
      max_ms = user_puzzle.max_ms,
      curr_ms = user_puzzle.curr_ms,
      score;

    GC.app.user.set('last_played_won', won);

    supr(this, 'gameOver', [type, cb, false]);
    if (won) {
      if (max_ms === curr_ms) {
        user_puzzle.max_ms = max_ms + 1;
      }
      score = this.get('score');
      if (!user_puzzle[curr_ms] ||
        user_puzzle[curr_ms].score < score) {
        user_puzzle[curr_ms] = {};
        user_puzzle[curr_ms].score = score;
        user_puzzle[curr_ms].stars = this.get('stars');
        user_puzzle[curr_ms].best_score = score;
      }
      puzzle_module.setUserBonusPuzzle(user_puzzle);
    }
    this.savePuzzleData(type);

    cb();
  };

  this.getBestScore = function () {
    var user_puzzle = puzzle_module.getUserBonusPuzzle(),
      curr_ms = user_puzzle.curr_ms,
      curr_ms_data = user_puzzle[curr_ms] || {};

    return curr_ms_data.best_score || curr_ms_data.score || 0;
  };

  this.savePuzzleData = function (exit_type, extras) {
    supr(this, 'savePuzzleData', [exit_type, _.union(extras,
      ['limit_given'])]);
  };
});
