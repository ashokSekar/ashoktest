/* global _, sounds, config, BonusModel */

/*jshint loopfunc: true */

/* jshint ignore:start */
import util.underscore as _;
import resources.data.config as config;
import src.models.puzzles.bonus as BonusModel;
import quest.modules.sounds as sounds;
/* jshint ignore:end */
exports = Class(BonusModel, function (supr) {
  'use strict';

  this.initializeData = function (data) {
    supr(this, 'initializeData', [data]);

    this.set({
      tile_per_match: 3,
      reduced_family: data.reduced_family
    });

    this.updatePairs();
  };

  this.updatePairs = function () {
    var families = this.get('families'),
      getSum = function () {
        return _.reduce(families, function (memo, curr) {
          return memo + (curr ? curr.pairs : 0);
        }, 0);
      },
      curr_pairs = getSum(),
      to_pairs = curr_pairs * 2 / 3,
      to_ratio = to_pairs / curr_pairs,
      curr_idx = 0,
      curr_family;

    _.each(families, function (curr) {
      if (curr) {
        curr.pairs = Math.floor(curr.pairs * to_ratio);
      }
    });

    curr_pairs = getSum();

    while (curr_pairs < to_pairs) {
      curr_family = families[curr_idx++];
      /* istanbul ignore else */
      if (curr_family) {
        curr_family.pairs = curr_family.pairs + 1;
        curr_pairs++;
      }
    }

    _.each(families, function (curr, key) {
      if (curr && curr.pairs <= 0) {
        families[key] = null;
      }
    });
  };

  // This function may generate unsolvable puzzle for three match
  // TODO: update puzzle generation logic for three match
  this.generateSolvableGame = function (tile_faces) {
    var resp = supr(this, 'generateSolvableGame', [tile_faces]);

    return resp && this.isBoardSolvable();
  };

  this.matchTile = function (curr_selected) {
    var selected_tiles = _.map(this.get('tile_selected')),
      changed_tiles = [],
      is_joker_match = false,
      face;

    this.cleanHinted();

    selected_tiles.push(curr_selected);
    _.find(selected_tiles, function (model) {
      if (model.isJoker()) {
        is_joker_match = true;
      } else if (is_joker_match && face && face !== model.get('face')) {
        is_joker_match = false;
        return true;
      } else {
        face = model.get('face');
      }
    });

    if (selected_tiles.length === 1) {
      this.set('tile_selected', selected_tiles);
    } else if (!selected_tiles[0].match(curr_selected) &&
      !curr_selected.isJoker() && !is_joker_match) {
      this.wrongMatch(selected_tiles);
    } else {
      // Required here to trigger change event for 3rd tile pick
      this.set('tile_selected', selected_tiles);
      if (selected_tiles.length === 3) {
        this.push('solved', curr_selected.get('face'));
        this.set('tile_selected', []);
        this.emit('tiles-matched');

        _.find(selected_tiles, bind(this, function (tile) {
          if (tile.isJoker()) {
            changed_tiles = this.updateJokerPair(selected_tiles);
            return true;
          }
        }));

        this.tileMatched(selected_tiles, changed_tiles);
      }
    }
  };

  this.pickTile = function (curr_selected) {
    var selected_tiles = this.get('tile_selected'),
      swap_mode = this.get('mode') === 'swap';

    if (swap_mode && selected_tiles) {
      selected_tiles = [selected_tiles];
    }

    if (this.isSameTile(selected_tiles, curr_selected)) {
      return false;
    }
    if (this.get('pick_tile')) {
      sounds.play('tap');
      if (this.get('mode') === 'swap' && curr_selected.isVisible()) {
        this.swapTiles(curr_selected, selected_tiles ?
          selected_tiles[0] : null);
      } else if (curr_selected.isFree()) {
        this.matchTile(curr_selected);
      }
    }
  };

  this.wrongMatch = function (tiles) {
    _.each(tiles, function (curr) {
      curr.emit('wrong-match-animate');
    });
    this.set('tile_selected', [tiles.pop()]);
  };

  this.isSameTile = function (selected_tiles, curr_selected) {
    return _.find(selected_tiles, function (tile) {
      return _.isEqual(tile.getCoordinates(), curr_selected.getCoordinates());
    });
  };

  this.highlight = function (tile_models) {
    var prev;

    if (this.get('mode') !== 'swap') {
      prev = this.getPrevious('tile_selected');
      prev = prev && !_.isArray(prev) ? [prev] : prev;
      _.each(prev, function (curr) {
        curr.set('state', 1);
      });
      _.each(tile_models, function (curr) {
        curr.set('state', 2);
      });
    }
  };

  this.beforePowerup = function () {
    // If tile_selected has property hidden should set visibility as false,
    // Ignoring for now since no memory match variation in three match
    this.cleanHinted();
  };

  this.undo = function () {
    var existing_stack = _.clone(this.get('last_match')),
      last_match = existing_stack.pop(),
      tiles_to_load = last_match.slice(0, this.get('tile_per_match')),
      neighbours = [],
      tile_one = last_match[0],
      tile_two = last_match[1],
      tile_three = last_match[2],
      joker_face = config.special_faces.joker,
      changed_tiles = last_match.slice(3, last_match.length),
      tile, neighbour;

    this.beforePowerup();

    // Add tile to model
    this.loadTiles(tiles_to_load);

    // Update dependency
    this.placeTile(this.getTile(tile_one), tile_one[3]);
    this.placeTile(this.getTile(tile_two),
      tile_two[3] || tile_one[3]);
    this.placeTile(this.getTile(tile_three),
      tile_three[3] || tile_one[3]);

    if (tile_one[3] === joker_face) {
      this.getTile(tile_one).set('real_face', tile_one[4]);
      this.increment('no_of_jokers');
    }

    if (tile_two[3] === joker_face) {
      this.getTile(tile_two).set('real_face', tile_two[4]);
      this.increment('no_of_jokers');
    }

    if (tile_three[3] === joker_face) {
      this.getTile(tile_three).set('real_face', tile_three[4]);
      this.increment('no_of_jokers');
    }

    _.each(changed_tiles, bind(this, function (tile_data) {
      this.changeFace(this.getTile(tile_data), tile_data[3]);
    }));

    this.increment('undo');
    this.increment('tile_count', 3);
    this.pop('solved');

    _.each(tiles_to_load, function (coord) {
      tile = this.getTile(coord);
      neighbour = this.getNeighbours(tile, [], true);

      this.emit('render-tile', tile);

      neighbours = _.union(neighbours, neighbour);
    }, this);

    this.set('tile_selected', null);
    this.set('last_match', existing_stack);
    this.emit('tiles-update', neighbours);
  };

  this.allocateTiles = function () {
    var unique_tiles = supr(this, 'allocateTiles'),
      grouped_tiles = _.groupBy(unique_tiles),
      grouped_faces = _.shuffle(_.keys(grouped_tiles)),
      reduced_count = Math.floor(grouped_faces.length *
        this.get('reduced_family')),
      reduced_tiles = [];

    grouped_faces = grouped_faces.slice(0, reduced_count);

    _.each(grouped_faces, function (curr_face) {
      reduced_tiles = reduced_tiles.concat(grouped_tiles[curr_face]);
    });

    /* istanbul ignore next */
    while (reduced_tiles.length < unique_tiles.length) {
      reduced_tiles.push(_.first(_.shuffle(grouped_faces)));
    }

    return reduced_tiles;
  };

  this.randomiseFaces = function () {
    var tile_faces = [],
      uniq_faces = this.allocateTiles(),
      total_tiles = this.get('tile_count');

    // Shuffle
    uniq_faces = _.shuffle(uniq_faces);

    _.find(uniq_faces, function (val) {
      tile_faces.push(val);
      tile_faces.push(val);
      tile_faces.push(val);

      return tile_faces.length === total_tiles;
    });

    return tile_faces;
  };

  this.getPairWithJoker = function (open_tiles) {
    var pairs = [],
      keys = _.keys(open_tiles),
      joker_face = config.special_faces.joker,
      tile_per_match = this.get('tile_per_match');

    if (open_tiles[joker_face]) {
      pairs.push(open_tiles[joker_face].pop());
      while (pairs.length < tile_per_match) {
        _.find(keys, function (key) {
          if (open_tiles[key].length >= tile_per_match - pairs.length &&
            key !== joker_face) {
            _.each(open_tiles[key], function (tile) {
              pairs.push(tile);
            });
            return true;
          }
        });

        if (pairs.length < tile_per_match &&
          open_tiles[joker_face].length > 0) {
          pairs.push(open_tiles[joker_face].pop());
        } else {
          break;
        }
      }
    }

    return pairs.length >= this.get('tile_per_match') ? pairs : [];
  };

  this.afterSwooshAnimate = function (pair, changed_tiles) {
    var last_match,
      silent = true,
      first = pair[0],
      second = pair[1],
      third = pair[2],
      first_coord = first.getCoordinates(),
      second_coord = second.getCoordinates(),
      third_coord = third.getCoordinates(),
      existing_stack = _.clone(this.get('last_match')) || [];

    last_match = this.populateLastMatch([first, second, third], changed_tiles);

    if (this.isOpenSlotTile(first_coord) || this.isOpenSlotTile(second_coord) ||
      this.isOpenSlotTile(third_coord)) {
      existing_stack = [];
    } else {
      existing_stack.push(last_match);
    }

    this.set('last_match', existing_stack);
    this.cleanTile(first);
    this.cleanTile(second);
    this.cleanTile(third);
    if (this.get('swoosh_pairs').length > 0) {
      _.delay(this.swooshAnimate, 200);
    } else {
      if (this.get('swooshed_count') ===
        config.swoosh_pairs) {
        this.set('swooshed_count', 0);
        this.emit('powerup-applied', 'swoosh');
        silent = false;
        this.set('shuffle_game', true);
      } else if (this.get('tile_count') > 0) {
        this.once('resize-complete', this.swoosh, true);
      }
      this.updateGameState([], false, silent);
    }
  };

  this.setHintAsSelected = function (model) {
    this.set('tile_selected', [model], true);
  };

  this.savePuzzleData = function (exit_type) {
    supr(this, 'savePuzzleData', [exit_type, ['reduced_family']]);
  };
});
