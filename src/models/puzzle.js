/* global _, ModelPool, util, array, TileModel, sounds,
  config, timer, PuzzleModel, test, Promise, loading, puzzle_module,
  event_manager
*/

/*jshint loopfunc: true */

/* jshint ignore:start */
import quest.lib.bluebird as Promise;
import quest.utils.array as array;
import resources.data.config as config;

import util.underscore as _;
import quest.modules.util as util;
import quest.models.puzzle as PuzzleModel;
import DevkitHelper.timer as timer;
import DevkitHelper.loading as loading;
import src.models.tile as TileModel;
import DevkitHelper.modelpool as ModelPool;
import quest.modules.sounds as sounds;
import DevkitHelper.test as test;
import DevkitHelper.event_manager as event_manager;
import quest.modules.puzzle as puzzle_module;

/* jshint ignore:end */

exports = Class(PuzzleModel, function (supr) {
  'use strict';

  var memory_variation,
    tiles,
    timers = ['puzzle', 'auto_hint'],
    load_start_time = 0,
    curr_attempt = 0,
    partial_memory_variation,
    getTileModel = function () {
      if (!tiles) {
        tiles = new ModelPool({
          ctor: TileModel,
          initCount: 150
        });
      }
      return tiles;
    };

  this.init = function () {
    supr(this, 'init');

    this.tile = getTileModel();
    this.first_tap = bind(this, this.logEvent, 'first-tap');
    this.first_pair_match = bind(this, this.logEvent, 'first-pair-match');
    memory_variation = config.variations.memory.id;
    partial_memory_variation = config.variations.partial_memory.id;

    _.bindAll(this, 'selectCoordinates', 'onlyFreeInLine', 'setPending',
      'startTimers', 'gameOver', 'hint', 'halfHint', 'autoHalfHint',
      'shuffle', 'undo', 'onTapChange',
      'highlight', 'magnet', 'applyMagnet', 'swoosh', 'swooshAnimate',
      'updateGameState', 'onGeneratedSolvable', 'createLoadArray',
      'onShuffleGenerate', 'shuffleAsync', 'substitute', 'swap',
      'doSwap', 'openSlot', 'beforeTutorial', 'afterTutorial');

    test.prepare(this, {
      getTileModel: getTileModel,
      setTiles: function (val) {
        tiles = val;
      },
      setAttempt: function (val) {
        curr_attempt = val;
      }
    });
  };

  this.build = function () {
    // Handle Addition of listeners
    this.on('shuffle', this.shuffle);
    this.on('hint', this.hint);
    this.on('half_hint', this.halfHint);
    this.on('magnet', this.magnet);
    this.on('swoosh', this.swoosh);
    this.on('substitute', this.substitute);
    this.on('swap', this.swap);
    this.on('flip', this.flip);
    this.on('undo', this.undo);
    this.on('open-slot', this.openSlot);
    this.on('change:tile_selected', this.highlight);
    this.on('change:ongoing', this.setPending);
    this.on('before-tutorial', this.beforeTutorial);
    this.on('after-tutorial', this.afterTutorial);
    this.on('change:tap', this.onTapChange);
    this.on('change:collected_golden', this.checkGoals);
    if (GC.app.user.getMs('max') === 1) {
      this.once('change:tile_selected', this.first_tap);
      this.once('tiles-matched', this.first_pair_match);
    }
    supr(this, 'build', [{}]);
  };

  this.initializeData = function (data) {
    var limit_type = data.limit_type || 'time',
      obj = {
        tiles: [[]],
        total_tiles: data.total_tiles,
        tile_count: data.layout.length,
        last_match: [],
        families: data.families,
        rows: 0,
        columns: 0,
        depth: 0,
        time: 0,
        tiles_placings: 0,
        load_attempts: 0,
        stars: 0,
        ongoing: 0,
        solved: [],
        free_shuffle: 0,
        undo: 0,
        hinted: [],
        swoosh_pairs: [],
        pending_matches: 0,
        swooshed_count: 0,
        shuffle_game: false,
        mode: null,
        slot_tile: null,
        collected_golden: 0,
        given_golden: data.goal ? data.goal.golden : 0,
        type: data.type || 'classic',
        limit_type: limit_type,
        limit: data[limit_type],
        tap: 0,
        jewels: data.type === config.variations.jewels.id ?
          _.shuffle(config.jewels) : [],
        jewel_order: [],
        tile_set: GC.app.user.get('curr_tile_set'),
        pick_tile: true,
        tile_per_match: 2,
        goal: data.goal,
        no_of_jokers: config.jokers_per_level,
        bonus_map: data.bonus_map,
        lock_keys: data.lock_keys,
        joker_indexes: null,
        lock_key_indexes: null,
        golden_tile_position: null,
        jewels_positions: null
      };

    _.each(_.keys(util.getPowerups('ingame')), function (curr) {
      obj[curr] = 0;
    });

    _.each(_.keys(util.getPowerups('pregame')), function (curr) {
      obj[curr] = data[curr];
      if (data[curr]) {
        GC.app.user.decrement('inventory_' + curr);
      }
    });

    this.set(obj, true);

    // initializes tile model with puzzle data
    this.loadTiles(data.layout);
  };

  this.loadPuzzle = function (data) {
    var init = data.init,
      limit_type, load_promise;

    this.initializeData(data);

    // assign face to tiles
    if (init.length > 0) {
      this.initFaces(init);
      load_promise = Promise.resolve(true);
    } else {
      load_promise = this.assignFaces(null);
    }

    return load_promise
      .then(bind(this, function (success) {
        this.createLoadArray(success, 'load', curr_attempt);

        if (success) {
          limit_type = this.get('limit_type');
          this.emit('render-tiles');
          if (limit_type === 'time' || limit_type === 'untimed') {
            this.startTimers();
          }
          if (GC.app.user.get('infinite_lives_remaining') <= 0) {
            GC.app.user.decrement('inventory_lives');
          }

          if (this.get('auto_hint')) {
            this.autoHalfHint();
          }
          supr(this, 'loadPuzzle', [data]);

          return success;
        } else {
          GC.app.user.set('puzzle_ongoing', false);
          this.emit('load-failed', false);
          loading.hide();

          return Promise.reject();
        }
      }));
  };

  this.openSlot = function (tile) {
    if (this.get('slot_tile') === null) {
      sounds.play('open_slot');
      this.beforePowerup();
      this.set('tile_selected', null);
      this.set('slot_tile', tile);
      this.set('last_match', []);
      this.updateGameState([], this.getNeighbours(tile, [], true));
    }
  };

  this.getNoOfLocks = function () {
    var locks_no = 0,
      lock_key = this.get('lock_keys');

    _.each(lock_key, function (data) {
      locks_no = locks_no + data[1];
    });
    return locks_no;
  };

  /* @Override */
  this.getLimit = function () {
    return this.get('limit_type');
  };

  this.setPending = function (ongoing) {
    if (ongoing !== 0) {
      return;
    }

    if (this.get('pending_last_match')) {
      this.set('last_match', this.get('pending_last_match'));
      this.unset('pending_last_match');
    }
  };

  this.eachCell = function (cb) {
    _.find(this.get('tiles'), function (xymodel, z) {
      return _.find(xymodel, function (ymodel, x) {
        return _.find(ymodel, function (model, y) {
          if (model) {
            return cb(model, x, y, z);
          }
        });
      });
    });
  };

  this.eachCellByLayer = function (cb) {
    var tiles = this.get('tiles');

    _.find(tiles, function (xymodel, z) {
      return _.find(xymodel, function (ymodel, x) {
        return _.find(ymodel, function (model, y) {
          if (model) {
            return cb(model, x, y, z);
          }
        });
      });
    });
  };

  // jshint maxdepth:4
  this.eachTileFromTop = function (cb) {
    var tiles = this.get('tiles'),
      max_y = this.get('rows'),
      x, y, z, xtiles, ytiles;

    // Getting top most tiles
    for (z = tiles.length; z >= 0; z--) {
      ytiles = tiles[z] || [];

      // Getting upper half tiles
      for (y = 0; y <= max_y; y++) {
        xtiles = ytiles[y] || [];
        for (x = 0; x <= xtiles.length; x++) {
          if (xtiles[x] && cb(xtiles[x])) {
            return true;
          }
        }
      }
    }
  };

  this.loadTiles = function (tile_map) {
    var tile_obj = this.get('tiles'),
      rows = this.get('rows'),
      columns = this.get('columns'),
      depth = this.get('depth');

    _.each(tile_map, function (arr) {
      var x = arr[0],
        y = arr[1],
        z = arr[2],
        model = this.tile.obtainModel();

      // Set to tile model
      model.build({
        x: x,
        y: y,
        z: z,
        puzzle: this
      });

      // Set rows, columns, depth of board
      columns = Math.max(x, columns);
      rows = Math.max(y, rows);
      depth = Math.max(z, depth);

      // initialize empty tile-obj pos
      if (!tile_obj[z]) {
        tile_obj[z] = [];
      }

      if (!tile_obj[z][x]) {
        tile_obj[z][x] = [];
      }

      // Refence to puzzle stored in x, y, z
      tile_obj[z][x][y] = model;
    }, this);

    /* A tile at (x, y) is on 1/4 th of a tile at also includes
      positions (x + 1, y), (x + 1, y + 1), (x, y + 1). So for a tile at
      the end of a row/column, we will need to increment value of
      rows/columns to represent it better
    */
    if (rows !== this.get('rows')) {
      rows += 2;
    }

    if (columns !== this.get('columns')) {
      columns += 2;
    }

    if (depth !== this.get('depth')) {
      depth += 1;
    }

    this.set({
      tiles: tile_obj,
      rows: rows,
      columns: columns,
      depth: depth
    }, true);
  };

  this.initFaces = function (init_pos) {
    _.each(init_pos, bind(this, function (curr_pos) {
      var curr_face = curr_pos.pop();

      this.placeTile(this.getTile(curr_pos), curr_face, null, true);
    }));
  };

  this.assignFaces = function (tile_faces) {
    curr_attempt = 0;
    load_start_time = Date.now();
    this.set({
      shuffle_faces: tile_faces,
      loading_elapsed: 0
    });

    return new Promise(bind(this, function (resolve) {
      this.once('tiles-placed', resolve);
      this.attemptPuzzleGenerate();
    }));
  };

  /*
    Bootstrap to assign faces to the tiles
    Max attempts is set to 64
  */
  this.attemptPuzzleGenerate = function () {
    var attempts_allowed = config.puzzle_load.retry;

    this.set('loading_elapsed',
      Math.floor((Date.now() - load_start_time) / 1000));

    if (++curr_attempt > attempts_allowed) {
      this.emit('tiles-placed', false);
    } else {
      /*
        Initialise the faces to allocate. For the classic
        dragon board there are 144 tiles. So we allocate and
        randomise the assignment of 144 tiles. If there are > 144
        tiles we will reallocate and re-randomise as we run out.
        One advantage of this method is that the pairs to assign are
        non-linear. In kmahjongg 0.4, If there were > 144 the same
        allocation series was followed. So 154 = 144 + 10 rods.
        184 = 144 + 40 rods (20 pairs) which overwhemed the board
        with rods and made deadlock games more likely.
      */
      this.set('load_attempts', curr_attempt);
      this.generateSolvableGameAsync(this.get('shuffle_faces') ||
          this.randomiseFaces())
        .then(this.onGeneratedSolvable);
    }
  };

  this.onGeneratedSolvable = function (success) {
    if (success) {
      this.emit('tiles-placed', true);
    } else {
      this.attemptPuzzleGenerate();
    }
  };

  this.generateSolvableGameAsync = function (tile_faces) {
    return new Promise(bind(this, function (resolve) {
      _.defer(bind(this, function () {
        resolve(this.generateSolvableGame(tile_faces));
      }));
    }));
  };

  /*
    Golden tile is assigned to last layered locked and invisible tile
    priority wise locked tile = 1 invisible tile = 2
    priority 2 means both constraints need to be there and so on
  */
  this.assignGoldenTile = function (ignore_pos) {
    var priority_tiles = [[], [], []],
      findPriorityTiles = function (tiles_arr) {
        return _.find(tiles_arr, function (tiles) {
          return !_.isEmpty(tiles);
        });
      };

    this.eachCellByLayer(bind(this, function (model, x, y, z) {
      if (z === 0) {
        if (!_.isEqual(model.getCoordinates(), ignore_pos) &&
          !this.isOpenSlotTile(model)) {
          if (!this.isFreeTile(model) && !model.isVisible()) {
            priority_tiles[0].push(model);
          }
          if (!this.isFreeTile(model)) {
            priority_tiles[1].push(model);
          }
          priority_tiles[2].push(model);
        }
      } else {
        return true;
      }
    }));
    return _.sample(findPriorityTiles(priority_tiles));
  };

  // TODO: To shift to utils.js
  this.getRandomNo = function (multiplier, exclude) {
    var random_no;

    exclude = exclude || [];
    do {
      random_no = Math.floor(Math.random() * multiplier);
    } while (_.contains(exclude, random_no));
    return random_no;
  };

  this.getJokerIndexes = function (tile_count) {
    var joker_indexes = [],
      no_of_jokers = this.get('no_of_jokers'),
      limit = tile_count - 1;

    if (this.get('joker')) {
      /*
        TODO: logic to place joker in proper layers
      */
      while (joker_indexes.length < no_of_jokers) {
        joker_indexes.push(this.getRandomNo(limit, joker_indexes));
      }
    }

    return joker_indexes;
  };

  this.updateSpecialTileIndexes = function (tile_count) {
    var joker_indexes = [],
      jewels = this.get('jewels'),
      jewel_order, available_jewels, lock_key_indexes;

    if (!this.get('joker_indexes')) {
      joker_indexes = this.getJokerIndexes(tile_count);
      this.set('joker_indexes', joker_indexes);
    }

    if (!this.get('lock_key_indexes')) {
      lock_key_indexes =
        this.getLockAndKeyIndexes(joker_indexes, tile_count);
      this.set('lock_key_indexes', lock_key_indexes);
    }

    if (jewels && !this.get('jewels_positions')) {
      jewel_order = this.get('jewel_order');
      available_jewels = _.difference(jewels, jewel_order);
      this.set('jewels_positions',
        this.getJewelIndexes(available_jewels, tile_count));
    }
  };

  this.updateSpecialTilePositions = function (position, golden_tile_placed) {
    var is_golden_tile_variation =
      this.get('given_golden') - this.get('collected_golden') > 0;

    if (is_golden_tile_variation && !golden_tile_placed) {
      this.set('golden_tile_position', this.assignGoldenTile(position));
    }
  };

  this.afterPuzzleGenerate = function () {
    this.set({
      joker_indexes: null,
      lock_key_indexes: null,
      golden_tile_position: null,
      jewels_positions: null
    });

    // For lock and key need to check is Solvable after rendering
    return !this.get('lock_keys') || this.isBoardSolvable();
  };

  /*
    Core logic to attempt to assign face to tiles
  */
  this.generateSolvableGame = function (tile_faces) {
    var last_position, position, position2, face, golden_tile_update,
      golden_tile, slot_pair, slot_real_face, jewels_indexes,
      lock_key_info, joker_indexes, lock_key_indexes,
      is_joker,
      face_visibility = true,
      i = 0,
      hidden = false,
      tile_count = this.get('tile_count'),
      tiles = this.flattenTiles(),
      flatten = this.flattenTiles('layer', 0),
      slot_tile = this.get('slot_tile'),
      slot_face = slot_tile ? slot_tile.get('face') : null,
      jewels = this.get('jewels'),
      jewel_order = this.get('jewel_order'),
      special_faces = _.clone(config.special_faces),
      joker_face = special_faces.joker,
      tile_per_match = this.get('tile_per_match'),
      golden_face = special_faces.golden,
      golden_pair = true,
      golden_tile_placed = false,
      tag = null,
      hidden_tiles = [],
      type = this.get('type'),
      is_memory_match = type === config.variations.memory.id,
      is_partial_memory_match = type ===
        partial_memory_variation,
      partially_hidden_number = is_partial_memory_match ?
        config.partial_memory_ratio * tile_count : 0,
      available_jewels = _.difference(jewels, jewel_order),
      golden_pair_count = this.get('given_golden') -
        this.get('collected_golden'),
      is_golden_tile_variation = golden_pair_count > 0;

    _.each(special_faces.lock, function (lock_face) {
      special_faces[lock_face] = lock_face;
    });

    while (hidden_tiles.length < partially_hidden_number) {
      hidden_tiles.push(this.getRandomNo(tile_count, hidden_tiles));
    }

    tile_faces = _.clone(tile_faces);

    this.updateSpecialTileIndexes(tile_count);

    joker_indexes = this.get('joker_indexes');
    lock_key_indexes = this.get('lock_key_indexes');
    jewels_indexes = _.clone(this.get('jewels_positions'));

    // Temporary fix for not generating solvable game with less tiles
    // and overlaping configuration.
    // TODO: Modify logic so that same logic handles all cases.
    if (tile_count <= 6) {
      if (slot_tile) {
        slot_face = slot_tile.getActualFace();
        tile_faces.splice(tile_faces.indexOf(slot_face), 1);

        tiles = _.filter(tiles, function (curr) {
          return !curr.isSlot();
        });
      }
      tiles = _.sortBy(tiles, function (curr) {
        return curr.isFree() ? 0 : 1;
      });
      _.each(tiles, bind(this, function (tile, idx) {
        is_joker = false;
        face = tile_faces[idx];
        tag = null;
        tile.set('face', face, available_jewels.pop());
        tile.set('real_face', null);

        if (_.contains(joker_indexes, idx)) {
          tile.set('real_face', face);
          tile.set('face', joker_face);
          is_joker = true;
        }
        if (golden_pair_count > 0) {
          if (!golden_tile && !is_joker) {
            tile.set('real_face', face);
            tile.set('face', golden_face);
            golden_tile = tile;
          } /* istanbul ignore else */ else if (golden_tile &&
            golden_tile.get('real_face') === face) {
            tile.set('real_face', face);
            tile.set('face', golden_face);
            --golden_pair_count;
          }
        } else if (this.get('lock_keys') && !is_joker) {
          lock_key_info = this.placeLockAndKey(lock_key_indexes,
            idx, tile.getCoordinates(), face, tag);
          face = lock_key_info[0];
          tag = lock_key_info[1];
          if (_.contains(special_faces.lock, face)) {
            tile.set('real_face', tile_faces[idx]);
          }
          tile.set('face', face);
          tile.set('tag', tag);
        }
      }));

      return this.afterPuzzleGenerate();
    }

    // Reset attempts
    this.set('tiles_placings', 0);

    /*
      Attempt face assigning after clearing
      any old data of the tiles
    */
    this.eachCell(bind(this, function (model) {
      if (this.isOpenSlotTile(model)) {
        if (model.get('tag')) {
          available_jewels = _.without(available_jewels, model.get('tag'));
          jewels_indexes.pop();
        }
        if (model.isJoker()) {
          joker_indexes.pop();
        }
      } else {
        model.set({
          face: null,
          tag: null,
          free: false,
          real_face: null,
          face_visibility: true,
          hidden: false
        });
      }
    }));

    // Attempts to compute free tiles in a line
    _.each(_.shuffle(flatten), function (model) {
      if (this.onlyFreeInLine(model.getCoordinates())) {
        model.set('free', true);
      }
    }, this);

    last_position = null;
    position = null;
    position2 = null;

    if (slot_face) {
      position = this.selectCoordinates(null, tiles);
      slot_pair = this.getTile(position);
      slot_real_face = slot_face;

      if (_.contains(special_faces, slot_face)) {
        slot_real_face = slot_tile.get('real_face');

        if (slot_face === special_faces.joker ||
          _.contains(special_faces.lock, slot_face)) {
          slot_face = slot_real_face;
        } /* istanbul ignore else */ else if (slot_face === special_faces
          .golden) {
          /* This case will make sure that if golden tile in openslot, its
           * pair will also be updated as golden tile with proper real_face */
          golden_tile_placed = true;
          slot_pair.set('real_face', slot_tile.get('real_face'));
        }
      }

      tile_faces.splice(tile_faces.indexOf(slot_real_face), 1);
      tile_faces.splice(tile_faces.indexOf(slot_real_face), 1);
      this.placeTile(slot_pair, slot_face);
      tile_count = tile_count - 2;
    }

    this.updateSpecialTilePositions(position, golden_tile_placed);

    golden_tile = this.get('golden_tile_position');

    // Real assigning begins here
    for (i = 0; i < tile_count; ++i) {
      // Of a pair, if it is 1st one reset last_position count
      if (i % tile_per_match === 0) {
        last_position = null;
      }

      // Randomly select a positition which is free for a tile
      // If lastPositon != null, then select a position based
      // on last_position
      position = this.getPosition(last_position, tiles, tile_count, i);
      if (!position) {
        return false;
      }
      face = tile_faces[i % 144];
      if (is_golden_tile_variation && !golden_tile_placed) {
        golden_tile_update = this.placeGolden(golden_tile, face, position,
          last_position, golden_pair);
        face = golden_tile_update[0];
        golden_tile_placed = golden_tile_update[1];
        golden_pair = golden_tile_update[2];
      }
      tag = null;
      if (!_.isEmpty(jewels_indexes) && _.last(jewels_indexes) === i &&
        _.contains(available_jewels,
          available_jewels[jewels_indexes.length - 1])) {
        tag = available_jewels[jewels_indexes.length - 1];
        jewels_indexes.pop();
      }

      lock_key_info = this.placeLockAndKey(lock_key_indexes,
          i, position, face, tag);
      face = lock_key_info[0];
      tag = lock_key_info[1];

      // TODO: If position of golden tile and joker gets conflict, we should
      // try to render joker in some other position. Right now Joker is ignored.
      if (!_.contains(special_faces, face) && _.contains(joker_indexes, i)) {
        this.getTile(position).set('real_face', face);
        face = joker_face;
      }

      if (_.contains(hidden_tiles, i) || is_memory_match) {
        hidden = true;
        face_visibility = false;
      } else {
        face_visibility = true;
        hidden = false;
      }

      // Set face at that position
      this.placeTile(this.getTile(position), face, tag, face_visibility,
        hidden);

      // Remember last position
      last_position = position;
    }

    return this.afterPuzzleGenerate();
  };

  this.placeLockAndKey =
    function (lock_key_indexes, index, position, face, tag) {
    var lock_index, key_index,
      lock_faces = config.special_faces.lock;

    if (this.get('lock_keys')) {
      lock_index = _.findIndex(lock_key_indexes.locks, {
        position: index
      });
      if (lock_index >= 0) {
        this.getTile(position).set('real_face', face);
        face = lock_faces[lock_key_indexes.locks[lock_index].type];
      }

      key_index = _.findIndex(lock_key_indexes.keys, {
        position: index
      });
      if (key_index >= 0) {
        tag = config.key_prefix + lock_key_indexes.keys[key_index].type;
      }
    }

    return [face, tag];
  };

  this.placeGolden =
    function (golden_tile, face, position, last_position, golden_pair) {
    var golden_tile_placed = false,
      golden_face = config.special_faces.golden,
      last_tile;

    /* If position of golden tile is same as currpos update
     * the last_positioned tile too */
    if (_.isEqual(golden_tile.getCoordinates(), position)) {
      this.getTile(position).set('real_face', face);
      face = golden_face;
      if (!last_position) {
        golden_pair = false;
      } else {
        last_tile = this.getTile(last_position);
        last_tile.set('real_face',
          last_tile.get('real_face') || last_tile.get('face'));
        last_tile.set('face', golden_face);
        golden_tile_placed = true;
      }
    }
    if (!golden_pair && last_position) {
      this.getTile(position).set('real_face', face);
      face = golden_face;
      golden_pair = true;
      golden_tile_placed = true;
    }

    return [face, golden_tile_placed, golden_pair];
  };

  this.getPosition = function (last_position, tiles, tile_count, curr_index) {
    var position, position2;

    position = this.selectCoordinates(last_position, tiles);
    if (!position) {
      return false;
    }

    // Attempt again, if we get a better position
    if (curr_index < tile_count - 1) {
      position2 = this.selectCoordinates(last_position, tiles);
      if (!position2) {
        return false;
      }

      // Switch tile if new position is higher than older one
      if (this.getTile(position2).get('z') >
        this.getTile(position).get('z')) {
        position = position2;
      }
    }

    return position;
  };

  this.getJewelIndexes = function (available_jewels, range) {
    var jewels = this.get('jewels'),
      jewels_indexes = [],
      offset = Math.floor(range / jewels.length),
      min;

    while (jewels_indexes.length < available_jewels.length) {
      min = range;
      range = range - offset;
      jewels_indexes.push(Math.floor(Math.random() * (range - min)) + min);
    }

    return jewels_indexes;
  };

  /* lock_key data will be an array each element represent
    one color of key and lock info. ex: [0 ,1 ,2] will be one item
    0 represent color, 1 is number of locks, 2 is no of keys
    This function find random indexes to place lock and key
  */
  this.getLockAndKeyIndexes = function (joker_tiles, tile_count) {
    var lock_n_keys = {
      locks: [],
      keys: []
    },
    i = 0,
    lock_keys = this.get('lock_keys'),
    lock_conf = [],
    key_conf = [],
    exceptions = _.clone(joker_tiles),
    slot_tile = this.get('slot_tile'),
    random_no, ignore_type;

    if (lock_keys) {
      /*
        TODO: logic to place lock n key in proper layers
      */

      _.each(lock_keys, function (lock_key) {
        for (i = 0; i < lock_key[1]; i++) {
          lock_conf.push(lock_key[0]);
        }
        for (i = 0; i < lock_key[2]; i++) {
          key_conf.push(lock_key[0]);
        }
      });
      if (slot_tile) {
        if (tile_count > 6) {
          tile_count = tile_count - 2;
        } else {
          tile_count = tile_count - 1;
        }
        if (slot_tile.isKey()) {
          ignore_type = parseInt(slot_tile.get('tag')
            .replace(config.key_prefix, ''), 10);
          key_conf.splice(key_conf.indexOf(ignore_type), 1);
        }
      }

      while (lock_n_keys.locks.length < lock_conf.length) {
        random_no = this.getRandomNo(tile_count, exceptions);

        lock_n_keys.locks.push({
          position: random_no,
          type: lock_conf[lock_n_keys.locks.length]
        });
        exceptions.push(random_no);
      }

      while (lock_n_keys.keys.length < key_conf.length) {
        random_no = this.getRandomNo(tile_count, exceptions);
        lock_n_keys.keys.push({
          position: random_no,
          type: key_conf[lock_n_keys.keys.length]
        });
        exceptions.push(random_no);
      }
    }

    return lock_n_keys;
  };

  /*
    This function returns if a tile at a given position
    is the only one free in that horizontal line
  */
  this.onlyFreeInLine = function (position, direction) {
    var key, neighbour,
      lr_obj = {
        left: [],
        right: []
      },
      tile = this.getTile(position);

    if (tile.get('free') || tile.isFilled()) {
      return false;
    }

    if (direction) {
      lr_obj = _.pick(lr_obj, direction);
    }

    for (key in lr_obj) {
      tile = this.getTile(position);
      do {
        lr_obj = this.getNeighbours(tile, ['below', 'above']);

        neighbour = lr_obj[key];
        if (neighbour[0] && neighbour[2] &&
          (!this.onlyFreeInLine(neighbour[0], key) ||
          !this.onlyFreeInLine(neighbour[2], key))) {
          return false;
        }

        tile = this.getTile(_.compact(neighbour)[0]);
        if (tile && !this.isOpenSlotTile(tile) &&
          (tile.get('free') || tile.isFilled())) {
          return false;
        }
      } while (tile);
    }
    return true;
  };

  this.flattenTiles = function (type, val) {
    var tiles = this.get('tiles'),
      look, ret_obj = [];

    // We can add further funtion where
    // we can return all tiles in a particular row
    // not needed as of now
    switch (type) {
      case 'layer':
        look = tiles[val];
        ret_obj = _.flatten(look);
        break;

      case 'custom':
        ret_obj = _.flatten(_.values(val), true);
        break;

      default:
        ret_obj = _.flatten(tiles);
        break;
    }
    return _.without(ret_obj, null, undefined);
  };

  /*
    Returns a random position to assign on the board
  */
  this.selectCoordinates = function (last_position, tiles) {
    var i, neighbour,
      good_position = false,
      tile_model = null,
      tile_count = this.get('tile_count'),
      lastPositionCheck = function (coord) {
        return _.isEqual(coord, last_position);
      };

    for (i = 0; i < tile_count && !good_position; i++) {
      this.increment('tiles_placings');

      // Get a random position to place tile which is free
      tile_model = _.find(_.shuffle(tiles), function (model) {
        if (model.get('free')) {
          return true;
        }
      });
      if (!tile_model) {
        return null;
      }

      good_position = true;

      // When we need to place tile based on a previous tile
      if (last_position) {
        neighbour = this.getNeighbours(tile_model, ['above'], true);

        // When considering position , we look at tiles left, right and
        // below current position to prevent blocked tiles
        if (_.find(neighbour, lastPositionCheck)) {
          good_position = false;
        }
      }
    }

    return good_position ? tile_model.getCoordinates() : null;
  };

  /*
    Randomize a tiles arrangement
  */
  this.randomiseFaces = function () {
    var tile_faces = [],
      uniq_faces = this.allocateTiles(),
      grouped = _.groupBy(uniq_faces),
      curr_idx = 0,
      max_type = config.max_type,
      faces, to_replace;

    faces = _.shuffle(_.keys(grouped));

    if (this.get('type') === memory_variation &&
      faces.length > max_type) {
      to_replace = faces.splice(max_type);
      uniq_faces = [];
      _.each(grouped, function (curr, fce) {
        if (faces.indexOf(fce) > -1) {
          uniq_faces = uniq_faces.concat(curr);
        }
      });

      _.each(to_replace, function (fce) {
        _.times(grouped[fce].length, function () {
          uniq_faces.push(faces[curr_idx]);
          curr_idx++;
          curr_idx = curr_idx % max_type;
        });
      });
    }

    // Shuffle
    uniq_faces = _.shuffle(uniq_faces);

    _.each(uniq_faces, function (val) {
      tile_faces.push(val);
      tile_faces.push(val);
    });

    return tile_faces;
  };

  /*
    Get a sequential arrangement of tile faces based on config
  */
  this.allocateTiles = function () {
    var tile_faces = [],
      families = this.get('families'),
      tiles_per_match = this.get('tile_per_match');

    _.each(families, function (family, idx) {
      var tile_types, pos,
        tile_pair, max_tile, prefix,
        list, frequency;

      if (family) {
        tile_pair = family.pairs;
        max_tile = config.max_tiles[idx];
        tile_types = family.types ? family.types :
          _.min([Math.ceil(tile_pair / tiles_per_match), max_tile]);
        prefix = 'f' + idx + '_';
        list = _.shuffle(_.range(0, max_tile));

        frequency = array.create(tile_types, 0, 1);

        // Pick n random elements from list
        // This will be types of a family
        list = _.first(list, tile_types);

        // Get adjusted count to allocate tiles
        tile_pair = tile_pair - array.sum(frequency);

        while (tile_pair > 0) {
          pos = _.random(0, tile_types - 1);

          frequency[pos] += 1;
          tile_pair -= 1;
        }

        _.each(list, function (item, i) {
          var obj = array.create(frequency[i], 0,
            prefix + item);

          tile_faces.push(obj);
        });
      }
    });
    return _.flatten(tile_faces);
  };

  /*
    Function to generate neighbour data for a tile
  */
  this.getNeighbours = function (ref, position_exclude, flatten) {
    var model = ref.length === 3 ? this.getTile(ref) : ref,
      tile_neighbour = model.getNeighbours(position_exclude),
      obj = {};

    _.each(tile_neighbour, function (val, key) {
      var ret = [];

      _.each(val, function (coord) {
        ret.push(this.tileAt(coord));
      }, this);
      obj[key] = ret;
    }, this);

    return flatten ? this.flattenTiles('custom', obj) : obj;
  };

  /*
    Return if a tile present at a certain x,y,z
  */
  this.tileAt = function (tile_pos, offset) {
    var tile;

    if (offset) {
      tile_pos = util.getOffsetPosition(tile_pos, offset);
      if (!tile_pos) {
        return null;
      }
    }
    tile = this.getTile(tile_pos);
    return tile ? tile.getCoordinates() : null;
  };

  /* Given a position i.e. number between 0 and tile_count
    returns tile model at that position
   */
  this.getTile = function (position) {
    var tiles = this.get('tiles');

    if (!position || !tiles[position[2]] ||
      !tiles[position[2]][position[0]] ||
      !tiles[position[2]][position[0]][position[1]]) {
      return null;
    }
    return tiles[position[2]][position[0]][position[1]];
  };

  /*
    Given a position and face , it assigns the tile
    at that position with that face
  */
  this.placeTile = function (tile, face, tag, face_visibility,
      hidden) {
    var depend = this.getNeighbours(tile, ['below'], true);

    face_visibility = face ===
      config.special_faces.joker || face_visibility;

    // update tile attributes
    tile.set({face: face,
              tag: tag,
              free: false,
              face_visibility: _.isBoolean(face_visibility) ?
                face_visibility : true
            });

    if (_.isBoolean(hidden)) {
      tile.set('hidden', hidden);
    }

    // Whenever a tile is filled, need to update
    // Neighbour status of that tile
    _.each(depend, function (val) {
      this.updateProperty(val);
    }, this);
  };

  /*
    Function to recompute neighbour data for that tile
  */
  this.updateProperty = function (position) {
    var depend, i,
      lfilled, rfilled,
      tile_model = this.getTile(position);

    if (tile_model.isFilled()) {
      return;
    }

    depend = this.getNeighbours(tile_model, ['above']);

    // Update state for tiles below it
    for (i = 0; i < 9; ++i) {
      if (depend.below[i] && !this.getTile(depend.below[i]).isFilled()) {
        return;
      }
    }

    if (this.onlyFreeInLine(position)) {
      tile_model.set('free', true);
      return;
    }

    // Attempt to update for tiles on the left/right
    lfilled = depend.left[0] || depend.left[1] ||
      depend.left[2] ? true : false;
    rfilled = depend.right[0] || depend.right[1] ||
      depend.right[2] ? true : false;
    for (i = 0; i < 3; ++i) {
      if (depend.left[i] && !this.getTile(depend.left[i]).isFilled() &&
        lfilled) {
        lfilled = false;
      }
      if (depend.right[i] && !this.getTile(depend.right[i]).isFilled() &&
        rfilled) {
        rfilled = false;
      }
    }
    tile_model.set({free: lfilled || rfilled});
  };

  this.cleanHinted = function () {
    var hinted_tiles = this.get('hinted');

    if (timer.has(timers[1])) {
      timer.unregister(timers[1]);
      this.autoHalfHint();
    }
    this.set('hinted', []);
    _.each(hinted_tiles, function (hinted) {
      hinted.showHint(false);
    });
  };

  this.beforePowerup = function () {
    var tile_selected = this.get('tile_selected');

    if (tile_selected) {
      if (tile_selected.get('hidden')) {
        tile_selected.set('face_visibility', false);
      }
    }
    this.cleanHinted();
  };

  this.isFreeTile = function (tile) {
    var neighbour = this.getNeighbours(tile, ['below']),
      slot_tile = this.get('slot_tile'),
      neighbourCheck = bind(this, function (item) {
        return !!item && (!slot_tile ||
          !this.isOpenSlotTile(item));
      });

    // Tile above not free
    if (_.find(neighbour.above, neighbourCheck)) {
      return false;
    }

    // Tiles on left AND right, not free
    if (_.find(neighbour.left, neighbourCheck) &&
      _.find(neighbour.right, neighbourCheck)) {
      return false;
    }

    return true;
  };

  this.isOpenSlotTile = function (tile) {
    var coords = _.isArray(tile) ? tile.slice(0, 3) : tile.getCoordinates(),
      slot_tile = this.get('slot_tile'),
      open_coords = slot_tile ? slot_tile.getCoordinates() : [];

    return _.isEqual(coords, open_coords);
  };

  this.isJewelNotInOrder = function () {
    var order = this.get('jewel_order'),
      jewels = this.get('jewels');

    return _.find(order, function (jewel, index) {
      if (jewel !== jewels[index]) {
        return true;
      }
    });
  };

  this.matchJewel = function (first_tile, second_tile) {
    var collected_jewels = _.map(this.get('jewel_order')),
      curr_collected = [],
      jewels = this.get('jewels');

    _.each([first_tile, second_tile], function (tile) {
      var tag = tile.get('tag');

      if (tag && _.contains(jewels, tag)) {
        curr_collected.push(tag);
      }
    });

    if (curr_collected.length > 0) {
      curr_collected = _.sortBy(curr_collected, function (curr) {
        return jewels.indexOf(curr);
      });

      collected_jewels = collected_jewels.concat(curr_collected);
      this.set('jewel_order', collected_jewels);
    }
  };

  this.matchTile = function (curr_selected) {
    var last_selected, changed_tiles;

    sounds.play('tap');

    this.cleanHinted();

    if (!this.get('tile_selected')) {
      this.set('tile_selected', curr_selected);
    } else {
      last_selected = this.get('tile_selected');
      if (last_selected.match(curr_selected)) {
        if (last_selected.isGoldenTile()) {
          this.increment('collected_golden');
        }
        if (last_selected.isKey() || curr_selected.isKey()) {
          changed_tiles = this.updateLockedTiles([last_selected,
            curr_selected]);
        }
        this.tileMatched([last_selected, curr_selected], changed_tiles);
        this.push('solved', curr_selected.get('face'));
        this.set('tile_selected', null);
        this.emit('tiles-matched');
      } else if ((last_selected.isJoker() || curr_selected.isJoker()) &&
        !(curr_selected.isGoldenTile() || last_selected.isGoldenTile())) {
        // Avoiding match of golden tile with joker
        /* istanbul ignore else */
        if (last_selected.isJoker() || curr_selected.isJoker()) {
          changed_tiles = this.updateJokerPair([last_selected,
            curr_selected]);
        }

        if (last_selected.isKey() || curr_selected.isKey()) {
          changed_tiles = _.union(changed_tiles,
            this.updateLockedTiles([last_selected, curr_selected]));
        }

        this.tileMatched([last_selected, curr_selected], changed_tiles);
        this.push('solved', curr_selected.get('face'));
        this.set('tile_selected', null);
        this.emit('tiles-matched', 'joker');
      } else {
        this.wrongMatch(last_selected, curr_selected);
      }
    }
  };

  this.pickTile = function (curr_selected) {
    var tile_selected, is_free, is_memory_match,
      valid_tap, is_hidden;

    if (curr_selected.isLock()) {
      if (curr_selected.isFree()) {
        curr_selected.emit('wrong-match-animate');
      }
      return false;
    }

    if (this.get('pick_tile')) {
      tile_selected = this.get('tile_selected');
      is_free = curr_selected.isFree();
      is_hidden = curr_selected.get('hidden');
      is_memory_match = this.get('type') === config.variations.memory.id;
      valid_tap = is_memory_match && is_free;

      if (this.get('mode') === 'swap' && curr_selected.isVisible()) {
        this.swapTiles(curr_selected, tile_selected);
      } else if (is_free &&
        !(tile_selected && _.isEqual(tile_selected.getCoordinates(),
        curr_selected.getCoordinates()))) {
        if (is_hidden) {
          curr_selected.set('face_visibility', true);
          if (curr_selected.isJoker()) {
            curr_selected.emit('update-joker-face', true);
          }
        }
        this.matchTile(curr_selected);
      } else if (is_free) {
        if (is_hidden) {
          if (curr_selected.isJoker()) {
            curr_selected.emit('update-joker-face', false);
          } else {
            curr_selected.set('face_visibility', false);
          }
        }
        this.set('tile_selected', null);
      }

      // This must be to at last,
      // other wise game finish with last tap will thow error
      if (valid_tap) {
        this.increment('tap');
      }
    }
  };

  this.swapTiles = function (curr_selected, tile_selected) {
    sounds.play('swap_tap');
    if (!tile_selected) {
      this.set('tile_selected', curr_selected);
      curr_selected.emit('swap-animate');
    } else {
      this.set('pick_tile', false);
      curr_selected.emit('swap-animate', bind(this, function () {
        sounds.play('swap');
        this.doSwap(tile_selected, curr_selected);
      }));
    }
  };

  this.doSwap = function (tile_one, tile_two, silent) {
    var selected_real_face,
      selected_tile_face = tile_one.get('face'),
      tile_one_tag = tile_one.get('tag'),
      tile_two_tag = tile_two.get('tag'),
      cb = _.after(2, bind(this, function () {
        this.updateGameState([], false, false);
      }));

    if (tile_one.isJoker() || tile_two.isJoker() ||
      tile_one.isGoldenTile() || tile_two.isGoldenTile()) {
      selected_real_face = tile_one.get('real_face');
      tile_one.set('real_face', tile_two.get('real_face'));
      tile_two.set('real_face', selected_real_face);
    }

    tile_two.set('tag', tile_one_tag);
    tile_one.set('tag', tile_two_tag);
    tile_one.set('face', tile_two.get('face'));
    tile_two.set('face', selected_tile_face);
    if (!silent) {
      tile_one.emit('after-swap-animate', cb);
      tile_two.emit('after-swap-animate', cb);
      this.set('tile_selected', null);
      this.set('mode', null);
      this.set('pick_tile', true);
      this.emit('powerup-applied', 'swap');
    }
  };

  this.tileMatched = function (tiles, changed_tiles) {
    var callback = _.after(tiles.length,
      bind(this, this.afterTileMatchAnim, tiles, changed_tiles));

    this.increment('pending_matches');
    _.each(tiles, function (curr) {
      curr.emit('match-animate', callback);
    });
  };

  this.updateLockedTiles = function (tiles) {
    var unlocked_array = [],
      key_prefix = config.key_prefix,
      unlocked_tile, type;

    _.each(tiles, bind(this, function (curr) {
      if (curr.isKey()) {
        type = curr.get('tag').replace(key_prefix, '');
        unlocked_tile = this.unlockTile(type);
        if (unlocked_tile) {
          unlocked_array.push(unlocked_tile);
        }
      }
    }));

    return unlocked_array;
  };

  this.lockTile = function (type) {
    var lock_keys = this.get('lock_keys');

    type = parseInt(type, 10);

    // Decrementing lock count from lock_key data
    _.each(lock_keys, function (lock_key, i) {
      if (lock_key[0] === type) {
        lock_keys[i][2] = lock_key[2] + 1;
        lock_keys[i][1] = lock_key[1] + 1;
      }
    });

    this.set('lock_keys', lock_keys);
  };

  this.unlockTile = function (type) {
    var lock_keys = this.get('lock_keys'),
      unlocked = false,
      unlocked_tile;

    type = parseInt(type, 10);

    this.eachCell(bind(this, function (model) {
      if (model.get('face') === 'f_lock_' + type) {
        model.set('prev_face', model.get('face'));
        model.set('face', model.get('real_face'));
        unlocked_tile = model;
        unlocked = true;
        return true;
      }
    }));

    // Decrementing lock count from lock_key data
    _.each(lock_keys, function (lock_key, i) {
      if (lock_key[0] === type) {
        lock_keys[i][2] = lock_key[2] - 1;
        if (unlocked) {
          lock_keys[i][1] = lock_key[1] - 1;
        }
      }
    });
    if (unlocked) {
      this.emit('key-matched', [type, unlocked_tile]);
    }
    this.set('lock_keys', lock_keys);
    return unlocked_tile;
  };

  this.wrongMatch = function (first, second) {
    var cb,
      type = this.get('type');

    if (type === memory_variation || type === partial_memory_variation) {
      cb = _.after(2,
        bind(this, this.afterWrongMatchAnimate, first, second));
      this.set('tile_selected', null);
    } else {
      this.set('tile_selected', second);
    }

    first.emit('wrong-match-animate', cb);
    second.emit('wrong-match-animate', cb);
  };

  this.afterWrongMatchAnimate = function (first, second) {
    if (first.get('hidden')) {
      first.set('face_visibility', false);
    }
    if (second.get('hidden')) {
      second.set('face_visibility', false);
    }
  };

  this.afterTileMatchAnim = function (tiles, changed_tiles) {
    var last_match = this.populateLastMatch(tiles, changed_tiles),
      neighbours = [],
      tile_coords = last_match.slice(0, this.get('tile_per_match'));

    _.find(tile_coords, bind(this, function (coordinates) {
      if (this.isOpenSlotTile(coordinates)) {
        this.set('last_match', []);
        last_match = [];
        return true;
      }
    }));

    this.matchJewel(tiles[0], tiles[1]);
    _.each(tiles, bind(this, function (curr) {
      neighbours = _.union(neighbours, this.getNeighbours(curr, [], true));
      this.cleanTile(curr);
    }));

    this.decrement('pending_matches');
    this.updateGameState(last_match, neighbours);
  };

  this.updateGameState = function (last_match, neighbours, silent) {
    var won = true,
      existing_stack = _.clone(this.get('last_match')) || [],
      pushLastMatch = bind(this, function () {
        if (last_match.length > 0) {
          existing_stack.push(last_match);
          this.set('last_match', existing_stack);
        }
      }),
      reset_shuffle = true;

    if (this.get('tile_count') === 0) {
      if (!_.isEmpty(this.get('jewels')) && this.isJewelNotInOrder()) {
        won = false;
      }
      if (!this.get('goal_completed')) {
        this.emit('game-over', won);
      }
    } else if (!this.isSolvableLayout()) {
      this.generateSolvableLayout();
      this.emit('layout-changed', this.get('last_match') &&
        this.get('last_match').length === 0);
      this.set('tile_selected', null);
      this.set('last_match', []);
    } else if (!this.isBoardSolvable() && !this.get('goal_completed')) {
      pushLastMatch();
      if (silent) {
        this.shuffle(silent);
      } else if (this.get('shuffle_game')) {
        reset_shuffle = false;
        this.emit('shuffle');
      } else {
        // TODO: Need to change event name.
        this.emit('auto-shuffle');
      }
    } else {
      // Update the views of all tiles
      // Values are neighbours of matched tile
      this.emit('tiles-update', neighbours || false, true);
      pushLastMatch();
    }

    if (reset_shuffle) {
      this.set('shuffle_game', false);
    }
  };

  this.highlight = function (tile_model) {
    var prev = this.getPrevious('tile_selected');

    if (this.get('mode') !== 'swap') {
      if (tile_model) {
        tile_model.set('state', 2);
      }

      if (prev && !prev.match(tile_model)) {
        prev.set('state', 1);
      }
    }
  };

  this.cleanTile = function (model) {
    var tiles = this.get('tiles'),
      coord = model.getCoordinates();

    if (this.isOpenSlotTile(coord)) {
      this.set('slot_tile', null);
    }

    // Clean Tile
    this.tile.releaseModel(model);

    // Remove from tile model
    tiles[coord[2]][coord[0]][coord[1]] = null;

    this.set('tiles', tiles);
    this.decrement('tile_count');
  };

  this.isBoardSolvable = function () {
    var open_tiles = {},
      board_solvable = false,
      tile_per_match = this.get('tile_per_match'),
      joker_count = 0,
      face;

    this.eachCell(bind(this, function (model) {
      if (this.isMatchable(model)) {
        if (model.isJoker()) {
          joker_count++;
        } else {
          face = model.get('face');
          if (!_.has(open_tiles, face)) {
            open_tiles[face] = 0;
          }
          open_tiles[face] += 1;
        }

        if (open_tiles[face] >= tile_per_match ||
          joker_count >= tile_per_match - open_tiles[face]) {
          board_solvable = true;
        }
        return board_solvable;
      }
    }));
    return board_solvable;
  };

  this.createLoadArray = function (success, type, attempt) {
    var user = GC.app.user,
      load_array = user.get('puzzle_loads');

    load_array.unshift({
      ms: this.get('ms_no'),
      id: this.get('id'),
      type: type,
      success: success,
      time: Date.now() - load_start_time,
      attempts: attempt,
      tiles_placing: this.get('tiles_placings')
    });

    if (load_array.length > config.puzzle_load.track) {
      load_array.pop();
    }

    user.set('puzzle_loads', load_array);
  };

  this.onShuffleGenerate = function (success, silent) {
    var not_powerup = silent || this.get('shuffle_game');

    if (!success) {
      return this.onShuffleFailed();
    }

    this.set('shuffle_game', false);
    this.createLoadArray(true, 'shuffle', curr_attempt);
    this.eachCell(function (model) {
      model.emit('change-face');
    });

    this.emit('tiles-update');

    this.emit('shuffle-complete');

    // shuffle invoked by powerup useage
    if (!not_powerup) {
      this.emit('powerup-applied', 'shuffle');

      if (this.get('type') === memory_variation) {
        GC.app.user.increment('free_shuffle');
      }
    }

    // Reset as tile re-arranged
    if (silent || this.get('ongoing') === 0) {
      this.set('last_match', []);
    } else {
      this.set('pending_last_match', []);
    }
  };

  this.shuffle = function (silent) {
    var attempt,
      success = false,
      tile_faces = [],
      free_shuffle = this.get('free_shuffle') === 0,
      attempts_allowed = config.puzzle_load.retry;

    this.beforePowerup();
    this.set('tile_selected', null);

    this.eachCell(function (model) {
      if (!silent) {
        model.emit('shuffle-animate');
      }
      if (model.isJoker() || model.isGoldenTile() || model.isLock()) {
        tile_faces.push(model.get('real_face'));
      } else {
        tile_faces.push(model.get('face'));
      }
    });

    load_start_time = Date.now();
    this.set('tiles_placings', 0);

    tile_faces = _.flatten(_.shuffle(_.groupBy(tile_faces)));

    if (silent) {
      for (attempt = 0; attempt < attempts_allowed; ++attempt) {
        curr_attempt = attempt;

        if (this.generateSolvableGame(tile_faces, true)) {
          success = true;
          break;
        }
      }
      this.onShuffleGenerate(success, true);
    } else {
      if (free_shuffle) {
        this.increment('free_shuffle');
      }
      this.shuffleAsync(tile_faces);
    }
    this.createLoadArray(false, 'shuffle', attempt);
  };

  this.onShuffleFailed = function () {
    this.emit('load-failed', true);
  };

  this.shuffleAsync = function (tile_faces) {
    if (!tile_faces) {
      tile_faces = this.get('shuffle_faces');
    }
    this.assignFaces(tile_faces)
      .then(this.onShuffleGenerate);
  };

  this.halfHint = function (auto) {
    this.hint(true);
    sounds.play('half_hint');
    if (!auto) {
      this.emit('powerup-applied', 'half_hint');
    }
  };

  this.autoHalfHint = function () {
    timer.register(timers[1], bind(this, function () {
      if (this.get('mode') === null) {
        this.halfHint(true);
      }
    }), config.auto_hint_interval);
  };

  this.hint = function (half) {
    var pair, pair_tiles,
      open_tiles = {},
      hinted = this.get('hinted'),
      pair_count = this.get('tile_per_match');

    // first is to unlight the higlighted/hinted tile
    // second one to so on next time we
    // select tile it should record a change event
    // else setting null again without unset will not emit
    // change event
    this.beforePowerup();
    this.set('tile_selected', null);
    this.unset('tile_selected');

    this.eachCell(bind(this, function (model) {
      var face = model.get('face');

      if (this.isMatchable(model)) {
        if (!_.has(open_tiles, face)) {
          open_tiles[face] = [];
        }
        open_tiles[face].push(model);
      }
    }));

    // Purge faces that cannot form a pair
    pair_tiles = _.pick(open_tiles, function (val) {
      return val.length >= pair_count;
    });

    pair = _.first(_.shuffle(pair_tiles)[0], pair_count);

    if (!pair) {
      pair = this.getPairWithJoker(open_tiles);
    }

    if (!half) {
      // emit highlight for tile model
      sounds.play('hint');
      _.each(pair, function (model) {
        model.emit('highlight');
      });
      this.set('hinted', pair);
      this.emit('powerup-applied', 'hint');
    } else {
      this.selectTileForHalfHint(hinted, pair);
    }
  };

  this.selectTileForHalfHint = function (hinted, pair) {
    var pair_count = this.get('tile_per_match');

    if (hinted.length === pair_count && hinted) {
      hinted[1].emit('highlight');
      this.setHintAsSelected(hinted[1]);
      this.set('hinted', [hinted[1]]);
    } else if (_.isArray(pair)) {
      pair[0].emit('highlight');
      this.setHintAsSelected(pair[0]);
      this.set('hinted', pair);
    }
  };

  this.setHintAsSelected = function (model) {
    this.set('tile_selected', model, true);
  };

  this.magnet = function () {
    // first is to remove any current effect like hint
    // second one to so on next time we
    // select tile it should record a change event
    // else setting null again without unset will not emit
    // change event
    this.beforePowerup();
    this.set('tile_selected', null);
    this.unset('tile_selected');
    this.once('change:tile_selected', this.applyMagnet);
  };

  this.applyMagnet = function (selected) {
    var open_tile = null,
      close_tile = null,
      selected_face;

    // when user click anywhere else bt nt on tile
    if (_.isNull(selected)) {
      this.emit('powerup-applied', 'magnet', false);
      return;
    }

    selected_face = selected.get('face');

    this.eachCellByLayer(bind(this, function (model) {
      var face = model.get('face');

      // ignore same select tile and ignore tiles with
      // differnt faces then selected one
      // and if any open tile found we just need to return
      if (selected === model || selected_face !== face || open_tile) {
        return;
      }

      // as when open tile found ,we stop finding more tiles
      // look at above condition
      // but if even if we found close tile
      // we keep searching for any open tile on till last layer
      if (this.isFreeTile(model)) {
        open_tile = model;
      } else if (!close_tile) {
        close_tile = model;
      }
    }));

    this.tileMatched([selected], open_tile || close_tile);
    this.emit('tiles-matched');
    this.push('solved', selected_face);
    this.emit('powerup-applied', 'magnet');
  };

  // swoosh purpose is to remove configured no of tiles from board
  // firstly it try to select match open pair less than max configured
  // if only less then max available ,then after matching all available
  // ,we listen to get board view updated and then after updation, again
  // start matching open till till max hit or board empty
  this.swoosh = function (match_joker) {
    var pair_tiles, face,
      open_tiles = {},
      pairs = [],
      pair_count = this.get('swooshed_count'),
      pair_no = this.get('tile_per_match'),
      max_count = config.swoosh_pairs,
      addPairs = function (val) {
        var item;

        while (val.length && pair_count < max_count) {
          item = [];

          while (item.length < pair_no) {
            item.push(val.shift());
          }
          pairs.push(item);
          pair_count++;
        }
      };

    this.beforePowerup();
    this.set('tile_selected', null);

    this.eachCellByLayer(bind(this, function (model) {
      face = model.get('face');

      if (this.isMatchable(model)) {
        if (!open_tiles[face]) {
          open_tiles[face] = [];
        }
        open_tiles[face].push(model);
      }
    }));

    // remove single tiles from open

    pair_tiles = _.pick(open_tiles, function (val, key) {
      return val.length >= pair_no && key !== config.special_faces.joker;
    });

    // save first 5 pairs from tiles ,firstly try to
    // get from open ,if nt avail then close
    _.each(pair_tiles, function (val) {
      var mod = val.length % pair_no;

      if (mod !== 0) {
        val = val.slice(0, val.length - mod);
      }
      addPairs(val);
    });

    if (pairs.length === 0 ||
      this.get('swooshed_count') + pairs.length < config.swoosh_pairs &&
      match_joker) {
      addPairs(this.getPairWithJoker(open_tiles));
    }
    this.set('swoosh_pairs', pairs);
    this.swooshAnimate();
  };

  this.swooshAnimate = function () {
    var pair = this.get('swoosh_pairs').shift(),
      callback, changed_tiles;

    if (!this.get('goal_completed')) {
      _.find(pair, bind(this, function (model) {
        if (model.isJoker()) {
          changed_tiles = this.updateJokerPair(pair);
          return true;
        }
      }));

      _.find(pair, bind(this, function (model) {
        if (model.isKey()) {
          changed_tiles = _.union(changed_tiles, this.updateLockedTiles(pair));
          return true;
        }
      }));

      callback = _.after(pair.length,
        bind(this, this.afterSwooshAnimate, pair, changed_tiles));

      this.increment('swooshed_count');
      this.push('solved', pair[0].get('face'));

      if (pair[0].isGoldenTile()) {
        this.increment('collected_golden');
      }
      _.each(pair, function (model) {
        model.emit('match-animate', callback);
      });
      this.emit('tiles-matched');
    }
  };

  this.afterSwooshAnimate = function (pair, changed_tile) {
    var last_match,
      silent = true,
      first = pair[0],
      second = pair[1],
      first_coord = first.getCoordinates(),
      second_coord = second.getCoordinates(),
      existing_stack = _.clone(this.get('last_match')) || [];

    last_match = this.populateLastMatch([first, second], changed_tile);

    if (this.isOpenSlotTile(first_coord) || this.isOpenSlotTile(second_coord)) {
      existing_stack = [];
    } else {
      existing_stack.push(last_match);
    }

    this.set('last_match', existing_stack);
    this.matchJewel(first, second);
    this.cleanTile(first);
    this.cleanTile(second);
    if (this.get('swoosh_pairs').length > 0) {
      _.delay(this.swooshAnimate, 200);
    } else {
      if (this.get('swooshed_count') ===
        config.swoosh_pairs) {
        this.set('swooshed_count', 0);
        this.emit('powerup-applied', 'swoosh');
        silent = false;
        this.set('shuffle_game', true);
      } else if (this.get('tile_count') > 0) {
        this.once('resize-complete', this.swoosh, true);
      }
      this.updateGameState([], false, silent);
    }
  };

  this.substitute = function () {
    var source_face, dest_face, faces;

    faces = this.getFacesToSubstitute(true);

    if (faces.length < 2) {
      faces = this.getFacesToSubstitute(false);
    }
    this.beforePowerup();
    this.set('tile_selected', null);
    source_face = faces[0];
    dest_face = faces[1];

    /*
      TODO: Ideally we should allow user to use substitute
      powerup should be disabled
    */
    if (source_face && dest_face) {
      this.emit('substitute-overlay', dest_face, source_face);
      timer.timeout('substitute', bind(this, function () {
        this.eachCellByLayer(bind(this, function (model) {
          if (dest_face === model.get('face')) {
            model.emit('substitute-animate', bind(this, function () {
              this.changeFace(model, source_face);
            }));
          } else if (dest_face === model.get('real_face')) {
            model.set('real_face', source_face);
          }
        }));
        this.emit('powerup-applied', 'substitute');
      }), 900);
    } else {
      this.emit('powerup-applied', 'substitute');
    }
  };

  this.flip = function () {
    var can_flipped = [];

    this.beforePowerup();
    this.set('tile_selected', null);
    this.set('mode', 'flip');

    this.eachCell(function (tile) {
      if (!tile.get('face_visibility') && !tile.isJoker() && tile.isVisible() &&
          tile.get('hidden')) {
        can_flipped.push(tile);
      }
    });

    _.each(_.sample(can_flipped, config.flip_count), function (tile) {
      tile.set('hidden', false);
      tile.set('face_visibility', true);
    });
    this.emit('powerup-applied', 'flip');
  };

  this.swap = function () {
    this.beforePowerup();
    this.set('tile_selected', null);
    this.set('mode', 'swap');
  };

  this.getFacesToSubstitute = function (unlocked_only) {
    var curr_face,
      faces = [];

    this.eachCell(bind(this, function (model) {
      curr_face = model.get('face');
      if (faces.indexOf(curr_face) === -1 &&
        (!unlocked_only || this.isFreeTile(model)) &&
        !model.isJoker() && !model.isGoldenTile() &&
        !model.isLock()) {
        faces.push(curr_face);
      }
      return faces.length === 2;
    }));

    return faces;
  };

  this.changeFace = function (model, face) {
    model.set('face', face);
    model.emit('change-face');
  };

  this.undo = function () {
    var tile_one_model, tile_two_model, face, prev_face,
      extra_tile, lock_prefix, changed_tiles,
      existing_stack = _.clone(this.get('last_match')),
      last_match = existing_stack.pop(),
      tiles_to_load = last_match.slice(0, 2),
      neighbours = [],
      jewels = [],
      jewel_order = this.get('jewel_order'),
      special_faces = config.special_faces,
      tile_one = last_match[0],
      tile_two = last_match[1];

    this.beforePowerup();

    // Add tile to model
    this.loadTiles(tiles_to_load);

    tile_one_model = this.getTile(tile_one);
    tile_two_model = this.getTile(tile_two);

    // Update dependency
    this.placeTile(tile_one_model, tile_one[3]);
    this.placeTile(tile_two_model,
      tile_two[3] || tile_one[3]);

    if (tile_one_model.isJoker() ||
      tile_one[3] === special_faces.golden) {
      tile_one_model.set('real_face', tile_one[4]);
      this.increment('no_of_jokers');
      tile_one[6] = false;
    }

    if (tile_two_model.isJoker() ||
      tile_one[3] === special_faces.golden) {
      this.getTile(last_match[1]).set('real_face', tile_two[4]);
      this.increment('no_of_jokers');
      tile_two[6] = false;
    }

    if (tile_one[5]) {
      jewels.push(tile_one[5]);
      tile_one_model.set('tag', tile_one[5]);
    }
    if (tile_two[5]) {
      tile_two_model.set('tag', tile_two[5]);
      jewels.push(tile_two[5]);
    }
    if (tile_one[6]) {
      tile_one_model.set('hidden', tile_one[6]);
      tile_one_model.set('face_visibility', !tile_one[6]);
    }
    if (tile_two[6]) {
      tile_two_model.set('hidden', tile_two[6]);
      tile_two_model.set('face_visibility', !tile_two[6]);
    }
    _.each(jewels, function (jewel) {
      jewel_order = _.without(jewel_order, jewel);
    });
    this.set('jewel_order', jewel_order);

    /* taking the tiles changes rather than matched,
      and updating those to older condition. It will happen
      normally after joker and lock key mathed
    */
    changed_tiles = _.rest(last_match, 2);
    _.each(changed_tiles, bind(this, function (curr) {
      if (_.isArray(curr)) {
        extra_tile = this.getTile(curr);
        prev_face = curr[4];
        face = prev_face || curr[3];
        this.changeFace(extra_tile, face);

        extra_tile.set('real_face', curr[3]);
        lock_prefix = config.lock_prefix;
        if (face.search(lock_prefix) !== -1) {
          this.lockTile(face.replace(lock_prefix + '_', ''));
        }
      }
    }));

    this.increment('undo');
    this.increment('tile_count', 2);
    this.pop('solved');

    _.each(tiles_to_load, function (coord) {
      var tile = this.getTile(coord),
        neighbour = this.getNeighbours(tile, [], true);

      this.emit('render-tile', tile);

      neighbours = _.union(neighbours, neighbour);
    }, this);

    this.emit('tiles-update', neighbours);

    // Using timer can break resize
    _.defer(bind(this, function () {
      this.set('tile_selected', null);
      this.set('last_match', existing_stack);
    }));
  };

  this.isSolvableLayout = function () {
    var count = 0,
      tile_per_match = this.get('tile_per_match');

    // checks is there  more then one open tile on board
    this.eachCell(bind(this, function (model) {
      // count <=1 so if few found more than 1 count stop
      // checking further

      if (this.isMatchable(model)) {
        count++;
      }
      return count >= tile_per_match;
    }));
    return count >= tile_per_match;
  };

  this.generateSolvableLayout = function () {
    var tiles = [],
      tile_per_match = this.get('tile_per_match'),
      offset = 3,
      no_tiles = this.get('tile_count'),
      pos = {
        x: 0,
        y: 0
      },
      initPosition = function (x, y) {
        pos.x = x - offset;
        pos.y = y - offset * (no_tiles / tile_per_match - 1);

        pos.x = pos.x < 0 ? 0 : pos.x;
        pos.y = pos.y < 0 ? 0 : pos.y;
      },
      count = 0;

    this.eachCell(function (model, x, y, z) {
      if (count === 0) {
        initPosition(x, y);
      }

      x = pos.x;
      y = pos.y;
      z = 0;

      if (!tiles[z]) {
        tiles[z] = [];
      }

      if (!tiles[z][x]) {
        tiles[z][x] = [];
      }

      tiles[z][x][y] = model;
      model.set({
        state: 1,
        x: x,
        y: y,
        z: z
      });

      if (++count % tile_per_match === 0) {
        pos.x = pos.x - offset;
        pos.y = pos.y + offset;
      } else {
        pos.x = pos.x + offset;
      }
    });

    this.set('tiles', tiles);
    test.prepare(this, {
      initPosition: initPosition,
      pos: pos
    });
  };

  this.getOccupiedColumnsRows = function () {
    var x = {
        min: {
          val: 999,
          model: null,
          z: 0
        },
        max: {
          val: 0,
          model: null,
          z: 999
        }
      },
      y = {
        min: {
          val: 999,
          model: null,
          z: 0
        },
        max: {
          val: 0,
          model: null,
          z: 999
        }
      },
      setMin = function (current, model, tx, tz, r) {
        // r is to inverse < operation.
        if (!model.isSlot() && (tx * r < current.val * r ||
            tx === current.val && current.z * r < tz * r)) {
          current.val = tx;
          current.z = tz;
          current.model = model;
        }
      };

    this.eachCell(function (model, tx, ty, tz) {
      // min x with max z
      setMin(x.min, model, tx, tz, 1);

      // max x with min z
      setMin(x.max, model, tx, tz, -1);

      // min y with max z
      setMin(y.min, model, ty, tz, 1);

      // max y with min z
      setMin(y.max, model, ty, tz, -1);
    });

    return {
      x: {
        min: x.min.model,
        max: x.max.model,
        depth: x.min.z
      },
      y: {
        min: y.min.model,
        max: y.max.model,
        depth: y.min.z
      }
    };
  };

  this.startTimers = function () {
    timer.register('puzzle', bind(this, function () {
      var time = this.increment('time');

      if (time >= this.get('limit') && this.get('limit_type') !== 'untimed') {
        this.pauseTimers();
        this.emit('check-complete');
      }
    }), 1000);
  };

  this.pauseTimers = function () {
    timer.pause(timers);
  };

  this.stopTimers = function () {
    timer.unregister(timers);
  };

  this.resumeTimers = function () {
    timer.resume(timers);
  };

  this.checkGoals = function () {
    var completed = false,
      goals = this.get('goal');

    // TODO: Change the logic for every variations
    completed = _.find(_.keys(goals), bind(this, function (goal) {
      return goal === 'golden' &&
        goals[goal] === this.get('collected_golden');
    }));
    if (completed) {
      this.set('goal_completed', true);
      this.checkComplete();
    }
  };

  // chain emited with undefined
  this.checkComplete = function (val, cb) {
    var limit_type = this.get('limit_type');

    if (this.get('goal_completed')) {
      this.emit('game-over', true);
    } else if (this.get(limit_type) >= this.get('limit') &&
      this.get('pending_matches') !==
      this.get('tile_count') / this.get('tile_per_match')) {
      this.emit('out-of-limit');
    }
    if (cb) {
      cb();
    }
  };

  this.gameOver = function (type, cb, invoke_supr) {
    var won = type === true,
      limit_type = this.get('limit_type'),
      user = GC.app.user;

    invoke_supr = _.isBoolean(invoke_supr) ? invoke_supr : true;

    // lives should update if won or did nt matched any tile or solved.
    // after any powerup use which removes any tile from board
    // live should nt increase
    // if any tile pair solved then  this.get('solved').length  will
    // be greater than zero bt nt in only case when after solving
    // first pair he use undo, so using this.get('undo') > 0 to handle
    // that case
    // no need to update if out-of-time and user didnt matched pair

    if ((won || this.get(limit_type) < this.get('limit') &&
      !(this.get('solved').length > 0 || this.get('undo') > 0)) &&
      user.get('infinite_lives_remaining') <= 0) {
      user.incrementLives();
    }
    if (user.get('inventory_lives') === 0) {
      event_manager.emit('out-of-lives');
    }
    this.stopTimers();
    if (won) {
      this.set('stars', this.getStars());
    }
    this.set('score', this.getScore(won));
    this.updateProgress();
    if (invoke_supr) {
      supr(this, 'gameOver', [type, cb]);
    }
  };

  this.getStars = function () {
    var time_remaining_ratio, time_per_tile,
      limit_type = this.get('limit_type'),
      stars = 0;

    if (limit_type === 'untimed') {
      time_per_tile = this.get('total_tiles') / this.get('time') * 1.2;
      time_remaining_ratio = time_per_tile * 100;
      if (time_remaining_ratio < 70) {
        stars = 1;
      } else if (time_remaining_ratio < 90) {
        stars = 2;
      } else {
        stars = 3;
      }
    } else {
      time_remaining_ratio = this.get(limit_type) / this.get('limit') * 100;
      if (time_remaining_ratio < 70) {
        stars = 3;
      } else if (time_remaining_ratio < 90) {
        stars = 2;
      } else {
        stars = 1;
      }
    }
    return stars;
  };

  this.getScore = function (won) {
    var families = this.get('families'),
      family_score = config.family_score,
      limit_type = this.get('limit_type'),
      bonus = 0,
      time_per_tile = this.get('total_tiles') / this.get('time') * 1.2,
      board_score = 0,
      limit = this.get('limit'),
      limit_left = limit - this.get(limit_type),
      solved = _.clone(this.get('solved')),
      unsolved = {},
      face, group_solved;

    /* For golden tile goal to make sure that score is consistant,
     * we are considering that all tiles are solved. */
    if (won && _.contains(_.keys(this.get('goal')),
      config.variations.golden.id)) {
      this.eachCell(function (model) {
        if (model.isJoker() || model.isGoldenTile()) {
          face = model.get('real_face');
        } else {
          face = model.get('face');
        }

        if (!unsolved[face]) {
          unsolved[face] = 0;
        }
        ++unsolved[face];
      });

      _.each(unsolved, function (count, curr_face) {
        solved = solved.concat(array.create(Math.ceil(count / 2), 0,
          curr_face));
      });
    }

    group_solved = _.groupBy(solved, function (face) {
      return face.split('_')[0][1];
    });

    _.each(families, function (family, i) {
      if (family) {
        board_score = board_score + family_score[i] * (
          group_solved[i] ? group_solved[i].length : 0);
      }
    });

    if (won) {
      if (limit_type === 'untimed') {
        /* Adding bonus for untimed levels on the basis of ratio of
          total tiles vs time considering it takes one seconds to solve one tile
        */
        bonus = 400 * Math.round(solved.length * time_per_tile);
      } else {
        bonus = 400 * Math.round(limit_left * solved.length / limit);
      }
    }
    return bonus + board_score;
  };

  this.updateProgress = function () {
    var solved_pairs = this.get('solved').length,
      progress = 0,
      total_pairs = _.reduce(this.get('families'), function (count, data) {
        return count + (data ? data.pairs : 0);
      }, 0);

    progress = Math.floor(solved_pairs / total_pairs * 100);
    this.set('progress', progress);
  };

  this.limitBought = function (quantity) {
    var type = this.getLimit(),
      product = util.getProducts()[type];

    this.increment('limit', quantity || product.quantity);
    this.resumeTimers();
    this.updateGameState([], false, true);
  };

  this.savePuzzleData = function (exit_type, extra_keys) {
    var keys = _.union(['free_shuffle', 'undo', 'loading_elapsed', 'tap',
      'tiles_placings', 'load_attempts', 'type', 'tile_set', 'bonus_map'],
      _.keys(util.getPowerups()), extra_keys || []);

    if (!exit_type && this.get('free_shuffle') > 0) {
      exit_type = 'shuffle';
    }
    supr(this, 'savePuzzleData', [exit_type, keys]);
  };

  // This function will keep the puzzle solvable
  // by changing face of unsolvable tiles
  this.updateJokerPair = function (pairs) {
    var faces = {},
      changed_tiles = [],
      face_change = [],
      i = 0,
      tile_per_match = this.get('tile_per_match'),
      prev_occu = tile_per_match,
      selected_coords = [],
      is_joker, face, face_to_change, index, is_special_tile;

    _.each(pairs, bind(this, function (tile) {
      is_joker = tile.isJoker();
      face = is_joker ? tile.get('real_face') : tile.get('face');
      if (is_joker) {
        this.decrement('no_of_jokers');
      }
      if (!_.has(faces, face)) {
        faces[face] = 0;
      }
      faces[face] += 1;
      selected_coords.push(tile.getCoordinates().toString());
    }));

    _.each(faces, function (occurance, face) {
      if (prev_occu > occurance) {
        prev_occu = occurance;
        face_to_change = face;
      }
      for (i = 0; i < tile_per_match - occurance; i++) {
        face_change.push(face);
      }
    });
    faces = _.filter(face_change, function (face) {
      return face !== face_to_change;
    });

    this.eachCellByLayer(bind(this, function (model) {
      if (_.contains(selected_coords, model.getCoordinates().toString())) {
        return false;
      }
      is_special_tile = model.isJoker() || model.isLock();
      face = is_special_tile ? model.get('real_face') : model.get('face');
      index = _.indexOf(faces, face);
      if (index >= 0) {
        if (!is_special_tile) {
          model.set('real_face', face);
          this.changeFace(model, face_to_change);
        } else {
          model.set('real_face', face_to_change);
        }
        faces.splice(index, 1);
        changed_tiles.push(model);
      }
    }));

    return changed_tiles;
  };

  this.getPairWithJoker = function (open_tiles) {
    var pairs = null,
      keys = _.keys(open_tiles),
      joker_face = config.special_faces.joker;

    if (open_tiles[joker_face]) {
      pairs = [];
      pairs.push(open_tiles[joker_face].pop());

      if (keys.length > 1) {
        keys = _.shuffle(_.without(keys, joker_face));
        pairs.push(open_tiles[keys.pop()].pop());
      } else {
        pairs.push(open_tiles[joker_face].pop());
      }
    }
    return pairs;
  };

  this.populateLastMatch = function (pairs, changed_tiles) {
    var last_match = [],
      index = 0;

    _.each(pairs, function (model, i) {
      last_match.push(model.getCoordinates());
      last_match[i].push(model.get('face'));
      last_match[i].push(model.get('real_face'));
      last_match[i].push(model.get('tag'));
      last_match[i].push(model.get('hidden'));
      index = i;
    });

    if (changed_tiles) {
      _.each(changed_tiles, function (changed_tile) {
        last_match.push(changed_tile.getCoordinates());
        last_match[++index].push(changed_tile.get('real_face'));
        last_match[index].push(changed_tile.get('prev_face'));
        changed_tile.set('prev_face', null);
      });
    }

    return last_match;
  };

  this.beforeTutorial = function (item, view) {
    var model;

    if (item === 'joker_intro') {
      model = view.model;
      model.set('real_face', model.get('face'));
      this.changeFace(model, config.special_faces.joker);
    } else if (item === 'swap_select_tiles') {
      view.overlay.hide();
    }
  };

  this.afterTutorial = function (item, view) {
    if (item === 'swap_select_tiles') {
      view.overlay.show();
    }
  };

  this.onTapChange = function () {
    this.emit('check-complete');
  };

  this.getBestScore = function () {
    return puzzle_module.getScore(this.get('id')).score || 0;
  };

  this.isMatchable = function (model) {
    return this.isFreeTile(model) && !model.isLock();
  };

  this.logEvent = function (event_id) {
    event_manager.emit(event_id);
  };

  this.clean = function () {
    // this will call effects.commit in screen
    this.set('tile_selected', null);
    this.removeListener('change:tile_selected', this.first_tap);
    this.removeListener('tiles-matched', this.first_pair_match);
    this.tile.releaseAllModels();
    supr(this, 'clean');
  };
});
