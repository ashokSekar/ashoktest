/*
 New Relic agent configuration.

 See lib/config.defaults.js in the agent distribution for a more complete
 description of configuration variables and their potential values.
*/
exports.config = {
  /*
    Array of application names.
  */
  app_name: ['Mahjongg-Quest-Server'],
  /*
   Your New Relic license key.
  */
  license_key: '7a7cad16512db647d7a08903c4308c8e892021b4',
  logging: {
    /*
      Level at which to log. 'trace' is most useful
      to New Relic when diagnosing
      issues with the agent, 'info' and higher will
      impose the least overhead on
      production applications.
    */
    level: 'info',
    filepath: '/var/log/mahjongg/newrelic_agent.log'
  }
};
