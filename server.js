'use strict';

if (process.env.NODE_ENV === 'production') {
  require('newrelic');
}

var logger = require('quest/modules/debug')('SERVER'),
  express = require('express'),
  path = require('path'),
  body_parser = require('body-parser'),
  app = express(),
  config = require('quest/resources/config'),
  access_control_dev = require('quest/modules/access_control_dev'),
  access_control_prod = require('quest/modules/access_control_prod'),
  validation = require('quest/modules/validator'),
  response_handler = require('quest/modules/response'),
  test = require('quest/test/lib/private'),
  jsErrorHandler = function (err, req, res, next) { // jshint ignore:line
    logger.error('Javascript Error :%s', err);
    res.error(err);
  };

app.use(body_parser.urlencoded({extended: true}));
app.use(body_parser.json({limit: '10mb'}));
app.use(response_handler);
app.use(validation.express);

app.use(express.static(path.join(config.game.dir)));

// allow cross origin requests for dev mode else Allow specific origins
if (app.get('env') === 'development') {
  app.use(access_control_dev);
} else {
  app.use(access_control_prod);
}

// load controllers
require('quest/modules/boot')(app);

// Error handler should be the last middle ware
app.use(jsErrorHandler);

app.set('port', process.env.PORT || 3002);

// Need this to close connection on server
app.running = app.listen(app.get('port'));

test.prepare(app, {
  jsErrorHandler: jsErrorHandler
});

module.exports = app;
