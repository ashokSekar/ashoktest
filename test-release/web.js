// jshint node:true

describe('Facebook web Release -', function () {
  'use strict';

  var manifest_web = require('../manifest/production_web'),
    chai = require('chai'),
    expect = chai.expect,
    check = it;

  check('App Id', function () {
    expect(manifest_web.fbid).to
      .equal('180709025702831');
  });

  check('upload access token', function () {
    expect(manifest_web.upload_access_token).to
        .equal('EAAYsfZAxiFmMBAGX88f42wUpfHQpSA43zZC7zGl8clM4QBt5VqRoEGEc' +
        '0c5UtsOj3yyknbOnbZCrwDVbuAW5zlE2a2EYp0GEyksixQ1tppobZCdshd5S7L' +
        'CNAf1GsPbqAT12mf02EhWRGNnbcCbf99hLDvW4bidYYpfj4mNDEgZDZD');
  });
});
