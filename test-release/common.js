// jshint node:true

describe('Release Common -', function () {
  'use strict';

  var config = jsio('../resources/data/config'),
    exec = require('shelljs').exec,
    chai = require('chai'),
    manifest = require('../manifest'),
    expect = chai.expect,
    check = it;

  check('title', function () {
    expect(manifest.title).to.equal('Mahjong');
  });

  check('server url', function () {
    expect(config.server).to
      .equal('https://mahjong.hashcube.com');
  });

  check('life object id', function () {
    expect(config.fb_objects.life).to
      .equal('1918484408388413');
  });

  describe('module', function () {
    var modules = JSON.parse(exec('devkit modules --json',
      {silent: true}).stdout),
      module, opts,
      compare = function (exp, curr) {
        expect(exp).to.equal(curr);
      };

    for (module in modules) {
      opts = modules[module];
      check(module, compare.bind(this, opts.version, opts.currentVersion.tag));
    }
  });
});
