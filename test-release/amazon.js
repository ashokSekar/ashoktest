// jshint node:true

describe('Amazon Release -', function () {
  'use strict';

  var manifest = require('../manifest'),
    chai = require('chai'),
    fs = require('fs'),
    expect = chai.expect,
    firebase_config = jsio('../' + manifest.addons.firebase.android),
    check = it;

  check('google project id', function () {
    expect(manifest.android.googleProjectNo).to.equal('559142647925');
  });

  check('facebook app id', function () {
    expect(manifest.android.facebookAppID).to.equal('180709025702831');
  });

  check('facebook app name', function () {
    expect(manifest.android.facebookDisplayName).to.equal('Mahjong Quest');
  });

  check('flurry key', function () {
    expect(manifest.android.flurryKey).to.equal('WYDBT9HNFV4PZFCRZFHF');
  });

  check('App Links are correct', function () {
    expect(manifest.android.app_links[0].scheme).to
      .equal('https');

    expect(manifest.android.app_links[0].host).to
      .equal('mahjongquestfree.com');

    expect(manifest.android.app_links[0].pathPrefix).to
      .equal('/play');
  });

  check('Firecase config', function () {
    var client_info = firebase_config.client[0].client_info;

    expect(firebase_config.project_info.project_number).to
      .equal('621443799882');
    expect(client_info.mobilesdk_app_id).to
      .equal('1:621443799882:android:91ee77df51050cf1');
    expect(client_info.android_client_info.package_name).to
      .equal('com.hashcube.mahjongfree');
  });

  check('freshchat app ID', function () {
    expect(manifest.android.freshchatAppID).to
      .equal('0a7cc14c-18df-4d8a-a28f-e31326e7328f');
  });

  check('freshchat app key', function () {
    expect(manifest.android.freshchatAppKey).to
      .equal('4ae2306c-1273-4e30-a82e-79e155202545');
  });

  check('freshchat app tag', function () {
    expect(manifest.android.freshchatTag).to
      .equal('mahjong_quest');
  });

  check('hotline app key', function () {
    expect(manifest.android.hotlineAppKey).to
      .equal('6a9f67df-3e14-45a0-818d-6936a192f11d');
  });

  check('package name', function () {
    expect(manifest.android.packageName).to.equal('com.hashcube.mahjongfree');
  });

  check('kochava key', function () {
    expect(manifest.android.kochavaAppGUID).to
      .equal('komahjong-quest-amazon-57c6q');
  });

  check('gamecircle api key', function () {
    var file = fs.readFileSync(__dirname + '/../assets/api_key.txt'),

    // jscs:disable
      key = 'eyJhbGciOiJSU0EtU0hBMjU2IiwidmVyIjoiMSJ9.eyJ2ZXIiOiIzIiwiZW5kcG9pbnRzIjp7ImF1dGh6IjoiaHR0cHM6Ly93d3cuYW1hem9uLmNvbS9hcC9vYSIsInRva2VuRXhjaGFuZ2UiOiJodHRwczovL2FwaS5hbWF6b24uY29tL2F1dGgvbzIvdG9rZW4ifSwiY2xpZW50SWQiOiJhbXpuMS5hcHBsaWNhdGlvbi1vYTItY2xpZW50LjVmMDg0YjU0MDhiYzQ1MDRiODZlYzRkODRmODQ5MTZjIiwiYXBwRmFtaWx5SWQiOiJhbXpuMS5hcHBsaWNhdGlvbi5hYjRjNzk4YWNiOTI0NjQ2YjlhNjM5ZTc2NzlkYTE3NSIsImlzcyI6IkFtYXpvbiIsInR5cGUiOiJBUElLZXkiLCJwa2ciOiJjb20uaGFzaGN1YmUubWFoam9uZ2ZyZWUiLCJhcHBWYXJpYW50SWQiOiJhbXpuMS5hcHBsaWNhdGlvbi1jbGllbnQuMzNiYzk2MWViZmY0NDc4ZDg5OGFjODc5YmI5M2U0NWYiLCJ0cnVzdFBvb2wiOm51bGwsImFwcHNpZ1NoYTI1NiI6IkQ5OjgwOjlBOjA1OjlFOjdGOjJFOkU3OjE2OjIyOjEyOjI3OjQwOjE0OjYzOkE0OjVCOkJFOjY2OjlFOkJGOjUzOjZBOjFFOjBDOkJCOjA3OjVEOjUxOjRGOjZGOjY1IiwiYXBwc2lnIjoiMkI6MUM6RjU6NUE6RDA6RkM6NzE6NkU6MkU6OUI6MEM6QzA6RDg6RjI6QUM6QjIiLCJhcHBJZCI6ImFtem4xLmFwcGxpY2F0aW9uLWNsaWVudC4zM2JjOTYxZWJmZjQ0NzhkODk4YWM4NzliYjkzZTQ1ZiIsImlkIjoiNGQ4YjQxMDctMGQ4Ni00Y2Q5LTk4NmItMTBmY2QyY2EzOWE5IiwiaWF0IjoiMTQ4NDA0ODAxNDE3MSJ9.NgRHs3KsTtCViByqUYDdOwFsEZ4CZEmEyi2mWay6jYDmPqhPxEeq3KGHc49XMknitOnzw2ct741LaQJ8/klWQp+U4eDMuWrgKRFMgrR+Gzg56uniL3HhJJlU+o92PBU7mt2obc8dwSdQwU0CR/ILJDCetlroyrs+0JWUOaqAeB3RyrKVwxGgCypZtYlZV61q8xFTvK9/z/bt/KDmxwE5yeymK5bmXspoZlUDyoBUDGULzfX9lsqeoaw3eF0fD4URN9tKAkqP+RGxgmmrlUfaReHDIBzwSApUwEg7xvv4BNlu8XQCk6cPdp8EwAjQLlLjdlPqJt0FyvZ9EtpYLH9NIg==\n';
    // jscs:enable

    expect(file.toString()).to.equal(key);
  });

  check('onesignal app id', function () {
    expect(manifest.android.onesignalAppID).to
      .equal('99c70237-8941-40b5-9dd9-da76090a83c5');
  });
});
