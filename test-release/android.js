// jshint node:true

describe('Android Release -', function () {
  'use strict';

  var manifest = require('../manifest'),
    chai = require('chai'),
    expect = chai.expect,
    firebase_config = jsio('../' + manifest.addons.firebase.android),
    check = it;

  check('google project id', function () {
    expect(manifest.android.googleProjectNo).to.equal('559142647925');
  });

  check('facebook app id', function () {
    expect(manifest.android.facebookAppID).to.equal('180709025702831');
  });

  check('facebook app name', function () {
    expect(manifest.android.facebookDisplayName).to.equal('Mahjong Quest');
  });

  check('flurry key', function () {
    expect(manifest.android.flurryKey).to.equal('X2TBZ3T7D3TNVX7RW28F');
  });

  check('gameanalytics game key', function () {
    expect(manifest.android.gameanalyticsGameKey).to
      .equal('7ff01fffe36cfc9f9183dbc2593b93fa');
  });

  check('gameanalytics secret', function () {
    expect(manifest.android.gameanalyticsSecretKey).to
      .equal('1085102d8e8e48c6aca88792817b8212c5207ab8');
  });

  check('gameplay id', function () {
    expect(manifest.android.gameplayID).to
      .equal('559142647925');
  });

  check('App Links are correct', function () {
    expect(manifest.android.app_links[0].scheme).to
      .equal('https');

    expect(manifest.android.app_links[0].host).to
      .equal('mahjongquestfree.com');

    expect(manifest.android.app_links[0].pathPrefix).to
      .equal('/play');
  });

  check('Firecase config', function () {
    var client_info = firebase_config.client[0].client_info;

    expect(firebase_config.project_info.project_number).to
      .equal('801213421495');
    expect(client_info.mobilesdk_app_id).to
      .equal('1:801213421495:android:91ee77df51050cf1');
    expect(client_info.android_client_info.package_name).to
      .equal('com.hashcube.mahjongfree');
  });

  check('install_store', function () {
    expect(manifest.android.install_store).to.equal('android');
  });

  check('freshchat app ID', function () {
    expect(manifest.android.freshchatAppID).to
      .equal('0a7cc14c-18df-4d8a-a28f-e31326e7328f');
  });

  check('freshchat app key', function () {
    expect(manifest.android.freshchatAppKey).to
      .equal('4ae2306c-1273-4e30-a82e-79e155202545');
  });

  check('freshchat app tag', function () {
    expect(manifest.android.freshchatTag).to
      .equal('mahjong_quest');
  });

  check('package name', function () {
    expect(manifest.android.packageName).to.equal('com.hashcube.mahjongfree');
  });

  check('kochava key', function () {
    expect(manifest.android.kochavaAppGUID).to
      .equal('komahjong-quest-android-xoqgx1n1');
  });

  check('onesignal app id', function () {
    expect(manifest.android.onesignalAppID).to
      .equal('80107e2d-cd1f-402d-ab93-d7cd22df3aa9');
  });

  check('billing signature', function () {
    // jscs:disable
    expect(manifest.android.billingSignature).to.equal('MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnKTJE5LAEpNnnl59RpqHkYyKhivF3AQqNdEmd50cbKuF7U/8pj8EhG2enoIUSzBa8eM4Fx+53PhEOmfnZrS4ttQdTPCCanhqgTEZyIziNigmHS5hLwPm1C2AkfQ5Q8zX/91tuiryf4Vh6Ak4wCmjL2ks58/2tQyHGu6osLkOe+W/5mYvNjO10+EH+NRqs0A/4pf91+fh22ZoqMmUNq1+6xjwZhkloOpOOaad+YvqZ/v4r99XmL8C9ryx9987vvm5b69Ry+hXr3SXaGoS0ffW/svv7HFEBtwJnd3KDK738K5+3iAitRho4lGhYJh5AaTPY/Z/N8auRIq5WXh4spghJQIDAQAB');
    // jscs:enable
  });

  check('crashlytics key', function () {
    expect(manifest.android.crashlyticsKey).to
      .equal('dcef80eddb727b66936ac9b44bf876bc0d55c690');
  });
});
