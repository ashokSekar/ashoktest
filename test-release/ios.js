// jshint node:true

describe('IOS Release -', function () {
  'use strict';

  var fs = require('fs'),
    plist = require('plist'),
    manifest = require('../manifest'),
    chai = require('chai'),
    expect = chai.expect,
    check = it;

  check('apple id', function () {
    expect(manifest.ios.appleID).to.equal('1227443330');
  });

  check('team id', function () {
    expect(manifest.ios.teamID).to.equal('CQ9DS8QV5K');
  });

  check('bundle id', function () {
    expect(manifest.ios.bundleID).to.equal('com.hashcube.mahjongfree');
  });

  check('facebook app id', function () {
    expect(manifest.addons.facebook.facebookAppID).to.equal('180709025702831');
  });

  check('facebook app name', function () {
    expect(manifest.addons.facebook.facebookDisplayName).to
      .equal('Mahjong Quest');
  });

  check('flurry key', function () {
    expect(manifest.ios.flurryKey).to.equal('WDCDBN67P8VXHVCKXC7X');
  });

  check('firebase config file', function () {
    var obj = plist.parse(fs.readFileSync('./' + manifest.addons.firebase.ios,
      'utf8'));

    expect(obj.GCM_SENDER_ID).to
      .equal('801213421495');
  });

  check('freshchat app ID', function () {
    expect(manifest.ios.freshchatAppID).to
      .equal('0a7cc14c-18df-4d8a-a28f-e31326e7328f');
  });

  check('freshchat app key', function () {
    expect(manifest.ios.freshchatAppKey).to
      .equal('4ae2306c-1273-4e30-a82e-79e155202545');
  });

  check('freshchat app tag', function () {
    expect(manifest.ios.freshchatTag).to
      .equal('mahjong_quest');
  });

  check('kochava key', function () {
    expect(manifest.ios.kochavaAppGUID).to
      .equal('komahjong-quest-ios-o2e5w');
  });

  check('onesignal app id', function () {
    expect(manifest.ios.onesignalAppID).to
      .equal('90467599-e18c-4f77-95bb-4db7c7747bba');
  });
  check('crashlytics key', function () {
    expect(manifest.ios.crashlyticsKey).to
      .equal('dcef80eddb727b66936ac9b44bf876bc0d55c690');
  });
  check('crashlytics build Secret', function () {
    expect(manifest.ios.crashlyticsBuildSecret).to
      .equal('fdbe9865d4bcc5a6091b3bd287245565a6da7' +
        '3c69a513bfde3b81de853f63fb2');
  });
});
