'use strict';

var supertest = require('supertest'),
  assert = require('assert'),
  path = require('path'),
  util_test = require('./lib/util'),
  express = require('express');

describe('Server', function() {
  var server, config, logger,
    remove = function () {
      if (server) {
        server.running.close();
      }

      util_test.removeFromCache(path.join(__dirname, '../server.js'));
    },
    add = function () {
      server = require('../server');
    },
    reCache = function () {
      remove();
      add();
    },
    setDummyRoute = function () {
      server.get('/test', function (req, res) {
        res.json({test: true});
      });
    };

  before(function () {
    config = require('quest/resources/config');
    logger = require('quest/modules/debug')('SERVER');
  });

  afterEach(remove);

  it('should set proper port', function () {
    add();
    assert.equal(3002, server.get('port'));
  });

  it('should set env to test', function () {
    process.env.NODE_ENV = 'test';
    reCache();
    assert.equal(server.get('env'), 'test');
  });

  it('should set env to development', function () {
    process.env.NODE_ENV = 'development';
    reCache();

    assert.equal(server.get('env'), 'development');
  });

  it('should set env to development', function () {
    process.env.NODE_ENV = 'development';
    reCache();

    assert.equal(server.get('env'), 'development');
  });

  it('should require new relic', function () {
    process.env.NODE_ENV = 'production';
    reCache();
    assert.strictEqual(!!require.cache[
      path.resolve('./node_modules/newrelic/index.js')
    ], true);
  });

  it('should set proper headers for CORS', function (done) {
    var access_control = config.access_control;

    process.env.NODE_ENV = 'development';
    reCache();
    setDummyRoute();
    supertest.agent(server)
      .get('/test')
      .then(function (resp) {
        assert.strictEqual(resp.headers['access-control-allow-origin'],
          access_control.origin);
        assert.strictEqual(resp.headers['access-control-allow-headers'],
          access_control.headers);
        done();
      });
  });

  describe('jsErrorHandle()', function () {
    it('should send error response with proper message', function (done) {
      var
        err_msg = 'Error happened',
        res = {
          error: function (status) {
            assert.strictEqual(err_msg, status.error);
            done();
          }
        },
        req = {
          body: {
            uid: 1234
          }
        },
        err = {error: err_msg};

      process.env.NODE_ENV = 'test';
      reCache();
      express(req, res, function () {});
      server._jsErrorHandler(err, req, res);
    });
  });
});
