/* global global, process, require */

global.assert = require('assert');

global.CONFIG = {
  shortName: 'tests'
};

process.env.NODE_ENV = 'test';

require('./mock/mockDevice').setup();
require('./mock/mockEngine').setup();
require('./mock/mockGlobals').setup();
