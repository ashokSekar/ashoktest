/* global tile1*/

jsio('import src.adventuremap.extra.tile as tile1');

describe('Map Tile', function () {
  'use strict';

  describe('directory()', function () {
    it('should return tiles from map 6', function () {
      var val = tile1.directory(13, 20);

      assert.strictEqual(val, 'resources/images/tiles/map6/0_13.png');
    });
  });
});
