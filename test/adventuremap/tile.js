/* global tile*/

jsio('import src.adventuremap.tile as tile');

describe('Map Tile', function () {
  'use strict';

  describe('directory()', function () {
    it('should return tiles from top', function () {
      var val = tile.directory(9, 2);

      assert.strictEqual(val, 'resources/images/tiles/comingsoon/2_9.png');
    });

    it('should return tiles from map 4', function () {
      var val = tile.directory(9, 335);

      assert.strictEqual(val, 'resources/images/tiles/map9/9_9.png');
    });

    it('should return tiles from map4', function () {
      var val = tile.directory(9, 255);

      assert.strictEqual(val, 'resources/images/tiles/map2/15_9.png');
    });
  });
});
