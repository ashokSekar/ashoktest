/* global _, style, Puzzle, PuzzleBg, Hud, QuestPuzzle, PuzzleModel, User,
   dh_timer, shuffle, sounds, effects, event_manager, Tutorial, game_intro,
   animate, util_test, loading, Promise, Toast, retry_popup, history,
   popup_manager, device, util, settings, Tile, GoalsPopup */

jsio('import util.underscore as _');
jsio('import DevkitHelper.loading as loading');
jsio('import test.lib.util as util_test');
jsio('import quest.lib.bluebird as Promise');
jsio('import device');

describe('Puzzle Screen', function () {
  'use strict';

  var puzzle_screen,
    puzzle_model,
    toast,
    prepare = function () {
      jsio('import DevkitHelper.tutorial as Tutorial');
      GC.app = {
        set3DTouchPending: function () {},
        pop: function () {},
        tutorial: new Tutorial({}),
        engine: {
          subscribe: function () {}
        }
      };

      jsio('import DevkitHelper.style as style');
      style.init = function () {
        return;
      };
      jsio('import src.views.hud as Hud');
      jsio('import src.views.tile as Tile');
      jsio('import src.views.puzzle_bg as PuzzleBg');
      jsio('import DevkitHelper.timer as dh_timer');
      jsio('import quest.screens.puzzle as QuestPuzzle');
      jsio('import src.views.popups.shuffle as shuffle');
      jsio('import DevkitHelper.event_manager as event_manager');
      jsio('import DevkitHelper.history as history');
      jsio('import src.modules.game_intro as game_intro');
      jsio('import src.views.toast as Toast');
      jsio('import src.views.popups.retry_puzzle_load as retry_popup');
      jsio('import quest.modules.popup_manager as popup_manager');
      jsio('import src.views.popups.goals as GoalsPopup');

      // Override those function called using supr
      QuestPuzzle.prototype.pause = function () {};
      QuestPuzzle.prototype.resume = function () {};

      jsio('import quest.modules.sounds as sounds');
      jsio('import src.models.puzzle as PuzzleModel');
      jsio('import src.screens.puzzle as Puzzle');
      jsio('import quest.models.user as User');
      jsio('import effects');
      jsio('import quest.views.popups.settings as settings');
      GC.app.user = new User();
      GC.app.user.set({
        social_facebook: true,
        tooltip_index: 0
      });
      event_manager.register('', []);
      jsio('import quest.modules.util as util');
      util.getSafeArea = function () {
        return {
          top: 44,
          bottom: 34
        };
      };
    },
    init = function () {
      jsio('import animate');
      puzzle_model = new PuzzleModel();
      puzzle_model.generateSolvableGame = function () {
        return true;
      };
      puzzle_screen = new Puzzle();
      puzzle_screen.model = puzzle_model;
      toast = new Toast();

      puzzle_screen.toast_viewpool.obtainView = function () {
        toast = new Toast();
        toast.build = function () {
        };
        return toast;
      };
    };

  before(prepare);
  beforeEach(init);
  afterEach(function () {
    util_test.removeFromJSIOCache('src/animate.js');
  });

  describe('init()', function () {
    it('should create child subviews', function () {
      assert.strictEqual(true, puzzle_screen.puzzle_bg instanceof PuzzleBg);
      assert.strictEqual(true, puzzle_screen.hud instanceof Hud);
    });
  });

  describe('build()', function () {
    it('should build puzzle_bg', function (done) {
      puzzle_screen.puzzle_bg.build = done;
      puzzle_screen.build();
    });

    it('should build hud', function (done) {
      puzzle_screen.hud.build = function () {
        done();
      };
      puzzle_screen.build();
    });

    it('should add pause btn as subview to hud', function (done) {
      puzzle_screen.hud.addSubview = function () {
        done();
      };
      puzzle_screen.build();
    });

    it('should add listener to change of ongoing', function (done) {
      puzzle_screen.toggleHandleEvents = function () {
        done();
      };
      puzzle_screen.build();

      puzzle_screen.model.set('ongoing', 1);
    });

    it('should add listener for out-of-limit', function (done) {
      puzzle_screen.outOfLimit = done;
      puzzle_screen.build();

      puzzle_screen.model.emit('out-of-limit');
    });

    it('should add listener for auto-shuffle', function (done) {
      puzzle_screen.autoShuffle = done;
      puzzle_screen.build();

      puzzle_screen.model.emit('auto-shuffle');
    });

    it('should add listener for tiles-matched', function (done) {
      puzzle_screen.onTilesMatched = done;
      puzzle_screen.build();

      puzzle_screen.model.emit('tiles-matched');
    });

    it('should have listener for puzzle-loaded', function (done) {
      var cache = puzzle_screen.model.on;

      puzzle_screen.model.on = function (evt) {
        if (evt === 'puzzle-loaded') {
          puzzle_screen.model.on = cache;
          done();
        }
      };
      puzzle_screen.onLoad = done;
      puzzle_screen.build();
    });
  });

  describe('clearAnimations()', function () {
    it('should commit animations', function (done) {
      var cache = animate.getGroup,
        anim_obj = {
          commit: function () {
            done();
          }
        };

      animate.getGroup = function () {
        animate.getGroup = cache;
        return anim_obj;
      };

      puzzle_screen.clearAnimations();
    });

    it('should unregister lock_key_timers', function (done) {
      var cache = animate.getGroup,
        cache2 = dh_timer.unregister,
        anim_obj = {
          commit: function () {}
        },
        lock_key_timer_array = ['id_one'];

      puzzle_screen._setLockKeyTimerIds(lock_key_timer_array);

      animate.getGroup = function () {
        animate.getGroup = cache;
        return anim_obj;
      };

      dh_timer.unregister = function (ids) {
        dh_timer.unregister = cache2;
        assert.deepEqual(lock_key_timer_array, ids);
        done();
      };

      puzzle_screen.clearAnimations();
    });
  });

  describe('gameOver()', function () {
    it('should emit gameOver for first level', function (done) {
      var cache = event_manager.emit;

      puzzle_screen.gameover.build = function () { };
      event_manager.emit = function (param, data) {
        event_manager.emit = cache;
        if (param === 'level-first-after' && data === 'complete') {
          done();
        }
      };
      GC.app.user.set('curr_ms', 1);
      puzzle_screen.gameOver(null, function () {});
    });
  });
  describe('shuffle()', function () {
    it('should build toast for shuffle', function (done) {
      var cache = toast.build;

      puzzle_screen.toast_viewpool.obtainView = function () {
        toast = new Toast();
        toast.build = function (view, type, opts) {
          toast.build = cache;
          assert.strictEqual('puzzle_shuffle', type);
          assert.deepEqual({
            text_type: 'shuffle_progress',
            bg: 'puzzlescreen/shuffle_overlay',
            offsetY: 75
          }, opts);
          done();
        };
        return toast;
      };
      puzzle_screen.shuffle();
    });

    it('should clean toast on tiles-update', function (done) {
      puzzle_screen.toast_viewpool.obtainView = function () {
        toast = new Toast();
        toast.build = function (view, type, opts) {
          assert.strictEqual('puzzle_shuffle', type);
          assert.deepEqual({
            text_type: 'shuffle_progress',
            bg: 'puzzlescreen/shuffle_overlay',
            offsetY: 75
          }, opts);
        };
        toast.clean = function () {
          done();
        };
        return toast;
      };

      puzzle_screen.shuffle();
      puzzle_screen.model.emit('tiles-update');
    });

    it('should play sound and stop', function (done) {
      var play_cache = sounds.play,
        stop_cache = sounds.stop,
        i = 0;

      puzzle_screen.toast_viewpool.obtainView = function () {
        toast = new Toast();
        toast.build = function (view, type, opts) {
          assert.strictEqual('puzzle_shuffle', type);
          assert.deepEqual({
            text_type: 'shuffle_progress',
            bg: 'puzzlescreen/shuffle_overlay',
            offsetY: 75
          }, opts);
        };
        toast.clean = function () {
        };
        return toast;
      };

      sounds.play = function (name) {
        sounds.play = play_cache;
        if (name === 'shuffle') {
          i++;
        }
      };

      sounds.stop = function (name) {
        sounds.stop = stop_cache;
        done(name === 'shuffle' && i === 1 ?
          undefined : 'error');
      };

      puzzle_screen.shuffle();
      puzzle_screen.model.emit('tiles-update');
    });
  });

  describe('load()', function () {
    it('should register listener for loading_elapsed change', function (done) {
      var cache = puzzle_screen.onLoadDelay,
        cache_load = puzzle_screen.model.loadPuzzle;

      puzzle_screen.onLoadDelay = function () {
        puzzle_screen.onLoadDelay = cache;
        done();
      };
      puzzle_screen.model.loadPuzzle = function () {
        puzzle_screen.model.loadPuzzle = cache_load;
        return Promise.resolve(true);
      };
      puzzle_screen.build();
      puzzle_screen.load({init: []});
      puzzle_screen.model.increment('loading_elapsed');
    });

    it('should call showtip if init length zero', function (done) {
      var cache = puzzle_screen.onLoadDelay,
        cache_load = puzzle_screen.model.loadPuzzle,
        cache_show_tip = puzzle_screen.showTip;

      puzzle_screen.onLoadDelay = function () {
        puzzle_screen.onLoadDelay = cache;
      };
      puzzle_screen.model.loadPuzzle = function () {
        puzzle_screen.model.loadPuzzle = cache_load;
        return Promise.resolve(true);
      };
      puzzle_screen.showTip = function () {
        puzzle_screen.showTip = cache_show_tip;
        done();
      };
      puzzle_screen.build();
      puzzle_screen.load({init: []});
      puzzle_screen.model.increment('loading_elapsed');
    });

    it('should not call showTip if init has value', function (done) {
      var cache = puzzle_screen.onLoadDelay,
        cache_load = puzzle_screen.model.loadPuzzle,
        cache_show_tip = puzzle_screen.showTip;

      puzzle_screen.onLoadDelay = function () {
        puzzle_screen.onLoadDelay = cache;
      };
      puzzle_screen.model.loadPuzzle = function () {
        puzzle_screen.model.loadPuzzle = cache_load;
        return Promise.resolve(true);
      };
      puzzle_screen.showTip = function () {
        done('error');
      };
      puzzle_screen.build();
      puzzle_screen.load({init: [{}, {}]});
      puzzle_screen.model.increment('loading_elapsed');
      puzzle_screen.showTip = cache_show_tip;
      done();
    });
  });

  describe('onLoadDelay()', function () {
    it('should build toast if loading time exceeds info ' +
      'timeout', function (done) {
        var cache = puzzle_screen.model.removeListener;

        puzzle_screen.toast_viewpool.obtainView = function () {
          var toast = new Toast();

          toast.build = function (view, type, opts) {
            done(opts.text_type === 'puzzle_load' ? undefined : 'error');
          };
          return toast;
        };

        puzzle_screen.model.removeListener = function (evt) {
          puzzle_screen.model.removeListener = cache;
          assert.strictEqual(evt, 'change:loading_elapsed');
        };
        puzzle_screen.onLoadDelay(4);
      }
    );

    it('shouldnt do anything if loading time less than info message' +
      'timeout', function (done) {
        var cache = puzzle_screen.model.removeListener,
          cache_build = toast.build;

        puzzle_screen.model.removeListener = function () {
          puzzle_screen.model.removeListener = cache;
          done('error');
        };
        toast.build = function () {
          toast.build = cache_build;
          done('error');
        };
        puzzle_screen.onLoadDelay(0);
        puzzle_screen.model.removeListener = cache;
        toast.build = cache_build;
        done();
      }
    );
  });

  describe('afterLoading', function () {
    it('should release the toast views', function (done) {
      var cache_clean = puzzle_screen.toast_viewpool.releaseAllViews;

      puzzle_screen.toast_viewpool.releaseAllViews = function () {
        puzzle_screen.toast_viewpool.releaseAllViews = cache_clean;
        done();
      };
      puzzle_screen.afterLoading();
    });

    it('should set busy if puzzle type is not classic', function (done) {
      var cache_clean = puzzle_screen.toast_viewpool.releaseAllViews,
        cache_busy = history.setBusy,
        get_cache = puzzle_screen.model.get;

      puzzle_screen.toast_viewpool.releaseAllViews = function () {
        puzzle_screen.toast_viewpool.releaseAllViews = cache_clean;
      };

      puzzle_screen.model.get = function () {
        puzzle_screen.model.get = get_cache;
        return 'non classic';
      };

      history.setBusy = function () {
        history.setBusy = cache_busy;
        done();
      };
      puzzle_screen.afterLoading();
    });

    it('should not set busy if puzzle type is classic', function (done) {
      var cache_clean = puzzle_screen.toast_viewpool.releaseAllViews,
        cache_busy = history.setBusy,
        get_cache = puzzle_screen.model.get;

      puzzle_screen.toast_viewpool.releaseAllViews = function () {
        puzzle_screen.toast_viewpool.releaseAllViews = cache_clean;
      };

      puzzle_screen.model.get = function () {
        puzzle_screen.model.get = get_cache;
        return 'classic';
      };

      history.setBusy = function () {
        done('error');
      };
      puzzle_screen.afterLoading();
      history.setBusy = cache_busy;
      done();
    });
  });
  describe('onLoad()', function () {
    it('should remove change:loading_elapsed listener', function () {
        var cache = puzzle_screen.model.removeListener;

        puzzle_screen.model.removeListener = function (evt) {
          puzzle_screen.model.removeListener = cache;
          assert.strictEqual(evt, 'change:loading_elapsed');
        };
        puzzle_screen.onLoad();
      }
    );
  });

  describe('buildGoalPopup', function () {
    it('should call afterGoalClose once popup close', function (done) {
      var cache = puzzle_screen.afterGoalClose,
        get_cache = puzzle_screen.model.get;

      puzzle_screen.goal_popup = new GoalsPopup();
      puzzle_screen.model.get = function () {
        puzzle_screen.model.get = get_cache;
        return 'classic';
      };

      puzzle_screen.afterGoalClose = function () {
        puzzle_screen.afterGoalClose = cache;
        done();
      };
      puzzle_screen.goal_popup.build = function () {};
      puzzle_screen.buildGoalPopup();
      popup_manager.emit('goals:close');
    });
  });

  describe('afterGoalClose', function () {
    it('should call load tutorial', function (done) {
      var cache = puzzle_screen.loadTutorial;

      puzzle_screen.loadTutorial = function () {
        puzzle_screen.loadTutorial = cache;
        done();
      };
      puzzle_screen.afterGoalClose();
    });
  });

  describe('loadTutorial', function () {
    it('should not build the tutorial for golden tile with' +
        'correct opts', function (done) {
      var cache_register = game_intro.register;

      puzzle_screen.model.set('type', 'golden');
      game_intro.register = function () {
        game_intro.register = cache_register;
        done('err');
      };
      puzzle_screen.loadTutorial();
      done();
      puzzle_screen.model.set('type', null);
    });
    it('should build the tutorial if not golden tile', function (done) {
        var cache_register = game_intro.register;

        puzzle_screen.model.set('type', 'jewels');
        game_intro.register = function () {
          game_intro.register = cache_register;
          done();
        };
        puzzle_screen.loadTutorial();
        puzzle_screen.model.set('type', null);
      });
  });
  describe('onUILoad()', function () {
    it('should not call game intro for golden tile', function (done) {
      var cache = game_intro.register;

      puzzle_screen.model.set('type', 'golden');
      game_intro.register = function () {
        game_intro.register = cache;
        done('err');
      };
      puzzle_screen.goal_popup = new GoalsPopup();
      puzzle_screen.goal_popup.build = function () { };
      puzzle_screen.onUILoad();
      puzzle_screen.model.set('type', null);
      done();
    });

    it('should invoke toggleHandleEvents', function (done) {
      var cache = game_intro.register,
        cache_handle = puzzle_screen.toggleHandleEvents,
        add_cache = popup_manager.add;

      game_intro.register = function () {
        game_intro.register = cache;
      };

      popup_manager.add = function () {
        popup_manager.add = add_cache;
      };

      puzzle_screen.toggleHandleEvents = function (ong, enable) {
        puzzle_screen.toggleHandleEvents = cache_handle;
        assert.strictEqual(null, ong);
        assert.strictEqual(true, enable);
        done();
      };
      puzzle_screen.onUILoad();
    });

    it('should build goal popup', function (done) {
      var cache = game_intro.register,
        cache_handle = puzzle_screen.toggleHandleEvents;

      game_intro.register = function () {
        game_intro.register = cache;
      };

      puzzle_screen.toggleHandleEvents = function (ong, enable) {
        puzzle_screen.toggleHandleEvents = cache_handle;
        assert.strictEqual(null, ong);
        assert.strictEqual(true, enable);
      };
      puzzle_screen.goal_popup = new GoalsPopup();
      puzzle_screen.goal_popup.build = function () {
        done();
      };
      puzzle_screen.onUILoad();
    });

    it('should not build goal popup if puzzle id classic', function (done) {
      var cache = game_intro.register,
        cache_handle = puzzle_screen.toggleHandleEvents,
        get_cache = puzzle_screen.model.get;

      game_intro.register = function () {
        game_intro.register = cache;
      };

      puzzle_screen.model.get = function () {
        puzzle_screen.model.get = get_cache;
        return 'classic';
      };

      puzzle_screen.toggleHandleEvents = function (ong, enable) {
        puzzle_screen.toggleHandleEvents = cache_handle;
        assert.strictEqual(null, ong);
        assert.strictEqual(true, enable);
      };
      puzzle_screen.goal_popup = new GoalsPopup();
      puzzle_screen.goal_popup.build = function () {
        done('error');
      };
      puzzle_screen.onUILoad();
      done();
    });
  });

  describe('commitEffects()', function () {
    it('should commit effects', function () {
      var cache = effects.commitEffects;

      effects.commitEffects = function (view, name) {
        assert.strictEqual('hover', name);
        effects.commitEffects = cache;
      };
      puzzle_screen.commitEffects();
    });
  });

  describe('pause()', function () {
    it('should not set tile_selected to null if swap mode', function () {
      puzzle_screen.model.set('mode', 'swap');
      puzzle_screen.model.set('tile_selected', 'xyz');
      puzzle_screen.pause();
      assert.strictEqual('xyz', puzzle_screen.model.get('tile_selected'));
      puzzle_screen.model.set('mode', 'null');
    });

    it('should call history add to null if swap mode', function (done) {
      var cache = history.add;

      history.add = function () {
        history.add = cache;
        done();
      };

      puzzle_screen.model.set('mode', 'swap');
      puzzle_screen.model.set('tile_selected', 'xyz');
      puzzle_screen.pause();
      assert.strictEqual('xyz', puzzle_screen.model.get('tile_selected'));
      puzzle_screen.model.set('mode', 'null');
    });

    it('should save puzzle state if swap', function (done) {
      var cache = history.add,
        save_cache = puzzle_screen.model.savePuzzleData;

      history.add = function () {
        history.add = cache;
      };

      puzzle_screen.model.savePuzzleData = function () {
        puzzle_screen.model.savePuzzleData = save_cache;
        done();
      };

      puzzle_screen.model.set('mode', 'swap');
      puzzle_screen.model.set('tile_selected', 'xyz');
      puzzle_screen.pause();
      assert.strictEqual('xyz', puzzle_screen.model.get('tile_selected'));
      puzzle_screen.model.set('mode', 'null');
    });

    it('should call cb to null if swap mode', function (done) {
      puzzle_screen.model.set('mode', 'swap');
      puzzle_screen.model.set('tile_selected', 'xyz');
      puzzle_screen.pause({
        fire: function () {
          done();
        }
      });
      assert.strictEqual('xyz', puzzle_screen.model.get('tile_selected'));
      puzzle_screen.model.set('mode', 'null');
    });

    it('should not set set puzzle_paused to true if swap mode', function () {
      puzzle_screen.model.set('mode', 'swap');
      puzzle_screen.pause();
      assert.strictEqual(undefined, puzzle_screen.model.get('puzzle_paused'));
      puzzle_screen.model.set('mode', 'null');
    });

    it('should not increment ongoing if swap mode', function () {
      puzzle_screen.model.set('mode', 'swap');
      puzzle_screen.model.set('ongoing', 0);
      puzzle_screen.pause();
      assert.strictEqual(0, puzzle_screen.model.get('ongoing'));
      puzzle_screen.model.set('mode', 'null');
    });

    it('should set call commit effects', function (done) {
      var cache = puzzle_screen.commitEffects;

      puzzle_screen.commitEffects = function () {
        puzzle_screen.commitEffects = cache;
        done();
      };
      puzzle_screen.model.set('tile_selected', 'xyz');
      puzzle_screen.pause();
    });

    it('should not set tile_selected to null', function () {
      puzzle_screen.model.set('tile_selected', 'xyz');
      puzzle_screen.model.set('type', 'memory');
      puzzle_screen.pause();
      assert.strictEqual('xyz', puzzle_screen.model.get('tile_selected'));
    });

    it('should set puzzle_paused to true', function () {
      puzzle_screen.pause();
      assert.strictEqual(true, puzzle_screen.model.get('puzzle_paused'));
    });

    it('should increment ongoing', function () {
      puzzle_screen.model.set('ongoing', 0);
      puzzle_screen.pause();
      assert.strictEqual(puzzle_screen.model.get('ongoing'), 1);
    });

    it('should call animate pause', function (done) {
      var cache = animate.getGroup,
        anim_obj = {
          now: function () {
            return this;
          },
          wait: function () {
            return this;
          },
          then: function (cb) {
            if (_.isFunction(cb)) {
              cb();
            }
            return this;
          },
          pause: function () {
            done();
          }
        };

      animate.getGroup = function () {
        animate.getGroup = cache;
        return anim_obj;
      };
      puzzle_screen.pause();
    });

    it('should pause tutorial', function (done) {
      var tutorial = GC.app.tutorial,
        cache = tutorial.pause;

      tutorial.pause = function () {
        tutorial.pause = cache;
        done();
      };

      puzzle_screen.pause();
    });

    it('should invoke pauseTimer of model', function (done) {
      puzzle_screen.model.pauseTimers = done;
      puzzle_screen.pause();
    });

    it('should callImmediate outoflimit_build if timeout is ' +
      'set', function (done) {
      var cache = dh_timer.hasTimeout,
        cache_immediate = dh_timer.callImmediate,
        count = 0;

      dh_timer.hasTimeout = function (tag) {
        if (tag === 'outoflimit_build' || tag === 'auto_shuffle') {
          if (count === 1) {
            dh_timer.hasTimeout = cache;
          }
          count++;
          return true;
        }
      };

      dh_timer.callImmediate = function (tag) {
        if (tag === 'outoflimit_build') {
          dh_timer.callImmediate = cache_immediate;
          done();
        }
      };

      puzzle_screen.pause();
    });

    it('should callImmediate substitute if timeout is ' +
      'set', function (done) {
      var cache = dh_timer.hasTimeout,
        cache_immediate = dh_timer.callImmediate,
        count = 0;

      dh_timer.hasTimeout = function (tag) {
        if (tag === 'substitute' || tag === 'auto_shuffle') {
          if (count === 1) {
            dh_timer.hasTimeout = cache;
          }
          count++;
          return true;
        }
      };

      dh_timer.callImmediate = function (tag) {
        if (tag === 'substitute') {
          dh_timer.callImmediate = cache_immediate;
          done();
        }
      };

      puzzle_screen.pause();
    });
  });

  describe('resume()', function () {
    it('should invoke resumeTimer of model', function (done) {
      puzzle_screen.model.resumeTimers = done;
      puzzle_screen.resume();
    });
    it('should set puzzle_paused to false', function () {
      puzzle_screen.resume();
      assert.strictEqual(false, puzzle_screen.model.get('puzzle_paused'));
    });

    it('should decrement ongoing', function () {
      puzzle_screen.model.set('ongoing', 1);
      puzzle_screen.resume();
      assert.strictEqual(puzzle_screen.model.get('ongoing'), 0);
    });

    it('should call animate pause', function (done) {
      var cache = animate.getGroup,
        anim_obj = {
          now: function () {
            return this;
          },
          wait: function () {
            return this;
          },
          then: function (cb) {
            if (_.isFunction(cb)) {
              cb();
            }
            return this;
          },
          pause: function () {
          },
          resume: function () {
            done();
          },
          commit: function () {}
        };

      animate.getGroup = function () {
        animate.getGroup = cache;
        return anim_obj;
      };
      puzzle_screen.resume();
    });

    it('should resume tutorial', function (done) {
      var tutorial = GC.app.tutorial,
        cache = tutorial.resume;

      tutorial.resume = function () {
        tutorial.resume = cache;
        done();
      };

      puzzle_screen.resume();
    });
  });

  describe('outOfLimit()', function () {
    it('should disable and enable toggleHandleEvents', function (done) {
      var cache = puzzle_screen.out_of_limit.build;

      puzzle_screen.out_of_limit.build = function () {
        puzzle_screen.out_of_limit.build = cache;
      };
      puzzle_screen.build();
      dh_timer.mock(5);
      puzzle_screen.model.set('limit_type', 'time');
      puzzle_screen.model.set('time', 40);
      puzzle_screen.model.set('limit', 60);
      puzzle_screen.outOfLimit();
      assert.strictEqual(puzzle_screen.grid.isHandlingEvents(), false);
      dh_timer.timeout(null, function () {
        assert.strictEqual(puzzle_screen.grid.isHandlingEvents(), true);
        done();
      }, 5000);
    });

    it('should disable and enable history', function (done) {
      var cache = puzzle_screen.out_of_limit.build,
        cache_set = history.setBusy,
        cache_reset = history.resetBusy,
        count = 0;

      puzzle_screen.out_of_limit.build = function () {
        puzzle_screen.out_of_limit.build = cache;
      };
      puzzle_screen.build();
      dh_timer.mock(5);
      history.setBusy = function () {
        history.setBusy = cache_set;
        ++count;
      };
      history.resetBusy = function () {
        history.resetBusy = cache_reset;
        done(++count === 2 ? undefined : 'error');
      };
      puzzle_screen.outOfLimit();
    });

    it('should register outoflimit_build timeout', function (done) {
      var cache = dh_timer.timeout;

      dh_timer.timeout = function (tag) {
        dh_timer.timeout = cache;
        done(tag === 'outoflimit_build' ? undefined : 'error');
      };
      puzzle_screen.build();
      puzzle_screen.outOfLimit();
    });

    it('should call build function of out_of_limit popup', function (done) {
      var out_of_limit = puzzle_screen.out_of_limit,
        temp = out_of_limit.build;

      out_of_limit.build = function () {
        done();
        out_of_limit.build = temp;
        return {
          next: function () { }
        };
      };
      dh_timer.mock(1);
      puzzle_screen.outOfLimit();
    });

    it('should not call build function of out_of_limit popup ' +
      'if animation is on but more tiles to match', function (done) {
      var out_of_limit = puzzle_screen.out_of_limit,
        temp = out_of_limit.build,
        cache = animate.getGroup;

      animate.getGroup = function () {
        animate.getGroup = cache;
        return {
          isActive: function () {
              return true;
            }
        };
      };

      out_of_limit.build = function () {
        done();
        out_of_limit.build = temp;
        return {
          next: function () { }
        };
      };
      dh_timer.mock(1);
      puzzle_screen.model.set('pending_matches', 1);
      puzzle_screen.model.set('tile_count', 4);
      puzzle_screen.outOfLimit();
    });
  });

  describe('enableGrid', function () {
    it('should call toggleHandleEvent the grid', function (done) {
      var cache = puzzle_screen.toggleHandleEvents;

      puzzle_screen.toggleHandleEvents = function () {
        puzzle_screen.toggleHandleEvents = cache;
        done();
      };
      puzzle_screen.enableGrid();
    });
  });
  describe('toggleHandleEvents()', function () {
    it('should enable hud events when time remaining', function () {
      puzzle_screen.model.set('limit_type', 'time');
      puzzle_screen.model.set('time', 40);
      puzzle_screen.model.set('limit', 60);
      puzzle_screen.toggleHandleEvents(null, true);
      assert.strictEqual(puzzle_screen.hud.isHandlingEvents(), true);
    });

    it('should disable hud events', function () {
      puzzle_screen.toggleHandleEvents(null, false);
      assert.strictEqual(puzzle_screen.hud.isHandlingEvents(), false);
    });

    it('should disable the grid if time not left', function () {
      puzzle_screen.model.set('limit_type', 'time');
      puzzle_screen.model.set('time', 60);
      puzzle_screen.model.set('limit', 60);
      puzzle_screen.toggleHandleEvents(null, false);
      assert.notStrictEqual(puzzle_screen.grid.isHandlingEvents(), true);
    });

    it('should enable grid if ongoing is zero and time remaining', function () {
      puzzle_screen.model.set('limit_type', 'time');
      puzzle_screen.model.set('time', 40);
      puzzle_screen.model.set('limit', 60);
      puzzle_screen.model.set('pending_matches', 1);
      puzzle_screen.toggleHandleEvents(0, true);
      assert.strictEqual(puzzle_screen.grid.isHandlingEvents(), true);
    });

    it('should enable grid if pending > 0 and time remaining', function () {
      puzzle_screen.model.set('limit_type', 'time');
      puzzle_screen.model.set('time', 40);
      puzzle_screen.model.set('limit', 60);
      puzzle_screen.model.set('pending_matches', 1);
      puzzle_screen.toggleHandleEvents(1, false);
      assert.strictEqual(puzzle_screen.grid.isHandlingEvents(), true);
    });

    it('should disable grid if pending = 0', function () {
      puzzle_screen.model.set('pending_matches', 0);
      puzzle_screen.toggleHandleEvents(1, false);
      assert.strictEqual(puzzle_screen.grid.isHandlingEvents(), false);
    });

    it('should not disable grid if mode is swap' +
      'and time remaining', function () {
      puzzle_screen.model.set('limit_type', 'time');
      puzzle_screen.model.set('time', 40);
      puzzle_screen.model.set('limit', 60);
      puzzle_screen.model.set('pending_matches', 0);
      puzzle_screen.model.set('mode', 'swap');
      puzzle_screen.toggleHandleEvents(1, false);
      assert.strictEqual(puzzle_screen.grid.isHandlingEvents(), true);
      puzzle_screen.model.set('mode', 'null');
    });
  });

  describe('autoShuffle()', function () {
    it('should build shuffle popup', function (done) {
      var cache = shuffle.build;

      shuffle.build = function () {
        shuffle.build = cache;
        done();
      };
      puzzle_screen.autoShuffle(true);
    });
    it('should return false and wont call build ' +
      'shuffle popup', function (done) {
      var cache = shuffle.build,
        cache_has_time_out = dh_timer.hasTimeout;

      dh_timer.hasTimeout = function (key) {
        if (key === 'outoflimit_build') {
          dh_timer.hasTimeout = cache_has_time_out;
          return true;
        }
      };

      shuffle.build = function () {
        shuffle.build = cache;
        done('error');
      };
      done(puzzle_screen.autoShuffle(true) === false ? undefined : 'error');
      shuffle.build = cache;
    });
    it('should pauseTimers', function (done) {
      var cache = shuffle.build,
        cache_time = puzzle_screen.model.pauseTimers;

      puzzle_screen.model.pauseTimers = function () {
        puzzle_screen.model.pauseTimers = cache_time;
        done();
      };

      shuffle.build = function () {
        shuffle.build = cache;
      };
      puzzle_screen.autoShuffle(true);
    });
  });

  describe('onTilesMatched()', function () {
    it('should play sound tile_match by default', function (done) {
      var cache = sounds.play;

      sounds.play = function (param) {
        sounds.play = cache;
        assert.strictEqual('tile_match', param);
        done();
      };

      puzzle_screen.onTilesMatched();
    });

    it('should play sound in given type', function (done) {
      var cache = sounds.play;

      sounds.play = function (param) {
        sounds.play = cache;
        assert.strictEqual('joker_match', param);
        done();
      };

      puzzle_screen.onTilesMatched('joker');
    });
  });

  describe('clean()', function () {
    it('should call tutorial clean', function (done) {
      var cache = GC.app.tutorial.clean,
        cache_show = loading.show;

      loading.show = function () {
        loading.show = cache_show;
      };

      GC.app.tutorial.clean = function () {
        GC.app.tutorial.clean = cache;
        done();
      };

      puzzle_screen.clean();
    });
  });

  describe('cleanViews()', function () {
    it('should call toast clean', function (done) {
      var cache_clean = puzzle_screen.toast_viewpool.releaseAllViews;

      puzzle_screen.toast_viewpool.releaseAllViews = function () {
        puzzle_screen.toast_viewpool.releaseAllViews = cache_clean;
        done();
      };

      puzzle_screen.cleanViews();
    });
  });

  describe('handleLoadFail()', function () {
    it('should call toast clean', function (done) {
      var cache_clean = puzzle_screen.toast_viewpool.releaseAllViews,
        cache_shuffle_fail = puzzle_screen.onShuffleFailed;

      puzzle_screen.toast_viewpool.releaseAllViews = function () {
        puzzle_screen.toast_viewpool.releaseAllViews = cache_clean;
        done();
      };

      puzzle_screen.onShuffleFailed = function () {
        puzzle_screen.onShuffleFailed = cache_shuffle_fail;
      };

      puzzle_screen.handleLoadFail(true);
    });

    it('should call onShuffleFail', function (done) {
      var cache_clean = toast.clean,
        cache_shuffle_fail = puzzle_screen.onShuffleFailed;

      toast.clean = function () {
        toast.clean = cache_clean;
      };

      puzzle_screen.onShuffleFailed = function () {
        puzzle_screen.onShuffleFailed = cache_shuffle_fail;
        done();
      };

      puzzle_screen.handleLoadFail(true);
    });

    it('should call cleanViews', function (done) {
      var cache_clean = puzzle_screen.cleanViews;

      puzzle_screen.cleanViews = function () {
        puzzle_screen.cleanViews = cache_clean;
        done();
      };

      puzzle_screen.handleLoadFail();
    });
  });

  describe('onShuffleFailed()', function () {
    it('should call retry_popup.build', function (done) {
      var cache,
        shuffle_cache = puzzle_screen.onRetryShuffle;

      puzzle_screen.retry_popup = retry_popup;
      cache = puzzle_screen.retry_popup.build;

      puzzle_screen.retry_popup.build = function (type) {
        puzzle_screen.retry_popup.build = cache;
        assert.strictEqual('shuffle', type);
        done();
      };
      puzzle_screen.onRetryShuffle = function () {};
      puzzle_screen.onShuffleFailed();
      puzzle_screen.onRetryShuffle = shuffle_cache;
    });

    it('should call retry shuffle on taping retry', function (done) {
      var cache,
        shuffle_cache = puzzle_screen.shuffle,
        retry_cache = puzzle_screen.onRetryShuffle;

      puzzle_screen.retry_popup = retry_popup;
      cache = puzzle_screen.retry_popup.build;
      puzzle_screen.retry_popup.build = function () {
        puzzle_screen.retry_popup.build = cache;
      };

      puzzle_screen.onRetryShuffle = function () {
        puzzle_screen.onRetryShuffle = retry_cache;
        done();
      };
      puzzle_screen.shuffle = function () {};
      puzzle_screen.onShuffleFailed();
      popup_manager.emit('retry-shuffle');
      puzzle_screen.shuffle = shuffle_cache;
    });
  });

  describe('onRetryShuffle()', function () {
    it('should call model.shuffleAsync', function (done) {
      var cache = puzzle_screen.model.shuffleAsync,
        shuffle_cache = puzzle_screen.shuffle;

      puzzle_screen.model.shuffleAsync = function () {
        puzzle_screen.model.shuffleAsync = cache;
        done();
      };

      puzzle_screen.shuffle = function () {};
      puzzle_screen.onRetryShuffle();
      puzzle_screen.shuffle = shuffle_cache;
    });

    it('should call shuffle once popup close', function (done) {
      var cache = puzzle_screen.shuffle,
        cache2 = animate.getGroup,
        anim_obj = {
          commit: function () {}
        };

      animate.getGroup = function () {
        return anim_obj;
      };

      puzzle_screen.shuffle = function () {
        puzzle_screen.shuffle = cache;
        animate.getGroup = cache2;
        done();
      };
      puzzle_screen.onRetryShuffle();
      popup_manager.emit('retry_puzzle_load:close');
    });
  });

  describe('onRetryHome()', function () {
    it('should call exit', function (done) {
      var cache = puzzle_screen.exit;

      puzzle_screen.model.set('solved', []);
      puzzle_screen.exit = function () {
        puzzle_screen.exit = cache;
        done();
      };

      GC.app.user.incrementLives = function () {
        done('error');
      };
      puzzle_screen.onRetryHome();
    });

    it('should increment life if user started solving', function (done) {
      var user = GC.app.user,
        cache = user.incrementLives;

      puzzle_screen.model.set('solved', [1, 2, 3]);

      user.incrementLives = function () {
        user.incrementLives = cache;
        done();
      };

      puzzle_screen.onRetryHome();
    });
  });

  describe('getContextForTutorial()', function () {
    it('should return 1 tile', function (done) {
      var tiles = [],
        cache_get = puzzle_screen.model.get,
        cache = device.isTablet,
        contexts;

      tiles[1] = [];
      tiles[1][1] = [];

      tiles[1][1][1] = {
        model: {
          isFree: function () {
            return true;
          }
        }
      };
      tiles[1][1][2] = {
        model: {
          isFree: function () {
            return true;
          },
          isJoker: function () {
            return false;
          },
          isVisible: function () {
            return true;
          }
        }
      };

      device.isTablet = true;

      puzzle_screen.model.get = function (key) {
        if (key === 'rows') {
          return 16;
        }
      };
      puzzle_screen.grid.tiles = tiles;
      contexts = puzzle_screen.getContextForTutorial(5);
      done(contexts.length === 1 ? undefined : 'error');
      puzzle_screen.model.get = cache_get;
      device.isTablet = cache;
    });

    it('should return 2 tile', function (done) {
      var tiles = [],
        cache_get = puzzle_screen.model.get,
        contexts;

      tiles[1] = [];
      tiles[1][2] = [];

      tiles[1][2][1] = {
        model: {
          isFree: function () {
            return true;
          },
          isJoker: function () {
            return false;
          },
          isVisibleCompletely: function () {
            return true;
          },
          getCoordinates: function () {
            return [4, 5, 6];
          }
        }
      };

      tiles[1][2][2] = {
        model: {
          isFree: function () {
            return true;
          },
          isJoker: function () {
            return true;
          },
          isVisible: function () {
            return true;
          },
          getCoordinates: function () {
            return [4, 5, 6];
          }
        }
      };

      tiles[1][2][3] = {
        model: {
          isFree: function () {
            return true;
          },
          isJoker: function () {
            return false;
          },
          isVisible: function () {
            return true;
          },
          getCoordinates: function () {
            return [4, 5, 6];
          }
        }
      };

      puzzle_screen.model.get = function (key) {
        if (key === 'rows') {
          return 8;
        }
      };

      puzzle_screen.grid.tiles = tiles;
      contexts = puzzle_screen.getContextForTutorial(28);
      done(contexts.length === 2 ? undefined : 'error');
      puzzle_screen.model.get = cache_get;
    });

    it('should return 2 tile one shouldnt free', function (done) {
      var tiles = [],
        cache_get = puzzle_screen.model.get,
        contexts;

      tiles[1] = [];
      tiles[1][2] = [];

      tiles[1][2][1] = {
        model: {
          isFree: function () {
            return false;
          },
          isVisibleCompletely: function () {
            return true;
          },
          get: function () {
            return 'f1';
          },
          getCoordinates: function () {
            return [4, 5, 6];
          }
        }
      };

      tiles[1][2][0] = {
        model: {
          isFree: function () {
            return true;
          },
          isVisible: function () {
            return true;
          },
          get: function () {
            return 'f2';
          },
          getCoordinates: function () {
            return [10, 5, 6];
          }
        }
      };

      puzzle_screen.model.get = function (key) {
        if (key === 'rows') {
          return 8;
        }
      };

      puzzle_screen.grid.tiles = tiles;
      contexts = puzzle_screen.getContextForTutorial(32);
      done(contexts.length === 2 ? undefined : 'error');
      puzzle_screen.model.get = cache_get;
    });

    it('should return 0 tiles', function (done) {
      var tiles = [],
        cache_get = puzzle_screen.model.get,
        contexts;

      tiles[1] = [];
      tiles[1][2] = [];

      tiles[1][2][1] = {
        model: {
          isFree: function () {
            return false;
          },
          isVisible: function () {
            return false;
          },
          isJoker: function () {
            return false;
          },
          getCoordinates: function () {
            return [4, 5, 6];
          }
        }
      };

      tiles[1][2][0] = {
        model: {
          isFree: function () {
            return false;
          },
          isVisible: function () {
            return false;
          },
          isJoker: function () {
            return false;
          },
          getCoordinates: function () {
            return [4, 5, 6];
          }
        }
      };

      puzzle_screen.model.get = function (key) {
        if (key === 'rows') {
          return 8;
        }
      };

      puzzle_screen.grid.tiles = tiles;
      contexts = puzzle_screen.getContextForTutorial(32);
      done(contexts.length === 0 ? undefined : 'error');
      puzzle_screen.model.get = cache_get;
    });
  });

  describe('substituteOverlay()', function () {
    it('should call substitute_toast.build', function (done) {
      var cache = puzzle_screen.substitute_toast.build;

      puzzle_screen.substitute_toast.build = function () {
        puzzle_screen.substitute_toast.build = cache;
        done();
      };

      puzzle_screen.substituteOverlay();
    });

    it('should call emit powerup-applied', function (done) {
      var cache = puzzle_screen.substitute_toast.build;

      puzzle_screen.substitute_toast.build = function () {
        puzzle_screen.substitute_toast.build = cache;
        done();
      };

      puzzle_screen.model.emit = function (name, cb) {
        if (name === 'powerup-applied' && cb) {
          done();
          cb();
        }
      };
      puzzle_screen.substituteOverlay();
    });
  });

  describe('showTip()', function () {
    it('should call tip toast build', function (done) {
      puzzle_screen.toast_viewpool.obtainView = function () {
        var toast = new Toast();

        toast.build = function (view, type, opts) {
          done(opts.text_type === 'tool_tip_head' ? undefined : 'error');
        };
        return toast;
      };
      puzzle_screen.showTip();
    });

    it('should set user tooltip_index', function (done) {
      var cache = GC.app.user.set,
        cache_get = GC.app.user.get;

      GC.app.user.get = function (key) {
        if (key === 'tooltip_index') {
          GC.app.user.get = cache_get;
          return 0;
        }
      };

      puzzle_screen.toast_viewpool.obtainView = function () {
        var toast = new Toast();

        toast.build = function () {
        };
        return toast;
      };
      GC.app.user.set('tooltip_index', 0);
      GC.app.user.set = function (key, val) {
        GC.app.user.set = cache;
        if (key === 'tooltip_index') {
          assert.strictEqual(val, GC.app.user.get('tooltip_index') + 1);
          done();
        }
      };

      puzzle_screen.showTip();
    });

    it('should set user tooltip_index 0', function (done) {
      var cache = GC.app.user.set;

      puzzle_screen.toast_viewpool.obtainView = function () {
        var toast = new Toast();

        toast.build = function () {
        };
        return toast;
      };
      GC.app.user.set('tooltip_index', 123456);
      GC.app.user.set = function (key, val) {
        if (key === 'tooltip_index' && val === 0) {
          GC.app.user.set = cache;
          done();
        }
      };
      puzzle_screen.showTip();
    });
  });

  describe('showSettings', function () {
    it('should build the setting popup with correct props', function () {
      var cache = settings.build;

      puzzle_screen.model.set('bonus_level', 'three_match');
      puzzle_screen.model.set('ms_no', 5);
      settings.build = function (props) {
        settings.build = cache;
        assert.strictEqual(props.origin, 'puzzle');
        assert.strictEqual(props.bonus_level, 'three_match');
        assert.strictEqual(props.level_no, 5);
      };
      puzzle_screen.showSettings();
    });
  });
  describe('on3DTouch()', function () {
    it('should clean puzzle if no popup present', function (done) {
      var cache = puzzle_screen.clean;

      puzzle_screen.clean = function () {
        puzzle_screen.clean = cache;
        done();
      };
      puzzle_screen.on3DTouch();
    });

    it('should invoke popup.closeAll with cb', function (done) {
      var cache_stack = popup_manager.getStack,
        cache_close = popup_manager.closeAll;

      popup_manager.getStack = function () {
        popup_manager.getStack = cache_stack;
        return [1, 2];
      };

      popup_manager.closeAll = function () {
        popup_manager.closeAll = cache_close;
        puzzle_screen.model.emit('clean-start');
        done();
      };

      puzzle_screen.on3DTouch();
    });

    it('should clean if not cleaned', function (done) {
      var cache_stack = popup_manager.getStack,
        cache_close = popup_manager.closeAll,
        cache = puzzle_screen.clean;

      popup_manager.getStack = function () {
        popup_manager.getStack = cache_stack;
        return [1, 2];
      };

      popup_manager.closeAll = function (cb) {
        popup_manager.closeAll = cache_close;
        cb.fire();
      };

      puzzle_screen.clean = function () {
        puzzle_screen.clean = cache;
        done();
      };

      puzzle_screen.on3DTouch();
    });

    it('shouldnt clean if already cleaned', function (done) {
      var cache_stack = popup_manager.getStack,
        cache_close = popup_manager.closeAll,
        cache = puzzle_screen.clean;

      popup_manager.getStack = function () {
        popup_manager.getStack = cache_stack;
        return [1, 2];
      };

      popup_manager.closeAll = function (cb) {
        popup_manager.closeAll = cache_close;
        puzzle_screen.model.emit('clean-start');
        cb.fire();
      };

      puzzle_screen.clean = function () {
        puzzle_screen.clean = cache;
        done('error');
      };

      puzzle_screen.on3DTouch();
      puzzle_screen.clean = cache;
      done();
    });
  });

  describe('animateKey()', function () {
    it('should not unregister timer for up', function (done) {
      var anim_obj = {
          now: function () {
            return this;
          },
          wait: function () {
            return this;
          },
          then: function (cb) {
            if (_.isFunction(cb)) {
              cb();
            }
            return this;
          }
        },
        tile = {
          getCoordinates: function () {
            return [1, 2, 3];
          },
          get: function () {
          }
        };

      util_test.removeFromJSIOCache('quest/src/screens/puzzle.js');
      util_test.removeFromJSIOCache('src/screens/puzzle.js');
      util_test.removeFromJSIOCache('src/animate.js');

      jsio('import animate');
      util_test.getFromJSIOCache('src/animate.js').exports = function () {
        return anim_obj;
      };
      jsio('import src.screens.puzzle as Puzzle');
      jsio('import DevkitHelper.timer as dh_timer');

      dh_timer.register = function (id, cb) {
        if (_.isFunction(cb)) {
          cb();
        }
      };

      dh_timer.unregister = function (id) {
        if (id === 'move_lock_up') {
          done('error');
        }
      };

      puzzle_screen = new Puzzle();
      puzzle_screen.grid.tiles = [];
      puzzle_screen.grid.tiles[1] = [];
      puzzle_screen.grid.tiles[1][2] = [];
      puzzle_screen.grid.tiles[1][2][3] = new Tile({});

      puzzle_screen.animateKey([1, tile, false]);
      done();
    });
  });

  describe('onShuffleComplete()', function () {
    it('should set shuffle_count to zero', function () {
      puzzle_screen.onShuffleComplete();

      assert.strictEqual(puzzle_screen._getShuffleCount(), 0);
    });
  });
});
