/* global View, ImageView, TextView, style, LoadingScreen */

jsio('import ui.View as View');
jsio('import ui.ImageView as ImageView');
jsio('import ui.TextView as TextView');

describe('loading Screen', function () {
  'use strict';

  var loading_screen,
    prepare = function () {
      GC.app = {
        view: {
          updateOpts: function () {}
        },
        getCurrentView: function () {
          return new View();
        }
      };
      jsio('import DevkitHelper.style as style');
      style.init({
        orientation: 'portrait',
        bound_width: 640,
        bound_height: 960,
        tablet: 0.8,
        phablet: 0.9
      });

      jsio('import quest.screens.loading as LoadingScreen');
    },
    init = function () {
      loading_screen = new LoadingScreen();
    };

  before(prepare);
  beforeEach(init);

  describe('init()', function () {
    it('it should create icon , msg view', function () {
      assert.strictEqual(true, loading_screen.icon instanceof ImageView);
      assert.strictEqual(true, loading_screen.msg instanceof TextView);
    });
  });

  describe('build()', function () {
    it('it should set icon image', function () {
      loading_screen.build();
      assert.strictEqual('resources/images/loading_screen/loading.png',
        loading_screen.icon.getImage().getURL());
    });

    it('it should set msg text', function () {
      loading_screen.build();
      assert.strictEqual('transaction_loading_title',
        loading_screen.msg.getText());
    });
  });
});
