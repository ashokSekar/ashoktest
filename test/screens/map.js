/* global Map, View, User, loader, util_test, style, MapQuest,
PuzzleScreen, _, Promise, retry_popup, popup_manager, Toast, TileStore,
puzzle_module, util, effects,
image, timer, gifting */

jsio('import util.underscore as _');
jsio('import ui.View as View');
jsio('import test.lib.util as util_test');
jsio('import quest.models.user as User');
jsio('import ui.resource.loader as loader');
jsio('import src.modules.cutscene as cutscene');
jsio('import quest.modules.image as image');
jsio('import DevkitHelper.timer as timer');

describe('Map Screen', function () {
  'use strict';

  var map_screen,
    is_completed = false,
    prepare = function () {
      GC.app = {
        user: new User(),
        tutorial: {
          build: function () {
          },
          add: function () {
          },
          isCompleted: function () {
            return is_completed;
          },
          clean: function () {}
        },
        view: {
          updateOpts: function () {}
        },
        updateOpts: function () {},
        getCurrentView: function () {
          return new View();
        },
        engine: {
          subscribe: function () {}
        }
      };
      jsio('import effects as effects');
      jsio('import src.screens.puzzle as PuzzleScreen');
      jsio('import DevkitHelper.style as style');
      jsio('import src.views.popups.retry_puzzle_load as retry_popup');
      jsio('import quest.modules.popup_manager as popup_manager');
      jsio('import quest.modules.puzzle as puzzle_module');
      jsio('import src.views.toast as Toast');
      jsio('import quest.modules.gifting as gifting');

      style.init({
        orientation: 'portrait',
        bound_width: 640,
        bound_height: 960,
        tablet: 0.8,
        phablet: 0.9
      });
      loader.getMap = function () {
        return {
          'resources/images/coins/coin_splatter_01.png': {
            marginRight: 0,
            sheetSize: [1024, 512],
            w: 63,
            x: 932,
            h: 63,
            y: 320,
            scale: 1,
            marginBottom: 0,
            sheet: 'spritesheets/resources-images-coins0.png',
            marginTop: 0,
            marginLeft: 0,
            group: 'resources-images-coins'
          }
        };
      };

      util_test.removeFromJSIOCache('SpriteView.js');
      jsio('import ui.SpriteView as SpriteView');
      jsio('import quest.screens.map as MapQuest');
      jsio('import src.screens.map as Map');
      jsio('import src.views.popups.tile_store as TileStore');
      jsio('import quest.modules.util as util');
      util.getSafeArea = function () {
        return {
          top: 44,
          bottom: 34
        };
      };
    },
    init = function () {
      map_screen = new Map();
    };

  before(prepare);
  beforeEach(init);

  describe('init()', function () {
    it('it should tile_store instnace of TileStore', function (done) {
      map_screen = new MapQuest();
      done(map_screen.tile_store instanceof TileStore ? undefined : 'error');
    });
  });

  describe('build()', function () {
    it('it should call addCustomLoad', function (done) {
      var cache;

      map_screen = new Map(function (ctx, fn) {
        assert.strictEqual(fn, 'build');
      });

      map_screen.map_icon = {
        updateOpts: function () {},
        setState: function () {}
      };
      map_screen.gifting_image = {
        updateOpts: function () {},
        on: function () {}
      };
      map_screen.overlay = {
        updateOpts: function () {},
        hide: function () {}
      };
      cache = map_screen.addCustomLoad;

      map_screen.addCustomLoad = function () {
        map_screen.addCustomLoad = cache;
        done();
      };
      map_screen.build();
    });

    it('it not set start_ms while building bonus map', function () {
      var cache = map_screen.addCustomLoad,
        map_quest = new MapQuest();

      map_screen = new Map(function () { });
      map_screen.map_icon = {
        updateOpts: function () { },
        setState: function () { }
      };

      map_screen.gifting_image = {
        updateOpts: function () { },
        on: function () { }
      };
      map_screen.overlay = {
        updateOpts: function () { },
        hide: function () { }
      };

      GC.app.user.setMs('curr', 15);
      GC.app.user.setMs('max_ms', 20);
      GC.app.user.set('screen_bonus', true);
      map_screen.addCustomLoad = function () {
        map_screen.addCustomLoad = cache;
      };
      map_quest.build = function (param) {
        assert.strictEqual(param, 15);
      };
      map_screen.build();
    });

    it('it set start_ms to max_ms while building map', function () {
      var cache = map_screen.addCustomLoad,
        map_quest = new MapQuest();

      map_screen = new Map(function () {});
      map_screen.map_icon = {
        updateOpts: function () { },
        setState: function () { }
      };
      map_screen.gifting_image = {
        updateOpts: function () { },
        on: function () { }
      };
      map_screen.overlay = {
        updateOpts: function () { },
        hide: function () { }
      };

      GC.app.user.setMs('curr', 5);
      GC.app.user.setMs('max_ms', 20);
      GC.app.user.set('screen_bonus', false);
      map_screen.addCustomLoad = function () {
        map_screen.addCustomLoad = cache;
      };
      map_quest.build = function (param) {
        assert.strictEqual(param, 20);
      };
      map_screen.build();
    });
  });

  describe('showPlayPopup()', function () {
    it('it should invoke supr with third param', function (done) {
      var Base = function (ctxt, fn, param) {
          assert.strictEqual(fn, 'showPlayPopup');
          assert.strictEqual(param[2], 'layout');
          done();
        },
        map_screen = new Map(Base);

      map_screen.showPlayPopup();
    });
  });

  describe('renderPuzzle()', function () {
    it('it should invoke supr with third param', function () {
      var Base = function (ctxt, fn, param) {
          assert.strictEqual(fn, 'renderPuzzle');
          assert.strictEqual(param[1], 'layout');
        },
        map_screen = new Map(Base);

      map_screen.renderPuzzle();
    });
  });

  describe('focusMilestone()', function () {
    it('should invoke parent function if no bonus tutorial', function (done) {
      var Base = function (ctx, fn, param) {
          assert.strictEqual(fn, 'focusMilestone');
          assert.deepEqual([1, 2, 3, false], param);
          done();
        },
        map_screen = new Map(Base),
        cache = map_screen.buildBonusLevelTutorial;

      map_screen.buildBonusLevelTutorial = function () {
        map_screen.buildBonusLevelTutorial = cache;
        return false;
      };
      map_screen.focusMilestone(1, 2, 3, false);
    });

    it('shouldnt invoke parent function if bonus tutorial', function (done) {
      var Base = function () {
          done('error');
        },
        map_screen = new Map(Base),
        cache = map_screen.buildBonusLevelTutorial;

      map_screen.buildBonusLevelTutorial = function () {
        map_screen.buildBonusLevelTutorial = cache;
        return true;
      };
      map_screen.focusMilestone(1, 2, 3);
      done();
    });
  });

  describe('buildBonusLevelTutorial()', function () {
    it('should focus milestone and build tutorial', function (done) {
      var map_screen = new Map(),
        tutorial = GC.app.tutorial,
        cache_puzzle = puzzle_module.getLastUnlockedBonus,
        cache_build = tutorial.build,
        cache_comp = tutorial.isCompleted;

      map_screen.map = {
        focusNodeById: function (id, cb) {
          assert.strictEqual(10, id);
          cb();
        },
        getModel: function () {
          return {
            getNodesById: function () {
              return {
                10: {
                  itemViews: {
                    BonusLevel: {
                      emit: function () {
                      }
                    }
                  }
                }
              };
            }
          };
        }
      };

      puzzle_module.getLastUnlockedBonus = function () {
        puzzle_module.getLastUnlockedBonus = cache_puzzle;

        return 10;
      };

      tutorial.isCompleted = function () {
        tutorial.isCompleted = cache_comp;

        return false;
      };

      popup_manager.getStack = function () {
        return {
          length: 0
        };
      };

      tutorial.build = function (data) {
        tutorial.build = cache_build;
        assert.strictEqual('map', data.type);
        assert.strictEqual(true, _.has(data.positions, 'bonus_level_intro'));
        data.on_cancel({
          fire: function () {}
        });
        done();
      };

      GC.app.user.setMs('max', 11);
      map_screen.buildBonusLevelTutorial();
    });

    it('should emit InputSelect of bonus level view', function (done) {
      var map_screen = new Map(),
        tutorial = GC.app.tutorial,
        cache_puzzle = puzzle_module.getLastUnlockedBonus,
        cache_build = tutorial.build,
        cache_comp = tutorial.isCompleted;

      map_screen.map = {
        focusNodeById: function (id, cb) {
          assert.strictEqual(10, id);
          cb();
        },
        getModel: function () {
          return {
            getNodesById: function () {
              return {
                10: {
                  itemViews: {
                    BonusLevel: {
                      emit: function (evt) {
                        done(evt === 'InputSelect' ? undefined : 'error');
                      }
                    }
                  }
                }
              };
            }
          };
        }
      };

      puzzle_module.getLastUnlockedBonus = function () {
        puzzle_module.getLastUnlockedBonus = cache_puzzle;

        return 10;
      };

      tutorial.isCompleted = function () {
        tutorial.isCompleted = cache_comp;

        return false;
      };

      tutorial.build = function (data) {
        tutorial.build = cache_build;
        assert.strictEqual('map', data.type);
        assert.strictEqual(true, _.has(data.positions, 'bonus_level_intro'));
        data.on_cancel();
        data.positions.bonus_level_intro.action.context.onInputSelect();
      };

      GC.app.user.setMs('max', 11);
      map_screen.buildBonusLevelTutorial();
    });

    it('shouldnt build tutorial if already shown', function (done) {
      var map_screen = new Map(),
        tutorial = GC.app.tutorial,
        cache_puzzle = puzzle_module.getLastUnlockedBonus,
        cache_build = tutorial.build,
        cache_comp = tutorial.isCompleted;

      map_screen.map = {
        getModel: function () {
          return {
            getNodesById: function () {
              return {};
            }
          };
        },
        focusNodeById: function () {
          done('error');
        }
      };

      puzzle_module.getLastUnlockedBonus = function () {
        puzzle_module.getLastUnlockedBonus = cache_puzzle;

        return 10;
      };

      tutorial.isCompleted = function () {
        tutorial.isCompleted = cache_comp;

        return true;
      };

      tutorial.build = function () {
        done('error');
      };

      GC.app.user.setMs('max', 11);
      map_screen.buildBonusLevelTutorial();
      tutorial.build = cache_build;
      done();
    });
  });

  describe('puzzleComplete()', function () {
    it('it should invoke supr with third param', function () {
      var Base = function (ctxt, fn, param) {
          assert.strictEqual(fn, 'puzzleComplete');
          assert.strictEqual(param[2], 'layout');
        },
        map_screen = new Map(Base);

      map_screen.puzzleComplete();
    });
  });

  describe('loadMapIcon', function () {
    it('should update the time on bonus-map-time get emitted', function (done) {
      var map_screen = new Map(function () { }),
        cache = map_screen.updateMapTimer;

      map_screen.map_icon = {
        hide: function () {}
      };
      map_screen.updateMapTimer = function () {
        map_screen.updateMapTimer = cache;
        done();
      };
      map_screen.loadMapIcon();
      puzzle_module.emit('bonus-map-time', [100]);
    });
  });
  describe('refresh', function () {
    it('should call loadMapIcon', function (done) {
      map_screen = new Map(function () {});
      map_screen.loadMapIcon = function () {
        done();
      };
      map_screen.refresh();
    });
  });

  describe('chainStartMs()', function () {
    it('it should call cutscene register if max ms == cur and if ' +
      'bonus_map is false', function (done) {
      var cache_avail, cache, cache_chain;

      cache = util_test.getFromJSIOCache('src/modules/cutscene')
        .exports.register;
      util_test.getFromJSIOCache('src/modules/cutscene').exports.register =
        function () {
          done();
        };

      GC.app.user.set('max_ms', 5);
      GC.app.user.set('curr_ms', 5);
      GC.app.user.set('screen_bonus', false);
      map_screen = new Map(function () {});
      cache_chain = map_screen.chain;
      map_screen.chain = function () {
      };
      cache_avail = map_screen.mapEventAvailable;
      map_screen.mapEventAvailable = function () {
        map_screen.mapEventAvailable = cache_avail;
      };
      map_screen.chainStartMs(5);
      util_test.getFromJSIOCache('src/modules/cutscene').exports.register =
        cache;
      map_screen.chain = cache_chain;
    });
    it('it not should call cutscene register if max ms != cur and if' +
      'bonus_map is true', function (done) {
      var cache_avail;

      util_test.getFromJSIOCache('src/modules/cutscene').exports.register =
        function () {
          done('error');
        };

      GC.app.user.set('max_ms', 5);
      GC.app.user.set('curr_ms', 4);
      GC.app.user.set('screen_bonus', true);
      map_screen = new Map(function () {});
      cache_avail = map_screen.mapEventAvailable;
      map_screen.mapEventAvailable = function () {
        map_screen.mapEventAvailable = cache_avail;
      };
      map_screen.chainStartMs(4);
      done();
    });

    it('it should chain showEventPopup if not bonus map and not show play',
      function (done) {
      var cache_chain, cache;

      GC.app.user.set('max_ms', 5);
      GC.app.user.set('curr_ms', 4);
      GC.app.user.set('screen_bonus', true);
      map_screen = new Map(function () {});
      cache = map_screen.showEventPopup;
      cache_chain = map_screen.chain;

      map_screen.showEventPopup = function () {
        done();
      };
      map_screen.chain = function (event, fun) {
        fun();
      };
      map_screen.chainStartMs(false, false);
      done();
    });
  });

  describe('checkGiftingIcon', function () {
    it('should return value if within gifting ms', function () {
        var cache_get = util.getProducts;

        util.getProducts = function () {
          util.getProducts = cache_get;
          return {
            gifts: {
              31: {
                type: 'cash',
                quantity: 300,
                bonus: 'first_time_bonus'
              }
            }
          };
        };
        GC.app.user.set('gifted_ms', 32);
        GC.app.user.set('max_ms', 12);
        assert.strictEqual(map_screen.checkGiftingIcon(31), true);
      });
  });

  describe('showGiftingPopup', function () {
    it('should shake the gift icon if ' +
      'frame<4 and !1', function (done) {
        var cache = gifting.giftAvailable;

        map_screen = new Map(function () { });

        map_screen.frame_pos = 2;
        map_screen.gifting_image = {
          updateOpts: function () {},
          show: function () {}
        };
        gifting.giftAvailable = function () {
          gifting.giftAvailable = cache;
          return true;
        };
        effects.shake = function () {
          done();
        };
        effects.hover = function () { };
        map_screen.overlay = {
          updateOpts: function () { },
          hide: function () { },
          show: function () { }
        };
        map_screen.showGiftingPopup();
      });
    it('should show the gift icon if ' +
      'and frame<4', function (done) {
      var img = image.get('map_screen/gift/gift-tap-' + 1),
        cache = gifting.giftAvailable;

      map_screen = new Map(function () { });

      map_screen.frame_pos = 1;
      map_screen.gifting_image = {
        updateOpts: function (params) {
          assert.deepEqual(params.image, img);
          done();
        },
        show: function () {}
      };
      gifting.giftAvailable = function () {
        gifting.giftAvailable = cache;
        return true;
      };
      effects.shake = function () {};
      effects.hover = function () { };
      map_screen.overlay = {
        updateOpts: function () { },
        hide: function () {},
        show: function () {}
      };
      map_screen.showGiftingPopup();
    });

    it('should build the gifting popup if ' +
      'frame>=4', function (done) {
      var Base = function (ctxt, fn) {
        if (fn === 'showGiftingPopup') {
          done();
        }
      },
      cache = gifting.giftAvailable,
      cache_timer = timer.timeout;

      map_screen = new Map(Base);

      map_screen.frame_pos = 6;
      map_screen.gifting_image = {
        updateOpts: function () {},
        show: function () {},
        hide: function () {}
      };
      gifting.giftAvailable = function () {
        gifting.giftAvailable = cache;
        return true;
      };

      map_screen.overlay = {
        updateOpts: function () { },
        hide: function () {},
        show: function () {}
      };
      gifting.giftAvailable = function () {
        return true;
      };
      timer.timeout = function (name, cb, length) {
        timer.timeout = cache_timer;
        assert.strictEqual(name, 'show_gifting_popup');
        assert.strictEqual(length, 750);
        cb();
      };
      map_screen.map = {
        getModel: function () {
          return {
            removeTagById: function () {}
          };
        }
      };
      map_screen.showGiftingPopup();
    });

    it('should call the Quest method if condition fails', function (done) {
      var Base = function (ctxt, fn) {
        if (fn === 'showGiftingPopup') {
          done();
        }
      },
      map_screen = new Map(Base),
      cache = gifting.giftAvailable;

      gifting.giftAvailable = function () {
        gifting.giftAvailable = cache;
        return false;
      };

      map_screen.overlay = {
        show: function () {}
      };
      map_screen.showGiftingPopup();
    });
  });

  describe('showEventPopup()', function () {
    it('should call load map on events:play', function (done) {
      var cache;

      map_screen = new Map(function () {});
      map_screen.loadMap = function () {
        map_screen.cache = cache;
        done();
      };
      puzzle_module.showEventPopup = function () {
        return true;
      };
      map_screen.showEventPopup();
      popup_manager.emit('events:play');
    });

    it('should call cb directly', function (done) {
      var cache;

      map_screen = new Map(function () {});
      cache = puzzle_module.showEventPopup;
      puzzle_module.showEventPopup = function () {
        return false;
      };
      map_screen.showEventPopup(1, function () {
        done();
      });
      puzzle_module.showEventPopup = cache;
    });
  });

  describe('mapEventAvailable()', function () {
    it('it should return true if cutscene available',
      function () {
      is_completed = false;
      map_screen = new Map(function () {});
      assert.strictEqual(map_screen.mapEventAvailable(5), true);
    });
    it('it should return false if cutscene unavailable',
      function () {
      is_completed = true;
      map_screen = new Map(function () {});
      assert.strictEqual(map_screen.mapEventAvailable(5), undefined);
    });
  });

  describe('startMilestone()', function () {
    it('it call tut build if inventorylive >0,curr=maxms',
      function (done) {
      var app = GC.app,
        cache = app.tutorial.build;

      app.user.set('inventory_lives', 1);
      app.user.set('max_ms', 2);
      app.user.set('curr_ms', 2);
      app.tutorial.build = function () {
        app.tutorial.build = cache;
        done();
      };
      map_screen = new Map(function () {});
      map_screen.startMilestone(2);
    });
    it('it not call tut build if inventory live=0,curr=maxms',
      function (done) {
      var app = GC.app,
        cache = app.tutorial.build;

      app.user.set('inventory_lives', 0);
      app.user.set('max_ms', 2);
      app.user.set('curr_ms', 2);
      app.tutorial.build = function () {
        app.tutorial.build = cache;
        done('error');
      };
      map_screen = new Map(function () {});
      map_screen.startMilestone(2);
      done();
    });
    it('it not call tut build if inventory live > 0,curr!=maxms',
      function (done) {
      var app = GC.app,
      cache = app.tutorial.build;

      app.user.set('inventory_lives', 0);
      app.user.set('max_ms', 2);
      app.user.set('curr_ms', 1);
      app.tutorial.build = function () {
        app.tutorial.build = cache;
        done('error');
      };
      map_screen = new Map(function () {});
      map_screen.startMilestone(1);
      done();
    });
  });

  describe('loadPuzzle()', function () {
    beforeEach(function () {
      map_screen.puzzle_screen = new PuzzleScreen();

      map_screen.puzzle_screen.toast_viewpool.obtainView = function () {
        var toast = new Toast();

        toast.build = function () {
        };
        return toast;
      };
    });

    it('should push puzzle screen to stack if load success', function (done) {
      var cache = GC.app.push,
        cache_load = map_screen.puzzle_screen.load;

      map_screen.puzzle_screen.load = function () {
        map_screen.puzzle_screen.load = cache_load;
        return Promise.resolve(true);
      };

      GC.app.push = function () {
        GC.app.push = cache;
        done();
      };
      map_screen.loadPuzzle();
    });

    it('shouldnt push puzzlescreen if load fails', function (done) {
      var cache = GC.app.push,
        cache_load = map_screen.puzzle_screen.load;

      map_screen.puzzle_screen.load = function () {
        map_screen.puzzle_screen.load = cache_load;
        return Promise.reject(false);
      };

      GC.app.push = function () {
        done('error');
      };
      map_screen.loadPuzzle({});
      _.defer(function () {
        GC.app.push = cache;
        done();
      });
    });

    it('should call showRetryPopup if load fails', function (done) {
      var cache = GC.app.push,
        cache_load = map_screen.puzzle_screen.load,
        cache_retry = map_screen.showRetryPopup;

      map_screen.puzzle_screen.load = function () {
        GC.app.push = cache;
        map_screen.puzzle_screen.load = cache_load;
        return Promise.reject(false);
      };

      GC.app.push = function () {
        done('error');
      };

      map_screen.showRetryPopup = function () {
        map_screen.showRetryPopup = cache_retry;
        done();
      };
      map_screen.loadPuzzle({});
    });
  });

  describe('showRetryPopup()', function () {
    it('should call retry_popup.build', function (done) {
      var cache;

      map_screen.retry_popup = retry_popup;
      cache = map_screen.retry_popup.build;

      map_screen.retry_popup.build = function (type) {
        map_screen.retry_popup.build = cache;
        assert.strictEqual('gameload', type);
        done();
      };
      map_screen.showRetryPopup({});
    });

    it('should call popup.once', function (done) {
      var cache;

      map_screen.retry_popup = retry_popup;
      map_screen.popup = popup_manager;
      cache = map_screen.popup;

      map_screen.retry_popup.build = function () {
        map_screen.retry_popup.build = cache;
      };

      map_screen.popup.once = function (event) {
        assert.strictEqual(event, 'retry-gameload');
        done();
      };
      map_screen.showRetryPopup({});
    });

    it('should retry for bonus level', function (done) {
      var cache = map_screen.startMilestone;

      map_screen.retry_popup = retry_popup;
      map_screen.retry_popup.build = function () {};

      popup_manager.once = function (event, cb) {
        assert.strictEqual(event, 'retry-gameload');
        cb();
      };

      map_screen.startMilestone = function (ms, bonus) {
        map_screen.startMilestone = cache;
        done(bonus === true ? undefined : 'error');
      };
      map_screen.showRetryPopup({
        bonus_level: 1
      });
    });
  });

  describe('updateMapTimer', function () {
    it('should show the map icon if there is time left', function (done) {
      map_screen = new Map(function () {});
      map_screen.map_icon = {
        setTitle: function () {},
        show: function () {
          done();
        }
      };

      map_screen.updateMapTimer(500);
    });
    it('should hide the map icon if there is no time left', function (done) {
      map_screen = new Map(function () {});
      map_screen.map_icon = {
        setTitle: function () {},
        hide: function () {
          done();
        }
      };
      map_screen.updateMapTimer(0);
    });
  });

  describe('loadMap', function () {
    it('should call close', function (done) {
      var map_screen = new Map(function () { }),
      cache = map_screen.close;

      map_screen.close = function () {
        map_screen.close = cache;
        done();
      };
      map_screen.loadMap(false);
    });
    it('should call emit load-map with bonus map', function (done) {
      var map_screen = new Map(function () {}),
        cache_emit = GC.app.emit,
        cache = map_screen.close;

      GC.app.emit = function (param1, param2) {
        GC.app.emit = cache_emit;
        assert.strictEqual('load-map', param1);
        assert.strictEqual(true, param2);
        done();
      };
      map_screen.close = function (cb) {
        map_screen.close = cache;
        cb._run[0]();
      };
      map_screen.loadMap(true);
    });
  });

  describe('showPopup()', function () {
    it('should invoke parent showPopup with passed type', function () {
      var Base = function (ctxt, fn, param) {
          assert.strictEqual(fn, 'showPopup');
          assert.strictEqual(param[0], 'abc');
        },
        map_screen = new Map(Base);

      map_screen.showPopup('abc');
    });

    it('should invoke parent showPopup with type messages for invites',
      function () {
        var Base = function (ctxt, fn, param) {
            assert.strictEqual(fn, 'showPopup');
            assert.strictEqual(param[0], 'messages');
          },
          map_screen = new Map(Base);

        map_screen.showPopup('invites');
      }
    );
  });

  describe('onTileStore()', function () {
    it('should invoke tile_store build', function (done) {
      var cache;

      map_screen = new MapQuest();

      cache = map_screen.tile_store.build;

      map_screen.tile_store.build = function () {
        map_screen.tile_store.build = cache;
        done();
      };

      map_screen.onTileStore();
    });
  });

  describe('on3DTouch()', function () {
    it('should invoke showPopup with tag', function (done) {
      var cache = map_screen.showPopup;

      map_screen.showPopup = function (val) {
        map_screen.showPopup = cache;
        done(val === 'abc' ? undefined : 'error');
      };

      map_screen.on3DTouch('abc');
    });

    it('should close all popup if origin is true and popup ' +
      'present', function (done) {
        var cache = popup_manager.closeAll,
          cache_show = map_screen.showPopup,
          cache_stack = popup_manager.getStack;

        popup_manager.closeAll = function () {
          popup_manager.closeAll = cache;
          done();
        };

        popup_manager.getStack = function () {
          popup_manager.getStack = cache_stack;
          return [1, 2];
        };

        map_screen.showPopup = function () {
          map_screen.showPopup = cache_show;
        };

        map_screen.removeAllListeners = function (param) {
          assert.strictEqual(param, 'start-ms');
        };

        map_screen.on3DTouch('test', true);
      }
    );
  });
  describe('clean()', function () {
    it('should remove tile-set-select listener', function (done) {
      var cache = puzzle_module.removeListener;

      map_screen = new Map(function () {});
      puzzle_module.removeListener = function (event) {
        puzzle_module.removeListener = cache;
        done(event === 'bonus-map-time' ? undefined : 'error');
      };

      map_screen.gifting_image = {
        removeAllListeners: function () {
        },
        hide: function () {}
      };

      map_screen.clean(function () {});
    });
    it('should remove tile-set-select listener', function (done) {
      map_screen = new Map(function () { });
      map_screen.gifting_image = {
        removeAllListeners: function (event) {
          done(event === 'InputSelect' ? undefined : 'error');
        },
        hide: function () {}
      };
      map_screen.clean(function () { });
    });
  });
});
