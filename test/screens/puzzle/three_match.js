/* global _, ThreeMatch, test_util, Puzzle, Puzzle_quest, ThreeMatchModel */

jsio('import util.underscore as _');
jsio('import test.lib.util as test_util');
jsio('import resources.styles.portrait as style');
jsio('import ui.View as View');
describe('Three match screen', function () {
  'use strict';

  var three_match, puzzle, puzzle_quest, three_match_model,
      is_completed = false,
      prepare = function () {
        GC.app = {
          user: {
            get: function () {},
            getMs: function () {}
          },
          tutorial: {
            build: function () {},
            isCompleted: function () {
              return is_completed;
            },
            add: function () {}
          },
          engine: {
            subscribe: function () {}
          }
        };
      },
      init = function () {
        jsio('import src.screens.puzzles.three_match as ThreeMatch');
        jsio('import src.screens.puzzle as Puzzle');
        jsio('import quest.screens.puzzle as Puzzle_quest');
        jsio('import src.models.puzzles.three_match as ThreeMatchModel');
        puzzle_quest = new Puzzle_quest();
        three_match = new ThreeMatch();
        three_match_model = new ThreeMatchModel();
        puzzle = new Puzzle();
      },
    clean = function () {
      test_util.removeFromJSIOCache('src/screens/puzzles/three_match.js');
    };

  before(prepare);
  beforeEach(init);
  afterEach(clean);
  describe('onChangeSelected', function () {
    it('should call loadThirdMatchTutorial', function (done) {
      var cache = three_match.loadThirdMatchTutorial,
        cache_commit = Puzzle.commitEffects;

      three_match.loadThirdMatchTutorial = function () {
        three_match.loadThirdMatchTutorial = cache;
        done();
      };
      puzzle.commitEffects = function () {
        puzzle.commitEffects = cache_commit;
      };
      three_match.onChangeSelected();
    });
  });
  describe('loadThirdMatchTutorial', function () {
    it('should build the tutorial if tutorial is not compleleted' +
      'and two tiles are selected', function () {
        three_match.model.set('tile_selected', [1, 2]);
        GC.app.tutorial.build = function (opts) {
          assert.strictEqual(true, _.has(opts.positions, 'hint_three_match'));
          assert.strictEqual(opts.type, 'puzzle');
        };
        three_match.loadThirdMatchTutorial();
      });
    it('should not build the tutorial if tutorial' +
      'is compleleted', function (done) {
        three_match.model.set('tile_selected', null);
        GC.app.tutorial.isCompleted = function () {
          return true;
        };
        GC.app.tutorial.build = function () {
          done('err');
        };
        three_match.loadThirdMatchTutorial();
        done();
      });
    it('should pause the timer before running tutorial' +
      ' resume after running', function () {
        var pivot = 0,
          cache = three_match.model.pauseTimers,
          cache_resume = three_match.model.resumeTimers;

        three_match.model.set('tile_selected', [1, 2]);
        three_match.model.pauseTimers = function () {
          three_match.model.pauseTimers = cache;
          pivot++;
        };
        GC.app.tutorial.isCompleted = function () {
          return false;
        };
        three_match.model.resumeTimers = function () {
          three_match.model.resumeTimers = cache_resume;
          pivot++;
        };
        GC.app.tutorial.build = function (opts) {
          opts.positions.hint_three_match.before();
          opts.positions.hint_three_match.after();
        };
        three_match.loadThirdMatchTutorial();
        assert.strictEqual(2, pivot);
      });
  });
  describe('gameOver', function () {
    it('should call supr of game_over in three_match', function (done) {
      var cache = Puzzle_quest.prototype.gameOver;

      Puzzle_quest.prototype.gameOver = function () {
        Puzzle_quest.prototype.gameOver = cache;
        done();
      };
      three_match.gameOver(true, function () {});
    });
  });
});
