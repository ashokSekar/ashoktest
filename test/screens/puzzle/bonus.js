/* global _, BonusScreen, test_util, Puzzle, Puzzle_quest, BonusModel,
  puzzle_module */

jsio('import util.underscore as _');
jsio('import test.lib.util as test_util');
jsio('import resources.styles.portrait as style');
jsio('import ui.View as View');
describe('Bonus screen', function () {
  'use strict';

  var bonus_screen, puzzle, puzzle_quest, bonus_model,
      is_completed = false,
      prepare = function () {
        GC.app = {
          user: {
            get: function () {},
            getMs: function () {}
          },
          tutorial: {
            build: function () {},
            isCompleted: function () {
              return is_completed;
            },
            add: function () {}
          },
          engine: {
            subscribe: function () {}
          }
        };
      },
      init = function () {
        jsio('import src.screens.puzzles.bonus as BonusScreen');
        jsio('import src.screens.puzzle as Puzzle');
        jsio('import quest.screens.puzzle as Puzzle_quest');
        jsio('import src.models.puzzles.bonus as BonusModel');
        jsio('import quest.modules.puzzle as puzzle_module');
        puzzle_quest = new Puzzle_quest();
        bonus_screen = new BonusScreen();
        bonus_model = new BonusModel();
        puzzle = new Puzzle();
      },
    clean = function () {
      test_util.removeFromJSIOCache('src/screens/puzzles/three_match.js');
    };

  before(prepare);
  beforeEach(init);
  afterEach(clean);

  describe('loadTutorial', function () {
    it('should build the tutorial for three match with' +
      'correct opts', function () {
      var cache = puzzle_module.getBonusMapType;

      puzzle_module.getBonusMapType = function () {
        return 'three_match';
      };
      GC.app.tutorial.build = function (opts) {
        assert.strictEqual(true, _.has(opts.positions, 'three_match'));
        assert.strictEqual(true, _.has(opts.positions, 'three_match_end'));
        assert.strictEqual(opts.type, 'game_intro');
        assert.strictEqual(opts.milestone, 'three_match');
        puzzle_module.getBonusMapType = cache;
      };
      bonus_screen.loadTutorial();
    });

    it('should not call build of non tutorial variation', function (done) {
      var cache = puzzle_module.getBonusMapType;

      puzzle_module.getBonusMapType = function () {
        return 'abc';
      };
      GC.app.tutorial.build = function () {
        done('error');
      };
      bonus_screen.loadTutorial();
      done();
      puzzle_module.getBonusMapType = cache;
    });

    it('should pause the timer before running tutorial' +
      'resume after running', function (done) {
        var pivot = 0,
          cache = bonus_screen.model.pauseTimers,
          cache_resume = bonus_screen.model.resumeTimers,
          cache_type = puzzle_module.getBonusMapType,
          cache_update = bonus_screen.updateGoldenTileForTutorial;

        puzzle_module.getBonusMapType = function () {
          return 'golden';
        };

        bonus_screen.updateGoldenTileForTutorial = function () {
          bonus_screen.updateGoldenTileForTutorial = cache_update;
          pivot++;
        };

        bonus_screen.model.pauseTimers = function () {
          bonus_screen.model.pauseTimers = cache;
          pivot++;
        };

        bonus_screen.model.resumeTimers = function () {
          bonus_screen.model.resumeTimers = cache_resume;
          pivot++;
        };
        GC.app.tutorial.build = function (opts) {
          opts.positions.golden_tile.before();
          opts.positions.golden_tile.after();
        };
        bonus_screen.loadTutorial();
        done(pivot === 3 ? undefined : 'error');
        puzzle_module.getBonusMapType = cache_type;
      });

    it('should pause the timer before running tutorial' +
      'resume after running for jewels', function (done) {
        var pivot = 0,
          cache = bonus_screen.model.pauseTimers,
          cache_resume = bonus_screen.model.resumeTimers,
          cache_type = puzzle_module.getBonusMapType;

        puzzle_module.getBonusMapType = function () {
          return 'jewels';
        };

        bonus_screen.model.pauseTimers = function () {
          pivot++;
        };

        bonus_screen.model.resumeTimers = function () {
          pivot++;
        };
        GC.app.tutorial.build = function (opts) {
          opts.positions.jewels_hud.before();
          opts.positions.jewels_hud.after();
          opts.positions.jewels_match1.before();
          opts.positions.jewels_match1.after();
          opts.positions.jewels_continue.before();
          opts.positions.jewels_continue.after();
        };
        bonus_screen.loadTutorial();
        done(pivot === 6 ? undefined : 'error');
        puzzle_module.getBonusMapType = cache_type;
        bonus_screen.model.pauseTimers = cache;
        bonus_screen.model.resumeTimers = cache_resume;
      });

    it('should pause the timer before running tutorial' +
      'resume after running for lock_key', function (done) {
        var pivot = 0,
          cache = bonus_screen.model.pauseTimers,
          cache_resume = bonus_screen.model.resumeTimers,
          cache_type = puzzle_module.getBonusMapType;

        puzzle_module.getBonusMapType = function () {
          return 'lock_key';
        };

        bonus_screen.model.pauseTimers = function () {
          pivot++;
        };

        bonus_screen.model.resumeTimers = function () {
          pivot++;
        };
        GC.app.tutorial.build = function (opts) {
          opts.positions.lock_key1.before();
          opts.positions.lock_key1.after();
          opts.positions.lock_key2.before();
          opts.positions.lock_key2.after();
          opts.positions.lock_key3.before();
          opts.positions.lock_key3.after();
        };
        bonus_screen.loadTutorial();
        done(pivot === 6 ? undefined : 'error');
        puzzle_module.getBonusMapType = cache_type;
        bonus_screen.model.pauseTimers = cache;
        bonus_screen.model.resumeTimers = cache_resume;
      });

    it('should pause the timer before running tutorial' +
      'resume after running for three_match', function (done) {
        var pivot = 0,
          cache = bonus_screen.model.pauseTimers,
          cache_resume = bonus_screen.model.resumeTimers,
          cache_type = puzzle_module.getBonusMapType;

        puzzle_module.getBonusMapType = function () {
          return 'three_match';
        };

        bonus_screen.model.pauseTimers = function () {
          bonus_screen.model.pauseTimers = cache;
          pivot++;
        };

        bonus_screen.model.resumeTimers = function () {
          bonus_screen.model.resumeTimers = cache_resume;
          pivot++;
        };
        GC.app.tutorial.build = function (opts) {
          opts.positions.three_match.before();
          opts.positions.three_match.after();
        };
        bonus_screen.loadTutorial();
        done(pivot === 2 ? undefined : 'error');
        puzzle_module.getBonusMapType = cache_type;
      });
    it('should pause the timer before running tutorial' +
      'after running for three_match_end', function (done) {
        var pivot = 0,
          cache = bonus_screen.model.pauseTimers,
          cache_resume = bonus_screen.model.resumeTimers,
          cache_type = puzzle_module.getBonusMapType;

        puzzle_module.getBonusMapType = function () {
          return 'three_match';
        };

        bonus_screen.model.pauseTimers = function () {
          bonus_screen.model.pauseTimers = cache;
          pivot++;
        };

        bonus_screen.model.resumeTimers = function () {
          bonus_screen.model.resumeTimers = cache_resume;
          pivot++;
        };
        GC.app.tutorial.build = function (opts) {
          opts.positions.three_match_end.before();
          opts.positions.three_match_end.after();
        };
        bonus_screen.loadTutorial();
        done(pivot === 2 ? undefined : 'error');
        puzzle_module.getBonusMapType = cache_type;
      });
  });
  describe('loadGoldenTileUnlockTutorial', function () {
    it('should call build tutorial', function (done) {
      var pivot = 0,
        cache_build = GC.app.tutorial.build,
        cache = bonus_screen.model.pauseTimers,
        cache_resume = bonus_screen.model.resumeTimers,
        tiles = [
          {
            getCoordinates: function () {
              return [];
            },
            isGoldenTile: function () {
              return false;
            }
          },
          {
            getCoordinates: function () {
              return [0, 0, 0];
            },
            isGoldenTile: function () {
              return true;
            },
            isFree: function () {
              return true;
            }
          }
        ];

      bonus_screen.grid.tiles = [[[[]]]];

      bonus_screen.model.pauseTimers = function () {
        bonus_screen.model.pauseTimers = cache;
        pivot++;
      };

      bonus_screen.model.resumeTimers = function () {
        bonus_screen.model.resumeTimers = cache_resume;
        pivot++;
      };
      GC.app.tutorial.build = function (opts) {
        opts.positions.golden_tile_unlock.before();
        opts.positions.golden_tile_unlock.after();
        GC.app.tutorial.build = cache_build;
        done();
      };
      bonus_screen.loadGoldenTileUnlockTutorial(tiles);
    });

    it('should call puzzle getTile', function (done) {
      var cache = bonus_screen.model.getTile,
        tiles = [
          []
        ];

      bonus_screen.model.getTile = function () {
        bonus_screen.model.getTile = cache;
        done();
      };
      bonus_screen.loadGoldenTileUnlockTutorial(tiles);
    });

    it('should listen for ongoing', function (done) {
      var cache = bonus_screen.model.getTile,
        on_cache = bonus_screen.model.on,
        tiles = [
          {
            getCoordinates: function () {
              return [];
            },
            isGoldenTile: function () {
              return false;
            }
          },
          {
            getCoordinates: function () {
              return [0, 0, 0];
            },
            isGoldenTile: function () {
              return true;
            },
            isFree: function () {
              return true;
            }
          }
        ];

      bonus_screen.grid.tiles = [[[[]]]];
      bonus_screen.model.eachCell = function (cb) {
        bonus_screen.model.eachCell = cache;
        cb(tiles[1]);
      };
      bonus_screen.model.set('ongoing', 1);
      bonus_screen.model.on = function (event) {
        if (event === 'change:ongoing') {
          bonus_screen.model.on = on_cache;
          done();
        }
      };
      GC.app.tutorial.build = function () {
      };
      bonus_screen.loadGoldenTileUnlockTutorial();
    });
  });

  describe('updateGoldenTileForTutorial', function () {
    it('should call doSwap', function (done) {
      var tiles = [],
        cache_swap = bonus_screen.model.doSwap,
        cache_get = bonus_screen.model.get;

      tiles[1] = [];
      tiles[1][1] = [];

      tiles[0] = [];
      tiles[0][1] = [];

      tiles[1][1][1] = {
        isFree: function () {
          return true;
        },
        isVisible: function () {
          return true;
        },
        get: function () {
          return 'a1';
        },
        isGoldenTile: function () {
          return false;
        }
      };
      tiles[1][1][2] = {
        isFree: function () {
          return true;
        },
        isVisible: function () {
          return true;
        },
        get: function () {
          return 'a1';
        },
        isGoldenTile: function () {
          return false;
        }
      };
      tiles[1][1][3] = {
        isFree: function () {
          return true;
        },
        isVisible: function () {
          return true;
        },
        get: function () {
          return 'a3';
        },
        isGoldenTile: function () {
          return false;
        },
        emit: function () {
        }
      };
      tiles[1][1][4] = {
        isFree: function () {
          return false;
        },
        isVisible: function () {
          return true;
        },
        get: function () {
          return 'a3';
        },
        isGoldenTile: function () {
          return false;
        },
        emit: function () {
        }
      };
      tiles[1][1][5] = {
        isFree: function () {
          return true;
        },
        isVisible: function () {
          return true;
        },
        get: function () {
          return 'a2';
        },
        isGoldenTile: function () {
          return true;
        },
        emit: function () {
        }
      };
      tiles[0][1][1] = {
        isFree: function () {
          return true;
        },
        isVisible: function () {
          return false;
        },
        get: function () {
          return 'a2';
        },
        isGoldenTile: function () {
          return true;
        },
        emit: function () {
        }
      };
      tiles[0][1][2] = {
        isFree: function () {
          return true;
        },
        isVisible: function () {
          return false;
        },
        get: function () {
          return 'a2';
        },
        isGoldenTile: function () {
          return true;
        },
        emit: function () {
        }
      };

      bonus_screen.model.get = function (key) {
        if (key === 'tile_per_match') {
          return 2;
        } else if (key === 'tiles') {
          return tiles;
        } else if (key === 'rows') {
          return 10;
        }
      };

      bonus_screen.model.doSwap = function () {
        bonus_screen.model.doSwap = cache_swap;
        bonus_screen.model.get = cache_get;
        done();
      };

      bonus_screen.updateGoldenTileForTutorial();
    });

    it('should not call doSwap', function (done) {
      var tiles = [],
        cache_swap = bonus_screen.model.doSwap,
        cache_get = bonus_screen.model.get;

      tiles[1] = [];
      tiles[1][1] = [];

      tiles[0] = [];
      tiles[0][1] = [];

      tiles[1][1][1] = {
        isFree: function () {
          return true;
        },
        get: function () {
          return 'a1';
        },
        isGoldenTile: function () {
          return false;
        }
      };
      tiles[1][1][2] = {
        isFree: function () {
          return true;
        },
        get: function () {
          return 'a1';
        },
        isGoldenTile: function () {
          return false;
        }
      };

      bonus_screen.model.get = function (key) {
        if (key === 'tile_per_match') {
          return 2;
        } else if (key === 'tiles') {
          return tiles;
        } else if (key === 'rows') {
          return 10;
        }
      };

      bonus_screen.model.doSwap = function () {
        done('error');
      };

      bonus_screen.updateGoldenTileForTutorial();
      bonus_screen.model.doSwap = cache_swap;
      bonus_screen.model.get = cache_get;
      done();
    });
  });
});
