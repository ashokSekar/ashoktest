/* global _, MapExtra, MapGame, puzzle_module, event_manager,
QuestMap, loading, ThreeMatchScreen, BonusPuzzleScreen, popup */
jsio('import util.underscore as _');
jsio('import DevkitHelper.style as style');
jsio('import DevkitHelper.loading as loading');
jsio('import DevkitHelper.storage as storage');
jsio('import resources.data.config as config');
jsio('import DevkitHelper.event_manager as event_manager');
describe('Three match screen', function () {
  'use strict';

  var map_extra,
      prepare = function () {
        jsio('import src.screens.map_extra as MapExtra');
        jsio('import quest.screens.map as MapGame');
        jsio('import quest.modules.puzzle as puzzle_module');
        jsio('import quest.screens.map as QuestMap');
        jsio('import src.screens.puzzles.three_match as ThreeMatchScreen');
        jsio('import src.screens.puzzles.bonus as BonusPuzzleScreen');
        jsio('import quest.modules.popup_manager as popup');
        GC.app = {
          user: {
            set: function () {},
            getMs: function () {},
            get: function () {}
          },
          engine: {
            subscribe: function () {}
          }
        };
      },
      init = function () {
      puzzle_module.getBonusMapType = function () {
        return 'golden';
      };
      map_extra = new MapExtra();
    };

  before(prepare);
  beforeEach(init);
  describe('build', function () {
    it('should call bonus map popup once ViewDidAppear', function (done) {
      var cache = map_extra.showBonusMapPopup,
        cache_build = MapGame.prototype.build;

      map_extra.showBonusMapPopup = function () {
        map_extra.showBonusMapPopup = cache;
        done();
      };
      MapGame.prototype.build = function () {
        MapGame.prototype.build = cache_build;
        return;
      };
      map_extra.build();
      map_extra.emit('ViewDidAppear');
    });
  });

  describe('loadPuzzleScreen()', function () {
    it('should set proper puzzle screen for three match', function () {
      var cache = puzzle_module.getBonusMapType;

      puzzle_module.getBonusMapType = function () {
        puzzle_module.getBonusMapType = cache;
        return 'three_match';
      };

      map_extra.loadPuzzleScreen();
      assert.strictEqual(true, map_extra.puzzle_screen instanceof
        ThreeMatchScreen);
    });

    it('should set default bonus puzzle screen for others', function () {
      var cache = puzzle_module.getBonusMapType;

      puzzle_module.getBonusMapType = function () {
        puzzle_module.getBonusMapType = cache;
        return 'golden';
      };

      map_extra.loadPuzzleScreen();
      assert.strictEqual(true, map_extra.puzzle_screen instanceof
        BonusPuzzleScreen);
    });
  });

  describe('showBonusMapPopup', function () {
    it('should show bonus map popup if there is ' +
        'a new variation', function (done) {
        var cache = puzzle_module.getBonusMapId,
            cache_get = GC.app.user.get,
            cache_get_type = puzzle_module.getBonusMapType,
            cache_build = map_extra.bonus_map.build;

        puzzle_module.getBonusMapType = function () {
          puzzle_module.getBonusMapType = cache_get_type;
          return 'three_match';
        };
        GC.app.user.get = function () {
          GC.app.user.get = cache_get;
          return ['lock_n_key', 'jewels'];
        };
        map_extra.bonus_map.build = function () {
          map_extra.bonus_map.build = cache_build;
          done();
        };
        puzzle_module.getBonusMapId = function () {
          puzzle_module.getBonusMapId = cache;
          return 'three_match';
        };
        map_extra.showBonusMapPopup();
      });
    it('should not show bonus map popup if there is ' +
        'no new variation', function (done) {
        var cache = puzzle_module.getBonusMapId,
            cache_get = GC.app.user.get,
            cache_build = map_extra.bonus_map.build;

        GC.app.user.get = function () {
          GC.app.user.get = cache_get;
          return ['lock_n_key', 'jewels'];
        };
        map_extra.bonus_map.build = function () {
          map_extra.bonus_map.build = cache_build;
          done('err');
        };
        puzzle_module.getBonusMapId = function () {
          puzzle_module.getBonusMapId = cache;
          return 'lock_n_key';
        };
        map_extra.showBonusMapPopup();
        done();
      });
  });
  describe('loadMapIcon', function () {
    it('should show map icon', function (done) {
      var cache = map_extra.map_icon.show;

      map_extra.map_icon.show = function () {
        map_extra.map_icon.show = cache;
        done();
      };
      map_extra.showPopup();
      map_extra.on3DTouch();
      map_extra.buildSaleInfo();
      map_extra.loadMapIcon();
    });
  });
  describe('loadMap', function () {
    it('should call supr loadMap', function (done) {
      var cache = QuestMap.prototype.loadMap;

      QuestMap.prototype.loadMap = function (data) {
        assert.strictEqual(false, data);
        QuestMap.prototype.loadMap = cache;
        done();
      };
      map_extra.loadMap();
    });
  });

  describe('renderPuzzle', function () {
    it('should call loadPuzzle if data is Numbered', function (done) {
      var cache = puzzle_module.loadBonusMapPuzzle,
        cache_loading = loading.show;

      loading.show = function (type, cb) {
        loading.show = cache_loading;
        cb();
      };
      puzzle_module.loadBonusMapPuzzle = function () {
        puzzle_module.loadBonusMapPuzzle = cache;
        done();
        return {then: function () {}};
      };
      map_extra.renderPuzzle(44);
    });
    it('should call loadPuzzle if data is Numbered', function (done) {
      var cache = map_extra.loadPuzzle,
        cache_loading = loading.show;

      loading.show = function (type, cb) {
        loading.show = cache_loading;
        cb();
      };
      map_extra.loadPuzzle = function () {
        map_extra.loadPuzzle = cache;
        done();
      };
      map_extra.renderPuzzle('something');
    });
  });
  describe('renderCrossPromoBtn', function () {
    it('should hide crossPromo btn', function (done) {
      var cache = map_extra.cross_promo_btn.hide;

      map_extra.cross_promo_btn.hide = function () {
        map_extra.cross_promo_btn.hide = cache;
        done();
      };
      map_extra.renderCrossPromoBtn();
    });
  });
  describe('rateUsAvailable', function () {
    it('should always return false', function () {
      assert.strictEqual(false, map_extra.rateUsAvailable());
    });
  });
  describe('canOvertake', function () {
    it('should always return false', function () {
      assert.strictEqual(false, map_extra.canOvertake());
    });
  });
  describe('facebookAvailable', function () {
    it('should always return false', function () {
      assert.strictEqual(false, map_extra.facebookAvailable());
    });
  });
  describe('focusMilestone', function () {
    it('should call startMilestone if start true' +
        'and ms_focus equal to ms', function (done) {
      var cache = map_extra.startMilestone,
        cache_emit = event_manager.emit,
        cache_get = puzzle_module.getUserBonusPuzzle,
        cache_set = puzzle_module.setUserBonusPuzzle;

      event_manager.emit = function () {
        event_manager.emit = cache_emit;
      };
      puzzle_module.setUserBonusPuzzle = function () {
        puzzle_module.setUserBonusPuzzle = cache_set;
      };
      puzzle_module.getUserBonusPuzzle = function () {
        puzzle_module.getUserBonusPuzzle = cache_get;
        return {
          id: 'three_match',
          time_left: 200,
          curr_ms: 20
        };
      };
      map_extra.startMilestone = function (ms, bonus, threed_touch) {
        map_extra.startMilestone = cache;
        assert.strictEqual(ms, 10);
        assert.strictEqual(bonus, false);
        assert.strictEqual(threed_touch, false);
        done();
      };
      map_extra.focusMilestone(10, true);
    });
    it('should not emit milestone-selected if ' +
        'start is false', function (done) {
      var cache_emit = event_manager.emit,
        cache_get = puzzle_module.getUserBonusPuzzle,
        cache_set = puzzle_module.setUserBonusPuzzle;

      puzzle_module.getUserBonusPuzzle = function () {
        puzzle_module.getUserBonusPuzzle = cache_get;
        return {
          id: 'three_match',
          time_left: 200,
          curr_ms: 10
        };
      };
      event_manager.emit = function (data) {
        event_manager.emit = cache_emit;
        assert.notStrictEqual(data, 'milestone-selected');
      };
      puzzle_module.setUserBonusPuzzle = function () {
        puzzle_module.setUserBonusPuzzle = cache_set;
      };
      map_extra.focusMilestone(null, false);
      done();
    });
  });
  describe('puzzleComplete', function () {
    it('should load the next puzzle if next param given', function (done) {
      var cache_get = puzzle_module.getUserBonusPuzzle,
        cache_set = puzzle_module.setUserBonusPuzzle,
        cache_loading = loading.once,
        cache_load = puzzle_module.load,
        cache_event_finish = map_extra.showEventFinish;

      map_extra.showEventFinish = function () {
        map_extra.showEventFinish = cache_event_finish;
      };
      map_extra.refresh = function () {};
      map_extra.canFocusMs = function () {
        return false;
      };
      loading.once = function (type, cb) {
        loading.once = cache_loading;
        cb();
      };
      puzzle_module.load = function (ms) {
        puzzle_module.load = cache_load;
        assert.strictEqual(ms, 21);
        done();
      };
      puzzle_module.setUserBonusPuzzle = function () {
        puzzle_module.setUserBonusPuzzle = cache_set;
      };
      puzzle_module.getUserBonusPuzzle = function () {
        return {
          id: 'three_match',
          time_left: 200,
          curr_ms: 20
        };
      };
      map_extra.puzzleComplete(true);
      puzzle_module.getUserBonusPuzzle = cache_get;
    });

    it('should not load the next puzzle if next' +
    'param not given', function (done) {
      var cache_get = puzzle_module.getUserBonusPuzzle,
        cache_set = puzzle_module.setUserBonusPuzzle,
        cache_loading = loading.once,
        cache_load = puzzle_module.load;

      map_extra.refresh = function () {};
      map_extra.canFocusMs = function () {
        return true;
      };
      loading.once = function (type, cb) {
        loading.once = cache_loading;
        cb();
      };
      puzzle_module.load = function () {
        puzzle_module.load = cache_load;
        done('err');
      };
      puzzle_module.setUserBonusPuzzle = function () {
        puzzle_module.setUserBonusPuzzle = cache_set;
      };
      puzzle_module.getUserBonusPuzzle = function () {
        return {
          id: 'three_match',
          time_left: 200,
          curr_ms: 20
        };
      };
      map_extra.puzzleComplete();
      puzzle_module.getUserBonusPuzzle = cache_get;
      done();
    });
    it('should not load the next puzzle if next param given ' +
        'but ms cannot be focused', function (done) {
      var cache_get = puzzle_module.getUserBonusPuzzle,
        cache_set = puzzle_module.setUserBonusPuzzle,
        cache_loading = loading.once,
        cache_load = puzzle_module.load,
        cache_event_finish = map_extra.showEventFinish;

      map_extra.showEventFinish = function () {
        map_extra.showEventFinish = cache_event_finish;
      };

      map_extra.refresh = function () {};
      map_extra.canFocusMs = function () {
        return true;
      };
      loading.once = function (type, cb) {
        loading.once = cache_loading;
        cb();
      };
      puzzle_module.load = function () {
        puzzle_module.load = cache_load;
        done('err');
      };
      puzzle_module.setUserBonusPuzzle = function () {
        puzzle_module.setUserBonusPuzzle = cache_set;
      };
      puzzle_module.getUserBonusPuzzle = function () {
        return {
          id: 'three_match',
          time_left: 200,
          curr_ms: 20
        };
      };
      map_extra.puzzleComplete(true);
      puzzle_module.getUserBonusPuzzle = cache_get;
      done();
    });
  });
  describe('showPlayPopup', function () {
    it('should call Map screen showPlayPopup', function (done) {
      var cache = QuestMap.prototype.showPlayPopup;

      QuestMap.prototype.showPlayPopup = function () {
        QuestMap.prototype.showPlayPopup = cache;
        done();
      };
      map_extra.showPlayPopup(20, function () {});
    });
  });

  describe('showEventFinish', function () {
    it('should build event finish popup', function (done) {
      var cache = map_extra.event_finish.build,
        cache_get = GC.app.user.get,
        cache_loading = loading.once;

      loading.once = function (type, cb) {
        loading.once = cache_loading;
        cb();
      };

      map_extra._setLastMs(20);
      GC.app.user.get = function () {
        GC.app.user.get = cache_get;
        return true;
      };
      map_extra.event_finish.build = function () {
        map_extra.event_finish.build = cache;
        done();
      };

      assert.equal(map_extra.showEventFinish(21, undefined), true);
    });

    it('should build event if current ms equals limit', function (done) {
      var cache = map_extra.event_finish.build,
        cache_get = GC.app.user.get,
        cache_loading = loading.once;

      loading.once = function (type, cb) {
        loading.once = cache_loading;
        cb();
      };

      map_extra._setLastMs(20);
      GC.app.user.get = function () {
        GC.app.user.get = cache_get;
        return true;
      };
      map_extra.event_finish.build = function () {
        map_extra.event_finish.build = cache;
        done();
      };

      assert.equal(map_extra.showEventFinish(20, undefined), true);
    });

    it('should not build event finish popup', function (done) {
      var cache = map_extra.event_finish.build,
        cache_get = GC.app.user.get,
        cache_loading = loading.once;

      loading.once = function (type, cb) {
        loading.once = cache_loading;
        cb();
      };

      map_extra._setLastMs(20);
      GC.app.user.get = function () {
        GC.app.user.get = cache_get;
        return false;
      };
      map_extra.event_finish.build = function () {
        map_extra.event_finish.build = cache;
        done('error');
      };

      assert.equal(map_extra.showEventFinish(21, undefined), false);
      done();
    });

    it('should not build event finish popup if current ' +
      'ms is not max', function (done) {
      var cache = map_extra.event_finish.build,
        cache_get = GC.app.user.get,
        cache_loading = loading.once;

      loading.once = function (type, cb) {
        loading.once = cache_loading;
        cb();
      };

      map_extra._setLastMs(20);
      GC.app.user.get = function () {
        GC.app.user.get = cache_get;
        return true;
      };
      map_extra.event_finish.build = function () {
        map_extra.event_finish.build = cache;
        done('error');
      };

      assert.equal(map_extra.showEventFinish(18, undefined), false);
      done();
    });

    it('should remove listeners once popup close', function (done) {
      var cache = map_extra.event_finish.build,
        cache_get = GC.app.user.get,
        cache_loading = loading.once,
        cache_remove_listener = popup.removeAllListeners,
        i = 0;

      loading.once = function (type, cb) {
        loading.once = cache_loading;
        cb();
      };

      map_extra._setLastMs(20);
      GC.app.user.get = function () {
        GC.app.user.get = cache_get;
        return true;
      };
      map_extra.event_finish.build = function () {
        map_extra.event_finish.build = cache;
      };
      popup.removeAllListeners = function (event) {
        if ((event === 'event-finish:ok' || event === 'event-finish:ok') &&
          ++i) {
          popup.removeAllListeners = cache_remove_listener;
          done();
        }
      };

      assert.equal(map_extra.showEventFinish(21, undefined), true);

      popup.emit('event-finish:close');
    });
  });

  describe('getPuzzleDataToLoad', function () {
    it('should return proper data to load', function () {
      var cache = puzzle_module.loadBonusMapPuzzle,
        cache_get = puzzle_module.getUserBonusPuzzle;

      puzzle_module.getUserBonusPuzzle = function () {
        puzzle_module.getUserBonusPuzzle = cache_get;
        return {
          22: {
            id: 'three_match',
            time_left: 200,
            curr_ms: 20,
            stars: 2
          }
        };
      };
      puzzle_module.loadBonusMapPuzzle = function () {
        puzzle_module.loadBonusMapPuzzle = cache;
        return {then: function (cb) {
          var response = cb({
            id: 'three_match'
          });

          assert.strictEqual(true, _.isEqual({
            id: 'three_match',
            stars: 2
          }, response));
        }};
      };
      map_extra.getPuzzleDataToLoad(22);
    });
    it('should set star to zero if not is bonus_data', function () {
      var cache = puzzle_module.loadBonusMapPuzzle,
        cache_get = puzzle_module.getUserBonusPuzzle;

      puzzle_module.getUserBonusPuzzle = function () {
        puzzle_module.getUserBonusPuzzle = cache_get;
        return {
          id: 'three_match',
          time_left: 200,
          curr_ms: 20
        };
      };
      puzzle_module.loadBonusMapPuzzle = function () {
        puzzle_module.loadBonusMapPuzzle = cache;
        return {then: function (cb) {
          var response = cb({
            id: 'three_match'
          });

          assert.strictEqual(true, _.isEqual({
            id: 'three_match',
            stars: 0
          }, response));
        }};
      };
      map_extra.getPuzzleDataToLoad(22);
    });
  });
  describe('renderNodes', function () {
    it('should render nodes for map if ms_id is> max_ms', function () {
      var cache = map_extra.map.getModel,
        i = 0;

      puzzle_module.getUserBonusPuzzle = function () {
        return {
          id: 'three_match',
          max_ms: 5
        };
      };

      map_extra.map.getModel = function () {
        map_extra.map.getModel = cache;
        return {
          getNodesByTag: function () {
            return [{
              id: '12'
            }, {
              id: '15'
            },
            {
              id: '14'
            }];
          },
          addTagById: function (ms, type) {
            assert.strictEqual(type, 'locked');
            i++;
          }
        };
      };
      map_extra.renderNodes();
      assert.strictEqual(3, i);
    });
    it('should add tags for map ms_id is<=max_ms', function () {
      var cache = map_extra.map.getModel,
        i = 0;

      puzzle_module.getUserBonusPuzzle = function () {
        return {
          id: 'three_match',
          max_ms: 30,
          12: {
            stars: 2
          },
          15: {
            stars: 2
          }
        };
      };

      map_extra.map.getModel = function () {
        map_extra.map.getModel = cache;
        return {
          getNodesByTag: function () {
            return [{
              id: '12'
            }, {
              id: '15'
            },
            {
              id: '14'
            }];
          },
          removeTagById: function (ms, type) {
            assert.strictEqual(type, 'locked');
            i++;
          }
        };
      };
      map_extra.renderNodes();
      assert.strictEqual(3, i);
    });
  });
});
