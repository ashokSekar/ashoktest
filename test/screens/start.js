/* global StartScreen, User, style, popup_manager, puzzle_module,
  BonusMap, loading */

jsio('import quest.models.user as User');

describe('start Screen', function () {
  'use strict';

  var start_screen,
    prepare = function () {
      GC.app = {
        pop: function () {},
        push: function () {},
        engine: {
          subscribe: function () {}
        },
        user: new User(),
        view: {
          updateOpts: function () {}
        },
        set3DTouchPending: function () {},
        on: function () {}
      };
      jsio('import DevkitHelper.style as style');
      style.init({
        orientation: 'portrait',
        bound_width: 640,
        bound_height: 960,
        tablet: 0.8,
        phablet: 0.9
      });
      jsio('import quest.screens.start as StartScreen');
      jsio('import quest.modules.popup_manager as popup_manager');
      jsio('import src.screens.map_extra as BonusMap');
      jsio('import DevkitHelper.loading as loading');
      jsio('import quest.modules.puzzle as puzzle_module');
    },
    init = function () {
      start_screen = new StartScreen();
    };

  before(prepare);
  beforeEach(init);

  describe('build()', function () {
    it('it should add title to play btn', function () {
      start_screen.build();
      assert.strictEqual('start_screen_play',
        start_screen.play_btn.getText().getText());
    });

    it('should register loadMap for load-map event', function (done) {
      var cache = GC.app.on,
        cache_load = start_screen.loadMap;

      start_screen.loadMap = function () {
        start_screen.loadMap = cache_load;

        done();
      };

      GC.app.on = function (evt, cb) {
        if (evt === 'load-map') {
          GC.app.on = cache;
          cb();
        }
      };

      start_screen.build();
    });
  });

  describe('getBonusMap()', function () {
    it('should return cached instance', function () {
      start_screen.bonus_map = true;

      assert.strictEqual(true, start_screen.getBonusMap());
    });

    it('should return new instance if cache miss', function () {
      var cache = puzzle_module.getBonusMapType;

      start_screen.bonus_map = null;

      puzzle_module.getBonusMapType = function () {
        puzzle_module.getBonusMapType = cache;
        return 'three_match';
      };
      assert.strictEqual(true, start_screen.getBonusMap() instanceof BonusMap);
      BonusMap.prototype.getBonusMapType = cache;
    });
  });

  describe('loadMap()', function () {
    it('should invoke showMap if bonus false', function (done) {
      var cache = start_screen.showMap;

      start_screen.showMap = function () {
        start_screen.showMap = cache;
        done();
      };

      start_screen.loadMap(false);
    });

    it('should build bonusmap', function (done) {
      var cache_get = start_screen.getBonusMap,
        cache_push = GC.app.push,
        cache_show = loading.show;

      start_screen.getBonusMap = function () {
        start_screen.getBonusMap = cache_get;

        return {
          id: 'test_bonus',
          build: function () {}
        };
      };

      GC.app.push = function (map) {
        if (map.id === 'test_bonus') {
          GC.app.push = cache_push;
          done();
        }
      };

      loading.show = function (id, cb) {
        loading.show = cache_show;

        assert.strictEqual('map_1', id);
        cb();
      };

      start_screen.loadMap(true);
    });
  });

  describe('on3DTouch()', function () {
    it('should invoke show map', function (done) {
      var cache = start_screen.showMap;

      start_screen.showMap = function () {
        start_screen.showMap = cache;
        done();
      };

      start_screen.on3DTouch();
    });

    it('should invoke popup.closeAll with cb', function (done) {
      var cache = start_screen.showMap,
        cache_stack = popup_manager.getStack,
        cache_close = popup_manager.closeAll;

      popup_manager.getStack = function () {
        popup_manager.getStack = cache_stack;
        return [1, 2];
      };

      popup_manager.closeAll = function (cb) {
        popup_manager.closeAll = cache_close;
        cb.fire();
      };

      start_screen.showMap = function () {
        start_screen.showMap = cache;
        done();
      };

      start_screen.on3DTouch();
    });
  });
});
