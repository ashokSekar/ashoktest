/* global Application, QuestApplication, event_manager, style, Tutorial,
 util_test, _
*/

jsio('import test.lib.util as util_test');
jsio('import util.underscore as _');

describe('Application', function () {
  'use strict';

  var app,
    prepare = function () {
      GC.app = {
        view: {
          updateOpts: function () {}
        },
        engine: {
          subscribe: function () {}
        }
      };
      jsio('import DevkitHelper.tutorial as Tutorial');
      jsio('import DevkitHelper.event_manager as event_manager');
      jsio('import DevkitHelper.style as style');
      style.init({
        orientation: 'portrait',
        bound_width: 640,
        bound_height: 960,
        tablet: 0.8,
        phablet: 0.9
      });
      jsio('import src.Application as Application');
      jsio('import quest.Application as QuestApplication');
    },
    init = function () {
      app = new Application();
    };

  before(prepare);
  beforeEach(init);

  describe('Application()', function () {
    it('should be instance of quest Application', function () {
      assert.strictEqual(true, app instanceof QuestApplication);
    });
  });

  describe('initUi()', function () {
    it('should invoke event_manager register', function () {
      var cache = event_manager.register,
        cache_event = event_manager.emit;

      event_manager.register = function (path, modules) {
        assert.strictEqual(path, 'quest.modules');
        assert.deepEqual(modules, ['flurry', 'firebase', 'facebook',
          'freshchat', 'kochava', 'onesignal', 'achievements', 'crashlytics']);
        event_manager.register = cache;
      };
      event_manager.emit = function () {
        event_manager.emit = cache_event;
      };
      app.initUI();
    });

    it('should create tutorial instance', function () {
      var cache_event = event_manager.emit;

      event_manager.emit = function () {
        event_manager.emit = cache_event;
      };
      app.initUI();
      assert.strictEqual(true, app.tutorial instanceof Tutorial);
    });
  });

  describe('startGame()', function () {
    it('should register max_ms change for update3DTouch', function (done) {
      var cache = app.update3DTouch,
        cache_parent = QuestApplication.prototype.startGame;

      app.update3DTouch = function () {
        app.update3DTouch = cache;
        done();
      };
      QuestApplication.prototype.startGame = function () {
        QuestApplication.prototype.startGame = cache_parent;
      };

      app.startGame();
      GC.app.user.increment('max_ms');
    });
  });

  describe('update3DTouch()', function () {
    it('shouldnt update 3dtouch options for fb connected', function (done) {
      var conf_data = util_test.getFromJSIOCache('resources/data/config')
        .exports,
        cache_parent = QuestApplication.prototype.update3DTouch;

      conf_data.touch3d = {
        a: {
          icon: 'aaa',
          facebook: false
        },
        b: {
          icon: 'bbb',
          facebook: true
        }
      };

      app.user = GC.app.user;
      app.user.set({
        social_facebook: false,
        max_ms: 2
      }, true);
      QuestApplication.prototype.update3DTouch = function (data) {
        QuestApplication.prototype.update3DTouch = cache_parent;
        assert.strictEqual(1, _.size(data));
        assert.strictEqual('aaa', data.a.icon);
        done();
      };
      app.update3DTouch();
    });

    it('should update 3dtouch options for fb connected', function (done) {
      var conf_data = util_test.getFromJSIOCache('resources/data/config')
        .exports,
        cache_parent = QuestApplication.prototype.update3DTouch;

      conf_data.touch3d = {
        a: {
          icon: 'aaa',
          facebook: false
        },
        b: {
          icon: 'bbb',
          facebook: true
        }
      };

      app.user = GC.app.user;
      app.user.set({
        social_facebook: true,
        max_ms: 2
      }, true);
      QuestApplication.prototype.update3DTouch = function (data) {
        QuestApplication.prototype.update3DTouch = cache_parent;
        assert.strictEqual(2, _.size(data));
        assert.strictEqual('aaa', data.a.icon);
        assert.strictEqual('bbb', data.b.icon);
        done();
      };
      app.update3DTouch();
    });

    it('shouldnt update 3dtouch if max_mx < 2', function (done) {
      var conf_data = util_test.getFromJSIOCache('resources/data/config')
        .exports,
        cache_parent = QuestApplication.prototype.update3DTouch;

      conf_data.touch3d = {
        a: {
          icon: 'aaa',
          facebook: false
        },
        b: {
          icon: 'bbb',
          facebook: true
        }
      };

      app.user = GC.app.user;
      app.user.set({
        social_facebook: true,
        max_ms: 1
      }, true);
      QuestApplication.prototype.update3DTouch = function () {
        QuestApplication.prototype.update3DTouch = cache_parent;
        done('error');
      };
      app.update3DTouch();
      QuestApplication.prototype.update3DTouch = cache_parent;
      done();
    });
  });

  describe('set3DTouchPending()', function () {
  });

  describe('check3DTouch()', function () {
    it('should invoke on3DTouch if 3d touch pending', function (done) {
      var cache = app.on3DTouch;

      app.on3DTouch = function (tag) {
        app.on3DTouch = cache;

        done(tag === 'taga' ? undefined : 'error');
      };

      app.set3DTouchPending('taga');
      app.check3DTouch();
    });

    it('shouldnt invoke on3DTouch if no pending 3d touch', function (done) {
      var cache = app.on3DTouch;

      app.on3DTouch = function () {
        app.on3DTouch = cache;

        done('error');
      };

      app.set3DTouchPending(null);
      app.check3DTouch();
      app.on3DTouch = cache;
      done();
    });
  });
});
