/* global global */

jsio('import util.underscore as _');

exports = {
  removeFromJSIOCache: function (file_name) {
    'use strict';

    var to_remove,
      cache_modules = global.jsio.__modules;

    to_remove = _.find(_.keys(cache_modules), function (key) {
      return key.indexOf(file_name) !== -1;
    });

    if (to_remove) {
      delete cache_modules[to_remove];
    }
  },
  getFromJSIOCache: function (file_name) {
    'use strict';

    var to_get,
      cache_modules = global.jsio.__modules;

    to_get = _.find(_.keys(cache_modules), function (key) {
      return key.indexOf(file_name) !== -1;
    });

    return cache_modules[to_get];
  },
  assertSignal: function (done, obj, signal, test, time) {
    /*Params
      done : done from mocha
      obj: object on which signal emits
      signal: name of signal emitted
      test: true if signal must emit false otherwise
      time: how long will it take for signal to emit
    */
    'use strict';

    var status = false;

    obj.on(signal, function () {
      status = true;
    });
    setTimeout(function () {
      if (status === test) {
        done();
      } else {
        done(signal + ' test failed');
      }
      obj.removeAllListeners(signal);
    }, time || 10);
  },

  /*
   Params:
    next: done method to be called
    obj: obj whose property has to be cached
    prop: property that has to be cached
    before: assertions to be done before calling done
  */
  callTest: function (next, obj, prop, before) {
    'use strict';

    var cache = obj[prop];

    obj[prop] = function () {
      if (before) {
        before.apply(this, arguments);
      }
      obj[prop] = cache;
      next();
    };
  }
};
