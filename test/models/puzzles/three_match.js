/* global config, PuzzleModel, TileModel, util_test, util,
   User, event_manager, style, Promise, sounds, ParentPuzzle, puzzle_module */

jsio('import util.underscore as _');
jsio('import test.lib.util as util_test');
jsio('import timer');
jsio('import quest.lib.bluebird as Promise');

describe('Puzzle Model', function () {
  'use strict';

  var puzzle, tile,
    config_data = {
      server: 'http://localhost:3000',
      board_data: {
        rows: 30,
        columns: 30,
        depth: 5
      },
      orientation: 'portrait',
      bound_width: 640,
      bound_height: 960,

      puzzle_attempt: 64,

      max_tiles: [9, 9, 9, 4, 4, 3, 3],
      swoosh_pairs: 5,

      powerups: {
        ingame: {
          shuffle: true,
          hint: true,
          swoosh: true
        },
        pregame: {
          open_slot: true,
          joker_tile: true
        }
      },
      auto_hint_interval: 5000,
      lives: {
        max: 5,
        gen: 1000
      },
      family_score: [100, 150, 200, 300, 250, 400, 350],
      puzzle_load: {
        retry: 64,
        info_timeout: 1,
        track: 5
      },
      jokers_per_level: 2,
      special_faces: {
        joker: 'f_joker',
        golden: 'f_golden'
      },
      variations: {
        memory: {
          id: 'memory',
          powerups_exclude: ['shuffle', 'hint', 'half_hint']
        },
        partial_memory: {
          id: 'partial_memory',
          powerups_exclude: []
        },
        jewels: {
          id: 'jewels',
          powerups_exclude: []
        }
      },
      max_type: 5,
      bonus_level_def: [[20, 40], [13, 15], [7, 30]]
    },
    families = [
      {
        pairs: 1,
        types: 1
      },
      {
        pairs: 4,
        types: 3
      },
      {
        pairs: 1,
        types: 1
      },
      {
        pairs: 1,
        types: 1
      },
      {
        pairs: 3,
        types: 2
      },
      {
        pairs: 1,
        types: 1
      }
    ],
    puzzle_data = {
      total_tiles: 8,
      layout: [
        [2, 0, 0], [0, 2, 0], [2, 2, 0],
        [4, 2, 0], [2, 4, 0], [2, 2, 1],
        [8, 1, 0], [8, 5, 0]],
      id: 'test',
      families: families,
      time: 60,
      init: [],
      solved: []
    },
    product_data = {
      time: {
        quantity: 10,
        cost: 5
      }
    },
    cache = [],
    setup_common = function () {
      var board_config = config.board_data;

      puzzle = new PuzzleModel();
      tile = new TileModel();

      puzzle.set({
        id: puzzle_data.id,
        tile_count: puzzle_data.total_tiles,
        rows: board_config.rows,
        columns: board_config.columns,
        depth: board_config.depth,
        time: 0
      });
      cache = [puzzle.assignFaces, puzzle.setTiles];
      puzzle.assignFaces = function () {
        return Promise.resolve(true);
      };
      puzzle.setTiles = function () {};
    },
    init = function () {
      jsio('import resources.data.config as config');
      util_test.removeFromJSIOCache('src/modules/util');
      util_test.getFromJSIOCache('resources/data/config').exports = config_data;

      jsio('import quest.modules.util as util');
      util.getProductData = function () {
        return product_data;
      };

      jsio('import quest.models.user as User');
      jsio('import quest.models.puzzle as QuestPuzzle');
      jsio('import src.models.puzzle as ParentPuzzle');
      jsio('import src.models.puzzles.three_match as PuzzleModel');
      jsio('import src.models.tile as TileModel');
      jsio('import DevkitHelper.event_manager as event_manager');
      jsio('import DevkitHelper.style as style');
      jsio('import DevkitHelper.loading as loading');
      util_test.removeFromJSIOCache('quest/modules/sounds.js');
      jsio('import quest.modules.sounds as sounds');
      jsio('import quest.modules.puzzle as puzzle_module');

      GC.app = {
        view: {
          updateOpts: function () {}
        },
        user: new User(),
        engine: {
          subscribe: function () {}
        }
      };

      GC.app.user.set({
        lives: 3,
        max_ms: 1,
        curr_ms: 1,
        infinite_lives_remaining: 0
      }, true);
      event_manager.register('', []);
      style.init({
        orientation: 'portrait',
        bound_width: 640,
        bound_height: 960,
        tablet: 0.8,
        phablet: 0.9
      });
    };

  before(init);
  beforeEach(setup_common);

  describe('randomiseFaces()', function () {
    it('should randomize faces of tiles', function () {
      var tile_pair_org = puzzle.allocateTiles(families),
        faces = [
                  'f0_8',
                  'f0_9',
                  'f2_1',
                  'f3_2',
                  'f4_3',
                  'f5_4',
                  'f0_8',
                  'f0_9',
                  'f2_1',
                  'f3_2',
                  'f4_3',
                  'f5_4'],
        cache = puzzle.allocateTiles;

      puzzle.allocateTiles = function () {
        puzzle.allocateTiles = cache;
        return faces;
      };

      assert.notEqual(tile_pair_org,
        puzzle.randomiseFaces(families));
      assert.equal(tile_pair_org.length * 2,
        puzzle.randomiseFaces(families).length);
    });
  });

  describe('allocateTiles()', function () {
    it('it should reduce family to 2/3rd of puzzle data', function () {
      var resp,
        cache = ParentPuzzle.prototype.allocateTiles;

      ParentPuzzle.prototype.allocateTiles = function () {
        var resp = [],
          curr = 0;

        ParentPuzzle.prototype.allocateTiles = cache;
        while (resp.length < 108) {
          resp.push('f' + curr);

          if (++curr >= 60) {
            curr = 0;
          }
        }

        assert.strictEqual(60, _.keys(_.groupBy(resp)).length);

        return resp;
      };

      puzzle.set({
        tile_count: 108,
        tile_per_match: 3,
        reduced_family: 2 / 3
      });
      resp = puzzle.allocateTiles();
      assert.strictEqual(40, _.keys(_.groupBy(resp)).length);
      puzzle.set('reduced_family', undefined);
    });
  });

  describe('savePuzzleData', function () {
    it('should call supr of three_match with proper args', function () {
      ParentPuzzle.prototype.savePuzzleData = function (exit_type, data) {
        assert.strictEqual(exit_type, 'quit');
        assert.strictEqual(data[0], 'reduced_family');
      };
      puzzle.savePuzzleData('quit');
    });
  });
  describe('gameOver', function () {
    it('should call supr of three_match with proper args', function () {
      var cache = puzzle_module.getUserBonusPuzzle,
        cache_set = puzzle_module.setUserBonusPuzzle;

      puzzle.set('stars', 2);
      puzzle.set('score', 200);
      puzzle_module.getUserBonusPuzzle = function () {
        puzzle_module.getUserBonusPuzzle = cache;
        return {
          id: 'three_match',
          max_ms: 20,
          curr_ms: 20
        };
      };
      ParentPuzzle.prototype.gameOver = function () {
        return;
      };
      puzzle_module.setUserBonusPuzzle = function (data) {
        puzzle_module.setUserBonusPuzzle = cache_set;
        assert.strictEqual(data[20].stars, 2);
        assert.strictEqual(data[20].score, 200);
        assert.strictEqual(data.max_ms, 21);
      };
      puzzle.savePuzzleData = function (type) {
        assert.strictEqual(type, true);
      };
      puzzle.gameOver(true, function () {});
    });
    it('should should not increase the ms if not same as curr ms', function () {
      var cache = puzzle_module.getUserBonusPuzzle,
        cache_set = puzzle_module.setUserBonusPuzzle;

      puzzle.set('stars', 2);
      puzzle.set('score', 200);
      puzzle_module.getUserBonusPuzzle = function () {
        puzzle_module.getUserBonusPuzzle = cache;
        return {
          id: 'three_match',
          max_ms: 20,
          curr_ms: 25,
          25: {
            score: 250
          }
        };
      };
      ParentPuzzle.prototype.gameOver = function () {
        return;
      };
      puzzle_module.setUserBonusPuzzle = function (data) {
        puzzle_module.setUserBonusPuzzle = cache_set;
        assert.notStrictEqual(data.max_ms, 25);
      };
      puzzle.savePuzzleData = function (type) {
        assert.strictEqual(type, true);
      };
      puzzle.gameOver(true, function () { });
    });
    it('should should not increase the ms if not same as curr ms', function () {
      var cache = puzzle_module.getUserBonusPuzzle,
        cache_set = puzzle_module.setUserBonusPuzzle;

      puzzle.set('stars', 2);
      puzzle.set('score', 200);
      puzzle_module.getUserBonusPuzzle = function () {
        puzzle_module.getUserBonusPuzzle = cache;
        return {
          id: 'three_match',
          max_ms: 20,
          curr_ms: 21,
          21: {
            score: 150
          }
        };
      };
      ParentPuzzle.prototype.gameOver = function () {
        return;
      };
      puzzle_module.setUserBonusPuzzle = function (data) {
        puzzle_module.setUserBonusPuzzle = cache_set;
        assert.notStrictEqual(data[20].stars, 2);
        assert.notStrictEqual(data[20].score, 200);
        assert.notStrictEqual(data.max_ms, 22);
      };
      puzzle.savePuzzleData = function (type) {
        assert.strictEqual(type, false);
      };
      puzzle.gameOver(false, function () { });
    });
  });
  describe('generateSolvableGame()', function () {
    it('should call isBoardSolvable', function (done) {
      var cache = puzzle.isBoardSolvable;

      ParentPuzzle.prototype.generateSolvableGame = function () {
        return true;
      };

      puzzle.loadPuzzle(puzzle_data);
      puzzle.isBoardSolvable = function () {
        puzzle.isBoardSolvable = cache;
        done();
      };

      puzzle.generateSolvableGame([]);
    });
  });

  describe('pickTile()', function () {
    it('should add a tile if tile_selected is unset', function () {
      var model,
        position = [2, 2, 1];

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);

      puzzle.pickTile(model);

      assert.deepEqual(position,
        puzzle.get('tile_selected')[0].getCoordinates());
    });

    it('should return false if tapped on selected tile', function (done) {
      var model,
        cache = puzzle.isSameTile,
        position = [2, 2, 1];

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);

      puzzle.isSameTile = function () {
        puzzle.isSameTile = cache;
        return true;
      };

      done(puzzle.pickTile(model) === false ? undefined : 'error');
    });

    it('should call swap tile if model is swap', function (done) {
      var model,
        cache = puzzle.isSameTile,
        cache_swap = puzzle.swapTiles,
        position = [2, 2, 1];

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);

      puzzle.isSameTile = function () {
        puzzle.isSameTile = cache;
        return false;
      };

      puzzle.set('mode', 'swap');

      puzzle.swapTiles = function () {
        puzzle.set('mode', null);
        puzzle.swapTiles = cache_swap;
        done();
      };

      model.isVisible = function () {
        return true;
      };

      puzzle.pickTile(model);
    });

    it('should call swap tile if mode is swap with ' +
      'tile_selected', function (done) {
      var model,
        cache = puzzle.isSameTile,
        cache_swap = puzzle.swapTiles,
        position = [2, 2, 1];

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);

      puzzle.set('tile_selected', {});

      puzzle.isSameTile = function () {
        puzzle.isSameTile = cache;
        return false;
      };

      puzzle.set('mode', 'swap');

      puzzle.swapTiles = function (model, tile_selected) {
        puzzle.set('mode', null);
        assert.deepEqual(tile_selected, {});
        puzzle.swapTiles = cache_swap;
        done();
      };

      model.isVisible = function () {
        return true;
      };

      puzzle.pickTile(model);
    });

    it('should call not call swapTile or matchTile', function (done) {
      var model,
        cache = puzzle.isSameTile,
        cache_swap = puzzle.swapTiles,
        cache_match = puzzle.matchTile,
        position = [2, 2, 1];

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);

      puzzle.isSameTile = function () {
        puzzle.isSameTile = cache;
        return false;
      };

      puzzle.set('mode', 'swap');

      puzzle.swapTiles = function () {
        done('error');
      };

      puzzle.matchTile = function () {
        done('error');
      };

      model.isVisible = function () {
        return true;
      };
      puzzle.set('pick_tile', false);
      puzzle.pickTile(model);
      puzzle.set('pick_tile', true);
      done();

      puzzle.swapTiles = cache_swap;
      puzzle.matchTile = cache_match;
    });

    it('should call not call swapTile or matchTile if' +
      'tile not free', function (done) {
      var model,
        cache = puzzle.isSameTile,
        cache_swap = puzzle.swapTiles,
        cache_match = puzzle.matchTile,
        position = [2, 2, 1];

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);

      puzzle.isSameTile = function () {
        puzzle.isSameTile = cache;
        return false;
      };

      puzzle.swapTiles = function () {
        done('error');
      };

      puzzle.matchTile = function () {
        done('error');
      };

      model.isVisible = function () {
        return true;
      };
      model.isFree = function () {
        return false;
      };
      puzzle.pickTile(model);
      done();

      puzzle.swapTiles = cache_swap;
      puzzle.matchTile = cache_match;
    });

    it('should invoke tileMatched if tiles faces match', function (done) {
      var model_one, model_two, model_three,
        position_one = [2, 2, 1],
        position_two = [2, 0, 0];

      puzzle.tileMatched = function () {
        done();
      };

      puzzle.loadPuzzle(puzzle_data);
      model_one = puzzle.getTile(position_one);
      model_two = puzzle.getTile(position_two);
      model_one.set('face', 1);
      model_two.set('face', 1);
      puzzle.set('tile_selected', [model_one, model_two]);

      model_three = puzzle.getTile([8, 5, 0]);
      model_three.set('face', 1);

      puzzle.pickTile(model_three);
    });

    it('should call wrongMatch', function (done) {
      var model_one, model_two, model_three,
        position_one = [2, 2, 1],
        position_two = [2, 0, 0],
        cache = puzzle.wrongMatch;

      puzzle.tileMatched = function () {};

      puzzle.loadPuzzle(puzzle_data);
      model_one = puzzle.getTile(position_one);
      model_two = puzzle.getTile(position_two);
      model_one.set('face', 1);
      model_two.set('face', 1);
      puzzle.set('tile_selected', [model_one, model_two]);

      model_three = puzzle.getTile([8, 5, 0]);
      model_three.set('face', 2);

      puzzle.wrongMatch = function () {
        puzzle.wrongMatch = cache;
        done();
      };

      puzzle.pickTile(model_three);
    });

    it('should emit tiles-matched when tile faces match',
      function (done) {
      var model_one, model_two, model_three,
        position_one = [2, 2, 1],
        position_two = [2, 0, 0];

      puzzle.on('tiles-matched', function () {
        done();
      });

      puzzle.loadPuzzle(puzzle_data);
      model_one = puzzle.getTile(position_one);
      model_two = puzzle.getTile(position_two);
      model_one.set('face', 1);
      model_two.set('face', 1);
      puzzle.set('tile_selected', [model_one, model_two]);

      model_three = puzzle.getTile([8, 5, 0]);
      model_three.set('face', 1);

      puzzle.pickTile(model_three);
    });

    it('should update tile_selected for second selection',
      function (done) {
      var model_one, model_two,
        position_one = [2, 2, 1],
        position_two = [2, 0, 0];

      puzzle.loadPuzzle(puzzle_data);
      model_one = puzzle.getTile(position_one);
      model_two = puzzle.getTile(position_two);
      model_one.set('face', 1);
      model_two.set('face', 1);
      puzzle.set('tile_selected', [model_one]);

      puzzle.pickTile(model_two);

      assert.deepEqual(puzzle.get('tile_selected'),
        [model_one, model_two]);
      done();
    });

    it('should call updateJokerPair if first pair is joker',
      function (done) {
      var model_one, model_two, model_three,
        position_one = [2, 2, 1],
        position_two = [2, 0, 0];

      puzzle.on('tiles-matched', function () {
      });

      puzzle.loadPuzzle(puzzle_data);
      model_one = puzzle.getTile(position_one);
      model_two = puzzle.getTile(position_two);
      model_one.set('face', 'f_joker');
      model_two.set('face', 1);
      puzzle.set('tile_selected', [model_one, model_two]);

      model_three = puzzle.getTile([8, 5, 0]);
      model_three.set('face', 1);
      puzzle.updateJokerPair = function () {
        done();
      };
      puzzle.pickTile(model_three);
    });

    it('should call updateJokerPair if second pair is joker',
      function (done) {
      var model_one, model_two, model_three,
        position_one = [2, 2, 1],
        position_two = [2, 0, 0];

      puzzle.on('tiles-matched', function () {
      });

      puzzle.loadPuzzle(puzzle_data);
      model_one = puzzle.getTile(position_one);
      model_two = puzzle.getTile(position_two);
      model_one.set('face', 1);
      model_two.set('face', 'f_joker');
      puzzle.set('tile_selected', [model_one, model_two]);

      model_three = puzzle.getTile([8, 5, 0]);
      model_three.set('face', 1);
      puzzle.updateJokerPair = function () {
        puzzle.updateJokerPair = cache;
        done();
      };
      puzzle.pickTile(model_three);
    });

    it('should not match tile even one is joker but' +
      'other two are not same', function (done) {
      var model_one, model_two, model_three,
        position_one = [2, 2, 1],
        position_two = [2, 0, 0],
        cache = puzzle.tileMatched;

      puzzle.on('tiles-matched', function () {
      });

      puzzle.loadPuzzle(puzzle_data);
      model_one = puzzle.getTile(position_one);
      model_two = puzzle.getTile(position_two);
      model_one.set('face', 1);
      model_two.set('face', 'f_joker');
      puzzle.set('tile_selected', [model_one, model_two]);

      model_three = puzzle.getTile([8, 5, 0]);
      model_three.set('face', 2);
      puzzle.tileMatched = function () {
        done('error');
      };
      puzzle.tileMatched = cache;
      puzzle.pickTile(model_three);
      done();
    });

    it('should play tap sound', function (done) {
      var model,
        position = [2, 2, 1],
        cache = sounds.play;

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);

      sounds.play = function (name) {
        sounds.play = cache;
        done(name === 'tap' ? undefined : 'error');
      };

      puzzle.pickTile(model);
    });
  });

  describe('isSameTile()', function () {
    it('should return true', function (done) {
      var model,
        position = [2, 2, 1];

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);

      if (puzzle.isSameTile([model], model)) {
        done();
      }
    });

    it('should return false', function (done) {
      var model_one, model_two, model_three,
        position_one = [2, 2, 1],
        position_two = [2, 0, 0],
        position_three = [8, 5, 0];

      puzzle.loadPuzzle(puzzle_data);
      model_one = puzzle.getTile(position_one);
      model_two = puzzle.getTile(position_two);
      model_three = puzzle.getTile(position_three);

      if (puzzle.isSameTile([model_one, model_two], model_three)) {
        done('error');
      }
      done();
    });
  });

  describe('wrongMatch()', function () {
    it('should emit wrong-match-animate thrice', function (done) {
      var model,
        position = [2, 2, 1],
        i = 0;

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);

      model.on('wrong-match-animate', function () {
        if (++i >= 3) {
          done();
        }
      });
      puzzle.wrongMatch([model, model, model]);
      assert.deepEqual([model], puzzle.get('tile_selected'));
    });
  });

  describe('highlight', function () {
    it('should change state if in swap mode', function (done) {
      puzzle.set('mode', 'swap');
      puzzle.highlight([tile]);
      if (tile.get('state') === 2) {
        done('error');
      } else {
        puzzle.set('swap', null);
        done();
      }
    });

    it('should change state of tile if passed', function () {
      puzzle.highlight([tile]);
      assert.strictEqual(tile.get('state'), 2);
    });

    it('should highlight both tiles if match', function () {
      var cache = puzzle.getPrevious,
        tile2 = new TileModel();

      tile2.set({
        face: tile.get('face'),
        state: 2
      });
      puzzle.getPrevious = function () {
        puzzle.getPrevious = cache;
        return tile2;
      };

      puzzle.highlight([tile]);
      assert.strictEqual(tile2.get('state'), 1);
      assert.strictEqual(tile.get('state'), 2);
    });

    it('should reset state to orginal if previous didnt match new',
      function () {
      var cache = puzzle.getPrevious;

      puzzle.getPrevious = function () {
        puzzle.getPrevious = cache;
        return [tile];
      };

      puzzle.highlight();
      assert.strictEqual(tile.get('state'), 1);
    });

    it('should get last_match blank array of no moves made',
      function () {
      var cache = puzzle.get;

      puzzle.get = function (key) {
        if (key === 'last_match') {
          puzzle.get = cache;
          return null;
        }
      };
      puzzle.updateGameState([], false, false);
    });
  });

  describe('afterSwooshAnimate()', function () {
    it('should invoke cleanTile', function () {
      var position = [2, 2, 1],
        count = 0,
        cache = puzzle.cleanTile,
        model;

      puzzle.cleanTile = function () {
        count++;
      };
      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);
      puzzle.afterSwooshAnimate([model, model, model]);
      assert.equal(3, count);
      puzzle.cleanTile = cache;
    });

    it('should call swooshAnimate', function (done) {
      var position = [2, 2, 1],
        cache = puzzle.swooshAnimate,
        cache_cl = puzzle.cleanTile,
        model;

      puzzle.cleanTile = function () {};
      puzzle.swooshAnimate = function () {
        puzzle.swooshAnimate = cache;
        done();
      };
      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);
      puzzle.set('swoosh_pairs', [[model, model]]);
      puzzle.afterSwooshAnimate([model, model, model]);
      puzzle.cleanTile = cache_cl;
    });

    it('should call updateGameState', function (done) {
      var position = [2, 2, 1],
        cache = puzzle.updateGameState,
        model;

      puzzle.loadPuzzle(puzzle_data);
      puzzle.cleanTile = function () {};
      puzzle.set('swoosh_pairs', []);
      puzzle.updateGameState = function () {
        puzzle.updateGameState = cache;
        done();
      };
      model = puzzle.getTile(position);
      puzzle.afterSwooshAnimate([model, model, model]);
    });

    it('should set listener to resize-complete if ' +
      'tile count > 0', function (done) {
        var position = [2, 2, 1],
          cache = puzzle.updateGameState,
          cache_sw = puzzle.swoosh,
          cache_open = puzzle.isOpenSlotTile,
          model;

        puzzle.loadPuzzle(puzzle_data);
        puzzle.cleanTile = function () {};
        puzzle.set('swoosh_pairs', []);
        puzzle.updateGameState = function () {
          puzzle.updateGameState = cache;
        };
        puzzle.swoosh = function () {
          puzzle.swoosh = cache_sw;
          done();
        };
        puzzle.set('swooshed_count', 2);
        model = puzzle.getTile(position);
        puzzle.isOpenSlotTile = function () {
          puzzle.isOpenSlotTile = cache_open;

          return true;
        };
        puzzle.afterSwooshAnimate([model, model, model]);
        puzzle.emit('resize-complete');
      }
    );

    it('shouldnt set listener to resize-complete if ' +
      'tile count == 0', function (done) {
        var position = [2, 2, 1],
          cache = puzzle.updateGameState,
          cache_sw = puzzle.swoosh,
          model;

        puzzle.loadPuzzle(puzzle_data);
        puzzle.cleanTile = function () {};
        puzzle.set('swoosh_pairs', []);
        puzzle.updateGameState = function () {
          puzzle.updateGameState = cache;
        };
        puzzle.swoosh = function () {
          puzzle.swoosh = cache_sw;
          done('error');
        };
        puzzle.set('swooshed_count', 2);
        model = puzzle.getTile(position);
        puzzle.set('tile_count', 0);
        puzzle.afterSwooshAnimate([model, model, model]);
        puzzle.emit('resize-complete');
        puzzle.swoosh = cache_sw;
        done();
      }
    );

    it('should emit powerup-applied', function (done) {
      var position = [2, 2, 1],
        cache = puzzle.updateGameState,
        model;

      puzzle.loadPuzzle(puzzle_data);
      puzzle.cleanTile = function () {};
      puzzle.set('swoosh_pairs', []);
      puzzle.updateGameState = function () {
        puzzle.updateGameState = cache;
      };
      puzzle.on('powerup-applied', function () {
        done();
      });
      puzzle.set('swooshed_count', 5);
      model = puzzle.getTile(position);
      puzzle.afterSwooshAnimate([model, model, model]);
    });

    it('should last_match be a blank array no moves made', function (done) {
      var position = [2, 2, 1],
        count = 0,
        cache = puzzle.cleanTile,
        model,
        set_cache = puzzle.set,
        get_cache = puzzle.get;

      puzzle.cleanTile = function () {
        count++;
      };

      puzzle.set('last_match', null);
      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);
      puzzle.get = function (key) {
        if (key === 'last_match') {
          puzzle.get = get_cache;
          return null;
        }
      };
      puzzle.set = function (key, val) {
        if (key === 'last_match') {
          assert.notEqual(val, null);
          assert.strictEqual(Array.isArray(val), true);
          done();
        }
      };
      puzzle.afterSwooshAnimate([model, model, model]);
      assert.equal(3, count);
      puzzle.set = set_cache;
      puzzle.cleanTile = cache;
    });
  });

  describe('undo()', function () {
    var obj = [[0, 0, 0], [0, 4, 0], [0, 6, 0]];

    beforeEach(function () {
      puzzle.set('tiles', [[]]);
      puzzle.set({
        tile_count: 0,
        last_match: [[[0, 0, 0], [0, 4, 0], [0, 6, 0], 2]],
        solved: [],
        tile_per_match: 3
      });
    });

    it('should invoke render-tile for restored model', function () {
      var ctr = 0,
        cache = puzzle.changeFace;

      puzzle.on('render-tile', function () {
        ctr++;
      });

      puzzle.changeFace = function () {};

      puzzle.undo();
      assert.strictEqual(3, ctr);
      puzzle.changeFace = cache;
    });

    it('should increment undo', function () {
      var cache = puzzle.changeFace;

      puzzle.changeFace = function () {};
      puzzle.set('undo', 1);
      puzzle.undo();
      assert.strictEqual(2, puzzle.get('undo'));
      puzzle.changeFace = cache;
    });

    it('should invoke tiles-update for model neighbour', function () {
      var ctr = 0,
        cache = puzzle.changeFace;

      puzzle.changeFace = function () {};

      puzzle.on('tiles-update', function () {
        ctr++;
      });

      puzzle.undo();
      assert.strictEqual(1, ctr);
      puzzle.changeFace = cache;
    });

    it('should increment tile count', function () {
      var cache = puzzle.changeFace;

      puzzle.changeFace = function () {};
      puzzle.undo();
      assert.strictEqual(3, puzzle.get('tile_count'));
      puzzle.changeFace = cache;
    });

    it('should call loadTiles to to add tile model', function (done) {
      var cache = puzzle.changeFace;

      puzzle.changeFace = function () {};
      puzzle.placeTile = function () {};
      puzzle.getNeighbours = function () {};

      puzzle.loadTiles = function (tiles) {
        assert.deepEqual(obj, tiles);
        done();
      };
      puzzle.undo();
      puzzle.changeFace = cache;
    });

    it('should invoke placeTile to update tile dependency', function () {
      var ctr = 0,
        cache = puzzle.changeFace;

      puzzle.changeFace = function () {};
      puzzle.loadTiles = function () {};
      puzzle.getNeighbours = function () {};

      puzzle.getTile = function (param) {
        return param;
      };

      puzzle.placeTile = function (tile, face) {
        assert.deepEqual(obj[ctr], tile);
        assert.strictEqual(face, tile[3]);
        ctr++;
      };
      puzzle.undo();
      puzzle.changeFace = cache;
    });

    it('should unset tile_selected', function () {
      var cache = puzzle.changeFace;

      puzzle.changeFace = function () {};
      puzzle.set('tile_selected', {
        get: function () {
          return true;
        },
        set: function () {}
      });
      puzzle.undo();
      assert.strictEqual(null, puzzle.get('tile_selected'));
      puzzle.changeFace = cache;
    });
    it('should reset last_match', function () {
      var cache = puzzle.changeFace;

      puzzle.changeFace = function () {};
      puzzle.undo();
      assert.deepEqual([], puzzle.get('last_match'));
      puzzle.changeFace = cache;
    });

    it('should test for solved getting updated', function () {
      var cache = puzzle.changeFace;

      puzzle.changeFace = function () {};
      puzzle.set('solved', [2]);
      puzzle.undo();
      assert.deepEqual([], puzzle.get('solved'));
      puzzle.changeFace = cache;
    });

    it('should call change face thrise', function (done) {
      var cache = puzzle.changeFace,
        i = 0;

      puzzle.set('last_match', [[[1, 2, 3, 'f_joker'],
        [1, 2, 3, 'f_joker'], [1, 2, 3, 'f_joker'],
        [1, 2, 3, 'f_joker'], [1, 2, 3, 'f_joker'],
        [1, 2, 3, 'f_joker']]]);

      puzzle.changeFace = function () {
        if (++i >= 3) {
          done();
        }
      };
      puzzle.undo();
      puzzle.changeFace = cache;
    });

    it('should not call changeFace', function () {
      var cache = puzzle.changeFace;

      puzzle.set('last_match', [[[1, 2, 3, 'f_joker'],
        [1, 2, 3, 'f_joker'], [1, 2, 3, 'f_joker']]]);

      puzzle.changeFace = function () {
        assert.strictEqual(true, false);
      };
      puzzle.undo();
      puzzle.changeFace = cache;
    });
  });

  describe('updateJokerPair', function () {
    var tile_array = [
      {
        get: function (key) {
          if (key === 'face' || key === 'real_face') {
            return 'f1_18';
          } else {
            return null;
          }
        },
        set: function () {},
        getCoordinates: function () {
          return [2, 3, 4];
        },
        isJoker: function () {
          return true;
        },
        isLock: function () {
          return true;
        }
      },
      {
        get: function (key) {
          if (key === 'face' || key === 'real_face') {
            return 'f1_11';
          } else {
            return null;
          }
        },
        set: function () {},
        getCoordinates: function () {
          return [2, 3, 4];
        },
        isJoker: function () {
          return true;
        },
        isLock: function () {
          return true;
        }
      },
      {
        get: function () {
          return 'f1_11';
        },
        set: function () {},
        getCoordinates: function () {
          return [2, 3, 5];
        },
        isJoker: function () {},
        isLock: function () {}
      },
      {
        get: function () {
          return 'f1_11';
        },
        set: function () {},
        getCoordinates: function () {
          return [1, 2, 3];
        },
        isJoker: function () {
          return true;
        },
        isLock: function () {
          return true;
        }
      }
    ];

    it('should not call changeFace', function (done) {
      var cache = puzzle.eachCellByLayer,
        change_face_cache = puzzle.changeFace,
        tile_free_cache = puzzle.isFreeTile,
        joker_tile = {
          get: function () {
            return 'f2_10';
          },
          isJoker: function () {
            return false;
          },
          getCoordinates: function () {
            return [1, 2, 3];
          },
          isLock: function () {
            return false;
          }
        },
        joker_tile_two = {
          get: function () {
            return 'f2_12';
          },
          isJoker: function () {
            return false;
          },
          getCoordinates: function () {
            return [1, 2, 3];
          },
          isLock: function () {
            return false;
          }
        },
        matched_tile = {
          get: function () {
            return 'f1_11';
          },
          isJoker: function () {
            return true;
          },
          getCoordinates: function () {
            return [1, 2, 5];
          },
          isLock: function () {
            return true;
          }
        };

      puzzle.isFreeTile = function () {
        return false;
      };
      puzzle.set('tile_per_match', 2);
      puzzle.eachCellByLayer = function (cb) {
        _.find(tile_array, function (model) {
          cb(model);
        });
      };

      puzzle.changeFace = function () {
        done('error');
      };
      done();
      puzzle.updateJokerPair([joker_tile, matched_tile, joker_tile_two]);
      puzzle.eachCellByLayer = cache;
      puzzle.changeFace = change_face_cache;
      puzzle.isFreeTile = tile_free_cache;
      puzzle.changeFace = change_face_cache;
    });

    it('should call changeFace', function () {
      var cache = puzzle.eachCellByLayer,
        change_face_cache = puzzle.changeFace,
        tile_free_cache = puzzle.isFreeTile,
        joker_tile = {
          get: function () {
            return 'f2_10';
          },
          isJoker: function () {
            return false;
          },
          getCoordinates: function () {
            return [1, 2, 3];
          },
          isLock: function () {
            return false;
          }
        },
        matched_tile = {
          get: function () {
            return 'f1_11';
          },
          isJoker: function () {
            return true;
          },
          getCoordinates: function () {
            return [1, 2, 5];
          },
          isLock: function () {
            return true;
          }
        };

      puzzle.isFreeTile = function () {
        return false;
      };
      puzzle.set('tile_per_match', 2);
      puzzle.eachCellByLayer = function (cb) {
        _.find(tile_array, function (model) {
          cb(model);
        });
      };

      puzzle.changeFace = function () {
      };
      puzzle.updateJokerPair([joker_tile, matched_tile, matched_tile]);
      puzzle.eachCellByLayer = cache;
      puzzle.changeFace = change_face_cache;
      puzzle.isFreeTile = tile_free_cache;
      puzzle.changeFace = change_face_cache;
    });
  });

  describe('getPairWithJoker', function () {
    it('should return []', function (done) {
      var cache = this.eachCell,
        tile_free_cache = puzzle.isFreeTile,
        tiles = [
          {},
          {}
        ];

      puzzle.isFreeTile = function () {
        return false;
      };

      assert.deepEqual([], puzzle.getPairWithJoker(tiles));
      done();
      puzzle.isFreeTile = tile_free_cache;
      this.eachCell = cache;
    });

    it('should return a pair one will be joker', function (done) {
      var pair,
        tile_free_cache = puzzle.isFreeTile,
        tiles = {
          f2_10: [{
            isJoker: function () {
              return false;
            }
          },
          {
            isJoker: function () {
              return false;
            }
          }],
          f_joker: [{
            isJoker: function () {
              return true;
            }
          }]
        };

      puzzle.isFreeTile = function () {
        return true;
      };
      puzzle.set('tile_per_match', 3);
      pair = puzzle.getPairWithJoker(tiles);
      puzzle.isFreeTile = tile_free_cache;
      assert.strictEqual(pair[0].isJoker(), true);
      assert.strictEqual(pair[1].isJoker(), false);
      done();
    });

    it('should return [] if no joker tiles', function (done) {
      var pair,
        tile_free_cache = puzzle.isFreeTile,
        tiles = {
          f2_10: [{
            isJoker: function () {
              return false;
            }
          }],
          f2_11: [{
            isJoker: function () {
              return false;
            }
          }]
        };

      puzzle.isFreeTile = function () {
        return false;
      };
      puzzle.set('tile_per_match', 3);
      pair = puzzle.getPairWithJoker(tiles);
      puzzle.isFreeTile = tile_free_cache;
      assert.deepEqual(pair, []);
      done();
    });

    it('should return two joker', function (done) {
      var pair,
        tile_free_cache = puzzle.isFreeTile,
        tiles = {
          f2_10: [{
            isJoker: function () {
              return false;
            }
          }],
          f2_11: [{
            isJoker: function () {
              return false;
            }
          }],
          f_joker: [{
            isJoker: function () {
              return true;
            }
          },
          {
            isJoker: function () {
              return true;
            }
          }]
        };

      puzzle.isFreeTile = function () {
        return true;
      };
      puzzle.set('tile_per_match', 3);
      pair = puzzle.getPairWithJoker(tiles);
      puzzle.isFreeTile = tile_free_cache;
      assert.strictEqual(pair[0].isJoker(), true);
      assert.strictEqual(pair[1].isJoker(), true);
      done();
    });
  });

  describe('setHintAsSelected()', function () {
    it('should set tile_selected', function (done) {
      puzzle.setHintAsSelected({});

      assert.deepEqual(puzzle.get('tile_selected'), [{}]);
      done();
    });
  });
  describe('getBestScore()', function () {
    it('get the best score', function () {
      var cache = puzzle_module.getUserBonusPuzzle;

      puzzle_module.getUserBonusPuzzle = function () {
        puzzle_module.getUserBonusPuzzle = cache;
        return {
          id: 'three_match',
          max_ms: 20,
          curr_ms: 20,
          20: {
            score: 200
          }
        };
      };
      assert.strictEqual(200, puzzle.getBestScore());
    });
    it('get return zero if no dat for the puzzle', function () {
      var cache = puzzle_module.getUserBonusPuzzle;

      puzzle_module.getUserBonusPuzzle = function () {
        puzzle_module.getUserBonusPuzzle = cache;
        return {
          id: 'three_match',
          max_ms: 20,
          curr_ms: 20
        };
      };
      assert.strictEqual(0, puzzle.getBestScore());
    });
  });
});
