/* global config, PuzzleModel, TileModel, ParentPuzzle,
   User, event_manager, style, Promise */

jsio('import util.underscore as _');
jsio('import test.lib.util as util_test');
jsio('import timer');
jsio('import quest.lib.bluebird as Promise');

describe('Bonus Model', function () {
  'use strict';

  var puzzle, tile,
    families = [
      {
        pairs: 1,
        types: 1
      },
      {
        pairs: 4,
        types: 3
      },
      {
        pairs: 1,
        types: 1
      },
      {
        pairs: 1,
        types: 1
      },
      {
        pairs: 3,
        types: 2
      },
      {
        pairs: 1,
        types: 1
      }
    ],
    puzzle_data = {
      total_tiles: 8,
      layout: [
        [2, 0, 0], [0, 2, 0], [2, 2, 0],
        [4, 2, 0], [2, 4, 0], [2, 2, 1],
        [8, 1, 0], [8, 5, 0]],
      id: 'test',
      families: families,
      time: 60,
      init: [],
      solved: []
    },
    cache = [],
    setup_common = function () {
      var board_config = config.board_data;

      puzzle = new PuzzleModel();
      tile = new TileModel();

      puzzle.set({
        id: puzzle_data.id,
        tile_count: puzzle_data.total_tiles,
        rows: board_config.rows,
        columns: board_config.columns,
        depth: board_config.depth,
        time: 0
      });
      cache = [puzzle.assignFaces, puzzle.setTiles];
      puzzle.assignFaces = function () {
        return Promise.resolve(true);
      };
      puzzle.setTiles = function () {};
    },
    init = function () {
      jsio('import src.models.tile as TileModel');
      jsio('import resources.data.config as config');
      jsio('import quest.models.user as User');
      jsio('import src.models.puzzles.bonus as PuzzleModel');
      jsio('import DevkitHelper.event_manager as event_manager');
      jsio('import DevkitHelper.style as style');
      jsio('import src.models.puzzle as ParentPuzzle');
      GC.app = {
        view: {
          updateOpts: function () {}
        },
        user: new User(),
        engine: {
          subscribe: function () {}
        }
      };

      GC.app.user.set({
        lives: 3,
        max_ms: 1,
        curr_ms: 1,
        infinite_lives_remaining: 0
      }, true);
      event_manager.register('', []);
      style.init({
        orientation: 'portrait',
        bound_width: 640,
        bound_height: 960,
        tablet: 0.8,
        phablet: 0.9
      });
    };

  before(init);
  beforeEach(setup_common);

  describe('initializeData', function () {
    it('should set the correct data and set free shuffle to 1' +
        ' for untimed levels', function () {
          var cache = ParentPuzzle.prototype.initializeData;

          puzzle_data.type = 'untimed';
          puzzle_data.total_tiles = 15;
          ParentPuzzle.prototype.initializeData = function (data) {
            assert.strictEqual(data.limit_type, 'untimed');
            assert.strictEqual(data.untimed, 15);
            ParentPuzzle.prototype.initializeData = cache;
          };
          puzzle.initializeData(puzzle_data);
          assert.strictEqual(puzzle.get('untimed'), 0);
        });
  });
});
