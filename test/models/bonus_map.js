var bluebird = require('bluebird'),
  assert = require('assert'),
  sinon = require('sinon'),
  fs = require('fs-extra-promise'),
  cache = require('quest/models/db/cache'),
  bonus_map = require('../../models/bonus_map');

describe('Bonus Map', function () {
  'use strict';

  var cache_get, cache_add, fs_read_file;

  before(function () {
    cache_get = sinon.stub(cache, 'getValue');
    cache_add = sinon.stub(cache, 'addValue');
    cache_add.returns(null);
    fs_read_file = sinon.stub(fs, 'readFileAsync');
  });

  after(function () {
    cache_get.restore();
    cache_add.restore();
    fs_read_file.restore();
  });

  describe('getCurrentData()', function () {
    it('should return cached data', function (done) {
      cache_get.returns(bluebird.resolve({
        min_ms: 1,
        android: [{
          start_time: Date.now() - 10000,
          end_time: Date.now() + 10000,
          id: 'abc'
        }]
      }));

      bonus_map.getCurrentData('android', 5)
        .then(function (resp) {
          done(resp.id === 'abc' ? undefined : 'error');
        });
    });

    it('shouldnt return data if curr version is less than 0.6.90' +
      ' for type', function (done) {
        cache_get.returns(bluebird.resolve({
          min_ms: 1,
          android: [{
            start_time: Date.now() - 10000,
            end_time: Date.now() + 10000,
            id: 'abc',
            type: 'golden'
          }]
        }));

        bonus_map.getCurrentData('android', 5, '0.6.82')
          .then(function (resp) {
            done(resp === null ? undefined : 'error');
          });
      }
    );

    it('should cache data and return for cache miss', function (done) {
      var tomoro = new Date(),
        yest = new Date();

      tomoro.setDate(tomoro.getDate() + 1);
      yest.setDate(yest.getDate() - 1);

      cache_get.returns(bluebird.reject('not_found'));
      fs_read_file.returns(bluebird.resolve(JSON.stringify({
        platform: {
          ios: [
            [
              'jkoi',
              '2000-10-10',
              '2000-10-11'
            ],
            [
              'abcd',
              yest.toISOString(),
              tomoro.toISOString()
            ]
          ]
        },
        min_ms: 10,
        data: {
          abcd: {
            type: 'golden',
            levels: [1, 2, 3],
            popup_bg: 'background_image_link'
          }
        }
      })));

      bonus_map.getCurrentData('ios', 15, '0.8')
        .then(function (resp) {
          assert.strictEqual('golden', resp.type);
          assert.deepEqual([1, 2, 3], resp.levels);
          assert.strictEqual('background_image_link', resp.popup_bg);
          done(resp.id === 'abcd' + yest.getTime() ? undefined : 'error');
        });
    });

    it('should return null if no bonus map present platform', function (done) {
      cache_get.returns(bluebird.resolve({
        min_ms: 1,
        android: [{
          start_time: Date.now() - 10000,
          end_time: Date.now() + 10000,
          id: 'abc'
        }]
      }));

      bonus_map.getCurrentData('ios', 5)
        .then(function (resp) {
          done(resp === null ? undefined : 'error');
        });
    });

    it('should return null if available bonus map min ms greater than user' +
      ' max_ms', function (done) {
        cache_get.returns(bluebird.resolve({
          min_ms: 10,
          android: [{
            start_time: Date.now() - 10000,
            end_time: Date.now() + 10000,
            id: 'abc'
          }]
        }));

        bonus_map.getCurrentData('android', 5)
          .then(function (resp) {
            done(resp === null ? undefined : 'error');
          });
      }
    );

    it('should return null for any error', function (done) {
      cache_get.returns(bluebird.reject('err'));

      bonus_map.getCurrentData('android', 5)
        .then(function (resp) {
          done(resp === null ? undefined : 'error');
        });
    });
  });

  describe('reload()', function () {
    it('should delete cache and invoke getCurrentData', function (done) {
      var count = 0,
        cache_del = sinon.stub(cache, 'delValue', function () {
          ++count;
          cache_del.restore();

          return bluebird.resolve(true);
        }),
        cache_get = bonus_map.getCurrentData;

      bonus_map.getCurrentData = function () {
        ++count;
        bonus_map.getCurrentData = cache_get;

        return bluebird.resolve(true);
      };

      bonus_map.reload()
        .then(function () {
          done(count === 2 ? undefined : 'error');
        });
    });
  });
});
