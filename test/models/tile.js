/* global Tile,util, Puzzle, util_test, style */

jsio('import test.lib.util as util_test');

describe('Tile Model', function () {
  'use strict';

  var tile, puzzle,
    portrait_data = {
      tile_size: {
        gradient: {
          x: 10,
          y: 10
        }
      },
      grid: {
        height: 740,
        width: 620,
        order: 2,
        layout: 'box',
        centerX: true
      }
    },
    init = function () {
      jsio('import quest.modules.util as util');

      GC.app = {
        view: {
          updateOpts: function () {}
        },
        engine: {
          subscribe: function () {}
        }
      };
      util_test.removeFromJSIOCache('devkithelper/src/style');
      jsio('import resources.styles.portrait as portrait');

      util_test.getFromJSIOCache('resources/styles/portrait').exports =
        portrait_data;

      jsio('import DevkitHelper.style as style');
      style.init({
        orientation: 'portrait',
        bound_width: 640,
        bound_height: 960,
        tablet: 0.9,
        phablet: 0.9
      });
      jsio('import src.models.puzzle as Puzzle');
    },
    setup_common = function () {
      jsio('import src.models.tile as Tile');
      tile = new Tile();
      puzzle = new Puzzle();

      tile.set('puzzle', puzzle);
    },
    destroy_common = function () {
      util_test.removeFromJSIOCache('src/models/tile.js');
    };

  before(init);
  beforeEach(setup_common);
  afterEach(destroy_common);

  describe('init()', function () {
    it('should set default properties', function () {
      var opts = {
        x: null,
        y: null,
        z: null,
        face: null,
        free: false,
        state: 0
      };

      tile.onObtain();

      assert.equal(opts.x, tile.get('x'));
      assert.equal(opts.y, tile.get('y'));
      assert.equal(opts.z, tile.get('z'));
      assert.equal(opts.face, tile.get('face'));
      assert.equal(opts.free, tile.get('free'));
      assert.equal(opts.state, tile.get('state'));
    });
  });

  describe('build()', function () {
    it('should set opts passed', function () {
      var opts = {
        x: 1,
        y: 1,
        z: 1
      };

      tile.build(opts);
      assert.equal(opts.x, tile.get('x'));
      assert.equal(opts.y, tile.get('y'));
      assert.equal(opts.z, tile.get('z'));
    });
  });

  describe('getCoordinates()', function () {
    it('return proper coordinates', function () {
      var coord = [2, 2, 0];

      tile.set({
        x: 2,
        y: 2,
        z: 0
      });
      assert.deepEqual(coord, tile.getCoordinates());
    });
  });

  describe('isFilled()', function () {
    it('should return false where face is not set', function () {
      tile.set({
        x: 2,
        y: 2,
        z: 0
      });
      assert.equal(false, tile.isFilled());
    });

    it('should return true where face is set', function () {
      tile.set({
        face: 10
      });
      assert.equal(true, tile.isFilled());
    });
  });

  describe('getPrevCordinates()', function () {
    it('should retun null array if no previous cordinates', function () {
      assert.deepEqual(tile.getPrevCordinates(), [null, null, null]);
    });

    it('should retun previous cordinates', function () {
      tile.onObtain();
      tile.set({
        x: 10,
        y: 12,
        z: 14
      });

      tile.set({
        x: 12,
        y: 12,
        z: 16
      });
      assert.deepEqual(tile.getPrevCordinates(), [10, 12, 14]);
    });
  });

  describe('getNeighbours()', function () {
    it('return all neighour coordinates', function () {
      var cache = util.getOffsetPosition,
        coord = {
          left: [[0, 1, 0], [0, 2, 0], [0, 3, 0]],
          right: [[4, 1, 0], [4, 2, 0], [4, 3, 0]],
          above: [[1, 1, 1], [2, 1, 1], [3, 1, 1],
                  [1, 2, 1], [2, 2, 1], [3, 2, 1],
                  [1, 3, 1], [2, 3, 1], [3, 3, 1]],
          below: [[1, 1, -1], [2, 1, -1], [3, 1, -1],
                  [1, 2, -1], [2, 2, -1], [3, 2, -1],
                  [1, 3, -1], [2, 3, -1], [3, 3, -1]]
        };

      tile.set({
        x: 2,
        y: 2,
        z: 0
      });
      util.getOffsetPosition = function (c, offset) {
        return [c[0] + offset[0], c[1] + offset[1],
          c[2] + offset[2]];
      };

      assert.deepEqual(coord, tile.getNeighbours());
      util.getOffsetPosition = cache;
    });

    it('return selective neighour coordinates', function () {
      var cache = util.getOffsetPosition,
        coord = {
          right: [[4, 1, 0], [4, 2, 0], [4, 3, 0]],
          below: [[1, 1, -1], [2, 1, -1], [3, 1, -1],
                  [1, 2, -1], [2, 2, -1], [3, 2, -1],
                  [1, 3, -1], [2, 3, -1], [3, 3, -1]]
        };

      tile.set({
        x: 2,
        y: 2,
        z: 0
      });
      util.getOffsetPosition = function (c, offset) {
        return [c[0] + offset[0], c[1] + offset[1],
          c[2] + offset[2]];
      };
      assert.deepEqual(coord, tile.getNeighbours(['left', 'above']));
      util.getOffsetPosition = cache;
    });
  });

  describe('match', function () {
    it('should return undefined if tile not passed', function () {
      tile.set('face', 1);
      assert.equal(undefined, tile.match());
    });

    it('should return true if face match', function () {
      var tile2 = new Tile();

      tile.set('face', 1);
      tile2.set('face', 1);

      assert.equal(true, tile.match(tile2));
    });

    it('should return false if matching joker', function () {
      var tile2 = new Tile();

      tile.set('face', 'f_joker');
      tile2.set('face', 'f_joker');

      assert.equal(false, tile.match(tile2));
    });
  });

  describe('isLock', function () {
    it('should return true', function (done) {
      tile.set('face', 'f_lock_1');
      done(tile.isLock() === true ? undefined : 'error');
    });

    it('should return false', function (done) {
      tile.set('face', 'f_a_1');
      done(tile.isLock() === false ? undefined : 'error');
    });

    it('should return false if no face', function (done) {
      tile.set('face', undefined);
      done(tile.isLock() === false ? undefined : 'error');
    });
  });

  describe('isKey', function () {
    it('should return true', function (done) {
      tile.set('tag', 'key_1');
      done(tile.isKey() === true ? undefined : 'error');
    });

    it('should return false', function (done) {
      tile.set('tag', 'jewel_1');
      done(tile.isKey() === false ? undefined : 'error');
    });

    it('should return false if no face', function (done) {
      tile.set('tag', undefined);
      done(tile.isKey() === false ? undefined : 'error');
    });
  });

  describe('getActualFace', function () {
    it('should return face', function (done) {
      tile.set('face', 'f_lock_1');
      done(tile.getActualFace() === 'f_lock_1' ? undefined : 'error');
    });

    it('should return real_face', function (done) {
      tile.set('face', 'f_golden');
      tile.set('real_face', 'f_lock_1');
      done(tile.getActualFace() === 'f_lock_1' ? undefined : 'error');
    });
  });

  describe('showHint', function () {
    it('should set state to 3', function () {
      tile.showHint();
      assert.equal(3, tile.get('state'));
    });

    it('should set state 1', function () {
      tile.showHint(false);
      assert.equal(1, tile.get('state'));
    });
  });

  describe('getOffset', function () {
    it('should return offset x', function () {
      var opts = {
        x: 1,
        y: 1,
        z: 1
      };

      tile.build(opts);

      assert.equal(-15, tile.getOffset('x'));
    });
    it('should return offset y', function () {
      var opts = {
        x: 1,
        y: 2,
        z: 1
      };

      tile.build(opts);

      assert.equal(-20, tile.getOffset('y'));
    });
  });

  describe('isFree', function () {
    it('should return true if tile is free', function () {
      puzzle.isFreeTile = function () {
        return true;
      };

      assert.equal(true, tile.isFree());
    });
  });

  describe('pick', function () {
    it('should invoke pickTile of puzzle model if ' +
      'tile is free and different', function (done) {
      puzzle.pickTile = function () {
        done();
      };

      tile.isFree = function () {
        return true;
      };
      tile.pick();
    });

    it('should not invoke pickTile of puzzle model if ' +
      'tile tap limit reached', function (done) {
      puzzle.pickTile = function () {
        done('error');
      };

      tile.isFree = function () {
        return true;
      };
      puzzle.set({
        limit_type: 'tap',
        limit: 10,
        tap: 10
      });
      tile.pick();
      done();
    });
  });

  describe('isVisible', function () {
    it('should return true', function (done) {
      var cache = tile.getCoordinates,
        get_cache = tile.get;

      tile.getCoordinates = function () {
        tile.getCoordinates = cache;
        return [1, 2, 3];
      };
      tile.get = function (item) {
        if (item === 'puzzle') {
          return {
            tileAt: function () {}
          };
        } else if (item === 'x') {
          return 1;
        } else {
          return 2;
        }
      };

      if (tile.isVisible()) {
        tile.get = get_cache;
        done();
      }
    });

    it('should return false', function (done) {
      var cache = tile.getCoordinates,
        get_cache = tile.get;

      tile.getCoordinates = function () {
        tile.getCoordinates = cache;
        return [1, 2, 3];
      };
      tile.get = function (item) {
        if (item === 'puzzle') {
          return {
            tileAt: function () {
              return true;
            }
          };
        } else if (item === 'x') {
          return 1;
        } else {
          return 2;
        }
      };

      if (!tile.isVisible()) {
        tile.get = get_cache;
        done();
      }
    });
  });

  describe('isVisibleCompletely', function () {
    it('should return true', function (done) {
      var cache = tile.getCoordinates,
        get_cache = tile.get;

      tile.getCoordinates = function () {
        tile.getCoordinates = cache;
        return [1, 2, 3];
      };

      tile.get = function () {
        tile.get = get_cache;
        return {
          getNeighbours: function () {
            return [
              [6, 8, 3]
            ];
          }
        };
      };

      if (!tile.isVisibleCompletely()) {
        tile.get = get_cache;
        done('error');
      }
      done();
    });

    it('should return true', function (done) {
      var cache = tile.getCoordinates,
        get_cache = tile.get;

      tile.getCoordinates = function () {
        tile.getCoordinates = cache;
        return [1, 2, 3];
      };

      tile.get = function () {
        tile.get = get_cache;
        return {
          getNeighbours: function () {
            return [
              [1, 2, 4]
            ];
          }
        };
      };

      if (tile.isVisibleCompletely()) {
        done('error');
      } else {
        done();
      }
    });
  });

  describe('isJoker', function () {
    it('should return true', function (done) {
      tile.set('face', 'f_joker');

      assert.strictEqual(true, tile.isJoker());
      done();
    });

    it('should invoke false', function (done) {
      tile.set('face', 'f2_16');

      assert.strictEqual(false, tile.isJoker());
      done();
    });

    it('should return true for memory match joker', function () {
      tile.set('face', 'memory_f_joker');
      tile.set('face_visibility', false);
      tile.set('hidden', true);

      puzzle.set({
        type: 'memory'
      });

      assert.strictEqual(true, tile.isJoker());
    });
  });

  describe('onRelease', function () {
    it('should emit clean', function (done) {
      tile.on('clean', function () {
        done();
      });
      tile.onRelease();
    });

    it('should invoke supr', function (done) {
      var cache = tile.removeAllListeners;

      tile.removeAllListeners = function () {
        tile.removeAllListeners = cache;
        done();
      };
      tile.onRelease();
    });
  });
});
