/* global _, config, PuzzleModel, TileModel, util_test, dh_timer, ModelPool,
 util, User, event_manager, style, Promise, loading, QuestPuzzle, sounds */

jsio('import util.underscore as _');
jsio('import test.lib.util as util_test');
jsio('import DevkitHelper.timer as dh_timer');
jsio('import DevkitHelper.modelpool as ModelPool');
jsio('import timer');
jsio('import quest.lib.bluebird as Promise');

describe('Puzzle Model', function () {
  'use strict';

  var puzzle, tile,
    config_data = {
      server: 'http://localhost:3000',
      board_data: {
        rows: 30,
        columns: 30,
        depth: 5
      },
      orientation: 'portrait',
      bound_width: 640,
      bound_height: 960,

      puzzle_attempt: 64,

      max_tiles: [9, 9, 9, 4, 4, 3, 3],
      swoosh_pairs: 5,
      flip_count: 3,
      jewels: ['circle', 'triangle', 'square'],
      key_prefix: 'key_',

      partial_memory_ratio: 0.5,
      powerups: {
        ingame: {
          shuffle: true,
          hint: true,
          swoosh: true
        },
        pregame: {
          open_slot: true,
          joker_tile: true
        }
      },
      auto_hint_interval: 5000,
      lives: {
        max: 5,
        gen: 1000
      },
      family_score: [100, 150, 200, 300, 250, 400, 350],
      puzzle_load: {
        retry: 64,
        info_timeout: 1,
        track: 5
      },
      jokers_per_level: 2,
      special_faces: {
        joker: 'f_joker',
        golden: 'f_golden',
        lock: ['f_lock_0', 'f_lock_1', 'f_lock_2']
      },
      variations: {
        memory: {
          id: 'memory',
          powerups_exclude: ['shuffle', 'hint', 'half_hint']
        },
        partial_memory: {
          id: 'partial_memory',
          powerups_exclude: []
        },
        jewels: {
          id: 'jewels',
          powerups_exclude: []
        },
        golden: {
          id: 'golden'
        }
      },
      max_type: 5,
      bonus_level_def: [[20, 40], [13, 15], [7, 30]],
      lock_prefix: 'f_lock'
    },
    timers = ['puzzle', 'auto_hint'],
    families = [
      {
        pairs: 1,
        types: 1
      },
      {
        pairs: 4,
        types: 3
      },
      {
        pairs: 1,
        types: 1
      },
      {
        pairs: 1,
        types: 1
      },
      {
        pairs: 3,
        types: 2
      },
      {
        pairs: 1,
        types: 1
      }
    ],
    tile_faces = ['f0_8', 'f0_8', 'f2_8', 'f3_3',
      'f4_3', 'f5_2', 'f6_2'],
    puzzle_data = {
      total_tiles: 8,
      layout: [
        [2, 0, 0], [0, 2, 0], [2, 2, 0],
        [4, 2, 0], [2, 4, 0], [2, 2, 1],
        [8, 1, 0], [8, 5, 0]],
      id: 'test',
      families: families,
      time: 60,
      init: [],
      solved: []
    },
    puzzle_neighbour = {
      total_tiles: 8,
      layout: [
        [0, 3, 0], [2, 2, 0], [6, 2, 0], [8, 3, 0],
        [0, 8, 0], [2, 8, 0], [6, 8, 0], [8, 8, 0]],
      id: 'jcv',
      families: families,
      init: []
    },
    puzzle_neighbour_autofix = {
      total_tiles: 10,
      layout: [
        [0, 2, 0], [2, 2, 0], [1, 2, 0],
        [0, 8, 0], [2, 8, 0], [2, 9, 0],
        [10, 10, 0], [12, 10, 0], [14, 11, 0],
        [6, 2, 0]],
      id: 'jcv',
      families: families,
      init: []
    },
    product_data = {
      time: {
        quantity: 10,
        cost: 5
      }
    },
    cache = [],
    setup_common = function () {
      var board_config = config.board_data;

      puzzle = new PuzzleModel();
      tile = new TileModel();

      puzzle.set({
        id: puzzle_data.id,
        tile_count: puzzle_data.total_tiles,
        rows: board_config.rows,
        columns: board_config.columns,
        depth: board_config.depth,
        time: 0
      });
      cache = [puzzle.assignFaces, puzzle.setTiles];
      puzzle.assignFaces = function () {
        return Promise.resolve(true);
      };
      puzzle.setTiles = function () {};
    },
    destroy_common = function () {
      dh_timer.unregister(timers);
      dh_timer.mock();
      puzzle.stopTimers();
      puzzle.assignFaces = cache[0];
      puzzle.setTiles = cache[1];
      cache = [];
    },
    init = function () {
      jsio('import resources.data.config as config');
      util_test.removeFromJSIOCache('src/modules/util');
      util_test.getFromJSIOCache('resources/data/config').exports = config_data;

      jsio('import quest.modules.util as util');
      util.getProductData = function () {
        return product_data;
      };

      jsio('import quest.models.user as User');
      jsio('import quest.models.puzzle as QuestPuzzle');
      jsio('import src.models.puzzle as PuzzleModel');
      jsio('import src.models.tile as TileModel');
      jsio('import DevkitHelper.event_manager as event_manager');
      jsio('import DevkitHelper.style as style');
      jsio('import DevkitHelper.loading as loading');
      util_test.removeFromJSIOCache('quest/modules/sounds.js');
      jsio('import quest.modules.sounds as sounds');

      GC.app = {
        view: {
          updateOpts: function () {}
        },
        user: new User(),
        engine: {
          subscribe: function () {}
        }
      };

      GC.app.user.set({
        lives: 3,
        max_ms: 1,
        curr_ms: 1,
        infinite_lives_remaining: 0
      }, true);
      event_manager.register('', []);
      style.init({
        orientation: 'portrait',
        bound_width: 640,
        bound_height: 960,
        tablet: 0.8,
        phablet: 0.9
      });
    },
    loadPuzzle_setup = function () {
      cache = [puzzle.loadTiles, puzzle.assignFaces,
        puzzle.setTiles];
      puzzle.loadTiles = function () {};
      puzzle.assignFaces = function () {
        return Promise.resolve(true);
      };
      puzzle.setTiles = function () {};
      puzzle.set('tile_per_match', 2);
    },
    loadPuzzle_destroy = function () {
      puzzle.loadTiles = cache[0];
      puzzle.assignFaces = cache[1];
      puzzle.setTiles = cache[2];
      cache = [];
    };

  before(init);
  beforeEach(setup_common);
  afterEach(destroy_common);

  describe('build()', function () {
    it('should add listener shuffle', function (done) {
      puzzle.shuffle = done;

      puzzle.build();
      puzzle.emit('shuffle');
    });

    it('should add listener hint', function (done) {
      puzzle.hint = done;
      puzzle.build();

      puzzle.emit('hint');
    });

    it('should add listener magnet', function (done) {
      puzzle.magnet = done;
      puzzle.build();

      puzzle.emit('magnet');
    });

    it('should add listener undo', function (done) {
      puzzle.undo = done;
      puzzle.build();

      puzzle.emit('undo');
    });

    it('should invoke supr build', function (done) {
      puzzle.limitBought = done;

      puzzle.build();

      puzzle.emit('limit:bought');
    });

    it('should add listener for change in tile_selected', function (done) {
      puzzle.highlight = function () {
        done();
      };
      puzzle.build();

      puzzle.set('tile_selected', 'test');
    });

    it('should add listener for change in tile_selected when Maxms = 1',
      function (done) {
      puzzle.highlight = function () {
        return;
      };
      puzzle.first_tap = function () {
        done();
      };
      GC.app.user.set('max_ms', 1);
      puzzle.build();
      puzzle.set('tile_selected', 'test');
    });

    it('should add listener for tiles_matched when Maxms = 1',
      function (done) {
      puzzle.first_pair_match = function () {
        done();
      };
      GC.app.user.set('max_ms', 1);
      puzzle.build();
      puzzle.emit('tiles-matched');
    });
  });
  describe('getTileModel()', function () {
    it('should create new tilemodel if not present', function (done) {
      var tile;

      if (!tile) {
        puzzle.build();
        tile = puzzle._getTileModel();
        if (tile instanceof ModelPool) {
          done();
        }
      }
    });
    it('should return same tilemodel if already there', function (done) {
      var tile_another,
        cache;

      puzzle.build();
      cache = puzzle._tiles;
      puzzle._setTiles(true);
      tile_another = puzzle._getTileModel();
      if (!(tile_another instanceof ModelPool)) {
        puzzle._setTiles(cache);
        done();
      }
    });
  });

  describe('loadPuzzle()', function () {
    beforeEach(loadPuzzle_setup);
    afterEach(loadPuzzle_destroy);

    it('should set default board config', function (done) {
      puzzle.loadPuzzle(puzzle_data)
        .then(function () {
          assert.equal(0, puzzle.get('rows'));
          assert.equal(0, puzzle.get('columns'));
          assert.equal(0, puzzle.get('depth'));
          done();
        });
    });

    it('should enable pregame powerup accoring to input', function (done) {
      puzzle_data.open_slot = true;
      puzzle_data.joker_tile = false;

      puzzle.loadPuzzle(puzzle_data)
        .then(function () {
          assert.equal(true, puzzle.get('open_slot'));
          assert.equal(false, puzzle.get('joker_tile'));
          done();
        });
    });

    it('should set bonus_map property', function (done) {
      puzzle_data.bonus_map = true;

      puzzle.loadPuzzle(puzzle_data)
        .then(function () {
          assert.equal(true, puzzle.get('bonus_map'));
          done();
        });
    });

    it('should set golden_tiles_given accoring to input', function (done) {
      puzzle_data.goal = {
        golden: 4
      };

      puzzle.loadPuzzle(puzzle_data)
        .then(function () {
          assert.equal(4, puzzle.get('given_golden'));
          done();
        });
    });

    it('should have id set', function (done) {
      puzzle.loadPuzzle(puzzle_data)
        .then(function () {
          assert.equal('test', puzzle.get('id'));
          done();
        });
    });

    it('should have time limit set', function (done) {
      puzzle_data.limit_type = 'time';
      puzzle.loadPuzzle(puzzle_data)
        .then(function () {
          delete puzzle_data.limit_type;
          assert.strictEqual('time', puzzle.get('limit_type'));
          assert.equal('60', puzzle.get('limit'));
          done();
        });
    });

    it('should have tap limit set', function (done) {
      puzzle_data.limit_type = 'tap';
      puzzle_data.tap = 100;
      puzzle.loadPuzzle(puzzle_data)
        .then(function () {
          delete puzzle_data.limit_type;
          assert.strictEqual('tap', puzzle.get('limit_type'));
          assert.equal('100', puzzle.get('limit'));
          done();
        });
    });

    it('should start the auto half hint if selected' +
      ' in play popup', function (done) {
      var cache = puzzle.autoHalfHint;

      puzzle.set('auto_hint', true);
      puzzle.autoHalfHint = function () {
        puzzle.autoHalfHint = cache;
        done();
      };
      puzzle.loadPuzzle(puzzle_data);
    });

    it('should have added load details', function (done) {
      var result_data = {
          ms: 0,
          id: 'test',
          type: 'load',
          success: true,
          attempts: 0,
          tiles_placing: 0
        },
        user = GC.app.user;

      user.set('puzzle_loads', []);
      puzzle.loadPuzzle(puzzle_data)
        .then(function () {
          assert.deepEqual(_.omit(user.get('puzzle_loads')[0], 'time'),
            result_data);
          done();
        });
    });

    it('should have tile count of puzzle', function (done) {
      var tile_count = puzzle_data.total_tiles;

      puzzle.loadPuzzle(puzzle_data)
        .then(function () {
          assert.equal(tile_count, puzzle.get('tile_count'));
          done();
        });
    });

    it('should have tiles set', function (done) {
      puzzle.loadPuzzle(puzzle_data)
        .then(function () {
          assert.deepEqual([[]], puzzle.get('tiles'));
          done();
        });
    });

    it('should have empty last_match', function (done) {
      puzzle.loadPuzzle(puzzle_data)
        .then(function () {
          assert.deepEqual([], puzzle.get('last_match'));
          done();
        });
    });

    it('should have pending_matches 0', function (done) {
      puzzle.loadPuzzle(puzzle_data)
        .then(function () {
          assert.strictEqual(0, puzzle.get('pending_matches'));
          done();
        });
    });

    it('should emit puzzle-loaded', function (done) {
      puzzle.on('puzzle-loaded', function () {
        done();
      });

      puzzle.loadPuzzle(puzzle_data);
    });
    it('should decrement inventory_lives', function (done) {
      var user = GC.app.user;

      user.set('inventory_lives', 4);
      puzzle.loadPuzzle(puzzle_data)
        .then(function () {
          assert.strictEqual(3, user.get('inventory_lives'));
          done();
        });
    });
    it('should invoke initFaces if init value specified', function (done) {
      var cache = puzzle.initFaces;

      puzzle.initFaces = function (val) {
        puzzle.initFaces = cache;
        assert.deepEqual(val, [1, 2, 3]);
        done();
      };

      puzzle.loadPuzzle(_.extend({}, puzzle_data, {
        init: [1, 2, 3]
      }));
    });

    it('should emit load failed', function (done) {
        var cache = puzzle.assignFaces,
          emit_cache = puzzle.emit,
          cache_hide = loading.hide,
          load_failed_called = false;

        loading.hide = function () {
          loading.hide = cache_hide;
        };
        puzzle.assignFaces = function () {
          puzzle.assignFaces = cache;

          return Promise.resolve(false);
        };

        puzzle.emit = function (action) {
          if (action === 'load-failed') {
            load_failed_called = true;
          }
        };

        puzzle.loadPuzzle(_.extend({}, puzzle_data))
          .catch(function () {
            puzzle.emit = emit_cache;
            assert.strictEqual(load_failed_called, true);
            done();
          });
      });

    it('should not decrease life', function () {
      var user = GC.app.user,
        cache = user.decrement;

      user.decrement = function (key) {
        if (key === 'infinite_lives_remaining') {
          assert.strictEqual(true, false);
        }
      };

      GC.app.user.set('infinite_lives_remaining', 10000);

      puzzle.loadPuzzle(_.extend({}, puzzle_data))
        .catch(function () {
        });
      user.decrement = cache;
    });
  });

  describe('getRandomNo', function () {
    it('should not break even if exclude not given', function (done) {
      var random_no = puzzle.getRandomNo(10);

      if (random_no <= 10 && random_no >= 0) {
        done();
      }
    });
  });

  describe('eachCell()', function () {
    it('should iterate through each cell', function () {
      var count = 8,
        ctr = 0;

      puzzle.loadPuzzle(puzzle_data);
      puzzle.eachCell(function () {
        ++ctr;
      });

      assert.equal(count, ctr);
    });

    it('should not iterate over empty cell', function (done) {
      puzzle.set({
        tiles: [[[null, null]]]
      });

      puzzle.eachCell(done);
      done();
    });
  });

  describe('getTile()', function () {
    it('should return reference of tile', function () {
      var position = [0, 2, 0],
        model, obj;

      puzzle.loadPuzzle(puzzle_data);

      model = puzzle.getTile(position);
      obj = model.getCoordinates();

      assert.deepEqual([0, 2, 0], obj);
    });

    it('should not return reference of tile', function () {
      var position = [10, 30, 0],
        model;

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);

      assert.equal(null, model);
    });
  });

  describe('placeTile()', function () {
    it('should set face of tile', function () {
      var position = [0, 2, 0],
        model;

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);

      // Reset Tile Face
      model.set('face', 10);
      puzzle.placeTile(model, 5);

      assert.equal(5, model.get('face'));
    });

    it('should set free to false for tile', function () {
      var position = [0, 2, 0],
        model;

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);

      // Reset Tile Face
      model.set('free', true);
      puzzle.placeTile(model, 5);

      assert.equal(false, model.get('free'));
    });

    it('should set invoke updateProperty when neighbour', function (done) {
      var position = [0, 2, 0],
        cache = puzzle.updateProperty;

      puzzle.updateProperty = function () {
        done();
      };

      puzzle.loadPuzzle(puzzle_data);

      puzzle.placeTile(puzzle.getTile(position), 5);

      puzzle.updateProperty = cache;
    });

    it('should set not invoke updateProperty for tile ' +
      'with no neighbour', function (done) {
      var position = [8, 5, 0],
        cache = puzzle.updateProperty;

      puzzle.updateProperty = function () {
        done('error');
      };
      puzzle.loadPuzzle(puzzle_data);

      puzzle.placeTile(puzzle.getTile(position), 5);
      puzzle.updateProperty = cache;
      done();
    });
  });

  describe('loadTiles()', function () {
    beforeEach(function () {
      puzzle.set({
        tiles: [[]],
        rows: 0,
        columns: 0,
        depth: 0
      });
    });

    it('should map all coordinates to tile map', function () {
      var
        tiles, tile_model,
        coord = [],
        pass = true,
        tile_map;

      puzzle.loadTiles(puzzle_data.layout);

      tiles = puzzle.get('tiles');
      tile_map = puzzle_data.layout;

      _.each(tiles, function (xy_, z) {
        if (!_.isUndefined(xy_)) {
          _.each(xy_, function (y_, x) {
            if (!_.isUndefined(y_)) {
              tile_model = _.values(y_)[0];
              coord = [x, parseInt(_.keys(y_)[0]), z];
              if (_.findIndex(tile_map, coord) < 0) {
                pass = false;
              }
            }
          });
        }
      });

      assert.equal(true, pass);
    });

    it('should not invoke listener while setting', function () {
      puzzle.set = function (data, bool) {
        assert.equal(true, bool);
      };
      puzzle.loadTiles(puzzle_data.layout);
    });

    it('should set board rows, columns and depth', function () {
      puzzle.loadTiles(puzzle_data.layout);

      assert.equal(7, puzzle.get('rows'));
      assert.equal(10, puzzle.get('columns'));
      assert.equal(2, puzzle.get('depth'));
    });

    it('should update rows only if rows increase', function () {
      puzzle.loadTiles(puzzle_data.layout);
      assert.equal(7, puzzle.get('rows'));

      puzzle.loadTiles([[10, 6, 10]]);
      assert.strictEqual(7, puzzle.get('rows'));
    });

    it('should update rows only if rows increase', function () {
      puzzle.loadTiles(puzzle_data.layout);
      assert.equal(7, puzzle.get('rows'));

      puzzle.loadTiles([[10, 6, 10]]);
      assert.strictEqual(7, puzzle.get('rows'));
    });

    it('should update rows only if columns increase', function () {
      puzzle.loadTiles(puzzle_data.layout);
      assert.equal(10, puzzle.get('columns'));

      puzzle.loadTiles([[6, 6, 10]]);
      assert.strictEqual(10, puzzle.get('columns'));
    });

    it('should update rows only if depth increase', function () {
      puzzle.loadTiles(puzzle_data.layout);
      assert.equal(2, puzzle.get('depth'));

      puzzle.loadTiles([[6, 6, 0]]);
      assert.strictEqual(2, puzzle.get('depth'));
    });
  });

  describe('tileAt()', function () {
    it('should return position of tile present at (x,y,z)', function () {
      var tile_present = [2, 2, 0];

      puzzle.loadPuzzle(puzzle_data);

      assert.deepEqual([2, 2, 0], puzzle.tileAt(tile_present));
    });

    it('should return if tile present at (x,y,z) given offset', function () {
      var tile_present = [2, 0, 0],
        offset = [0, 2, 0];

      puzzle.loadPuzzle(puzzle_data);

      assert.deepEqual([2, 2, 0], puzzle.tileAt(tile_present, offset));
    });

    it('should return if tile absent at (x,y,z) given offset', function () {
      var tile_absent = [2, 4, 0],
        offset = [-2, 0, 0];

      puzzle.loadPuzzle(puzzle_data);

      assert.equal(null, puzzle.tileAt(tile_absent, offset));
    });

    it('should return false if tile present at (x,y,z) ' +
      ' given shifted position', function () {
      var tile_present = [7, 2, 0];

      puzzle.loadPuzzle(puzzle_data);

      assert.equal(null, puzzle.tileAt(tile_present));
    });

    it('should return false if position is negative', function () {
      var tile_absent = [2, 4, 0],
        offset = [-3, 0, 0];

      puzzle.loadPuzzle(puzzle_data);

      assert.equal(null, puzzle.tileAt(tile_absent, offset));
    });
  });

  describe('getNeighbours()', function () {
    it('should identify neighbour of tile', function () {
      var position = [2, 2, 0],
        obj = {left: [null, [0, 2, 0], null],
              right: [null, [4, 2, 0], null],
              above: [null, null, null,
                      null, [2, 2, 1], null,
                      null, null, null],
              below: [null, null, null,
                      null, null, null,
                      null, null, null]};

      puzzle.loadPuzzle(puzzle_data);

      assert.deepEqual(obj, puzzle.getNeighbours(position));
    });

    it('should not have neightbour of tile', function () {
      var position = [8, 5, 0],
        obj = {left: [null, null, null],
              right: [null, null, null],
              above: [null, null, null,
                      null, null, null,
                      null, null, null],
              below: [null, null, null,
                      null, null, null,
                      null, null, null]};

      puzzle.loadPuzzle(puzzle_data);

      assert.deepEqual(obj, puzzle.getNeighbours(position));
    });

    it('should not fix overlap neighbour on same layer for left', function () {
      var position = [2, 2, 0],
        obj = {left: [null, [0, 2, 0], null],
        right: [null, [4, 2, 0], null],
        above: [null, null, null,
                null, [2, 2, 1], null,
                null, null, null],
        below: [null, null, null,
                null, null, null,
                null, null, null]};

      puzzle.loadPuzzle(puzzle_neighbour_autofix);

      assert.notEqual(obj, puzzle.getNeighbours(position));
    });

    it('should return selective neighbour of a tile', function () {
      var position = [2, 2, 0],
        obj = {right: [null, [4, 2, 0], null],
          below: [null, null, null,
                null, null, null,
                null, null, null]};

      puzzle.loadPuzzle(puzzle_data);

      assert.deepEqual(obj, puzzle.getNeighbours(position, ['left', 'above']));
    });

    it('should not fix overlap neighbour on same layer for right', function () {
      var position = [0, 8, 0],
        obj = {left: [null, null, null],
              right: [null, [2, 8, 0], null],
              above: [null, null, null,
                      null, null, null,
                      null, null, null],
              below: [null, null, null,
                      null, null, null,
                      null, null, null]};

      puzzle.loadPuzzle(puzzle_neighbour_autofix);

      assert.notEqual(obj, puzzle.getNeighbours(position));
    });

    it('should return neighbour of tile in single array', function () {
      var position = [2, 2, 0],
        obj = [[0, 2, 0], [4, 2, 0], [2, 2, 1]];

      puzzle.loadPuzzle(puzzle_data);

      assert.deepEqual(obj, puzzle.getNeighbours(position, null, true));
    });

    it('should return selective neighbour of ' +
      'tile in single array', function () {
      var position = [2, 2, 0],
        obj = [[0, 2, 0], [2, 2, 1]];

      puzzle.loadPuzzle(puzzle_data);

      assert.deepEqual(obj, puzzle.getNeighbours(position,
        ['right'], true));
    });
  });

  describe('allocateTiles()', function () {
    it('should set sequential faces to tiles', function () {
      var cache = _.random,
        families = [
          {
            pairs: 2,
            types: 1
          },
          null,
          {
            pairs: 1,
            types: 1
          },
          {
            pairs: 1,
            types: 1
          },
          {
            pairs: 1,
            types: 1
          },
          {
            pairs: 1,
            types: 1
          },
          {
            pairs: 1,
            types: 1
          }
        ],
        tile_pair_exp = [
                        'f0_8',
                        'f0_8',
                        'f2_8',
                        'f3_3',
                        'f4_3',
                        'f5_2',
                        'f6_2'],
        tile_pair_ret = [],
        cache_families = puzzle_data.families;

      _.random = function () {
        return 0;
      };
      puzzle_data.families = families;
      puzzle.loadPuzzle(puzzle_data);
      tile_pair_ret = puzzle.allocateTiles();
      puzzle_data.families = cache_families;

      assert.deepEqual(tile_pair_exp, tile_pair_ret);
      _.random = cache;
    });

    it('should set use alternate method for tile allocation ' +
      ' if type is null', function () {
      var cache = _.random,
        families = [
          {
            pairs: 2,
            types: null
          },
          null,
          {
            pairs: 1,
            types: null
          },
          {
            pairs: 1,
            types: null
          },
          {
            pairs: 1,
            types: null
          },
          {
            pairs: 1,
            types: null
          },
          {
            pairs: 1,
            types: null
          }
        ],
        tile_pair_exp = [
                        'f0_8',
                        'f0_8',
                        'f2_8',
                        'f3_3',
                        'f4_3',
                        'f5_2',
                        'f6_2'],
        tile_pair_ret = [],
        cache_families = puzzle_data.families;

      _.random = function () {
        return 0;
      };
      puzzle_data.families = families;
      puzzle.loadPuzzle(puzzle_data);
      tile_pair_ret = puzzle.allocateTiles();
      puzzle_data.families = cache_families;
      _.random = cache;

      assert.deepEqual(tile_pair_exp, tile_pair_ret);
    });
  });

  describe('randomiseFaces()', function () {
    it('should randomize faces of tiles', function () {
      var tile_pair_org = puzzle.allocateTiles(families);

      assert.notEqual(tile_pair_org,
        puzzle.randomiseFaces(families));
      assert.equal(tile_pair_org.length * 2,
        puzzle.randomiseFaces(families).length);
    });

    it('should reduce family if memory', function () {
      var faces = [
                  'f0_8',
                  'f0_9',
                  'f2_1',
                  'f3_2',
                  'f4_3',
                  'f5_4',
                  'f0_8',
                  'f0_9',
                  'f2_1',
                  'f3_2',
                  'f4_3',
                  'f5_4'],
        cache = puzzle.allocateTiles;

      puzzle.allocateTiles = function () {
        puzzle.allocateTiles = cache;
        return faces;
      };

      puzzle.set('type', 'memory');

      assert.equal(config_data.max_type,
        _.keys(_.groupBy(puzzle.randomiseFaces(families))).length);

      puzzle.set('type', null);
    });
  });

  describe('assignFaces()', function () {
    it('should invoke puzzleGenerate', function (done) {
      var cache_gen = puzzle.attemptPuzzleGenerate;

      puzzle.attemptPuzzleGenerate = function () {
        puzzle.attemptPuzzleGenerate = cache_gen;
        done();
      };

      puzzle.assignFaces = cache[0];
      puzzle.assignFaces();
    });

    it('should resolve with tiles-places event', function (done) {
      var cache_gen = puzzle.attemptPuzzleGenerate;

      puzzle.attemptPuzzleGenerate = function () {
        puzzle.attemptPuzzleGenerate = cache_gen;
      };

      puzzle.assignFaces = cache[0];
      puzzle.assignFaces()
        .then(function (res) {
          done(res === true ? undefined : 'error');
        });

      puzzle.emit('tiles-placed', true);
    });
  });

  describe('attemptPuzzleGenerate()', function () {
    it('should emit tiles-placed with false if all attemts are ' +
      'done', function (done) {
        puzzle.once('tiles-placed', function (val) {
          done(val === false ? undefined : 'error');
        });
        puzzle._setAttempt(64);
        puzzle.attemptPuzzleGenerate();
      }
    );

    it('should invoke generatesolvable game with randomised ' +
      'faces', function (done) {
        var cache_gen = puzzle.generateSolvableGameAsync,
          cache_ran = puzzle.randomiseFaces,
          cache_gen_solvable = puzzle.onGeneratedSolvable,
          count = 0;

        puzzle._setAttempt(1);
        puzzle.randomiseFaces = function () {
          puzzle.randomiseFaces = cache_ran;
          ++count;
          return 1;
        };
        puzzle.generateSolvableGameAsync = function (val) {
          assert.strictEqual(val, 1);
          puzzle.generateSolvableGameAsync = cache_gen;
          return Promise.resolve('gen');
        };

        puzzle.onGeneratedSolvable = function (val) {
          puzzle.onGeneratedSolvable = cache_gen_solvable;
          done(val === 'gen' ? undefined : 'error');
        };

        puzzle.attemptPuzzleGenerate();
      }
    );
  });

  describe('onGeneratedSolvable()', function () {
    it('should emit tiles-placed with true for success ' +
      'generation', function (done) {
        puzzle.once('tiles-placed', function (val) {
          done(val === true ? undefined : 'error');
        });
        puzzle.onGeneratedSolvable(true);
      }
    );

    it('should retry puzzle regenerate if failed', function (done) {
      var cache = puzzle.attemptPuzzleGenerate;

      puzzle.attemptPuzzleGenerate = function () {
        puzzle.attemptPuzzleGenerate = cache;
        done();
      };

      puzzle.onGeneratedSolvable(false);
    });
  });

  describe('initFaces()', function () {
    it('should assign faces as per init data', function () {
      var cache_gettile = puzzle.getTile,
        cache_placetile = puzzle.placeTile,
        init_pos = [
          [1, 1, 1, 'f0_3'],
          [2, 2, 2, 'f0_3'],
          [3, 3, 3, 'f1_5'],
          [4, 4, 4, 'f1_5']
        ],
        get_cnt = 0,
        place_cnt = 0,
        res = {};

      puzzle.placeTile = function (tile, face) {
        if (++place_cnt === init_pos.length) {
          puzzle.placeTile = cache_placetile;
        }

        if (!res[face]) {
          res[face] = [];
        }
        res[face].push(tile);
      };

      puzzle.getTile = function (cord) {
        if (++get_cnt === init_pos.length) {
          puzzle.getTile = cache_gettile;
        }

        return cord[0] + cord[1] + cord[2];
      };

      puzzle.assignFaces = cache[0];
      puzzle.initFaces(init_pos);
      assert.deepEqual({
          f0_3: [3, 6],
          f1_5: [9, 12]
        }, res);
    });
  });

  describe('generateSolvableGameAsync()', function () {
    it('should invoke generateSolvableGame asynchronously', function (done) {
      var cache = puzzle.generateSolvableGame;

      puzzle.generateSolvableGame = function () {
        puzzle.generateSolvableGame = cache;
        return 100;
      };

      puzzle.generateSolvableGameAsync()
        .then(function (val) {
          done(val === 100 ? undefined : 'error');
        });
    });
  });

  describe('isJewelNotInOrder', function () {
    it('should return value if jewels not in order', function () {
      puzzle.set('jewels', ['c', 't', 's']);
      puzzle.set('jewel_order', ['t', 'c', 's']);
      assert.strictEqual(!puzzle.isJewelNotInOrder(), false);
    });

    it('should return value if jewels not in order', function () {
      puzzle.set('jewels', ['c', 't', 's']);
      puzzle.set('jewel_order', ['c', 't', 's']);
      assert.strictEqual(puzzle.isJewelNotInOrder(), undefined);
    });
  });

  describe('matchJewel', function () {
    it('should update jewel_order if tile matched has tag', function () {
      var tile_a = {
          get: function () {
            return 'circle';
          }
        },
        tile_b = {
          get: function () {
            return 'triangle';
          }
        };

      puzzle.set({
        jewel_order: [],
        jewels: ['circle', 'triangle']
      });
      puzzle.matchJewel(tile_b, tile_a);
      assert.deepEqual(puzzle.get('jewel_order'), ['circle', 'triangle']);

      puzzle.set({
        jewel_order: [],
        jewels: ['circle', 'triangle']
      });

      puzzle.matchJewel(tile_b, {
        get: function () {}
      });
      assert.deepEqual(puzzle.get('jewel_order'), ['triangle']);
    });
  });

  describe('generateSolvableGame()', function () {
    it('should set partially visible for ratio of' +
      'partial memory variation', function () {
      var count = 0,
        cache_place = puzzle.placeTile,
        cache_select = puzzle.selectCoordinates;

      puzzle.flattenTiles = function () {
        return;
      };
      puzzle.isBoardSolvable = function () {
        return true;
      };

      puzzle.loadPuzzle(puzzle_data);
      puzzle.onlyFreeInLine = function () {
        return false;
      };

      puzzle.placeTile = function (param1, param2, param3,
          param4, param5) {
        if (param4 === false && param5 === true) {
          count++;
        }
      };
      puzzle.selectCoordinates = function () {
        return [0, 2, 0];
      };

      puzzle.set('type', 'partial_memory');
      puzzle.generateSolvableGame(tile_faces);

      assert.strictEqual(4, count);
      puzzle.placeTile = cache_place;
      puzzle.selectCoordinates = cache_select;
    });

    it('should reset tile faces', function () {
      var model;

      puzzle.flattenTiles = function () {
        return;
      };

      puzzle.isBoardSolvable = function () {
        return true;
      };

      puzzle.loadPuzzle(puzzle_data);
      puzzle.onlyFreeInLine = function () {
        return false;
      };

      puzzle.generateSolvableGame();

      _.each(puzzle_data.layout, function (coord) {
        model = puzzle.get('tiles')[coord[2]][coord[0]][coord[1]];

        assert.equal(null, model.get('face'));
        assert.equal(false, model.get('free'));
      });
    });

    it('shouldnt reset tile face for open slot tile', function () {
      var model;

      puzzle.flattenTiles = function () {
        return;
      };
      puzzle.placeTile = function () {};
      puzzle.getTile = function () {};

      puzzle.loadPuzzle(puzzle_data);
      puzzle.onlyFreeInLine = function () {
        return false;
      };

      puzzle.generateSolvableGame();

      _.each(puzzle_data.layout, function (coord) {
        if (!model) {
          model = puzzle.get('tiles')[coord[2]][coord[0]][coord[1]];
        }
      });

      model.set('face', 'asa');
      puzzle.set('slot_tile', model);
      puzzle.generateSolvableGame([]);
      assert.strictEqual('asa', model.get('face'));
    });

    it('should assign jewels even if open slot tile has jewel', function () {
      var model;

      puzzle.getTile = function () {};

      puzzle.loadPuzzle(puzzle_data);
      puzzle.onlyFreeInLine = function () {
        return false;
      };

      _.each(puzzle_data.layout, function (coord) {
        if (!model) {
          model = puzzle.get('tiles')[coord[2]][coord[0]][coord[1]];
          model.set('tag', 'circle');
        }
      });

      puzzle.set('jewels', ['circle', 'triangle', 'square']);
      puzzle.set('slot_tile', model);
      puzzle.set('type', 'jewels');
      puzzle.generateSolvableGame();
      assert.strictEqual(_.isEmpty(_.difference(puzzle.get('jewels'),
        ['circle', 'square', 'triangle'])), true);
    });

    it('should assign the tag correctly', function () {
      puzzle.flattenTiles = function () {
        return;
      };
      puzzle.loadPuzzle(puzzle_data);
      puzzle.set('type', 'jewels');
      puzzle.generateSolvableGame();
      assert.strictEqual(_.isEmpty(_.difference(puzzle.get('jewels'),
        ['circle', 'square', 'triangle'])), true);
    });

    it('shouldnt reset tile face for open slot tile', function () {
      var model;

      puzzle.flattenTiles = function () {
        return;
      };
      puzzle.placeTile = function () { };
      puzzle.getTile = function () { };

      puzzle.loadPuzzle(puzzle_data);
      puzzle.onlyFreeInLine = function () {
        return false;
      };

      puzzle.generateSolvableGame();

      _.each(puzzle_data.layout, function (coord) {
        if (!model) {
          model = puzzle.get('tiles')[coord[2]][coord[0]][coord[1]];
        }
      });

      model.set('face', 'f_joker');
      model.set('real_face', 'asa');
      puzzle.set('slot_tile', model);
      puzzle.generateSolvableGame([]);
      assert.strictEqual('f_joker', model.get('face'));
    });

    it('should return false if no tiles present at bottom layer', function () {
      puzzle.loadPuzzle({layout: [
                              [2, 0, 1], [0, 2, 1], [2, 2, 1],
                              [4, 2, 1], [2, 4, 1], [2, 2, 2],
                              [8, 1, 1], [8, 5, 1]],
                        id: 'fail',
                        families: {},
                        init: []
                      });

      assert.equal(false, puzzle.generateSolvableGame(tile_faces));
    });

    it('should return false if position to place is null', function () {
      puzzle.selectCoordinates = function () {
        return null;
      };
      puzzle.loadPuzzle(puzzle_data);

      assert.equal(false, puzzle.generateSolvableGame(tile_faces));
    });

    it('should place the jewels if there', function () {
      var i = 0,
        cache_placetiles = puzzle.placeTile;

      puzzle_data.type = 'jewels';
      puzzle.placeTile = function (param1, param2, param3) {
        if (param3) {
          i++;
        }
      };
      puzzle.loadPuzzle(puzzle_data);
      puzzle.generateSolvableGame(tile_faces);
      assert.strictEqual(i, 3);
      puzzle_data.type = null;
      puzzle.placeTile = cache_placetiles;
    });

    it('should return false if position to place is' +
      ' null when calling second time', function () {
      var ctr = 0,
        cache = puzzle.selectCoordinates;

      puzzle.selectCoordinates = function (last_position) {
        if (++ctr % 2 !== 0) {
          return cache(last_position, puzzle.flattenTiles());
        } else {
          return null;
        }
      };
      puzzle.loadPuzzle(puzzle_data);

      assert.equal(false, puzzle.generateSolvableGame(tile_faces));
    });

    it('should switch to new position if it is higher', function () {
      var ctr = 0,
        cache = puzzle.selectCoordinates;

      puzzle.selectCoordinates = function (last_position) {
        ++ctr;
        if (ctr === 1) {
          return [8, 5, 0];
        } else if (ctr === 2) {
          return [2, 2, 1];
        } else {
          return cache(last_position);
        }
      };
      puzzle.loadPuzzle(puzzle_data);

      assert.notEqual(null, puzzle.generateSolvableGame(tile_faces));
    });

    it('should not switch to new position if not higher', function (done) {
      var ctr = 0,
        cache = puzzle.selectCoordinates,
        cache_placetiles = puzzle.placeTile;

      puzzle.selectCoordinates = function (last_position) {
        ++ctr;
        if (ctr === 1) {
          return [8, 5, 0];
        } else if (ctr === 2) {
          return [2, 2, 0];
        } else {
          return cache(last_position);
        }
      };
      puzzle.placeTile = function (tile) {
        puzzle.placeTile = cache_placetiles;
        assert.deepEqual(tile.getCoordinates(), [8, 5, 0]);
        done();
      };
      puzzle.loadPuzzle(puzzle_data);
      puzzle.generateSolvableGame(tile_faces);
    });

    it('should return true on generating solvable game', function () {
      puzzle.loadPuzzle({
        layout: [
          [0, 0, 0],
          [3, 0, 0],
          [6, 0, 0],
          [9, 0, 0],
          [0, 4, 0],
          [3, 4, 0],
          [6, 4, 0],
          [9, 4, 0]
        ],
        id: 'abhg',
        families: [
          {
            pairs: 4,
            types: null
          },
          {
            pairs: 4,
            types: null
          }
        ],
        init: []
      });

      assert.strictEqual(true,
        puzzle.generateSolvableGame(_.union(['f0_1'], tile_faces)));
    });

    it('should assign 2 golden tiles', function () {
      var i = 0;

      puzzle.loadPuzzle({
        layout: [
          [0, 0, 0],
          [3, 0, 0],
          [6, 0, 0],
          [9, 0, 0],
          [0, 4, 0],
          [3, 4, 0],
          [6, 4, 0],
          [9, 4, 0]
        ],
        id: 'abhg',
        families: [
          {
            pairs: 4,
            types: null
          },
          {
            pairs: 4,
            types: null
          }
        ],
        init: []
      });

      puzzle.set('collected_golden', 0);
      puzzle.set('given_golden', 1);
      puzzle.generateSolvableGame(tile_faces);
      puzzle.eachCell(function (model) {
        if (model.isGoldenTile()) {
          i++;
        }
      });
      assert.strictEqual(i, 2);
    });

    it('should assign 2 golden tiles even for' +
    'last position is null', function () {
      var i = 0,
        cache_select = puzzle.selectCoordinates,
        cache = puzzle.assignGoldenTile;

      puzzle.selectCoordinates = function () {
        puzzle.selectCoordinates = cache_select;
        return [9, 4, 0];
      };
      puzzle.loadPuzzle({
        layout: [
          [0, 0, 0],
          [3, 0, 0],
          [6, 0, 0],
          [9, 0, 0],
          [0, 4, 0],
          [3, 4, 0],
          [6, 4, 0],
          [9, 4, 0]
        ],
        id: 'abhg',
        families: [
          {
            pairs: 4,
            types: null
          },
          {
            pairs: 4,
            types: null
          }
        ],
        init: []
      });

      puzzle.assignGoldenTile = function () {
        puzzle.assignGoldenTile = cache;
        return {
          getCoordinates: function () {
            return [9, 4, 0];
          }
        };
      };

      puzzle.set('collected_golden', 0);
      puzzle.set('given_golden', 1);
      puzzle.generateSolvableGame(tile_faces);
      puzzle.eachCell(function (model) {
        if (model.isGoldenTile()) {
          i++;
        }
      });
      assert.strictEqual(i, 2);
    });

    it('should assign 2 golden tiles even for' +
      ' last position is not null', function () {
      var i = 0,
        cache_select = puzzle.selectCoordinates,
        cache = puzzle.assignGoldenTile;

      puzzle.selectCoordinates = function () {
        puzzle.selectCoordinates = cache_select;
        return [3, 0, 0];
      };
      puzzle.loadPuzzle({
        layout: [
          [0, 0, 0],
          [3, 0, 0],
          [6, 0, 0],
          [9, 0, 0],
          [0, 4, 0],
          [3, 4, 0],
          [6, 4, 0],
          [9, 4, 0]
        ],
        id: 'abhg',
        families: [
          {
            pairs: 4,
            types: null
          },
          {
            pairs: 4,
            types: null
          }
        ],
        init: []
      });

      puzzle.set('tile_per_match', 0);
      puzzle.assignGoldenTile = function () {
        puzzle.assignGoldenTile = cache;
        return {
          getCoordinates: function () {
            return [6, 0, 0];
          }
        };
      };

      puzzle.set('collected_golden', 0);
      puzzle.set('given_golden', 1);
      puzzle._last_position = null;
      puzzle.generateSolvableGame(tile_faces);
      puzzle.eachCell(function (model) {
        if (model.isGoldenTile()) {
          i++;
        }
      });
      assert.strictEqual(i, 2);
    });

    it('should place golden tile if tile ' +
      'count <=6', function () {
        var i = 0;

        puzzle.loadPuzzle({
          layout: [
            [0, 0, 0],
            [0, 0, 1],
            [0, 0, 2],
            [4, 4, 0]
          ],
          id: 'abhg',
          families: [
            {
              pairs: 1,
              types: null
            },
            {
              pairs: 1,
              types: null
            }
          ],
          init: []
        });

        puzzle.set('collected_golden', 0);
        puzzle.set('given_golden', 1);
        puzzle.generateSolvableGame([
          'f0_1', 'f0_1', 'f1_1', 'f1_1'
        ]);
        puzzle.eachCell(function (model) {
          if (model.isGoldenTile()) {
            i++;
          }
        });
        assert.strictEqual(i, 2);
      }
    );

    it('should place golden tile for open slot but dont' +
      'change coordinates', function () {
        var cache = puzzle.selectCoordinates;

        puzzle.loadPuzzle(puzzle_data);
        puzzle.selectCoordinates = function () {
          puzzle.selectCoordinates = cache;
          return [0, 2, 0];
        };
        puzzle.set('slot_tile', {
          get: function (param) {
            if (param === 'face') {
              return 'f_golden';
            }
          },
          getCoordinates: function () {
            return [0, 2, 0];
          },
          isLock: function () {
            return false;
          },
          isKey: function () {
            return false;
          }
        });
        puzzle.set('collected_golden', 0);
        puzzle.set('given_golden', 1);
        puzzle.generateSolvableGame(tile_faces);

        assert.deepEqual(puzzle.get('slot_tile').getCoordinates(), [0, 2, 0]);
        assert.strictEqual(puzzle.get('slot_tile').get('face'), 'f_golden');
      }
    );

    it('should place golden tile for open slot but dont' +
      'change coordinates for last position', function () {
        var cache = puzzle.selectCoordinates;

        puzzle.loadPuzzle(puzzle_data);
        puzzle.selectCoordinates = function () {
          return [2, 2, 0];
        };
        puzzle.set('slot_tile', {
          get: function (param) {
            if (param === 'face') {
              return 'f_golden';
            }
          },
          getCoordinates: function () {
            return [2, 2, 0];
          },
          isLock: function () {
            return false;
          },
          isKey: function () {
            return false;
          }
        });
        puzzle.set('tiles_per_match', 2);
        puzzle.set('collected_golden', 0);
        puzzle.set('given_golden', 1);
        puzzle.generateSolvableGame(tile_faces);

        assert.deepEqual(puzzle.get('slot_tile').getCoordinates(), [2, 2, 0]);
        assert.strictEqual(puzzle.get('slot_tile').get('face'), 'f_golden');
        puzzle.selectCoordinates = cache;
      }
    );

    it('should place golden tile for count <=6', function () {
        puzzle.loadPuzzle({
          layout: [
            [0, 0, 0],
            [0, 0, 1],
            [0, 0, 2],
            [4, 4, 0]
          ],
          id: 'abhg',
          families: [
            {
              pairs: 1,
              types: null
            },
            {
              pairs: 1,
              types: null
            }
          ],
          init: []
        });

        puzzle.set('slot_tile', {
          get: function (param) {
            if (param === 'face') {
              return 'f_1';
            }
          },
          getCoordinates: function () {
            return [2, 2, 0];
          },
          isLock: function () {
            return false;
          },
          isKey: function () {
            return false;
          },
          getActualFace: function () {
            return 'f_1';
          }
        });

        puzzle.set('golden_tiles_given', 1);
        puzzle.set('collected_golden', 0);

        puzzle.generateSolvableGame([
          'f0_1', 'f0_1', 'f1_1', 'f1_1'
        ]);
        assert.strictEqual(puzzle.getTile([0, 0, 2]).get('face'),
          puzzle.getTile([4, 4, 0]).get('face'));
      }
    );

    it('should ignore slot face even tiles < 6', function (done) {
        puzzle.loadPuzzle({
          layout: [
            [0, 0, 0],
            [0, 0, 1],
            [0, 0, 2],
            [4, 4, 0]
          ],
          id: 'abhg',
          families: [
            {
              pairs: 1,
              types: null
            },
            {
              pairs: 1,
              types: null
            }
          ],
          init: []
        });

        puzzle.set('slot_tile', {
          get: function (param) {
            if (param === 'face') {
              return 'f_golden';
            }
          },
          getCoordinates: function () {
            return [2, 2, 0];
          },
          isLock: function () {
            return false;
          },
          isKey: function () {
            return false;
          },
          set: function (key) {
            if (key === 'face') {
              done('error');
            }
          },
          getActualFace: function () {
            return 'f_1';
          }
        });

        puzzle.set('golden_tiles_given', 1);
        puzzle.set('collected_golden', 0);

        puzzle.generateSolvableGame([
          'f0_1', 'f0_1', 'f1_1', 'f1_1'
        ]);
        done();
      }
    );

    it('should convert tiles to joker if in joker mode', function (done) {
      var i = 0,
        j = 0,
        cache = puzzle.get,
        math_cache = Math.floor;

      Math.floor = function () {
        j++;
        if (j < 4) {
          return 2;
        } else {
          Math.floor = math_cache;
          return 3;
        }
      };

      puzzle.loadPuzzle({
        layout: [
          [0, 0, 0],
          [3, 0, 0],
          [6, 0, 0],
          [9, 0, 0],
          [0, 4, 0],
          [3, 4, 0],
          [6, 4, 0],
          [9, 4, 0]
        ],
        id: 'abhg',
        families: [
          {
            pairs: 4,
            types: 2
          },
          {
            pairs: 4,
            types: 2
          }
        ],
        init: []
      });

      puzzle.set('joker', true);
      puzzle.generateSolvableGame('f0_1', 'f0_1', 'f1_1', 'f1_1');
      puzzle.eachCell(function (model) {
        if (model.isJoker()) {
          i++;
        }
      });
      if (i > 0) {
        done();
      }
      puzzle.get = cache;
    });

    it('should joker count will be count of joker if' +
      'solved is > o', function (done) {
      var i = 0,
        cache = puzzle.get;

      puzzle.loadPuzzle({
        layout: [
          [0, 0, 0],
          [3, 0, 0],
          [6, 0, 0],
          [9, 0, 0],
          [0, 4, 0],
          [3, 4, 0],
          [6, 4, 0],
          [9, 4, 0]
        ],
        id: 'abhg',
        families: [
          {
            pairs: 4,
            types: 2
          },
          {
            pairs: 4,
            types: 2
          }
        ],
        init: []
      });

      puzzle.eachCell = function (cb) {
        var tiles = [
          {
            isJoker: function () {
              return true;
            },
            getCoordinates: function () {},
            set: function () {}
          },
          {
            isJoker: function () {
              return true;
            },
            getCoordinates: function () {},
            set: function () {}
          }
        ];

        _.find(tiles, function (model) {
          cb(model);
        });
      };

      puzzle.set('joker', true);
      puzzle.set('solved', 10);

      puzzle.generateSolvableGame('f0_1', 'f0_1', 'f1_1', 'f1_1');

      puzzle.eachCell(function (model) {
        if (model.isJoker()) {
          i++;
        }
      });
      if (i > 0) {
        done();
      }
      puzzle.get = cache;
    });

    it('should call placeLockAndKey', function (done) {
      var cache = puzzle.placeLockAndKey,
        cache2 = puzzle.getLockAndKeyIndexes,
        place_lock_key = false;

      puzzle.placeLockAndKey = function () {
        place_lock_key = true;
        return ['f_lock_1', 'tag'];
      };

      puzzle.getLockAndKeyIndexes = function () {
        return [1, 2, 3];
      };
      puzzle.loadPuzzle({
        layout: [
          [0, 0, 0],
          [0, 0, 1],
          [0, 0, 2],
          [4, 4, 0]
        ],
        id: 'abhg',
        families: [
          {
            pairs: 1,
            types: null
          },
          {
            pairs: 1,
            types: null
          }
        ],
        init: []
      });

      puzzle.set('lock_keys', [[0, 2, 4], [1, 1, 1], [2, 1, 2]]);
      puzzle.generateSolvableGame(tile_faces);
      puzzle.placeLockAndKey = cache;
      puzzle.getLockAndKeyIndexes = cache2;

      done(place_lock_key ? undefined : 'error');
    });

    it('should not set real face for non lock for ' +
      'lock and key puzzle', function (done) {
      var cache = puzzle.placeLockAndKey,
        cache2 = puzzle.getLockAndKeyIndexes,
        place_lock_key = false;

      puzzle.placeLockAndKey = function () {
        place_lock_key = true;
        return ['face', 'tag'];
      };

      puzzle.getLockAndKeyIndexes = function () {
        return [1, 2, 3];
      };
      puzzle.loadPuzzle({
        layout: [
          [0, 0, 0],
          [0, 0, 1],
          [0, 0, 2],
          [4, 4, 0]
        ],
        id: 'abhg',
        families: [
          {
            pairs: 1,
            types: null
          },
          {
            pairs: 1,
            types: null
          }
        ],
        init: []
      });

      puzzle.set('lock_keys', [[0, 2, 4], [1, 1, 1], [2, 1, 2]]);
      puzzle.generateSolvableGame(tile_faces);
      puzzle.placeLockAndKey = cache;

      puzzle.eachCell(function (tile) {
        if (tile.get('real_face')) {
          done('error');
        }
      });

      puzzle.getLockAndKeyIndexes = cache2;
      done();
    });
  });

  describe('placeLockAndKey()', function () {
    it('should return proper face', function (done) {
      var cache = puzzle.getTile,
        lock_key_indx = {
          locks: [
            {
              position: 1,
              type: 0
            },
            {
              position: 3,
              type: 1
            }
          ],
          keys: [
            {
              position: 2,
              type: 0
            },
            {
              position: 8,
              type: 1
            }
          ]
        };

      puzzle.getTile = function () {
        return {
          set: function () {
          }
        };
      };
      puzzle.set('lock_keys', [[0, 2, 4], [1, 1, 1], [2, 1, 2]]);

      assert.deepEqual(['f_lock_0', null],
        puzzle.placeLockAndKey(lock_key_indx, 1, [1, 2, 3], 'f_a', null));
      puzzle.getTile = cache;
      done();
    });

    it('should return proper tag', function (done) {
      var cache = puzzle.getTile,
        lock_key_indx = {
          locks: [
            {
              position: 1,
              type: 0
            },
            {
              position: 3,
              type: 1
            }
          ],
          keys: [
            {
              position: 2,
              type: 0
            },
            {
              position: 8,
              type: 1
            }
          ]
        };

      puzzle.getTile = function () {
        return {
          set: function () {
          }
        };
      };
      puzzle.set('lock_keys', [[0, 2, 4], [1, 1, 1], [2, 1, 2]]);

      assert.deepEqual(['f_a', 'key_0'],
        puzzle.placeLockAndKey(lock_key_indx, 2, [1, 2, 3], 'f_a', null));
      puzzle.getTile = cache;
      done();
    });

    it('should return the same values', function (done) {
      var cache = puzzle.getTile,
        lock_key_indx = {
          locks: [
            {
              position: 1,
              type: 0
            },
            {
              position: 3,
              type: 1
            }
          ],
          keys: [
            {
              position: 2,
              type: 0
            },
            {
              position: 8,
              type: 1
            }
          ]
        };

      puzzle.getTile = function () {
        return {
          set: function () {
          }
        };
      };
      puzzle.set('lock_keys', [[0, 2, 4], [1, 1, 1], [2, 1, 2]]);

      assert.deepEqual(['f_b', 'key_b'],
        puzzle.placeLockAndKey(lock_key_indx, 100, [1, 2, 3], 'f_b', 'key_b'));
      puzzle.getTile = cache;
      done();
    });
  });

  describe('getLockAndKeyIndexes()', function () {
    it('should return lock and key indexes', function (done) {
      var res;

      puzzle.loadPuzzle({
        layout: [
          [0, 0, 0],
          [3, 0, 0],
          [6, 0, 0],
          [9, 0, 0],
          [0, 4, 0],
          [3, 4, 0],
          [6, 4, 0],
          [9, 4, 0]
        ],
        id: 'abhg',
        families: [
          {
            pairs: 4,
            types: 2
          },
          {
            pairs: 4,
            types: 2
          }
        ],
        init: []
      });
      puzzle.set('lock_keys', [[0, 2, 2]]);
      res = puzzle.getLockAndKeyIndexes([1, 2, 3], 8);
      if (res && res.locks && res.keys) {
        done();
      }
    });

    it('should get enought number of keys', function (done) {
      var res, tile_count;

      puzzle.loadPuzzle({
        layout: [
          [0, 0, 0],
          [3, 0, 0],
          [6, 0, 0],
          [9, 0, 0],
          [0, 4, 0],
          [3, 4, 0]
        ],
        id: 'abhg',
        families: [
          {
            pairs: 4,
            types: 2
          },
          {
            pairs: 4,
            types: 2
          }
        ],
        init: []
      });
      puzzle.set('slot_tile', {
        isKey: function () {
          return false;
        },
        get: function () {
          return '';
        }
      });
      puzzle.set('lock_keys', [[0, 2, 2]]);
      tile_count = puzzle.get('tile_count');
      res = puzzle.getLockAndKeyIndexes([], 6);
      assert.equal(res.keys.length, 2);
      done();
    });

    it('should lock index will be', function (done) {
      var res, tile_count;

      puzzle.loadPuzzle({
        layout: [
          [0, 0, 0],
          [3, 0, 0],
          [6, 0, 0],
          [9, 0, 0],
          [0, 4, 0],
          [3, 4, 0],
          [6, 4, 0],
          [9, 4, 0]
        ],
        id: 'abhg',
        families: [
          {
            pairs: 4,
            types: 2
          },
          {
            pairs: 4,
            types: 2
          }
        ],
        init: []
      });
      puzzle.set('slot_tile', {
        isKey: function () {
          return true;
        },
        get: function () {
          return '';
        }
      });
      puzzle.set('lock_keys', [[0, 2, 2]]);
      tile_count = puzzle.get('tile_count');
      res = puzzle.getLockAndKeyIndexes([1, 2, 3], 8);
      _.each(res.keys, function (item) {
        if (item.position > tile_count) {
          done('error');
        }
      });
      puzzle.set('slot_tile', null);
      done();
    });
  });

  describe('onlyFreeInLine()', function () {
    it('should return false for filled tile early return', function () {
      var position = [8, 8, 0];

      puzzle.loadPuzzle(puzzle_neighbour);
      puzzle.getTile(position).set({free: true});

      assert.equal(false, puzzle.onlyFreeInLine(position));
    });

    it('should return false for filled tile check right', function () {
      var position = [8, 8, 0];

      puzzle.loadPuzzle(puzzle_neighbour);
      puzzle.getTile(position).set({free: true});

      assert.equal(false, puzzle.onlyFreeInLine([6, 8, 0]));
    });

    it('should return false for filled tile check left', function () {
      var position = [0, 3, 0];

      puzzle.loadPuzzle(puzzle_neighbour);
      puzzle.getTile(position).set({face: 10});

      assert.equal(false, puzzle.onlyFreeInLine([2, 2, 0]));
    });

    it('should return true if it is the only free tile' +
      ' in horizontal line', function () {
      var position = [2, 2, 0];

      puzzle.loadPuzzle(puzzle_data);

      assert.equal(true, puzzle.onlyFreeInLine(position));
    });

    it('should compute free recursively for ' +
      ' having two neighbour on a side', function () {
      var tile;

      puzzle.loadPuzzle({layout: [[2, 0, 0], [4, 0, 0], [2, 2, 0],
                        [4, 2, 0], [6, 0, 0], [6, 2, 0],
                        [0, 1, 0], [8, 1, 0]],
                        id: 'pi',
                        families: families,
                        init: []
                      });

      tile = puzzle.get('tiles')[0][4][2];
      tile.set({free: false,
                face: 'ace'});

      assert.equal(false, puzzle.onlyFreeInLine([8, 1, 0]));
    });

    it('should return true for tiles checked recursively', function () {
      puzzle.loadPuzzle({layout: [[2, 5, 0], [4, 5, 0], [6, 4, 0],
                        [2, 3, 0], [4, 3, 0],
                        [8, 5, 0], [10, 5, 0], [12, 5, 0],
                        [8, 3, 0], [10, 3, 0], [12, 3, 0]],
                        id: 'pi',
                        families: families,
                        init: []
                      });

      assert.equal(true, puzzle.onlyFreeInLine([6, 4, 0]));
    });

    it('should return false for tiles checked recursively' +
      ' to the right', function () {
      var tile;

      puzzle.loadPuzzle({layout: [[2, 5, 0], [4, 5, 0], [6, 4, 0],
                        [2, 3, 0], [4, 3, 0],
                        [8, 5, 0], [10, 5, 0], [12, 5, 0],
                        [8, 3, 0], [10, 3, 0], [12, 3, 0]],
                        id: 'pi',
                        families: families,
                        init: []
                      });

      tile = puzzle.get('tiles')[0][10][5];
      tile.set({free: false,
                face: 'ace'});

      assert.equal(false, puzzle.onlyFreeInLine([6, 4, 0]));
    });

    it('should look only left or right if' +
      ' direction provided', function () {
      var tile;

      puzzle.loadPuzzle({layout: [[0, 0, 0], [2, 0, 0],
                        [4, 0, 0], [6, 0, 0]],
                        id: 'pi',
                        families: families,
                        init: []
                      });

      tile = puzzle.get('tiles')[0][6][0];
      tile.set({free: false,
                face: 'ace'});

      assert.equal(true, puzzle.onlyFreeInLine([4, 0, 0], 'left'));
    });
  });

  describe('flattenTiles()', function () {
    it('should return all tiles coord in 1D array', function () {
      var pass = true;

      _.each(puzzle.flattenTiles(), function (model) {
        var coord = model.getCoordinates();

        if (_.indexOf(puzzle_data.puzzle, coord) < 0) {
          pass = false;
        }
      });
      assert.equal(true, pass);
    });

    it('should return selective tiles coord in 1D array', function () {
      var exp_res = [[1, 2], [2, 3], [3, 4],
                      [4, 5], [6, 7], [8, 9]],
          send = {
                  l: [[1, 2], [2, 3], [3, 4]],
                  r: [[4, 5], [6, 7], [8, 9]]
                };

      assert.deepEqual(exp_res, puzzle.flattenTiles('custom', send));
    });

    it('should return all tiles of a layer', function () {
      var exp_res = [2, 2, 1];

      puzzle.loadPuzzle(puzzle_data);

      assert.deepEqual(exp_res,
        puzzle.flattenTiles('layer', 1)[0].getCoordinates());
    });
  });

  describe('selectCoordinates()', function () {
    it('should fail to return position to place tile when' +
      ' no tiles are free', function () {
      puzzle.loadPuzzle(puzzle_data);

      assert.equal(null, puzzle.selectCoordinates());
    });

    it('should not return suitable tile position based' +
      ' on 1st tile position', function () {
      var
        tile_send = [2, 2, 0],
        tile_free = [0, 2, 0],
        model;

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(tile_free);
      model.set('free', true);

      assert.equal(null, puzzle.selectCoordinates(tile_send,
        puzzle.flattenTiles()));
    });
  });

  describe('setPending()', function () {
    it('should set last_match value to that from pending if ongoing ' +
      'is 0', function () {
        puzzle.set({
          pending_last_match: 'test_val',
          last_match: 0
        }, true);

        puzzle.setPending(0);
        assert.strictEqual('test_val', puzzle.get('last_match'));
        assert.strictEqual(undefined, puzzle.get('pending_last_match'));
      }
    );

    it('should do nothing if pending value is not defined', function () {
      puzzle.set('last_match', 0, true);
      puzzle.unset('pending_last_match');
      puzzle.setPending(0);
      assert.strictEqual(0, puzzle.get('last_match'));
    });
    it('should do nothing if ongoing is not 0', function () {
      puzzle.set({
        last_match: 0,
        pending_last_match: 'abc'
      }, true);
      puzzle.setPending(1);
      assert.strictEqual(0, puzzle.get('last_match'));
    });
  });

  describe('updateProperty()', function () {
    it('should not update free attrib of tile if filled', function () {
      var position = [2, 0, 0],
        model, free;

      puzzle.loadPuzzle(puzzle_data);

      model = puzzle.getTile(position);
      model.set('face', 10);

      free = model.get('free');

      puzzle.updateProperty(position);

      assert.equal(free, model.get('free'));
    });

    it('should set free attrib of tile if its onlyFreeInline', function () {
      var position = [2, 0, 0],
        cache = puzzle.onlyFreeInLine,
        model;

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);
      model.set('free', false);
      puzzle.onlyFreeInLine = function () {
        puzzle.onlyFreeInLine = cache;
        return true;
      };
      puzzle.updateProperty(position);

      assert.equal(true, model.get('free'));
    });

    it('should unset free if no right left and below neighbours', function () {
      var position = [2, 0, 0],
        cache = puzzle.onlyFreeInLine,
        cache_get_neighbour = puzzle.getNeighbours,
        model;

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);
      model.set('free', true);
      puzzle.onlyFreeInLine = function () {
        puzzle.onlyFreeInLine = cache;
        return false;
      };
      puzzle.getNeighbours = function () {
        puzzle.getNeighbours = cache_get_neighbour;
        return {
          left: [],
          right: [],
          below: []
        };
      };
      puzzle.updateProperty(position);
      assert.equal(false, model.get('free'));
    });

    it('should update free attrib of tile neighbour check right', function () {
      var position = [2, 2, 0],
        neighbour = [0, 2, 0],
        model;

      puzzle.loadPuzzle(puzzle_data);

      puzzle.getTile(neighbour).set({free: false,
                                    face: null});
      puzzle.getTile(position).set({free: false,
                                    face: 10});

      puzzle.updateProperty(neighbour);

      model = puzzle.getTile(neighbour);

      assert.equal(true, model.get('free'));
    });

    it('should update free attrib of tile neighbour check left', function () {
      var position = [4, 2, 0],
        neighbour = [2, 2, 0],
        model;

      puzzle.loadPuzzle(puzzle_data);

      puzzle.getTile(neighbour).set({free: false});
      puzzle.getTile(position).set({free: false,
                                    face: 10});

      puzzle.updateProperty(neighbour);

      model = puzzle.getTile(neighbour);

      assert.equal(true, model.get('free'));
    });

    it('should not update free attrib of tile neighbour' +
      ' check below', function () {
      var position = [2, 2, 0],
        neighbour = [2, 2, 1],
        model;

      puzzle.loadPuzzle(puzzle_data);
      puzzle.getTile(neighbour).set('free', false);
      puzzle.getTile(position).set('free', false);
      puzzle.updateProperty(neighbour);
      model = puzzle.getTile(neighbour);

      assert.equal(false, model.get('free'));
    });
  });

  describe('isFreeTile()', function () {
    it('should return false if tile is above', function () {
      var model,
        position = [2, 2, 0];

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);

      assert.equal(false, puzzle.isFreeTile(model));
    });

    it('should return true if tile above is slot tile and no right and ' +
      'left tiles', function () {
        var model, above,
          position = [2, 2, 0],
          cache = puzzle.getNeighbours;

        puzzle.loadPuzzle(puzzle_data);
        model = puzzle.getTile(position);
        above = puzzle.getTile([2, 2, 1]);
        puzzle.set('slot_tile', above);

        puzzle.getNeighbours = function () {
          puzzle.getNeighbours = cache;

          return {
            above: [[2, 2, 1]]
          };
        };
        assert.equal(true, puzzle.isFreeTile(model));
        puzzle.set('slot_tile', null);
      }
    );

    it('should return false if tile is on left and right', function () {
      var model,
        position = [12, 10, 0];

      puzzle.loadPuzzle(puzzle_neighbour_autofix);
      model = puzzle.getTile(position);

      assert.equal(false, puzzle.isFreeTile(model));
    });

    it('should return true for a free', function () {
      var model,
        position = [8, 5, 0];

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);

      assert.equal(true, puzzle.isFreeTile(model));
    });
  });

  describe('isOpenSlotTile()', function () {
    it('should return true if passed position is open_slot tile', function () {
      puzzle.set('slot_tile', {
        getCoordinates: function () {
          return [1, 2, 3];
        }
      });

      assert.strictEqual(true, puzzle.isOpenSlotTile([1, 2, 3]));
      puzzle.set('slot_tile', null);
    });

    it('should return true if passed model is open_slot one', function () {
      var obj = {
        getCoordinates: function () {
          return [1, 2, 3];
        }
      };

      puzzle.set('slot_tile', obj);

      assert.strictEqual(true, puzzle.isOpenSlotTile(obj));
      puzzle.set('slot_tile', null);
    });

    it('should return false if slot_tile is null', function () {
      puzzle.set('slot_tile', null);
      assert.strictEqual(false, puzzle.isOpenSlotTile([1, 2, 3]));
    });
  });

  describe('checkGoals', function () {
    it('should call checkComplete if goal completed', function (done) {
      var cache = puzzle.checkComplete;

      puzzle.checkComplete = function () {
        puzzle.checkComplete = cache;
        done();
      };
      puzzle.set('goal', {
        golden: 4
      });
      puzzle.set('collected_golden', 4);
      puzzle.checkGoals();
    });
    it('should not checkComplete if goal not completed', function (done) {
      var cache = puzzle.checkComplete;

      puzzle.checkComplete = function () {
        puzzle.checkComplete = cache;
        done('err');
      };
      puzzle.set('goal', {
        golden: 4
      });
      puzzle.set('collected_golden', 1);
      puzzle.checkGoals();
      puzzle.set('goal', {
        golden: 0
      });
      puzzle.set('golden_collected', 0);
      done();
    });
  });

  describe('flip()', function () {
    it('should set tile_selected to null', function (done) {
      var cache = puzzle.set;

      puzzle.set = function (key, val) {
        if (key === 'tile_selected') {
          puzzle.set = cache;
          assert.strictEqual(val, null);
          done();
        }
      };
      puzzle.flip();
    });
    it('should set mode to flip', function (done) {
      var cache = puzzle.set;

      puzzle.set = function (key, val) {
        if (key === 'mode') {
          puzzle.set = cache;
          assert.strictEqual(val, 'flip');
          done();
        }
      };
      puzzle.flip();
    });
    it('should set hidden to false', function () {
      var i = 0,
        cache = puzzle.eachCell;

      puzzle.eachCell = function (cb) {
        var tiles = [
          {
            isJoker: function () {
              return true;
            },
            isVisible: function () {
              return true;
            },
            get: function (param) {
              if (param === 'face_visibility') {
                return false;
              }
            },
            set: function (param) {
              if (param === 'hidden') {
                i++;
              }
            }
          },
          {
            isJoker: function () {
              return false;
            },
            isVisible: function () {
              return true;
            },
            get: function (param) {
              if (param === 'hidden') {
                return true;
              }
            },
            set: function (param) {
              if (param === 'hidden') {
                i++;
              }
            }
          }
        ];

        puzzle.eachCell = cache;

        _.each(tiles, function (model) {
          cb(model);
        });
      };
      puzzle.flip();
      assert.strictEqual(i, 1);
    });
  });
  describe('pickTile()', function () {
    it('should emit update-joker-face if selected tile is' +
      'not joker and other tile selected',
      function (done) {
      var model,
        type = puzzle.get('type'),
        position = [2, 2, 1];

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);
      model.set('face', 1);
      model.set('hidden', true);
      puzzle.set('tile_selected', model);
      puzzle.set('type', 'memory');
      model = puzzle.getTile([0, 2, 0]);
      model.set('face', 1);
      model.set('hidden', true);
      model.isJoker = function () {
        return false;
      };
      model.isFree = function () {
        return true;
      };
      model.once('update-joker-face', function () {
        puzzle.set('type', type);
        done('err');
      });

      model.isLock = function () {
        return false;
      };

      puzzle.pickTile(model);
      done();
    });
    it('should add a tile if tile_selected is unset', function () {
      var model,
        position = [2, 2, 1];

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);

      puzzle.pickTile(model);

      assert.deepEqual(position,
        puzzle.get('tile_selected').getCoordinates());
    });

    it('should not invke matchTile or swapTile', function (done) {
      var model,
        position = [2, 2, 1];

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);

      puzzle.matchTile = function () {
        done('error');
      };

      puzzle.swapTile = function () {
        done('error');
      };
      puzzle.set('pick_tile', false);
      puzzle.pickTile(model);
      puzzle.set('pick_tile', true);
      done();
    });

    it('should invoke swaptiles', function (done) {
      var model,
        cache = puzzle.swapTile,
        position = [2, 2, 1];

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);

      puzzle.matchTile = function () {
        done('error');
      };

      puzzle.swapTile = function () {
        puzzle.swapTile = cache;
        done();
      };
      puzzle.set('mode', 'swap');
      model.isVisible = function () {
        return true;
      };
      puzzle.pickTile(model);
      done();
      puzzle.set('mode', null);
    });

    it('should set face visibility false if' +
      'partial-memory match and partially hidden', function (done) {
      var model,
        position = [2, 2, 1];

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);

      model.set('hidden', true);
      puzzle.matchTile = function () {
        done('error');
      };

      puzzle.swapTile = function () {
        done('error');
      };
      puzzle.set({
        type: 'partial_memory',
        tile_selected: model
      });
      model.set = function (key, val) {
        if (key === 'face_visibility' && val === false) {
          done();
        }
      };

      model.isFree = function () {
        return true;
      };
      puzzle.pickTile(model);
      puzzle.set({
        type: null,
        tile_selected: null
      });
    });

    it('should set face visibility false if memory match', function (done) {
      var model,
        position = [2, 2, 1];

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);
      model.set('hidden', true);
      puzzle.matchTile = function () {
        done('error');
      };

      puzzle.swapTile = function () {
        done('error');
      };
      puzzle.set({
        type: 'memory',
        tile_selected: model
      });
      model.set = function (key, val) {
        if (key === 'face_visibility' && val === false) {
          done();
        }
      };

      model.isFree = function () {
        return true;
      };
      puzzle.pickTile(model);
      puzzle.set({
        type: null,
        tile_selected: null
      });
    });

    it('should increase golden tile collected if picked', function () {
      var model1, model2,
        i = 0,
        cache = puzzle.increment,
        position1 = [2, 2, 1],
        position2 = [2, 0, 0];

      puzzle.loadPuzzle(puzzle_data);
      model1 = puzzle.getTile(position1);
      model2 = puzzle.getTile(position2);

      puzzle.increment = function (param) {
        puzzle.checkGoals = cache;
        if (param === 'collected_golden') {
          i++;
        }
      };
      puzzle.set('tile_selected', model2);

      model1.isFree = function () {
        return true;
      };
      model2.isFree = function () {
        return true;
      };

      model1.isLock = function () {
        return false;
      };
      model2.isLock = function () {
        return false;
      };
      model1.set('face', 'f_golden');
      model2.set('face', 'f_golden');
      puzzle.pickTile(model1);
      assert.strictEqual(i, 1);
      puzzle.increment = cache;
    });

    it('should set tile selected null', function (done) {
      var model,
        position = [2, 2, 1];

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);

      puzzle.matchTile = function () {
        done('error');
      };

      puzzle.swapTile = function () {
        done('error');
      };
      puzzle.set('tile_selected', model);

      model.isFree = function () {
        return true;
      };
      puzzle.pickTile(model);

      assert.strictEqual(puzzle.get('tile_selected'), null);
      done();
      puzzle.set('tile_selected', null);
    });

    it('should not anything of tile is not free', function (done) {
      var model,
        position = [2, 2, 1];

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);

      puzzle.matchTile = function () {
        done('error');
      };

      puzzle.swapTile = function () {
        done('error');
      };
      puzzle.set('tile_selected', model);

      model.isFree = function () {
        return false;
      };
      puzzle.pickTile(model);

      done();
      puzzle.set('tile_selected', null);
    });

    it('should invoke tileMatched if tiles faces match', function (done) {
      var model,
        position = [2, 2, 1];

      puzzle.tileMatched = function () {
        done();
      };

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);
      model.set('face', 1);
      puzzle.set('tile_selected', model);

      model = puzzle.getTile([8, 5, 0]);
      model.set('face', 1);

      model.isLock = function () {
        return false;
      };

      puzzle.pickTile(model);
    });

    it('should not call updateJokerpair for golden tiles', function (done) {
      var model,
        cache = puzzle.updateJokerpair,
        position = [2, 2, 1];

      puzzle.updateJokerpair = function () {
        puzzle.updateJokerpair = cache;
        done('err');
      };

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);
      model.set('face', 'f_golden');
      puzzle.set('tile_selected', model);

      model = puzzle.getTile([8, 5, 0]);
      model.set('face', 'f_joker');

      puzzle.pickTile(model);
      done();
    });

    it('should call updateLockedTiles is selected is key', function (done) {
      var model,
        cache = puzzle.updateLockedTiles,
        position = [2, 2, 1];

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);
      model.set('face', 'f_abc');
      model.isKey = function () {
        return true;
      };
      puzzle.set('tile_selected', model);

      model = puzzle.getTile([8, 5, 0]);
      model.set('face', 'f_abc');

      puzzle.updateLockedTiles = function () {
        puzzle.updateLockedTiles = cache;
        done();
      };
      puzzle.pickTile(model);
    });

    it('should call updateLockedTiles if one is joker ' +
      'and other is key', function (done) {
      var model,
        cache = puzzle.updateLockedTiles,
        position = [2, 2, 1];

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);
      model.set('face', 'f_abc');
      model.isKey = function () {
        return true;
      };
      puzzle.set('tile_selected', model);

      model = puzzle.getTile([8, 5, 0]);
      model.set('face', 'f_joker');

      puzzle.updateLockedTiles = function () {
        puzzle.updateLockedTiles = cache;
        done();
      };
      puzzle.pickTile(model);
    });

    it('should call wrongMatch', function (done) {
      var model,
        position = [2, 2, 1],
        cache = puzzle.wrongMatch;

      puzzle.tileMatched = function () {};

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);
      model.set('face', 1);
      puzzle.set('tile_selected', model);

      model = puzzle.getTile([8, 5, 0]);
      model.set('face', 2);

      model.isLock = function () {
        return false;
      };

      puzzle.wrongMatch = function () {
        puzzle.wrongMatch = cache;
        done();
      };

      puzzle.pickTile(model);
    });

    it('should emit tiles-matched when tile faces match',
      function (done) {
      var model,
        position = [2, 2, 1];

      puzzle.on('tiles-matched', function () {
        done();
      });

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);
      model.set('face', 1);
      puzzle.set('tile_selected', model);

      model = puzzle.getTile([8, 5, 0]);
      model.set('face', 1);

      model.isLock = function () {
        return false;
      };

      puzzle.pickTile(model);
    });

    it('should emit update-joker-face if one tile is joker',
      function (done) {
      var model,
        type = puzzle.get('type'),
        position = [2, 2, 1];

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);
      model.set('face', 1);
      model.set('hidden', true);
      puzzle.set('tile_selected', model);

      model = puzzle.getTile([8, 5, 0]);
      model.set('face', 1);
      model.set('hidden', true);
      puzzle.set('type', 'memory');
      model.isJoker = function () {
        return true;
      };

      model.once('update-joker-face', function () {
        puzzle.set('type', type);
        done();
      });

      model.isLock = function () {
        return false;
      };

      puzzle.pickTile(model);
    });

    it('should emit update-joker-face if one tile is joker ' +
      'but not mwmory match', function (done) {
      var model,
        type = puzzle.get('type'),
        position = [2, 2, 1];

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);
      model.set('face', 1);
      model.set('hidden', false);
      puzzle.set('tile_selected', model);

      model = puzzle.getTile([8, 5, 0]);
      model.set('face', 1);
      puzzle.set('type', null);
      model.isJoker = function () {
        return false;
      };

      model.once('update-joker-face', function () {
        done('error');
      });
      model.isLock = function () {
        return false;
      };
      puzzle.pickTile(model);
      done();
      puzzle.set('type', type);
    });

    it('should emit update-joker-face if first tile is joker',
      function (done) {
      var model,
        type = puzzle.get('type');

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile([8, 5, 0]);
      model.set('face', 1);
      puzzle.set('tile_selected', model);
      puzzle.set('tile_selected', model);

      model = puzzle.getTile([8, 5, 0]);
      model.set('face', 1);
      model.set('hidden', true);
      puzzle.set('type', 'memory');
      model.isJoker = function () {
        return true;
      };

      model.once('update-joker-face', function () {
        puzzle.set('type', type);
        done();
      });

      model.isLock = function () {
        return false;
      };

      puzzle.pickTile(model);
    });

    it('should call updateJokerPair if first pair is joker',
      function (done) {
      var model,
        position = [2, 2, 1];

      puzzle.on('tiles-matched', function () {
      });

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);
      model.set('face', 'f_joker');
      puzzle.set('tile_selected', model);

      model = puzzle.getTile([8, 5, 0]);
      model.set('face', 1);
      puzzle.updateJokerPair = function () {
        done();
      };
      model.isLock = function () {
        return false;
      };
      puzzle.pickTile(model);
    });

    it('should call updateJokerPair if second pair is joker',
      function (done) {
      var model,
        position = [2, 2, 1],
        cache = puzzle.updateJokerPair;

      puzzle.on('tiles-matched', function () {
      });

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);
      model.set('face', 1);
      puzzle.set('tile_selected', model);

      model = puzzle.getTile([8, 5, 0]);
      model.set('face', 'f_joker');
      puzzle.updateJokerPair = function () {
        puzzle.updateJokerPair = cache;
        done();
      };
      model.isLock = function () {
        return false;
      };
      puzzle.pickTile(model);
    });

    it('should play tap sound', function (done) {
      var model,
        position = [2, 2, 1],
        cache = sounds.play;

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);

      sounds.play = function (name) {
        sounds.play = cache;
        done(name === 'tap' ? undefined : 'error');
      };
      model.isLock = function () {
        return false;
      };
      puzzle.pickTile(model);
    });

    it('should return false if selected tile is lock', function (done) {
      var model,
        position = [2, 2, 1];

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);

      model.emit = function (signal) {
        if (signal === 'wrong-match-animate') {
          done('error');
        }
      };

      model.isFree = function () {
        return false;
      };

      model.isLock = function () {
        return true;
      };

      puzzle.matchTile = function () {
        done('error');
      };

      puzzle.swapTile = function () {
        done('error');
      };

      done(puzzle.pickTile(model) === false ?
        undefined : 'error');
    });

    it('should emit wrong match of tile is lock and free', function (done) {
      var model,
        position = [2, 2, 1];

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);

      model.emit = function (signal) {
        if (signal === 'wrong-match-animate') {
          done();
        }
      };

      model.isFree = function () {
        return true;
      };

      model.isLock = function () {
        return true;
      };

      puzzle.matchTile = function () {
        done('error');
      };

      puzzle.swapTile = function () {
        done('error');
      };

      assert.equal(puzzle.pickTile(model), false);
    });
  });

  describe('swapTiles()', function () {
    it('should add a tile if tile_selected is unset', function () {
      var model,
        position = [2, 2, 1];

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);

      puzzle.swapTiles(model);

      assert.deepEqual(position,
        puzzle.get('tile_selected').getCoordinates());
    });

    it('should emit swap-animate', function (done) {
      var cache = puzzle.emit,
        get_cache = puzzle.get,
        model = {
          emit: function (event) {
            if (event === 'swap-animate') {
              done();
            }
          }
        };

      puzzle.loadPuzzle(puzzle_data);
      puzzle.get = function (key) {
        if (key === 'tile_selected') {
          puzzle.get = cache;
          return model;
        } else {
          return get_cache();
        }
      };

      puzzle.swapTiles(model);
    });

    it('should call doSwap Animate', function (done) {
      var cache = puzzle.emit,
        cache_swap_animate = puzzle.doSwap,
        get_cache = puzzle.get,
        model = {
          emit: function (event, cb) {
            cb();
          },
          get: function () {}
        };

      puzzle.get = function (key) {
        if (key === 'tile_selected') {
          puzzle.get = cache;
          return model;
        } else {
          return get_cache();
        }
      };

      puzzle.doSwap = function () {
        puzzle.doSwap = cache_swap_animate;
        done();
      };

      puzzle.swapTiles(model, model);
    });

    it('should play sound swap_tap', function (done) {
      var model,
        position = [2, 2, 1],
        cache = sounds.play;

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);

      sounds.play = function (name) {
        sounds.play = cache;
        done(name === 'swap_tap' ? undefined : 'error');
      };
      puzzle.swapTiles(model, model);
    });

    it('should play sound swap', function (done) {
      var cache = puzzle.emit,
        cache_swap_animate = puzzle.doSwap,
        get_cache = puzzle.get,
        play_cache = sounds.play,
        model = {
          emit: function (event, cb) {
            cb();
          },
          get: function () {}
        };

      puzzle.get = function (key) {
        if (key === 'tile_selected') {
          puzzle.get = cache;
          return model;
        } else {
          return get_cache();
        }
      };

      puzzle.doSwap = function () {
        puzzle.doSwap = cache_swap_animate;
      };

      sounds.play = function (name) {
        if (name === 'swap') {
          sounds.play = play_cache;
          done();
        }
      };

      puzzle.swapTiles(model, model);
    });

    it('should set pick_tile to false', function (done) {
      var cache_swap_animate = puzzle.doSwap,
        model = {
          emit: function (event, cb) {
            cb();
          },
          get: function () {}
        };

      puzzle.doSwap = function () {
        puzzle.doSwap = cache_swap_animate;
      };

      puzzle.swapTiles(model, model);

      done(puzzle.get('pick_tile') === false ? undefined : 'error');
    });
  });

  describe('doSwap()', function () {
    it('should call puzzle.set twise', function (done) {
      var cache = puzzle.set,
        i = 0,
        tile_one = {
          get: function () {},
          emit: function () {},
          set: function () {},
          isJoker: function () {},
          isGoldenTile: function () {}
        },
        tile_two = {
          get: function () {},
          emit: function () {},
          set: function () {},
          isJoker: function () {},
          isGoldenTile: function () {}
        };

      puzzle.set = function () {
        i++;
        if (i === 1) {
          done();
        }
      };
      puzzle.doSwap(tile_one, tile_two);
      puzzle.set = cache;
    });

    it('should update game state aftr swap animation', function (done) {
      var cache = puzzle.updateGameState,
        tile_one = {
          get: function () {},
          emit: function (param, cb) {
            if (cb) {
              cb();
            }
          },
          set: function () {},
          isJoker: function () {},
          isGoldenTile: function () {}
        },
        tile_two = {
          get: function () {},
          emit: function (param, cb) {
            if (cb) {
              cb();
            }
          },
          set: function () {},
          isJoker: function () {},
          isGoldenTile: function () {}
        };

      puzzle.updateGameState = function () {
        puzzle.updateGameState = cache;
        done();
      };

      puzzle.doSwap(tile_one, tile_two);
    });

    it('should emit after-swap-animate for two tiles', function (done) {
      var i = 0,
        tile_one = {
          emit: function () {
            i++;
          },
          get: function () {},
          set: function () {},
          isJoker: function () {},
          isGoldenTile: function () {}
        },
        tile_two = {
          emit: function () {
            i++;
          },
          get: function () {},
          set: function () {},
          isJoker: function () {},
          isGoldenTile: function () {}
        };

      puzzle.doSwap(tile_one, tile_two);

      if (i === 2) {
        done();
      }
    });

    it('should emit powerup applied', function (done) {
      var emit_cache = puzzle.emit,
        tile_one = {
          emit: function () {},
          get: function () {},
          set: function () {},
          isJoker: function () {},
          isGoldenTile: function () {}
        },
        tile_two = {
          emit: function () {},
          get: function () {},
          set: function () {},
          isJoker: function () {},
          isGoldenTile: function () {}
        };

      puzzle.emit = function (event) {
        if (event === 'powerup-applied') {
          puzzle.changeFace = cache;
          puzzle.emit = emit_cache;
          done();
        }
      };

      puzzle.doSwap(tile_one, tile_two);
    });

    it('should not emit powerup applied if silent', function (done) {
      var tile_one = {
          emit: function () {},
          get: function () {},
          set: function () {},
          isJoker: function () {},
          isGoldenTile: function () {}
        },
        tile_two = {
          emit: function () {},
          get: function () {},
          set: function () {},
          isJoker: function () {},
          isGoldenTile: function () {}
        };

      puzzle.emit = function (event) {
        if (event === 'powerup-applied') {
          done('error');
        }
      };

      puzzle.doSwap(tile_one, tile_two, true);
      done();
    });

    it('should swap real_face id joker', function (done) {
      var emit_cache = puzzle.emit,
        tile_one = {
          emit: function () {},
          get: function () {},
          set: function () {},
          isJoker: function () {}
        },
        tile_two = {
          emit: function () {},
          get: function (key) {
            if (key === 'real_face') {
              done();
            }
          },
          set: function () {},
          isJoker: function () {
            return true;
          }
        };

      puzzle.emit = function (event) {
        if (event === 'powerup-applied') {
          puzzle.changeFace = cache;
          puzzle.emit = emit_cache;
        }
      };

      puzzle.doSwap(tile_one, tile_two);
    });

    it('should set tile pickable', function (done) {
      var emit_cache = puzzle.emit,
        tile_one = {
          emit: function () {},
          get: function () {},
          set: function () {},
          isJoker: function () {}
        },
        tile_two = {
          emit: function () {},
          get: function () {},
          set: function () {},
          isJoker: function () {
            return true;
          }
        };

      puzzle.emit = function (event) {
        if (event === 'powerup-applied') {
          puzzle.changeFace = cache;
          puzzle.emit = emit_cache;
        }
      };

      puzzle.doSwap(tile_one, tile_two);

      done(puzzle.get('pick_tile') ? undefined : false);
    });
  });

  describe('tileMatched()', function () {
    it('should emit match-animate by first and second', function (done) {
      var position = [2, 2, 1],
        count = 1,
        cache_clean = puzzle.cleanTile,
        cache, model;

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);
      cache = model._subscribers['match-animate'];
      puzzle.cleanTile = function () {
        puzzle.cleanTile = cache_clean;
      };
      model.on('match-animate', function (callback) {
        callback();
        if (count === 2) {
          model.on('match-animate', cache);
          done();
        }
        count++;
      });

      puzzle.tileMatched([model, model]);
    });

    it('should call afterTitleMatchAnim only second time', function (done) {
      var position = [2, 2, 1],
        count = 1,
        cache = puzzle.afterTileMatchAnim,
        cache_clean = puzzle.cleanTile,
        model, cache_anim;

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);
      cache_anim = model._subscribers['match-animate'];
      puzzle.afterTileMatchAnim = function () {
        done();
        puzzle.afterTileMatchAnim = cache;
      };
      puzzle.cleanTile = function () {
        puzzle.cleanTile = cache_clean;
      };
      model.on('match-animate', function (callback) {
        callback();
        if (count === 2) {
          model.on('match-animate', cache_anim);
        }
        count++;
      });
      puzzle.tileMatched([model, model]);
    });
  });

  describe('wrongMatch()', function () {
    it('should emit match-animate by first and second', function (done) {
      var position = [2, 2, 1],
        count = 1,
        cache_clean = puzzle.cleanTile,
        cache, model;

      puzzle.loadPuzzle(puzzle_data);
      puzzle.set('type', 'normal');
      model = puzzle.getTile(position);
      cache = model._subscribers['match-animate'];
      puzzle.cleanTile = function () {
        puzzle.cleanTile = cache_clean;
      };
      model.on('wrong-match-animate', function (cb) {
        if (count === 2) {
          model.on('match-animate', cache);
          done();
        }
        count++;
        if (cb) {
          cb();
        }
      });

      puzzle.wrongMatch(model, model);
    });

    it('should set value for tile_selected', function (done) {
      var position = [2, 2, 1],
        cache, model;

      puzzle_data.type = 'normal';
      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);
      cache = model.set;

      puzzle.set = function (key) {
        if (key === 'tile_selected') {
          puzzle.set = cache;
          done();
        }
      };

      model.on('wrong-match-animate', function (cb) {
        if (cb) {
          cb();
        }
      });

      puzzle.wrongMatch(model, model);
    });

    it('should invoke call back if memory variation', function (done) {
      var position = [2, 2, 1],
        cache_clean = puzzle.cleanTile,
        after_animate_cache = puzzle.afterWrongMatchAnimate,
        cache, model;

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);
      cache = model._subscribers['match-animate'];
      puzzle.cleanTile = function () {
        puzzle.cleanTile = cache_clean;
      };

      puzzle.set('type', 'memory');
      model.on('wrong-match-animate', function (cb) {
        if (cb) {
          cb();
        }
      });

      puzzle.afterWrongMatchAnimate = function () {
        puzzle.afterWrongMatchAnimate = after_animate_cache;
        done();
      };

      puzzle.wrongMatch(model, model);
    });
  });

  describe('updateLockedTiles()', function () {
    it('should call unlockTile', function (done) {
      var tiles = [
          {
            isKey: function () {
              return true;
            },
            get: function () {
              return 'key_1';
            }
          },
          {
            isKey: function () {
              return false;
            }
          }
        ],
        cache = puzzle.unlockTile;

      puzzle.unlockTile = function (type) {
        puzzle.unlockTile = cache;
        done(type === '1' ? undefined : 'error');
      };

      puzzle.updateLockedTiles(tiles);
    });

    it('should return unlocked tiles', function (done) {
      var tiles = [
          {
            isKey: function () {
              return true;
            },
            get: function () {
              return 'key_1';
            }
          },
          {
            isKey: function () {
              return false;
            }
          }
        ],
        cache = puzzle.unlockTile;

      puzzle.unlockTile = function () {
        puzzle.unlockTile = cache;
        return tiles[0];
      };

      assert.deepEqual([tiles[0]], puzzle.updateLockedTiles(tiles));
      done();
    });
  });

  describe('unlockTile()', function () {
    it('should reduce lock_tiles', function (done) {
      var set_cache = puzzle.set;

      puzzle.loadPuzzle({
        layout: [
          [0, 0, 0],
          [3, 0, 0],
          [6, 0, 0],
          [9, 0, 0],
          [0, 4, 0],
          [3, 4, 0],
          [6, 4, 0],
          [9, 4, 0]
        ],
        id: 'abhg',
        families: [
          {
            pairs: 4,
            types: null
          },
          {
            pairs: 4,
            types: null
          }
        ],
        init: []
      });

      puzzle.eachCell(function (model, i) {
        model.set('face', 'f_lock_' + i);
      });
      puzzle.set('lock_keys', [[0, 2, 4]]);

      puzzle.set = function (key, val) {
        puzzle.set = set_cache;
        assert.deepEqual([[0, 1, 3]], val);
        done();
      };

      puzzle.unlockTile(0);
    });

    it('should emit key-matched', function (done) {
      var emit_cache = puzzle.emit,
        i = 0;

      puzzle.loadPuzzle({
        layout: [
          [0, 0, 0],
          [3, 0, 0],
          [6, 0, 0],
          [9, 0, 0],
          [0, 4, 0],
          [3, 4, 0],
          [6, 4, 0],
          [9, 4, 0]
        ],
        id: 'abhg',
        families: [
          {
            pairs: 4,
            types: null
          },
          {
            pairs: 4,
            types: null
          }
        ],
        init: []
      });

      puzzle.eachCell(function (model) {
        model.set('face', 'f_lock_' + i);
        i++;
      });
      puzzle.set('lock_keys', [[1, 1, 1], [0, 1, 4]]);

      puzzle.emit = function (event) {
        puzzle.emit = emit_cache;
        done(event === 'key-matched' ? undefined : 'error');
      };

      puzzle.unlockTile(0);
    });

    it('should reduce key only', function (done) {
      var set_cache = puzzle.set,
        i = 1;

      puzzle.loadPuzzle({
        layout: [
          [0, 0, 0],
          [3, 0, 0],
          [6, 0, 0],
          [9, 0, 0],
          [0, 4, 0],
          [3, 4, 0],
          [6, 4, 0],
          [9, 4, 0]
        ],
        id: 'abhg',
        families: [
          {
            pairs: 4,
            types: null
          },
          {
            pairs: 4,
            types: null
          }
        ],
        init: []
      });

      puzzle.eachCell(function (model) {
        model.set('face', 'f_lock_' + i);
        i++;
      });
      puzzle.set('lock_keys', [[0, 2, 4]]);

      puzzle.set = function (key, val) {
        assert.deepEqual([[0, 2, 3]], val);
        done();
      };

      puzzle.unlockTile(0);
      puzzle.set = set_cache;
    });
  });

  describe('lockTile', function () {
    it('should increment key and lock', function (done) {
      var set_cache = puzzle.set;

      puzzle.set('lock_keys', [[0, 1, 3], [1, 0, 0]]);

      puzzle.set = function (key, val) {
        puzzle.set = set_cache;
        assert.deepEqual([[0, 1, 3], [1, 1, 1]], val);
        done();
      };

      puzzle.lockTile(1);
    });
  });

  describe('afterTileMatchAnim()', function () {
    it('should invoke cleanTile', function () {
      var position = [2, 2, 1],
        count = 0,
        count_match = 1,
        model, cache_anim;

      puzzle.cleanTile = function () {
        count++;
      };
      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);
      cache_anim = model._subscribers['match-animate'];
      model.on('match-animate', function (callback) {
        callback();
        if (count_match === 2) {
          model.on('match-animate', cache_anim);
        }
        count_match++;
      });

      puzzle.tileMatched([model, model]);

      assert.equal(2, count);
    });

    it('should decrement pending_matches', function () {
      var position = [2, 2, 1],
        count = 0,
        count_match = 1,
        model, cache_anim;

      puzzle.cleanTile = function () {
        count++;
      };
      puzzle.loadPuzzle(puzzle_data);
      puzzle.set('pending_matches', 3);
      model = puzzle.getTile(position);
      cache_anim = model._subscribers['match-animate'];
      model.on('match-animate', function (callback) {
        assert.equal(4, puzzle.get('pending_matches'));
        callback();
        if (count_match === 2) {
          model.on('match-animate', cache_anim);
        }
      });

      puzzle.tileMatched([model, model]);

      assert.equal(3, puzzle.get('pending_matches'));
    });

    it('should reset last matches if any matching tile is open slot ' +
      'tile', function () {
        var position = [2, 2, 1],
          count = 0,
          count_match = 1,
          cache_open = puzzle.isOpenSlotTile,
          model, cache_anim;

        puzzle.cleanTile = function () {
          count++;
        };
        puzzle.loadPuzzle(puzzle_data);
        puzzle.set({
          pending_matches: 3,
          last_match: [1]
        });
        model = puzzle.getTile(position);
        cache_anim = model._subscribers['match-animate'];
        model.on('match-animate', function (callback) {
          assert.equal(4, puzzle.get('pending_matches'));
          callback();
          if (count_match === 2) {
            model.on('match-animate', cache_anim);
          }
        });

        puzzle.isOpenSlotTile = function () {
          return true;
        };
        puzzle.tileMatched([model, model]);

        assert.deepEqual([], puzzle.get('last_match'));
        puzzle.isOpenSlotTile = cache_open;
      }
    );
  });

  describe('afterWrongMatchAnimate()', function () {
    it('should set visibility false if hidden tile', function (done) {
      var position = [2, 2, 1],
        count = 0,
        model;

      puzzle.cleanTile = function () {
      };
      puzzle.loadPuzzle(puzzle_data);
      puzzle.set('type', 'memory');
      model = puzzle.getTile(position);
      model.set('hidden', true);
      model.set = function (key) {
        if (key === 'face_visibility') {
          count++;
        }
      };

      puzzle.afterWrongMatchAnimate(model, model);
      if (count === 2) {
        done();
      }
    });

    it('should not set visibility false if not hidden tile', function (done) {
      var position = [2, 2, 1],
        model;

      puzzle.cleanTile = function () {
      };
      puzzle.loadPuzzle(puzzle_data);
      puzzle.set('type', 'memory');
      model = puzzle.getTile(position);
      model.set('hidden', false);
      model.set = function (key) {
        if (key === 'face_visibility') {
          done('err');
        }
      };

      puzzle.afterWrongMatchAnimate(model, model);
      done();
    });
  });

  describe('updateGameState()', function () {
    it('should not emit game-over if goal completed', function (done) {
      puzzle.set('goal_completed', true);
      puzzle.set('tile_count', 0);
      puzzle.on('game-over', function () {
        done('err');
      });
      puzzle.updateGameState();
      done();
      puzzle.set('goal_completed', true);
    });

    it('should emit game-over with lose if jewels in order', function () {
      var cache = puzzle.emit;

      puzzle.emit = function (param, param2) {
        if (param === 'game-over') {
          assert.strictEqual(param2, false);
          puzzle.emit = cache;
        }
      };
      puzzle.set('jewels', ['c', 't', 's']);
      puzzle.set('jewel_order', ['c', 's', 't']);
      puzzle.set('tile_count', 0);
      puzzle.updateGameState();
    });
    it('should emit game-over if no tile on the board', function (done) {
      var position = [2, 2, 1],
        cache = puzzle.getScore,
        count_match = 1,
        model, cache_anim;

      puzzle.getScore = function () {
        puzzle.getScore = cache;
        return 10;
      };

      puzzle.cleanTile = function () {
        puzzle.decrement('tile_count');
      };

      puzzle.on('game-over', function () {
        done();
      });

      puzzle.on('change:last_match', done);

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);

      cache_anim = model._subscribers['match-animate'];
      model.on('match-animate', function (callback) {
        callback();
        if (count_match === 2) {
          model.on('match-animate', cache_anim);
        }
        count_match++;
      });
      puzzle.set('tile_count', 2);

      puzzle.tileMatched([model, model]);
    });

    it('should emit tiles-update if tiles on board', function (done) {
      var position = [2, 2, 1],
        count_match = 1,
        model, cache_anim;

      puzzle.cleanTile = function () {};
      puzzle.getNeighbours = function () {
        return position;
      };

      puzzle.on('tiles-update', function (obj) {
        assert.deepEqual(obj, _.union(position, position));
        done();
      });

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);
      cache_anim = model._subscribers['match-animate'];
      model.on('match-animate', function (callback) {
        callback();
        if (count_match === 2) {
          model.on('match-animate', cache_anim);
        }
        count_match++;
      });

      puzzle.tileMatched([model, model]);
    });

    it('should set last_match with tiles and its faces', function () {
      var position = [2, 2, 1],
        count_match = 1,
        model, cache_anim;

      puzzle.cleanTile = function () {};
      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);
      model.on('match-animate', function (callback) {
        callback();
        if (count_match === 2) {
          model.on('match-animate', cache_anim);
        }
        count_match++;
      });
      model.set('face', 2);
      puzzle.set('tile_count', 2);

      puzzle.tileMatched([model, model]);

      assert.deepEqual(puzzle.get('last_match'), [[[2, 2, 1, 2, null,
          null, false],
        [2, 2, 1, 2, null, null, false]]]);
    });

    it('should set last_match empty', function () {
      var position = [2, 2, 1],
        count_match = 1,
        model, cache_anim;

      puzzle.cleanTile = function () {};
      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);
      model.on('match-animate', function (callback) {
        callback();
        if (count_match === 2) {
          model.on('match-animate', cache_anim);
        }
        count_match++;
      });
      model.set('face', 2);
      puzzle.set('tile_count', 2);

      puzzle.updateGameState(false, false, false);

      assert.deepEqual(puzzle.get('last_match'), []);
    });
    it('should test board unsolvable', function () {
      var position = [2, 2, 1],
        model;

      puzzle.cleanTile = function () {};
      puzzle.getNeighbours = function () {
        return position;
      };
      puzzle.isBoardSolvable = function () {
        puzzle.set('free_shuffle', 1);
        return false;
      };

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);
      puzzle.set('tile_count', 2);

      puzzle.afterTileMatchAnim([model, model]);
      assert.strictEqual(1, puzzle.get('free_shuffle'));
      puzzle.set('free_shuffle', 0);
    });

    it('should emit auto shuffle if isBoardSolvable false, silent false',
      function (done) {
      var position = [2, 2, 1],
        model;

      puzzle.cleanTile = function () {};
      puzzle.getNeighbours = function () {
        return position;
      };
      puzzle.isBoardSolvable = function () {
        puzzle.set('free_shuffle', 1);
        return false;
      };

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);
      puzzle.set('tile_count', 2);

      puzzle.once('auto-shuffle', done);

      puzzle.afterTileMatchAnim([model, model], false);
      puzzle.set('free_shuffle', 0);
    });

    it('should emit shuffle if isBoardSolvable false, shuffle_game set',
      function (done) {
      var position = [2, 2, 1],
        model;

      puzzle.cleanTile = function () {};
      puzzle.getNeighbours = function () {
        return position;
      };
      puzzle.isBoardSolvable = function () {
        puzzle.set('free_shuffle', 1);
        return false;
      };

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);
      puzzle.set('tile_count', 2);
      puzzle.set('shuffle_game', true);

      puzzle.once('shuffle', done);

      puzzle.afterTileMatchAnim([model, model], false);
      puzzle.set('free_shuffle', 0);
    });

    it('should call shuffle if isBoardSolvable false and silent true',
      function (done) {
      var position = [2, 2, 1],
        cache = puzzle.shuffle,
        model;

      puzzle.cleanTile = function () {};
      puzzle.getNeighbours = function () {
        return position;
      };
      puzzle.isBoardSolvable = function () {
        puzzle.set('free_shuffle', 1);
        return false;
      };

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);
      puzzle.set('tile_count', 2);

      puzzle.shuffle = function () {
        puzzle.shuffle = cache;
        done();
      };

      puzzle.updateGameState([], false, true);
      puzzle.set('free_shuffle', 0);
    });

    it('should update last_match if board is not solvable',
      function () {
        var position = [2, 2, 1],
          cache = puzzle.shuffle,
          model;

        puzzle.cleanTile = function () {};
        puzzle.getNeighbours = function () {
          return position;
        };
        puzzle.isBoardSolvable = function () {
          puzzle.set('free_shuffle', 1);
          return false;
        };

        puzzle.loadPuzzle(puzzle_data);
        model = puzzle.getTile(position);
        puzzle.set('tile_count', 2);

        puzzle.shuffle = function () {
          puzzle.shuffle = cache;
        };

        puzzle.set('last_match', [], true);
        puzzle.updateGameState([1, 2, 3], false, true);
        puzzle.set('free_shuffle', 0);
        assert.deepEqual([[1, 2, 3]], puzzle.get('last_match'));
      }
    );

    it('should emit layout-changed event with false', function () {
      var position = [2, 2, 1],
        model;

      puzzle.cleanTile = function () {};
      puzzle.getNeighbours = function () {
        return position;
      };
      puzzle.isBoardSolvable = function () {
        return true;
      };
      puzzle.generateSolvableLayout = function () {};
      puzzle.isSolvableLayout = function () {};

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);

      puzzle.set('last_match', [1, 2]);
      puzzle.on('layout-changed', function (resize) {
        assert.strictEqual(resize, false);
      });
      puzzle.afterTileMatchAnim([model, model]);
    });
    it('should emit layout-changed event with true', function () {
      var position = [2, 2, 1],
        model;

      puzzle.cleanTile = function () {};
      puzzle.getNeighbours = function () {
        return position;
      };
      puzzle.isBoardSolvable = function () {
        return true;
      };
      puzzle.generateSolvableLayout = function () {};
      puzzle.isSolvableLayout = function () {};

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);

      puzzle.on('layout-changed', function (resize) {
        assert.strictEqual(resize, true);
      });
      puzzle.afterTileMatchAnim([model, model]);
    });
    it('should set last match is empty', function (done) {
      var position = [2, 2, 1],
        cache = puzzle.set,
        model;

      puzzle.cleanTile = function () {};
      puzzle.getNeighbours = function () {
        return position;
      };
      puzzle.isBoardSolvable = function () {
        return true;
      };
      puzzle.generateSolvableLayout = function () {};
      puzzle.isSolvableLayout = function () {};

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);

      puzzle.on('layout-changed', function (resize) {
        assert.strictEqual(resize, true);
      });
      puzzle.set = function (key, val) {
        if (key === 'last_match') {
          puzzle.set = cache;
          assert.equal(val.length, 0);
          done();
        }
      };
      puzzle.afterTileMatchAnim([model, model]);
    });
  });

  describe('highlight', function () {
    it('should change state oif in swap mode', function (done) {
      puzzle.set('mode', 'swap');
      puzzle.highlight(tile);
      if (tile.get('state') === 2) {
        done('error');
      } else {
        puzzle.set('swap', null);
        done();
      }
    });

    it('should change state of tile if passed', function () {
      puzzle.highlight(tile);
      assert.strictEqual(tile.get('state'), 2);
    });

    it('should highlight both tiles if match', function () {
      var cache = puzzle.getPrevious,
        tile2 = new TileModel(),
        cache_joker = puzzle.isJoker;

      tile2.set({
        face: tile.get('face'),
        state: 2
      });
      tile2.isJoker = function () {
        tile2.isJoker = cache_joker;
        return false;
      };
      puzzle.getPrevious = function () {
        puzzle.getPrevious = cache;
        return tile2;
      };

      puzzle.highlight(tile);
      assert.strictEqual(tile2.get('state'), 2);
      assert.strictEqual(tile.get('state'), 2);
    });

    it('should reset state to orginal if previous didnt match new',
      function () {
      var cache = puzzle.getPrevious;

      puzzle.getPrevious = function () {
        puzzle.getPrevious = cache;
        return tile;
      };

      puzzle.highlight();
      assert.strictEqual(tile.get('state'), 1);
    });

    it('should get last_match blank array of no moves made',
      function () {
      var cache = puzzle.get;

      puzzle.get = function (key) {
        if (key === 'last_match') {
          puzzle.get = cache;
          return null;
        }
      };
      puzzle.updateGameState([], false, false);
    });
  });
  describe('cleanHinted', function () {
    it('should clean the half hint tile', function () {
      var hinted;

      puzzle.loadPuzzle(puzzle_data);
      hinted = [puzzle.getTile([2, 2, 1])];

      puzzle.get = function (val) {
        if (val === 'hinted') {
          return hinted;
        }
      };

      puzzle.cleanHinted();
      assert.notStrictEqual(3, hinted[0].get('state'));
    });

    it('should clean the full hint tiles', function () {
      var hinted;

      puzzle.loadPuzzle(puzzle_data);

      hinted = [puzzle.getTile([2, 2, 1]), puzzle.getTile([0, 2, 0])];
      puzzle.get = function (val) {
        if (val === 'hinted') {
          return hinted;
        }
      };

      puzzle.cleanHinted();
      _.each(hinted, function (h) {
        assert.strictEqual(1, h.get('state'));
      });
    });
    it('should unregister the auto hinted timer if running', function (done) {
      var cache_timer_has = dh_timer.has,
          cache_unregister = dh_timer.unregister;

      puzzle.loadPuzzle(puzzle_data);

      dh_timer.has = function () {
        dh_timer.has = cache_timer_has;
        return true;
      };
      dh_timer.unregister = function () {
        dh_timer.unregister = cache_unregister;
        done();
      };
      puzzle.cleanHinted();
    });
  });

  describe('beforePowerup', function () {
    it('should call cleanHinted', function (done) {
      var cache = puzzle.cleanHinted;

      puzzle.cleanHinted = function () {
        puzzle.cleanHinted = cache;
        done();
      };

      puzzle.beforePowerup();
    });

    it('should hide the face for selected tile ' +
      'which is hidden', function (done) {
      puzzle.set('type', 'partial_memory');
      puzzle.set('tile_selected', {
        get: function (param) {
          if (param === 'hidden') {
            return true;
          }
        },
        set: function (param, value) {
          if (param === 'face_visibility' && value === false) {
            done();
          }
        }
      });

      puzzle.beforePowerup();
    });

    it('should not hide the face for selected tile ' +
      'which is not hidden', function (done) {
      puzzle.set('type', 'partial_memory');
      puzzle.set('tile_selected', {
        get: function (param) {
          if (param === 'hidden') {
            return false;
          }
        },
        set: function (param, value) {
          if (param === 'face_visibility' && value === false) {
            done('err');
          }
        }
      });

      puzzle.beforePowerup();
      done();
    });

    it('should hide face for hidden tile', function (done) {
      var cache = puzzle.cleanHinted,
        get_cache = puzzle.get;

      puzzle.cleanHinted = function () {
        puzzle.cleanHinted = cache;
      };

      puzzle.get = function (item) {
        if (item === 'type') {
          return 'memory';
        } else if (item === 'tile_selected') {
          puzzle.get = get_cache;
          return {
            set: function (key, val) {
              assert.strictEqual(key, 'face_visibility');
              done(val === false ? undefined : 'error');
            },
            get: function (params) {
              if (params === 'hidden') {
                return true;
              }
            }
          };
        }
      };

      puzzle.beforePowerup();
    });
  });

  describe('cleanTile()', function () {
    it('should update tiles attribute', function () {
      var position = [2, 2, 1],
        model;

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);

      puzzle.cleanTile(model);

      assert.equal(null,
        puzzle.get('tiles')[1][2][2]);
    });

    it('should unset slot_tile if for slot tile clean', function () {
      var position = [2, 2, 1],
        cache = puzzle.isOpenSlotTile,
        model;

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);
      puzzle.set('slot_tile', {});

      puzzle.isOpenSlotTile = function () {
        puzzle.isOpenSlotTile = cache;
        return true;
      };
      puzzle.cleanTile(model);

      assert.strictEqual(null, puzzle.get('slot_tile'));
    });

    it('should decrement tile_count', function () {
      var position = [2, 2, 1],
        model;

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);

      puzzle.cleanTile(model);

      assert.equal(7, puzzle.get('tile_count'));
    });

    it('should call releaseModel', function (done) {
      var cache = puzzle.tile.releaseModel;

      puzzle.tile.releaseModel = function () {
        puzzle.tile.releaseModel = cache;
        done();
      };
      tile.set({
        x: 0,
        y: 0,
        z: 0
      });
      puzzle.set('tiles', [[[0]]]);

      puzzle.cleanTile(tile);
    });
  });

  describe('isBoardSolvable()', function () {
    it('should return true if one tile is joker', function () {
      var cache = puzzle.eachCell,
        tile_free_cache = puzzle.isFreeTile;

      puzzle.isMatchable = function () {
        return true;
      };

      puzzle.eachCell = function (cb) {
        var tiles = [
          {
            isJoker: function () {
              return true;
            },
            get: function () {}
          },
          {
            isJoker: function () {
              return false;
            },
            get: function () {}
          }
        ];

        _.find(tiles, function (model) {
          cb(model);
        });
      };
      puzzle.set('tile_per_match', 2);
      assert.equal(true, puzzle.isBoardSolvable());
      puzzle.isFreeTile = tile_free_cache;
      puzzle.eachCell = cache;
    });

    it('should invoke false for unsolvable board', function () {
      var tile;

      puzzle.loadPuzzle({layout: [[2, 0, 0], [4, 0, 0], [2, 2, 0],
                                  [4, 2, 0], [6, 0, 0], [6, 2, 0],
                                  [0, 1, 0], [8, 1, 0]],
                        id: 'pi',
                        families: families,
                        init: []
                      });

      puzzle.assignFaces(families);

      tile = puzzle.get('tiles')[0][0][1];
      tile.set('face', '99');

      tile = puzzle.get('tiles')[0][8][1];
      tile.set('face', '98');

      assert.equal(false, puzzle.isBoardSolvable());
    });
  });

  describe('onShuffleGenerate()', function () {
    it('should invoke onShuffleFailed if not success', function (done) {
      var cache = puzzle.onShuffleFailed;

      puzzle.onShuffleFailed = function () {
        done();
        puzzle.onShuffleFailed = cache;
      };

      puzzle.onShuffleGenerate(false);
    });

    it('should invoke createLoadArray', function (done) {
      var cache = puzzle.createLoadArray;

      puzzle.createLoadArray = function (success, type) {
        puzzle.createLoadArray = cache;
        assert.strictEqual(success, true);
        assert.strictEqual(type, 'shuffle');
        done();
      };

      puzzle.onShuffleGenerate(true, true);
    });

    it('should emit tiles-update', function (done) {
      puzzle.once('tiles-update', function () {
        done();
      });
      puzzle.onShuffleGenerate(true, true);
    });

    it('should emit powerup-applied with shuffle if powerup ' +
      'useage', function (done) {
        puzzle.once('powerup-applied', function (powerup) {
          done(powerup === 'shuffle' ? undefined : 'error');
        });

        puzzle.onShuffleGenerate(true);
      }
    );

    it('shouldnt emit powerup applied with shuffle if shuffle_game is ' +
      'true', function (done) {
        puzzle.once('powerup-applied', done);
        puzzle.set('shuffle_game', true);
        puzzle.onShuffleGenerate(true);
        done();
      }
    );

    it('should emit shuffle-complete if success', function (done) {
      puzzle.once('shuffle-complete', function () {
        done();
      });
      puzzle.onShuffleGenerate(true);
    });

    it('shouldnt emit shuffle-complete if failed', function (done) {
      puzzle.once('shuffle-complete', function () {
        done('error');
      });
      puzzle.onShuffleGenerate(false);
      done();
    });

    it('should set last_match to empty array if ongoing is 0 ' +
      'and powerup', function () {
        puzzle.set({
          last_match: [1],
          ongoing: 0
        }, true);
        puzzle.onShuffleGenerate(true);
        assert.deepEqual([], puzzle.get('last_match'));
      }
    );

    it('should set pending_last_match to empty if ongoing is not 0 ' +
      'for powerup cb', function () {
        puzzle.set({
          last_match: [1],
          pending_last_match: [1, 2],
          ongoing: 1
        }, true);
        puzzle.onShuffleGenerate(true);
        assert.deepEqual([1], puzzle.get('last_match'));
        assert.deepEqual([], puzzle.get('pending_last_match'));
      }
    );

    it('should increment shuffle if memory', function (done) {
        var user = GC.app.user,
          cache = user.increment;

        puzzle.set({
          last_match: [1],
          pending_last_match: [1, 2],
          ongoing: 1,
          type: 'memory'
        }, true);
        user.increment = function (item) {
          if (item === 'free_shuffle') {
            user.increment = cache;
            done();
          }
        };
        puzzle.onShuffleGenerate(true);
      }
    );
  });

  describe('shuffle()', function () {
    it('should invoke assignFaces with randomised array for powerup ' +
      'usage', function (done) {
        var faces_before = [],
          ctr = 0,
          cache = puzzle.assignFaces,
          cache_shuffle = puzzle.onShuffleGenerate;

        puzzle.generateSolvableGame = function () {
          return false;
        };

        puzzle.loadPuzzle({
          layout: [
            [2, 0, 0], [4, 0, 0], [2, 2, 0],
            [4, 2, 0], [6, 0, 0], [6, 2, 0],
            [0, 1, 0], [8, 1, 0]
          ],
          id: 'pi',
          families: families,
          init: []
        });

        puzzle.eachCell(function (model) {
          model.set('face', ctr++);
          model.isLock = function () {
            return false;
          };
          faces_before.push(model.get('face'));
        });
        puzzle.assignFaces = function (tile_arr) {
          puzzle.assignFaces = cache;
          assert.strictEqual(tile_arr.length, ctr);
          return Promise.resolve(true);
        };

        puzzle.onShuffleGenerate = function (val) {
          puzzle.onShuffleGenerate = cache_shuffle;
          done(val === true ? undefined : 'error');
        };
        puzzle.shuffle();
      }
    );

    it('should shuffle tile faces', function () {
      var faces_before = [],
        faces_after = [],
        ctr = 0;

      puzzle.generateSolvableGame = function () {
        return false;
      };

      puzzle.loadPuzzle({layout: [[2, 0, 0], [4, 0, 0], [2, 2, 0],
                                  [4, 2, 0], [6, 0, 0], [6, 2, 0],
                                  [0, 1, 0], [8, 1, 0]],
                        id: 'pi',
                        families: families,
                        init: []
                      });

      puzzle.eachCell(function (model) {
        model.set('face', ctr++);
        model.isLock = function () {
          return false;
        };
        faces_before.push(model.get('face'));
      });

      puzzle.shuffle();

      puzzle.eachCell(function (model) {
        faces_after.push(model.get('face'));
      });

      assert.deepEqual(faces_before, _.sortBy(faces_after));
      assert.equal(ctr, faces_after.length);
    });

    it('should emit change-face when a solvable puzzle ' +
      'is created after shuffling', function () {
      var ctr = 0;

      puzzle.loadPuzzle({layout: [[2, 0, 0], [4, 0, 0], [2, 2, 0],
                            [4, 2, 0], [6, 0, 0], [6, 2, 0],
                            [0, 1, 0], [8, 1, 0]],
                  id: 'pi',
                  families: families,
                  init: []
                });

      puzzle.generateSolvableGame = function () {
        return true;
      };

      puzzle.eachCell(function (model) {
        model.on('change-face', function () {
          ctr++;
        });
      });

      puzzle.shuffle(true);

      assert.strictEqual(ctr, 8);
    });

    it('should reset last_match', function (done) {
      puzzle.generateSolvableGame = function () {
        return true;
      };

      puzzle.once('change:last_match', function () {
        done();
      });

      puzzle.set('last_match', 'test');
      puzzle.shuffle(true);

      assert.deepEqual([], puzzle.get('last_match'));
    });

    it('should unset tile_selected', function () {
      puzzle.generateSolvableGame = function () {
        return true;
      };
      puzzle.set('tile_selected', {
        get: function () {
          return true;
        },
        set: function () {}
      });
      puzzle.shuffle();

      assert.strictEqual(null, puzzle.get('tile_selected'));
    });

    it('should emit shuffle-animate for each tile model ', function (done) {
      puzzle.generateSolvableGame = function () {
        return false;
      };

      puzzle.loadPuzzle({layout: [[2, 0, 0]],
                        id: 'pi',
                        families: families,
                        init: []
                      });

      puzzle.eachCell(function (model) {
        model.once('shuffle-animate', done);
      });

      puzzle.shuffle();
    });

    it('should not emit shuffle-animate for each tile model, silent true',
      function (done) {
      puzzle.generateSolvableGame = function () {
        return false;
      };

      puzzle.loadPuzzle({layout: [[2, 0, 0]],
                        id: 'pi',
                        families: families,
                        init: []
                      });

      puzzle.eachCell(function (model) {
        model.once('shuffle-animate', function () {
          done('error');
        });
      });

      puzzle.shuffle(true);
      done();
    });

    it('should test calling tiles-update event', function (done) {
      puzzle.generateSolvableGame = function () {
        return true;
      };
      puzzle.on('tiles-update', function () {
        done();
      });
      puzzle.shuffle();
    });

    it('should test calling powerup-applied event', function () {
      puzzle.generateSolvableGame = function () {
        return true;
      };
      puzzle.once('powerup-applied', function (powerup) {
        assert.strictEqual(powerup, 'shuffle');
      });
      puzzle.shuffle();
    });

    it('should test not calling powerup-applied event, silent true',
      function (done) {
      puzzle.generateSolvableGame = function () {
        return true;
      };
      puzzle.once('powerup-applied', function () {
        done('error');
      });
      puzzle.shuffle(true);
      done();
    });

    it('should push real_face if tile is joker', function (done) {
      var cache = puzzle.eachCell,
        generate_cache = puzzle.generateSolvableGame;

      puzzle.eachCell = function (cb) {
        var tiles = [
          {
            isJoker: function () {
              return true;
            },
            get: function (key) {
              if (key === 'real_face') {
                done();
              }
            },
            isGoldenTile: function () {
              return false;
            },
            isLock: function () {
              return false;
            }
          },
          {
            isJoker: function () {
              return false;
            },
            get: function () {},
            isGoldenTile: function () {
              return false;
            },
            isLock: function () {
              return false;
            }
          }
        ];

        _.find(tiles, function (model) {
          cb(model);
        });
      };
      puzzle.generateSolvableGame = function () {
      };
      puzzle.shuffle(true);
      puzzle.generateSolvableGame = generate_cache;
      puzzle.eachCell = cache;
    });
  });

  describe('onShuffleFailed()', function () {
    it('should emit load-failed', function (done) {
      var cache = puzzle.emit;

      puzzle.emit = function (event) {
        puzzle.emit = cache;
        assert.strictEqual(event, 'load-failed');
        done();
      };
      puzzle.onShuffleFailed();
    });
  });

  describe('shuffleAsync()', function () {
    it('should call this.get', function (done) {
      var cache = puzzle.get;

      puzzle.get = function (event) {
        puzzle.get = cache;
        assert.strictEqual(event, 'shuffle_faces');
        done();
      };
      puzzle.shuffleAsync();
    });

    it('should call assignFaces', function (done) {
      var tile_faces = ['f0_8', 'f0_8', 'f2_8', 'f3_3',
        'f4_3', 'f5_2', 'f6_2'];

      puzzle.assignFaces = function () {
        done();
        return Promise.resolve(true);
      };
      puzzle.shuffleAsync(tile_faces);
    });
  });

  describe('_createLoadArray()', function () {
    it('should create success', function (done) {
      var result_data = {
          ms: 0,
          id: 'test',
          type: 'shuffle',
          success: true,
          attempts: 12,
          tiles_placing: 14
        },
        user = GC.app.user;

      user.set('puzzle_loads', []);
      puzzle.set('tiles_placings', 14);

      puzzle.createLoadArray(true, 'shuffle', 12);
      assert.deepEqual(_.omit(user.get('puzzle_loads')[0], 'time'),
        result_data);
      done();
    });

    it('should create fail', function (done) {
      var result_data = {
          ms: 0,
          id: 'test',
          type: 'shuffle',
          success: false,
          attempts: 10,
          tiles_placing: 11
        },
        user = GC.app.user;

      user.set('puzzle_loads', [{}, {}, {}, {}, {}]);
      puzzle.set('tiles_placings', 11);

      puzzle.createLoadArray(false, 'shuffle', 10);
      assert.deepEqual(_.omit(user.get('puzzle_loads')[0], 'time'),
        result_data);
      done();
    });

    it('should create fail', function (done) {
      var result_data = {
          ms: 0,
          id: 'test',
          type: 'load',
          success: false,
          attempts: 2,
          tiles_placing: 11
        },
        user = GC.app.user;

      user.set('puzzle_loads', [{}, {}, {}]);
      puzzle.set('tiles_placings', 11);

      puzzle.createLoadArray(false, 'load', 2);
      assert.deepEqual(_.omit(user.get('puzzle_loads')[0], 'time'),
        result_data);
      done();
    });
  });

  describe('isSolvableLayout()', function () {
    it('should return true if more than one open tile', function () {
      var tile_a = new TileModel(),
        tile_b = new TileModel();

      tile_a.set({
        z: 1,
        x: 0,
        y: 0
      });
      tile_b.set({
        z: 3,
        x: 0,
        y: 0
      });
      puzzle.set({
        tile_per_match: 2,
        tiles: [null, [[tile_a]], null, [[tile_b]]]
      });
      assert.deepEqual(puzzle.isSolvableLayout(), true);
    });

    it('should return false if one open tile only' +
      'x axis', function () {
        var tile_a = new TileModel(),
          tile_b = new TileModel();

        tile_a.set({
          z: 1,
          x: 1,
          y: 0
        });
        tile_b.set({
          z: 2,
          x: 1,
          y: 0
        });

        puzzle.set('tiles', [null, [null, [tile_a]], null, [[tile_b]]]);
        assert.deepEqual(puzzle.isSolvableLayout(), false);
      }
    );
  });

  describe('generateSolvableLayout()', function () {
    it('should update tiles property by spreading tiles around current x ,' +
      'y cordnates', function () {
        var tile_nos = 4,
          xy = 10,
          model_arr = [],
          tiles;

        _.times(tile_nos, function (idx) {
          if (!model_arr[idx]) {
            model_arr[idx] = [];
          }

          if (!model_arr[idx][xy]) {
            model_arr[idx][xy] = [];
          }
          model_arr[idx][xy][xy] = new TileModel({
            x: 10,
            y: 10,
            z: idx
          });
        });
        puzzle.set({
          tiles: model_arr,
          tile_count: tile_nos,
          columns: 12,
          rows: 12,
          depth: 4,
          tile_per_match: 2
        });

        puzzle.generateSolvableLayout();
        tiles = puzzle.get('tiles');
        assert.deepEqual(tiles[0][7][7].getCoordinates(), [7, 7, 0]);
        assert.deepEqual(tiles[0][10][7].getCoordinates(), [10, 7, 0]);
        assert.deepEqual(tiles[0][7][10].getCoordinates(), [7, 10, 0]);
        assert.deepEqual(tiles[0][10][10].getCoordinates(), [10, 10, 0]);
      }
    );
    describe('initPosition()', function () {
      it('should set init x, y pos as 0 if calculated value goes beyond ' +
        'screen', function () {
          var tile_nos = 4,
            xy = 10,
            model_arr = [];

          _.times(tile_nos, function (idx) {
            if (!model_arr[idx]) {
              model_arr[idx] = [];
            }

            if (!model_arr[idx][xy]) {
              model_arr[idx][xy] = [];
            }
            model_arr[idx][xy][xy] = new TileModel({
              x: 2,
              y: 2,
              z: idx
            });
          });
          puzzle.set({
            tiles: model_arr,
            tile_count: tile_nos,
            columns: 12,
            rows: 12,
            depth: 4,
            tile_per_match: 2
          });
          puzzle.generateSolvableLayout();

          puzzle._initPosition(2, 2);
          assert.strictEqual(puzzle._pos.x, 0);
          assert.strictEqual(puzzle._pos.y, 0);
        }
      );
    });
  });

  describe('hint()', function () {
    it('should return emit highlight for two open tiles', function () {
      var ctr = 0,
        tiles,
        cache = sounds.play;

      puzzle.loadPuzzle({layout: [[0, 0, 0], [2, 0, 0], [0, 2, 0],
                                  [2, 2, 0], [8, 1, 0], [1, 1, 1]],
                        id: 'pi',
                        families: families,
                        init: []
                      });

      tiles = puzzle.get('tiles');

      tiles[1][1][1].once('highlight', function () {
        ctr++;
      });

      tiles[0][8][1].once('highlight', function () {
        ctr++;
      });

      sounds.play = function (name) {
        sounds.play = cache;
        if (name === 'hint') {
          ctr++;
        }
      };
      puzzle.hint();
      assert.equal(3, ctr);
    });

    it('should highlight the matching tile for half hint', function (done) {
      var hinted_tiles, tiles;

      puzzle.loadPuzzle(puzzle_data);
      tiles = puzzle.get('tiles');
      hinted_tiles = [tiles[0][2][0], tiles[0][8][1]];
      tiles[0][8][1].once('highlight', done);

      puzzle.get = function (props) {
        if (props === 'hinted') {
          return hinted_tiles;
        } else if (props === 'tile_per_match') {
          return 2;
        } else if (props === 'tile_selected') {
          return null;
        }
      };
      puzzle.hint(true);
    });

    it('should hightlight the pair of the other ' +
    'matching tiles in case of half hint', function (done) {
      var tiles, cache, hinted_tile, paired_tile, model_arr, i = 0,
        cache_isMatchable = puzzle.isMatchable;

      puzzle.loadPuzzle(puzzle_data);
      tiles = puzzle.get('tiles');
      hinted_tile = [tiles[0][2][0]];

      model_arr = [
          {
            get: function () {
              return 'f2_s';
            },
            emit: function (name, cb) {
              if (cb) {
                cb();
              }
            },
            set: function () {},
            getNeighbours: function () {}
          },
          {
            get: function () {
              return 'f3_s';
            },
            emit: function (name, cb) {
              if (name === 'highlight') {
                i++;
              }
              if (cb) {
                cb();
              }
            },
            set: function () {},
            getNeighbours: function () {}
          },
          {
            get: function () {
              return 'f3_s';
            },
            emit: function (name, cb) {
              if (name === 'highlight') {
                i++;
              }
              if (cb) {
                cb();
              }
            },
            set: function () {},
            getNeighbours: function () {}
          }
        ];

      cache = puzzle.tileFree;

      puzzle.isMatchable = function () {
        return true;
      };
      puzzle.get = function (props) {
        if (props === 'hinted') {
          return hinted_tile;
        } else if (props === 'tile_per_match') {
          return 2;
        }
      };
      puzzle.eachCell = function (cb) {
        _.each(model_arr, function (model) {
          cb(model);
        });
      };
      paired_tile = _.first(_.shuffle(model_arr)[0], 2);
      puzzle.hint(true);
      assert.strictEqual(i, 1);
      puzzle.isMatchable = cache_isMatchable;
      done();
    });

    it('should  emit powerup-applied for two open tiles', function () {
      puzzle.loadPuzzle({layout: [[0, 0, 0], [2, 0, 0], [0, 2, 0],
                                  [2, 2, 0], [8, 1, 0], [1, 1, 1]],
                        id: 'pi',
                        families: families,
                        init: []
                      });

      puzzle.once('powerup-applied', function (powerup) {
        assert.strictEqual(powerup, 'hint');
      });
      puzzle.hint();
    });
  });

  describe('selectTileForHalfHint()', function () {
    var data = [{
        emit: function () {}
      }, {
        emit: function () {}
      }];

    it('should call setHintAsSelected with hinted', function (done) {
      var cache = puzzle.setHintAsSelected,
        tile_per_match = puzzle.get('tile_per_match'),
        hinted = data;

      puzzle.set('tile_per_match', 2);

      puzzle.setHintAsSelected = function (data) {
        puzzle.setHintAsSelected = cache;
        assert.deepEqual(hinted[1], data);
        puzzle.set('tile_per_match', tile_per_match);
        done();
      };

      puzzle.selectTileForHalfHint(hinted);
    });

    it('should call setHintAsSelected with pairs', function (done) {
      var cache = puzzle.setHintAsSelected,
        tile_per_match = puzzle.get('tile_per_match'),
        pairs = data;

      puzzle.set('tile_per_match', 2);

      puzzle.setHintAsSelected = function (data) {
        puzzle.setHintAsSelected = cache;
        assert.deepEqual(pairs[0], data);
        puzzle.set('tile_per_match', tile_per_match);
        done();
      };

      puzzle.selectTileForHalfHint([], pairs);
    });

    it('should not call setHintAsSelected', function (done) {
      var cache = puzzle.setHintAsSelected,
        tile_per_match = puzzle.get('tile_per_match');

      puzzle.set('tile_per_match', 2);

      puzzle.setHintAsSelected = function () {
        done('error');
      };

      puzzle.selectTileForHalfHint([], null);
      puzzle.setHintAsSelected = cache;
      puzzle.set('tile_per_match', tile_per_match);
      done();
    });
  });

  describe('autoHalfHint()', function () {
    it('should call the timer for auto half hint ' +
    'every X seconds', function (done) {
      var cache = dh_timer.register;

      puzzle.loadPuzzle(puzzle_data);
      dh_timer.register = function (val, cb, interval) {
        if (val === timers[1]) {
          dh_timer.register = cache;
          assert.strictEqual(5000, interval);
          done();
        }
      };
      puzzle.autoHalfHint();
    });
    it('should call half_hint with value as true if mode is ' +
      'null', function (done) {
        var cache = puzzle.halfHint,
          cache_timer = dh_timer.register;

        puzzle.loadPuzzle(puzzle_data);

        puzzle.halfHint = function (val) {
          puzzle.halfHint = cache;
          if (val) {
            done();
          }
        };
        dh_timer.register = function (val, cb, interval) {
          if (val === 'auto_hint') {
            dh_timer.register = cache_timer;
            assert.strictEqual(5000, interval);
            cb();
          }
        };
        puzzle.set('mode', null);
        puzzle.autoHalfHint();
      }
    );

    it('shouldnt call half_hint with value as true if mode is ' +
      'not null', function (done) {
        var cache = puzzle.halfHint,
          cache_timer = dh_timer.register;

        puzzle.loadPuzzle(puzzle_data);

        puzzle.halfHint = function () {
          done('error');
        };
        dh_timer.register = function (val, cb, interval) {
          if (val === 'auto_hint') {
            dh_timer.register = cache_timer;
            assert.strictEqual(5000, interval);
            cb();
          }
        };
        puzzle.set('mode', 'open_slot');
        puzzle.autoHalfHint();
        puzzle.halfHint = cache;
        done();
      }
    );
  });

  describe('halfHint()', function () {
    it('should call the hint with parameter as half', function (done) {
      puzzle.hint = function (half) {
        assert.strictEqual(true, half);
        done();
      };
      puzzle.halfHint();
    });

    it('should play sound half hint', function (done) {
      var cache = sounds.play;

      puzzle.hint = function () {};
      sounds.play = function (name) {
        sounds.play = cache;
        done(name === 'half_hint' ? undefined : 'error');
      };
      puzzle.halfHint();
    });

    it('should emit powerup-applied in case of half hint', function (done) {
      var cache = puzzle.emit;

      puzzle.loadPuzzle(puzzle_data);
      puzzle.emit = function (name, val) {
        if (name === 'powerup-applied' && val === 'half_hint') {
          puzzle.emit = cache;
          done();
        }
      };
      puzzle.halfHint(false);
    });

    it('should not emit powerup-applied in case ' +
      'of auto half hint', function (done) {
      var cache = puzzle.emit;

      puzzle.loadPuzzle(puzzle_data);
      puzzle.emit = function (name, val) {
        if (name !== 'powerup-applied' && val !== 'half_hint') {
          puzzle.emit = cache;
          done();
        }
      };
      puzzle.halfHint(true);
    });
  });

  describe('magnet()', function () {
    it('should set tile_selected null first', function () {
      var cache = puzzle.set;

      puzzle.loadPuzzle({layout: [[0, 0, 0], [2, 0, 0], [0, 2, 0],
                                  [2, 2, 0], [8, 1, 0], [1, 1, 1]],
                        id: 'pi',
                        families: families,
                        init: []
                      });

      puzzle.set = function (key, val) {
        puzzle.set = cache;
        if (key === 'tile_selected') {
          assert.strictEqual(val, null);
        }
      };
      puzzle.magnet();
    });

    it('should unset tile_selected', function () {
      var cache = puzzle.unset;

      puzzle.loadPuzzle({layout: [[0, 0, 0], [2, 0, 0], [0, 2, 0],
                                  [2, 2, 0], [8, 1, 0], [1, 1, 1]],
                        id: 'pi',
                        families: families,
                        init: []
                      });

      puzzle.unset = function (key) {
        puzzle.unset = cache;
        assert.strictEqual(key, 'tile_selected');
      };
      puzzle.magnet();
    });

    it('should add listener for change:tile_selected', function (done) {
      var cache = puzzle.unset;

      puzzle.loadPuzzle({layout: [[0, 0, 0], [2, 0, 0], [0, 2, 0],
                                  [2, 2, 0], [8, 1, 0], [1, 1, 1]],
                        id: 'pi',
                        families: families,
                        init: []
                      });

      puzzle.applyMagnet = function () {
        puzzle.unset = cache;
        done();
      };
      puzzle.magnet();
      puzzle.emit('change:tile_selected');
    });
  });

  describe('applyMagnet()', function () {
    it('should emit powerup-applied with false', function () {
      puzzle.loadPuzzle({layout: [[0, 0, 0], [2, 0, 0], [0, 2, 0],
                                  [2, 2, 0], [8, 1, 0], [1, 1, 1]],
                        id: 'pi',
                        families: families,
                        init: []
                      });

      puzzle.on('powerup-applied', function (powerup, applied) {
        assert.strictEqual(powerup, 'magnet');
        assert.strictEqual(applied, false);
      });
      puzzle.applyMagnet(null);
    });

    it('should return if selected match current tile', function () {
      var tile;

      puzzle.loadPuzzle({layout: [[0, 0, 0]],
                        id: 'pi',
                        families: families,
                        init: []
                      });

      tile = puzzle.get('tiles');
      puzzle.tileMatched = function (selected, match) {
        puzzle.tileMatched = cache;
        assert.strictEqual(match, null);
      };
      puzzle.applyMagnet(tile[0][0][0]);
    });

    it('should return if not model', function () {
      var tile;

      puzzle.loadPuzzle({layout: [[0, 0, 0], [0, 0, 1]],
                        id: 'pi',
                        families: families,
                        init: []
                      });

      tile = puzzle.get('tiles');
      puzzle.tileMatched = function (selected, match) {
        puzzle.tileMatched = cache;
        assert.strictEqual(match, null);
      };
      tile[1][0][0] = null;
      puzzle.applyMagnet(tile[0][0][0]);
    });

    it('should return if selected face dnt match currene tile', function () {
      var cache = puzzle.tileMatched,
        tile;

      puzzle.loadPuzzle({layout: [[0, 0, 0], [0, 0, 1]],
                        id: 'pi',
                        families: families,
                        init: []
                      });

      tile = puzzle.get('tiles');
      puzzle.tileMatched = function (selected, match) {
        puzzle.tileMatched = cache;
        assert.strictEqual(match, null);
      };
      tile[0][0][0].set('face', 'f_0');
      tile[1][0][0].set('face', 'f_1');
      puzzle.applyMagnet(tile[0][0][0]);
    });

    it('should return if open tile found', function () {
      var cache = puzzle.tileMatched,
        tile;

      puzzle.loadPuzzle({layout: [[0, 0, 0], [0, 0, 1], [0, 0, 2]],
                        id: 'pi',
                        families: families,
                        init: []
                      });

      tile = puzzle.get('tiles');
      puzzle.tileMatched = function (selected, match) {
        puzzle.tileMatched = cache;
        assert.deepEqual(match, tile[1][0][0]);
      };

      puzzle.isFreeTile = function (tile) {
        if (tile.get('z') === 1) {
          return true;
        }
      };
      tile[0][0][0].set('face', 'f_0');
      tile[1][0][0].set('face', 'f_0');
      tile[2][0][0].set('face', 'f_0');
      puzzle.applyMagnet(tile[0][0][0]);
    });

    it('should push face to solved', function () {
      var cache = puzzle.tileMatched,
        cache_push = puzzle.push,
        tile;

      puzzle.loadPuzzle({layout: [[0, 0, 0], [0, 0, 1], [0, 0, 2]],
                        id: 'pi',
                        families: families,
                        init: []
                      });

      tile = puzzle.get('tiles');
      puzzle.tileMatched = function () {
        puzzle.tileMatched = cache;
      };

      puzzle.push = function (key, value) {
        if (key === 'solved') {
          assert.strictEqual(value, 'f_0');
          puzzle.push = cache_push;
        }
      };

      puzzle.isFreeTile = function () {
        return false;
      };
      tile[0][0][0].set('face', 'f_0');
      tile[1][0][0].set('face', 'f_0');
      tile[2][0][0].set('face', 'f_0');
      puzzle.applyMagnet(tile[0][0][0]);
    });

    it('should call tile match', function (done) {
      var cache = puzzle.tileMatched,
        tile;

      puzzle.loadPuzzle({layout: [[0, 0, 0], [0, 0, 1], [0, 0, 2]],
                        id: 'pi',
                        families: families,
                        init: []
                      });

      tile = puzzle.get('tiles');
      puzzle.tileMatched = function () {
        puzzle.tileMatched = cache;
        done();
      };

      puzzle.isFreeTile = function () {
        return false;
      };
      tile[0][0][0].set('face', 'f_0');
      tile[1][0][0].set('face', 'f_0');
      tile[2][0][0].set('face', 'f_0');
      puzzle.applyMagnet(tile[0][0][0]);
    });

    it('should emit powerup-applied', function (done) {
      var cache = puzzle.tileMatched,
        tile;

      puzzle.loadPuzzle({layout: [[0, 0, 0], [0, 0, 1], [0, 0, 2]],
                        id: 'pi',
                        families: families,
                        init: []
                      });

      tile = puzzle.get('tiles');
      puzzle.tileMatched = function () {
        puzzle.tileMatched = cache;
      };

      puzzle.isFreeTile = function () {
        return false;
      };
      tile[0][0][0].set('face', 'f_0');
      tile[1][0][0].set('face', 'f_0');
      tile[2][0][0].set('face', 'f_0');
      puzzle.on('powerup-applied', function () {
        done();
      });
      puzzle.applyMagnet(tile[0][0][0]);
    });

    it('should emit tiles-matched', function (done) {
      var cache = puzzle.tileMatched,
        tile;

      puzzle.loadPuzzle({layout: [[0, 0, 0], [0, 0, 1], [0, 0, 2]],
                        id: 'pi',
                        families: families,
                        init: []
                      });

      tile = puzzle.get('tiles');
      puzzle.tileMatched = function () {
        puzzle.tileMatched = cache;
      };

      puzzle.isFreeTile = function () {
        return false;
      };
      tile[0][0][0].set('face', 'f_0');
      tile[1][0][0].set('face', 'f_0');
      tile[2][0][0].set('face', 'f_0');
      puzzle.on('tiles-matched', function () {
        done();
      });
      puzzle.applyMagnet(tile[0][0][0]);
    });
  });

  describe('getOccupiedColumnsRows', function () {
    it('should get left most tile with higest z', function () {
      var data;

      puzzle.loadPuzzle({layout: [
            [1, 1, 1], [2, 1, 1],
            [2, 1, 1], [2, 2, 1],
            [1, 1, 2], [2, 1, 2],
            [2, 1, 2], [2, 2, 2]
          ],
          id: 'pi',
          families: families,
          init: []
        });
      data = puzzle.getOccupiedColumnsRows();
      assert.strictEqual(data.x.min.get('x'), 1);
      assert.strictEqual(data.x.min.get('z'), 2);
    });

    it('should get right most tile with lowest z', function () {
      var data;

      puzzle.loadPuzzle({layout: [
            [1, 1, 1], [2, 1, 1],
            [2, 1, 1], [2, 2, 1],
            [1, 1, 2], [2, 1, 2],
            [2, 1, 2], [2, 2, 2]
          ],
          id: 'pi',
          families: families,
          init: []
        });
      data = puzzle.getOccupiedColumnsRows();
      assert.strictEqual(data.x.max.get('x'), 2);
      assert.strictEqual(data.x.max.get('z'), 1);
    });

    it('should get top most tile with higest z', function () {
      var data;

      puzzle.loadPuzzle({layout: [
            [1, 1, 1], [2, 1, 1],
            [2, 1, 1], [2, 2, 1],
            [1, 1, 2], [2, 1, 2],
            [2, 1, 2], [2, 2, 2]
          ],
          id: 'pi',
          families: families,
          init: []
        });
      data = puzzle.getOccupiedColumnsRows();
      assert.strictEqual(data.y.min.get('y'), 1);
      assert.strictEqual(data.y.min.get('z'), 2);
    });

    it('should get bottom most tile with lowest z', function () {
      var data;

      puzzle.loadPuzzle({layout: [
            [1, 1, 1], [2, 1, 1],
            [2, 1, 1], [2, 2, 1],
            [1, 1, 2], [2, 1, 2],
            [2, 1, 2], [2, 2, 2]
          ],
          id: 'pi',
          families: families,
          init: []
        });
      data = puzzle.getOccupiedColumnsRows();
      assert.strictEqual(data.y.max.get('y'), 2);
      assert.strictEqual(data.y.max.get('z'), 1);
    });

    it('should return z of bottom right', function () {
      var data;

      puzzle.loadPuzzle({layout: [
            [1, 1, 1], [2, 1, 1],
            [2, 1, 1], [2, 2, 1],
            [1, 1, 2], [2, 1, 2],
            [2, 1, 2], [2, 2, 2]
          ],
          id: 'pi',
          families: families,
          init: []
        });
      data = puzzle.getOccupiedColumnsRows();
      assert.strictEqual(data.y.depth, 2);
    });

    it('should return z of top left tile', function () {
      var data;

      puzzle.loadPuzzle({layout: [
            [1, 1, 1], [2, 1, 1],
            [2, 1, 1], [2, 2, 1],
            [2, 1, 2], [2, 2, 2]
          ],
          id: 'pi',
          families: families,
          init: []
        });
      data = puzzle.getOccupiedColumnsRows();
      assert.strictEqual(data.x.depth, 1);
    });
  });

  describe('swoosh()', function () {
    it('should set tile_selected null first', function () {
      var cache = puzzle.set,
        count = 0;

      puzzle.loadPuzzle({layout: [[0, 0, 0], [2, 0, 0], [0, 2, 0],
                                  [2, 2, 0], [8, 1, 0], [1, 1, 1]],
                        id: 'pi',
                        families: families,
                        init: []
                      });

      puzzle.set = function (key, val) {
        puzzle.set = cache;
        if (count === 0 && key === 'tile_selected') {
          assert.strictEqual(val, null);
        }
        count++;
      };
      puzzle.swoosh();
    });

    it('should not set last_match null first', function () {
      var cache = puzzle.set,
        cache_sa = puzzle.swooshAnimate,
        count = 0;

      puzzle.loadPuzzle({layout: [[0, 0, 0], [2, 0, 0], [0, 2, 0],
                                  [2, 2, 0], [8, 1, 0], [1, 1, 1]],
                        id: 'pi',
                        families: families,
                        init: []
                      });

      puzzle.set = function (key) {
        if (count === 1) {
          assert.notEqual(key, 'last_match');
        }
        count++;
      };
      puzzle.swooshAnimate = function () {
        puzzle.swooshAnimate = cache_sa;
      };
      puzzle.swoosh();
      puzzle.set = cache;
    });

    it('should set open tiles to swosh pairs', function () {
      var cache = puzzle.set,
        cache_free = puzzle.isFreeTile;

      puzzle.loadPuzzle({layout: [[0, 0, 0], [2, 0, 0]],
                        id: 'pi',
                        families: families,
                        init: []
                      });

      puzzle.isMatchable = function () {
        return true;
      };
      tile = puzzle.get('tiles');
      tile[0][0][0].set('face', 'f_1');
      tile[0][2][0].set('face', 'f_1');
      puzzle.set = function (key, val) {
        if (key === 'swoosh_pairs') {
          assert.deepEqual(val, [[tile[0][0][0], tile[0][2][0]]]);
        }
      };
      puzzle.swooshAnimate = function () {};
      puzzle.swoosh();
      puzzle.set = cache;
      puzzle.isFreeTile = cache_free;
    });
    it('should checkGoals if golden tile has been matched', function () {
      var cache_goals = puzzle.checkGoals,
        cache_free = puzzle.isMatchable;

      puzzle.loadPuzzle({
        layout: [[0, 0, 0], [2, 0, 0]],
        id: 'pi',
        families: families,
        init: []
      });

      puzzle.isMatchable = function () {
        return true;
      };
      tile = puzzle.get('tiles');
      tile[0][0][0].set('face', 'f_golden');
      tile[0][2][0].set('face', 'f_golden');
      puzzle.checkGoals = function (param) {
        puzzle.checkGoals = cache_goals;
        assert.strictEqual(param, 'golden_tile');
      };
      puzzle.swoosh();
      puzzle.isMatchable = cache_free;
    });
    it('should set swoosh_pairs', function () {
      var count = 0,
        cache = puzzle.set;

      puzzle.loadPuzzle(puzzle_data);
      puzzle.set = function (key) {
        if (count === 2) {
          assert.strictEqual(key, 'swoosh_pairs');
        }
        count++;
      };
      puzzle.swooshAnimate = function () {};
      puzzle.swoosh();
      puzzle.set = cache;
    });

    it('should call swooshAnimate', function (done) {
      var cache = puzzle.swooshAnimate;

      puzzle.swooshAnimate = function () {
        puzzle.swooshAnimate = cache;
        done();
      };
      puzzle.loadPuzzle(puzzle_data);
      puzzle.swoosh();
    });

    it('should call getPairWithJoker', function (done) {
      var cache = puzzle.getPairWithJoker,
        tile_free_cache = puzzle.isFreeTile,
        swoosh_animate_cache = puzzle.swooshAnimate;

      puzzle.isFreeTile = function () {
        return false;
      };

      puzzle.getPairWithJoker = function () {
        puzzle.getPairWithJoker = cache;
        done();
        return [[1, 3, 4], [1, 3, 4]];
      };
      puzzle.loadPuzzle(puzzle_data);
      puzzle.swooshAnimate = function () {
        puzzle.swooshAnimate = swoosh_animate_cache;
      };
      puzzle.swoosh();
      puzzle.isFreeTile = tile_free_cache;
    });
  });

  describe('getNoOfLocks', function () {
    it('should return number of lock', function () {
      puzzle.set('lock_keys', [[0, 1, 1], [1, 1, 1], [2, 1, 1]]);
      assert.strictEqual(puzzle.getNoOfLocks(), 3);
    });
  });

  describe('swooshAnimate()', function () {
    it('should not emit tiles-matched if the' +
      'goal is completed', function () {
      var cache = puzzle.emit;

      puzzle.set('goal_completed', true);
      puzzle.emit = function (param) {
        assert.notStrictEqual('tiles-matched', param);
        puzzle.emit = cache;
      };
      puzzle.loadPuzzle(puzzle_data);
      puzzle.swooshAnimate();
    });

    it('should increase golden tile if found', function () {
      var model1, model2,
        i = 0,
        cache = puzzle.increment,
        position1 = [2, 2, 1],
        position2 = [2, 0, 0];

      puzzle.loadPuzzle(puzzle_data);
      model1 = puzzle.getTile(position1);
      model2 = puzzle.getTile(position2);

      puzzle.increment = function (param) {
        if (param === 'collected_golden') {
          i++;
        }
      };
      model1.isJoker = function () {
        return false;
      };
      model2.isJoker = function () {
        return false;
      };
      model1.isGoldenTile = function () {
        return true;
      };
      model2.isGoldenTile = function () {
        return true;
      };
      model1.set('face', 'f_golden');
      model2.set('face', 'f_golden');
      puzzle.set('swoosh_pairs', [[model1, model2]]);
      puzzle.swooshAnimate();
      assert.strictEqual(i, 1);
      puzzle.increment = cache;
    });

    it('should emit match-animate', function () {
      var position = [2, 2, 1],
        count = 0,
        model;

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);
      puzzle.set('swoosh_pairs', [[model, model]]);

      model.on('match-animate', function () {
        count++;
      });
      puzzle.swooshAnimate();
      assert.equal(2, count);
    });
    it('should increment swooshed count', function (done) {
      var position = [2, 2, 1],
        cache = puzzle.increment,
        model;

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);
      puzzle.set('swoosh_pairs', [[model, model]]);
      puzzle.increment = function () {
        puzzle.increment = cache;
        done();
      };
      puzzle.swooshAnimate();
    });
    it('should emit tiles-matched', function (done) {
      var position = [2, 2, 1],
        model;

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);
      puzzle.set('swoosh_pairs', [[model, model]]);

      puzzle.on('tiles-matched', function () {
        done();
      });
      puzzle.swooshAnimate();
    });

    it('should push face to solve', function () {
      var position = [2, 2, 1],
        cache = puzzle.push,
        model;

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);
      model.set('face', 'f_0');
      puzzle.set('swoosh_pairs', [[model, model]]);

      puzzle.push = function (key, value) {
        if (key === 'solved') {
          assert.strictEqual(value, 'f_0');
          puzzle.push = cache;
        }
      };
      puzzle.swooshAnimate();
    });

    it('should invoke callback once', function () {
      var position = [2, 2, 1],
        count = 0,
        cache = puzzle.afterSwooshAnimate,
        model;

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);
      puzzle.set('swoosh_pairs', [[model, model]]);
      model.on('match-animate', function (cb) {
        cb();
      });
      puzzle.afterSwooshAnimate = function () {
        count++;
      };
      puzzle.swooshAnimate();
      assert.equal(1, count);
      puzzle.afterSwooshAnimate = cache;
    });

    it('should call puzzle.updateJokerPair', function (done) {
      var position = [2, 2, 1],
        cache = puzzle.updateJokerPair,
        model;

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);
      model.isJoker = function () {
        return true;
      };
      model.set('face', 'f_0');
      puzzle.set('swoosh_pairs', [[model, model]]);
      puzzle.updateJokerPair = function () {
        puzzle.updateJokerPair = cache;
        done();
      };
      puzzle.swooshAnimate();
    });

    it('should call puzzle.updateJokerPair with pair 1', function (done) {
      var position = [2, 2, 1],
        i = 0,
        cache = puzzle.updateJokerPair,
        model;

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);
      model.isJoker = function () {
        i++;
        if (i === 1) {
          return false;
        } else {
          return true;
        }
      };
      model.set('face', 'f_0');
      puzzle.set('swoosh_pairs', [[model, model]]);
      puzzle.updateJokerPair = function (pair) {
        puzzle.updateJokerPair = cache;
        assert.deepEqual(pair, [model, model]);
        done();
      };
      puzzle.swooshAnimate();
    });

    it('should call updateLockedTiles if key', function (done) {
      var position = [2, 2, 1],
        cache = puzzle.updateLockedTiles,
        model;

      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);
      model.isJoker = function () {
        return false;
      };
      model.isKey = function () {
        return true;
      };
      model.set('face', 'f_lock_1');
      puzzle.set('swoosh_pairs', [[model, model]]);

      puzzle.updateLockedTiles = function () {
        puzzle.updateLockedTiles = cache;
        done();
      };
      puzzle.swooshAnimate();
    });
  });

  describe('afterSwooshAnimate()', function () {
    it('should invoke cleanTile', function () {
      var position = [2, 2, 1],
        count = 0,
        cache = puzzle.cleanTile,
        model;

      puzzle.cleanTile = function () {
        count++;
      };
      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);
      puzzle.afterSwooshAnimate([model, model]);
      assert.equal(2, count);
      puzzle.cleanTile = cache;
    });

    it('should call swooshAnimate', function (done) {
      var position = [2, 2, 1],
        cache = puzzle.swooshAnimate,
        cache_cl = puzzle.cleanTile,
        model;

      puzzle.cleanTile = function () {};
      puzzle.swooshAnimate = function () {
        puzzle.swooshAnimate = cache;
        done();
      };
      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);
      puzzle.set('swoosh_pairs', [[model, model]]);
      puzzle.afterSwooshAnimate([model, model]);
      puzzle.cleanTile = cache_cl;
    });

    it('should call updateGameState', function (done) {
      var position = [2, 2, 1],
        cache = puzzle.updateGameState,
        model;

      puzzle.loadPuzzle(puzzle_data);
      puzzle.cleanTile = function () {};
      puzzle.set('swoosh_pairs', []);
      puzzle.updateGameState = function () {
        puzzle.updateGameState = cache;
        done();
      };
      model = puzzle.getTile(position);
      puzzle.afterSwooshAnimate([model, model]);
    });

    it('should set listener to resize-complete if ' +
      'tile count > 0', function (done) {
        var position = [2, 2, 1],
          cache = puzzle.updateGameState,
          cache_sw = puzzle.swoosh,
          cache_open = puzzle.isOpenSlotTile,
          model;

        puzzle.loadPuzzle(puzzle_data);
        puzzle.cleanTile = function () {};
        puzzle.set('swoosh_pairs', []);
        puzzle.updateGameState = function () {
          puzzle.updateGameState = cache;
        };
        puzzle.swoosh = function () {
          puzzle.swoosh = cache_sw;
          done();
        };
        puzzle.set('swooshed_count', 2);
        model = puzzle.getTile(position);
        puzzle.isOpenSlotTile = function () {
          puzzle.isOpenSlotTile = cache_open;

          return true;
        };
        puzzle.afterSwooshAnimate([model, model]);
        puzzle.emit('resize-complete');
      }
    );

    it('shouldnt set listener to resize-complete if ' +
      'tile count == 0', function (done) {
        var position = [2, 2, 1],
          cache = puzzle.updateGameState,
          cache_sw = puzzle.swoosh,
          model;

        puzzle.loadPuzzle(puzzle_data);
        puzzle.cleanTile = function () {};
        puzzle.set('swoosh_pairs', []);
        puzzle.updateGameState = function () {
          puzzle.updateGameState = cache;
        };
        puzzle.swoosh = function () {
          puzzle.swoosh = cache_sw;
          done('error');
        };
        puzzle.set('swooshed_count', 2);
        model = puzzle.getTile(position);
        puzzle.set('tile_count', 0);
        puzzle.afterSwooshAnimate([model, model]);
        puzzle.emit('resize-complete');
        puzzle.swoosh = cache_sw;
        done();
      }
    );

    it('should emit powerup-applied', function (done) {
      var position = [2, 2, 1],
        cache = puzzle.updateGameState,
        model;

      puzzle.loadPuzzle(puzzle_data);
      puzzle.cleanTile = function () {};
      puzzle.set('swoosh_pairs', []);
      puzzle.updateGameState = function () {
        puzzle.updateGameState = cache;
      };
      puzzle.on('powerup-applied', function () {
        done();
      });
      puzzle.set('swooshed_count', 5);
      model = puzzle.getTile(position);
      puzzle.afterSwooshAnimate([model, model]);
    });

    it('should last_match be a blank array no moves made', function (done) {
      var position = [2, 2, 1],
        count = 0,
        cache = puzzle.cleanTile,
        model,
        set_cache = puzzle.set,
        get_cache = puzzle.get;

      puzzle.cleanTile = function () {
        count++;
      };

      puzzle.set('last_match', null);
      puzzle.loadPuzzle(puzzle_data);
      model = puzzle.getTile(position);
      puzzle.get = function (key) {
        if (key === 'last_match') {
          puzzle.get = get_cache;
          return null;
        }
      };
      puzzle.set = function (key, val) {
        if (key === 'last_match') {
          assert.notEqual(val, null);
          assert.strictEqual(Array.isArray(val), true);
          done();
        }
      };
      puzzle.afterSwooshAnimate([model, model]);
      assert.equal(2, count);
      puzzle.set = set_cache;
      puzzle.cleanTile = cache;
    });
  });

  describe('swap()', function () {
    it('should set tile_selected to null', function (done) {
      var cache = puzzle.set;

      puzzle.set = function (key, val) {
        if (key === 'tile_selected') {
          puzzle.set = cache;
          assert.strictEqual(val, null);
          done();
        }
      };
      puzzle.swap();
    });

    it('should set mode to swap', function (done) {
      var cache = puzzle.set;

      puzzle.set = function (key, val) {
        if (key === 'mode') {
          puzzle.set = cache;
          assert.strictEqual(val, 'swap');
          done();
        }
      };
      puzzle.swap();
    });
  });

  describe('substitute()', function () {
    it('should reduce no of families', function (done) {
      var cache_timer = dh_timer.timeout,
        cache_assign = puzzle.assignGoldenTile,
        no_of_families = 0,
        no_of_families_after_powerup = 0,
        family_counter = [];

      puzzle.assignFaces = cache[0];
      puzzle.setTiles = cache[1];
      puzzle.isFreeTile = function () {
        return true;
      };

      dh_timer.timeout = function (tag, cb) {
        cb();
      };

      puzzle.assignGoldenTile = function () {
        puzzle.assignGoldenTile = cache_assign;
      };
      puzzle.loadPuzzle({layout: [[0, 0, 0], [2, 0, 0], [0, 2, 0],
                                  [2, 2, 0], [8, 1, 0], [1, 1, 1]],
                        id: 'pi',
                        families: [{
                                      pairs: 2,
                                      types: 1
                                    },
                                    {
                                      pairs: 2,
                                      types: 1
                                    },
                                    {
                                      pairs: 2,
                                      types: 1
                                    }],
                        init: []
                      })
      .then(function () {
        puzzle.eachCellByLayer(function (model) {
          model.on('substitute-animate', function (cb) {
            cb();
          });
          if (!_.contains(family_counter, model.get('face'))) {
            family_counter.push(model.get('face'));
          }
        });
        no_of_families = family_counter.length;
        family_counter = [];
        puzzle.eachCellByLayer(function (model) {
          model.isLock = function () {
            return false;
          };
        });
        puzzle.substitute();
        puzzle.eachCellByLayer(function (model) {
          if (!_.contains(family_counter, model.get('face'))) {
            family_counter.push(model.get('face'));
          }
        });
        no_of_families_after_powerup = family_counter.length;
        done(no_of_families - 1 === no_of_families_after_powerup ?
          undefined : 'error');
        dh_timer.timeout = cache_timer;
      });
    });

    it('should change real_face property if that face is getting ' +
      'substituted', function (done) {
        var cache_timer = dh_timer.timeout;

        puzzle.assignFaces = cache[0];
        puzzle.setTiles = cache[1];
        puzzle.isFreeTile = function () {
          return true;
        };

        dh_timer.timeout = function (tag, cb) {
          cb();
        };

        puzzle.loadPuzzle({layout: [[0, 0, 0], [2, 0, 0], [0, 2, 0],
                                    [2, 2, 0], [8, 1, 0], [1, 1, 1]],
                          id: 'pi',
                          families: [{
                                        pairs: 2,
                                        types: 1
                                      },
                                      {
                                        pairs: 2,
                                        types: 1
                                      },
                                      {
                                        pairs: 2,
                                        types: 1
                                      }],
                          init: []
                        })
        .then(function () {
          var cache_getsub = puzzle.getFacesToSubstitute;

          puzzle.getFacesToSubstitute = function () {
            puzzle.getFacesToSubstitute = cache_getsub;
            return ['f1', 'f2'];
          };
          puzzle.eachCellByLayer(function (model) {
            model.set({
              face: 'f5',
              real_face: 'f2'
            });
          });

          puzzle.substitute();
          puzzle.eachCellByLayer(function (model) {
            if (model.get('real_face') === 'f2') {
              done('error');
            }
          });
          dh_timer.timeout = cache_timer;
          done();
        });
      }
    );

    it('should emit powerup applied', function (done) {
      var cache = dh_timer.timeout;

      puzzle.loadPuzzle(puzzle_data);

      puzzle.once('powerup-applied', function (name) {
        assert.strictEqual(name, 'substitute');
        done();
      });

      dh_timer.timeout = function (tag, cb) {
        cb();
      };

      puzzle.substitute();
      assert.strictEqual(null, puzzle.get('tile_selected'));
      dh_timer.timeout = cache;
    });

    it('should call getFacesToSubstitute', function (done) {
      var cache = puzzle.getFacesToSubstitute;

      puzzle.loadPuzzle(puzzle_data);

      puzzle.getFacesToSubstitute = function () {
        puzzle.getFacesToSubstitute = cache;
        done();
        return [];
      };
      puzzle.substitute();
    });

    it('should call getFacesToSubstitute twise', function (done) {
      var cache = puzzle.getFacesToSubstitute;

      puzzle.loadPuzzle(puzzle_data);

      puzzle.getFacesToSubstitute = function (unlock) {
        if (unlock) {
          return [];
        }
        puzzle.getFacesToSubstitute = cache;
        done();
        return [];
      };
      puzzle.substitute();
    });

    it('should call puzzle.changeFace', function (done) {
      var cache = puzzle.getFacesToSubstitute,
        model_array = [
          {
            get: function () {
              return 'f2_s';
            },
            emit: function (name, cb) {
              if (cb) {
                cb();
              }
            },
            set: function () {}
          },
          {
            get: function () {
              return 'f3_s';
            },
            emit: function (name, cb) {
              if (cb) {
                cb();
              }
            },
            set: function () {}
          }
        ];

      puzzle.loadPuzzle(puzzle_data);

      puzzle.eachCellByLayer = function (cb) {
        _.each(model_array, function (model) {
          cb(model);
        });
      };

      puzzle.getFacesToSubstitute = function (unlock) {
        if (unlock) {
          puzzle.getFacesToSubstitute = cache;
          return ['f2_s', 'f3_s'];
        }
      };
      puzzle.changeFace = function () {
        done();
      };
      puzzle.substitute();
    });
  });

  describe('getFacesToSubstitute()', function () {
    it('should return two visible tiles', function () {
      var faces = [],
        cache = puzzle.isFreeTile,
        each_cell_cache = puzzle.eachCell,
        model_array = [
          {
            get: function () {
              return 'f2_s';
            },
            isJoker: function () {
              return false;
            },
            isGoldenTile: function () {
              return false;
            },
            isLock: function () {
              return false;
            }
          },
          {
            get: function () {
              return 'f3_s';
            },
            isJoker: function () {
              return false;
            },
            isGoldenTile: function () {
              return false;
            },
            isLock: function () {
              return false;
            }
          }
        ];

      puzzle.isFreeTile = function () {
        return true;
      };

      puzzle.assignFaces = cache[0];
      puzzle.setTiles = cache[1];

      puzzle.eachCell = function (cb) {
        puzzle.eachCell = each_cell_cache;
        _.each(model_array, function (model) {
          cb(model);
        });
      };

      faces = puzzle.getFacesToSubstitute(true);
      assert.strictEqual(faces.length, 2);
    });

    it('should return two visible tiles', function () {
      var faces = [],
        cache = puzzle.isFreeTile,
        each_cell_cache = puzzle.eachCell,
        model_array = [
          {
            get: function () {
              return 'f2_s';
            },
            isJoker: function () {
              return false;
            },
            isGoldenTile: function () {
              return false;
            },
            isLock: function () {
              return false;
            }
          },
          {
            get: function () {
              return 'f3_s';
            },
            isJoker: function () {
              return false;
            },
            isGoldenTile: function () {
              return false;
            },
            isLock: function () {
              return false;
            }
          }
        ];

      puzzle.isFreeTile = function () {
        return false;
      };

      puzzle.assignFaces = cache[0];
      puzzle.setTiles = cache[1];

      puzzle.eachCell = function (cb) {
        puzzle.eachCell = each_cell_cache;
        _.each(model_array, function (model) {
          cb(model);
        });
      };

      faces = puzzle.getFacesToSubstitute(false);
      assert.strictEqual(faces.length, 2);
    });
  });

  describe('undo()', function () {
    var obj = [[0, 0, 0], [0, 4, 0]],
      isJoker = function () {};

    _.each(obj, function (t) {
      t.isJoker = isJoker;
    });
    beforeEach(function () {
      puzzle.set('tiles', [[]]);
      puzzle.set({
        tile_count: 0,
        last_match: [[[0, 0, 0], [0, 4, 0], 2]],
        solved: []
      });
    });

    it('should invoke render-tile for restored model', function () {
      var ctr = 0,
        cache = puzzle.changeFace;

      puzzle.on('render-tile', function () {
        ctr++;
      });

      puzzle.changeFace = function () {};

      puzzle.undo();
      assert.strictEqual(2, ctr);
      puzzle.changeFace = cache;
    });

    it('should undo the symbol order for the last tiles', function () {
      puzzle.set('jewels', ['circle', 'triangle', 'square']);
      puzzle.set('jewel_order', ['circle', 'triangle']);
      puzzle.changeFace = function () {};
      puzzle.set('last_match', [[[1, 2, 3, null, null, 'circle'],
        [1, 2, 3, null, null, 'triangle']]]);
      puzzle.undo();
      assert.strictEqual(_.isEmpty(puzzle.get('jewel_order')), true);
    });

    it('should undo the partially hidden tiles', function () {
      var count = 0,
        position = [0, 2, 0, null, null, 'circle', true];

      puzzle.cleanTile = function () {};
      puzzle.set('type', 'partial_memory');

      puzzle.loadPuzzle(puzzle_data);
      puzzle.changeFace = function () {};
      puzzle.placeTile = function () {};
      puzzle.getNeighbours = function () {};

      puzzle.set('last_match', [[position, position, null]]);

      puzzle.getTile = function () {
        return {
          set: function (param) {
            if (param === 'hidden') {
              count++;
            } else if (param === 'face_visibility') {
              count++;
            }
          },
          isJoker: function () {
            return false;
          }
        };
      };
      puzzle.undo();
      assert.strictEqual(4, count);
    });

    it('should increment undo', function () {
      var cache = puzzle.changeFace;

      puzzle.changeFace = function () {};
      puzzle.set('undo', 1);
      puzzle.undo();
      assert.strictEqual(2, puzzle.get('undo'));
      puzzle.changeFace = cache;
    });

    it('should hide there is selected tile is hidden', function (done) {
        var position = [0, 2, 0],
          model;

        puzzle.cleanTile = function () {
        };
        puzzle.loadPuzzle(puzzle_data);
        puzzle.set('type', 'memory');
        puzzle.changeFace = function () {};
        puzzle.set('last_match', [[position, position, null]]);
        model = puzzle.getTile(position);
        model.set('hidden', true);
        puzzle.set('tile_selected', model);
        model.set = function (param) {
          if (param === 'face_visibility') {
            done();
          }
        };
        puzzle.undo();
      });

    it('should not hide there is no selected tile and game' +
      ' mode is memory match', function (done) {
        var position = [0, 2, 0],
          model;

        puzzle.cleanTile = function () {
        };
        puzzle.loadPuzzle(puzzle_data);
        puzzle.set('type', 'memory');
        puzzle.changeFace = function () {};
        puzzle.set('last_match', [[position, position, null]]);
        model = puzzle.getTile(position);
        puzzle.set('tile_selected', null);
        model.set = function (param) {
          if (param === 'face_visibility') {
            done('err');
          }
        };
        puzzle.undo();
        done();
      });

    it('should invoke tiles-update for model neighbour', function () {
      var ctr = 0,
        cache = puzzle.changeFace;

      puzzle.changeFace = function () {};

      puzzle.on('tiles-update', function () {
        ctr++;
      });

      puzzle.undo();
      assert.strictEqual(1, ctr);
      puzzle.changeFace = cache;
    });

    it('should increment tile count', function () {
      var cache = puzzle.changeFace;

      puzzle.changeFace = function () {};
      puzzle.undo();
      assert.strictEqual(2, puzzle.get('tile_count'));
      puzzle.changeFace = cache;
    });

    it('should call loadTiles to to add tile model', function (done) {
      var cache = puzzle.changeFace;

      puzzle.changeFace = function () {};
      puzzle.placeTile = function () {};
      puzzle.getNeighbours = function () {};

      puzzle.getTile = function (param) {
        param.isJoker = isJoker;
        return param;
      };
      puzzle.loadTiles = function (tiles) {
        _.each(tiles, function (t) {
          t.isJoker = isJoker;
        });
        assert.deepEqual(obj, tiles);
        done();
      };
      puzzle.undo();
      puzzle.changeFace = cache;
    });

    it('should invoke placeTile to update tile dependency', function () {
      var ctr = 0,
        cache = puzzle.changeFace;

      puzzle.changeFace = function () {};
      puzzle.loadTiles = function () {};
      puzzle.getNeighbours = function () {};

      puzzle.getTile = function (param) {
        param.isJoker = isJoker;
        return param;
      };

      puzzle.placeTile = function (tile, face) {
        assert.deepEqual(obj[ctr], tile);
        assert.strictEqual(face, tile[3]);
        ctr++;
      };
      puzzle.undo();
      puzzle.changeFace = cache;
    });

    it('should unset tile_selected', function () {
      var cache = puzzle.changeFace,
        cache_defer = _.defer;

      _.defer = function (cb) {
        _.defer = cache_defer;
        cb();
      };
      puzzle.changeFace = function () {};
      puzzle.set('tile_selected', {
        get: function () {
          return true;
        },
        set: function () {}
      });
      puzzle.undo();
      assert.strictEqual(null, puzzle.get('tile_selected'));
      puzzle.changeFace = cache;
    });
    it('should reset last_match', function () {
      var cache = puzzle.changeFace,
        cache_defer = _.defer;

      _.defer = function (cb) {
        _.defer = cache_defer;
        cb();
      };
      puzzle.changeFace = function () {};
      puzzle.undo();
      assert.deepEqual([], puzzle.get('last_match'));
      puzzle.changeFace = cache;
    });

    it('should test for solved getting updated', function () {
      var cache = puzzle.changeFace;

      puzzle.changeFace = function () {};
      puzzle.set('solved', [2]);
      puzzle.undo();
      assert.deepEqual([], puzzle.get('solved'));
      puzzle.changeFace = cache;
    });

    it('should not call changeFace', function () {
      var cache = puzzle.changeFace;

      puzzle.set('last_match', [[[1, 2, 3, 'f_joker'], [1, 2, 3, 'f_joker']]]);

      puzzle.changeFace = function () {
        assert.strictEqual(true, false);
      };
      puzzle.undo();
      puzzle.changeFace = cache;
    });

    it('should call changeFace for changed tiles also', function (done) {
      var position = [0, 2, 0],
        position1 = [2, 2, 0, 'f_lock_1', 'f_real'],
        position2 = [8, 1, 0, 'f_1'],
        position3 = [8, 1, 0, 'f_2', 'f_lock'],
        cache = puzzle.changeFace,
        pivot = 1;

      puzzle.cleanTile = function () {
      };
      puzzle.loadPuzzle(puzzle_data);
      puzzle.set('type', 'memory');
      puzzle.set('last_match',
        [[position, position, position1, position2, position3]]);
      puzzle.set('tile_selected', null);
      puzzle.changeFace = function (tile, face) {
        if (_.contains(['f_real', 'f_1', 'f_2'], face)) {
          pivot++;

          if (pivot === 3) {
            puzzle.changeFace = cache;
            done();
          }
        }
      };
      puzzle.undo();
    });
  });

  describe('startTimers()', function () {
    it('should start puzzle_timer', function (done) {
      puzzle.set({
        limit_time: 1,
        time: 0
      });
      puzzle.startTimers();
      assert.strictEqual(true, !!dh_timer._listeners_interval.puzzle);
      done();
    });

    it('should emit check-complete when time >= limit_time', function (done) {
      puzzle.on('check-complete', done());

      puzzle.set({
          limit_type: 'time',
          limit: 2,
          time: 0
        });
      dh_timer.mock(2);
      puzzle.startTimers();
    });

    it('should pause timers when time = 0', function (done) {
      var cache = puzzle.pauseTimers;

      puzzle.pauseTimers = function () {
        done();
        puzzle.pauseTimers = cache;
      };
      puzzle.set({
          limit_type: 'time',
          limit: 2,
          time: 0
        });
      dh_timer.mock(2);
      puzzle.startTimers();
    });

    it('should pause timers when time > limit_time', function (done) {
      var cache = puzzle.pauseTimers;

      puzzle.pauseTimers = function () {
        done();
        puzzle.pauseTimers = cache;
      };
      puzzle.set({
          limit_type: 'time',
          limit: 2,
          time: 0
        });
      dh_timer.mock(5);
      puzzle.startTimers();
    });
  });
  describe('pauseTimers()', function () {
    it('should pause all active timers', function (done) {
      var cache = dh_timer.pause;

      dh_timer.pause = function () {
        done();
        dh_timer.pause = cache;
      };
      puzzle.pauseTimers();
    });
  });

  describe('resumeTimers()', function () {
    it('should resume all active timers', function (done) {
      var cache = dh_timer.resume;

      dh_timer.resume = function () {
        done();
        dh_timer.resume = cache;
      };
      puzzle.resumeTimers();
    });
  });

  describe('stopTimers()', function () {
    it('should unregister all active timers', function (done) {
      var cache = dh_timer.unregister;

      dh_timer.unregister = function () {
        done();
        dh_timer.unregister = cache;
      };
      puzzle.stopTimers();
    });
  });
  describe('checkComplete()', function () {
    it('should emit out-of-limit', function (done) {
      puzzle.set({
        time: 40,
        limit: 40
      }, true);
      puzzle.set('limit_type', 'time');
      puzzle.on('out-of-limit', function () {
        done();
      });
      puzzle.checkComplete();
    });

    it('should not emit out-of-limit', function (done) {
      puzzle.set({
        time: 38,
        limit_time: 40
      }, true);
      util_test.assertSignal(done, puzzle, 'out-of-limit', false);
      puzzle.checkComplete();
    });

    it('should trigger gameover', function (done) {
      var cache_score = puzzle.getScore;

      puzzle.getScore = function () {
        puzzle.getScore = cache_score;
        return 10;
      };
      puzzle.set({
        goal_completed: true
      });
      util_test.assertSignal(done, puzzle, 'game-over', true);
      puzzle.checkComplete();
    });

    it('should not trigger gameover if goal completed', function (done) {
      puzzle.set({
        goal_completed: false
      });
      util_test.assertSignal(done, puzzle, 'game-over', false);
      puzzle.checkComplete();
    });

    it('should not trigger gameover', function (done) {
      puzzle.set({
        goal_completed: false,
        pending_matches: 1,
        tile_count: 2
      });
      util_test.assertSignal(done, puzzle, 'game-over', false);
      puzzle.checkComplete();
    });

    it('should invoke cb if available', function (done) {
      puzzle.checkComplete(null, done);
    });
  });

  describe('gameOver()', function () {
    it('should call supr game over if given in arguments', function (done) {
      var cache = QuestPuzzle.prototype.gameOver,
        cache_timers = puzzle.stopTimers,
        cache_score = puzzle.getScore,
        cache_progress = puzzle.updateProgress;

      QuestPuzzle.prototype.gameOver = function () {
        QuestPuzzle.prototype.gameOver = cache;
        done();
      };
      puzzle.set('solved', []);
      puzzle.updateProgress = function () {
        puzzle.updateProgress = cache_progress;
        return;
      };

      puzzle.stopTimers = function () {
        puzzle.stopTimers = cache_timers;
      };
      puzzle.getScore = function () {
        puzzle.getScore = cache_score;
        return 10;
      };
      puzzle.gameOver(true, function () {}, true);
    });
    it('should call supr game over' +
      'even if not given in arguments', function (done) {
      var cache = QuestPuzzle.prototype.gameOver,
        cache_timers = puzzle.stopTimers,
        cache_score = puzzle.getScore,
        cache_progress = puzzle.updateProgress;

      QuestPuzzle.prototype.gameOver = function () {
        QuestPuzzle.prototype.gameOver = cache;
        done();
      };
      puzzle.set('solved', []);
      puzzle.updateProgress = function () {
        puzzle.updateProgress = cache_progress;
        return;
      };

      puzzle.stopTimers = function () {
        puzzle.stopTimers = cache_timers;
      };
      puzzle.getScore = function () {
        puzzle.getScore = cache_score;
        return 10;
      };
      puzzle.gameOver(true, function () {});
    });
    it('should call stopTimers', function (done) {
      var cache = puzzle.stopTimers,
        cache_score = puzzle.getScore,
        cache_progress = puzzle.updateProgress;

      puzzle.set('solved', []);
      puzzle.updateProgress = function () {
        puzzle.updateProgress = cache_progress;
        return;
      };

      puzzle.stopTimers = function () {
        puzzle.stopTimers = cache;
        done();
      };
      puzzle.getScore = function () {
        puzzle.getScore = cache_score;
        return 10;
      };
      puzzle.gameOver(true, function () {}, false);
    });
    it('should update stars and scores if won', function () {
      var cache_star = puzzle.getStars,
        cache_score = puzzle.getScore,
        cache_progress = puzzle.updateProgress;

      puzzle.updateProgress = function () {
        puzzle.updateProgress = cache_progress;
        return;
      };

      puzzle.getStars = function () {
        puzzle.getStars = cache_star;
        return 1;
      };
      puzzle.getScore = function () {
        puzzle.getScore = cache_score;
        return 10;
      };
      puzzle.set('solved', []);
      puzzle.gameOver(true, function () {}, false);
      assert.strictEqual(1, puzzle.get('stars'));
      assert.strictEqual(10, puzzle.get('score'));
    });

    it('shouldnt update stars if type is restart', function () {
      var cache_star = puzzle.getStars,
        cache_score = puzzle.getScore,
        cache_progress = puzzle.updateProgress;

      puzzle.updateProgress = function () {
        return;
      };

      puzzle.getStars = function () {
        return 1;
      };

      puzzle.getScore = function () {
        puzzle.getScore = cache_score;
        return 10;
      };
      puzzle.set('solved', []);
      puzzle.set('stars', 0);
      puzzle.gameOver('restart', function () {}, false);
      puzzle.updateProgress = cache_progress;
      puzzle.getStars = cache_star;
      assert.strictEqual(0, puzzle.get('stars'));
    });

    it('shouldnt increment life if type is restart & moves made', function () {
      var cache_star = puzzle.getStars,
        user = GC.app.user,
        cache_score = puzzle.getScore,
        cache_progress = puzzle.updateProgress;

      user.set('inventory_lives', 4);
      puzzle.set({
        time: 10,
        limit_time: 20,
        undo: 1,
        solved: []
      });
      puzzle.updateProgress = function () {
        return;
      };

      puzzle.getStars = function () {
        return 1;
      };

      puzzle.getScore = function () {
        puzzle.getScore = cache_score;
        return 10;
      };

      puzzle.set('solved', []);
      puzzle.set('stars', 0);
      puzzle.gameOver('quit', function () {}, false);
      puzzle.updateProgress = cache_progress;
      puzzle.getStars = cache_star;
      assert.strictEqual(4, user.get('inventory_lives'));
    });

    it('shouldnt update stars if type is quit', function () {
      var cache_star = puzzle.getStars,
        cache_score = puzzle.getScore,
        cache_progress = puzzle.updateProgress;

      puzzle.updateProgress = function () {
        return;
      };

      puzzle.getStars = function () {
        return 1;
      };

      puzzle.getScore = function () {
        puzzle.getScore = cache_score;
        return 10;
      };

      puzzle.set('stars', 0);
      puzzle.gameOver('quit', function () {}, false);
      puzzle.updateProgress = cache_progress;
      puzzle.getStars = cache_star;
      assert.strictEqual(0, puzzle.get('stars'));
    });

    it('should not update stars if fail', function (done) {
      var cache_star = puzzle.getStars,
        cache_score = puzzle.getScore,
        cache_progress = puzzle.updateProgress;

      puzzle.updateProgress = function () {
        puzzle.updateProgress = cache_progress;
        return;
      };

      puzzle.getStars = function () {
        puzzle.getStars = cache_star;
        done('error');
      };
      puzzle.getScore = function () {
        puzzle.getScore = cache_score;
        return 10;
      };
      puzzle.gameOver(false, function () {}, false);
      assert.strictEqual(undefined, puzzle.get('stars'));
      assert.strictEqual(10, puzzle.get('score'));
      done();
    });
    it('should update lives if won', function () {
      var user = GC.app.user,
        cache = puzzle.updateProgress;

      user.set('infinite_lives_remaining', 0);

      puzzle.updateProgress = function () {
        puzzle.updateProgress = cache;
        return;
      };

      user.set('inventory_lives', 4);
      puzzle.set('solved', []);
      puzzle.set('limit_type', 'time');
      puzzle.gameOver(true, function () {}, false);
      assert.strictEqual(5, user.get('inventory_lives'));
    });
    it('should update lives if didnt make any move and not out of time',
        function () {
      var user = GC.app.user,
        cache = puzzle.updateProgress;

      puzzle.updateProgress = function () {
        puzzle.updateProgress = cache;
        return;
      };

      user.set('inventory_lives', 4);
      puzzle.set({
        undo: 0,
        time: 5,
        limit: 10,
        limit_type: 'time',
        solved: []
      });
      puzzle.gameOver(false, function () {}, false);
      assert.strictEqual(5, user.get('inventory_lives'));
    });
    it('should not update lives if out of time but did make move',
        function () {
      var user = GC.app.user,
        cache = puzzle.updateProgress;

      puzzle.updateProgress = function () {
        puzzle.updateProgress = cache;
        return;
      };

      user.set('inventory_lives', 4);
      puzzle.set({
        undo: 5,
        time: 5,
        limit_time: 10,
        solved: []
      });
      puzzle.gameOver(false, function () {}, false);
      assert.strictEqual(4, user.get('inventory_lives'));
    });
    it('should not update lives if out of time and didnt make move',
      function () {
      var user = GC.app.user,
        cache = puzzle.updateProgress;

      puzzle.updateProgress = function () {
        puzzle.updateProgress = cache;
        return;
      };

      user.set('inventory_lives', 4);
      puzzle.set({
        undo: 0,
        time: 10,
        limit_time: 10,
        solved: []
      });
      puzzle.gameOver(false, function () {}, false);
      assert.strictEqual(4, user.get('inventory_lives'));
    });
    it('should call updateProgress', function (done) {
      var cache = puzzle.updateProgress;

      puzzle.updateProgress = function () {
        puzzle.updateProgress = cache;
        done();
      };
      puzzle.gameOver(false, function () {}, false);
    });
    it('should not increment life if in infinite lives',
      function () {
      var user = GC.app.user,
        cache = puzzle.updateProgress;

      puzzle.updateProgress = function () {
        puzzle.updateProgress = cache;
        return;
      };

      user.set('inventory_lives', 4);
      user.set('infinite_lives_remaining', 100);
      puzzle.set({
        undo: 0,
        time: 10,
        limit_time: 10,
        solved: []
      });
      puzzle.gameOver(true, function () {}, false);
      assert.strictEqual(4, user.get('inventory_lives'));
    });
    it('should increment life if no infinite lives but won',
      function () {
      var user = GC.app.user,
        cache = puzzle.updateProgress;

      puzzle.updateProgress = function () {
        puzzle.updateProgress = cache;
        return;
      };

      user.set('inventory_lives', 4);
      user.set('infinite_lives_remaining', 0);
      puzzle.set({
        undo: 0,
        time: 10,
        limit_time: 10,
        solved: []
      });
      puzzle.gameOver(true, function () {}, false);
      assert.strictEqual(5, user.get('inventory_lives'));
    });
    it('should not increment life if infinite life but fail',
      function () {
      var user = GC.app.user,
        cache = puzzle.updateProgress;

      puzzle.updateProgress = function () {
        puzzle.updateProgress = cache;
        return;
      };

      user.set('inventory_lives', 4);
      user.set('infinite_lives_remaining', 0);
      puzzle.set({
        undo: 0,
        time: 10,
        limit_time: 10,
        solved: []
      });
      puzzle.gameOver(false, function () {}, false);
      assert.strictEqual(4, user.get('inventory_lives'));
    });
    it('should emit out-of-lives', function (done) {
      var user = GC.app.user,
        cache = puzzle.updateProgress,
        cache_emit = event_manager.emit;

      puzzle.updateProgress = function () {
        puzzle.updateProgress = cache;
        return;
      };

      user.set('inventory_lives', 0);
      user.set('infinite_lives_remaining', 0);
      puzzle.set({
        undo: 0,
        time: 10,
        limit_time: 10,
        solved: []
      });

      event_manager.emit = function (event) {
        if (event === 'out-of-lives') {
          event_manager.emit = cache_emit;
          done();
        }
      };
      puzzle.gameOver(false, function () {}, false);
    });
  });

  describe('getStars()', function () {
    it('should test stars 1', function () {
      var cache_score = puzzle.getScore;

      puzzle.getScore = function () {
        puzzle.getScore = cache_score;
        return 10;
      };
      puzzle.loadPuzzle(puzzle_data);
      puzzle.set('limit_type', 'time');
      puzzle.set('limit', 60);
      puzzle.set('time', 60);
      puzzle.gameOver(true, function () {}, false);
      assert.strictEqual(1, puzzle.get('stars'));
    });
    it('should test stars 2', function () {
      var cache_score = puzzle.getScore;

      puzzle.getScore = function () {
        puzzle.getScore = cache_score;
        return 10;
      };
      puzzle.loadPuzzle(puzzle_data);
      puzzle.set('limit_type', 'time');
      puzzle.set('limit', 60);
      puzzle.set('time', 45);
      puzzle.gameOver(true, function () {}, false);
      assert.strictEqual(2, puzzle.get('stars'));
    });

    it('should test 3 star for untimed', function () {
      puzzle.set('limit_type', 'untimed');
      puzzle.set('time', 60);
      puzzle.set('total_tiles', 60);
      assert.strictEqual(3, puzzle.getStars());
    });

    it('should test 2 star for untimed', function () {
      puzzle.set('limit_type', 'untimed');
      puzzle.set('time', 100);
      puzzle.set('total_tiles', 60);
      assert.strictEqual(2, puzzle.getStars());
    });

    it('should test 1 star for untimed', function () {
      puzzle.set('limit_type', 'untimed');
      puzzle.set('time', 200);
      puzzle.set('total_tiles', 60);
      assert.strictEqual(1, puzzle.getStars());
    });
  });

  describe('getScore()', function () {
    it('should test score if won', function () {
      var cache_families = puzzle_data.families,
        families = [
          {
            pairs: 2,
            types: 1
          },
          null,
          {
            pairs: 1,
            types: 1
          },
          {
            pairs: 1,
            types: 1
          },
          {
            pairs: 1,
            types: 1
          },
          {
            pairs: 1,
            types: 1
          },
          {
            pairs: 1,
            types: 1
          }
        ];

      puzzle_data.families = families;
      puzzle_data.limit_type = 'time';
      puzzle.loadPuzzle(puzzle_data);
      puzzle.set('goal', null);
      puzzle.set('solved', ['f0_0', 'f0_1', 'f2_0']);
      puzzle.gameOver(true, function () {}, false);
      assert.strictEqual(1600, puzzle.get('score'));
      puzzle_data.families = cache_families;
      puzzle.set('solved', []);
    });

    it('should test score if won for untimed level', function () {
      var cache_families = puzzle_data.families,
        families = [
          {
            pairs: 2,
            types: 1
          },
          null,
          {
            pairs: 1,
            types: 1
          },
          {
            pairs: 1,
            types: 1
          },
          {
            pairs: 1,
            types: 1
          },
          {
            pairs: 1,
            types: 1
          },
          {
            pairs: 1,
            types: 1
          }
        ];

      puzzle_data.families = families;
      puzzle_data.limit_type = 'untimed';
      puzzle.loadPuzzle(puzzle_data);
      puzzle.set('goal', null);
      puzzle.set('total_tiles', 200);
      puzzle.set('time', 200);
      puzzle.set('solved', ['f0_0', 'f0_1', 'f2_0']);
      puzzle.gameOver(true, function () { }, false);
      assert.strictEqual(2000, puzzle.get('score'));
      puzzle_data.families = cache_families;
      puzzle.set('solved', []);
    });

    it('should set score if won and golden tile goal', function () {
      var cache_families = puzzle_data.families,
        cache_each = puzzle.eachCell,
        families = [
          {
            pairs: 2,
            types: 1
          },
          null,
          {
            pairs: 1,
            types: 1
          },
          {
            pairs: 1,
            types: 1
          },
          {
            pairs: 1,
            types: 1
          },
          {
            pairs: 1,
            types: 1
          },
          {
            pairs: 1,
            types: 1
          }
        ];

      puzzle_data.families = families;
      puzzle_data.limit_type = 'time';
      puzzle.loadPuzzle(puzzle_data);
      puzzle.set('goal', {
        golden: 1
      });

      puzzle.eachCell = function (cb) {
        var tile_a = new TileModel(),
          tile_b = new TileModel();

        tile_a.set('face', 'f_golden');
        tile_a.set('real_face', 'f0_1');
        tile_b.set('face', 'f0_1');
        tile_a.isJoker = function () {
          return false;
        };
        tile_b.isJoker = function () {
          return false;
        };
        _.each([tile_a, tile_b], cb);
        puzzle.eachCell = cache_each;
      };

      puzzle.set('solved', ['f0_0', 'f0_1', 'f2_0']);
      puzzle.gameOver(true, function () {}, false);
      assert.strictEqual(2100, puzzle.get('score'));
      puzzle_data.families = cache_families;
      puzzle.set('solved', []);
    });

    it('should test score if fail', function () {
      var cache_families = puzzle_data.families,
        families = [
          {
            pairs: 2,
            types: 1
          },
          null,
          {
            pairs: 1,
            types: 1
          },
          {
            pairs: 1,
            types: 1
          },
          {
            pairs: 1,
            types: 1
          },
          {
            pairs: 1,
            types: 1
          },
          {
            pairs: 1,
            types: 1
          }
        ];

      puzzle.set('goal', null);
      puzzle_data.families = families;
      puzzle.loadPuzzle(puzzle_data);
      puzzle.set('solved', ['f0_0', 'f0_1', 'f2_0']);
      puzzle.gameOver(false, function () {}, false);
      assert.strictEqual(400, puzzle.get('score'));
      puzzle_data.families = cache_families;
      puzzle.set('solved', []);
    });
  });

  describe('updateProgress()', function () {
    it('should test progress update', function () {
      var cache_families = puzzle_data.families,
        families = [
          {
            pairs: 2,
            types: 1
          },
          null,
          {
            pairs: 1,
            types: 1
          },
          {
            pairs: 1,
            types: 1
          }
        ];

      puzzle_data.families = families;
      puzzle.loadPuzzle(puzzle_data);
      puzzle.set('solved', ['f0_0', 'f0_1', 'f2_0', 'f3_0']);
      puzzle.updateProgress();
      assert.strictEqual(100, puzzle.get('progress'));
      puzzle_data.families = cache_families;
      puzzle.set('solved', []);
    });
  });

  describe('limitBought()', function () {
    it('should update limit', function () {
      puzzle.set({
        limit: 1,
        limit_type: 'time'
      });

      puzzle.limitBought();
      assert.strictEqual(1 + util.getProducts().time.quantity,
        puzzle.get('limit'));
      puzzle.unset('limit');
    });

    it('should call resumeTimers', function (done) {
      var temp = puzzle.resumeTimers;

      puzzle.set({
        limit: 1,
        limit_type: 'time'
      });
      puzzle.resumeTimers = function () {
        done();
        puzzle.resumeTimers = temp;
      };

      puzzle.limitBought();
    });

    it('should call updateGameState', function (done) {
      var temp = puzzle.resumeTimers,
        cache = puzzle.updateGameState;

      puzzle.set({
        limit: 1,
        limit_type: 'tap'
      });
      puzzle.resumeTimers = function () {
        puzzle.resumeTimers = temp;
      };

      puzzle.updateGameState = function () {
        puzzle.updateGameState = cache;
        done();
      };

      puzzle.limitBought();
    });
  });

  describe('savePuzzleData()', function () {
    it('should invoke supr with required keys', function (done) {
      var cache = QuestPuzzle.prototype.savePuzzleData;

      QuestPuzzle.prototype.savePuzzleData = function (exit, keys) {
        QuestPuzzle.prototype.savePuzzleData = cache;
        assert.deepEqual(keys, [
          'free_shuffle',
          'undo',
          'loading_elapsed',
          'tap',
          'tiles_placings',
          'load_attempts',
          'type',
          'tile_set',
          'bonus_map',
          'shuffle',
          'hint',
          'swoosh',
          'open_slot',
          'joker_tile'
        ]);
        done();
      };
      puzzle.savePuzzleData();
    });

    it('should invoke super with exit_type shuffle if free_shuffle is ' +
      'used', function (done) {
        var cache = QuestPuzzle.prototype.savePuzzleData;

        QuestPuzzle.prototype.savePuzzleData = function (type) {
          QuestPuzzle.prototype.savePuzzleData = cache;
          done(type === 'shuffle' ? undefined : 'error');
        };

        puzzle.set('free_shuffle', 1, true);
        puzzle.savePuzzleData(false);
      }
    );
  });

  describe('changeFace', function () {
    it('should set face and emit face-change', function (done) {
      var i = 0,
        model = {
          set: function () {
            i++;
          },
          emit: function () {
            if (i === 1) {
              done();
            }
          }
        };

      puzzle.changeFace(model);
    });
  });

  describe('openSlot()', function () {
    it('should set passed tile as open slot tile if slot not ' +
      'occupied and reset last_match', function () {
        var model = 'abc',
          cache = puzzle.updateGameState,
          cache_neigh = puzzle.getNeighbours;

        puzzle.updateGameState = function () {
          puzzle.updateGameState = cache;
        };
        puzzle.getNeighbours = function () {
          puzzle.getNeighbours = cache_neigh;
        };
        puzzle.set('slot_tile', null);
        puzzle.openSlot(model);
        assert.strictEqual('abc', puzzle.get('slot_tile'));
        puzzle.set('slot_tile', null);
      }
    );

    it('should call cleanHinted and set tile selected to null', function () {
      var model = 'abc',
        cache = puzzle.updateGameState,
        cache_neigh = puzzle.getNeighbours,
        cache_hinted = puzzle.cleanHinted,
        count = 0;

      puzzle.updateGameState = function () {
        puzzle.updateGameState = cache;
      };
      puzzle.getNeighbours = function () {
        puzzle.getNeighbours = cache_neigh;
      };
      puzzle.cleanHinted = function () {
        puzzle.cleanHinted = cache_hinted;
        count++;
      };
      puzzle.set('slot_tile', null);
      puzzle.openSlot(model);
      assert.strictEqual('abc', puzzle.get('slot_tile'));
      assert.strictEqual(1, count);
      assert.strictEqual(null, puzzle.get('tile_selected'));
      puzzle.set('slot_tile', null);
    });

    it('shouldnt update slot_tile if already occupied', function () {
      puzzle.set('slot_tile', 'bbb');
      puzzle.openSlot('ccc');
      assert.strictEqual('bbb', puzzle.get('slot_tile'));
      puzzle.set('slot_tile', null);
    });
  });

  describe('updateJokerPair', function () {
    var tile_array = [
      {
        get: function (key) {
          if (key === 'face') {
            return 'f2_10';
          } else {
            return null;
          }
        },
        set: function () {},
        getCoordinates: function () {
          return [2, 3, 4];
        },
        isJoker: function () {},
        isLock: function () {}
      },
      {
        get: function () {
          return 'f1_11';
        },
        set: function () {},
        getCoordinates: function () {
          return [2, 3, 5];
        },
        isJoker: function () {},
        isLock: function () {}
      }
    ];

    it('should return []', function (done) {
      var cache = puzzle.eachCellByLayer;

      puzzle.eachCellByLayer = function () {
        puzzle.eachCellByLayer = cache;
      };
      assert.deepEqual([], puzzle.updateJokerPair());
      done();
    });

    it('should call changeFace', function (done) {
      var cache = puzzle.eachCellByLayer,
        change_face_cache = puzzle.changeFace,
        tile_free_cache = puzzle.isFreeTile,
        joker_tile = {
          get: function () {
            return 'f2_10';
          },
          isJoker: function () {
            return false;
          },
          getCoordinates: function () {
            return [1, 2, 3];
          },
          isLock: function () {
            return false;
          }
        },
        matched_tile = {
          get: function () {
            return 'f1_11';
          },
          isJoker: function () {
            return true;
          },
          getCoordinates: function () {
            return [1, 2, 5];
          },
          isLock: function () {
            return false;
          }
        };

      puzzle.isFreeTile = function () {
        return false;
      };
      puzzle.set('tile_per_match', 2);
      puzzle.eachCellByLayer = function (cb) {
        _.find(tile_array, function (model) {
          cb(model);
        });
      };

      puzzle.changeFace = function () {
        puzzle.changeFace = change_face_cache;
        done();
      };

      puzzle.updateJokerPair([joker_tile, matched_tile]);
      puzzle.eachCellByLayer = cache;
      puzzle.changeFace = change_face_cache;
      puzzle.isFreeTile = tile_free_cache;
    });

    it('should change face to joker if tile <=6', function (done) {
      var cache = puzzle.flattenTiles,
          cache_assign = puzzle.assignGoldenTile,
        set = function (face, id) {
          if (id === 'f_joker') {
            done();
          }
        },
        isFree = function () {
          return true;
        },
        tiles = _.times(5, function () {
          return {
            set: set,
            get: function () {},
            isFree: isFree
          };
        });

      puzzle.assignGoldenTile = function () {
        puzzle.assignGoldenTile = cache_assign;
      };

      puzzle.set('joker', true);
      puzzle.set('tile_count', 5);
      puzzle.flattenTiles = function () {
        return tiles;
      };
      puzzle.set('jewels', []);
      puzzle.set('jewel_order', []);
      puzzle.set('no_of_jokers', 1);
      puzzle.generateSolvableGame(tiles);
      puzzle.flattenTiles = cache;
    });
    it('should call changeFace even all the ' +
      'tiles are free', function (done) {
      var cache = puzzle.eachCellByLayer,
        change_face_cache = puzzle.changeFace,
        tile_free_cache = puzzle.isFreeTile,
        joker_tile = {
          get: function () {
            return 'f2_10';
          },
          isJoker: function () {
            return false;
          },
          getCoordinates: function () {
            return [1, 2, 5];
          },
          isLock: function () {
            return true;
          }
        },
        matched_tile = {
          get: function () {
            return 'f1_11';
          },
          isJoker: function () {
            return false;
          },
          getCoordinates: function () {
            return [1, 2, 3];
          },
          isLock: function () {
            return false;
          }
        };

      puzzle.isFreeTile = function () {
        return true;
      };
      puzzle.set('tile_per_match', 2);
      puzzle.eachCellByLayer = function (cb) {
        _.find(tile_array, function (model) {
          cb(model);
        });
      };

      puzzle.changeFace = function () {
        puzzle.changeFace = change_face_cache;
        done();
      };

      puzzle.updateJokerPair([joker_tile, matched_tile]);
      puzzle.isFreeTile = tile_free_cache;
      puzzle.eachCellByLayer = cache;
    });
  });

  describe('getPairWithJoker', function () {
    it('should return null', function (done) {
      var cache = this.eachCell,
        tile_free_cache = puzzle.isFreeTile,
        tiles = [
          {},
          {}
        ];

      puzzle.isFreeTile = function () {
        return false;
      };

      assert.deepEqual(null, puzzle.getPairWithJoker(tiles));
      done();
      puzzle.isFreeTile = tile_free_cache;
      this.eachCell = cache;
    });

    it('should return a pair one will be joker', function (done) {
      var pair,
        tile_free_cache = puzzle.isFreeTile,
        tiles = {
          f2_10: [{
            isJoker: function () {
              return false;
            }
          }],
          f_joker: [{
            isJoker: function () {
              return true;
            }
          }]
        };

      puzzle.isFreeTile = function () {
        return true;
      };

      pair = puzzle.getPairWithJoker(tiles);
      puzzle.isFreeTile = tile_free_cache;
      assert.strictEqual(pair[0].isJoker(), true);
      assert.strictEqual(pair[1].isJoker(), false);
      done();
    });

    it('should return null if no joker tiles', function (done) {
      var pair,
        tile_free_cache = puzzle.isFreeTile,
        tiles = {
          f2_10: [{
            isJoker: function () {
              return false;
            }
          }],
          f2_11: [{
            isJoker: function () {
              return false;
            }
          }]
        };

      puzzle.isFreeTile = function () {
        return false;
      };

      pair = puzzle.getPairWithJoker(tiles);
      puzzle.isFreeTile = tile_free_cache;
      assert.deepEqual(pair, null);
      done();
    });

    it('should return two joker', function (done) {
      var pair,
        tile_free_cache = puzzle.isFreeTile,
        puzzle_count_cache = puzzle.get,
        tiles = {
          f_joker: [{
            isJoker: function () {
              return true;
            }
          },
          {
            isJoker: function () {
              return true;
            }
          }]
        };

      puzzle.get = function (key) {
        if (key === 'tile_count') {
          puzzle.get = puzzle_count_cache;
          return 2;
        } else {
          return puzzle_count_cache();
        }
      };

      puzzle.isFreeTile = function () {
        return true;
      };

      pair = puzzle.getPairWithJoker(tiles);
      puzzle.isFreeTile = tile_free_cache;
      assert.strictEqual(pair[0].isJoker(), true);
      assert.strictEqual(pair[1].isJoker(), true);
      done();
    });
  });

  describe('populateLastMatch', function () {
    var tile_one = {
        getCoordinates: function () {
          return [1, 2, 3];
        },
        get: function (param) {
          if (param === 'tag') {
            return 'triangle';
          } else if (param === 'hidden') {
            return false;
          } else {
            return 'f_01';
          }
        }
      },
      tile_two = {
        getCoordinates: function () {
          return [1, 2, 4];
        },
        get: function (param) {
          if (param === 'tag') {
            return 'triangle';
          } else if (param === 'hidden') {
            return false;
          } else {
            return 'f_02';
          }
        }
      },
      changed_tiles = [{
        getCoordinates: function () {
          return [1, 2, 5];
        },
        get: function (param) {
          if (param === 'hidden') {
            return false;
          } else {
            return 'f_03';
          }
        },
        set: function () {}
      }];

    it('should set add face and real_face with tiles', function (done) {
      var last_match = puzzle.populateLastMatch([tile_one, tile_two]);

      assert.deepEqual(last_match[0], [1, 2, 3, 'f_01', 'f_01',
        'triangle', false]);
      assert.deepEqual(last_match[1], [1, 2, 4, 'f_02', 'f_02',
        'triangle', false]);
      done();
    });
    it('should set changed tile details if available', function (done) {
      var last_match = puzzle.populateLastMatch([tile_one, tile_two],
        changed_tiles);

      assert.deepEqual(last_match[2], [1, 2, 5, 'f_03', 'f_03']);
      done();
    });
  });

  describe('beforeTutorial()', function () {
    it('shold set real face to joker and call change face', function (done) {
      var i = 0,
        model = {
          set: function (key) {
            if (key === 'real_face') {
              i++;
            }
          },
          get: function () {}
        },
        view = {model: model};

      puzzle.changeFace = function () {
        puzzle.changeFace = cache;
        i++;
      };

      puzzle.beforeTutorial('joker_intro', view);
      done(i === 2 ? undefined : 'error');
    });

    it('shold not change face if tutorial id not joker', function (done) {
      var i = 0,
        model = {
          set: function (key) {
            if (key === 'real_face') {
              i++;
            }
          },
          get: function () {}
        };

      puzzle.changeFace = function () {
        puzzle.changeFace = cache;
        i++;
      };

      puzzle.beforeTutorial('non_joker', model);
      done(i === 2 ? 'error' : undefined);
    });

    it('shold set hide overlay', function (done) {
      var view = {
          overlay: {
            hide: function () {
              done();
            }
          }
        };

      puzzle.beforeTutorial('swap_select_tiles', view);
    });
  });

  describe('afterTutorial()', function () {
    it('shold set show ov erlay', function (done) {
      var view = {
          overlay: {
            show: function () {
              done();
            }
          }
        };

      puzzle.afterTutorial('swap_select_tiles', view);
    });

    it('shold not set show overlay if not swap', function (done) {
      var view = {
          overlay: {
            show: function () {
              done('error');
            }
          }
        };

      puzzle.afterTutorial('no_swap_select_tiles', view);
      done();
    });
  });

  describe('onTapChange()', function () {
    it('should emit gameover if tap exceeds limit', function (done) {
      var cache = puzzle.emit;

      puzzle.emit = function (event) {
        if (event === 'check-complete') {
          puzzle.emit = cache;
          done();
        }
      };
      puzzle.onTapChange(100);
    });
  });

  describe('clean', function () {
    it('should set tile_selected null', function () {
      var cache = puzzle.set;

      puzzle.set = function (key, val) {
        puzzle.set = cache;
        assert.strictEqual(key, 'tile_selected');
        assert.strictEqual(val, null);
      };
      puzzle.clean();
    });
    it('should release tile model', function (done) {
      var cache = puzzle.tile.releaseAllModels;

      puzzle.tile.releaseAllModels = function () {
        puzzle.tile.releaseAllModels = cache;
        done();
      };
      puzzle.clean();
    });
  });
  describe('logEvent()', function () {
    it('if first tap, emits event first-tap', function (done) {
      var cache = event_manager.emit;

      event_manager.emit = function (event) {
        if (event === 'first-tap') {
          event_manager.emit = cache;
          done();
        }
      };
      puzzle.logEvent('first-tap');
    });
    it('if first match, emits event first-pair-match', function (done) {
      var cache = event_manager.emit;

      event_manager.emit = function (event) {
        if (event === 'first-pair-match') {
          event_manager.emit = cache;
          done();
        }
      };
      puzzle.logEvent('first-pair-match');
    });
  });
});
