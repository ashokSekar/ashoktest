var Class = require('js-class'),
  assert = require('assert'),
  db = require('../../../models/db/db');

describe('Db', function () {
  'use strict';

  describe('insertBonusMapPuzzles()', function () {
    it('should call parents insertRecordDupIgnore', function (done) {
      db.__base = Class({ // jshint ignore:line
        insertRecordsDupIgnore: function (data, table) {
          assert.strictEqual(table, 'user_gs_stats_all_exp');
          done();
        }
      });

      db.insertBonusMapPuzzles();
    });
  });
});
