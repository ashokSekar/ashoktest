/* global User, util_test, util, ab_testing */

jsio('import test.lib.util as util_test');
jsio('import quest.models.user as User');

describe('User Model', function () {
  'use strict';

  describe('onSync()', function () {
    if (util_test.getFromJSIOCache('src/modules/util.js')) {
      util_test.removeFromJSIOCache('src/modules/util.js');
    }
    util_test.removeFromJSIOCache('src/models/user.js');

    it('should add tile sets with existing', function (done) {
      var user, cache;

      jsio('import quest.models.user as User');
      jsio('import quest.modules.util as util');

      user = new User();
      user.set('social_facebook', true);
      cache = util.getNonSynced;

      user.set('tile_sets', ['classic']);

      util.getNonSynced = function () {
        return {};
      };

      user.onSync({
        user: {
          tile_sets: ['winter']
        }
      });

      assert.deepEqual(['classic', 'winter'], user.get('tile_sets'));
      done();
      util.getNonSynced = cache;
    });

    it('should merge unlocked powerups', function (done) {
      var user, cache;

      jsio('import quest.models.user as User');
      jsio('import quest.modules.util as util');

      user = new User();
      user.set('social_facebook', true);
      cache = util.getNonSynced;

      user.set('unlocked_powerups', ['shuffle']);

      util.getNonSynced = function () {
        return {};
      };

      user.onSync({
        user: {
          unlocked_powerups: ['joker']
        }
      });

      assert.deepEqual(['shuffle', 'joker'], user.get('unlocked_powerups'));
      done();
      util.getNonSynced = cache;
    });

    it('should not set tile sets if non fb user', function (done) {
      var cache_set, user, cache;

      jsio('import quest.models.user as User');
      jsio('import quest.modules.util as util');

      user = new User();
      user.set('social_facebook', false);
      cache = util.getNonSynced;

      user.set('tile_sets', ['classic']);

      util.getNonSynced = function () {
        return {};
      };

      cache_set = user.set;

      user.set = function (key) {
        if (key === 'tile_sets') {
          done('error');
        }
        return {
          save: function () {}
        };
      };

      user.onSync({
        user: {
          tile_sets: ['winter']
        }
      });
      done();
      util.getNonSynced = cache;
      user.set = cache_set;
    });
  });

  describe('getPowerupForInfo()', function () {
    it('it should return powerup name', function (done) {
      var user = new User();

      jsio('import quest.modules.util as util');

      user.set('last_powerup_info', [0, '']);
      user.set('unlocked_powerups', ['hint']);
      user.set('curr_ms', 6);

      assert.strictEqual(user.getPowerupForInfo(), 'hint');
      done();
    });

    it('it should return undefined', function (done) {
      var user = new User();

      jsio('import quest.modules.util as util');

      user.set('last_powerup_info', [0, '']);
      user.set('unlocked_powerups', []);
      user.set('curr_ms', 6);

      assert.strictEqual(user.getPowerupForInfo(), undefined);
      done();
    });
  });

  describe('getLivesGenTime()', function () {
    before(function () {
      jsio('import quest.modules.ab_testing as ab_testing');
    });

    it('should return value from remote', function () {
      var user = new User();

      ab_testing.set('lives_gen', 25);

      assert.equal(
        1500000,
        user.getLivesGenTime()
      );
    });

    it('should return config value', function () {
      var user = new User();

      ab_testing.set('lives_gen', '');

      assert.equal(
        1800000,
        user.getLivesGenTime()
      );
    });
  });
});
