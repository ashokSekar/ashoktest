/* global test_util, View, event_manager,
   history, cutscene */

jsio('import test.lib.util as test_util');
jsio('import DevkitHelper.model as Model');
jsio('import DevkitHelper.history as history');
jsio('import util.underscore as _');
jsio('import ui.View as View');
jsio('import DevkitHelper.event_manager as event_manager');

describe('CutScene Module', function () {
  'use strict';

  var model,
    is_added = true,
    is_completed = true,
    init = function () {
      model = new Model();
      model.getLimit = function () {
        return this.get('limit_time') ? 'time' : 'moves';
      };

      GC.app = {
        user: new Model(),
        tutorial: {
          add: function () {
            return is_added;
          },
          build: function () {},
          isCompleted: function () {
            return is_completed;
          },
          clean: function () {}
        },
        getCurrentView: function () {
          return new View();
        }
      };

      GC.app.user.set({
        inventory_hint: 0,
        inventory_shuffle: 0,
        inventory_magnet: 0,
        inventory_swoosh: 0
      });

      event_manager.register('', []);
      jsio('import src.modules.cutscene as cutscene');
    },
    clean = function () {
      test_util.removeFromJSIOCache('modules/cutscene.js');
    };

  beforeEach(init);
  afterEach(clean);

  it('should make sure that all corresponding funtions exist', function () {
    var data = cutscene._data;

    _.each(_.extend({}, data.map, data.puzzle), function (obj) {
      assert.strictEqual(typeof cutscene[obj.fn], 'function');
    });
  });

  it('should emit event powerup unlocked', function () {
    var data = cutscene._data,
      count_emit = 0,
      count = 0;

    event_manager.emit = function () {
      count_emit++;
    };
    _.each(_.extend({}, data.map, data.puzzle), function (obj) {
      cutscene[obj.fn]();
      count++;
    });
    assert.strictEqual(count_emit, count);
  });

  it('should add unlockables to map story properly', function () {
    var data = {
        unlock_shuffle: ['5', 'unlockShuffle'],
        unlock_hint: ['12', 'unlockHint'],
        unlock_swoosh: ['23', 'unlockSwoosh']
      },
      map_data = cutscene._data.map,
      current;

    _.each(data, function (dat, key) {
      current = map_data[dat[0]];
      assert.strictEqual(key, current.id);
      assert.strictEqual(typeof cutscene[dat[1]], 'function');
    });
  });

  describe('showTutorial', function () {
    it('should call tutorial.add', function (done) {
      var cache = GC.app.tutorial.add;

      GC.app.tutorial.add = function (id) {
        GC.app.tutorial.add = cache;
        assert.strictEqual('test', id);
        done();
      };
      cutscene._showTutorial('test')();
    });

    it('should call callback', function (done) {
      is_added = false;
      cutscene._showTutorial('test')(null, done);
    });

    it('should not call callback if tutotial is shown', function (done) {
      var flag = true;

      is_added = true;
      cutscene._showTutorial('test')(true, function () {
        flag = false;
      });
      setTimeout(function () {
        done(flag ? undefined : 'callback called');
      }, 10);
    });
    it('should call callback if tutotial is shown', function (done) {
      is_added = true;
      cutscene._showTutorial('test', done)(true);
    });

    it('should pass tutorial on_cancel cb', function (done) {
      var cache = GC.app.tutorial.add;

      GC.app.tutorial.add = function (id, cb, force, opts) {
        GC.app.tutorial.add = cache;
        assert.strictEqual(true, opts.on_cancel ? true : false);
        done();
        return false;
      };
      cutscene._showTutorial('test')(true);
    });

    it('on_cancel cb should call tutorial clean',
      function (done) {
        var cache = GC.app.tutorial;

        GC.app.tutorial = {
          add: function (id, cb, force, opts) {
            opts.on_cancel();
            return false;
          },
          clean: function () {
            done();
            this.clean = function () {};
          }
        };
        cutscene._showTutorial('free_currency')(true);
        GC.app.tutorial = cache;
      }
    );

    it('on_cancel cb should call history cb',
      function (done) {
        var cache = GC.app.tutorial;

        GC.app.tutorial = {
          add: function (id, cb, force, opts) {
            assert.strictEqual(true, opts.on_cancel ? true : false);
            opts.on_cancel({fire: done});
            return false;
          },
          clean: function () {
          }
        };
        cutscene._showTutorial('free_currency')(true);
        GC.app.tutorial = cache;
      }
    );

    it('on_cancel cb should call history_cb fire', function (done) {
      var cache = GC.app.tutorial.add;

      history.add(function (cb) {
        cb.fire();
      });
      GC.app.tutorial.add = function (id, cb, force, opts) {
        GC.app.tutorial.add = cache;
        opts.on_cancel({
          fire: done
        });
        return false;
      };

      cutscene._showTutorial('test')(true);
    });

    it('on_cancel cb should call passed cb with true', function (done) {
      var cache = GC.app.tutorial.add;

      GC.app.tutorial.add = function (id, cb, force, opts) {
        GC.app.tutorial.add = cache;
        assert.strictEqual(true, opts.on_cancel ? true : false);
        opts.on_cancel();
        return true;
      };

      cutscene._showTutorial('test')(true, function (val) {
        done(val ? undefined : 'error');
      });
    });
  });

  describe('register()', function () {
    it('should call model chain to register events', function (done) {
      var chain = model.chain;

      GC.app.user.get = function () {
        return 5;
      };
      is_completed = false;
      model.chain = function () {
        model.chain = chain;
        done();
      };
      cutscene.register(model, 'map');
    });

    it('should not call model chain if tutorial is shown', function (done) {
      var flag = true;

      GC.app.user.get = function () {
        return 5;
      };
      is_completed = true;
      model.chain = function () {
        flag = false;
      };
      cutscene.register(model, 'map');
      setTimeout(function () {
        done(flag ? undefined : 'error');
      }, 10);
    });

    it('should register for passed type and ms', function (done) {
      var cbs = [];

      GC.app.user.get = function () {
        return 12;
      };
      cutscene.unlockHint = done;

      is_completed = false;

      model.chain = function (evnt, cb) {
        cbs.push(cb);
      };

      cutscene.register(model, 'map', 12);
      assert.strictEqual(1, cbs.length);
      cbs[0]();
    });
  });

  describe('isCompleted()', function () {
    it('should return true if no cutscene for ms exists', function () {
      GC.app.user.get = function () {
        return 1;
      };
      assert.strictEqual(true, cutscene.isCompleted('map'));
    });

    it('should return true if tutorial is completed', function () {
      GC.app.user.get = function () {
        return 200;
      };

      is_completed = true;
      assert.strictEqual(true, cutscene.isCompleted('map'));
    });
  });
});
