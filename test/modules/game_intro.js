/* globals game_intro, util, util_test */
jsio('import util.underscore as _');
jsio('import test.lib.util as util_test');

describe('game_intro', function () {
  'use strict';

  var ms_no = 1,
    completed_flag = true,
    config_data = {
      STYLE: {
        orientation: 'portrait'
      },
      variations: {
        classic: {
          id: 'classic',
          powerups_exclude: ['flip']
        },
        memory: {
          id: 'memory',
          powerups_exclude: ['shuffle', 'half_hint',
            'swap', 'open_slot', 'auto_hint']
        }
      },
      award_powerup_count: {
        flip: 3,
        hint: 3,
        half_hint: 3,
        auto_hint: 3,
        swoosh: 3,
        substitute: 3,
        open_slot: 3,
        swap: 3,
        joker: 3
      },
      powerups: {
        ingame: {
          shuffle: true,
          hint: true,
          half_hint: true,
          swoosh: true,
          swap: false,
          substitute: true,
          flip: false
        },
        pregame: {
          joker: true,
          open_slot: true,
          auto_hint: true
        }
      },
      bonus_level_def: [[20, 40], [13, 15], [7, 30]],
      unlockables: {
        flip: 27,
        shuffle: 5,
        hint: 12,
        half_hint: 14,
        swoosh: 23,
        joker: 20,
        swap: 32,
        substitute: 40,
        open_slot: 44,
        auto_hint: 52
      },
      ms_per_chapter: 20,
      lives: {
        max: 5,
        cost_for_max: 10,
        gen: 1800000,
        ad: 1,
        fb: 1
      }
    };

  before(function () {
    var app = GC.app;

    jsio('import quest.modules.util as util');
    jsio('import resources.data.config as config_data');

    util_test.getFromJSIOCache('resources/data/config').exports = config_data;
    jsio('import src.modules.game_intro as game_intro');
    app.user = {
      getMs: function () {
        return ms_no;
      },
      getPowerupForInfo: function () {
      },
      get: function (key) {
        if (key === 'last_powerup_info') {
          return [];
        }
      }
    };

    app.tutorial = {
      build: function () {},
      isCompleted: function () {
        return completed_flag;
      }
    };
  });

  describe('register()', function () {
    it('should fail register if completed', function () {
      var puzzle = {
        model: {
          get: function (params) {
            if (params === 'type') {
              return 'classic';
            }
          }
        },
        grid: {
          tiles: []
        },
        hud: {
          powerups: {},
          goals_hud:
          {
            left_view: {
              limit_view: {}
            }
          }
        },
        getContextForTutorial: function () {
          return [];
        }
      },
      cache = game_intro.getOldTutorialMs;

      game_intro.getOldTutorialMs = function () {
        return false;
      };
      ms_no = 10;
      game_intro.register(puzzle);
      game_intro.build();

      assert.strictEqual(false, game_intro.register(puzzle));
      game_intro.getOldTutorialMs = cache;
    });

    it('should not fail register if old tutorial', function () {
      var puzzle = {
        model: {
          get: function (param) {
            if (param) {
              return 'classic';
            }
          }
        },
        grid: {
          tiles: []
        },
        hud: {
          powerups: {},
          goals_hud:
          {
            left_view: {
              limit_view: {}
            }
          }
        },
        getContextForTutorial: function () {
          return [];
        }
      },
      cache = game_intro.getOldTutorialMs;

      ms_no = 10;
      game_intro.register(puzzle);
      game_intro.build();

      game_intro.getOldTutorialMs = function () {
        return 10;
      };

      assert.strictEqual(true, game_intro.register(puzzle));
      game_intro.getOldTutorialMs = cache;
    });

    it('should show tutorial for powerup info', function (done) {
      var puzzle = {
        model: {
          get: function (param) {
            if (param === 'type') {
              return 'classic';
            }
          }
        },
        grid: {
          tiles: []
        },
        hud: {
          powerups: {},
          goals_hud:
          {
            left_view: {
              limit_view: {}
            }
          }
        },
        getContextForTutorial: function () {
          return [];
        }
      },
      cache = game_intro.getOldTutorialMs,
      cache_get_powerup_inf = GC.app.user.getPowerupForInfo,
      build_cache;

      ms_no = 10;
      game_intro.register(puzzle);
      game_intro.build();

      build_cache = game_intro.build;

      game_intro.getOldTutorialMs = function () {
        return null;
      };

      GC.app.user.getPowerupForInfo = function () {
        GC.app.user.getPowerupForInfo = cache_get_powerup_inf;
        return 'shuffle';
      };

      game_intro.build = function (curr_ms) {
        game_intro.build = build_cache;
        assert.strictEqual(curr_ms, 'powerup_info');
        done();
      };

      assert.strictEqual(true, game_intro.register(puzzle));
      game_intro.getOldTutorialMs = cache;
    });

    it('should fail if powerupcheck fail', function () {
      var puzzle = {
        model: {
          get: function (param) {
            if (param === 'type') {
              return 'classic';
            }
          }
        },
        grid: {
          tiles: []
        },
        hud: {
          powerups: {},
          goals_hud:
          {
            left_view: {
              limit_view: {}
            }
          }
        },
        getContextForTutorial: function () {
          return [];
        }
      },
      cache = game_intro.getOldTutorialMs,
      powerup_check_cache = game_intro.powerupCheck;

      ms_no = 20;
      game_intro.register(puzzle);
      game_intro.build();

      game_intro.powerupCheck = function () {
        return false;
      };

      game_intro.getOldTutorialMs = function () {
        return 5;
      };

      assert.strictEqual(false, game_intro.register(puzzle));
      game_intro.getOldTutorialMs = cache;
      game_intro.powerupCheck = powerup_check_cache;
    });

    it('should fail if curr milestore not the max', function (done) {
      var puzzle =
        {
          model: {
            get: function (params) {
              if (params === 'type') {
                return 'classic';
              }
            }
          },
          grid: {
            tiles: []
          },
          hud: {
            powerups: {},
            goals_hud:
            {
              left_view: {
                limit_view: {}
              }
            }
          }
        },
        cache = game_intro.build,
        user = GC.app.user,
        cache_getMs = user.getMs,
        cache_get_old_tut = game_intro.getOldTutorialMs;

      user.getMs = function (type) {
        return type === 'curr' ? 20 : 21;
      };

      game_intro.getOldTutorialMs = function () {
        return false;
      };

      game_intro.build = function () {
        done('error');
      };
      assert.strictEqual(false, game_intro.register(puzzle));
      user.getMs = cache_getMs;
      game_intro.build = cache;
      game_intro.getOldTutorialMs = cache_get_old_tut;
      done();
    });

    it('should invoke build and return true if not completed', function () {
      var cache = game_intro.build,
        count = 0,
        puzzle = {
          model: {
            get: function (params) {
              if (params === 'type') {
                return 'classic';
              }
            },
            emit: function () {}
          },
          grid: {
            tiles: []
          },
          hud: {
            powerups: {},
            goals_hud:
            {
              left_view: {
                limit_view: {}
              }
            }
          }
        },
        get_old_tutorial_cache = game_intro.getOldTutorialMs,
        cache_powerup_check = game_intro.powerupCheck;

      ms_no = 1;
      completed_flag = false;
      game_intro.build = function () {
        game_intro.build = cache;
        ++count;
      };

      game_intro.getOldTutorialMs = function () {
        return 1;
      };

      game_intro.powerupCheck = function () {
        return true;
      };

      assert.strictEqual(true, game_intro.register(puzzle));
      assert.strictEqual(1, count);
      game_intro.getOldTutorialMs = get_old_tutorial_cache;
      game_intro.powerupCheck = cache_powerup_check;
    });
  });

  describe('powerupCheck()', function () {
    it('should return false if current pregame powerup is not ' +
      'active', function () {
        var cache = util.getPowerups,
          puzzle = {
            model: {
              get: function (params) {
                if (params === 'joker') {
                  return false;
                } else if (params === 'type') {
                  return 'classic';
                }
              },
              emit: function () {}
            },
            grid: {
              tiles: []
            },
            hud: {
              powerups: {},
              goals_hud:
              {
                left_view: {
                  limit_view: {}
                }
              }
            },
            getContextForTutorial: function () {
              return [{}, {}];
            }
          },
          cache_user = GC.app.user.getMs;

        util.getPowerups = function () {
          return {
            joker: 1
          };
        };

        GC.app.user.getMs = function () {
          return 20;
        };

        game_intro.register(puzzle);
        assert.strictEqual(false, game_intro.powerupCheck('joker'));
        GC.app.user.getMs = cache_user;
        util.getPowerups = cache;
      }
    );

    it('should return false if excluded powerup', function () {
        var cache = util.getPowerups,
          puzzle = {
            model: {
              get: function (params) {
                if (params === 'joker') {
                  return false;
                } else if (params === 'type') {
                  return 'classic';
                }
              },
              emit: function () {}
            },
            grid: {
              tiles: []
            },
            hud: {
              powerups: {},
              goals_hud:
              {
                left_view: {
                  limit_view: {}
                }
              }
            },
            getContextForTutorial: function () {
              return [{}, {}];
            }
          },
          cache_user = GC.app.user.getMs,
          cache_get = GC.app.user.get;

        util.getPowerups = function () {
          return {
          };
        };

        GC.app.user.get = function () {
          return [];
        };

        GC.app.user.getMs = function () {
          return 27;
        };

        game_intro.register(puzzle);
        assert.strictEqual(false, game_intro.powerupCheck('flip', 'classic'));
        GC.app.user.getMs = cache_user;
        util.getPowerups = cache;
        GC.app.user.get = cache_get;
      }
    );
    it('should return false if already unlocked', function () {
        var cache = util.getPowerups,
          puzzle = {
            model: {
              get: function (params) {
                if (params === 'joker') {
                  return true;
                } else if (params === 'type') {
                  return 'classic';
                }
              },
              emit: function () {}
            },
            grid: {
              tiles: []
            },
            hud: {
              powerups: {},
              goals_hud:
              {
                left_view: {
                  limit_view: {}
                }
              }
            },
            getContextForTutorial: function () {
              return [{}, {}];
            }
          },
          cache_user = GC.app.user.getMs,
          cache_get = GC.app.user.get;

        util.getPowerups = function () {
          return {
            joker: 1
          };
        };

        GC.app.user.get = function (key) {
          if (key === 'unlocked_powerups') {
            return ['joker'];
          } else {
            return [];
          }
        };

        GC.app.user.getMs = function () {
          return 20;
        };

        game_intro.register(puzzle);
        assert.strictEqual(false, game_intro.powerupCheck('joker'));
        GC.app.user.getMs = cache_user;
        util.getPowerups = cache;
        GC.app.user.get = cache_get;
      }
    );

    it('should return true', function () {
        var cache = util.getPowerups,
          puzzle = {
            getContextForTutorial: function () {
              return [{}, {}];
            },
            model: {
              get: function (params) {
                if (params === 'joker') {
                  return true;
                } else if (params === 'type') {
                  return 'classic';
                }
              },
              emit: function () {}
            },
            grid: {
              tiles: []
            },
            hud: {
              powerups: {},
              goals_hud:
              {
                left_view: {
                  limit_view: {}
                }
              }
            }
          },
          cache_user = GC.app.user.getMs,
          cache_get = GC.app.user.get;

        util.getPowerups = function () {
          return {
            joker: 1
          };
        };

        GC.app.user.get = function (key) {
          if (key === 'unlocked_powerups') {
            return ['xyz'];
          } else {
            return [];
          }
        };

        GC.app.user.getMs = function () {
          return 20;
        };

        game_intro.register(puzzle);
        assert.strictEqual(true, game_intro.powerupCheck(20, 'classic'));
        GC.app.user.getMs = cache_user;
        util.getPowerups = cache;
        GC.app.user.get = cache_get;
      }
    );
  });

  describe('build()', function () {
    it('should build tutorial with proper opts', function () {
      var tut = GC.app.tutorial,
        cache = tut.build,
        puzzle = {
          model: {
            get: function (params) {
              if (params === 'type') {
                return 'classic';
              }
            },
            emit: function () {}
          },
          grid: {
            tiles: []
          },
          hud: {
            powerups: {},
            goals_hud:
            {
              left_view: {
                limit_view: {}
              }
            }
          },
          getContextForTutorial: function () {
            return [{}, {}];
          }
        },
        cache_old_tut = game_intro.getOldTutorialMs,
        cache_powerup_check = game_intro.powerupCheck;

      game_intro.getOldTutorialMs = function () {
        return 1;
      };

      game_intro.powerupCheck = function () {
        return true;
      };

      ms_no = 1;
      completed_flag = false;
      tut.build = function (opts) {
        tut.build = cache;
        assert.strictEqual(opts.type, 'game_intro');
        assert.deepEqual(opts.superview, puzzle);
        assert.strictEqual(opts.milestone, 1);
        assert.strictEqual(26, _.keys(opts.positions).length);
        assert.strictEqual(true, _.has(opts.positions, 'welcome_to_mahjong'));
        assert.strictEqual(true, _.has(opts.positions, 'match_pair'));
        assert.strictEqual(true, _.has(opts.positions, 'match_only_free'));
        assert.strictEqual(true, _.has(opts.positions, 'non_free_unmatch'));
        assert.strictEqual(true, _.has(opts.positions, 'timer_intro'));
        assert.strictEqual(true, _.has(opts.positions, 'shuffle_intro'));
        assert.strictEqual(true, _.has(opts.positions, 'hint_intro'));
        assert.strictEqual(true, _.has(opts.positions, 'half_hint_intro'));
        assert.strictEqual(true, _.has(opts.positions, 'swoosh_intro'));
        assert.strictEqual(true, _.has(opts.positions, 'open_slot_drag'));
        assert.strictEqual(true, _.has(opts.positions, 'open_slot_intro'));
        assert.strictEqual(true, _.has(opts.positions, 'open_slot_after'));
      };
      game_intro.register(puzzle);
      game_intro.build();
      game_intro.getOldTutorialMs = cache_old_tut;
      game_intro.powerupCheck = cache_powerup_check;
    });
    it('should pause the timer and grid should be untouchable', function () {
      var tut = GC.app.tutorial,
        i = 0,
        cache = tut.build,
        puzzle = {
          model: {
            pauseTimers: function () {
              i++;
            },
            get: function (param) {
              if (param === 'type') {
                return 'classic';
              }
            }
          },
          grid: {
            tiles: [],
            setHandleEvents: function () {
              i++;
            }
          },
          hud: {
            powerups: {},
            goals_hud:
            {
              left_view: {
                limit_view: {}
              }
            }
          },
          getContextForTutorial: function () {
            return [{}, {}];
          }
        };

      tut.build = function (opts) {
        tut.build = cache;
        opts.on_resume();
        assert.strictEqual(i, 2);
      };
      game_intro.register(puzzle);
      game_intro.build();
    });

    it('should build bonus tutorial with proper opts', function () {
      var tut = GC.app.tutorial,
        cache = tut.build,
        puzzle = {
          model: {
            get: function (params) {
              if (params === 'type') {
                return 'memory';
              } else if (params === 'bonus_level') {
                return '5';
              }
            }
          },
          grid: {
            tiles: []
          },
          hud: {
            powerups: {},
            goals_hud:
            {
              left_view: {
                limit_view: {}
              }
            }
          },
          getContextForTutorial: function () {
            return [{}, {}];
          }
        };

      ms_no = 'memory';
      completed_flag = false;
      tut.build = function (opts) {
        tut.build = cache;
        assert.strictEqual(opts.type, 'game_intro');
        assert.deepEqual(opts.superview, puzzle);
        assert.strictEqual(opts.milestone, 'memory');
        assert.strictEqual(26, _.keys(opts.positions).length);
        assert.strictEqual(true, _.has(opts.positions, 'memory_match_intro'));
        assert.strictEqual(true, _.has(opts.positions, 'memory_match_time'));
        assert.strictEqual(true,
          _.has(opts.positions, 'memory_match_mismatch'));
      };
      game_intro.register(puzzle);
      game_intro.build('bonus_memory');
    });

    it('shouldnt call start timer for swap info after', function (done) {
      var tut = GC.app.tutorial,
        cache = tut.build,
        puzzle = {
          model: {
            get: function (params) {
              if (params === 'type') {
                return 'classic';
              }
            },
            emit: function () {},
            resumeTimers: done
          },
          grid: {
            tiles: [],
            setHandleEvents: function (val) {
              done(val === true ? undefined : 'error');
            }
          },
          hud: {
            powerups: {},
            goals_hud:
            {
              left_view: {
                limit_view: {}
              }
            }
          },
          getContextForTutorial: function () {
            return [{}, {}];
          }
        };

      ms_no = 1;
      completed_flag = false;
      tut.build = function (opts) {
        tut.build = cache;
        opts.positions.swap_select_tiles.after();
      };
      game_intro.register(puzzle);
      game_intro.build(20);
    });

    it('tutorial before should pause puzzle timers', function (done) {
      var puzzle = {
          model: {
            get: function (params) {
              if (params === 'type') {
                return 'classic';
              }
            },
            pauseTimers: done
          },
          grid: {
            tiles: [],
            setHandleEvents: function () {}
          },
          hud: {
            powerups: {},
            goals_hud:
            {
              left_view: {
                limit_view: {}
              }
            }
          },
          getContextForTutorial: function () {
            return [{}, {}];
          }
        };

      ms_no = 1;
      completed_flag = false;
      game_intro.register(puzzle);
      game_intro.build();
      game_intro._common.before();
    });

    it('before cb of hint should add hint as free good', function (done) {
      var tut = GC.app.tutorial,
        user = GC.app.user,
        get_cache = user.get,
        cache = tut.build,
        cache_free_goods = user.addFreeGoods,
        puzzle = {
          model: {
            pauseTimers: function () {},
            get: function (param) {
              if (param === 'type') {
                return 'classic';
              }
            }
          },
          grid: {
            tiles: [],
            setHandleEvents: function () {}
          },
          hud: {
            powerups: {
              btn_hint: {
                setCount: function () {}
              }
            },
            goals_hud:
            {
              left_view: {
                limit_view: {}
              }
            }
          },
          getContextForTutorial: function () {
            return [{}, {}];
          }
        };

      user.get = function (key) {
        if (key.startsWith('inventory_')) {
          return 0;
        } else {
          return [];
        }
      };

      user.addFreeGoods = function (item, count, source) {
        user.addFreeGoods = cache_free_goods;
        assert.strictEqual('hint', item);
        assert.strictEqual(3, count);
        assert.strictEqual('first_time_bonus', source);
        done();
      };

      tut.build = function (opts) {
        tut.build = cache;
        opts.positions.hint_intro.before();
      };

      ms_no = 1;
      completed_flag = false;
      game_intro.register(puzzle);
      game_intro.build();
      user.get = get_cache;
    });

    it('should not add free powerup if there already there', function (done) {
      var tut = GC.app.tutorial,
        user = GC.app.user,
        get_cache = user.get,
        cache = tut.build,
        cache_free_goods = user.addFreeGoods,
        puzzle = {
          model: {
            pauseTimers: function () {},
            get: function (param) {
              if (param === 'type') {
                return 'classic';
              }
            }
          },
          grid: {
            tiles: [],
            setHandleEvents: function () {}
          },
          hud: {
            powerups: {
              btn_hint: {
                setCount: function () {}
              }
            },
            goals_hud: {
              left_view: {
                limit_view: {}
              }
            }
          },
          getContextForTutorial: function () {
            return [{}, {}];
          }
        };

      user.get = function (key) {
        if (key.startsWith('inventory_')) {
          return 5;
        } else {
          return [];
        }
      };

      user.addFreeGoods = function (item, count, source) {
        user.addFreeGoods = cache_free_goods;
        assert.strictEqual('hint', item);
        assert.strictEqual(3, count);
        assert.strictEqual('first_time_bonus', source);
        done('error');
      };

      tut.build = function (opts) {
        tut.build = cache;
        opts.positions.hint_intro.before();
      };

      ms_no = 1;
      completed_flag = false;
      game_intro.register(puzzle);
      game_intro.build();
      done();
      user.get = get_cache;
    });

    it('before cb of half hint should add half ' +
      'hint as free good', function (done) {
      var tut = GC.app.tutorial,
        user = GC.app.user,
        cache = tut.build,
        get_cache = user.get,
        cache_free_goods = user.addFreeGoods,
        puzzle = {
          model: {
            pauseTimers: function () {},
            get: function (params) {
              if (params === 'type') {
                return 'classic';
              }
            }
          },
          grid: {
            tiles: [],
            setHandleEvents: function () {}
          },
          hud: {
            powerups: {
              btn_half_hint: {
                setCount: function () {}
              }
            },
            goals_hud: {
              left_view: {
                limit_view: {}
              }
            }
          },
          getContextForTutorial: function () {
            return [{}, {}];
          }
        };

      user.get = function (key) {
        if (key.startsWith('inventory_')) {
          return 0;
        } else {
          return [];
        }
      };

      user.addFreeGoods = function (item, count, source) {
        user.addFreeGoods = cache_free_goods;
        assert.strictEqual('half_hint', item);
        assert.strictEqual(3, count);
        assert.strictEqual('first_time_bonus', source);
        done();
      };

      tut.build = function (opts) {
        tut.build = cache;
        opts.positions.half_hint_intro.before();
      };

      ms_no = 1;
      completed_flag = false;
      game_intro.register(puzzle);
      game_intro.build();
      user.get = get_cache;
    });
    it('before cb of swoosh should add swoosh as free good', function (done) {
      var tut = GC.app.tutorial,
        user = GC.app.user,
        get_cache = user.get,
        cache = tut.build,
        cache_free_goods = user.addFreeGoods,
        puzzle = {
          model: {
            pauseTimers: function () {},
            get: function (params) {
              if (params === 'type') {
                return 'classic';
              }
            }
          },
          grid: {
            tiles: [],
            setHandleEvents: function () {}
          },
          hud: {
            powerups: {
              btn_hint: {
                setCount: function () {}
              },
              btn_half_hint: {
                setCount: function () {}
              },
              btn_magnet: {
                setCount: function () {}
              },
              btn_swoosh: {
                setCount: function () {}
              }
            },
            goals_hud: {
              left_view: {
                limit_view: {}
              }
            }
          },
          getContextForTutorial: function () {
            return [{}, {}];
          }
        };

      user.get = function (key) {
        if (key.startsWith('inventory_')) {
          return 0;
        } else {
          return [];
        }
      };

      user.addFreeGoods = function (item, count, source) {
        user.addFreeGoods = cache_free_goods;
        assert.strictEqual('swoosh', item);
        assert.strictEqual(3, count);
        assert.strictEqual('first_time_bonus', source);
        done();
      };

      tut.build = function (opts) {
        tut.build = cache;
        opts.positions.swoosh_intro.before();
      };

      ms_no = 1;
      completed_flag = false;
      game_intro.register(puzzle);
      game_intro.build();
      user.get = get_cache;
    });

    it('should emit event if context is there in before tut', function (done) {
      var tut = GC.app.tutorial,
        cache = tut.build,
        event_emitted = false,
        user = GC.app.user,
        cache_free_goods = user.addFreeGoods,
        puzzle = {
          model: {
            pauseTimers: function () {},
            get: function (params) {
              if (params === 'type') {
                return 'classic';
              }
            },
            emit: function () {
              event_emitted = true;
            }
          },
          grid: {
            tiles: [],
            setHandleEvents: function () {}
          },
          hud: {
            powerups: {
              btn_hint: {
                setCount: function () {}
              },
              btn_half_hint: {
                setCount: function () {}
              },
              btn_magnet: {
                setCount: function () {}
              },
              btn_swoosh: {
                setCount: function () {}
              }
            },
            goals_hud:
            {
              left_view: {
                limit_view: {}
              }
            }
          },
          getContextForTutorial: function () {
            return [{}, {}];
          }
        };

      user.addFreeGoods = function () {};

      tut.build = function (opts) {
        tut.build = cache;
        opts.positions.joker_intro.before();
      };

      ms_no = 1;
      completed_flag = false;
      game_intro.register(puzzle);
      game_intro.build();

      if (event_emitted) {
        done();
      }
      user.addFreeGoods = cache_free_goods;
    });

    it('should emit event if context is there in before' +
      'tut for swap', function (done) {
      var tut = GC.app.tutorial,
        cache = tut.build,
        event_emitted = false,
        user = GC.app.user,
        cache_free_goods = user.addFreeGoods,
        puzzle = {
          model: {
            pauseTimers: function () {},
            get: function (params) {
              if (params === 'type') {
                return 'classic';
              }
            },
            emit: function () {
              event_emitted = true;
            }
          },
          grid: {
            tiles: [],
            setHandleEvents: function () {}
          },
          hud: {
            powerups: {
              btn_hint: {
                setCount: function () {}
              },
              btn_half_hint: {
                setCount: function () {}
              },
              btn_magnet: {
                setCount: function () {}
              },
              btn_swap: {
                setCount: function () {}
              }
            },
            goals_hud:
            {
              left_view: {
                limit_view: {}
              }
            }
          },
          getContextForTutorial: function () {
            return [{}];
          }
        };

      user.addFreeGoods = function () {};

      tut.build = function (opts) {
        tut.build = cache;
        opts.positions.swap_select_tiles.before();
      };

      ms_no = 1;
      completed_flag = false;
      game_intro.register(puzzle);
      game_intro.build();

      if (event_emitted) {
        done();
      }
      user.addFreeGoods = cache_free_goods;
    });

    it('tutorial after should resume timers', function (done) {
      var puzzle = {
          model: {
            get: function (params) {
              if (params === 'type') {
                return 'classic';
              }
            },
            emit: function () {},
            resumeTimers: done
          },
          grid: {
            tiles: [],
            setHandleEvents: function () {}
          },
          hud: {
            powerups: {},
            goals_hud:
            {
              left_view: {
                limit_view: {}
              }
            }
          },
          getContextForTutorial: function () {
            return [{}, {}];
          }
        };

      GC.app.user.push = function () {};
      ms_no = 1;
      completed_flag = false;
      game_intro.register(puzzle);
      game_intro.build(20);
      game_intro._common.after();
    });

    it('should return false if dynamic powerup and ' +
      'no context', function (done) {
      var puzzle = {
          model: {
            get: function (param) {
              if (param === 'type') {
                return 'classic';
              }
            },
            resumeTimers: done
          },
          grid: {
            tiles: [],
            setHandleEvents: function () {}
          },
          hud: {
            powerups: {}
          },
          getContextForTutorial: function () {
            return [];
          }
        };

      GC.app.user.push = function () {};
      ms_no = 1;
      completed_flag = true;
      game_intro.register(puzzle);
      done(!game_intro.build(20) ? undefined : 'error');
    });

    it('tutorial after should not push powerup', function (done) {
      var puzzle = {
          model: {
            get: function (params) {
              if (params === 'type') {
                return 'classic';
              }
            },
            emit: function () {},
            resumeTimers: done
          },
          grid: {
            tiles: [],
            setHandleEvents: function () {}
          },
          hud: {
            powerups: {},
            goals_hud:
            {
              left_view: {
                limit_view: {}
              }
            }
          },
          getContextForTutorial: function () {
            return [{}, {}];
          }
        };

      GC.app.user.push = function () {
        done('error');
      };
      ms_no = 1;
      completed_flag = false;
      game_intro.register(puzzle);
      game_intro.build(22);
      game_intro._common.after();
    });

    describe('getContext()', function () {
      it('should return tile view for passed position', function () {
        var puzzle = {
            model: {
              get: function (params) {
                if (params === 'type') {
                  return 'classic';
                }
              }
            },
            grid: {
              tiles: [[['tile_a', 'tile_b']]]
            },
            hud: {
              powerups: {},
              goals_hud:
              {
                left_view: {
                  limit_view: {}
                }
              }
            },
            getContextForTutorial: function () {
              return [{}, {}];
            }
          };

        ms_no = 1;
        completed_flag = false;
        game_intro.register(puzzle);
        game_intro.build();
        assert.strictEqual('tile_b', game_intro._getContext(0, 0, 1));
      });

      it('should return null if tile not present in position', function () {
        var puzzle = {
            model: {
              get: function (param) {
                if (param === 'type') {
                  return 'classic';
                }
              }
            },
            grid: {
              tiles: [[['tile_a']]]
            },
            hud: {
              powerups: {},
              goals_hud:
              {
                left_view: {
                  limit_view: {}
                }
              }
            },
            getContextForTutorial: function () {
              return [{}, {}];
            }
          };

        ms_no = 1;
        completed_flag = false;
        game_intro.register(puzzle);
        game_intro.build();
        assert.strictEqual(null, game_intro._getContext(0, 0, 1));
      });

      it('should return model id model flag is true', function () {
        var puzzle = {
            model: {
              get: function (params) {
                if (params === 'type') {
                  return 'classic';
                }
              }
            },
            grid: {
              tiles: [[['tile_a', 'tile_b']]]
            },
            hud: {
              powerups: {},
              goals_hud:
              {
                left_view: {
                  limit_view: {}
                }
              }
            },
            getContextForTutorial: function () {
              return [{}, {}];
            }
          };

        ms_no = 1;
        completed_flag = false;
        game_intro.register(puzzle);
        game_intro.build();
        assert.strictEqual(puzzle.grid.tiles[0][0][1].model,
          game_intro._getContext(0, 0, 1, true));
      });
    });
  });

  describe('isBonus()', function () {
    it('should return true if the puzzle is bonus', function () {
      var puzzle = {
        model: {
          get: function (param) {
            if (param === 'bonus_level') {
              return 1;
            } else if (param === 'type') {
              return 'memory';
            }
          }
        },
        grid: {
          tiles: [[['tile_a', 'tile_b']]]
        },
        hud: {
          powerups: {},
          goals_hud:
          {
            left_view: {
              limit_view: {}
            }
          }
        },
        getContextForTutorial: function () {
          return [{}, {}];
        }
      };

      ms_no = 'memory';
      game_intro.register(puzzle);
      assert.strictEqual(true, game_intro.isBonus());
    });
  });
  describe('isCompleted()', function () {
    it('should return true for non intro level', function () {
      var puzzle = {
        model: {
          get: function (params) {
            if (params === 'type') {
              return 'classic';
            }
          }
        },
        grid: {
          tiles: [[['tile_a', 'tile_b']]]
        },
        hud: {
          powerups: {},
          goals_hud:
          {
            left_view: {
              limit_view: {}
            }
          }
        },
        getContextForTutorial: function () {
          return [{}, {}];
        }
      };

      game_intro.register(puzzle);
      ms_no = 8;
      assert.strictEqual(true, game_intro.isCompleted());
    });

    it('should return true if intro level and completed', function () {
      ms_no = 1;
      completed_flag = true;
      assert.strictEqual(true, game_intro.isCompleted());
      ms_no = 10;
      assert.strictEqual(true, game_intro.isCompleted());
    });

    it('should return false if tutorial level and not completed', function () {
      ms_no = 1;
      completed_flag = false;
      assert.strictEqual(false, game_intro.isCompleted());
    });

    describe('getLastTutorialData', function () {
      it('should return empty arr if no data present', function () {
        game_intro.isCompleted();
        assert.strictEqual(false, game_intro.getLastTutorialData(8));
      });

      it('should return last id from game intro', function () {
        game_intro._puzzle = {
          model: {
            get: function () {}
          }
        };

        game_intro.isCompleted();
        assert.strictEqual('match_pair', game_intro.getLastTutorialData(1));
      });
    });
  });

  describe('getTutorialKey', function () {
    it('should return old_ms for bonus' +
      'if tutorial has unlockables', function () {
        var cache = game_intro.getLastTutorialData,
          cache_bonus = game_intro.isBonus,
          puzzle = {
            model: {
              get: function (param) {
                if (param === 'type') {
                  return 'memory';
                }
              }
            },
            grid: {
              tiles: [],
              setHandleEvents: function () { }
            },
            hud: {
              powerups: {},
              goals_hud:
              {
                left_view: {
                  limit_view: {}
                }
              }
            },
            getContextForTutorial: function () {
              return [{}, {}];
            }
          };

        GC.app.user.getMs = function (param) {
          if (param === 'curr') {
            return 15;
          } else if (param === 'max') {
            return 30;
          }
        };
        completed_flag = true;
        game_intro.getLastTutorialData = function () {
          game_intro.getLastTutorialData = cache;
          return true;
        };
        game_intro.isBonus = function () {
          return true;
        };
        game_intro.register(puzzle);
        game_intro.build();
        assert.strictEqual(27, game_intro.getTutorialKey(20));
        game_intro.isBonus = cache_bonus;
      });

    it('should return type for bonus' +
      'if tutorial not completed', function () {
        var cache = game_intro.getLastTutorialData,
          cache_bonus = game_intro.isBonus,
          puzzle = {
            model: {
              get: function (param) {
                if (param === 'type') {
                  return 'memory';
                }
              },
              emit: function () {
              }
            },
            grid: {
              tiles: [],
              setHandleEvents: function () { }
            },
            hud: {
              powerups: {},
              goals_hud:
              {
                left_view: {
                  limit_view: {}
                }
              }
            },
            getContextForTutorial: function () {
              return [{}, {}];
            }
          };

        completed_flag = false;
        game_intro.getLastTutorialData = function () {
          game_intro.getLastTutorialData = cache;
          return true;
        };
        game_intro.isBonus = function () {
          return true;
        };
        game_intro.register(puzzle);
        game_intro.build();
        assert.strictEqual('memory', game_intro.getTutorialKey());
        game_intro.isBonus = cache_bonus;
      });

    it('should return ms for bonus levels with' +
      'bonus intro tutorial is completed', function () {
        var cache = game_intro.getLastTutorialData,
          cache_bonus = game_intro.isBonus,
          puzzle = {
            model: {
              get: function (param) {
                if (param === 'type') {
                  return 'memory';
                }
              }
            },
            grid: {
              tiles: [],
              setHandleEvents: function () { }
            },
            hud: {
              powerups: {},
              goals_hud:
              {
                left_view: {
                  limit_view: {}
                }
              }
            },
            getContextForTutorial: function () {
              return [{}, {}];
            }
          };

        ms_no = 12;
        completed_flag = true;
        game_intro.getLastTutorialData = function () {
          game_intro.getLastTutorialData = cache;
          return true;
        };
        GC.app.user.getMs = function (param) {
          if (param === 'max') {
            return 12;
          }
        };
        game_intro.isBonus = function () {
          return true;
        };
        game_intro.register(puzzle);
        game_intro.build();
        assert.strictEqual(12, game_intro.getTutorialKey());
        game_intro.isBonus = cache_bonus;
      });
  });
  describe('getOldTutorialMs', function () {
      it('should return an old ms', function () {
        var cache = game_intro.getTutorialKey,
          cache_get = GC.app.user.get,
          puzzle = {
            model: {
              get: function (param) {
                if (param === 'type') {
                  return 'classic';
                }
              }
            },
            grid: {
              tiles: [],
              setHandleEvents: function () { }
            },
            hud: {
              powerups: {},
              goals_hud:
              {
                left_view: {
                  limit_view: {}
                }
              }
            },
            getContextForTutorial: function () {
              return [{}, {}];
            }
          };

        game_intro.getTutorialKey = function () {
          return 10;
        };
        GC.app.user.get = function (key) {
          if (key === 'unlocked_powerups') {
            return [];
          } else if (key === 'last_powerup_info') {
            return [];
          }
        };
        game_intro.register(puzzle);
        assert.strictEqual(5, game_intro.getOldTutorialMs('classic'));
        game_intro.getTutorialKey = cache;
        GC.app.user.get = cache_get;
      });

      it('should not return ms if not unlocked', function () {
        var cache = game_intro.getTutorialKey,
          cache_get = GC.app.user.get,
          puzzle = {
            model: {
              get: function (param) {
                if (param === 'type') {
                  return 'classic';
                }
              }
            },
            grid: {
              tiles: [],
              setHandleEvents: function () { }
            },
            hud: {
              powerups: {},
              goals_hud:
              {
                left_view: {
                  limit_view: {}
                }
              }
            },
            getContextForTutorial: function () {
              return [{}, {}];
            }
          };

        game_intro.getTutorialKey = function () {
          return 10;
        };
        GC.app.user.get = function (key) {
          if (key === 'unlocked_powerups') {
            return ['joker', 'shuffle'];
          } else {
            return [];
          }
        };
        game_intro.register(puzzle);
        assert.strictEqual(game_intro.getOldTutorialMs('classic'));
        game_intro.getTutorialKey = cache;
        GC.app.user.get = cache_get;
      });
    });
});
