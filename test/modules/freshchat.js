/* global Freshchat, User
*/

jsio('import quest.models.user as User');
jsio('import freshchat as freshchatPlugin');

describe('Freshchat Module', function () {
  'use strict';

  var freshchat,
    prepare = function () {
      GC.app = {
        user: new User()
      };

      jsio('import DevkitHelper.style as style');
      jsio('import src.modules.freshchat as Freshchat');
    };

  before(prepare);

  describe('showSupport()', function () {
    it('should invoke quest freshchat showsupport', function () {
      freshchat = new Freshchat(function (class_name, function_name, params) {
        assert.deepEqual(class_name, freshchat);
        assert.strictEqual(function_name, 'showSupport');
        assert.deepEqual(params, ['testing']);
      });

      freshchat.showSupport('testing');
    });
  });
});
