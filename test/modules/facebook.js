/* global facebook, User, FB, GameFacebook*/
jsio('import src.modules.facebook as GameFacebook');
jsio('import quest.modules.facebook as facebook');
jsio('import facebook as FB');

describe('Facebook Module', function () {
  'use strict';

  var init = function () {
    jsio('import quest.models.user as User');
    GC.app.user = new User();
    FB.AppEvents = {
      logEvent: function () {
      }
    };
  };

  before(init);

  describe('firstTap', function () {
    it('should log to facebook for first tap', function (done) {
      var app_cache = FB.AppEvents.logEvent;

      FB.AppEvents.logEvent = function () {
        FB.logEvent = app_cache;
        done();
      };
      facebook.firstTap();
    });
  });

  describe('tutorialAction', function () {
    it('should log tutorials for first milestone', function () {
      var app_cache = FB.AppEvents.logEvent;

      GC.app.user.set('max_ms', 1);
      FB.AppEvents.logEvent = function (data) {
        FB.logEvent = app_cache;
        assert.strictEqual(data, 'tutorial_welcome_to_mahjong');
      };
      facebook.tutorialAction({
        data: {
          tut_id: 'welcome_to_mahjong'
        }
      });
    });

    it('should call supr log tutorials for other milestone', function (done) {
      var app_cache = FB.AppEvents.logEvent,
        Base = function (ctxt, fn) {
          assert.strictEqual(fn, 'tutorialAction');
          done();
        },
        game_fb = new GameFacebook(Base);

      GC.app.user.set('max_ms', 5);
      FB.AppEvents.logEvent = function (data) {
        FB.logEvent = app_cache;
        assert.strictEqual(data, 'tutorial_welcome_to_mahjong');
      };
      game_fb.tutorialAction({
        data: {
          tut_id: 'welcome_to_mahjong'
        }
      });
    });
  });

  describe('levelFirstAfter', function () {
    it('should log event for level-first-next', function (done) {
      var app_cache = FB.AppEvents.logEvent;

      FB.AppEvents.logEvent = function (param) {
        FB.logEvent = app_cache;
        if (param === 'level_first_next') {
          done();
        }
      };
      facebook.levelFirstAfter('next');
    });
  });
  describe('firstPairMatch', function () {
    it('should log to facebook for first pair match', function (done) {
      var app_cache = FB.AppEvents.logEvent;

      FB.AppEvents.logEvent = function () {
        FB.logEvent = app_cache;
        done();
      };
      facebook.firstPairMatch();
    });
  });
});
