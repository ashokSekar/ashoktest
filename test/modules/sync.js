/* global _, Sync, User, puzzle_module */

jsio('import util.underscore as _');
jsio('import quest.models.user as User');
jsio('import quest.modules.puzzle as puzzle_module');

describe('Sync Module', function () {
  'use strict';

  var sync,
    prepare = function () {
      GC.app = {
        user: new User()
      };

      jsio('import src.modules.sync as Sync');
      sync = new Sync(function () {});
    };

  before(prepare);

  describe('onSync()', function () {
    it('should set bonus map data if response and no error', function () {
      puzzle_module.setBonusMapData = function (data) {
          assert.strictEqual(true, _.isEqual({
            id: 'three_match'
          }, data));
        };
      sync.onSync(function () {}, null, {
        bonus_map: {
          id: 'three_match'
        }
      });
    });
    it('should not set bonus map data if response and error', function (done) {
      puzzle_module.setBonusMapData = function () {
        done('err');
      };
      sync.onSync(function () {}, 'error', {
        bonus_map: {
          id: 'three_match'
        }
      });
      done();
    });
  });
});
