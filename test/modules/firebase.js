/* global firebase, GameFirebase, User*/
jsio('import firebase as firebase');
jsio('import src.modules.firebase as GameFirebase');
jsio('import quest.models.user as User');
jsio('import quest.modules.firebase as firebaseSuper');

describe('firebase', function () {
  'use strict';

  var fireBase,
    data = {
      id: '',
      data: {
        tut_id: 'welcome_to_mahjong'
      }},
    init = function () {
    fireBase = new GameFirebase(function () {});
    GC.app.user = new User();
  };

  before(init);

  describe('tutorialAction', function () {
    it('if max_ms is 1 config data id should be tutorial id', function (done) {
      GC.app.user.setMs('max', 1);

      fireBase.tutorialAction(data);
      if (data.id === 'welcome_to_mahjong') {
        done();
      }
    });
    it('if max_ms is 1 and config does not contain tutorial', function (done) {
      GC.app.user.setMs('max', 1);
      data = {
        id: '',
        data: {
          tut_id: 'other_tutorial'
        }};
      fireBase.tutorialAction(data);
      if (data.id !== 'welcome_to_mahjong') {
        done();
      }
    });
    it('if max_ms not 1 config data id should be tutorial id', function (done) {
      GC.app.user.setMs('max', 5);
      data = {id: '',
        data: {
          tut_id: 'welcome_to_mahjong'
        }};
      fireBase.tutorialAction(data);
      if (data.id === '') {
        done();
      }
    });
    it('should invoke quest firebase tutorialAction', function (done) {
      fireBase = new GameFirebase(function (class_name, function_name, params) {
        assert.deepEqual(class_name, fireBase);
        assert.strictEqual(function_name, 'tutorialAction');
        assert.deepEqual(params, [{id: 'welcome_to_mahjong',
          data: {
            tut_id: 'welcome_to_mahjong'
          }}]);
        done();
      });
      GC.app.user.setMs('max', 1);
      fireBase.tutorialAction(data);
    });
  });
  describe('firstTap', function () {
    it('should log to firebase', function (done) {
      var cache = firebase.logEvent;

      firebase.logEvent = function () {
        firebase.logEvent = cache;
        done();
      };
      fireBase.firstTap();
    });
  });
  describe('firstPairMatch', function () {
    it('should log to firebase', function (done) {
      var cache = firebase.logEvent;

      firebase.logEvent = function () {
        firebase.logEvent = cache;
        done();
      };
      fireBase.firstPairMatch();
    });
  });

  describe('outOfLives', function () {
    it('should log out_of_lives', function (done) {
      var cache = firebase.logEvent;

      firebase.logEvent = function (event) {
        if (event === 'out_of_lives') {
          firebase.logEvent = cache;
          done();
        }
      };
      fireBase.outOfLives();
    });
  });
});
