/* global _, puzzle, User, storage, Promise, dh_timer, style,
  popup_manager, image, setImmediate */

jsio('import util.underscore as _');
jsio('import quest.modules.puzzle as puzzle');
jsio('import quest.models.user as User');
jsio('import DevkitHelper.storage as storage');
jsio('import DevkitHelper.timer as dh_timer');
jsio('import quest.lib.bluebird as Promise');
jsio('import quest.modules.puzzle as QuestPuzzle');
jsio('import DevkitHelper.style as style');
jsio('import resources.data.config as config');
jsio('import quest.modules.image as image');
jsio('import quest.modules.popup_manager as popup_manager');
describe('Puzzle Module', function () {
  'use strict';

  var init = function () {
    GC.app.user = new User();

    style.get = function () {
      return {};
    };
  };

  before(init);

  describe('setBonusMapData', function () {
    it('should not set bonus map data if bonus screen set', function () {
      GC.app.user.set('screen_bonus', true);
      puzzle.setBonusMapData({
        id: 'three_match',
        type: 'three_match',
        time_left: 300
      });
      assert.notStrictEqual(
        {
          id: 'three_match',
          type: 'three_match',
          time_left: 300
        }, puzzle._bonus_map_data);
    });

    it('should set bonus map data if bonus screen set', function () {
      GC.app.user.set('screen_bonus', false);
      puzzle._bonus_map_data = {
        id: 'jewel_collection',
        type: 'jewels',
        time_left: 300
      };
      puzzle.setBonusMapData({
        id: 'three_match',
        type: 'three_match',
        time_left: 300
      });
      assert.strictEqual(true, _.isEqual({
        id: 'three_match',
        type: 'three_match',
        time_left: 300
      }, puzzle._bonus_map_data));
    });

    it('shouldnt set bonus map data if type not supported', function () {
      var curr = puzzle._bonus_map_data;

      GC.app.user.set('screen_bonus', false);

      puzzle.setBonusMapData({
        id: '1234',
        type: 'abcde',
        time_left: 300
      });
      assert.strictEqual(curr, puzzle._bonus_map_data);
    });

    it('should set bonus map data if bonus screen not set', function () {
      var cache = storage.get;

      GC.app.user.set('screen_bonus', false);

      storage.get = function () {
        storage.get = cache;
        return {
          three_match: {
            id: 'three_match',
            max_ms: 20
          }
        };
      };

      puzzle.setBonusMapData({
        id: 'three_match',
        type: 'three_match',
        time_left: 300,
        max_ms: 2
      });
      assert.strictEqual(true, _.isEqual({
        id: 'three_match',
        type: 'three_match',
        time_left: 300,
        max_ms: 2
      }, puzzle._bonus_map_data));
    });
    it('should reset bonus time data if no data', function () {
      GC.app.user.set('screen_bonus', false);
      puzzle.setBonusMapData();
      assert.strictEqual(0, puzzle._duration);
    });
  });

  describe('loadPopupBg', function () {
    it('should set image into popup_bg', function () {
      var cache = image.getAsync;

      puzzle.setBonusMapData();
      image.getAsync = function () {
        image.getAsync = cache;
        return Promise.resolve([1]);
      };
      setImmediate(function () {
        puzzle._loadPopupBg({popup_bg: '',
          type: 'golden'});
        assert.strictEqual(1, puzzle._getPopupBg());
      });
    });
  });

  describe('getUserBonusPuzzle', function () {
    it('should get bonus data', function () {
      var cache = storage.get;

      puzzle._bonus_map_data = {
        id: 'three_match'
      };
      puzzle.setBonusMapData({
        id: 'three_match',
        time_left: 200
      });
      storage.get = function () {
        storage.get = cache;
        return {
          three_match: {
            id: 'three_match'
          }
        };
      };
      puzzle.getUserBonusPuzzle();
    });
  });

  describe('updateBonusMapInterval', function () {
    it('should register timer if not exist', function (done) {
      var cache = dh_timer.register,
        has_cache = dh_timer.has,
        duration = 10;

      dh_timer.has = function () {
        dh_timer.has = has_cache;
        return false;
      };
      dh_timer.register = function (id, cb) {
        dh_timer.register = cache;
        cb();
        done();
      };
      puzzle.updateBonusMapInterval(duration);
    });
    it('should not register timer if exist', function (done) {
      var cache = dh_timer.register,
        has_cache = dh_timer.has,
        duration = 10;

      dh_timer.has = function () {
        dh_timer.has = has_cache;
        return true;
      };
      dh_timer.register = function () {
        done('err');
      };
      puzzle.updateBonusMapInterval(duration);
      dh_timer.register = cache;
      done();
    });
    it('should unregister timer if duration<=1', function (done) {
      var cache = dh_timer.register,
        cache_unregister = dh_timer.unregister,
        has_cache = dh_timer.has;

      dh_timer.has = function () {
        dh_timer.has = has_cache;
        return false;
      };
      dh_timer.register = function (id, cb) {
        dh_timer.register = cache;
        _.defer(function () {
          cb();
        });
      };
      dh_timer.unregister = function (id) {
        dh_timer.unregister = cache_unregister;
        if (id === 'bonus_map_timer') {
          done();
        }
      };
      puzzle.updateBonusMapInterval(0);
    });
  });

  describe('loadBonusMapPuzzle', function () {
    it('should load bonus map data with correct params', function () {
      var cache = puzzle.load,
        cache_resolve = Promise.resolve;

      GC.app.user.set('screen_bonus', false);
      puzzle.setBonusMapData({
        id: 'three_match_a',
        type: 'three_match',
        time_left: 300,
        levels: [[1], [2], [3], [4]]
      });

      Promise.resolve = function (data) {
        Promise.resolve = cache_resolve;
        assert.strictEqual(data.type, 'three_match');
        assert.strictEqual(data.number, 3);
        assert.strictEqual(data.bonus_map, 'three_match_a');
      };

      puzzle.load = function () {
        puzzle.load = cache;
        return {
          then: function (opts) {
            var response;

            if (_.isFunction(opts)) {
              response = opts({});
              return Promise.resolve(response);
            }
          },
          catch: function () {}
        };
      };
      puzzle.loadBonusMapPuzzle(3);
    });

    it('should load bonus map data with custom puzzle data', function () {
      var cache = puzzle.load,
        cache_resolve = Promise.resolve;

      GC.app.user.set('screen_bonus', false);
      puzzle.setBonusMapData({
        id: 'three_match_a',
        type: 'three_match',
        time_left: 300,
        levels: [[1, 3 / 10, {
          time: 189,
          tap: 112,
          abc: 1,
          lock_keys: {}
        }], [2], [3], [4]]
      });

      Promise.resolve = function (data) {
        Promise.resolve = cache_resolve;
        assert.strictEqual(data.type, 'three_match');
        assert.strictEqual(data.number, 1);
        assert.strictEqual(data.time, 189);
        assert.strictEqual(data.tap, 112);
        assert.deepEqual(data.lock_keys, {});
        assert.strictEqual(data.abc, undefined);
        assert.strictEqual(data.bonus_map, 'three_match_a');
      };

      puzzle.load = function () {
        puzzle.load = cache;
        return {
          then: function (opts) {
            var response;

            if (_.isFunction(opts)) {
              response = opts({});
              return Promise.resolve(response);
            }
          },
          catch: function () {}
        };
      };
      puzzle.loadBonusMapPuzzle(1);
    });

    it('should set the goal for golden tile', function () {
      var cache = puzzle.load,
        cache_resolve = Promise.resolve;

      GC.app.user.set('screen_bonus', false);
      puzzle.setBonusMapData({
        id: 'golden',
        type: 'golden',
        time_left: 300,
        levels: [[1], [2], [3], [4]]
      });

      Promise.resolve = function (data) {
        Promise.resolve = cache_resolve;
        assert.deepEqual(data.goal, {
          golden: 1
        });
      };

      puzzle.load = function () {
        puzzle.load = cache;
        return {
          then: function (opts) {
            var response;

            if (_.isFunction(opts)) {
              response = opts({});
              return Promise.resolve(response);
            }
          },
          catch: function () { }
        };
      };
      puzzle.loadBonusMapPuzzle(3);
    });
    it('should catch with no puzzle if failed to load', function () {
      var cache = puzzle.load,
        cache_reject = Promise.reject;

      puzzle.setBonusMapData({
        id: 'three_match',
        type: 'three_match',
        time_left: 300,
        levels: [[1], [2], [3], [4]]
      });
      Promise.reject = function (param) {
        Promise.reject = cache_reject;
        assert.strictEqual(param, 'No Puzzle');
      };
      puzzle.load = function () {
        puzzle.load = cache;
        return {
          then: function (opts) {
            if (_.isFunction(opts)) {
              opts({type: 'three_match'});
              return {
                catch: function (opts) {
                  if (_.isFunction(opts)) {
                    opts();
                  }
                }
              };
            }
          }
        };
      };
      puzzle.loadBonusMapPuzzle(22);
    });
  });
  describe('setUserBonusPuzzle', function () {
    it('should set user storage for bonus_data', function () {
      var cache = storage.set,
       cache_get = storage.get;

      puzzle.setBonusMapData({
        id: 'three_match',
        time_left: 300,
        levels: [[1], [2], [3], [4]]
      });

      storage.get = function () {
        return {
          three_match: {
            id: 'three_match',
            max_ms: 10
          }
        };
      };

      storage.set = function (id, data) {
        storage.set = cache;
        assert.strictEqual(true, _.isEqual(data, {
          three_match: {
            id: 'three_match',
            time_left: 200
          }
        }));
      };
      puzzle.setUserBonusPuzzle({
        id: 'three_match',
        time_left: 200
      });
      storage.get = cache_get;
    });
  });
  describe('getBonusMapType', function () {
    it('should return bonusMap type', function () {
      puzzle._bonus_map_data.type = 'three_match';
      assert.strictEqual(puzzle.getBonusMapType(), 'three_match');
    });
  });
  describe('getBonusMapId', function () {
    it('should return bonusMap Id', function () {
      puzzle._bonus_map_data.id = 'three_match';
      assert.strictEqual(puzzle.getBonusMapId(), 'three_match');
    });
  });

  describe('updateBonusData()', function () {
    it('should invoke parent function with proper data', function (done) {
      var cache = puzzle.getBonusLevel,
        data = {};

      puzzle.getBonusLevel = function () {
        puzzle.getBonusLevel = cache;

        return {
          id: 12
        };
      };

      puzzle.updateBonusData(1, data);
      assert.strictEqual(data.limit_type, 'tap');
      assert.strictEqual(data.type, 'memory');
      done();
    });
  });

  describe('saveHighScore()', function () {
    it('should set high score', function (done) {
      var cache = storage.set;

      storage.set = function () {
        storage.set = cache;
        done();
      };

      puzzle.saveHighScore({});
    });

    it('should not set high score', function (done) {
      var cache = storage.set;

      storage.set = function () {
        done('error');
      };

      puzzle.saveHighScore({bonus_map: true});
      storage.set = cache;
      done();
    });
  });

  describe('showEventPopup()', function () {
    it('should return true', function () {
      var cache = puzzle._bonus_map_data,
        cache_get = puzzle.getUserBonusPuzzle,
        cache_add = popup_manager.add;

      puzzle._bonus_map_data = {};

      puzzle.getUserBonusPuzzle = function () {
        puzzle.getUserBonusPuzzle = cache_get;
        return {
          played: false
        };
      };

      popup_manager.add = function () {
        popup_manager.add = cache_add;
      };

      assert.equal(puzzle.showEventPopup(), true);

      puzzle._bonus_map_data = cache;
    });

    it('should return false', function () {
      var cache = puzzle._bonus_map_data,
        cache_get = puzzle.getUserBonusPuzzle,
        cache_add = popup_manager.add;

      puzzle._bonus_map_data = {};

      puzzle.getUserBonusPuzzle = function () {
        puzzle.getUserBonusPuzzle = cache_get;
        return {
          played: true
        };
      };

      popup_manager.add = function () {
        popup_manager.add = cache_add;
      };

      assert.equal(puzzle.showEventPopup(), false);

      puzzle._bonus_map_data = cache;
    });
  });
});
