/* global BottomHudMap, style, View, ImageView, util*/

jsio('import src.views.bottom_hud_map as BottomHudMap');
jsio('import ui.View as View');
jsio('import ui.ImageView as ImageView');

describe('BottomHudMap View', function () {
  'use strict';

  var bottom_hud_map,
    config_data = {
        STYLE: {
          orientation: 'portrait',
          bound_width: 640,
          bound_height: 960,
          tablet: 0.8,
          phablet: 0.9
        }
      },
      prepare = function () {
        GC.app = {
          view: {
            updateOpts: function () {}
          },
          getCurrentView: function () {
            return new View();
          },
          engine: {
            subscribe: function () {}
          },
          user: {
            getMs: function () {}
          }
        };
        jsio('import DevkitHelper.style as style');
        style.init(config_data.STYLE);
        jsio('import quest.modules.util as util');
        util.getSafeArea = function () {
          return {
            top: 44,
            bottom: 34
          };
        };
      },
      setup_create = function () {
        bottom_hud_map = new BottomHudMap();
      };

  before(prepare);
  beforeEach(setup_create);

  describe('init()', function () {
    it('init', function () {
      assert.deepEqual(['settings', 'achievements', 'tile_store', 'messages'],
        bottom_hud_map._hud_items);
    });
  });
  describe('build()', function () {
    it('should build notification icon in tile store if' +
      'prop not set', function () {
        GC.app.user.get = function (param) {
          if (param === 'tile_notif_opened') {
            return false;
          }
        };
        util.getSafeArea = function () {
          return {
            bottom: 44
          };
        };
        bottom_hud_map.build();
        assert.strictEqual(true,
          bottom_hud_map.notif_icon instanceof ImageView);
      });
    it('should not build notification icon in tile store if' +
    'prop not set', function (done) {
      var cache = bottom_hud_map.notif_icon.updateOpts;

      bottom_hud_map.notif_icon.updateOpts = function () {
        bottom_hud_map.notif_icon.updateOpts = cache;
        done('err');
      };
      GC.app.user.get = function (param) {
        if (param === 'tile_notif_opened') {
          return true;
        }
      };
      bottom_hud_map.build();
      done();
    });
  });
  describe('resetNotif()', function () {
    it('should set the tile_notif_opened value to true' +
      'if item is tile_store', function () {
      var cache = GC.app.user.set;

      bottom_hud_map.notif_icon = new View();
      GC.app.user.set = function (param, value) {
        assert.strictEqual(param, 'tile_notif_opened');
        assert.strictEqual(value, true);
        GC.app.user.set = cache;
      };
      bottom_hud_map.resetNotif('tile_store');
    });
    it('should not set the tile_notif_opened value to true' +
    'if item is not tile_store', function (done) {
      var cache = GC.app.user.set;

      GC.app.user.set = function () {
        GC.app.user.set = cache;
        done('err');
      };
      bottom_hud_map.resetNotif('test');
      done();
    });
  });
});
