/* global PuzzleModel, util_test, ButtonView,
  Powerups, Hud, style, util
*/

jsio('import test.lib.util as util_test');

describe('HUDAction', function () {
  'use strict';

  var hud, puzzle,
    config_data = {
      STYLE: {
        orientation: 'portrait'
      },
      variations: {
        memory: {
          id: 'memory',
          powerups_exclude: []
        },
        partial_memory: {
          id: 'partial_memory',
          powerups_exclude: []
        }
      },
      powerups: [],
      bonus_level_def: [[20, 40], [13, 15], [7, 30]],
      ms_per_chapter: 20,
      lives: {
        max: 5,
        cost_for_max: 10,
        gen: 1800000,
        ad: 1,
        fb: 1
      }
    },
    prepare = function () {
      GC.app = {
        view: {
          updateOpts: function () {}
        },
        engine: {
          subscribe: function () {}
        }
      };

      jsio('import resources.data.config as config');
      util_test.getFromJSIOCache('resources/data/config').exports = config_data;

      util_test.removeFromJSIOCache('devkithelper/src/style');

      jsio('import resources.styles.portrait as portrait');
      util_test.getFromJSIOCache('resources/styles/portrait').exports =
        {};

      jsio('import DevkitHelper.style as style');
      style.init(config_data.STYLE);

      jsio('import quest.ui.ButtonView as ButtonView');
      jsio('import src.models.puzzle as PuzzleModel');
      jsio('import ui.ScoreView as ScoreView');
      jsio('import src.views.powerups as Powerups');
      jsio('import src.views.hud as Hud');
      jsio('import quest.modules.util as util');
      util.getSafeArea = function () {
        return {
          top: 44,
          bottom: 34
        };
      };
    },
    init = function () {
      puzzle = new PuzzleModel();
      hud = new Hud();

      hud.model = puzzle;
    };

  before(prepare);
  beforeEach(init);

  describe('init()', function () {
    it('should create proper child views', function () {
      assert.strictEqual(true, hud.undo_btn instanceof ButtonView);
      assert.strictEqual(true, hud.powerups instanceof Powerups);
    });
  });

  describe('build()', function () {
    it('should set background for hud', function () {
      var cache = util.isiPhoneX;

      util.isiPhoneX = function () {
        util.isiPhoneX = cache;
        return true;
      };
      hud.build(puzzle);
      assert.equal('resources/images/puzzlescreen/hud.png',
       hud.getImage().getURL());
    });

    it('should invoke toggleUndo to render undo', function (done) {
      hud.toggleUndo = done;
      hud.build(puzzle);
    });

    it('should call powerups build with puzzle model', function (done) {
      hud.powerups.build = function (model) {
        done(model instanceof PuzzleModel ? undefined : 'error');
      };
      hud.build(puzzle);
    });

    it('should call clean on model clean', function (done) {
      var cache = hud.powerups.clean;

      hud.powerups.clean = function () {};
      hud.clean = done;
      hud.build(puzzle);
      hud.model.emit('clean');

      hud.powerups.clean = cache;
    });

    it('should add listener for change in last_match', function (done) {
      var cache = hud.toggleUndo;

      hud.toggleUndo = function () {
        done();
        hud.toggleUndo = cache;
      };
      hud.build(puzzle);

      puzzle.emit('change:last_match');
    });
  });

  describe('toggleUndo()', function () {
    it('should invoke enable when unlocked is passed', function () {
      var cache = hud.undo_btn.setState;

      hud.undo_btn.setState = function (state) {
        assert.strictEqual(1, state);
        hud.undo_btn.setState = cache;
      };
      hud.toggleUndo([true]);
    });

    it('should invoke disable', function () {
      var cache = hud.undo_btn.setState;

      hud.undo_btn.setState = function (state) {
        assert.strictEqual(3, state);
        hud.undo_btn.setState = cache;
      };
      hud.toggleUndo();
    });
  });

  describe('undo()', function () {
    it('should emit undo from model', function (done) {
      hud.model.once('undo', done);
      hud.undo();
    });
  });

  describe('clean()', function () {
    it('should call btn onRelease', function (done) {
      hud.undo_btn.onRelease = function () {
        done();
      };
      hud.clean();
    });
  });
});
