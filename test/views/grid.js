/* global TileModel, PuzzleModel, Grid, View, ViewPool,
  util_test, utils, style, User, event_manager, animate:true
*/

jsio('import ui.View as View');
jsio('import test.lib.util as util_test');
jsio('import DevkitHelper.event_manager as event_manager');
jsio('import ui.ViewPool as ViewPool');

describe('Grid', function () {
  'use strict';

  var grid, puzzle, tile,
    config_data = {
      server: 'http://localhost:3000',
      board_data: {
        rows: 5,
        columns: 5,
        depth: 3
      },
      STYLE: {
        orientation: 'portrait',
        bound_width: 640,
        bound_height: 960,
        tablet: 0.8,
        phablet: 0.9
      },
      puzzle_load: {
        retry: 64,
        info_timeout: 1
      },

      max_tiles: [9, 9, 9, 4, 4, 3, 3],
      lives: {
        max: 5,
        gen: 1000
      },
      powerups: {},
      bonus_level_def: [[20, 40], [13, 15], [7, 30]],
      ms_per_chapter: 20,
      variations: {
        memory: {
          id: 'memory',
          powerups_exclude: []
        },
        jewels: {
          id: 'jewels',
          powerups_exclude: []
        },
        partial_memory: {
          id: 'partial_memory',
          powerups_exclude: []
        }
      }
    },
    families = [
      {
        pairs: 1,
        types: 1
      },
      {
        pairs: 4,
        types: 3
      },
      {
        pairs: 1,
        types: 1
      },
      {
        pairs: 1,
        types: 1
      },
      {
        pairs: 3,
        types: 2
      },
      {
        pairs: 1,
        types: 1
      }
    ],
    puzzle_data = {
      total_tiles: 8,
      layout: [
        [2, 0, 0], [0, 2, 0], [2, 2, 0],
        [4, 2, 0], [2, 4, 0], [2, 2, 1],
        [8, 1, 0], [8, 5, 0]],
      id: 'test',
      families: families
    },
    portrait_data = {
      tile_size: {
        gradient: {
          x: 10,
          y: 10
        },
        width: 98,
        height: 130
      },
      grid: {
        height: 740,
        width: 620,
        order: 2,
        layout: 'box',
        centerX: true
      }
    },
    prepare = function () {
      jsio('import resources.data.config as config');
      util_test.getFromJSIOCache('resources/data/config').exports =
        config_data;

      jsio('import quest.modules.util as utils');

      jsio('import resources.styles.portrait as portrait');
      util_test.removeFromJSIOCache('devkithelper/src/style');

      util_test.getFromJSIOCache('resources/styles/portrait').exports =
        portrait_data;

      util_test.getFromJSIOCache('resources/data/config.js')
      .exports.special_faces = {
        golden: 'f_golden'
      };
      jsio('import quest.models.user as User');
      event_manager.register('', []);
      GC.app = {
        view: {
          updateOpts: function () {}
        },
        engine: {
          subscribe: function () {}
        },
        user: new User()
      };
      jsio('import DevkitHelper.style as style');
      style.init(config_data.STYLE);

      jsio('import src.models.puzzle as PuzzleModel');
      jsio('import src.models.tile as TileModel');
    },
    setup_create = function () {
      jsio('import animate');
      jsio('import src.views.grid as Grid');
      puzzle = new PuzzleModel();
      puzzle.set({
        tiles: [[]],
        tile_count: puzzle_data.layout.length,
        rows: 0,
        columns: 0,
        depth: 0,
        last_match: []
      });
      puzzle.loadTiles(puzzle_data.layout);
      grid = new Grid();
      grid.model = puzzle;

      tile = new TileModel();
    },
    setup_destroy = function () {
      util_test.removeFromJSIOCache('src/views/grid.js');
      util_test.removeFromJSIOCache('src/animate.js');
    };

  before(prepare);
  beforeEach(setup_create);
  afterEach(setup_destroy);

  describe('init()', function () {
    it('should create tile pool with count from config', function () {
      assert.strictEqual(75, grid.tile_pool._views.length);
    });
  });

  describe('getTile()', function () {
    it('should create new instance of grid if not present', function (done) {
      var tile;

      if (!tile) {
        grid.build(puzzle);
        tile = grid._getTile();
        if (tile instanceof ViewPool) {
          done();
        }
      }
    });
    it('should return same instance if grid already there', function (done) {
      var tile_another,
        cache;

      grid.build(puzzle);
      cache = puzzle._tiles;
      grid._setTiles(true);
      tile_another = grid._getTile();
      if (!(tile_another instanceof ViewPool)) {
        puzzle._setTiles(cache);
        done();
      }
    });
  });

  describe('build()', function () {
    it('should add listener puzzle-loaded', function (done) {
      grid.load = function () {
        done();
      };
      grid.build(puzzle);

      puzzle.emit('puzzle-loaded', puzzle);
    });

    it('should add listener layout-changed', function (done) {
      grid.updateTilePosition = function () {
        done();
      };
      grid.build(puzzle);

      puzzle.emit('layout-changed', puzzle);
    });

    it('should add listener render-tiles', function (done) {
      grid.renderAllTiles = function () {
        done();
      };
      grid.build(puzzle);

      puzzle.emit('render-tiles');
    });

    it('should add listener render-tile', function (done) {
      grid.renderTile = function () {
        done();
      };
      grid.build(puzzle);

      puzzle.emit('render-tile');
    });

    it('should add listener tile-matched for changeTileViews', function (done) {
      grid.changeTileViews = function () {
        done();
      };
      grid.resize = function () {};
      grid.build(puzzle);

      puzzle.emit('tiles-update');
    });

    it('should add listener for change of last_match for resize',
      function (done) {
      grid.changeTileViews = function () {};
      grid.resize = function () {
        done();
      };
      grid.build(puzzle);
      puzzle.emit('change:last_match');
    });

    it('should init tile property', function () {
      grid.tiles = [1];
      grid.build(puzzle);
      assert.deepEqual([], grid.tiles);
    });
    it('should set the y axis space for the iPhoneX', function () {
      var cache = utils.isiPhoneX;

      utils.isiPhoneX = function () {
        utils.isiPhoneX = cache;
        return true;
      };
      grid.build(puzzle);
      assert.strictEqual(grid.open_slot.style.y, 20);
    });
    it('should set the y axis to zero for the' +
      'devices except iPhoneX', function () {
      var cache = utils.isiPhoneX;

      utils.isiPhoneX = function () {
        utils.isiPhoneX = cache;
        return false;
      };
      grid.build(puzzle);
      assert.strictEqual(grid.open_slot.style.y, 0);
    });
  });

  describe('load()', function () {
    it('should set proper value of row and ' +
      'columns for grid', function () {
      grid.resize = function () {};
      grid.load();

      assert.strictEqual(grid.grid_view._rows, 7);
      assert.strictEqual(grid.grid_view._cols, 10);
    });

    it('should set proper height and width of grid', function () {
      grid.resize = function () {};
      grid.load();
      assert.strictEqual(grid.grid_view.style.width, 490);
      assert.strictEqual(grid.grid_view.style.height, 455);
    });

    it('should set proper margin of grid', function () {
      grid.resize = function () {};
      grid.load();
      assert.strictEqual(grid.grid_view._opts.verticalMargin, 0);
      assert.strictEqual(grid.grid_view._opts.horizontalMargin, 0);
    });

    it('should call resize', function (done) {
      grid.resize = done;
      grid.load();
    });

    it('should show open_slot if powerup enabled', function (done) {
      grid.resize = function () {};
      grid.model.set('open_slot', true, true);
      grid.open_slot.show = done;
      grid.load();
    });
  });

  describe('updateSlot()', function () {
    it('should animate selected tile to open slot and clean', function (done) {
      var view = new View(),
        cache = grid.open_tile.updateOpts,
        cache_build = grid.open_tile.build;

      grid.tiles = [[[view]]];
      grid.open_tile.build = function () {
        grid.open_tile.build = cache_build;
      };
      grid.open_tile.updateOpts = function (opts) {
        grid.open_tile.updateOpts = cache;
        assert.deepEqual(opts, {
          visible: true,
          scale: 1,
          opacity: 1
        });
        done();
      };

      grid.updateSlot({
        getCoordinates: function () {
          return [0, 0, 0];
        },
        emit: function (evt) {
          assert.strictEqual(evt, 'clean');
        },
        removeAllListeners: function () {}
      });
    });

    it('should hide open_tile if val is null', function (done) {
      var cache = grid.open_tile.hide;

      grid.open_tile.hide = function () {
        grid.open_tile.hide = cache;
        done();
      };

      grid.updateSlot(null);
    });
  });

  describe('renderAllTiles', function () {
    it('should invoke renderTile for each tiles', function () {
      var count = 0;

      grid.renderTile = function () {
        count++;
      };

      grid.renderAllTiles();
      assert.strictEqual(puzzle_data.layout.length, count);
    });

    it('should invoke setHandleEvents to enable touch actions',
      function (done) {
      var cache = grid.setHandleEvents;

      grid.setHandleEvents = function (p1, p2) {
        assert.strictEqual(p1, true);
        assert.strictEqual(p2, false);
        done();
        grid.setHandleEvents = cache;
      };
      grid.build(puzzle);
      grid.renderAllTiles();
    });
  });

  describe('renderTile', function () {
    it('should obtain view for a tile', function (done) {
      grid.tile_pool.obtainView = function () {
        done();
        return {
          face: {
            updateOpts: function () {}
          },
          build: function () {}
        };
      };
      grid.build(puzzle);
      grid.resize = function () {};
      grid.load();
      puzzle.eachCell(function (model) {
        model.set('face', 'test_val');
      });
      grid.renderTile(puzzle.get('tiles')[0][2][0]);
    });

    it('should invoke set tile with obtained tile view and ' +
      'cordinates', function (done) {
        grid.tile_pool.obtainView = function () {
          return {
            build: function () {},
            id: 11
          };
        };
        grid.setTile = function (x, y, z, tile) {
          assert.deepEqual([2, 0, 0], [x, y, z]);
          done(tile.id === 11 ? undefined : 'error');
        };

        grid.build(puzzle);
        grid.resize = function () {};
        grid.load();
        grid.renderTile(puzzle.get('tiles')[0][2][0]);
      }
    );

    it('should invoke changeTileViews to update view', function (done) {
      var model = puzzle.get('tiles')[0][2][0];

      grid.tile_pool.obtainView = function () {
        return {
          face: {
            updateOpts: function () {}
          },
          build: function () {}
        };
      };

      grid.build(puzzle);
      grid.resize = function () {};
      grid.load();

      grid.changeTileViews = function (obj) {
        assert.deepEqual([model], obj);
        done();
      };
      grid.renderTile(model);
    });
  });

  describe('setTile()', function () {
    it('should add passed tile view to corresponding position', function () {
      grid.tiles = [];
      grid.setTile(1, 1, 1, 'tile_a');
      assert.strictEqual('tile_a', grid.tiles[1][1][1]);
    });
  });

  describe('changeTileViews', function () {
    it('should update state of all tiles if no params passed', function (done) {
      tile.on('selected', function (state) {
        assert.strictEqual(state, 1);
        done();
      });

      tile.isFree = function () {
        return true;
      };

      grid.model.eachCell = function (cb) {
        done();
        return cb(tile);
      };

      grid.changeTileViews();
    });

    it('should update state of those tile which are passed',
      function (done) {
      tile.on('selected', function (state) {
        assert.strictEqual(state, 1);
        done();
      });

      tile.isFree = function () {
        done();
        return true;
      };

      grid.model.getTile = function () {
        return tile;
      };

      grid.model.eachCell = function () {
        done('error');
      };

      grid.changeTileViews([[0, 0, 0]]);
    });

    it('should update state of passed tiles except selected',
      function (done) {
        tile.set('state', 2, true);
        tile.isFree = function () {
          done('error');
          return true;
        };

        grid.model.getTile = function () {
          return tile;
        };

        grid.model.eachCell = function () {
          done('error');
        };

        grid.changeTileViews([[0, 0, 0]], true);
        assert.strictEqual(2, tile.get('state'));
        done();
      }
    );

    it('should update state of those tile which are passed if ' +
      'there is a reference', function (done) {
      tile.on('selected', function (state) {
        assert.strictEqual(state, 1);
        done();
      });

      tile.isFree = function () {
        done();
        return true;
      };

      grid.model.getTile = function () {
        return;
      };

      grid.model.isFree = function () {
        done('error');
      };

      grid.model.eachCell = function () {
        done('error');
      };

      grid.changeTileViews([[0, 0, 0]]);
      done();
    });
  });

  describe('updateTilePosition()', function () {
    it('should update views in prev position with data for new ' +
      'position', function () {
        var model = grid.model,
          view = grid.tile_pool.obtainView({
            row: 1,
            col: 1
          });

        tile.set({
          x: 1,
          y: 1,
          z: 2
        });
        model.set('tiles', [null, null, [null, [null, tile]]]);
        grid.tiles = [];
        grid.setTile(1, 1, 2, view);
        assert.strictEqual(grid.tiles[1][1][2].uid, view.uid);
        tile.set({
          x: 3,
          y: 2,
          z: 0
        });
        model.set('tiles', [[null, null, null, [null, null, tile]]]);
        grid.updateTilePosition();
        assert.strictEqual(grid.tiles[3][2][0].uid, view.uid);
      }
    );

    it('should call resize when passed', function (done) {
      var model = grid.model,
        view = grid.tile_pool.obtainView({
          row: 1,
          col: 1
        });

      grid.resize = done;
      tile.set({
        x: 1,
        y: 1,
        z: 2
      });
      model.set('tiles', [null, null, [null, [null, tile]]]);
      grid.tiles = [];
      grid.setTile(1, 1, 2, view);
      assert.strictEqual(grid.tiles[1][1][2].uid, view.uid);
      tile.set({
        x: 3,
        y: 2,
        z: 0
      });
      model.set('tiles', [[null, null, null, [null, null, tile]]]);
      grid.updateTilePosition(true);
    });
    it('should not call resize when passed false', function (done) {
      var model = grid.model,
        view = grid.tile_pool.obtainView({
          row: 1,
          col: 1
        });

      grid.resize = function () {
        done('error');
      };
      tile.set({
        x: 1,
        y: 1,
        z: 2
      });
      model.set('tiles', [null, null, [null, [null, tile]]]);
      grid.tiles = [];
      grid.setTile(1, 1, 2, view);
      assert.strictEqual(grid.tiles[1][1][2].uid, view.uid);
      tile.set({
        x: 3,
        y: 2,
        z: 0
      });
      model.set('tiles', [[null, null, null, [null, null, tile]]]);
      grid.updateTilePosition(false);
      done();
    });
  });

  describe('resize', function () {
    var animate_mock = {
        now: function () {
          return this;
        },
        then: function () {
          return this;
        }
      },
      cache_now = animate_mock.now,
      cache_then = animate_mock.then;

    beforeEach(function () {
      var cache_animate = animate;

      util_test.removeFromJSIOCache('src/views/grid.js');
      util_test.getFromJSIOCache('src/animate.js').exports = function () {
        util_test.getFromJSIOCache('src/animate.js').exports = cache_animate;
        return animate_mock;
      };

      jsio('import src.views.grid as Grid');
      grid = new Grid();
      grid.build(puzzle);
    });

    afterEach(function () {
      animate_mock.now = cache_now;
      animate_mock.then = cache_then;
    });

    it('shouldnt resize if pending_matches > 0', function (done) {
      var cache = grid.grid_view.updateOpts,
        cache_tilepos = grid.getTilePosition,
        puzzle = grid.model;

      puzzle.set('pending_matches', 1);
      grid.grid_view.updateOpts = function () {
        done('error');
      };

      grid.getTilePosition = function () {
        return {
          x: 0,
          y: 0
        };
      };

      grid.resize();
      puzzle.set('pending_matches', 0);
      grid.grid_view.updateOpts = cache;
      grid.getTilePosition = cache_tilepos;
      done();
    });

    it('should call the grid reflow function', function (done) {
      puzzle.getOccupiedColumnsRows = function () {
        return {
          x: {
            min: {
              getCoordinates: function () {}
            },
            max: {
              getCoordinates: function () {}
            },
            depth: 1
          },
          y: {
            min: {
              getCoordinates: function () {}
            },
            max: {
              getCoordinates: function () {}
            },
            depth: 2
          }
        };
      };

      grid.getTilePosition = function () {
        return {
          x: 1,
          y: 1
        };
      };
      grid.grid_view.reflow = function () {
        done();
      };
      animate_mock.now = function () {
        return animate_mock;
      };

      grid.resize();
    });
    it('should set AnchorX value to grid', function (done) {
      grid.getTilePosition = function () {
        return {
          x: 1,
          y: 1
        };
      };
      animate_mock.now = function (opts) {
        assert.deepEqual(opts.anchorX, 50);
        done();
        return animate_mock;
      };

      grid.resize();
    });

    it('should set AnchorY value to grid', function (done) {
      grid.getTilePosition = function () {
        return {
          x: 0,
          y: 0
        };
      };

      animate_mock.now = function (opts) {
        assert.deepEqual(opts.anchorY, 65);
        done();
        return animate_mock;
      };

      grid.resize();
    });

    it('should set X value to grid', function (done) {
      grid.getTilePosition = function () {
        return {
          x: 0,
          y: 0
        };
      };

      animate_mock.now = function (opts) {
        assert.strictEqual(opts.x, 261);
        done();
        return animate_mock;
      };

      grid.resize();
    });

    it('should set Y value to grid', function (done) {
      animate_mock.now = function (opts) {
        assert.strictEqual(opts.y, 307.5);
        done();
        return animate_mock;
      };

      grid.renderAllTiles();
      grid.resize();
    });

    it('should set scale to grid', function (done) {
      grid = new Grid();
      puzzle.loadPuzzle({layout: [[0, 0, 0], [0, 30, 0]],
                         id: 'test',
                         families: families,
                         init: []
                       });
      grid.build(puzzle);
      grid.renderAllTiles();

      grid.style.width = 500;
      grid.style.height = 490;

      grid.tiles[0][30][0].updateOpts({
        x: 500,
        y: 500
      });

      animate_mock.now = function (opts) {
        assert.strictEqual(opts.scale, 1);
        done();
        return animate_mock;
      };

      grid.resize();
    });

    it('should set scale of y if scale_y is smaller', function (done) {
      grid = new Grid();

      puzzle.loadPuzzle({layout: [[0, 0, 0], [0, 30, 0]],
                         id: 'test',
                         families: families,
                         init: []
                       });
      grid.build(puzzle);
      grid.renderAllTiles();

      grid.wrapper.style.width = 500;
      grid.wrapper.style.height = 490;

      grid.tiles[0][30][0].updateOpts({
        y: 1000
      });

      animate_mock.now = function (opts) {
        assert.strictEqual(opts.scale, 0.5);
        done();
        return animate_mock;
      };

      grid.resize();
    });

    it('should set scale of x if scale_x is smaller', function (done) {
      grid = new Grid();

      puzzle.loadPuzzle({layout: [[0, 0, 0], [30, 0, 0]],
                         id: 'test',
                         families: families,
                         init: []
                       });
      grid.build(puzzle);
      grid.renderAllTiles();

      grid.wrapper.style.width = 474;
      grid.wrapper.style.height = 500;

      grid.tiles[30][0][0].updateOpts({
        x: 1000
      });

      animate_mock.now = function (opts) {
        assert.strictEqual(opts.scale, 0.5);
        done();
        return animate_mock;
      };
      grid.resize();
    });

    it('should downscale grid for iPhoneX', function (done) {
      var cache = utils.isiPhoneX;

      utils.isiPhoneX = function () {
        return true;
      };

      grid = new Grid();

      puzzle.loadPuzzle({
        layout: [[0, 0, 0], [30, 0, 0]],
        id: 'test',
        families: families,
        init: []
      });
      grid.build(puzzle);
      grid.renderAllTiles();

      grid.wrapper.style.width = 474;
      grid.wrapper.style.height = 500;

      grid.tiles[30][0][0].updateOpts({
        x: 1000
      });

      animate_mock.now = function (opts) {
        assert.strictEqual(opts.scale, 0.45);
        done();
        return animate_mock;
      };
      grid.resize();
      utils.isiPhoneX = cache;
    });

    it('should set scale to 1 if calculated scale value greater than 1',
      function (done) {
      grid = new Grid();
      grid.getTilePosition = function () {
        return {
          x: 0,
          y: 0
        };
      };
      puzzle.loadPuzzle({layout: [[0, 0, 0], [4, 2, 0]],
                         id: 'test',
                         families: families,
                         init: []
                       });
      grid.build(puzzle);
      animate_mock.now = function (opts) {
        assert.strictEqual(opts.scale, 1);
        done();
        return animate_mock;
      };
      grid.resize();
    });

    it('should call then after animate', function (done) {
      grid.getTilePosition = function () {
        return {
          x: 0,
          y: 0
        };
      };
      animate_mock.then = function () {
        done();
      };

      grid.resize();
    });

    it('should emit resize-complete once resize is done', function (done) {
      grid.getTilePosition = function () {
        return {
          x: 0,
          y: 0
        };
      };
      puzzle.once('resize-complete', done);
      animate_mock.then = function (cb) {
        cb();
      };
      grid.resize();
    });
  });

  describe('getTilePosition', function () {
    it('should give position of the tile', function () {
      grid.tiles = [[[]]];
      grid.tiles[0][0][0] = {
        getPosition: function () {
          return {x: 10};
        }
      };

      assert.strictEqual(grid.getTilePosition([0, 0, 0]).x, 10);
    });
  });

  describe('clean', function () {
    it('should call clean of animate', function (done) {
      var cache = animate,
        anim_obj = {
          now: function () {
            return this;
          },
          hasFrames: function () {
            return false;
          },
          then: function () {},
          clear: function () {
            done();
          }
        };

      util_test.removeFromJSIOCache('src/views/grid.js');

      util_test.getFromJSIOCache('src/animate.js').exports = function () {
        animate = cache;
        return anim_obj;
      };

      jsio('import src.views.grid as Grid');
      grid = new Grid();
      grid.build(puzzle);
      grid.renderAllTiles();
      grid.resize();
      grid.clean();
    });
  });
});
