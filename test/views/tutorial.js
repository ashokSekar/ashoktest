/* global View, tutorial_view, ImageScaleView, ImageView, clearTimeout:true,
   util_test, timer, style, event_manager, image, device, TextView, dh_timer,
   ButtonView
*/

jsio('import util.underscore as _');
jsio('import DevkitHelper.timer as dh_timer');
jsio('import ui.View as View');
jsio('import ui.ImageView as ImageView');
jsio('import ui.ImageScaleView as ImageScaleView');
jsio('import ui.TextView as TextView');
jsio('import test.lib.util as util_test');
jsio('import timer');
jsio('import DevkitHelper.event_manager as event_manager');

describe('TutorialView', function () {
  'use strict';

  var bg,
    opts = {
      superview: new View(),
      view: new View(),
      height: 110,
      x: 0,
      y: 0,
      text: 'tutorial',
      action: function () {}
    },
    prepare = function () {
      jsio('import DevkitHelper.style as style');

      util_test.removeFromJSIOCache('src/animate.js');
      jsio('import animate');
      util_test.removeFromJSIOCache('src/animate.js');

      GC.app = {
        view: new View(),
        getCurrentView: function () {
          return new View();
        },
        tutorial: {
          removeListener: function () {},
          on: function () {}
        },
        user: {
          getMs: function () {},
          removeListener: function () {}
        }
      };
      device.isTablet = false;

      style.init({
        orientation: 'portrait',
        bound_width: 640,
        bound_height: 960,
        tablet: 0.8,
        phablet: 0.9
      });

      CACHE = {
        'resources/languages/en.json': JSON.stringify({
          settings: 'Settings',
          test_data: 'hello world'
        })
      };

      jsio('import quest.modules.popup_manager as popup');
      util_test.removeFromJSIOCache('quest/modules/popup_manager.js');

      event_manager.plugins = [];
      jsio('import quest.ui.ButtonView as ButtonView');
      jsio('import ui.ViewPool as ViewPool');
      jsio('import DevkitHelper.style as style');
      jsio('import quest.modules.image as image');
      util_test.removeFromJSIOCache('src/i18n');
      jsio('import DevkitHelper.i18n as i18n');
    },
    init = function () {
      style.scale_height = 1;
      style.base_height = 600;
      jsio('import src.views.tutorial as tutorial_view');
      jsio('import device');
      bg = image.get('puzzlescreen/celeb_ribbon_en');
    },
    clean = function () {
      tutorial_view.clean();

      util_test.removeFromJSIOCache('views/tutorial');
    };

  before(prepare);
  beforeEach(init);
  afterEach(clean);

  describe('prepare()', function () {
    it('should do initialization of tutorial_view view', function () {
      tutorial_view.prepare();
      tutorial_view.build({});
      assert.strictEqual(true, tutorial_view instanceof View);
      assert.strictEqual(true, tutorial_view.image_main instanceof ImageView);
      assert.strictEqual(true, tutorial_view.image_info instanceof ImageView);
      assert.strictEqual(true, tutorial_view.overlay instanceof ImageScaleView);
      assert.strictEqual(true,
        tutorial_view.simple_message instanceof TextView);
      assert.strictEqual(true, tutorial_view.clickHandler instanceof View);
      assert.strictEqual(true, tutorial_view.main_text instanceof TextView);
      assert.strictEqual(true,
        tutorial_view.main_text_extra instanceof TextView);
      assert.strictEqual(true, tutorial_view.info_text instanceof TextView);
      assert.strictEqual(true,
        tutorial_view.main_text_img instanceof ImageView);
    });
  });

  describe('setup()', function () {
    it('should reset views of tutorial_view', function () {
      tutorial_view.prepare();
      tutorial_view.setup();
      assert.strictEqual(true, tutorial_view.overlay instanceof ImageScaleView);
      assert.deepEqual(tutorial_view.overlay,
        tutorial_view.clickHandler._opts.superview);
      assert.strictEqual(true, tutorial_view.image_main._opts.centerX);
      assert.strictEqual(true, tutorial_view.image_main._opts.centerY);
      assert.strictEqual(true, tutorial_view.image_info._opts.centerX);
      assert.strictEqual(true, tutorial_view.image_info._opts.centerY);
      assert.strictEqual(true, tutorial_view.image_info._opts.centerX);
      assert.strictEqual(0, tutorial_view.main_text_extra._opts.offsetX);
      assert.strictEqual(0, tutorial_view.main_text_extra._opts.offsetY);
      assert.strictEqual(0, tutorial_view.info_text._opts.offsetX);
      assert.strictEqual(0, tutorial_view.info_text._opts.offsetY);
    });
  });

  describe('build()', function () {
    it('should call prepare for the first time', function (done) {
      var cache = tutorial_view.prepare;

      tutorial_view.prepare = function () {
        tutorial_view.prepare = cache;
        done();
        cache.call(tutorial_view);
      };
      tutorial_view.build({});
    });

    it('should call setup for the second time', function (done) {
      var cache = tutorial_view.setup;

      tutorial_view.prepare();
      tutorial_view.setup = function () {
        tutorial_view.setup = cache;
        done();
      };
      tutorial_view.build({});
    });
  });

  describe('show()', function () {
    it('should just call super show if no opts passed', function (done) {
      var cache = tutorial_view.setupOverlay;

      tutorial_view.setupOverlay = function () {
        done('error');
      };
      tutorial_view.show();
      tutorial_view.setupOverlay = cache;
      done();
    });

    it('should use timeout if passed', function (done) {
      var cache = dh_timer.timeout;

      tutorial_view.prepare();
      dh_timer.timeout = function () {
        done();
        dh_timer.timeout = cache;
      };
      tutorial_view.show({
        superview: new View()
      }, 10);
    });

    it('it should invoke setupView', function (done) {
      tutorial_view.setupView = function () {
        done();
      };
      tutorial_view.build({});
      tutorial_view.show(opts);
    });

    it('it should invoke setupOverlay', function (done) {
      tutorial_view.setupOverlay = function () {
        done();
      };
      tutorial_view.build({});
      tutorial_view.show(opts);
    });

    it('it should register onPause for tutorial pause', function (done) {
      var cache = GC.app.tutorial.on;

      GC.app.tutorial.on = function (evt, func) {
        GC.app.tutorial.on = cache;
        assert.strictEqual(evt, 'pause');
        func();
      };

      tutorial_view.onPause = done;
      tutorial_view.setupOverlay = function () {};
      tutorial_view.build({});
      tutorial_view.show(opts);
    });

    it('it should register onResume for tutorial resume', function (done) {
      var cache = GC.app.tutorial.on,
        count = 0;

      GC.app.tutorial.on = function (evt, func) {
        if (++count !== 2) {
          return;
        }

        GC.app.tutorial.on = cache;
        assert.strictEqual(evt, 'resume');
        func();
      };

      tutorial_view.onResume = done;
      tutorial_view.setupOverlay = function () {};
      tutorial_view.build({});
      tutorial_view.show(opts);
    });
  });

  describe('setupOverlay()', function () {
    it('should not setup overlay if disable_overlay is true', function () {
      tutorial_view.build({});
      tutorial_view.setupOverlay({disable_overlay: true});
      assert.strictEqual(null, tutorial_view.overlay.getSuperview());
    });

    it('should not setup clickHandler if no action is passed', function () {
      var flag = 0;

      tutorial_view.setupClickHandler = function () {
        flag = 1;
      };
      tutorial_view.build({});
      tutorial_view.setupOverlay({});
      assert.strictEqual(flag, 0);
    });

    it('should set proper image for overlay', function () {
      tutorial_view.build({});
      tutorial_view.setupOverlay(opts);
      assert.strictEqual(tutorial_view.overlay.getImage().getURL(),
        'resources/images/tutorial/overlay_default.png');
    });

    it('should set proper image for overlay if passed', function () {
      tutorial_view.build({overlay: 'match_pair'});
      tutorial_view.setupOverlay({});
      assert.strictEqual(tutorial_view.overlay.getImage().getURL(),
        'resources/images/tutorial/overlay_match_pair.png');
    });

    it('should set null image for overlay if passed', function () {
      tutorial_view.build({overlay: 'plain'});
      tutorial_view.setupOverlay({});
      assert.strictEqual(tutorial_view.overlay.getImage(), undefined);
    });

    it('should set views height and width if  context is a view', function () {
      var view = new View({});

      view.getHeight = function () {
        return 100;
      };
      view.getWidth = function () {
        return 50;
      };
      tutorial_view.build({overlay: 'plain'});
      tutorial_view.setupOverlay({action: {
        context: view
      }});
      assert.strictEqual(tutorial_view.clickHandler._opts.height, 105);
    });
  });

  describe('setupClickHandler()', function () {
    it('should register click handler for input events to call' +
    ' passed function', function (done) {
      var contxt = {
        testFunc: function (param) {
          done(param);
        }
      };

      tutorial_view.build();

      tutorial_view.setupClickHandler({
        action: {
          context: contxt,
          func: 'testFunc'
        }
      });
      tutorial_view.clickHandler.emit('InputSelect');
    });

    it('should register click handler for InputStart when event' +
    ' is InputDrag', function (done) {
      var evt = {
          id: -1,
          srcPt: {
            x: 1,
            y: 1
          },
          root: tutorial_view
        };

      tutorial_view.build();
      tutorial_view.clickHandler.on('InputStart', function () {
        done();
      });
      tutorial_view.setupClickHandler({
        action: {
          evt: 'InputDrag',
          func: 'testFunc'
        }
      });
      tutorial_view.clickHandler.emit('InputStart', evt);
    });

    it('should use onInputSelect if no action function ' +
    'is passed', function (done) {
      var contxt = {
        onInputSelect: done
      };

      tutorial_view.build();

      tutorial_view.setupClickHandler({
        action: {
          context: contxt
        }
      });
      tutorial_view.clickHandler.emit('InputSelect');
    });

    it('should emit next if opts.action is present', function (done) {
      tutorial_view.getPos = function () {};
      tutorial_view.on('next', function () {});
      tutorial_view.build({});
      tutorial_view.setupClickHandler({
        action: {
          func: 'getPos',
          context: tutorial_view
        }
      });
      tutorial_view.clickHandler.emit('InputSelect');
      util_test.assertSignal(done, tutorial_view, 'next', false);
    });

    it('should call clean on InputSelect', function (done) {
      var cache = tutorial_view.clean;

      tutorial_view.getPos = function () {};
      tutorial_view.clean = function () {
        tutorial_view.clean = cache;
        done();
      };
      tutorial_view.on('next', function () {});
      tutorial_view.build({});
      tutorial_view.setupClickHandler({
        action: {
          func: 'getPos',
          context: tutorial_view
        }
      });
      tutorial_view.clickHandler.emit('InputSelect');
    });

    it('should call clean on onDragStop', function (done) {
      var cache = tutorial_view.clean,
        evt = {
          id: -1,
          srcPt: {
            x: 1,
            y: 1
          },
          root: tutorial_view
        },
        ctx = {
          getPos: function () {
          }
        };

      tutorial_view.clean = function () {
        tutorial_view.clean = cache;
        done();
      };
      tutorial_view.on('next', function () {});
      tutorial_view.build({});
      tutorial_view.setupClickHandler({
        action: {
          evt: 'InputDrag',
          context: ctx,
          func: 'getPos'
        }
      });
      tutorial_view.clickHandler.emit('InputStart', evt);
      tutorial_view.clickHandler.onDragStop();
    });

    it('event_manager should emit tutorial-action' +
    ' on InputSelect', function (done) {
      var cache = event_manager.emit;

      event_manager.emit = function (signal) {
        event_manager.emit = cache;
        assert.strictEqual(signal, 'tutorial-action');
        done();
      };
      tutorial_view.getPos = function () {};
      tutorial_view.closeTutorial = function () {};
      tutorial_view.on('next', function () {});
      tutorial_view.build({});
      tutorial_view.setupClickHandler({
        action: {
          func: 'getPos',
          context: tutorial_view
        }
      });
      tutorial_view.clickHandler.emit('InputSelect');
    });

    it('should show clickHandler', function () {
      tutorial_view.build({});
      tutorial_view.setupClickHandler({
        action: {
          func: 'getPos',
          context: tutorial_view
        }
      });
      assert.strictEqual(tutorial_view.clickHandler.style.visible, true);
    });

    it('should disable events for  clickHandler', function () {
      tutorial_view.build({});
      tutorial_view.setupClickHandler({
        action: {
          func: 'getPos',
          context: tutorial_view
        }
      });
      assert.strictEqual(tutorial_view.clickHandler.isHandlingEvents(), false);
    });
  });

  describe('setupView()', function () {
    it('it should invoke startAnimation', function (done) {
      tutorial_view.startAnimation = function (curr_opts) {
        assert.deepEqual(opts, curr_opts);
        done();
      };

      tutorial_view.build({});
      tutorial_view.setupView(opts);
    });

    it('it should set height to 0 on no_image', function () {
      tutorial_view.build({});
      opts.no_image = true;
      tutorial_view.setupView(opts);
      assert.strictEqual(tutorial_view.style.height, 0);
    });

    it('should show info image also if provided' +
    'is not present', function (done) {
      opts.no_image = false;
      opts.msg_text = true;
      opts.info_image = true;
      opts.id = 'test';
      opts.extra = {
        scale: false
      };
      tutorial_view.startAnimation = function () {};
      tutorial_view.build({});
      tutorial_view.setupView(opts);
      assert.strictEqual('resources/images/tutorial/test.png',
        tutorial_view.image_info.getImage().getURL());
      opts.info_image = false;
      done(tutorial_view.image_info.style.visible ? undefined : 'error');
      delete opts.extra;
    });

    it('should call startAnimation after getting image', function (done) {
      var cache = image.getLocalizedImage;

      image.getLocalizedImage = function () {
        image.getLocalizedImage = cache;
        return cache.apply(image, arguments);
      };

      tutorial_view.startAnimation = function () {
        done();
      };
      tutorial_view.build({});
      opts.powerup_count = 5;
      tutorial_view.setupView(opts);
    });
    it('should call animateHand if drag_target is passed', function () {
      var cache,
        opts = {
          custom: {}
        },
        animated = false;

      opts.custom.drag_target = new View();
      tutorial_view.build({});
      cache = tutorial_view.animateHand;

      tutorial_view.animateHand = function () {
        animated = true;
      };
      tutorial_view.setupView(opts);
      assert.strictEqual(true, animated);
      tutorial_view.animateHand({
        style: {
          x: 0,
          y: 0
        },
        custom: {
          drag_target: {
            getPosition: function () {
              return {};
            }
          }
        }
      });
      tutorial_view.animateHand = cache;
    });
    it('should not call animateHand if drag_target is passed', function (done) {
      var cache;

      tutorial_view.build({});
      opts.custom = null;
      cache = tutorial_view.animateHand;

      tutorial_view.animateHand = function () {
        tutorial_view.animateHand = cache;
        done('err');
      };
      tutorial_view.setupView(opts);
      done();
    });

    it('should set proper data for msg text info', function (done) {
      var cache = tutorial_view.startAnimation;

      tutorial_view.startAnimation = function () {
        tutorial_view.startAnimation = cache;
        assert.strictEqual('resources/images/tutorial/text_bg_gummy.png',
          tutorial_view.image_main.getImage().getURL());
        done();
      };

      tutorial_view.build({});
      opts.no_image = false;
      opts.action = null;
      tutorial_view.on('next', function () {});
      opts.msg_text = 'gummy';
      tutorial_view.setupView(opts);
    });

    it('should set main text image if passed', function (done) {
      var cache = tutorial_view.startAnimation;

      tutorial_view.startAnimation = function () {
        tutorial_view.startAnimation = cache;
        assert.strictEqual('resources/images/tutorial/blue_to_red.png',
          tutorial_view.main_text_img.getImage().getURL());
        done();
      };

      tutorial_view.build({});
      opts.no_image = false;
      opts.main_text_img = 'blue_to_red';
      opts.action = null;
      tutorial_view.on('next', function () {});
      opts.msg_text = 'gummy';
      tutorial_view.setupView(opts);
    });
  });

  describe('startAnimation()', function () {
    it('should call animateMainImage', function (done) {
      var cache = tutorial_view.animateMainImage;

      tutorial_view.build({});
      tutorial_view.animateMainImage = function () {
        tutorial_view.animateMainImage = cache;
        done();
      };
      tutorial_view.startAnimation({});
    });

    it('should animate overlay if not disable_overlay', function () {
      var cache = tutorial_view.animateMainImage;

      tutorial_view.build({});
      tutorial_view.animateMainImage = function () {
        tutorial_view.animateMainImage = cache;
      };
      tutorial_view.startAnimation({});
      timer.tick(500);
      assert.strictEqual(tutorial_view.overlay.style.opacity, 1);
    });

    it('should shouldnt animate overlay if disable_overlay', function () {
      tutorial_view.build({});
      tutorial_view.animateMainImage = function () {};
      tutorial_view.startAnimation({
        disable_overlay: true
      });
      assert.strictEqual(tutorial_view.overlay.style.opacity, 0);
    });
  });

  describe('animateMainImage()', function () {
    it('should place view above clickHandler', function () {
      opts.y = 1000;
      opts.no_image = false;
      tutorial_view.build({});
      tutorial_view.animateMainImage(opts);
      timer.tick(1000);
      assert.strictEqual(tutorial_view.style.y, 150);
    });

    it('should make simple_message visible if present', function () {
      tutorial_view.build({});
      tutorial_view.animateMainImage(opts);
      opts.message = 'test_data';
      timer.tick(1000);
      assert.strictEqual(tutorial_view.simple_message.style.visible, false);
    });

    it('should reposition info_image to bottom of screen if its out of ' +
      'screen', function () {
        tutorial_view.build({});
        tutorial_view.image_info.updateOpts({
          visible: true,
          height: 200,
          offsetY: 400
        });
        tutorial_view.image_main.style.height = 200;
        tutorial_view.animateMainImage(_.extend({
            offsetY: 700,
            stick_to_lightbox: false,
            center: false
          }, opts));
        opts.message = 'test_data';
        timer.tick(1000);
        assert.strictEqual(tutorial_view.image_info.style.offsetY, 170);
      }
    );

    it('should set text to simple_message', function () {
      tutorial_view.build({info: 'test_data'});
      tutorial_view.animateMainImage(_.extend({message: 'test_data'}, opts));
      timer.tick(1000);
      assert.strictEqual(tutorial_view.simple_message.style.visible, true);
      assert.strictEqual(tutorial_view.simple_message.getText(), 'hello world');
    });

    it('should set y to simple_message', function () {
      var info_style;

      tutorial_view.build({info: 'test_data'});
      info_style = tutorial_view.simple_message.style;
      tutorial_view.animateMainImage(_.extend({info: 'test_data'}, opts));
      timer.tick(1000);
      assert.strictEqual(info_style.y,
       style.base_height - info_style.height - tutorial_view.style.y);
    });

    it('should add offsety to simple_message if present', function () {
      var info_style;

      tutorial_view.build({info: 'test_data'});
      info_style = tutorial_view.simple_message.style;
      tutorial_view.animateMainImage(_.extend({
        info: 'test_data',
        message_offset: -200}, opts));
      timer.tick(1000);
      assert.strictEqual(info_style.y,
       style.base_height - info_style.height - tutorial_view.style.y - 200);
    });

    it('should set y so that it appears on bottom of screen for tablet ' +
      'view', function (done) {
        var cache_tab = device.isTablet,
          parent_y;

        device.isTablet = true;
        tutorial_view.build({info: 'test_data'});
        tutorial_view.image_main.style.height = 200;
        tutorial_view.animateMainImage(_.extend({
          offsetY: 700,
          stick_to_lightbox: false,
          center: false
        }, opts));
        device.isTablet = cache_tab;
        parent_y = style.base_height -
          tutorial_view.image_main.style.height - 10;
        done(tutorial_view.style.y === parent_y ? undefined : 'error');
      }
    );

    it('should set y to simple_message to base_height - 250 if calculated' +
      ' value go beyond screen for non tablet', function () {
        var info_style,
          cache_tablet = device.isTablet;

        device.isTablet = false;
        tutorial_view.build({info: 'test_data'});
        info_style = tutorial_view.simple_message.style;
        info_style.height = 10;
        tutorial_view.animateMainImage(_.extend({info: 'test_data'}, opts));
        timer.tick(1000);
        assert.strictEqual(info_style.y,
          style.base_height - 250);
        device.isTablet = cache_tablet;
      }
    );

    it('should set y to simple_message to base_height - msg_height ' +
      'if calculated value go beyond screen for tablet', function () {
        var info_style,
          cache_tablet = device.isTablet;

        device.isTablet = true;
        opts.no_image = true;
        tutorial_view.build({info: 'test_data'});
        info_style = tutorial_view.simple_message.style;
        info_style.height = 10;
        tutorial_view.animateMainImage(_.extend({info: 'test_data'}, opts));
        timer.tick(1000);
        assert.strictEqual(info_style.y,
          style.base_height - info_style.height * style.scale_height);
        device.isTablet = cache_tablet;
      }
    );

    it('should place view below clickHandler', function () {
      var cache = image.getLocalizedImage;

      image.getLocalizedImage = function () {
        image.getLocalizedImage = cache;
        return {
          then: function (func) {
            func(bg);
          }
        };
      };
      opts.y = 0;
      opts.no_image = null;

      // image height
      tutorial_view.build({});
      tutorial_view.show(opts);
      tutorial_view.image_main.style.height = 101;
      tutorial_view.animateMainImage(opts);
      timer.tick(1000);
      assert.strictEqual(tutorial_view.style.y,
        style.base_height - 150 -
        tutorial_view.image_main.style.height);
    });

    it('should set setHandleEvents to true', function (done) {
      var cache;

      tutorial_view.build({});
      cache = tutorial_view.clickHandler.setHandleEvents;

      tutorial_view.clickHandler.setHandleEvents = function (param) {
        tutorial_view.clickHandler.setHandleEvents = cache;
        assert.strictEqual(true, param);
        done();
      };
      tutorial_view.animateMainImage(opts);
      timer.tick(1000);
    });

    it('should build action buttons', function () {
      var subviews;

      tutorial_view.prepare();
      tutorial_view.id = 'test';
      tutorial_view.animateMainImage({
        text: 6,
        actions: [{id: 'welcome'}]
      });
      subviews = tutorial_view.buttons.getSubviews();
      assert.strictEqual(subviews.length, 1);
      assert.strictEqual(subviews[0] instanceof ButtonView, true);
    });

    it('should not register input select when actions are there', function () {
      tutorial_view.prepare();
      tutorial_view.id = 'test';
      tutorial_view.animateMainImage({
        text: 6,
        actions: [{id: 'welcome'}]
      });
      assert.equal(tutorial_view.listeners('InputSelect').length, 0);
    });

    it('should set view y to 0 if no_image is true', function (done) {
      var cache;

      tutorial_view.prepare();
      tutorial_view.build({});
      tutorial_view.show(opts);
      cache = tutorial_view.updateOpts;

      tutorial_view.updateOpts = function (opts) {
        tutorial_view.updateOpts = cache;
        assert.strictEqual(0, opts.y);
        done();
      };

      opts.no_image = true;
      tutorial_view.animateMainImage(opts);
      timer.tick(300);
    });

    it('should set view wrt center if offsetY passed', function (done) {
      var cache;

      tutorial_view.prepare();
      tutorial_view.build({});
      tutorial_view.image_main.style.height = 300;

      cache = tutorial_view.updateOpts;

      tutorial_view.updateOpts = function (opts) {
        tutorial_view.updateOpts = cache;
        assert.strictEqual(200, opts.y);
        done();
      };

      opts.offsetY = 200;
      tutorial_view.animateMainImage(opts);
      timer.tick(300);
    });

    it('should set image to top if calculated y goes -ve', function (done) {
      var cache;

      tutorial_view.prepare();
      tutorial_view.build({});
      tutorial_view.image_main.style.height = 300;

      cache = tutorial_view.updateOpts;

      tutorial_view.updateOpts = function (opts) {
        tutorial_view.updateOpts = cache;
        assert.strictEqual(0, opts.y);
        done();
      };

      opts.offsetY = -500;
      tutorial_view.animateMainImage(opts);
      timer.tick(300);
    });

    it('should set image to bottom if calculated y overflows' +
    ' screen height', function (done) {
      var cache;

      tutorial_view.prepare();
      tutorial_view.build({});
      tutorial_view.image_main.style.height = 300;

      cache = tutorial_view.updateOpts;

      tutorial_view.updateOpts = function (opts) {
        tutorial_view.updateOpts = cache;
        assert.strictEqual(200, opts.y);
        done();
      };

      opts.offsetY = 500;
      tutorial_view.animateMainImage(opts);
      timer.tick(300);
    });

    it('should position view to center if prop is true', function (done) {
      var cache;

      opts.offsetY = 0;
      opts.center = true;
      tutorial_view.prepare();
      tutorial_view.build({});
      tutorial_view.image_main.style.height = 300;

      cache = tutorial_view.updateOpts;

      tutorial_view.updateOpts = function (opts) {
        tutorial_view.updateOpts = cache;
        assert.strictEqual(150, opts.y);
        done();
      };

      tutorial_view.animateMainImage(_.extend({extra: {
        hand: {
          x: 200,
          y: 200
        }
      }}, opts));
      timer.tick(300);
    });

    it('should position view right below lightbox', function (done) {
      var cache;

      opts.y = 20;
      opts.stick_to_lightbox = true;
      tutorial_view.prepare();
      tutorial_view.build({});
      tutorial_view.image_main.style.height = 300;

      cache = tutorial_view.updateOpts;

      tutorial_view.updateOpts = function (pos) {
        tutorial_view.updateOpts = cache;
        assert.strictEqual(opts.height + opts.y, pos.y);
        done();
      };

      tutorial_view.animateMainImage(_.extend({extra: {
        hand: {
          x: 200,
          y: 200
        }
      }}, opts));
      timer.tick(300);
    });
  });

  describe('animateHand()', function () {
    it('shouldnt animate hand', function (done) {
      var cache = tutorial_view.animate_hand;

      tutorial_view.clickHandler = {
        style: {}
      };

      tutorial_view.animate_hand = {
        now: function () {
          return this;
        },
        then: function (cb) {
          if (_.isFunction(cb)) {
            tutorial_view.animate_hand = cache;
            done();
          } else {
            return this;
          }
        }
      };
      tutorial_view.animateHand({
        custom: {
          drag_target: {}
        }
      });
    });
  });

  describe('repositionImageInfo()', function () {
    it('shouldnt reposition image_info if within screen', function (done) {
      var info_style;

      tutorial_view.build({});
      info_style = tutorial_view.image_info.style;
      info_style.offsetY = 0;
      tutorial_view.repositionImageInfo(300);
      done(info_style.offsetY === 0 ? undefined : 'error');
    });

    it('should reposition image_info to bottom of screen if its beyond ' +
      'it', function (done) {
        var info_style;

        tutorial_view.build({});
        info_style = tutorial_view.image_info.style;
        info_style.offsetY = 500;
        info_style.height = 100;
        tutorial_view.repositionImageInfo(300);
        done(info_style.offsetY !== 500 ? undefined : 'error');
      }
    );
  });

  describe('onAction', function () {
    it('should disable setHandleEvents', function () {
      tutorial_view.build();
      tutorial_view.onAction();
      assert.strictEqual(tutorial_view.isHandlingEvents(), false);
      assert.strictEqual(tutorial_view.clickHandler.isHandlingEvents(), false);
      assert.strictEqual(tutorial_view.overlay.isHandlingEvents(), false);
    });

    it('should call clear timer', function (done) {
      var cache = clearTimeout;

      tutorial_view.build();
      tutorial_view.animateMainImage({
        max_time: 10000
      });
      clearTimeout = function () {
        clearTimeout = cache;
        done();
      };
      tutorial_view.onAction();
    });

    it('event_manager should emit tutorial-action', function (done) {
      var cache = event_manager.emit;

      event_manager.emit = function (signal) {
        event_manager.emit = cache;
        assert.strictEqual(signal, 'tutorial-action');
        done();
      };
      tutorial_view.build();
      tutorial_view.onAction();
    });

    it('should emit next on view', function (done) {
      GC.app.tutorial.emit = function () {
        delete GC.app.tutorial.emit;
      };
      tutorial_view.build();
      tutorial_view.id = 'test_id';
      tutorial_view.on('next', done);
      tutorial_view.onAction({
        id: 'action',
        close: true
      });
    });

    it('should call clean if data.close is true', function (done) {
      var cache = tutorial_view.clean;

      GC.app.tutorial.emit = function () {
        delete GC.app.tutorial.emit;
      };
      tutorial_view.build();
      tutorial_view.id = 'test_id';
      tutorial_view.clean = function () {
        tutorial_view.clean = cache;
        done();
      };
      tutorial_view.onAction({
        id: 'action',
        close: true
      });
    });

    it('should emit next if false is passed', function (done) {
      tutorial_view.build();
      tutorial_view.once('next', done);
      tutorial_view.onAction(false);
    });

    it('should call clean if false is passed', function (done) {
      var cache = tutorial_view.clean;

      tutorial_view.build();
      tutorial_view.clean = function () {
        tutorial_view.clean = cache;
        done();
      };
      tutorial_view.onAction(false);
    });

    it('should emit proper signal', function (done) {
      tutorial_view.build();
      tutorial_view.id = 'test_id';
      GC.app.tutorial.emit = function (signal) {
        delete GC.app.tutorial.emit;
        assert.strictEqual(signal, 'test_id:action');
        done();
      };
      tutorial_view.onAction({id: 'action'});
    });
  });

  describe('finish()', function () {
    it('should call callback if present', function (done) {
      tutorial_view.build({});
      tutorial_view.finish(false, function () {
        done();
      });
      timer.tick(500);
    });

    it('should setHandleEvents for the view passed', function () {
      var screen = new View(),
      cache = screen.setHandleEvents;

      screen.setHandleEvents = function (handleEvents, ignoreSubviews) {
        screen.setHandleEvents = cache;
        assert.strictEqual(true, handleEvents);
        assert.strictEqual(false, ignoreSubviews);
      };

      tutorial_view.build({});
      tutorial_view.finish(screen);
    });
  });

  describe('onPause()', function () {
    it('should hide view and overlay', function (done) {
      var count = 0;

      tutorial_view.overlay = {
        hide: function () {
          done(++count === 2 ? undefined : 'error');
        }
      };

      tutorial_view.hide = function () {
        ++count;
      };
      tutorial_view.onPause();
    });
  });

  describe('onResume()', function () {
    it('should show view and overlay', function (done) {
      var count = 0;

      tutorial_view.overlay = {
        show: function () {
          done(++count === 2 ? undefined : 'error');
        }
      };

      tutorial_view.show = function () {
        ++count;
      };
      tutorial_view.onResume();
    });
  });

  describe('clean()', function () {
    it('should hide tutorial elements', function () {
      tutorial_view.build({});
      tutorial_view.clean();
      assert.strictEqual(false, tutorial_view.image_main.style.visible);
      assert.strictEqual(false, tutorial_view.image_info.style.visible);
      assert.strictEqual(false, tutorial_view.main_text.style.visible);
    });

    it('should not remove all subviews of tutorial view', function () {
      tutorial_view.build({});
      assert.equal(true, tutorial_view.getSubviews().length > 0);
      tutorial_view.clean();
      assert.equal(tutorial_view.getSubviews().length, 3);
    });

    it('remove all subviews of overlay', function () {
      tutorial_view.build({});
      assert.equal(true, tutorial_view.overlay.getSubviews().length > 0);
      tutorial_view.clean();
      assert.equal(tutorial_view.overlay.getSubviews().length, 0);
    });

    it('should remove overlay from parent', function () {
      tutorial_view.build({});
      tutorial_view.setupOverlay({superview: new View()});
      assert.equal(true, !!tutorial_view.overlay.getSuperview());
      tutorial_view.clean();
      assert.equal(false, !!tutorial_view.overlay.getSuperview());
    });

    it('should remove itself from superview', function () {
      tutorial_view.build({});
      tutorial_view.clean();
      assert.strictEqual(null, tutorial_view.getSuperview());
    });

    it('should remove all listeners for clickHandler InputSelect', function () {
      tutorial_view.build({});
      tutorial_view.clean();
      assert.strictEqual(0, tutorial_view.clickHandler
        .listeners('InputSelect').length);
    });

    it('should remove all listeners for overlay InputSelect', function () {
      tutorial_view.build({});
      tutorial_view.clean();
      assert.strictEqual(0, tutorial_view.overlay
        .listeners('InputSelect').length);
    });

    it('should remove all listeners tutorial_view InputSelect', function () {
      tutorial_view.build({});
      tutorial_view.clean();
      device.isTablet = true;
      assert.strictEqual(0, tutorial_view.listeners('InputSelect').length);
    });

    it('should remove all listeners for clickHandler InputMove', function () {
      tutorial_view.build({});
      tutorial_view.clean();
      assert.strictEqual(0,
        tutorial_view.clickHandler.listeners('InputMove').length);
    });

    it('should call clear timer', function (done) {
      var cache = clearTimeout;

      tutorial_view.build({});
      tutorial_view.animateMainImage({
        max_time: 10000
      });
      clearTimeout = function () {
        clearTimeout = cache;
        done();
      };
      tutorial_view.clean();
    });

    it('should remove pause and resume listeners of tutorial', function () {
      var cache = GC.app.tutorial.removeListener,
        arr = [];

      GC.app.tutorial.removeListener = function (evt) {
        arr.push(evt);
        if (arr.length === 2) {
          GC.app.tutorial.removeListener = cache;
        }
      };

      tutorial_view.build({});
      tutorial_view.clean();
      assert.deepEqual(['resume', 'pause'], arr);
    });

    it('shouldnt clean if already cleaned or if not builded', function (done) {
      var cache = GC.app.tutorial.removeListener;

      GC.app.tutorial.removeListener = function () {
        done('error');
      };
      tutorial_view.clean();
      GC.app.tutorial.removeListener = cache;
      done();
    });
  });
});
