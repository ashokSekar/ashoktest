/* global User, TileTab, ViewPool, ButtonView,
  style, util_test
*/

jsio('import ui.ViewPool as ViewPool');
jsio('import test.lib.util as util_test');
jsio('import ui.resource.Image as ImageResource');

describe('TileTab', function () {
  'use strict';

  var tile_tab,
    product_data = {
      tile0: {
        name: 'classic',
        cost: 0
      },
      tile1: {
        name: 'winter',
        cost: 9999
      },
      tile2: {
        name: 'summer',
        cost: 9999
      },
      tile3: {
        name: 'halloween',
        cost: 9999
      }
    },
    prepare = function () {
      util_test.removeFromJSIOCache('src/model/user.js');
      util_test.removeFromJSIOCache('src/views/tile_tab.js');
      jsio('import src.models.user as User');

      GC.app = {
        user: new User()
      };

      jsio('import src.views.tile_tab as TileTab');
      jsio('import quest.ui.ButtonView as ButtonView');
      jsio('import DevkitHelper.style as style');
      jsio('import resources.data.products');
      util_test.getFromJSIOCache('resources/data/products')
        .exports = product_data;

      style.get = function () {
        return {};
      };
    };

  before(prepare);
  beforeEach(function () {
    tile_tab = new TileTab();
  });

  describe('init()', function () {
    it('should add subviews', function () {
      assert.strictEqual(true, tile_tab.tile_sets instanceof ViewPool);
    });
  });

  describe('build()', function () {
    it('should set bg', function (done) {
      var cache = tile_tab.renderTileSets;

      GC.app.user.get = function () {};

      tile_tab.renderTileSets = function () {
        tile_tab.renderTileSets = cache;
      };
      tile_tab.build();
      assert.strictEqual(tile_tab.getImage().getURL(),
        'resources/images/popup/tile_store/tile_tab_bg.png');
      done();
    });

    it('should call renderTileSets', function (done) {
      var cache = tile_tab.renderTileSets;

      tile_tab.renderTileSets = function () {
        tile_tab.renderTileSets = cache;
        done();
      };
      tile_tab.build();
    });
  });

  describe('renderTileSets()', function () {
    it('should render all the tile sets', function (done) {
      var i = 0,
        cache = tile_tab.createButton,
        cache_tile_sel = tile_tab.tileSetSelected;

      GC.app.user.get = function () {};
      tile_tab.createButton = function () {
        i++;
      };
      tile_tab.tileSetSelected = function () {
        tile_tab.tileSetSelected = cache_tile_sel;
      };

      tile_tab.renderTileSets();
      done(i === 4 ? undefined : 'error');
      tile_tab.createButton = cache;
    });
    it('should select the unbought tile set', function (done) {
      var cache = tile_tab.tileSetSelected,
          cache_get = GC.app.user.get;

      GC.app.user.get = function (param) {
        GC.app.user.get = cache_get;
        if (param === 'tile_sets') {
          return ['classic', 'summer', 'halloween'];
        }
      };
      tile_tab.tileSetSelected = function (param) {
        tile_tab.tileSetSelected = cache;
        assert.strictEqual(param, 'tile1');
        done();
      };
      tile_tab.renderTileSets();
    });
    it('should select the classic tile set if all bought', function () {
      var cache = tile_tab.tileSetSelected,
          cache_get = GC.app.user.get;

      GC.app.user.get = function (param) {
        if (param === 'tile_sets') {
          return ['classic', 'winter', 'summer', 'halloween'];
        }
      };
      tile_tab.tileSetSelected = function (param) {
        tile_tab.tileSetSelected = cache;
        assert.strictEqual(param, 'tile0');
      };
      tile_tab.renderTileSets();
      GC.app.user.get = cache_get;
    });
  });

  describe('createButton()', function () {
    it('should create button', function (done) {
      GC.app.user.get = function () {};
      tile_tab.createButton('tile0');
      done(tile_tab.btn_classic instanceof ButtonView ? undefined : 'error');
    });
  });

  describe('tileSetSelected()', function () {
    it('should emit tile-set-select', function (done) {
      var cache = tile_tab.emit;

      tile_tab.emit = function (event) {
        tile_tab.emit = cache;
        assert.strictEqual(event, 'tile-set-select');
        done();
      };
      tile_tab.build();
      tile_tab.tileSetSelected('tile0');
    });
  });

  describe('fetchTileProducts()', function () {
    it('should set value for tile_sets', function (done) {
      var products = {
        tile0: {
          name: 'classic',
          cost: 0,
          quantity: 1
        },
        tile1: {
          name: 'winter',
          cost: 9999,
          quantity: 1
        },
        tile2: {
          name: 'summer',
          cost: 9999,
          quantity: 1
        },
        tile3: {
          name: 'halloween',
          cost: 9999,
          quantity: 1
        }
      };

      tile_tab._fetchTileProducts();

      assert.deepEqual(tile_tab._tile_sets, products);
      done();
    });
  });

  describe('clean()', function () {
    it('should render all the tile sets', function (done) {
      var cache = tile_tab.tile_sets.releaseAllViews;

      tile_tab.tile_sets.releaseAllViews = function () {
        tile_tab.tile_sets.releaseAllViews = cache;
        done();
      };

      tile_tab.clean();
    });
  });
});
