/* global style, util_test, View, SubstituteToast, TextView, Tile, ImageView,
  User */

jsio('import ui.TextView as TextView');
jsio('import ui.ImageView as ImageView');
jsio('import test.lib.util as util_test');
jsio('import ui.View as View');

describe('SubstituteToast View', function () {
  'use strict';

  var toast_view;

  before(function () {
    util_test.removeFromJSIOCache('src/style.js');
    util_test.removeFromJSIOCache('src/modules/image.js');

    jsio('import effects');
    jsio('import src.views.tile as Tile');
    jsio('import DevkitHelper.style as style');
    jsio('import quest.modules.image as image');
    jsio('import src.views.substitute_toast as SubstituteToast');

    util_test.removeFromJSIOCache('src/model/user.js');
    jsio('import src.models.user as User');

    toast_view = new SubstituteToast();

    style.init = function () {
      return;
    };

    GC.app = {
      user: new User(),
      view: {
        updateOpts: function () {}
      },
      getCurrentView: function () {
        return new View();
      },
      addSubview: function () {
        return;
      },
      engine: {
        subscribe: function () {}
      }
    };
  });

  after(function () {
    util_test.removeFromJSIOCache('src/style.js');
    util_test.removeFromJSIOCache('src/modules/image.js');
  });

  describe('prepare()', function () {
    it('should init view and subviews', function () {
      toast_view.prepare();
      assert.strictEqual(true, toast_view.toast_title instanceof TextView);
      assert.strictEqual(true, toast_view.arrow instanceof ImageView);
      assert.strictEqual(true, toast_view.tile_one instanceof Tile);
      assert.strictEqual(true, toast_view.tile_two instanceof Tile);
    });
  });

  describe('build()', function () {
    it('should have 4 subviews', function () {
      toast_view.removeAllSubviews();
      toast_view.prepare();
      toast_view.tile_one.changeFace = function () {
      };
      toast_view.tile_two.changeFace = function () {};
      toast_view.build(GC.app, 'face_1', 'face_2');

      assert.strictEqual(toast_view.getSubviews().length, 4);
    });

    it('should invoke prepare if subviews not present', function (done) {
      var cache = toast_view.prepare;

      toast_view.removeAllSubviews();
      toast_view.prepare = function () {
        toast_view.prepare = cache;
        done();
      };
      toast_view.build(GC.app, 'face_1', 'face_2');
    });

    it('should not invoke prepare if subviews present', function (done) {
      var cache = toast_view.prepare;

      toast_view.prepare = function () {
        toast_view.prepare = cache;
        done('error');
      };
      toast_view.build(GC.app, 'face_1', 'face_2');
      done();
    });
  });
});
