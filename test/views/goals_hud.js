/* global PuzzleModel, util_test,
  GoalsHud, style
*/

jsio('import test.lib.util as util_test');

describe('GoalsHUD', function () {
  'use strict';

  var goals_hud, puzzle,
    config_data = {
      STYLE: {
        orientation: 'portrait'
      },
      goals_prop: {
        clear: 'tile_count',
        golden: 'golden_collected',
        time: 'time',
        tap: 'tap',
        jewels: 'jewels',
        lock: 'lock_keys'
      },
      variations: {
        classic: {
          id: 'classic',
          powerups_exclude: ['flip'],
          goals: ['clear', 'time']
        },
        memory: {
          id: 'memory',
          powerups_exclude: ['shuffle', 'half_hint',
            'swap', 'open_slot', 'auto_hint'],
          goals: ['clear', 'tap']
        },
        untimed: {
          id: 'untimed',
          powerups_exclude: ['flip'],
          goals: ['clear', 'time']
        },
        golden: {
          id: 'golden',
          powerups_exclude: ['flip'],
          goals: ['clear', 'time', 'golden']
        },
        jewels: {
          id: 'jewels',
          powerups_exclude: ['flip'],
          goals: ['clear', 'time', 'jewels']
        },
        three_match: {
          id: 'three_match',
          powerups_exclude: ['flip'],
          goals: ['clear', 'time']
        },
        lock_key: {
          id: 'lock_key',
          powerups_exclude: ['flip'],
          goals: ['clear', 'time', 'lock']
        },
        partial_memory: {
          id: 'partial_memory',
          powerups_exclude: ['flip'],
          goals: ['clear', 'time']
        }
      },
      powerups: [],
      bonus_level_def: [[20, 40], [13, 15], [7, 30]],
      ms_per_chapter: 20,
      lives: {
        max: 5,
        cost_for_max: 10,
        gen: 1800000,
        ad: 1,
        fb: 1
      }
    },
    prepare = function () {
      GC.app = {
        view: {
          updateOpts: function () {}
        },
        engine: {
          subscribe: function () {}
        }
      };

      jsio('import resources.data.config as config');
      util_test.getFromJSIOCache('resources/data/config').exports = config_data;

      util_test.removeFromJSIOCache('devkithelper/src/style');

      jsio('import resources.styles.portrait as portrait');
      util_test.getFromJSIOCache('resources/styles/portrait').exports =
        {};

      jsio('import DevkitHelper.style as style');
      style.init(config_data.STYLE);

      jsio('import src.models.puzzle as PuzzleModel');
      jsio('import ui.ScoreView as ScoreView');
      jsio('import src.views.goals_hud as GoalsHud');
      util_test.removeFromJSIOCache('src/i18n');
      CACHE = {
        'resources/languages/en.json': JSON.stringify({
          times_number_hud: 'X $1'
        })
      };
      jsio('import DevkitHelper.i18n as i18n');
    },
    init = function () {
      puzzle = new PuzzleModel();
      goals_hud = new GoalsHud();

      goals_hud.getSuperview = function () {
        return {
          style: {
            width: 377
          }
        };
      };
      goals_hud.model = puzzle;
    };

  before(prepare);
  beforeEach(init);

  describe('registerLimits()', function () {
    it('should listen to tap if limit_type is tap', function (done) {
      var cache = goals_hud.updateLimit,
        cache_update = goals_hud.updateGoal;

      goals_hud.model.set('limit_type', 'tap');
      goals_hud.updateLimit = function () {
        goals_hud.updateLimit = cache;
        done();
      };
      goals_hud.updateGoal = function () {
        goals_hud.updateGoal = cache_update;
      };
      goals_hud.registerLimits('classic');
      goals_hud.emit('change:tap');
    });

    it('should call updateGoal if goal tile_count is one ' +
      'of them', function (done) {
      var cache_update = goals_hud.updateGoal;

      goals_hud.model.set('type', 'classic');
      goals_hud.updateGoal = function () {
        goals_hud.updateGoal = cache_update;
        done();
      };
      goals_hud.registerLimits('classic');
      goals_hud.emit('change:tile_count');
    });
    it('should listen to time if normal game', function (done) {
      var cache = goals_hud.updateLimit,
          cache_update = goals_hud.updateGoal,
        i = 0;

      goals_hud.model.set('limit_type', 'time');
      goals_hud.updateLimit = function () {
        i++;
        if (i === 2) {
          goals_hud.updateLimit = cache;
          done();
        }
      };
      goals_hud.updateGoal = function () {
        goals_hud.updateGoal = cache_update;
      };
      goals_hud.registerLimits('classic');

      goals_hud.model.emit('change:time');
    });
  });

  describe('updateLimit()', function () {
    it('should hide the limit view for untimed', function () {
      puzzle.set('limit_type', 'untimed');
      puzzle.set('limit', 1200);
      puzzle.set('time', 600);
      goals_hud.build(puzzle);
      goals_hud.updateLimit();
      assert.strictEqual(false, goals_hud.left_view.style.visible);
    });

    it('should take time from model and update time', function () {
      puzzle.set('limit_type', 'time');
      puzzle.set('limit', 1200);
      puzzle.set('time', 600);
      goals_hud.build(puzzle);
      goals_hud.updateLimit();
      assert.strictEqual('10:00', goals_hud.left_view.limit_view._text);
    });

    it('should take time from model and update time', function () {
      puzzle.set('limit_type', 'tap');
      puzzle.set('limit', 100);
      puzzle.set('tap', 60);
      goals_hud.build(puzzle);
      goals_hud.updateLimit();
      assert.strictEqual('40', goals_hud.left_view.limit_view._text);
    });
  });

  describe('onLoad', function () {
    it('should hide the face for jewels variation', function () {
      var cache_register = goals_hud.registerLimits;

      goals_hud.registerLimits = function () {
        goals_hud.registerLimits = cache_register;
      };
      goals_hud.model.set('type', 'jewels');
      goals_hud.build(puzzle);
      goals_hud.onLoad();
      assert.strictEqual(goals_hud.face.style.opacity, 0);
    });

    it('should hide the face for three match', function () {
      goals_hud.model.set('type', 'three_match');
      goals_hud.build(puzzle);
      goals_hud.onLoad();
      assert.strictEqual(goals_hud.face.style.opacity, 0);
    });

    it('should not hide the face for normal game', function (done) {
      var cache = goals_hud.face.updateOpts;

      goals_hud.face.updateOpts = function (param) {
        goals_hud.face.updateOpts = cache;
        if (param.opacity === 0) {
          done('err');
        }
      };
      goals_hud.build(puzzle);
      goals_hud.model.set('type', 'classic');
      goals_hud.onLoad();
      done();
    });

    it('should update the tile bg for golden tile', function () {
      var img_path = 'resources/images/puzzlescreen/golden_hud.png';

      goals_hud.model.set('goal', {
        golden: 2
      });
      goals_hud.build(puzzle);
      goals_hud.model.set('type', 'golden');
      goals_hud.onLoad();
      assert.strictEqual(img_path, goals_hud.right_view.tile.style
        ._view._img._originalURL);
    });
  });

  describe('updateGoal', function () {
    it('should update the goal for golden variation', function () {
      var cache = goals_hud.model.get,
        cache_set;

      goals_hud.build(puzzle);
      cache_set = goals_hud.right_view.setText;
      goals_hud.right_view.setText = function (param) {
        goals_hud.limit_views[1].setText = cache_set;
        assert.strictEqual('X 0', param);
      };
      goals_hud.model.get = function (param) {
        if (param === 'goal') {
          return {
            golden: 1
          };
        } else if (param === 'collected_golden') {
          return 1;
        } else if (param === 'type') {
          return 'golden';
        } else if (param === 'tile_count') {
          return 40;
        }
      };
      goals_hud.updateGoal();
      goals_hud.model.get = cache;
    });

    it('should update the goal for golden variation', function () {
      var cache_set, cache = goals_hud.model.get,
        cache_get_locks = goals_hud.model.getNoOfLocks;

      goals_hud.build(puzzle);
      cache_set = goals_hud.right_view.setText;
      goals_hud.right_view.setText = function (param) {
        goals_hud.limit_views[1].setText = cache_set;
        assert.strictEqual('X 3', param);
      };
      goals_hud.model.getNoOfLocks = function () {
        goals_hud.model.getNoOfLocks = cache_get_locks;
        return 3;
      };
      goals_hud.model.get = function (param) {
        if (param === 'type') {
          return 'lock_key';
        } else if (param === 'tile_count') {
          return 40;
        }
      };
      goals_hud.updateGoal();
      goals_hud.model.get = cache;
    });

    it('should update the goal for other variations', function () {
      var cache = goals_hud.model.get;

      goals_hud.build(puzzle);
      goals_hud.right_view.setText = function (param) {
        assert.strictEqual('X 40', param);
      };
      goals_hud.model.get = function (param) {
        if (param === 'goal') {
          return {
            golden: 1
          };
        } else if (param === 'type') {
          return 'classic';
        } else if (param === 'tile_count') {
          return 40;
        }
      };
      goals_hud.updateGoal();
      goals_hud.model.get = cache;
    });
  });
});
