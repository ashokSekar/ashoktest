/* global Puzzle_bg, View, ImageScaleView, ImageView,
  style, util_test, image, event_manager, utils, User
*/

jsio('import test.lib.util as util_test');
jsio('import util.underscore as _');
jsio('import ui.View as View');
jsio('import ui.ImageScaleView as ImageScaleView');
jsio('import ui.ImageView as ImageView');
jsio('import quest.models.user as User');

describe('puzzle_bg', function () {
  'use strict';

  var puzzle_bg,
    portrait_data = {
      border_left: {
        offsetY: -50
      },
      border_right: {
        offsetY: -50
      }
    },
    prepare = function () {
      GC.app = {
        view: {
          updateOpts: function () {}
        },
        user: new User()
      };
      jsio('import DevkitHelper.event_manager as event_manager');
      event_manager.plugins = [];
      jsio('import resources.styles.portrait as portrait');
      util_test.removeFromJSIOCache('devkithelper/src/style');
      util_test.getFromJSIOCache('resources/styles/portrait').exports =
        portrait_data;
      jsio('import DevkitHelper.style as style');
      style.init({
        orientation: 'portrait',
        bound_width: 640,
        bound_height: 960,
        tablet: 0.8,
        phablet: 0.9
      });
      jsio('import quest.modules.image as image');

      jsio('import src.views.puzzle_bg as Puzzle_bg');
      jsio('import quest.modules.util as utils');
      utils.getSafeArea = function () {
        return {
          top: 44,
          bottom: 34
        };
      };
    },
    init = function () {
      puzzle_bg = new Puzzle_bg();
    };

  before(prepare);
  beforeEach(init);

  describe('init()', function () {
    it('should create proper child views', function () {
      assert.strictEqual(true, puzzle_bg instanceof View);
      assert.strictEqual(true, puzzle_bg.hud instanceof ImageScaleView);
      assert.strictEqual(true, puzzle_bg.puzzle instanceof ImageScaleView);
      assert.strictEqual(true,
        puzzle_bg.border_left instanceof View);
      assert.strictEqual(true,
        puzzle_bg.border_left_fixed instanceof ImageView);
      assert.strictEqual(true,
        puzzle_bg.border_right_fixed instanceof ImageView);
      assert.strictEqual(true,
        puzzle_bg.border_left_tile instanceof ImageScaleView);
      assert.strictEqual(true,
          puzzle_bg.border_right_tile instanceof ImageScaleView);
    });
  });

  describe('build()', function () {
    it('should set background for hud, puzzle', function () {
      var cache = utils.isiPhoneX;

      utils.isiPhoneX = function () {
        utils.isiPhoneX = cache;
        return true;
      };
      puzzle_bg.build();
      assert.equal('resources/images/puzzlescreen/hud_bg.png',
       puzzle_bg.hud.getImage().getURL());
      assert.equal('resources/images/puzzle/faces/' +
        GC.app.user.get('curr_tile_set') + '/bg.png',
       puzzle_bg.puzzle.getImage().getURL());
    });
    it('should set background for left fixed, right fixed', function () {
      puzzle_bg.build();
      assert.equal('resources/images/puzzlescreen/left_fixed.png',
       puzzle_bg.border_left_fixed.getImage().getURL());
      assert.equal('resources/images/puzzlescreen/right_fixed.png',
       puzzle_bg.border_right_fixed.getImage().getURL());
    });
    it('should set background for left tile, right tile', function () {
      puzzle_bg.build();
      assert.equal('resources/images/puzzlescreen/left_tile.png',
       puzzle_bg.border_left_tile.getImage().getURL());
      assert.equal('resources/images/puzzlescreen/right_tile.png',
       puzzle_bg.border_right_tile.getImage().getURL());
    });

    it('should set proper puzzle height', function () {
      image.get('puzzlescreen/hud')._map.height = 200;
      style.base_height = 600;
      puzzle_bg.build();
      assert.equal(puzzle_bg.puzzle._opts.height, 400);
    });

    it('should set proper left tile rows', function () {
      image.get('puzzlescreen/hud_bg')._map.height = 200;
      image.get('puzzlescreen/left_fixed')._map.height = 200;
      image.get('puzzlescreen/left_tile')._map.height = 200;
      style.base_height = 600;

      puzzle_bg.build();
      assert.equal(puzzle_bg.border_left_tile._opts.rows, 2);
    });

    it('should set proper left tile height', function () {
      image.get('puzzlescreen/hud_bg')._map.height = 200;
      image.get('puzzlescreen/left_fixed')._map.height = 200;
      image.get('puzzlescreen/left_tile')._map.height = 200;
      style.base_height = 600;

      puzzle_bg.build();

      assert.equal(puzzle_bg.border_left_tile._opts.height, 400);
    });

    it('should set proper right tile rows', function () {
      image.get('puzzlescreen/hud_bg')._map.height = 200;
      image.get('puzzlescreen/right_fixed')._map.height = 200;
      image.get('puzzlescreen/right_tile')._map.height = 200;
      style.base_height = 600;

      puzzle_bg.build();
      assert.equal(puzzle_bg.border_right_tile._opts.rows, 2);
    });

    it('should set proper right tile height', function () {
      image.get('puzzlescreen/hud_bg')._map.height = 200;
      image.get('puzzlescreen/right_fixed')._map.height = 200;
      image.get('puzzlescreen/right_tile')._map.height = 200;
      style.base_height = 600;

      puzzle_bg.build();
      assert.equal(puzzle_bg.border_right_tile._opts.height, 400);
    });
  });
});
