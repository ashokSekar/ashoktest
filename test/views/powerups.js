/* global View, Powerups, Quest_powerups, PuzzleModel, util_test,
  inventory_manager, User*/

jsio('import test.lib.util as util_test');
jsio('import ui.View as View');

describe('Powerups', function () {
  'use strict';

  var powerups, puzzle, user, cache_unlocked,
    prepare = function () {
      GC.app = {
        user: {
          getMs: function () {}
        },
        engine: {
          subscribe: function () {}
        },
        getCurrentView: function () {
          return new View();
        }
      };
      util_test.removeFromJSIOCache('devkithelper/src/style');
      jsio('import DevkitHelper.style as style');
      jsio('import src.models.puzzle as PuzzleModel');
      jsio('import src.views.powerups as Powerups');
      jsio('import quest.ui.ButtonView as ButtonView');
      jsio('import quest.modules.inventory_manager as inventory_manager');
      jsio('import quest.views.powerups as Quest_powerups');
      util_test.removeFromJSIOCache('quest/models/user');
      jsio('import quest.models.user as User');
      user = GC.app.user = new User();
      cache_unlocked = user.isUnlocked;

      user.isUnlocked = function () {
        return true;
      };
    },
    init = function () {
      puzzle = new PuzzleModel();
      puzzle.set('powerups', ['shuffle']);

      powerups = new Powerups();
      powerups.model = puzzle;
    },
    clean = function () {
      util_test.removeFromJSIOCache('src/views/powerups.js');
    };

  before(prepare);
  beforeEach(init);
  afterEach(clean);
  after(function () {
    user.isUnlocked = cache_unlocked;
  });

  describe('powerupSelected()', function () {
    it('should check free_shuffle', function () {
      var cache = powerups.applyPowerup;

      powerups.applyPowerup = function (powerup, inventory) {
        assert.strictEqual(false, inventory);
        assert.strictEqual('shuffle', powerup);
        powerups.applyPowerup = cache;
      };
      powerups.model.set('free_shuffle', 0);
      powerups.powerupSelected('shuffle');
      powerups.model.removeListener('powerup-applied');
    });
    it('should call super if free_shuffle is not zero', function (done) {
      var cache_inventory = inventory_manager.buy,
        cache_quest_powerup = Quest_powerups.prototype.powerupSelected;

      inventory_manager.buy = function () {
        inventory_manager.buy = cache_inventory;
      };
      Quest_powerups.prototype.powerupSelected = function () {
        Quest_powerups.prototype.powerupSelected = cache_quest_powerup;
        done();
      };
      powerups.model.set('free_shuffle', 10);
      powerups.powerupSelected('shuffle');
    });
  });
});
