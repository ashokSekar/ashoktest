/* global _, Toast, TextView, image, style, util_test, ui */

jsio('import ui.TextView as TextView');
jsio('import test.lib.util as util_test');
jsio('import util.underscore as _');

describe('Toast View', function () {
  'use strict';

  var toast_view;

  before(function () {
    var anim_obj;

    util_test.removeFromJSIOCache('src/style.js');
    util_test.removeFromJSIOCache('src/modules/image.js');
    util_test.removeFromJSIOCache('src/views/toast.js');

    anim_obj = {
      now: function () {
        return this;
      },
      wait: function () {
        return this;
      },
      then: function (cb) {
        if (_.isFunction(cb)) {
          cb();
        }
        return this;
      },
      pause: function () {
        return this;
      },
      clear: function () {
        return this;
      }
    };

    jsio('import animate');
    util_test.getFromJSIOCache('src/animate.js').exports = function () {
      return anim_obj;
    };

    jsio('import DevkitHelper.style as style');
    jsio('import quest.modules.image as image');
    jsio('import DevkitHelper.style as ui');
    jsio('import src.views.toast as Toast');

    toast_view = new Toast();

    style.init = function () {
      return;
    };
  });

  after(function () {
    util_test.removeFromJSIOCache('src/style.js');
    util_test.removeFromJSIOCache('src/modules/image.js');
    util_test.removeFromJSIOCache('src/views/toast.js');
  });

  describe('init()', function () {
    it('should set width of bg', function () {
      ui.base_width = 100;

      toast_view.init();
      assert.strictEqual(100, ui.base_width = 100);
    });
  });

  describe('clean()', function () {
    it('should call removeFromSuperview', function (done) {
      var cache = toast_view.removeFromSuperview;

      toast_view.removeFromSuperview = function () {
        toast_view.removeFromSuperview = cache;
        done();
      };
      toast_view.clean();
    });
    it('should call onRelease', function (done) {
      var cache = toast_view.removeFromSuperview,
        cache_onrelease = toast_view.onRelease;

      toast_view.removeFromSuperview = function () {
        toast_view.removeFromSuperview = cache;
      };

      toast_view.onRelease = function () {
        done();
      };
      toast_view.clean();
      toast_view.onRelease = cache_onrelease;
    });
  });

  describe('onRelease()', function () {
    it('should call setImage with null', function (done) {
      var cache = toast_view.setImage;

      toast_view.setImage = function (val) {
        toast_view.setImage = cache;
        assert.strictEqual(val, null);
        done();
      };

      toast_view.onRelease();
    });
  });

  describe('prepare()', function () {
    it('should init view and subviews', function () {
      toast_view.removeAllSubviews();
      delete toast_view.toast_text;

      toast_view.prepare();
      assert.strictEqual(true, toast_view.toast_title instanceof TextView);
      assert.strictEqual(true, toast_view.toast_content instanceof TextView);
    });
  });

  describe('build()', function () {
    it('should invoke prepare if subviews not build', function (done) {
      var cache = toast_view.prepare;

      toast_view.prepare = function () {
        toast_view.prepare = cache;
        done();
      };
      toast_view.removeAllSubviews();
      toast_view.build();
    });

    it('should update text with passed opts', function (done) {
      var cache = style.get;

      style.get = function (prop, opts) {
        if (prop === 'toast_puzzle') {
          style.get = cache;
          done();
          return {};
        }
        return cache(prop, opts);
      };
      toast_view.build(null, 'text', {
        text_type: 'puzzle'
      });
    });

    it('should call toast_content.updateOpts', function (done) {
      toast_view.toast_content.updateOpts = function () {
        done();
      };
      toast_view.build(null, 'text', {
        text_type: 'puzzle',
        content: 'tool_tip_1',
        content_type: 'tool_tip_content',
        offsetY: 450,
        offsetX: 20
      });
    });

    it('should set height of bg', function () {
      var cache = image.get,
        cache_update_view = toast_view.updateOpts;

      image.get = function (img) {
        if (img === 'puzzle') {
          image.get = cache;
          return {
            getHeight: function () {
              return 100;
            },
            getWidth: function () {
              return 50;
            },
            doOnLoad: function () {}
          };
        }

        return cache(img);
      };

      toast_view.updateOpts = function (obj) {
        toast_view.updateOpts = cache_update_view;
        assert.strictEqual(100, obj.height);
      };

      toast_view.build(null, 'text', {
        bg: 'puzzle'
      });
    });
  });
});
