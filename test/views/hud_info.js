/* global PuzzleModel, timer, HudInfo, style, util_test
*/

jsio('import util.underscore as _');
jsio('import test.lib.util as util_test');
jsio('import DevkitHelper.timer as timer');

describe('HUD Info', function () {
  'use strict';

  var hud_info, puzzle,
    config_data = {
      STYLE: {
        orientation: 'portrait'
      },
      variations: {
        memory: 'memory'
      },
      powerups: [],
      bonus_level_def: [[20, 40], [13, 15], [7, 30]],
      ms_per_chapter: 20,
      lives: {
        max: 5,
        cost_for_max: 10,
        gen: 1800000,
        ad: 1,
        fb: 1
      }
    },
    prepare = function () {
      var anim_obj = {
        now: function () {
          return this;
        },
        then: function (cb) {
                if (typeof cb === 'function') {
                  cb();
                }
                return this;
              }
      };

      GC.app = {
        view: {
          updateOpts: function () {}
        },
        engine: {
          subscribe: function () {}
        }
      };

      jsio('import resources.data.config as config');
      jsio('import src.models.puzzle as PuzzleModel');

      util_test.getFromJSIOCache('resources/data/config').exports = config_data;

      util_test.removeFromJSIOCache('devkithelper/src/style');

      jsio('import resources.styles.portrait as portrait');
      util_test.getFromJSIOCache('resources/styles/portrait').exports =
        {};
      util_test.removeFromJSIOCache('src/animate.js');
      jsio('import animate');
      util_test.getFromJSIOCache('src/animate.js').exports = function () {
        return anim_obj;
      };
      jsio('import src.views.hud_info as HudInfo');
      jsio('import DevkitHelper.style as style');
      style.init(config_data.STYLE);
    },
    init = function () {
      puzzle = new PuzzleModel();
      hud_info = new HudInfo();

      hud_info.model = puzzle;
    },
    afterAll = function () {
      util_test.removeFromJSIOCache('src/animate.js');
    };

  before(prepare);
  beforeEach(init);
  after(afterAll);

  describe('build()', function () {
    it('should call the functn of the puzzle type if exists', function (done) {
      var cache = hud_info.buildThreeMatch;

      hud_info.model.set('type', 'threeMatch');
      hud_info.buildThreeMatch = function () {
        hud_info.model.set('type', null);
        hud_info.buildThreeMatch = cache;
        done();
      };
      hud_info.build(hud_info.model);
    });
  });

  describe('buildJewels()', function () {
    it('should render the jewels and arrows', function () {
      hud_info.model.set('jewels', ['circle', 'triangle', 'square']);
      hud_info.buildJewels();
      assert.strictEqual(hud_info.jewel_list.length, 3);
      assert.strictEqual(hud_info.arrows.length, 2);
    });
  });

  describe('changeJewels', function () {
    it('should update the jewels with correct positions', function (done) {
      hud_info.model.set('jewels', ['circle', 'triangle', 'square']);
      hud_info.model.set('jewel_order', ['circle']);
      hud_info.buildJewels();
      hud_info.jewel_list[0] = {
        animateJewels: function () {
          return true;
        },
        isInactive: function () {
          return true;
        },
        changeImage: function () {
          done();
        },
        updateOpts: function () {},
        jewel: {
          updateOpts: function (obj) {
            if (obj.image && obj.image.getURL() ===
              'resources/images/puzzle/circle.png') {
              done();
            }
          },
          getImage: function () {
            return {
              getURL: function () {
                  return 'resources/images/puzzle/circle_inactive.png';
                }
            };
          }
        },
        getPosition: function () {
          return {x: -15,
                  y: 10
          };
        }
      };
      hud_info.changeJewels();
    });

    it('should animate all jewels after collecting all in right order',
      function (done) {
        hud_info.model.set('jewels', ['circle', 'triangle', 'square']);
        hud_info.model.set('jewel_order', ['circle', 'triangle', 'square']);
        hud_info.buildJewels();
        hud_info.jewel_list[2] = {
          isInactive: function () {
            return true;
          },
          animateJewels: function () {
            done();
          },
          changeImage: function (arg) {
            if (arg === 'active') {
              return true;
            }
          }
        };
        hud_info.changeJewels();
      });

    it('should set cross as image for jewel in ' +
      'incorrect positions', function (done) {
      hud_info.model.set('jewels', ['circle', 'triangle', 'square']);
      hud_info.model.set('jewel_order', ['circle', 'square']);
      hud_info.buildJewels();
      hud_info.jewel_list[1] = {
        wrongAnimate: function () {
          done();
        },
        changeImage: function () {
          return true;
        }
      };
      hud_info.changeJewels();
    });

    it('should not animate the already filled jewels', function (done) {
      var cache = hud_info.jewel_list[0];

      hud_info.model.set('jewels', ['circle', 'triangle', 'square']);
      hud_info.model.set('jewel_order', ['circle']);
      hud_info.buildJewels();
      hud_info.jewel_list[0] = {
        isInactive: function () {
          return false;
        },
        jewel: {
          updateOpts: function () {
            done('error');
          },
          getImage: function () {
            return {
              getURL: function () {
                return 'resources/images/puzzle/circle.png';
              }
            };
          }
        }
      };
      hud_info.changeJewels();
      hud_info.jewel_list[0] = cache;
      done();
    });

    it('should set goal-completed if tile' +
      ' are zero and in order', function () {
        var cache = hud_info.model.set,
          cache_get = hud_info.model.get,
          cache_order = hud_info.model.isJewelNotInOrder;

        hud_info.model.set = function (param, val) {
          if (param === 'goal_completed') {
            hud_info.model.set = cache;
            assert.strictEqual(true, val);
          }
        };
        hud_info.model.isJewelNotInOrder = function () {
          hud_info.model.isJewelNotInOrder = cache_order;
          return false;
        };
        hud_info.model.set('tile_count', 0);
        hud_info.model.get = function (param) {
          if (param === 'tile_count') {
            hud_info.model.get = cache_get;
            return 0;
          }
        };
        hud_info.model.set('jewels', ['circle', 'triangle', 'square']);
        hud_info.model.set('jewel_order', ['circle', 'triangle', 'square']);
        hud_info.buildJewels();
        hud_info.changeJewels();
      });

    it('should not set goal-completed if tile' +
      ' are zero and not in order', function (done) {
        var cache = hud_info.model.set,
          cache_get = hud_info.model.get,
          cache_order = hud_info.model.isJewelNotInOrder;

        hud_info.model.set = function (param) {
          if (param === 'goal_completed') {
            done('err');
          }
        };
        hud_info.model.isJewelNotInOrder = function () {
          hud_info.model.isJewelNotInOrder = cache_order;
          return true;
        };
        hud_info.model.set('tile_count', 0);
        hud_info.model.get = function (param) {
          if (param === 'tile_count') {
            hud_info.model.get = cache_get;
            return 0;
          }
        };
        hud_info.model.set('jewels', ['circle', 'triangle', 'square']);
        hud_info.model.set('jewel_order', ['circle', 'triangle', 'square']);
        hud_info.buildJewels();
        hud_info.changeJewels();
        hud_info.model.set = cache;
        done();
      });
  });
  describe('buildThreeMatch()', function () {
    it('should highlight the tiles in hud on selected', function () {
      var cache = hud_info.highlight,
       i = 0;

      hud_info.highlight = function () {
        i++;
      };
      hud_info.buildThreeMatch(hud_info.model);
      hud_info.model.emit('change:tile_selected');
      assert.strictEqual(3, hud_info.loaded_imgs.length);
      assert.strictEqual(2, i);
      hud_info.highlight = cache;
    });
  });
  describe('highlight', function () {
    it('should update the hud info with tile selected', function () {
      hud_info.model.set('tile_selected', [1, 2, 3]);
      hud_info.buildThreeMatch(hud_info.model);
      _.each(hud_info.loaded_imgs, function (view) {
        assert.strictEqual(view._img._srcImg.url,
          'resources/images/puzzlescreen/tile_selected.png');
      });
    });
    it('should run the timer if prev_selected length is 3' +
      'and no tile is selected', function () {
      var cache = hud_info.model.getPrevious,
        cache_timer = timer.timeout;

      hud_info.model.getPrevious = function () {
        hud_info.model.getPrevious = cache;
        return [1, 2, 3];
      };
      timer.timeout = function (name, cb, length) {
        timer.timeout = cache_timer;
        assert.strictEqual(name, 'selected_highlight');
        assert.strictEqual(length, 400);
        cb();
      };
      hud_info.model.set('tile_selected', null);
      hud_info.buildThreeMatch(hud_info.model);
    });
    it('should update the hud info if all the tiles' +
      'are not selected', function () {
      var selected = 0,
        unselected = 0;

      hud_info.model.set('tile_selected', [1, 2]);
      hud_info.buildThreeMatch(hud_info.model);
      _.each(hud_info.loaded_imgs, function (view) {
        if (view._img._srcImg.url ===
          'resources/images/puzzlescreen/tile_selected.png') {
          selected++;
        } else {
          unselected++;
        }
      });
      assert.strictEqual(selected, 2);
      assert.strictEqual(unselected, 1);
    });
  });
});
