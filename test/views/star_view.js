/* global star_view, event_manager, ViewPool, style, View,
 util_test, sounds
*/

jsio('import ui.View as View');
jsio('import ui.ViewPool as ViewPool');
jsio('import ui.ImageView as ImageView');
jsio('import util.underscore as _');
jsio('import test.lib.util as util_test');

describe('Star View', function () {
  'use strict';

  var cache_get_grp,
    config_data = {
      STYLE: {
        orientation: 'portrait',
        bound_width: 640,
        bound_height: 960,
        tablet: 0.8,
        phablet: 0.9
      }
    },
    anim_obj = {
      now: function () {
        return this;
      },
      wait: function () {
        return this;
      },
      then: function (cb) {
        if (_.isFunction(cb)) {
          cb();
        }
        return this;
      },
      pause: function () {
        return this;
      },
      clear: function () {
        return this;
      }
    },
    prepare = function () {
      GC.app = {
        view: {
          updateOpts: function () {}
        },
        getCurrentView: function () {
          return new View();
        }
      };
      util_test.removeFromJSIOCache('src/animate.js');
      jsio('import animate');
      util_test.getFromJSIOCache('src/animate.js').exports = function () {
        return anim_obj;
      };

      util_test.removeFromJSIOCache('src/modules/sounds.js');
      jsio('import quest.modules.sounds as sounds');

      jsio('import DevkitHelper.event_manager as event_manager');
      event_manager.plugins = [];
      jsio('import DevkitHelper.style as style');
      style.init(config_data.STYLE);
    },
    setup_create = function () {
      util_test.removeFromJSIOCache('src/views/star_view.js');
      jsio('import src.views.star_view as star_view');

      cache_get_grp = util_test.getFromJSIOCache('src/animate.js')
        .exports.getGroup;
      util_test.getFromJSIOCache('src/animate.js')
        .exports.getGroup = function () {
          util_test.getFromJSIOCache('src/animate.js')
            .exports.getGroup = cache_get_grp;
        };
    },
    clean = function () {
      util_test.getFromJSIOCache('src/animate.js')
        .exports.getGroup = cache_get_grp;
      util_test.removeFromJSIOCache('src/animate.js');
      util_test.removeFromJSIOCache('star_view.js');
    };

  before(prepare);
  beforeEach(setup_create);
  after(clean);

  describe('prepare()', function () {
    it('should test prepare', function () {
      star_view.prepare();
      assert.equal(true, star_view instanceof View);
      assert.equal(true, star_view.stars instanceof ViewPool);
      assert.equal(true, star_view.inactive_stars instanceof ViewPool);
    });
  });

  describe('populateStars()', function () {
    it('should call for prepare isprepare is false', function (done) {
      var cache = star_view.prepare;

      star_view.prepare = function () {
        star_view.prepare = cache;
        star_view.prepare();
        done();
      };

      star_view.populateStars(1, new View(), 'play');
      star_view.clean();
    });
    it('should not call for prepare isprepare is true', function (done) {
      var cache = star_view.prepare;

      star_view.prepare();
      star_view.prepare = function () {
        star_view.prepare = cache;
        done('error');
      };
      star_view.populateStars(1, star_view, 'play');
      star_view.clean();
      done();
    });
  });

  describe('showStar()', function () {
    it('should play sound', function (done) {
      var cache = star_view.prepare,
        play_cache,
        view = new View();

      star_view.prepare = function () {
        star_view.prepare = cache;
        star_view.prepare();
      };

      star_view.populateStars(1, new View(), 'play');
      play_cache = sounds.play;
      star_view._star_array = [view];
      sounds.play = function () {
        sounds.play = play_cache;
        done();
      };
      star_view._showStar(2);
    });

    it('should call animate', function (done) {
      var cache = star_view.prepare,
        view = new View(),
        cache_anim = anim_obj.then;

      star_view.prepare = function () {
        star_view.prepare = cache;
        star_view.prepare();
      };

      star_view.populateStars(1, new View(), 'play');
      star_view._star_array = [view];

      anim_obj.then = function () {
        anim_obj.then = cache_anim;
        done();
        return this;
      };
      star_view._showStar(0);
    });
  });

  describe('jumpStars()', function () {
    it('should call animate', function (done) {
      var cache = star_view.prepare,
        view = new View(),
        then_cache = anim_obj.then;

      star_view.prepare = function () {
        star_view.prepare = cache;
        star_view.prepare();
      };

      star_view.populateStars(3, new View(), 'play');
      anim_obj.then = function () {
        anim_obj.then = then_cache;
        done();
      };
      star_view._jumpStars(view);
    });
  });

  describe('clean()', function () {
    it('should clean starview', function (done) {
      var flag = 0,
        cache, cache2;

      star_view.prepare('play');
      star_view.populateStars(3, new View(), 'play');
      cache = star_view.inactive_stars.releaseAllViews;
      cache2 = star_view.stars.releaseAllViews;

      star_view.inactive_stars.releaseAllViews = function () {
        star_view.inactive_stars.releaseAllViews = cache;
        flag = 1;
      };
      star_view.stars.releaseAllViews = function () {
        star_view.stars.releaseAllViews = cache2;
        if (flag === 1) {
          done();
        }
      };
      star_view.clean();
    });

    it('should clean animation', function (done) {
      var flag = 0,
        clear_anim = anim_obj.clear,
        cache, cache2;

      star_view.prepare('play');
      star_view.populateStars(3, new View(), 'play');
      cache = star_view.inactive_stars.releaseAllViews;
      cache2 = star_view.stars.releaseAllViews;

      star_view.inactive_stars.releaseAllViews = function () {
        star_view.inactive_stars.releaseAllViews = cache;
        flag = 1;
      };
      util_test.getFromJSIOCache('src/animate.js')
        .exports.getGroup = function () {
          util_test.getFromJSIOCache('src/animate.js')
            .exports.getGroup = cache_get_grp;
          return anim_obj;
        };
      anim_obj.clear = function () {
        anim_obj.clear = clear_anim;
        done();
      };
      star_view.clean();
    });

    it('should call removeFromSuperview', function (done) {
      var cache;

      star_view.prepare('play');
      star_view.populateStars(3, new View(), 'play');
      cache = star_view.removeFromSuperview;

      star_view.removeFromSuperview = function () {
        star_view.releaseAllViews = cache;
        done();
      };
      star_view.clean();
    });

    it('should stop sounds', function (done) {
      var cache_stop = sounds.stop,
        clear_anim = anim_obj.clear,
        cache, cache2;

      star_view.prepare('play');
      star_view.populateStars(3, new View(), 'play');
      cache = star_view.inactive_stars.releaseAllViews;
      cache2 = star_view.stars.releaseAllViews;

      star_view.inactive_stars.releaseAllViews = function () {
        star_view.inactive_stars.releaseAllViews = cache;
      };
      util_test.getFromJSIOCache('src/animate.js')
        .exports.getGroup = function () {
          util_test.getFromJSIOCache('src/animate.js')
            .exports.getGroup = cache_get_grp;
          return anim_obj;
        };
      anim_obj.clear = function () {
        anim_obj.clear = clear_anim;
      };

      sounds.stop = function (file_name) {
        sounds.stop = cache_stop;
        assert.strictEqual('level_won', file_name);
        done();
      };
      star_view.clean();
    });
  });
});
