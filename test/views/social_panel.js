/* global social_panel, style, View, User,
  TextView
*/

jsio('import ui.View as View');
jsio('import ui.TextView as TextView');

describe('Social Panel', function () {
  'use strict';

  var config_data = {
      STYLE: {
        orientation: 'portrait',
        bound_width: 640,
        bound_height: 960,
        tablet: 0.8,
        phablet: 0.9
      }
    },
    prepare = function () {
      GC.app = {
        view: {
          updateOpts: function () {}
        },
        getCurrentView: function () {
          return new View();
        }
      };
      jsio('import DevkitHelper.event_manager as event_manager');

      jsio('import DevkitHelper.style as style');
      style.init(config_data.STYLE);

      jsio('import quest.views.social_panel as social_panel');
      jsio('import quest.models.user as User');
      GC.app.user = new User();
      GC.app.user.set({
        social_facebook: true
      });
    },
    setup_create = function () {
      social_panel.build(1);
    };

  before(prepare);
  beforeEach(setup_create);

  describe('prepare()', function () {
    it('should test initialization of social-panel', function () {
      assert.equal(true, social_panel instanceof View);
    });
    it('should test social-panel message view', function () {
      assert.equal(true, social_panel.message instanceof TextView);
    });
  });
  describe('build()', function () {
    it('should test message_view update', function (done) {
      var cache = social_panel.message.updateOpts;

      social_panel.message.updateOpts = function (item) {
        assert.strictEqual('social_panel_msg', item.text);
        social_panel.message.updateOpts = cache;
        done();
      };
      GC.app.user.set({
        social_facebook: false
      });
      social_panel.build(1);
    });
  });
  describe('clear()', function () {
    it('should test message_view clean', function (done) {
      var cache = social_panel.message.hide;

      social_panel.message.hide = function () {
        social_panel.message.hide = cache;
        done();
      };

      social_panel.clean();
    });
  });
});
