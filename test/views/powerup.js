/* global Powerup, popup, powerup_info, util, hc_timer */

jsio('import test.lib.util as util_test');
jsio('import quest.models.user as User');
jsio('import DevkitHelper.style as style');
jsio('import quest.modules.util as util');
describe('settings', function () {
  'use strict';

  var powerup;

  before(function () {
    jsio('import quest.modules.popup_manager as popup');
    jsio('import src.views.powerup as Powerup');
    jsio('import DevkitHelper.timer as hc_timer');
    jsio('import src.views.popups.powerup_info as powerup_info');
    popup.next = function () { };
    popup.add = function () { };
    powerup = new Powerup();
    GC.app.user = {
      get: function () { },
      on: function () { },
      getMs: function () { },
      isUnlocked: function () {
        return true;
      }
    };
  });

  describe('build()', function () {
    it('should call tutorial build on long press', function (done) {
      var cache = powerup_info.build,
        cache_timer = hc_timer.timeout,
        cache_on = powerup.on;

      util.getProducts = function () {
        return {
          hint: {
            cost: 50
          }
        };
      };
      powerup_info.build = function () {
        powerup_info.build = cache;
        done();
      };
      powerup.on = function (param, cb) {
        if (param === 'InputStart') {
          cb();
        }
      };
      hc_timer.timeout = function (tag, cb, delay) {
        hc_timer.timeout = cache_timer;
        assert.strictEqual(tag, 'touch');
        assert.strictEqual(delay, 500);
        cb();
      };
      powerup.build('hint');
      powerup.once('InputStart');
      powerup.on = cache_on;
    });
    it('should unregister the touch timer on Input out', function (done) {
      var cache_timer = hc_timer.clearTimeout,
        cache_on = powerup.on;

      util.getProducts = function () {
        return {
          hint: {
            cost: 50
          }
        };
      };
      powerup.on = function (param, cb) {
        if (param === 'InputOut') {
          cb();
        }
      };
      hc_timer.clearTimeout = function (tag) {
        hc_timer.clearTimeout = cache_timer;
        assert.strictEqual(tag, 'touch');
        done();
      };
      powerup.build('hint');
      powerup.once('InputOut');
      powerup.on = cache_on;
    });

    it('should not register Input listener' +
      'if powerup not unlocked', function (done) {
        powerup.on = function (param) {
          if (param === 'InputStart' || param === 'InputOut') {
            done('err');
          }
        };
        GC.app.user.isUnlocked = function () {
          return false;
        };
        powerup.build('hint');
        done();
      });
  });
});
