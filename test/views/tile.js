/* global Tile, TileModel, animate:true, util_test, _,
  event_manager, View, style, image, effects, util */

jsio('import test.lib.util as util_test');
jsio('import util.underscore as _');
jsio('import ui.View as View');
jsio('import ui.ImageView as ImageView');
jsio('import animate');

describe('Tile', function () {
  'use strict';

  var tile, tile_model, puzzle_model,
    config_data = {
      STYLE: {
        orientation: 'portrait',
        bound_width: 640,
        bound_height: 960,
        tablet: 0.8,
        phablet: 0.9
      }
    },
    style_data = {
      tile_size: {
        width: 98,
        height: 130
      }
    },
    prepare = function () {
      jsio('import animate');
      GC.app = {
        view: {
          updateOpts: function () {}
        },
        getCurrentView: function () {
          return new View();
        },
        addSubview: function () {
          return;
        },
        engine: {
          subscribe: function () {}
        },
        user: {
          get: function (attr) {
            return attr;
          }
        }
      };
      jsio('import effects');
      jsio('import quest.modules.image as image');
      jsio('import DevkitHelper.event_manager as event_manager');
      jsio('import quest.modules.util as util;');
      event_manager.plugins = [];
      jsio('import resources.styles.portrait as portrait');
      util_test.getFromJSIOCache('resources/styles/portrait').exports =
        style_data;
      jsio('import DevkitHelper.style as style');
      style.init(config_data.STYLE);
      jsio('import src.views.tile as Tile');
      jsio('import src.models.tile as TileModel');
      jsio('import DevkitHelper.model as Model;');
      jsio('import effects');
    },
    setup_create = function () {
      jsio('import animate');
      tile = new Tile();
      tile_model = new TileModel();
      puzzle_model = new Model();
      puzzle_model.set({ongoing: 0});
      tile_model.set('puzzle', puzzle_model);
      tile.model = tile_model;
    };

  before(prepare);
  beforeEach(setup_create);
  afterEach(function () {
    util_test.removeFromJSIOCache('src/animate.js');
  });

  describe('init()', function () {});

  describe('build()', function () {
    it('should add listener InputStart', function (done) {
      tile_model.pick = function () {
        done();
      };
      tile.build(tile_model);

      tile.emit('InputStart');
    });

    it('should register drag listener InputStart', function (done) {
      var cache = util.getDeviceInfo;

      tile.registerDrag = function () {
        done();
      };
      tile_model.get('puzzle').set('open_slot', true);
      util.getDeviceInfo = function () {
        util.getDeviceInfo = cache;
        return 'web';
      };
      tile_model.pick = function () {};
      tile.build(tile_model);

      tile.emit('InputStart');
    });

    it('should add onChangeFace listener' +
      'if golden tile remaining', function (done) {
      var cache = tile.onChangeFace;

      tile.onChangeFace = function () {
        tile.onChangeFace = cache;
        done();
      };
      tile_model.get('puzzle').set('given_golden', 2);
      tile_model.get('puzzle').set('collected_golden', 0);
      tile.build(tile_model);

      tile_model.set('face', 'f_golden');
    });

    it('should add InputMove listener if open_slot enabled', function (done) {
      tile.registerDrag = function () {
        done();
      };
      tile_model.get('puzzle').set('open_slot', true);
      tile.build(tile_model);

      tile.emit('InputMove');
    });

    it('should add listener highlight', function (done) {
      tile.highlight = function () {
        done();
      };
      tile.build(tile_model);

      tile_model.emit('highlight');
    });

    it('should add listener match-animate', function (done) {
      tile.matchAnimate = function () {
        done();
      };
      tile.build(tile_model);

      tile_model.emit('match-animate');
    });

    it('should add listener shuffle-animate', function (done) {
      tile.shuffleAnimate = function () {
        done();
      };
      tile.build(tile_model);

      tile_model.emit('shuffle-animate');
    });

    it('should add listener clean', function (done) {
      tile.clean = function () {
        done();
      };
      tile.build(tile_model);

      tile_model.emit('clean');
    });

    it('should add listener to change of state', function (done) {
      var ctr = 0;

      tile.changeImage = function () {
        if (++ctr === 1) {
          return;
        }
        done();
      };

      tile.build(tile_model);

      tile_model.set('state', 3);
    });

    it('should invoke changeImage', function (done) {
      tile.changeImage = done;

      tile.build(tile_model);
    });

    it('should add listener change-face', function (done) {
      var ctr = 0;

      tile.changeFace = function () {
        ++ctr;

        if (ctr === 2) {
          done();
        }
      };
      tile.build(tile_model);

      tile_model.emit('change-face');
    });

    it('should update overlay', function (done) {
      var cache = tile.overlay.updateOpts;

      tile.overlay.updateOpts = function (opts) {
        assert.strictEqual('resources/images/puzzle/tile_overlay.png',
          opts.image._srcImg.url);
        done();
        tile.overlay.updateOpts = cache;
      };
      tile.build(tile_model);
    });
  });

  describe('highlight', function () {
    it('should call model hint', function () {
      var hint = tile.model.showHint;

      tile.model.showhint = function (unset) {
        assert.strictEqual(undefined, unset);
        tile.model.showHint = hint;
      };
      tile.highlight();
    });

    it('should set scale and zindex', function () {
      var hover = effects.hover;

      effects.hover = function () {
        effects.hover = hover;
        return;
      };
      tile.highlight();
      assert.strictEqual(1.2, tile.style.scale);
      assert.strictEqual(999, tile.style.zIndex);
    });

    it('should call effects hover', function (done) {
      var hover = effects.hover;

      effects.hover = function () {
        effects.hover = hover;
        done();
      };
      tile.highlight();
    });
  });

  describe('changeTag', function () {
    it('should show the tag if there is a tag', function (done) {
      var cache = tile.tag.show;

      tile.tag.show = function () {
        tile.tag.show = cache;
        done();
      };
      tile.model.set('tag', 'circle');
      tile.changeTag();
    });
  });
  describe('onChangeFace', function () {
    it('should call the changeImage for golden tile', function (done) {
      var cache = tile.changeImage;

      tile.changeImage = function () {
        tile.changeImage = cache;
        done();
      };
      tile.model.set('face', 'f_golden');
      tile.onChangeFace();
    });

    it('should call change image if previous ' +
    'one was golden tile', function (done) {
      var cache_change = tile.changeImage,
        cache = tile_model.getPrevious;

      tile.changeImage = function () {
        tile.changeImage = cache_change;
        done();
      };
      tile_model.getPrevious = function () {
        tile_model.getPrevious = cache;
        return 'f_golden';
      };
      tile.model.set('face', 'f_joker');
      tile.onChangeFace();
    });

    it('should not call change image if previous ' +
    'one or current tile not golden tile', function (done) {
      var cache_change = tile.changeImage,
        cache = tile_model.getPrevious;

      tile.changeImage = function () {
        tile.changeImage = cache_change;
        done('err');
      };
      tile_model.getPrevious = function () {
        tile_model.getPrevious = cache;
        return 'f_joker';
      };
      tile.model.set('face', 'f_joker');
      tile.onChangeFace();
      done();
    });
  });

  describe('matchAnimate', function () {
    it('should decrement ongoing in end', function () {
      var cache = animate,
        anim_obj = {
          now: function () {
            return this;
          },
          wait: function () {
            return this;
          },
          then: function (cb) {
            if (_.isFunction(cb)) {
              cb();
            }
            return this;
          }
        };

      util_test.removeFromJSIOCache('src/views/tile.js');

      util_test.getFromJSIOCache('src/animate.js').exports = function () {
        animate = cache;
        return anim_obj;
      };
      jsio('import src.views.tile as Tile');
      tile = new Tile();
      tile.model = tile_model;
      tile.matchAnimate(function () {});
      assert.equal(0, tile.model.get('puzzle').get('ongoing'));
    });

    it('should increment first ongoing', function () {
      var cache = animate,
        anim_obj = {
          now: function () {
            return this;
          },
          wait: function () {
            return this;
          },
          then: function () {
            return this;
          }
        };

      util_test.removeFromJSIOCache('src/views/tile.js');

      util_test.getFromJSIOCache('src/animate.js').exports = function () {
        animate = cache;
        return anim_obj;
      };
      jsio('import src.views.tile as Tile');
      tile = new Tile();
      tile.model = tile_model;
      tile.matchAnimate();
      assert.equal(1, tile.model.get('puzzle').get('ongoing'));
    });

    it('should invoke animate', function (done) {
      var cache = animate,
        anim_obj = {
          now: function () {
            done();
            return this;
          },
          wait: function () {
            return this;
          },
          then: function () {
            return this;
          }
        };

      util_test.removeFromJSIOCache('src/views/tile.js');

      util_test.getFromJSIOCache('src/animate.js').exports = function () {
        animate = cache;
        return anim_obj;
      };

      jsio('import src.views.tile as Tile');
      tile = new Tile();
      tile.model = tile_model;

      tile.matchAnimate();
    });

    it('should invoke cb if passed', function (done) {
      var cache = animate,
        anim_obj = {
          now: function () {
            return this;
          },
          wait: function () {
            return this;
          },
          then: function (cb) {
            if (_.isFunction(cb)) {
              cb();
            }
            return this;
          }
        };

      util_test.removeFromJSIOCache('src/views/tile.js');

      util_test.getFromJSIOCache('src/animate.js').exports = function () {
        animate = cache;
        return anim_obj;
      };

      jsio('import src.views.tile as Tile');
      tile = new Tile();
      tile.model = tile_model;
      tile.matchAnimate(done);
    });
    it('should call animate pause if puzzle_paused set ', function (done) {
      var cache = animate,
        anim_obj = {
          now: function () {
            return this;
          },
          wait: function () {
            return this;
          },
          then: function (cb) {
            if (_.isFunction(cb)) {
              cb();
            }
            return this;
          },
          pause: done
        };

      util_test.removeFromJSIOCache('src/views/tile.js');

      util_test.getFromJSIOCache('src/animate.js').exports = function () {
        animate = cache;
        return anim_obj;
      };

      jsio('import src.views.tile as Tile');
      tile = new Tile();
      tile.model = tile_model;

      puzzle_model.set('puzzle_paused', true);
      tile.matchAnimate(function () {});
    });
    it('should not call animate pause if puzzle_paused set ', function (done) {
      var cache = animate,
        anim_obj = {
          now: function () {
            return this;
          },
          wait: function () {
            return this;
          },
          then: function (cb) {
            if (_.isFunction(cb)) {
              cb();
            }
            return this;
          },
          pause: function () {
            done('error');
          }
        };

      util_test.removeFromJSIOCache('src/views/tile.js');

      util_test.getFromJSIOCache('src/animate.js').exports = function () {
        animate = cache;
        return anim_obj;
      };

      jsio('import src.views.tile as Tile');
      tile = new Tile();
      tile.model = tile_model;

      puzzle_model.set('puzzle_paused', false);
      tile.matchAnimate(done);
    });
  });

  describe('swapAnimate', function () {
    it('should decrement ongoing in end', function () {
      var cache = animate,
        anim_obj = {
          now: function () {
            return this;
          },
          wait: function () {
            return this;
          },
          then: function (cb) {
            if (_.isFunction(cb)) {
              cb();
            }
            return this;
          }
        };

      util_test.removeFromJSIOCache('src/views/tile.js');

      util_test.getFromJSIOCache('src/animate.js').exports = function () {
        animate = cache;
        return anim_obj;
      };
      jsio('import src.views.tile as Tile');
      tile = new Tile();
      tile.model = tile_model;
      tile.swapAnimate();
      assert.equal(0, tile.model.get('puzzle').get('ongoing'));
    });

    it('should set handle events false', function (done) {
      var cache = animate,
        cache_handle_events = tile.setHandleEvents,
        anim_obj = {
          now: function () {
            return this;
          },
          wait: function () {
            return this;
          },
          then: function (cb) {
            if (_.isFunction(cb)) {
              cb();
            }
            return this;
          }
        };

      util_test.removeFromJSIOCache('src/views/tile.js');

      util_test.getFromJSIOCache('src/animate.js').exports = function () {
        animate = cache;
        return anim_obj;
      };
      jsio('import src.views.tile as Tile');
      tile = new Tile();
      tile.model = tile_model;
      tile.setHandleEvents = function (boolean) {
        tile.setHandleEvents = cache_handle_events;
        if (boolean === false) {
          done();
        }
      };
      tile.swapAnimate();
    });

    it('should increment first ongoing', function () {
      var cache = animate,
        anim_obj = {
          now: function () {
            return this;
          },
          wait: function () {
            return this;
          },
          then: function () {
            return this;
          }
        };

      util_test.removeFromJSIOCache('src/views/tile.js');

      util_test.getFromJSIOCache('src/animate.js').exports = function () {
        animate = cache;
        return anim_obj;
      };
      jsio('import src.views.tile as Tile');
      tile = new Tile();
      tile.model = tile_model;
      tile.swapAnimate();
      assert.equal(1, tile.model.get('puzzle').get('ongoing'));
    });

    it('should invoke animate', function (done) {
      var cache = animate,
        anim_obj = {
          now: function () {
            done();
            return this;
          },
          wait: function () {
            return this;
          },
          then: function () {
            return this;
          }
        };

      util_test.removeFromJSIOCache('src/views/tile.js');

      util_test.getFromJSIOCache('src/animate.js').exports = function () {
        animate = cache;
        return anim_obj;
      };

      jsio('import src.views.tile as Tile');
      tile = new Tile();
      tile.model = tile_model;

      tile.swapAnimate();
    });

    it('should invoke cb if passed', function (done) {
      var cache = animate,
        anim_obj = {
          now: function () {
            return this;
          },
          wait: function () {
            return this;
          },
          then: function (cb) {
            if (_.isFunction(cb)) {
              cb();
            }
            return this;
          }
        };

      util_test.removeFromJSIOCache('src/views/tile.js');

      util_test.getFromJSIOCache('src/animate.js').exports = function () {
        animate = cache;
        return anim_obj;
      };

      jsio('import src.views.tile as Tile');
      tile = new Tile();
      tile.model = tile_model;
      tile.swapAnimate(done);
    });
  });

  describe('afterSwapAnimate', function () {
    it('should decrement ongoing in end', function () {
      var cache = animate,
        anim_obj = {
          now: function () {
            return this;
          },
          wait: function () {
            return this;
          },
          then: function (cb) {
            if (_.isFunction(cb)) {
              cb();
            }
            return this;
          }
        };

      util_test.removeFromJSIOCache('src/views/tile.js');

      util_test.getFromJSIOCache('src/animate.js').exports = function () {
        animate = cache;
        return anim_obj;
      };
      jsio('import src.views.tile as Tile');
      tile = new Tile();
      tile.model = tile_model;
      tile.afterSwapAnimate(function () {});
      assert.equal(0, tile.model.get('puzzle').get('ongoing'));
    });

    it('should set handle events false', function (done) {
      var cache = animate,
        cache_handle_events = tile.setHandleEvents,
        anim_obj = {
          now: function () {
            return this;
          },
          wait: function () {
            return this;
          },
          then: function (cb) {
            if (_.isFunction(cb)) {
              cb();
            }
            return this;
          }
        };

      util_test.removeFromJSIOCache('src/views/tile.js');

      util_test.getFromJSIOCache('src/animate.js').exports = function () {
        animate = cache;
        return anim_obj;
      };
      jsio('import src.views.tile as Tile');
      tile = new Tile();
      tile.model = tile_model;
      tile.setHandleEvents = function (boolean) {
        tile.setHandleEvents = cache_handle_events;
        if (boolean === false) {
          done();
        }
      };
      tile.afterSwapAnimate(function () {});
    });

    it('should increment first ongoing', function () {
      var cache = animate,
        anim_obj = {
          now: function () {
            return this;
          },
          wait: function () {
            return this;
          },
          then: function () {
            return this;
          }
        };

      util_test.removeFromJSIOCache('src/views/tile.js');

      util_test.getFromJSIOCache('src/animate.js').exports = function () {
        animate = cache;
        return anim_obj;
      };
      jsio('import src.views.tile as Tile');
      tile = new Tile();
      tile.model = tile_model;
      tile.afterSwapAnimate(function () {});
      assert.equal(1, tile.model.get('puzzle').get('ongoing'));
    });

    it('should invoke animate', function (done) {
      var cache = animate,
        anim_obj = {
          wait: function () {
            return this;
          },
          now: function () {
            done();
            return this;
          },
          then: function () {
            return this;
          }
        };

      util_test.removeFromJSIOCache('src/views/tile.js');

      util_test.getFromJSIOCache('src/animate.js').exports = function () {
        animate = cache;
        return anim_obj;
      };

      jsio('import src.views.tile as Tile');
      tile = new Tile();
      tile.model = tile_model;
      tile.afterSwapAnimate(function () {});
    });

    it('should invoke change changeHandle events to true', function (done) {
      var cache = animate,
        cache_handle_events = tile.setHandleEvents,
        anim_obj = {
          now: function () {
            return this;
          },
          wait: function () {
            return this;
          },
          then: function (cb) {
            if (_.isFunction(cb)) {
              cb();
            }
            return this;
          }
        };

      util_test.removeFromJSIOCache('src/views/tile.js');

      util_test.getFromJSIOCache('src/animate.js').exports = function () {
        animate = cache;
        return anim_obj;
      };

      jsio('import src.views.tile as Tile');
      tile = new Tile();
      tile.model = tile_model;
      tile.setHandleEvents = function (boolean) {
        if (boolean) {
          tile.setHandleEvents = cache_handle_events;
          done();
        }
      };
      tile.afterSwapAnimate(function () {});
    });
  });

  describe('wrongMatchAnimate', function () {
    it('should invoke wrongMatch animate', function (done) {
      var cache = animate,
        i = 0,
        anim_obj = {
          then: function (cb) {
            i++;
            if (i === 3) {
              done();
            }
            if (_.isFunction(cb)) {
              cb();
            }
            return this;
          }
        };

      util_test.removeFromJSIOCache('src/views/tile.js');

      util_test.getFromJSIOCache('src/animate.js').exports = function () {
        animate = cache;
        return anim_obj;
      };

      jsio('import src.views.tile as Tile');
      tile = new Tile();
      tile.model = tile_model;

      tile.wrongMatchAnimate();
    });
    it('should return to old position', function () {
      var orig_x, orig_y;

      jsio('import src.views.tile as Tile');
      tile = new Tile();
      orig_x = tile.style.x;
      orig_y = tile.style.y;
      tile.model = tile_model;
      tile.wrongMatchAnimate();
      assert.strictEqual(orig_x, tile.style.x);
      assert.strictEqual(orig_y, tile.style.y);
    });
    it('should invoke callback', function (done) {
      var cache = animate,
        anim_obj = {
          then: function (cb) {
            if (_.isFunction(cb)) {
              cb();
            }
            return this;
          }
        };

      util_test.removeFromJSIOCache('src/views/tile.js');

      util_test.getFromJSIOCache('src/animate.js').exports = function () {
        animate = cache;
        return anim_obj;
      };

      jsio('import src.views.tile as Tile');
      tile = new Tile();
      tile.model = tile_model;

      tile.wrongMatchAnimate(function () {
        done();
      });
    });
  });

  describe('shuffleAnimate', function () {
    it('should increment first ongoing', function () {
      var cache = animate,
        anim_obj = {
          now: function () {
            return this;
          },
          wait: function () {
            return this;
          },
          then: function () {
            return this;
          }
        };

      util_test.removeFromJSIOCache('src/views/tile.js');

      util_test.getFromJSIOCache('src/animate.js').exports = function () {
        animate = cache;
        return anim_obj;
      };
      jsio('import src.views.tile as Tile');
      tile = new Tile();
      tile.getSuperview = function () {
        return new View();
      };
      tile_model.isSlot = function () {
        return false;
      };
      tile.model = tile_model;
      tile.shuffleAnimate();
      assert.equal(1, tile.model.get('puzzle').get('ongoing'));
    });

    it('should invoke animate', function (done) {
      var cache = animate,
        anim_obj = {
          now: function () {
            done();
            return this;
          },
          wait: function () {
            return this;
          },
          then: function () {
            return this;
          }
        };

      util_test.removeFromJSIOCache('src/views/tile.js');

      util_test.getFromJSIOCache('src/animate.js').exports = function () {
        animate = cache;
        return anim_obj;
      };

      jsio('import src.views.tile as Tile');
      tile = new Tile();
      tile.getSuperview = function () {
        return new View();
      };
      tile.model = tile_model;
      tile_model.isSlot = function () {
        return true;
      };

      tile.shuffleAnimate();
    });

    it('should add listener for shuffle-complete event', function (done) {
      var cache = animate,
        anim_obj = {
          now: function () {
            return this;
          },
          wait: function () {
            return this;
          },
          then: function () {
            return this;
          }
        },
        cache_anim;

      util_test.removeFromJSIOCache('src/views/tile.js');

      util_test.getFromJSIOCache('src/animate.js').exports = function () {
        animate = cache;
        return anim_obj;
      };

      jsio('import src.views.tile as Tile');
      tile = new Tile();
      tile.getSuperview = function () {
        return new View();
      };
      tile.model = tile_model;
      tile_model.isSlot = function () {
        return false;
      };

      cache_anim = tile.shuffleAnimComplete;
      tile.shuffleAnimComplete = function () {
        tile.shuffleAnimComplete = cache_anim;
        done();
      };
      tile.shuffleAnimate();
      tile_model.get('puzzle').emit('shuffle-complete');
    });
  });

  describe('shuffleAnimComplete', function () {
    it('should invoke animate wait first', function (done) {
      var cache = animate,
        anim_obj = {
          now: function () {
            return this;
          },
          wait: function () {
            done();
            return this;
          },
          then: function () {
            return this;
          }
        };

      util_test.removeFromJSIOCache('src/views/tile.js');

      util_test.getFromJSIOCache('src/animate.js').exports = function () {
        animate = cache;
        return anim_obj;
      };
      jsio('import src.views.tile as Tile');
      tile = new Tile();
      tile.getSuperview = function () {
        return new View();
      };
      tile.model = tile_model;
      tile.shuffleAnimComplete(0, 0);
    });

    it('should invoke animate then  with function', function () {
      var cache = animate,
        count = 1,
        anim_obj = {
          now: function () {
            return this;
          },
          wait: function () {
            return this;
          },
          then: function (param) {
            if (count === 1) {
              assert.deepEqual({
                x: 1,
                y: 2
              }, param);
            }
            count++;
            return this;
          },
          clear: function () {
            return this;
          }
        };

      util_test.removeFromJSIOCache('src/views/tile.js');

      util_test.getFromJSIOCache('src/animate.js').exports = function () {
        animate = cache;
        return anim_obj;
      };
      jsio('import src.views.tile as Tile');
      tile = new Tile();
      tile.getSuperview = function () {
        return new View();
      };
      tile.model = tile_model;
      tile.shuffleAnimComplete(1, 2);
    });

    it('should decrement ongoing', function () {
      var cache = animate,
        anim_obj = {
          now: function () {
            return this;
          },
          wait: function () {
            return this;
          },
          then: function (param) {
            if (_.isFunction(param)) {
              param();
            }
            return this;
          },
          clear: function () {
            return this;
          }
        };

      util_test.removeFromJSIOCache('src/views/tile.js');

      util_test.getFromJSIOCache('src/animate.js').exports = function () {
        animate = cache;
        return anim_obj;
      };
      jsio('import src.views.tile as Tile');
      tile = new Tile();
      tile.getSuperview = function () {
        return new View();
      };
      tile.model = tile_model;
      tile.shuffleAnimComplete();
      assert.equal(tile_model.get('puzzle').get('ongoing'), -1);
    });
  });
  describe('changeImage()', function () {
    it('should update image based on state', function (done) {
      var cache = tile.updateOpts;

      tile.model.set('state', 1);

      tile.updateOpts = function (opts) {
        assert.strictEqual('resources/images/puzzle/tile_1.png',
          opts.image._srcImg.url);
        done();
        tile.updateOpts = cache;
      };
      tile.changeImage();
    });

    it('should update state based on golden tile face', function (done) {
      var cache = tile.updateOpts;

      tile.model.set('face', 'f_golden');
      tile.model.set('state', 1);

      tile.updateOpts = function (opts) {
        assert.strictEqual('resources/images/puzzle/gold_1.png',
          opts.image._srcImg.url);
        done();
        tile.updateOpts = cache;
      };
      tile.changeImage();
    });

    it('should update the scale and zIndex if tile was hinted', function () {
      var cache = tile.updateOpts,
          cache_getPrev = tile.model.getPrevious,
          last_scale = 1.2,
          last_zIndex = 9999;

      tile.prev_zIndex = 1;
      tile.prev_scale = 1;

      tile.model.getPrevious = function (param) {
        if (param === 'state') {
          tile.model.getPrevious = cache_getPrev;
          return 3;
        }
      };

      tile.updateOpts = function (param) {
        assert.notStrictEqual(tile.style.scale, param.scale);
        assert.notStrictEqual(tile.style.zIndex, param.zIndex);
        tile.updateOpts = cache;
      };
      tile.style.scale = last_scale;
      tile.style.last_zIndex = last_zIndex;
      tile.changeImage();
    });

    it('should update overlay based on state', function (done) {
      var cache = tile.overlay.hide;

      tile.model.set('state', 1);

      tile.overlay.hide = function () {
        done();
        tile.overlay.hide = cache;
      };
      tile.changeImage();
    });
  });

  describe('changeFace()', function () {
    it('should update image from face attribute', function (done) {
      var cache = image.get;

      tile.model.set('face', 3);

      GC.app.user.get = function () {
        return 'classic';
      };

      image.get = function (file) {
        assert.strictEqual(file, 'puzzle/faces/classic/3');
        done();
        image.get = cache;
        return {
          getHeight: function () {},
          getWidth: function () {},
          doOnLoad: function () {}
        };
      };
      tile.changeFace();
    });

    it('should update image from passed face', function (done) {
      var cache = image.get;

      GC.app.user.get = function () {
        return 'classic';
      };

      image.get = function (file) {
        assert.strictEqual(file, 'puzzle/faces/classic/4');
        done();
        image.get = cache;
        return {
          getHeight: function () {},
          getWidth: function () {},
          doOnLoad: function () {}
        };
      };
      tile.changeFace(4);
    });

    it('should call getJokerFace if face is joker' +
      'and memory match variation', function (done) {
      var cache = tile.getJokerFace;

      tile.getJokerFace = function () {
        tile.getJokerFace = cache;
        tile.model.get('puzzle').set('type', null);
        done();
      };

      tile.model.set('hidden', true);
      tile.model.get('puzzle').set('type', 'memory');
      tile.changeFace('f_joker');
    });
  });

  describe('substituteAnimate()', function () {
    it('should call effects.explode', function (done) {
      var cache = effects.explode;

      effects.explode = function (parnt, opt) {
        effects.explode = cache;
        assert.strictEqual(opt.images[0],
          'resources/images/puzzlescreen/substitute_smoke.png');
        done();
      };

      tile.substituteAnimate(function () {});
    });
    it('should call effects.explode and cb', function (done) {
      var i = 0,
        cache_explode = effects.explode;

      effects.explode = function () {
        effects.explode = cache_explode;
        i++;
      };

      tile.substituteAnimate(function () {
        if (i === 1) {
          done();
        }
      });
    });
  });

  describe('getJokerFace()', function () {
    it('should return joker face for type', function (done) {
      var cache = tile.model.get;

      tile.model.get = function () {
        tile.model.get = cache;
        return {
          get: function () {
            return 'memory';
          }
        };
      };

      done(tile.getJokerFace('f_joker', false) === 'memory_f_joker' ?
        undefined : 'error');
    });

    it('should return joker default face if visible', function (done) {
      var cache = tile.model.get;

      tile.model.get = function () {
        tile.model.get = cache;
        return {
          get: function () {
            return null;
          }
        };
      };

      done(tile.getJokerFace('f_joker', true) === 'f_joker' ?
        undefined : 'error');
    });
  });

  describe('registerDrag()', function () {
    it('should startDrag for view', function (done) {
      var cache = tile.startDrag,
        puzzle = tile.model.get('puzzle');

      puzzle.set('slot_tile', null);
      puzzle.set('mode', null);
      puzzle.set('pending_matches', 0);
      tile.model.isFree = function () {
        return true;
      };
      tile.model.set('state', 2);
      tile.startDrag = function () {
        tile.startDrag = cache;
        tile.model.set('state', 2);
        done();
      };
      tile.registerDrag();
    });

    it('shouldnt startDrag for non free tile', function (done) {
      var cache = tile.startDrag;

      tile.model.get('puzzle').set('slot_tile', null);
      tile.model.isFree = function () {
        return false;
      };
      tile.startDrag = function () {
        done('error');
      };
      tile.registerDrag();
      tile.startDrag = cache;
      done();
    });
  });

  describe('onDragStart()', function () {
    it('should set curr position to stsrtPt', function () {
      tile.style.x = 20;
      tile.style.y = 30;
      tile.style.zIndex = 2;
      tile.onDragStart();
      assert.deepEqual(tile.startPt, {
        x: 20,
        y: 30,
        zIndex: 2
      });
      assert.strictEqual(tile.style.zIndex, 20000);
    });

    it('should set mode to open_slot', function () {
      var puzzle = tile.model.get('puzzle');

      puzzle.set('mode', null);
      tile.onDragStart();
      assert.strictEqual(puzzle.get('mode'), 'open_slot');
    });
  });

  describe('onDrag()', function () {
    it('should update increment x and y with delta', function () {
      tile.style.x = 20;
      tile.style.y = 30;
      tile.style.zIndex = 2;
      tile.onDragStart();

      tile.onDrag({
        srcPt: {
          x: 0,
          y: 0
        }
      }, {
        srcPt: {
          x: 10,
          y: 10
        }
      });

      assert.strictEqual(tile.style.x, 30);
      assert.strictEqual(tile.style.y, 40);
    });
  });

  describe('onDragStop()', function () {
    it('should emit open-slot if drop position is > 50px', function (done) {
      tile.model.get('puzzle').once('open-slot', function () {
        done();
      });
      tile.onDragStop({
        srcPt: {
          x: 0,
          y: 0
        }
      }, {
        srcPt: {
          x: 55,
          y: 10
        }
      });
    });

    it('should not emit open-slot event but invoke pick if drop position' +
      'is > 50px but tile is is locked', function (done) {
      var cache = tile.model.isLock,
        cache_getSuperView = tile.getSuperview;

      tile.model.isLock = function () {
        tile.model.isLock = cache;
        return true;
      };
      tile.getSuperview = function () {
        tile.getSuperview = cache_getSuperView;

        return {
          reflow: function () {}
        };
      };

      tile.style = {};
      tile.startPt = {zIndex: 2};
      tile.model.get('puzzle').once('open-slot', function () {
        done('error');
      });
      tile.onDragStop({
        srcPt: {
          x: 0,
          y: 0
        }
      }, {
        srcPt: {
          x: 55,
          y: 10
        }
      });
      done();
    });

    it('should set mode to null', function () {
      var puzzle = tile.model.get('puzzle');

      puzzle.set('mode', 'open_slot');
      tile.onDragStop({
        srcPt: {
          x: 0,
          y: 0
        }
      }, {
        srcPt: {
          x: 55,
          y: 10
        }
      });

      assert.strictEqual(puzzle.get('mode'), null);
    });

    it('should reset the view to start position if diff < 50', function (done) {
      var cache = tile.getSuperview,
        cache_pick = tile.model.pick;

      tile.model.pick = function () {
        tile.model.pick = cache_pick;
      };
      tile.getSuperview = function () {
        tile.getSuperview = cache;

        return {
          reflow: done
        };
      };

      tile.style = {};
      tile.startPt = {zIndex: 2};
      tile.onDragStop({
        srcPt: {
          x: 0,
          y: 0
        }
      }, {
        srcPt: {
          x: 40,
          y: 10
        }
      });
    });

    it('should call reflow if diff < 50', function (done) {
      var cache = tile.getSuperview;

      tile.getSuperview = function () {
        tile.getSuperview = cache;

        return {
          reflow: function () {
            done();
          }
        };
      };

      tile.style = {};
      tile.startPt = {zIndex: 2};
      tile.onDragStop({
        srcPt: {
          x: 0,
          y: 0
        }
      }, {
        srcPt: {
          x: 40,
          y: 10
        }
      });
    });
  });

  describe('updateJokerFace()', function () {
    it('should call changeFace', function (done) {
      var cache = tile.changeFace;

      tile.changeFace = function () {
        tile.changeFace = cache;
        done();
      };

      tile.updateJokerFace();
    });
  });

  describe('onChangeFaceVisibility()', function () {
    it('should call animate', function (done) {
      var cache = animate,
        anim_obj = {
          now: function () {
            done();
            return this;
          },
          wait: function () {
            return this;
          },
          then: function () {
            return this;
          }
        };

      util_test.removeFromJSIOCache('src/views/tile.js');

      util_test.getFromJSIOCache('src/animate.js').exports = function () {
        animate = cache;
        return anim_obj;
      };
      jsio('import src.views.tile as Tile');
      tile = new Tile();
      tile.model = tile_model;

      tile.onChangeFaceVisibility(true);
    });

    it('should call animate and set opacity 0', function (done) {
      var cache = animate,
        anim_obj = {
          now: function (arg) {
            if (arg.opacity === 0) {
              done();
            }
            return this;
          },
          wait: function () {
            return this;
          },
          then: function () {
            return this;
          }
        };

      util_test.removeFromJSIOCache('src/views/tile.js');

      util_test.getFromJSIOCache('src/animate.js').exports = function () {
        animate = cache;
        return anim_obj;
      };
      jsio('import src.views.tile as Tile');
      tile = new Tile();
      tile.model = tile_model;
      tile.model.set('hidden', true);

      tile.onChangeFaceVisibility(false);
    });
  });

  describe('clean', function () {
    it('should call animate clear', function (done) {
      var cache = animate,
        anim_obj = {
          now: function () {
            done();
            return this;
          },
          wait: function () {
            return this;
          },
          then: function () {
            return this;
          },
          commit: function () {
            return this;
          },
          clear: function () {
            done();
            return this;
          }
        },
        cache_pool;

      util_test.removeFromJSIOCache('src/views/tile.js');

      util_test.getFromJSIOCache('src/animate.js').exports = function () {
        animate = cache;
        return anim_obj;
      };
      jsio('import src.views.tile as Tile');
      tile = new Tile();
      cache_pool = tile.releaseFromPool;
      tile.releaseFromPool = function () {
        tile.releaseFromPool = cache_pool;
      };
      tile.clean();
    });

    it('should invoke removeAllListeners for InputSelect', function (done) {
      var cache = animate,
        anim_obj = {
          now: function () {
            done();
            return this;
          },
          wait: function () {
            return this;
          },
          then: function () {
            return this;
          },
          commit: function () {
            return this;
          },
          clear: function () {
            return this;
          }
        };

      util_test.removeFromJSIOCache('src/views/tile.js');

      util_test.getFromJSIOCache('src/animate.js').exports = function () {
        animate = cache;
        return anim_obj;
      };
      jsio('import src.views.tile as Tile');
      tile = new Tile();

      tile.model = tile_model;
      tile.releaseFromPool = undefined;

      tile.removeAllListeners = function (type) {
        assert.strictEqual(_.contains(['InputStart', 'InputMove'], type),
          true);
        if (type === 'InputMove') {
          done();
        }
      };
      tile.clean();
    });

    it('should release view from Pool', function (done) {
      var cache_tile = tile.releaseFromPool,
        cache = animate,
        anim_obj = {
          now: function () {
            done();
            return this;
          },
          wait: function () {
            return this;
          },
          then: function () {
            return this;
          },
          commit: function () {
            return this;
          },
          clear: function () {
            return this;
          }
        };

      util_test.removeFromJSIOCache('src/views/tile.js');

      util_test.getFromJSIOCache('src/animate.js').exports = function () {
        animate = cache;
        return anim_obj;
      };
      jsio('import src.views.tile as Tile');
      tile = new Tile();

      tile.releaseFromPool = function () {
        tile.releaseFromPool = cache_tile;
        done();
      };
      tile.clean();
    });
  });
});
