/* global Gifting */

jsio('import src.views.gifting_icon as Gifting');

describe('GoalsHUD', function () {
  'use strict';

  var gifting,
    prepare = function () {
      GC.app = {
        view: {
          updateOpts: function () {}
        },
        engine: {
          subscribe: function () {}
        }
      };
    },
    init = function () {
      gifting = new Gifting();
    };

  before(prepare);
  beforeEach(init);

  describe('init()', function () {
    it('should create the gifting icon with opts', function () {
      assert.strictEqual(gifting.style._view._initOpts.image._originalURL,
        'resources/images/map_screen/gift_icon.png');
    });
  });
});
