/* global Jewel, ImageView , HudInfo, PuzzleModel, util_test
*/

jsio('import util.underscore as _');
jsio('import src.models.puzzle as PuzzleModel');
jsio('import DevkitHelper.style as style');
jsio('import test.lib.util as util_test');

describe('JewelsAndGlow', function () {
  'use strict';

  var jewel_info,
    anim_obj,
    hud_info,
    puzzle_model,
    prepare = function () {
      anim_obj = {
        now: function () {
          return this;
        },
        then: function (cb) {
          if (typeof cb === 'function') {
            cb();
          }
          return this;
        }
      };

      util_test.removeFromJSIOCache('src/animate.js');
      jsio('import animate');
      util_test.getFromJSIOCache('src/animate.js').exports = function () {
        return anim_obj;
      };
      jsio('import src.views.jewel as Jewel');
      jsio('import ui.ImageView as ImageView');
      jsio('import src.views.hud_info as HudInfo');
      jewel_info = new Jewel();
      hud_info = new HudInfo();
      puzzle_model = new PuzzleModel();
    },
    afterAll = function () {
      util_test.removeFromJSIOCache('src/animate.js');
    };

  before(prepare);
  after(afterAll);

  describe('build()', function () {
    it('should set jewel image', function (done) {
      jewel_info.build('circle');
      if (jewel_info.jewel.getImage().getURL() ===
        'resources/images/puzzle/circle_inactive.png') {
        done();
      }
    });
    it('should set glow image', function (done) {
      jewel_info.build('circle');
      if (jewel_info.glow.getImage().getURL() ===
        'resources/images/puzzlescreen/glow.png') {
        done();
      }
    });
  });

  describe('init()', function () {
    it('init assigns jewel and glow as instance of imageview', function () {
      assert.strictEqual(true, jewel_info.jewel instanceof
        ImageView);
      assert.strictEqual(true, jewel_info.glow instanceof
        ImageView);
    });
  });

  describe('animateJewels', function () {
    it('should animate the jewels', function () {
      var counter = 0,
        cache = _.clone(anim_obj);

      hud_info.build(puzzle_model);
      hud_info.model.set('jewels', ['circle', 'triangle', 'square']);
      hud_info.model.set('jewel_order', ['circle']);
      hud_info.buildJewels();
      anim_obj.now = function () {
        counter++;
        return this;
      };
      anim_obj.then = function () {
        counter++;
        return this;
      };
      jewel_info.animateJewels(0);
      assert.strictEqual(counter, 4);
      anim_obj = cache;
    });
  });

  describe('changeImage', function () {
    it('should set image as cross', function () {
      jewel_info.changeImage('wrong');
      assert.equal(jewel_info.jewel.getImage().getURL(),
        'resources/images/puzzlescreen/cross.png');
    });
    it('should set image as jewel', function () {
      jewel_info.build('circle');
      jewel_info.changeImage('active');
      assert.equal(jewel_info.jewel.getImage().getURL(),
        'resources/images/puzzle/circle.png');
    });
    it('should set image as jewel_inactive', function () {
      jewel_info.build('circle');
      jewel_info.changeImage();
      assert.equal(jewel_info.jewel.getImage().getURL(),
        'resources/images/puzzle/circle_inactive.png');
    });
  });

  describe('isInactive', function () {
    it('should return true if inactive', function () {
      jewel_info.build('circle');
      jewel_info.isInactive();
      assert.equal(jewel_info.jewel.getImage().getURL()
        .search('_inactive') !== -1, true);
    });
    it('should return false if not inactive', function () {
      jewel_info.build('circle');
      jewel_info.jewel.updateOpts({
        image: 'resources/images/puzzlescreen/circle.png'
      });
      jewel_info.isInactive();
      assert.equal(jewel_info.jewel.getImage().getURL()
        .search('_inactive') !== -1, false);
    });
  });

  describe('wrongAnimate', function () {
    it('should animate when jewel wrongly matched', function () {
      var counter = 0;

      hud_info.build(puzzle_model);
      hud_info.model.set('jewels', ['circle', 'triangle', 'square']);
      hud_info.model.set('jewel_order', ['circle', 'square']);
      hud_info.buildJewels();
      hud_info.changeJewels();
      jewel_info.changeImage = function () {
        counter++;
      };
      jewel_info.wrongAnimate();
      assert.equal(counter, 2);
    });
  });
});
