/* global View, TextView, popup, EventsPopup, ImageView */

/* jshint ignore:start */
jsio('import ui.View as View');
jsio('import ui.TextView as TextView');
jsio('import ui.ImageView as ImageView');
jsio('import DevkitHelper.style as style');
/* jshint ignore:end */

describe('Events popup', function () {
  'use strict';

  var events_popup,
    prepare = function () {
      GC.app = {
        getCurrentView: function () {
          return new View();
        }
      };
      jsio('import quest.modules.popup_manager as popup');
      popup.prepare();
      jsio('import src.views.popups.events as EventsPopup');
    },
    setup_create = function () {
      events_popup = new EventsPopup();
    };

  before(prepare);
  beforeEach(setup_create);

  describe('init()', function () {
    it('should initialize the elements', function () {
      assert.strictEqual(true, events_popup instanceof View);
      assert.strictEqual(true, events_popup.bg_img instanceof ImageView);
      assert.strictEqual(true, events_popup.event_name instanceof TextView);
      assert.strictEqual(true, events_popup.text_is instanceof TextView);
      assert.strictEqual(true, events_popup.text_here instanceof TextView);
      assert.strictEqual(true, events_popup.event_end_info instanceof TextView);
      assert.strictEqual(true, events_popup.event_hurry instanceof TextView);
      assert.strictEqual(true, events_popup.text_end_in instanceof TextView);
    });
  });

  describe('build()', function () {
    it('should call popup.add', function (done) {
      var cache = popup.add,
        img = {getHeight: function () {
          return true;
        },
        getWidth: function () {
          return true;
        },
        doOnLoad: function () {}
        };

      popup.add = function () {
        popup.add = cache;
        done();
      };
      events_popup.build({}, img);
    });

    it('should emit events:play after tapping on play now btn',
      function (done) {
      var cache = popup.add,
        cache_emit = popup.emit,
        img = {getHeight: function () {
          return true;
        },
        getWidth: function () {
          return true;
        },
        doOnLoad: function () {}
        };

      popup.add = function () {
        popup.add = cache;
      };

      popup.emit = function (signal) {
        popup.emit = cache_emit;
        done(signal === 'events:play' ? undefined : 'error');
      };

      events_popup.build({}, img);
      events_popup.play_btn.onInputSelect();
    });
  });

  describe('updateTime()', function () {
    it('should set text for time remining', function (done) {
      var cache = events_popup.text_end_in.setText;

      events_popup.text_end_in.setText = function () {
        events_popup.text_end_in.setText = cache;
        done();
      };

      events_popup.updateTime(100);
    });

    it('should not set text and close popup', function (done) {
      var cache = events_popup.text_end_in.setText,
        cache_close = popup.close;

      events_popup.text_end_in.setText = function () {
        done('error');
      };

      popup.close = function () {
        popup.close = cache_close;
        done();
      };

      events_popup.text_end_in.setText = cache;
      events_popup.updateTime(-1);
    });
  });
});
