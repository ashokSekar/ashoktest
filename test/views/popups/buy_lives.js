/* global popup, BuyLives, event_manager, style, View, BuyLivesQuest
*/

jsio('import ui.View as View');

describe('Buy Lives', function () {
  'use strict';

  var buy_lives,
    config_data = {
      STYLE: {
        orientation: 'portrait',
        bound_width: 640,
        bound_height: 960,
        tablet: 0.8,
        phablet: 0.9
      }
    },
    prepare = function () {
      GC.app = {
        view: {
          updateOpts: function () {}
        },
        getCurrentView: function () {
          return new View();
        },
        user: {
          getMs: function () {}
        }
      };
      jsio('import DevkitHelper.event_manager as event_manager');
      event_manager.plugins = [];
      jsio('import DevkitHelper.style as style');
      style.init(config_data.STYLE);

      jsio('import quest.modules.popup_manager as popup');
      popup.prepare();
      jsio('import src.views.popups.buy_lives as BuyLives');
      jsio('import quest.views.popups.buy_lives as BuyLivesQuest');
    },
    setup_create = function () {
      buy_lives = new BuyLives();
    };

  before(prepare);
  beforeEach(setup_create);

  describe('init()', function () {
    it('should call set button', function (done) {
      var cache = BuyLivesQuest.prototype.setButtonType;

      BuyLivesQuest.prototype.setButtonType = function () {
        BuyLivesQuest.prototype.setButtonType = cache;
        done();
      };
      buy_lives = new BuyLives();
    });
  });

  describe('build()', function () {
    it('should call supr build with opts align bottom', function () {
      var cache = BuyLivesQuest.prototype.build;

      BuyLivesQuest.prototype.build = function (source, opts) {
        BuyLivesQuest.prototype.setButtonType = cache;
        assert.strictEqual('map', source);
        assert.deepEqual({align_bottom: true}, opts);
      };
      buy_lives.build('map');
    });
  });
});
