/* global popup, shuffle, event_manager, style, View, TextView,
 ImageView, util_test, ButtonView, ImageScaleView, inventory_manager,
 dh_timer, User, CACHE:true, history, product_data
*/

jsio('import ui.View as View');
jsio('import ui.TextView as TextView');
jsio('import ui.ImageView as ImageView');
jsio('import ui.ImageScaleView as ImageScaleView');
jsio('import DevkitHelper.history as history');
jsio('import quest.ui.ButtonView as ButtonView');
jsio('import test.lib.util as util_test');

describe('Shuffle Popup', function () {
  'use strict';

  var model, user,
    config_data = {
      STYLE: {
        orientation: 'portrait',
        bound_width: 640,
        bound_height: 960,
        tablet: 0.8,
        phablet: 0.9
      },
      shuffle_content: {}
    },
    prepare = function () {
      GC.app = {
        view: {
          updateOpts: function () {}
        },
        getCurrentView: function () {
          return new View();
        },
        engine: {
          subscribe: function () {}
        },
        user: {
          getMs: function () {}
        }
      };
      jsio('import resources.data.products as product_data');
      product_data.shuffle.cost = 159;
      CACHE = {
        'resources/languages/en.json': JSON.stringify({
          shuffle_buy_count: '$1',
          test_data: 'hello world'
        })
      };

      jsio('import DevkitHelper.event_manager as event_manager');
      jsio('import DevkitHelper.timer as dh_timer');
      jsio('import quest.modules.inventory_manager as inventory_manager');
      event_manager.plugins = [];
      jsio('import DevkitHelper.style as style');
      style.init(config_data.STYLE);
      jsio('import quest.modules.popup_manager as popup');
      popup.prepare();
      jsio('import src.models.puzzle as Model');
      jsio('import quest.models.user as User');
      jsio('import src.views.popups.shuffle as shuffle');
      util_test.removeFromJSIOCache('src/i18n');
      jsio('import DevkitHelper.i18n as i18n');
      model = new Model();
      GC.app.user = user = new User();
    },
    setup_create = function () {
      model.set({
        free_shuffle: 0,
        solvable: true
      });
      shuffle.prepare();
    };

  before(prepare);
  beforeEach(setup_create);
  after(function () {
    util_test.removeFromJSIOCache('resources/data/products.js');
    popup.closeAll();
  });

  describe('prepare()', function () {
    it('should test initialization of shuffle view', function () {
      assert.equal(true, shuffle instanceof View);
      assert.equal(true, shuffle.shuffle instanceof ImageView);
      assert.equal(true, shuffle.shuffle_msg instanceof TextView);
      assert.equal(true, shuffle.button_buy instanceof ImageScaleView);
    });
  });

  describe('build()', function () {
    it('should hide buttons if free_shuffle available', function () {
      shuffle.build(model);
      assert.strictEqual(false, shuffle.button_buy.style.visible);
      assert.strictEqual(false, shuffle.button_give_up.style.visible);
    });

    it('should invoke applyShuffle with 3 sec delay if free_shuffle ' +
      'available', function (done) {
        var cache = dh_timer.timeout;

        dh_timer.timeout = function (val, cb, time) {
          dh_timer.timeout = cache;
          assert.strictEqual(val, 'auto_shuffle');
          assert.strictEqual(time, 1500);
          done();
        };
        shuffle.build(model);
      }
    );

    it('should test invoke of popup manager', function () {
      var cache = popup.add;

      popup.add = function (item) {
        popup.add = cache;
        assert.strictEqual('shuffle', item.id);
        assert.strictEqual('none', item.type);
      };
      model.set('free_shuffle', 1);
      shuffle.build(model);
    });
    it('should test invoke model pausetimers', function (done) {
      var cache = model.pauseTimers;

      model.pauseTimers = function () {
        model.pauseTimers = cache;
        done();
      };
      model.set('free_shuffle', 1);
      shuffle.build(model);
    });
    it('should test prepare getting called', function (done) {
      var cache;

      util_test.removeFromJSIOCache('src/views/popups/shuffle');
      jsio('import src.views.popups.shuffle as shuffle');
      cache = shuffle.prepare;
      shuffle.prepare = function () {
        shuffle.prepare = cache;
        shuffle.prepare();
        done();
      };
      model.set('free_shuffle', 1);
      shuffle.build(model);
    });
    it('should test invoke of history setbusy', function (done) {
      var cache = history.setBusy;

      history.setBusy = function () {
        history.setBusy = cache;
        done();
      };
      model.set('free_shuffle', 1);
      shuffle.build(model);
    });
    it('should hide count if shuffle available', function (done) {
      var inventory_shuffle = GC.app.user.get('inventory_shuffle');

      GC.app.user.set('inventory_shuffle', 2);
      shuffle.buy_count.show = function () {
        done('error');
      };
      shuffle.buy_count.hide = function () {
        GC.app.user.set('inventory_shuffle', inventory_shuffle);
        done();
      };
      model.set('free_shuffle', 1);
      shuffle.build(model);
    });
    it('should hide count if shuffle available', function (done) {
      var inventory_shuffle = GC.app.user.get('inventory_shuffle');

      GC.app.user.set('inventory_shuffle', 0);
      shuffle.buy_count.show = function () {
        GC.app.user.set('inventory_shuffle', inventory_shuffle);
        done();
      };
      shuffle.buy_count.hide = function () {
        done('error');
      };
      model.set('free_shuffle', 1);
      shuffle.build(model);
    });

    it('should call buy if no shuffle', function (done) {
      var inventory_shuffle = GC.app.user.get('inventory_shuffle'),
        cache = shuffle.buy;

      GC.app.user.set('inventory_shuffle', 0);
      shuffle.buy = function () {
        shuffle.buy = cache;
        GC.app.user.set('inventory_shuffle', inventory_shuffle);
        done();
      };
      shuffle.use = function () {
        done('error');
      };
      model.set('free_shuffle', 1);
      shuffle.build(model);
      shuffle.button_buy.onInputSelect();
    });

    it('should call use if shuffle available', function (done) {
      var inventory_shuffle = GC.app.user.get('inventory_shuffle'),
        cache = shuffle.buy;

      GC.app.user.set('inventory_shuffle', 1);
      shuffle.use = function () {
        GC.app.user.set('inventory_shuffle', inventory_shuffle);
        shuffle.buy = cache;
        done();
      };
      shuffle.buy = function () {
        done('error');
      };
      model.set('free_shuffle', 1);
      shuffle.build(model);
      shuffle.button_buy.onInputSelect();
    });
  });

  describe('createButton()', function () {
    it('should test setup of  buttons', function () {
      util_test.removeFromJSIOCache('src/views/popups/shuffle');
      jsio('import src.views.popups.shuffle as shuffle');
      shuffle.build(model);
      assert.equal(ButtonView.states.UP, shuffle.button_buy._state);
      assert.equal(ButtonView.states.UP, shuffle.button_give_up._state);
    });

    it('should set proper cost on buy button', function () {
      shuffle.build(model);
      assert.equal(shuffle.buy_count.getText(), '159');
    });
  });

  describe('buy()', function () {
    it('should test inventory_manager', function (done) {
      var cache = inventory_manager.buy,
        inventory_shuffle = GC.app.user.get('inventory_shuffle');

      GC.app.user.set('inventory_shuffle', 0);
      inventory_manager.buy = function (id) {
        GC.app.user.set('inventory_shuffle', inventory_shuffle);
        assert.equal('shuffle', id);
        inventory_manager.buy = cache;
        done();
      };
      model.set('free_shuffle', 1);
      shuffle.build(model);
      shuffle.button_buy.onInputSelect();
    });
    it('should test inventory_manager resetBusy', function (done) {
      var cache = inventory_manager.buy,
        cache_resetBusy = history.resetBusy;

      inventory_manager.buy = function () {
        inventory_manager.buy = cache;
      };
      history.resetBusy = function () {
        history.resetBusy = cache_resetBusy;
        done();
      };
      model.set('free_shuffle', 1);
      shuffle.build(model);
      shuffle.button_buy.onInputSelect();
    });
  });

  describe('use()', function () {
    it('should call apply suffle', function (done) {
      var cache = shuffle.applyShuffle;

      GC.app.user.set('inventory_shuffle', 1);
      shuffle.applyShuffle = function (success) {
        shuffle.applyShuffle = cache;
        assert.strictEqual(success, true);
        done();
      };
      model.set('free_shuffle', 1);
      shuffle.build(model);
      shuffle.button_buy.onInputSelect();
    });
    it('should ispurchase not be true', function (done) {
      var cache = shuffle.applyShuffle;

      GC.app.user.set('inventory_shuffle', 1);
      shuffle.applyShuffle = function (success, powerup, ispurchase) {
        shuffle.applyShuffle = cache;
        assert.notEqual(ispurchase, true);
        done();
      };
      model.set('free_shuffle', 1);
      shuffle.build(model);
      shuffle.button_buy.onInputSelect();
    });
  });

  describe('closePopup()', function () {
    it('should test game-over emit', function (done) {
      var cache = model.emit;

      model.emit = function (event_name, won) {
        model.emit = cache;
        assert.strictEqual('game-over', event_name);
        assert.strictEqual(false, won);
        done();
      };
      shuffle.build(model);
      shuffle.button_give_up.onInputSelect();
    });
    it('should test resetBusy', function (done) {
      var cache_resetBusy = history.resetBusy;

      history.resetBusy = function () {
        history.resetBusy = cache_resetBusy;
        done();
      };
      shuffle.build(model);
      shuffle.button_give_up.onInputSelect();
    });
  });

  describe('applyShuffle()', function () {
    it('should decrement inventory if transaction success', function (done) {
      var cache = inventory_manager.buy,
        cache_user = user.decrement;

      inventory_manager.buy = function (id, cb) {
        assert.equal('shuffle', id);
        user.increment('inventory_shuffle');
        inventory_manager.buy = cache;
        if (cb) {
          cb(true);
        }
      };
      user.decrement = function (item) {
        assert.equal('inventory_shuffle', item);
        user.decrement = cache_user;
        done();
      };
      model.set('free_shuffle', 1);
      shuffle.build(model);
      shuffle.button_buy.onInputSelect();
    });
    it('should increment shuffle if transaction success', function (done) {
      var cache = inventory_manager.buy,
        cache_m = model.increment,
        count = 1;

      inventory_manager.buy = function (id, cb) {
        assert.equal('shuffle', id);
        user.increment('inventory_shuffle');
        inventory_manager.buy = cache;
        if (cb) {
          cb(true);
        }
      };
      model.increment = function (item) {
        if (count === 2) {
          assert.equal('shuffle', item);
          model.increment = cache_m;
          done();
        }
        count++;
      };
      model.set('free_shuffle', 1);
      shuffle.build(model);
      shuffle.button_buy.onInputSelect();
    });
    it('should test invoke applyShuffle', function (done) {
      var cache = inventory_manager.buy;

      inventory_manager.buy = function (id, cb) {
        assert.equal('shuffle', id);
        inventory_manager.buy = cache;
        if (cb) {
          cb(true);
          done();
        }
      };
      GC.app.user.set('inventory_shuffle', 0);
      model.set('free_shuffle', 1);
      shuffle.build(model);
      shuffle.button_buy.onInputSelect();
    });
    it('should increment ongoing', function () {
      var cache = inventory_manager.buy,
        cache_inc = model.increment;

      inventory_manager.buy = function (id, cb) {
        assert.equal('shuffle', id);
        inventory_manager.buy = cache;
        if (cb) {
          cb(true);
        }
      };
      model.increment = function (param) {
        model.increment = cache_inc;
        assert.strictEqual(param, 'ongoing');
      };

      model.set('free_shuffle', 1);
      shuffle.build(model);
      shuffle.button_buy.onInputSelect();
    });

    it('should add lister to change:ongoing', function (done) {
      var cache_inc = model.increment,
        cache_rt = shuffle.resumeTimer;

      model.increment = function () {
        model.increment = cache_inc;
      };
      shuffle.resumeTimer = function () {
        shuffle.resumeTimer = cache_rt;
        model.removeAllListeners();
        done();
      };

      model.set('free_shuffle', 1);
      shuffle.build(model);
      shuffle.applyShuffle(true, 'shuffle', true);
      model.emit('change:ongoing');
    });
    it('should test transaction success', function (done) {
      var cache = inventory_manager.buy,
        cache_shuffle = shuffle.applyShuffle;

      inventory_manager.buy = function (id, cb) {
        assert.equal('shuffle', id);
        inventory_manager.buy = cache;
        if (cb) {
          cb(true);
        }
      };
      shuffle.applyShuffle = function (success) {
        assert.equal(true, success);
        shuffle.applyShuffle = cache_shuffle;
        done();
      };
      model.set('free_shuffle', 1);
      shuffle.build(model);
      shuffle.button_buy.onInputSelect();
    });
    it('should test popup close on transaction success and purchased false',
      function (done) {
      var cache = inventory_manager.buy,
        cache_close = popup.close;

      inventory_manager.buy = function (id, cb) {
        inventory_manager.buy = cache;
        if (cb) {
          cb(true, 'shuffle', false);
        }
      };
      popup.close = function () {
        popup.close = cache_close;
        done();
      };
      model.set('free_shuffle', 1);
      shuffle.build(model);
      shuffle.button_buy.onInputSelect();
    });
    it('should test history setBusy on trans failure', function (done) {
      var cache = inventory_manager.buy,
        cache_setBusy = history.setBusy;

      inventory_manager.buy = function (id, cb) {
        inventory_manager.buy = cache;
        if (cb) {
          cb(false, 'shuffle', false);
        }
      };
      history.setBusy = function () {
        history.setBusy = cache_setBusy;
        done();
      };
      model.set('free_shuffle', 1);
      shuffle.build(model);
      shuffle.button_buy.onInputSelect();
    });
    it('should test history resetBusy on trans success', function (done) {
      var cache = inventory_manager.buy,
        cache_resetBusy = history.resetBusy;

      inventory_manager.buy = function (id, cb) {
        inventory_manager.buy = cache;
        if (cb) {
          cb(true, 'shuffle', false);
        }
      };
      history.resetBusy = function () {
        history.resetBusy = cache_resetBusy;
        done();
      };
      model.set('free_shuffle', 1);
      shuffle.build(model);
      shuffle.button_buy.onInputSelect();
    });
    it('should test transaction failure', function (done) {
      var cache = inventory_manager.buy;

      inventory_manager.buy = function (id, cb) {
        if (cb) {
          cb(false);
        }
        assert.equal('shuffle', id);
        done();
      };
      GC.app.user.set('inventory_shuffle', 0);
      model.set('free_shuffle', 1);
      shuffle.build(model);
      shuffle.button_buy.onInputSelect();
      inventory_manager.buy = cache;
    });
    it('should test popup close on transaction success and purchased true',
      function (done) {
      var cache = inventory_manager.buy,
        cache_close = popup.close;

      inventory_manager.buy = function (id, cb) {
        inventory_manager.buy = cache;
        if (cb) {
          cb(true, 'shuffle', true);
        }
      };
      model.on('shuffle', function () {});
      model.on('free_shuffle', 1);
      shuffle.build(model);
      popup.close = function () {
        done('error');
      };
      shuffle.button_buy.onInputSelect();
      popup.close = cache_close;
      done();
    });
    it('should close popup if success is undefined',
      function (done) {
      var cache = inventory_manager.buy,
        cache_close = popup.close,
        inventory_shuffle = GC.app.user.get('inventory_shuffle');

      GC.app.user.set('inventory_shuffle', 0);
      inventory_manager.buy = function (id, cb) {
        GC.app.user.set('inventory_shuffle', inventory_shuffle);
        GC.app.user.set('inventory_shuffle',
          GC.app.user.get('inventory_shuffle'));
        inventory_manager.buy = cache;
        if (cb) {
          cb(undefined, 'shuffle', true);
        }
      };
      model.on('shuffle', function () {});
      popup.close = function () {
        popup.close = cache_close;
        done();
      };
      model.set('free_shuffle', 1);
      shuffle.build(model);
      shuffle.button_buy.onInputSelect();
    });
  });

  describe('resumeTimer()', function () {
    it('should resume timer if ongoing ',
      function (done) {
      var cache = shuffle.model.resumeTimers;

      shuffle.model.resumeTimers = function () {
        cache = shuffle.model.resumeTimers;
        done();
      };
      shuffle.resumeTimer(0);
    });
    it('should remove listener if ongoing ',
      function () {
      var cache = shuffle.model.resumeTimers,
        cache_rl = shuffle.model.removeListener;

      shuffle.model.resumeTimers = function () {
        cache = shuffle.model.resumeTimers;
      };
      shuffle.model.removeListener = function (event, func) {
        shuffle.model.removeListener = cache_rl;
        assert.strictEqual(event, 'change:ongoing');
        assert.strictEqual(func, shuffle.resumeTimer);
      };
      shuffle.resumeTimer(0);
    });
    it('should not resume timer if ongoing ',
      function (done) {
      var cache = shuffle.model.resumeTimers;

      shuffle.model.resumeTimers = function () {
        cache = shuffle.model.resumeTimers;
        done('error');
      };
      shuffle.resumeTimer(1);
      done();
    });
  });

  describe('clean()', function () {
    it('should test clear timeout', function () {
      var cache = dh_timer.clear;

      dh_timer.timeout('auto_shuffle', function () {}, 2000);
      dh_timer.clear = function (id) {
        dh_timer.clear = cache;
        assert.strictEqual('auto_shuffle', id);
      };
      shuffle.clean();
      dh_timer.clear('auto_shuffle');
    });
  });
});

