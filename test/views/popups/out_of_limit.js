/* global PuzzleModel, event_manager, ImageView, ImageScaleView,
   style, View, popup, LimitPopup, LimitPopupQuest, TextView, _, util_test
*/

jsio('import ui.View as View');
jsio('import util.underscore as _');

describe('out of limit', function () {
  'use strict';

  var limit_popup,
    puzzle,
    config_data = {
      STYLE: {
        orientation: 'portrait',
        bound_width: 640,
        bound_height: 960,
        tablet: 0.8,
        phablet: 0.9
      }
    },
    prepare = function () {
      GC.app = {
        view: {
          updateOpts: function () {}
        },
        getCurrentView: function () {
          return new View();
        },
        user: {
          getMs: function () {}
        }
      };
      jsio('import test.lib.util as util_test');
      jsio('import DevkitHelper.event_manager as event_manager');
      event_manager.plugins = [];
      jsio('import DevkitHelper.style as style');
      style.init(config_data.STYLE);

      util_test.removeFromJSIOCache('src/i18n');
      util_test.removeFromJSIOCache('src/views/popups/out_of_limit');
      jsio('import ui.ImageScaleView as ImageScaleView');
      jsio('import ui.ImageView as ImageView');
      jsio('import ui.TextView as TextView');
      jsio('import quest.modules.popup_manager as popup');
      popup.prepare();
      jsio('import src.models.puzzle as PuzzleModel');
      jsio('import quest.views.popups.out_of_limit as LimitPopupQuest');
      jsio('import src.views.popups.out_of_limit as LimitPopup');
      jsio('import DevkitHelper.i18n as i18n');
    },
    setup_create = function () {
      puzzle = new PuzzleModel();
      GC.app.user.get = function () {};
      puzzle.initializeData({
        layout: [],
        families: []
      });
      limit_popup = new LimitPopup();
    },
    setup_destroy = function () {
    };

  before(prepare);
  after(function () {
    util_test.removeFromJSIOCache('src/i18n.js');
    util_test.removeFromJSIOCache('src/animate.js');
  });
  beforeEach(setup_create);
  afterEach(setup_destroy);

  describe('init()', function () {
    it('should initialize progress views', function () {
      assert.strictEqual(true, limit_popup.progress_bg instanceof ImageView);
      assert.strictEqual(true,
        limit_popup.progress_bar instanceof ImageScaleView);
      assert.strictEqual(true, limit_popup.progress_text instanceof TextView);
    });
  });

  describe('build()', function () {
    it('should call supr build with opts align bottom true', function () {
      var cache = LimitPopupQuest.prototype.build;

      LimitPopupQuest.prototype.build = function (model, type, opts) {
        LimitPopupQuest.prototype.build = cache;
        assert.deepEqual({align_bottom: true}, opts);
      };

      limit_popup.build(puzzle);
    });

    it('should build progress views', function () {
      limit_popup.build(puzzle);
      assert.strictEqual('resources/images/popup/limit/progress_bg.png',
        limit_popup.progress_bg.getImage().getURL());
      assert.strictEqual('resources/images/popup/limit/progress_bar.png',
        limit_popup.progress_bar.getImage().getURL());
    });

    it('should  set proper progress text', function (done) {
      var cache;

      util_test.removeFromJSIOCache('src/views/popups/out_of_limit.js');
      util_test.removeFromJSIOCache('src/views/popups/out_of_limit.js');
      util_test.removeFromJSIOCache('src/i18n.js');
      jsio('import DevkitHelper.i18n as i18n');
      util_test.getFromJSIOCache('src/i18n.js').exports = function (id, vals) {
        if (id === 'out_of_limit_progress') {
          util_test.getFromJSIOCache('src/i18n.js').exports = cache;
          assert.deepEqual([10, 15], vals);
          done();
        }
      };
      jsio('import src.views.popups.out_of_limit as LimitPopup');

      setup_create();
      puzzle.set({
        solved: [1, 2, 3, 4, 5],
        tile_count: 5
      }, true);

      limit_popup.build(puzzle);
    });

    it('should invoke animateProgress on popup appear', function (done) {
      var cache;

      util_test.removeFromJSIOCache('src/views/popups/out_of_limit.js');
      util_test.removeFromJSIOCache('src/views/popups/out_of_limit.js');
      util_test.removeFromJSIOCache('src/i18n.js');
      jsio('import src.views.popups.out_of_limit as LimitPopup');

      setup_create();

      cache = limit_popup.animateProgress;
      limit_popup.animateProgress = function () {
        limit_popup.animateProgress = cache;
        done();
      };
      limit_popup.build(puzzle);
      popup.emit('out-of-limit:appear');
    });

    it('should  set proper illustration', function () {
      util_test.removeFromJSIOCache('src/views/popups/out_of_limit.js');
      util_test.removeFromJSIOCache('src/views/popups/out_of_limit.js');
      util_test.removeFromJSIOCache('src/i18n.js');
      jsio('import src.views.popups.out_of_limit as LimitPopup');

      setup_create();
      puzzle.set({
        solved: [1, 2, 3, 4, 5],
        tile_count: 5,
        limit_type: 'tap'
      }, true);

      limit_popup.build(puzzle);
      assert.strictEqual('resources/images/popup/limit/tap.png',
        limit_popup.illustration.getImage().getURL());
    });
  });

  describe('animateProgress()', function () {
    it('should animate to target width', function (done) {
      util_test.removeFromJSIOCache('src/views/popups/out_of_limit.js');
      util_test.removeFromJSIOCache('src/views/popups/out_of_limit.js');
      util_test.removeFromJSIOCache('src/animate.js');
      jsio('import animate as animate');
      util_test.getFromJSIOCache('src/animate.js').exports = function () {
        var obj = {
          then: function (opts) {
            if (_.isFunction(opts)) {
              opts();
            } else {
              assert.strictEqual(350, opts.width);
              assert.strictEqual(300, arguments[1]);
            }
            return obj;
          },
          clear: function () {
            done();
          }
        };

        return obj;
      };
      jsio('import src.views.popups.out_of_limit as LimitPopup');

      setup_create();
      limit_popup.animateProgress(350);
    });
  });
});
