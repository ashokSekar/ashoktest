/* global gift_popup, ImageView, style, View, popup */

jsio('import ui.View as View');
jsio('import ui.ImageView as ImageView');

describe('Gifting Popup', function () {
  'use strict';

  before(function () {
    GC.app = {
      view: {
        updateOpts: function () {}
      },
      getCurrentView: function () {
        return new View();
      },
      user: {
        getMs: function () {}
      }
    };
    jsio('import DevkitHelper.style as style');
    style.init({
      orientation: 'portrait',
      bound_width: 640,
      bound_height: 960,
      tablet: 0.8,
      phablet: 0.9
    });

    jsio('import quest.modules.popup_manager as popup');
    jsio('import quest.views.popups.gifting as gift_popup');
  });

  describe('prepare()', function () {
    it('should add image view', function () {
      gift_popup.removeAllSubviews();
      gift_popup.prepare();
      assert.strictEqual(true, gift_popup.img instanceof ImageView);
    });
  });

  describe('build()', function () {
    it('should build image for gift type', function () {
      var cache = popup.add;

      popup.add = function () {
        popup.add = cache;
      };

      gift_popup.build({
        type: 'cash'
      });

      assert.strictEqual('resources/images/popup/gifting/cash.png',
        gift_popup.img.getImage().getURL());
    });
  });
});
