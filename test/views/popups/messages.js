/* global popup, Messages, event_manager, style, View, util_test
*/
jsio('import test.lib.util as util_test');
jsio('import ui.View as View');

describe('Buy Lives', function () {
  'use strict';

  var messages,
    config_data = {
      STYLE: {
        orientation: 'portrait',
        bound_width: 640,
        bound_height: 960,
        tablet: 0.8,
        phablet: 0.9
      }
    },
    prepare = function () {
      GC.app = {
        view: {
          updateOpts: function () {}
        },
        getCurrentView: function () {
          return new View();
        },
        user: {
          getMs: function () {},
          get: function () {}
        }
      };
      util_test.removeFromJSIOCache('src/modules/popup_manager');
      jsio('import DevkitHelper.event_manager as event_manager');
      event_manager.plugins = [];
      jsio('import DevkitHelper.style as style');
      style.init(config_data.STYLE);

      jsio('import quest.modules.popup_manager as popup');
      popup.prepare();
      jsio('import src.views.popups.messages as Messages');
    },
    setup_create = function () {
      messages = new Messages();
    };

  before(prepare);
  beforeEach(setup_create);

  describe('build()', function () {
    it('call build', function () {
      messages.build();
    });
  });
});
