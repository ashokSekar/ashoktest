/* global popup, event_manager, style, View, BonusMap
*/

jsio('import ui.View as View');

describe('Bonus map', function () {
  'use strict';

  var bonus_map,
    config_data = {
      STYLE: {
        orientation: 'portrait',
        bound_width: 640,
        bound_height: 960,
        tablet: 0.8,
        phablet: 0.9
      }
    },
    prepare = function () {
      GC.app = {
        view: {
          updateOpts: function () {}
        },
        getCurrentView: function () {
          return new View();
        },
        user: {
          getMs: function () {}
        }
      };
      jsio('import DevkitHelper.event_manager as event_manager');
      event_manager.plugins = [];
      jsio('import DevkitHelper.style as style');
      style.init(config_data.STYLE);

      jsio('import quest.modules.popup_manager as popup');
      popup.prepare();
      jsio('import src.views.popups.bonus_map as BonusMap');
    },
    setup_create = function () {
      bonus_map = new BonusMap();
    };

  before(prepare);
  beforeEach(setup_create);

  describe('build()', function () {
    it('should build popup with proper args', function () {
      var cache = popup.add;

      popup.add = function (data) {
        popup.add = cache;
        assert.strictEqual(data.id, 'new_variation');
        assert.strictEqual(data.close, true);
        assert.strictEqual(data.type, 'new_variation');
      };
      bonus_map.build('three_match');
    });

    it('should show the image for jewels variation', function () {
      bonus_map.build('jewels');
      assert.strictEqual(bonus_map.variation_img._img.getURL(),
        'resources/images/popup/new_variation/jewels.png');
    });
  });
  describe('clean', function () {
    it('should close the popup on clean', function (done) {
      var cache = popup.close;

      popup.close = function () {
        popup.close = cache;
        done();
      };
      bonus_map.clean();
    });
  });
});
