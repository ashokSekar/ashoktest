/* global View, TextView, popup, EventFinish
*/

/* jshint ignore:start */
jsio('import ui.View as View');
jsio('import ui.TextView as TextView');
jsio('import DevkitHelper.style as style');
/* jshint ignore:end */

describe('Events popup', function () {
  'use strict';

  var events_finish,
    prepare = function () {
      GC.app = {
        getCurrentView: function () {
          return new View();
        }
      };
      jsio('import quest.modules.popup_manager as popup');
      popup.prepare();
      jsio('import src.views.popups.event_finish as EventFinish');
    },
    setup_create = function () {
      events_finish = new EventFinish();
    };

  before(prepare);
  beforeEach(setup_create);

  describe('init()', function () {
    it('should initialize the elements', function () {
      assert.strictEqual(true, events_finish instanceof View);
      assert.strictEqual(true, events_finish.desc instanceof TextView);
    });
  });

  describe('build()', function () {
    it('should call popup.add', function (done) {
      var cache = popup.add;

      popup.add = function () {
        popup.add = cache;
        done();
      };

      events_finish.build({});
    });
  });
});
