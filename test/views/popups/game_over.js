/* global popup, GameOver, event_manager, style, View, TextView,
 ImageView, User, util_test, star_view, social_panel, puzzle_module,
 GameOverQuest, config
*/

jsio('import ui.View as View');
jsio('import ui.TextView as TextView');
jsio('import ui.ImageView as ImageView');
jsio('import util.underscore as _');
jsio('import test.lib.util as util_test');

describe('Game-over Popup', function () {
  'use strict';

  var game_over, model,
    config_data = {
      STYLE: {
        orientation: 'portrait',
        bound_width: 640,
        bound_height: 960,
        tablet: 0.8,
        phablet: 0.9
      }
    },
    prepare = function () {
      GC.app = {
        view: {
          updateOpts: function () {}
        },
        getCurrentView: function () {
          return new View();
        },
        user: {
          getMs: function () {}
        }
      };
      jsio('import DevkitHelper.event_manager as event_manager');
      util_test.removeFromJSIOCache('src/views/star_view');
      jsio('import src.views.star_view as star_view');
      event_manager.plugins = [];
      jsio('import DevkitHelper.style as style');
      style.init(config_data.STYLE);
      jsio('import quest.modules.popup_manager as popup');
      popup.add = function () {};
      popup.next = function () {};
      popup.prepare();
      jsio('import quest.models.user as User');
      jsio('import src.models.puzzle as Model');
      jsio('import src.views.popups.game_over as GameOver');
      jsio('import quest.views.social_panel as social_panel');
      jsio('import quest.modules.puzzle as puzzle_module');
      jsio('import quest.views.popups.game_over as GameOverQuest');
      jsio('import resources.data.config as config');
      config.no_of_motivation_msg = 36;
      social_panel.prepare();
      GC.app.user = new User();
      GC.app.user.set({
        social_facebook: true
      });
    },
    setup_create = function () {
      var cache;

      game_over = new GameOver();
      model = new Model();
      cache = model.setScoreData;
      model.setScoreData = function () {
        model.setScoreData = cache;
        return true;
      };
      model.set('id', 1);
    };

  before(prepare);
  beforeEach(setup_create);

  describe('init()', function () {
    it('should test initialization of gameover view', function () {
      assert.equal(true, game_over instanceof View);
      assert.equal(true, game_over.level_no instanceof TextView);
      assert.equal(true, game_over.high_score instanceof TextView);
      assert.equal(true, game_over.score instanceof View);
      assert.equal(true, game_over.illustration instanceof ImageView);
    });
  });

  describe('build()', function () {
    it('should test illustration if won', function () {
      model.set('stars', 2);
      game_over.build(model, true);
      assert.strictEqual('resources/images/popup/gameover/illustration_win.png',
        game_over.illustration.getImage().getURL());
    });
    it('should not update offset of illustration if hide_social', function () {
      model.set('stars', 2);
      game_over.build(model, true, true);
      assert.strictEqual(0,
        game_over.style.offsetY);
    });
    it('should test illustration if fail', function () {
      model.set('stars', 2);
      game_over.build(model, false);
      assert.strictEqual(
        'resources/images/popup/gameover/illustration_lose.png',
        game_over.illustration.getImage().getURL());
    });
    it('should test of startview', function (done) {
      var cache = star_view.populateStars;

      star_view.populateStars = function () {
        star_view.populateStars = cache;
        done();
      };
      model.set('stars', 2);
      GC.app.user.set('social_facebook', false);
      game_over.build(model, false);
    });
    it('should set text for level_no', function () {
      model.set('ms_no', 2);
      game_over.build(model, false);
      assert.strictEqual(game_over.level_no.getText(), '2');
    });
    it('should test of best score', function (done) {
      var cache = puzzle_module.getScore;

      puzzle_module.getScore = function (id) {
        puzzle_module.getScore = cache;
        assert.strictEqual(1, id);
        done();
        return {
          score: 12
        };
      };
      model.set('id', 1);
      game_over.build(model, true);
    });
    it('should pass proper popup bg if won', function () {
      var cache = popup.add;

      popup.add = function (opts) {
        popup.add = cache;
        assert.strictEqual(opts.popup_bg, 'puzzle_small_rays');
      };
      model.set('id', 1);
      game_over.build(model, true);
    });
    it('should pass proper popup bg if lost', function () {
      var cache = popup.add;

      popup.add = function (opts) {
        popup.add = cache;
        assert.strictEqual(opts.popup_bg, 'puzzle_small');
      };
      model.set('id', 1);
      game_over.build(model, false);
    });
    it('should show motivation message if won', function (done) {
      var cache = game_over.showMotivationMsg;

      model.set('id', 1);
      game_over.build(model, true);
      game_over.showMotivationMsg = function () {
        game_over.showMotivationMsg = cache;
        done();
      };
      game_over.build(model, true);
    });
    it('should not show motivation message if lose', function () {
      model.set('id', 1);
      game_over.build(model, true);
      game_over.showMotivationMsg = function () {
        assert.strictEqual(true, false);
      };
      game_over.build(model, false);
    });
    it('should no_bgm will true if won', function (done) {
      var cache = GameOverQuest.prototype.build;

      model.set('id', 1);
      GameOverQuest.prototype.build = function (source, won, opts) {
        GameOverQuest.prototype.build = cache;
        assert.strictEqual(opts.no_bgm, true);
        done();
      };
      game_over.build(model, true);
    });
    it('should no_bgm will false for bonus levels', function (done) {
      var cache = GameOverQuest.prototype.build;

      model.set('bonus_level', 1);

      GameOverQuest.prototype.build = function (source, won, opts) {
        GameOverQuest.prototype.build = cache;
        assert.strictEqual(opts.no_bgm, false);
        done();
      };
      game_over.build(model, true);
    });
    it('should no_bgm will false if fail', function (done) {
      var cache = GameOverQuest.prototype.build;

      model.set('id', 1);
      GameOverQuest.prototype.build = function (source, won, opts) {
        GameOverQuest.prototype.build = cache;
        assert.strictEqual(opts.no_bgm, false);
        done();
      };
      game_over.build(model, false);
    });

    it('should build for bonus level won', function (done) {
      var cache = GameOverQuest.prototype.build;

      model.set('bonus_level', 1);

      GameOverQuest.prototype.build = function (source, won, opts) {
        GameOverQuest.prototype.build = cache;
        assert.strictEqual(opts.offsetY, -100);
        assert.strictEqual(opts.title, 'gameover_bonus_title');
      };

      game_over.build(model, true);
      assert.strictEqual(false, game_over.level_no.style.visible);
      assert.strictEqual(false, game_over.illustration.style.visible);
      assert.strictEqual(true, game_over.bonus_star.style.visible);
      assert.strictEqual('gameover_won_bonus_info_1', game_over
        .score.getText());
      assert.strictEqual('gameover_won_bonus_info_2', game_over
        .high_score.getText());
      model.set('bonus_level', null);
      done();
    });

    it('should build for bonus level fail', function (done) {
      var cache = GameOverQuest.prototype.build;

      model.set('bonus_level', 1);

      GameOverQuest.prototype.build = function (source, won, opts) {
        GameOverQuest.prototype.build = cache;
        assert.strictEqual(opts.offsetY, -100);
        assert.strictEqual(opts.title, 'gameover_bonus_title');
      };

      game_over.build(model, false);
      assert.strictEqual(false, game_over.level_no.style.visible);
      assert.strictEqual(false, game_over.illustration.style.visible);
      assert.strictEqual(true, game_over.bonus_star.style.visible);
      assert.strictEqual('gameover_fail_bonus_info_1', game_over
        .score.getText());
      assert.strictEqual('gameover_fail_bonus_info_2', game_over
        .high_score.getText());
      model.set('bonus_level', null);
      done();
    });
  });

  describe('clean()', function () {
    it('should test of star_view clean', function (done) {
      var cache = star_view.clean;

      star_view.clean = function () {
        star_view.clean = cache;
        done();
      };
      game_over.clean();
    });
  });

  describe('showMotivationMsg()', function () {
    it('should increment the current motivation_msg_index', function (done) {
      var user = GC.app.user,
        cache = user.set;

      user.set('motivation_msg_index', 0);
      user.set = function (key, val) {
        if (key === 'motivation_msg_index') {
          user.set = cache;
          done(val === 1 ? null : 'error');
        }
      };

      game_over.showMotivationMsg();
    });
    it('should set motivation_msg_index as 0', function (done) {
      var user = GC.app.user,
        cache = user.set;

      user.set('motivation_msg_index', 1000);

      user.set = function (key, val) {
        if (key === 'motivation_msg_index' && val === 0) {
          user.set = cache;
          done();
        }
      };

      game_over.showMotivationMsg();
    });
    it('should call toast build', function (done) {
      var cache = game_over.toast.build;

      game_over.toast.build = function () {
        game_over.toast.build = cache;
        done();
      };

      game_over.showMotivationMsg();
    });

    it('should call toast clean', function (done) {
      var cache = game_over.toast.build,
        clean_cache = game_over.toast.clean;

      game_over.toast.build = function () {
        game_over.toast.build = cache;
      };

      game_over.toast.clean = function (timeout, delay) {
        game_over.toast.clean = clean_cache;
        assert.strictEqual(timeout, 1250);
        assert.strictEqual(delay, 1500);
        done();
      };

      game_over.showMotivationMsg();
    });
  });
});
