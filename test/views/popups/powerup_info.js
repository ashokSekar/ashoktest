/* global popup, powerup_info */

jsio('import ui.View as View');
describe('settings', function () {
  'use strict';

  before(function () {
    jsio('import quest.modules.popup_manager as popup');
    jsio('import DevkitHelper.style as style');
    popup.add = function () {};
    popup.next = function () {};
    popup.prepare();
    jsio('import src.views.popups.powerup_info as powerup_info');
  });

  describe('build()', function () {
    it('should call popup.next on build', function (done) {
      var cache = popup.next;

      popup.next = function () {
        popup.next = cache;
        done();
      };
      powerup_info.build('hint');
    });
    it('should pause the timer on build', function (done) {
      var model = {
        resumeTimers: function () {},
        pauseTimers: function () {
          done();
        }
      };

      powerup_info.build('hint', model);
    });
    it('should resume the timer on popup close', function (done) {
      var model = {
        resumeTimers: function () {
          done();
        },
        pauseTimers: function () {}
      };

      powerup_info.build('hint', model);
      popup.emit('powerup_info:close');
    });
    it('should not pause the timer on build' +
      'if not ingame powerup', function (done) {
      var model = {
        resumeTimers: function () { },
        pauseTimers: function () {
          done('err');
        }
      };

      powerup_info.build('joker', model);
      done();
    });
  });

  describe('clean', function () {
    it('should release steps viewpools', function (done) {
      powerup_info.steps.releaseAllViews = done;
      powerup_info.clean();
    });
  });
});
