/* global style, TextView, PlayPopup, star_view, PuzzleModel, event_manager,
   popup, User, View, localStorage, ViewPool, util, util_test, ImageView */

jsio('import ui.ImageView as ImageView');
jsio('import ui.TextView as TextView');
jsio('import ui.View as View');
jsio('import ui.ViewPool as ViewPool');
jsio('import util.underscore as _');
jsio('import test.lib.util as util_test');

describe('Play Popup', function () {
  'use strict';

  var play_popup, puzzle, cache_get_pow, cache_products,
    cache_unlock, cache_award,
    config_data = {
      STYLE: {
        orientation: 'portrait',
        bound_width: 640,
        bound_height: 960,
        tablet: 0.8,
        phablet: 0.9
      }
    },
    prepare = function () {
      jsio('import quest.models.user as User');
      GC.app = {
        view: {
          updateOpts: function () {}
        },
        getCurrentView: function () {
          return new View();
        },
        user: new User(),
        tutorial: {
          build: function () {},
          isCompleted: function () {}
        }
      };
      jsio('import DevkitHelper.event_manager as event_manager');
      event_manager.plugins = [];
      jsio('import DevkitHelper.style as style');
      style.init(config_data.STYLE);

      jsio('import src.views.star_view as star_view');
      jsio('import quest.modules.popup_manager as popup');
      popup.prepare();
      jsio('import src.models.puzzle as PuzzleModel');
      jsio('import src.views.popups.play as PlayPopup');
      jsio('import quest.modules.util as util');
      util.getSafeArea = function () {
        return {
          top: 44,
          bottom: 34
        };
      };
    },
    setup_create = function () {
      puzzle = new PuzzleModel();
      play_popup = new PlayPopup();

      cache_get_pow = util.getPowerups;
      cache_products = util.getProducts;
      util.getPowerups = function () {
        return {
          pow_a: true,
          pow_b: true
        };
      };

      util.getProducts = function () {
        return {
          pow_a: {
            cost: 200
          },
          pow_b: {
            cost: 200
          }
        };
      };

      cache_unlock = util_test.getFromJSIOCache('resources/data/config')
        .exports.unlockables;
      util_test.getFromJSIOCache('resources/data/config').exports
        .unlockables = {
          pow_a: 2,
          pow_b: 6
        };

      cache_award = util_test.getFromJSIOCache('resources/data/config')
        .exports.award_powerup_count;
      util_test.getFromJSIOCache('resources/data/config').exports
        .award_powerup_count = {
          pow_a: 8,
          pow_b: 5
        };
    },
    setup_destroy = function () {
      util.getPowerups = cache_get_pow;
      util.getProducts = cache_products;
      util_test.getFromJSIOCache('resources/data/config')
        .exports.unlockables = cache_unlock;
      util_test.getFromJSIOCache('resources/data/config')
        .exports.award_powerup_count = cache_award;
    };

  before(prepare);
  beforeEach(setup_create);
  afterEach(setup_destroy);

  describe('init()', function () {
    it('should test initialization of play popup view', function () {
      assert.equal(true, play_popup instanceof View);
      assert.equal(true, play_popup.popup_text instanceof TextView);
      assert.equal(true, play_popup.powerup_container instanceof View);
      assert.equal(true, play_popup.btn_pool instanceof ViewPool);
      assert.equal(true, play_popup.bonus_star instanceof ImageView);
      assert.equal(true, play_popup.bonus_info instanceof TextView);
      play_popup.init();
    });
  });

  describe('build()', function () {
    it('should call loadPowerupTutorial if not bonus map', function (done) {
      var cache = play_popup.loadPowerupTutorial,
        cache_on = popup.on;

      puzzle.bonus_map = false;
      popup.on = function (param, cb) {
        if (param === 'play-popup:appear') {
          cb();
        }
      };
      play_popup.loadPowerupTutorial = function () {
        play_popup.loadPowerupTutorial = cache;
        done();
      };
      play_popup.build(puzzle);
      play_popup.emit('appear');
      puzzle.bonus_map = false;
      popup.on = cache_on;
    });

    it('should not register popup.on listener loadPowerupTutorial' +
      'if bonus map', function (done) {
      var cache_on = popup.on;

      puzzle.bonus_map = true;
      popup.on = function (param) {
        if (param === 'play-popup:appear') {
          done('err');
        }
      };
      play_popup.build(puzzle);
      play_popup.emit('appear');
      puzzle.bonus_map = false;
      popup.on = cache_on;
      done();
    });
    it('should set call popuplate stars', function () {
      var cache = star_view.populateStars,
        cache_get = localStorage.getItem;

      puzzle.id = 2;
      localStorage.getItem = function () {
        return '{"2": {"score": "500", "stars": "3"} }';
      };
      GC.app.user.set('social_facebook', true);
      star_view.populateStars = function (stars) {
        star_view.populateStars = cache;
        assert.strictEqual('3', stars);
      };
      play_popup.build(puzzle);
      localStorage.getItem = cache_get;
    });

    it('should set proper popup text', function () {
      puzzle.time = 180;
      puzzle.stars = 2;
      GC.app.user.set('social_facebook', true);
      play_popup.build(puzzle);
      assert.strictEqual('time', play_popup.popup_text.getText());
    });

    it('should set proper popup text for untimed level', function () {
      var data = _.clone(puzzle);

      data.type = 'untimed';
      data.layout = {
        length: 160
      };
      GC.app.user.set('social_facebook', true);
      play_popup.build(data);
      assert.strictEqual('untimed_play', play_popup.popup_text.getText());
    });

    it('should set proper popup text for tap limit', function () {
      var data = _.clone(puzzle);

      data.tap = 180;
      data.limit_type = 'tap';
      GC.app.user.set('social_facebook', true);
      play_popup.build(data);
      assert.strictEqual('tap', play_popup.popup_text.getText());
    });

    it('should set proper popup text', function () {
      puzzle.time = 190;
      GC.app.user.set('social_facebook', true);
      play_popup.build(puzzle);
      assert.strictEqual('time_secs', play_popup.popup_text.getText());
    });

    it('should send data for nonfb user for social pannel', function (done) {
      var cache = star_view.populateStars;

      GC.app.user.set('social_facebook', false);
      star_view.populateStars = function () {
        star_view.populateStars = cache;
        done();
      };
      play_popup.build(puzzle);
    });

    it('should hide bonus views if not bonus level', function (done) {
      var cache = star_view.populateStars;

      GC.app.user.set('social_facebook', false);
      star_view.populateStars = function () {
        star_view.populateStars = cache;
      };
      play_popup.build(puzzle);

      assert.strictEqual(play_popup.powerup_container.style.visible, true);
      assert.strictEqual(play_popup.bonus_star.style.visible, false);
      assert.strictEqual(play_popup.bonus_info.style.visible, false);
      done();
    });

    it('should render bonus views and hide powerupcontainer', function (done) {
      var data = _.clone(puzzle);

      data.type = 'memory';
      data.bonus_level = 1;
      play_popup.build(data);

      assert.strictEqual(play_popup.bonus_star.style.visible, true);
      assert.strictEqual(play_popup.bonus_info.style.visible, true);
      done();
    });

    it('should add pregame powerups to container', function (done) {
      play_popup.btn_pool.releaseAllViews();

      play_popup.build(puzzle);

      done(play_popup.powerup_container.getSubviews().length === 2 ?
        undefined : 'error');
    });
  });

  describe('loadPowerupTutorial()', function () {
    var cache_current_view;

    beforeEach(function () {
      cache_current_view = popup.getCurrentView;
      popup.getCurrentView = function () {
        return {foot: {
          getSubviews: function () {
            return [];
          }
        }};
      };
    });

    afterEach(function () {
      popup.getCurrentView = function () {
        return {foot: {
          getSubviews: function () {
            return [];
          }
        }};
      };
    });
    it('should build tutorial for powerup', function (done) {
      var app = GC.app,
        user = app.user,
        tut = app.tutorial,
        cache = tut.build;

      user.set('curr_ms', 2);
      user.set('max_ms', 2);
      tut.isCompleted = function () {
        return false;
      };
      play_popup.build(puzzle);

      tut.build = function (data) {
        tut.build = cache;
        assert.strictEqual(true, _.has(data.positions, 'enable_pow_a'));
        assert.strictEqual(true, _.has(data.positions, 'enable_pow_a_play'));
        done();
      };
      play_popup.loadPowerupTutorial();
    });

    it('should build for old powerup', function (done) {
      var app = GC.app,
        user = app.user,
        tut = app.tutorial,
        cache = tut.build;

      user.set('curr_ms', 5);
      user.set('max_ms', 5);
      tut.isCompleted = function () {
        return false;
      };
      play_popup.build(puzzle);

      user.set('unlocked_powerups', ['pow_b']);

      tut.build = function (data) {
        tut.build = cache;
        assert.strictEqual(true, _.has(data.positions, 'enable_pow_a'));
        assert.strictEqual(true, _.has(data.positions, 'enable_pow_a_play'));
        done();
      };
      play_popup.loadPowerupTutorial();
    });

    it('should not build for old powerup', function (done) {
      var app = GC.app,
        user = app.user,
        tut = app.tutorial,
        cache = tut.build;

      user.set('curr_ms', 1);
      user.set('max_ms', 1);
      tut.isCompleted = function () {
        return false;
      };
      play_popup.build(puzzle);

      user.set('unlocked_powerups', ['pow_b']);

      tut.build = function () {
        done('error');
      };
      play_popup.loadPowerupTutorial();
      done();
      tut.build = cache;
    });

    it('should not build tutorial for powerup', function (done) {
      var app = GC.app,
        user = app.user,
        tut = app.tutorial,
        cache = tut.build;

      user.set('curr_ms', 2);
      user.set('max_ms', 4);
      user.set('unlocked_powerups', ['pow_a']);
      tut.isCompleted = function () {
        return false;
      };
      play_popup.build(puzzle);

      tut.build = function () {
        done('error');
      };
      play_popup.loadPowerupTutorial();
      tut.build = cache;
      done();
    });

    it('should build powerup info if available', function (done) {
      var app = GC.app,
        user = app.user,
        tut = app.tutorial,
        cache = tut.build;

      user.set('curr_ms', 6);
      user.set('max_ms', 6);
      user.set('unlocked_powerups', ['pow_a']);
      user.set('last_powerup_info', [0, '']);
      tut.isCompleted = function () {
        return true;
      };
      play_popup.build(puzzle);

      tut.build = function (opts) {
        if (opts.positions.powerup_info) {
          done();
        }
      };
      play_popup.loadPowerupTutorial();
      tut.build = cache;
    });

    it('should not build powerup info if not available', function (done) {
      var app = GC.app,
        user = app.user,
        tut = app.tutorial,
        cache = tut.build;

      user.set('curr_ms', 6);
      user.set('max_ms', 6);
      user.set('unlocked_powerups', ['pow_a']);
      user.set('last_powerup_info', [6, '']);
      tut.isCompleted = function () {
        return true;
      };
      play_popup.build(puzzle);

      tut.build = function (opts) {
        if (opts.positions.powerup_info) {
          done('error');
        }
      };
      play_popup.loadPowerupTutorial();
      tut.build = cache;
      done();
    });

    it('should add free powerup to inventory', function (done) {
      var app = GC.app,
        user = app.user,
        tut = app.tutorial,
        cache = user.addFreeGoods;

      user.set('curr_ms', 6);
      user.set('max_ms', 6);
      tut.isCompleted = function () {
        return false;
      };
      play_popup.build(puzzle);

      user.addFreeGoods = function (pow, count, type) {
        user.addFreeGoods = cache;
        assert.strictEqual(pow, 'pow_b');
        assert.strictEqual(count, 5);
        assert.strictEqual(type, 'first_time_bonus');
        done();
      };

      play_popup.loadPowerupTutorial();
    });

    it('shouldnt build if tutorial already shown', function (done) {
      var tut = GC.app.tutorial,
        cache = tut.build,
        cache_comp = tut.isCompleted;

      GC.app.user.set('curr_ms', 2);
      tut.build = done;

      tut.isCompleted = function () {
        tut.isCompleted = cache_comp;
        return true;
      };

      play_popup.build(puzzle);
      play_popup.loadPowerupTutorial();

      tut.build = cache;
      done();
    });
  });

  describe('clean', function () {
    it('should call star_view clean', function (done) {
      var cache = star_view.clean;

      star_view.clean = function () {
        star_view.clean = cache;
        done();
      };
      play_popup.clean(puzzle);
    });
  });
});
