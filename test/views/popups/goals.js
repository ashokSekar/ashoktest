/* global View, popup, GoalsPopup, dh_timer,
  util_test, ViewPool*/

/* jshint ignore:start */
jsio('import ui.View as View');
jsio('import ui.ViewPool as ViewPool');
jsio('import ui.TextView as TextView');
jsio('import ui.ImageView as ImageView');
jsio('import DevkitHelper.style as style');
jsio('import DevkitHelper.timer as dh_timer');
jsio('import test.lib.util as util_test');

/* jshint ignore:end */

describe('Goals popup', function () {
  'use strict';

  var goals_popup,
    prepare = function () {
      GC.app = {
        getCurrentView: function () {
          return new View();
        }
      };
      jsio('import quest.modules.popup_manager as popup');
      popup.prepare();
      jsio('import src.views.popups.goals as GoalsPopup');
    },
    setup_create = function () {
      goals_popup = new GoalsPopup();
    };

  before(prepare);
  beforeEach(setup_create);

  describe('init()', function () {
    it('should initialize the elements', function () {
      assert.strictEqual(true, goals_popup instanceof View);
      assert.strictEqual(true, goals_popup.goals_pool instanceof ViewPool);
    });
  });

  describe('build()', function () {
    it('should call popup.add', function (done) {
      var cache = popup.add;

      popup.add = function () {
        popup.add = cache;
        done();
      };
      util_test.getFromJSIOCache('resources/data/config').exports
        .variations = {
          classic: {
            goals: ['clear']
          },
          jewels: {
            goals: ['clear', 'jewels']
          }
        };
      goals_popup.build('jewels');
    });

    it('should add goal for classic mahjong', function (done) {
      var cache = popup.close;

      util_test.getFromJSIOCache('resources/data/config').exports
        .variations = {
          classic: {
            goals: ['clear']
          },
          jewels: {
            goals: ['clear', 'jewels']
          }
        };
      popup.add = function () {
        popup.add = cache;
        done();
      };
      goals_popup.build();
    });

    it('should close the popup after timer ends', function (done) {
      var cache = popup.close,
        cache_timeout = dh_timer.timeout,
        cache_add = popup.add;

      util_test.getFromJSIOCache('resources/data/config').exports
        .variations = {
          classic: {
            goals: ['clear']
          },
          jewels: {
            goals: ['clear', 'jewels']
          }
        };
      popup.add = function () {
        popup.add = cache_add;
      };
      popup.close = function () {
        popup.close = cache;
        done();
      };

      dh_timer.timeout = function (params, cb) {
        dh_timer.timeout = cache_timeout;
        cb();
      };

      goals_popup.build('jewels');
    });
  });

  describe('clean', function () {
    it('should relase all views on clean', function () {
      var count = 0;

      goals_popup.removeAllSubviews = function () {
        count++;
      };
      goals_popup.goals_pool.releaseAllViews = function () {
        count++;
      };
      goals_popup.clean();
      assert.strictEqual(count, 2);
    });
  });
});
