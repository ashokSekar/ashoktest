/* global View, QuestOvertake, Overtake*/

describe('Overtake Popup', function () {
  'use strict';

  var overtake_popup;

  jsio('import ui.View as View');
  jsio('import quest.views.popups.overtake as QuestOvertake');
  jsio('import src.views.popups.overtake as Overtake');

  before(function () {
    overtake_popup = new Overtake();
    overtake_popup.share_btn = new View();
  });

  describe('build()', function () {
    it('should build popup for score', function (done) {
      var cache = QuestOvertake.prototype.build;

      QuestOvertake.prototype.build = function (opts) {
        QuestOvertake.prototype.build = cache;
        assert.strictEqual(opts.popup_opts.popup_bg, 'small_red');
        assert.strictEqual(opts.popup_opts.title, null);
        done();
      };

      overtake_popup.build({type: 'score'});
    });

    it('should build popup for ms', function (done) {
      var cache = QuestOvertake.prototype.build;

      QuestOvertake.prototype.build = function (opts) {
        QuestOvertake.prototype.build = cache;
        assert.strictEqual(opts.popup_opts.popup_bg, 'small');
        assert.strictEqual(opts.popup_opts.title, 'overtake_title');
        done();
      };

      overtake_popup.build({type: 'ms'});
    });
  });
});
