/* global event_manager, ButtonView, settings, util,
   style, View, popup, util_test, User, timer, TextView
*/

jsio('import test.lib.util as util_test');
jsio('import ui.View as View');
jsio('import ui.TextView as TextView');
jsio('import quest.models.user as User');
jsio('import quest.ui.ButtonView as ButtonView');
jsio('import timer');
jsio('import quest.modules.util as util');

describe('settings', function () {
  'use strict';

  var config_data = {
      STYLE: {
        orientation: 'portrait',
        bound_width: 640,
        bound_height: 960,
        tablet: 0.8,
        phablet: 0.9
      }
    },
    prepare = function () {
      GC.app = {
        user: new User(),
        view: {
          updateOpts: function () {}
        },
        getCurrentView: function () {
          return new View();
        }
      };
      jsio('import DevkitHelper.event_manager as event_manager');
      event_manager.plugins = [];
      jsio('import DevkitHelper.style as style');
      style.init(config_data.STYLE);

      util_test.removeFromJSIOCache('src/modules/util');

      jsio('import quest.modules.util as util');
      jsio('import quest.modules.popup_manager as popup');
      popup.add = function () {};
      popup.next = function () {};
      popup.prepare();
      jsio('import quest.views.popups.settings as settings');
    },
    setup_create = function () {
      settings.prepare();
    },
    setup_destroy = function () {
      settings.clean();
      util_test.removeFromJSIOCache('settings');
      util_test.removeFromJSIOCache('src/modules/util');
      timer.tick(400);
    };

  before(prepare);
  beforeEach(setup_create);
  afterEach(setup_destroy);

  describe('prepare()', function () {
    it('should create buy btn container', function () {
      assert.strictEqual(true, settings.helper_view instanceof View);
    });

    it('should create level_no view', function () {
      assert.strictEqual(true, settings.level_no instanceof TextView);
    });

    it('should create 7 btn views through viewpool', function () {
      assert.strictEqual(7, settings.buttons_pool._views.length);
    });
  });

  describe('build()', function () {
    it('should set level visible true if from puzzle', function () {
      settings.build({
        origin: 'puzzle'
      });
      assert.strictEqual(settings.level_no._opts.visible, true);
    });

    it('should hide buttons for web', function () {
      var cache = util.getDeviceInfo,
        cache_set = settings.setButtonElements;

      util.getDeviceInfo = function () {
        util.getDeviceInfo = cache;
        return 'web';
      };
      settings.setButtonElements = function (params) {
        settings.setButtonElements = cache_set;
        assert.deepEqual(params.puzzle, ['resume', 'feedback']);
      };
      settings.build({
        origin: 'puzzle'
      });
    });

    it('should update level no if given in opts', function () {
      var cache = settings.level_no.updateOpts;

      settings.level_no.updateOpts = function (opts) {
        settings.level_no.updateOpts = cache;
        assert.strictEqual(opts.text, 5);
      };
      settings.build({
        origin: 'puzzle',
        level_no: 5
      });
    });

    it('shouldnt set level visible true if from bonus puzzle', function () {
      settings.build({
        origin: 'puzzle',
        bonus_level: true
      });
      assert.strictEqual(settings.level_no._opts.visible, false);
    });

    it('should set level visible false if from map', function () {
      settings.build({
        origin: 'map'
      });
      assert.strictEqual(settings.level_no._opts.visible, false);
    });

    it('should call set button elements', function (done) {
      var cache = settings.setButtonElements;

      settings.setButtonElements = function () {
        settings.setButtonElements = cache;
        done();
      };

      settings.build({
        origin: 'puzzle'
      });
    });

    it('should init feedback button on map', function () {
      var feedback_btn;

      settings.build({
        origin: 'map'
      });
      feedback_btn = settings.helper_view.getSubviews()[1];
      assert
        .strictEqual('resources/images/popup/settings/btn_mapfeedback_up.png',
        feedback_btn.getImage().getURL());
      assert.strictEqual(feedback_btn._state,
        ButtonView.states.UP);
    });

    it('should init help button on map', function () {
      var help_btn;

      settings.build({
        origin: 'map'
      });
      help_btn = settings.helper_view.getSubviews()[0];
      assert.strictEqual('resources/images/popup/settings/btn_maphelp_up.png',
        help_btn.getImage().getURL());
      assert.strictEqual(help_btn._state,
        ButtonView.states.UP);
    });

    it('should init map button on puzzle', function () {
      var map_btn;

      settings.build({
        origin: 'puzzle'
      });
      map_btn = settings.helper_view.getSubviews()[0];
      assert
        .strictEqual('resources/images/popup/settings/btn_map_up.png',
        map_btn.getImage().getURL());
      assert.strictEqual(map_btn._state,
        ButtonView.states.UP);
    });

    it('should init restart button on puzzle', function () {
      var restart_btn;

      settings.build({
        origin: 'puzzle'
      });
      restart_btn = settings.helper_view.getSubviews()[1];
      assert
        .strictEqual('resources/images/popup/settings/btn_restart_up.png',
        restart_btn.getImage().getURL());
      assert.strictEqual(restart_btn._state,
        ButtonView.states.UP);
    });

    it('should set callback for map feedback button', function (done) {
      var feedback_btn;

      util_test.callTest(done, settings, 'onMapfeedback');
      settings.build({
        origin: 'map'
      });
      feedback_btn = settings.helper_view.getSubviews()[1];
      feedback_btn.onInputSelect();
    });
  });

  describe('onMaphelp', function () {
    it('should call settings help', function (done) {
      var cache = settings.onHelp;

      settings.onHelp = function () {
        settings.onHelp = cache;
        done();
      };
      settings.onMaphelp();
    });
  });

  describe('onMapfeedback', function () {
    it('should return true', function () {
      assert.strictEqual(true, settings.onMapfeedback());
    });
  });

  describe('clean', function () {
    it('should clean helper view ', function () {
      settings.build({
        origin: 'map'
      });
      settings.clean();
      assert.strictEqual(0, settings.helper_view.getSubviews().length);
    });
  });
});
