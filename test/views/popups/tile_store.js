/* global User, popup, View, ButtonView, inventory_manager,
  style, ImageView, TileStore, TileTab
*/

jsio('import ui.View as View');
jsio('import ui.ImageView as ImageView');
jsio('import ui.resource.Image as ImageResource');

describe('TileStore Popup', function () {
  'use strict';

  var popup_view,
    prepare = function () {
      jsio('import src.models.user as User');

      GC.app = {
        user: new User(),
        getCurrentView: function () {
          return new View();
        }
      };

      jsio('import quest.modules.popup_manager as popup');
      jsio('import src.views.popups.tile_store as TileStore');
      jsio('import src.views.tile_tab as TileTab');
      jsio('import quest.ui.ButtonView as ButtonView');
      jsio('import DevkitHelper.style as style');
      jsio('import quest.modules.inventory_manager as inventory_manager');

      style.get = function () {
        return {};
      };
    };

  before(prepare);
  beforeEach(function () {
    popup_view = new TileStore();
  });

  describe('init()', function () {
    it('should add subviews', function () {
      assert.strictEqual(true, popup_view.tile_tab instanceof TileTab);
      assert.strictEqual(true, popup_view.tile_set_display instanceof
        ImageView);
      assert.strictEqual(true, popup_view.buy_btn instanceof ButtonView);
    });
  });

  describe('build()', function () {
    it('should build popup', function (done) {
      var cache = popup.add;

      GC.app.user.get = function () {};

      popup.add = function (opts) {
        popup.add = cache;
        assert.strictEqual(true, opts.close);
        done();
      };
      popup_view.build({});
    });
  });

  describe('onSelectTile()', function () {
    it('should change curr tile set if already purchased', function (done) {
      var user = GC.app.user,
        cache_get = user.get,
        cache_set = user.set;

      GC.app.user.get = function (key) {
        if (key === 'curr_tile_set') {
          return 'winter';
        } else {
          return ['classic', 'winter'];
        }
      };

      GC.app.user.set = function (key, val) {
        assert.strictEqual(key, 'curr_tile_set');
        done(val === 'winter' ? undefined : 'error');
      };

      popup_view.updateView('tile1');
      popup_view.onSelectTile();
      user.get = cache_get;
      user.set = cache_set;
    });

    it('should call inventory manager buy', function (done) {
      var buy_cache = inventory_manager.buy;

      GC.app.user.get = function (key) {
        if (key === 'curr_tile_set') {
          return 'winter';
        } else {
          return ['classic'];
        }
      };

      inventory_manager.buy = function () {
        inventory_manager.buy = buy_cache;
        done();
      };
      popup_view.updateView('tile1');
      popup_view.onSelectTile();
    });
  });

  describe('onBuy()', function () {
    it('should set item as curr tile set if success', function (done) {
      var i = 0;

      GC.app.user.get = function () {
        return [];
      };

      GC.app.user.push = function () {
        i++;
      };

      GC.app.user.set = function (key, val) {
        if (key === 'curr_tile_set' && val === 'winter') {
          i++;
        }
      };
      popup_view.updateView('tile1');

      GC.app.user.save = function () {
        done(i === 2 ? undefined : 'error');
      };
      popup_view.onBuy(true, 'classic', true);
    });

    it('should close popup if purchased with existing coins', function (done) {
      var cache = popup.close;

      GC.app.user.get = function () {
        return [];
      };

      GC.app.user.push = function () {};

      GC.app.user.set = function () {};
      popup_view.updateView('tile1');

      GC.app.user.save = function () {};

      popup.close = function () {
        popup.close = cache;
        done();
      };
      popup_view.onBuy(true, 'classic', false);
    });

    it('should not item as curr tile set if failure', function (done) {
      GC.app.user.get = function () {
        return [];
      };

      GC.app.user.push = function () {
        done('error');
      };

      popup_view.onBuy(false);
      done();
    });
  });

  describe('updateView()', function () {
    it('should set passed tile set img as display img', function (done) {
      popup_view.updateView('tile0');
      assert.strictEqual('resources/images/popup/tile_store/classic.png',
        popup_view.tile_set_display.getImage().getURL());

      done();
    });

    it('should set btn as buy if not purchsed', function (done) {
      GC.app.user.get = function () {
        return ['classic'];
      };
      popup_view.updateView('tile1');
      assert.strictEqual('tile_set_buy',
        popup_view.buy_btn.getText().getText());
      done();
    });

    it('should set btn as use if alrady purchases', function (done) {
      GC.app.user.get = function () {
        return ['classic', 'winter'];
      };
      popup_view.updateView('tile1');
      assert.strictEqual('tile_set_use',
        popup_view.buy_btn.getText().getText());
      done();
    });

    it('should hide btn if using current btn', function (done) {
      GC.app.user.get = function (key) {
        if (key === 'tile_sets') {
          return ['classic'];
        }
        return 'classic';
      };
      popup_view.updateView('tile0');
      assert.strictEqual(0, popup_view.buy_btn.style.opacity);
      done();
    });
  });

  describe('clean()', function () {
    it('should remove tile-set-select listener', function (done) {
      var cache = popup_view.tile_tab.removeListener;

      popup_view.tile_tab.removeListener = function (event) {
        popup_view.tile_tab.removeListener = cache;
        done(event === 'tile-set-select' ? undefined : 'error');
      };

      popup_view.clean();
    });
  });
});
