/* global View, User, TextView, popup, util_test,
  history, retry_puzzle_load */

/* jshint ignore:start */
jsio('import test.lib.util as util_test');
jsio('import quest.models.user as User');
jsio('import ui.TextView as TextView');
jsio('import ui.View as View');
jsio('import DevkitHelper.history as history');
/* jshint ignore:end */

describe('RetryPuzzleLoad popup', function () {
  'use strict';

  var prepare = function () {
      util_test.removeFromJSIOCache('src/ui/popup_view');
      util_test.removeFromJSIOCache('src/modules/popup_manager');
      jsio('import resources.styles.portrait as portrait');
      jsio('import quest.modules.popup_manager as popup');
      jsio('import DevkitHelper.style as style');
      jsio('import src.views.popups.retry_puzzle_load as retry_puzzle_load');

      popup.prepare();

      GC.app = {
        getCurrentView: function () {
          return new View();
        },
        user: new User()
      };
    },
    clean = function () {
      popup.close();
    };

  before(prepare);
  afterEach(clean);

  describe('prepare()', function () {
    it('should initialize the elements', function () {
      retry_puzzle_load.prepare();
      assert.strictEqual(
        true,
        retry_puzzle_load instanceof View
      );
      assert.strictEqual(true,
        retry_puzzle_load.retry_content instanceof TextView
      );
      assert.strictEqual(true,
        retry_puzzle_load.retry_content_small instanceof TextView
      );
    });
  });

  describe('build()', function () {
    it('should call popup.add', function (done) {
      var cache = popup.add,
        opts = {
          type: 'shuffle'
        };

      popup.add = function () {
        popup.add = cache;
        done();
      };
      retry_puzzle_load.build(opts);
    });

    it('should invoke prepare if not initialised', function (done) {
      var cache = retry_puzzle_load.prepare,
        cache_popup = popup.add;

      retry_puzzle_load.removeAllSubviews();
      retry_puzzle_load.prepare = function () {
        retry_puzzle_load.prepare = cache;
        done();
      };
      popup.add = function () {};
      retry_puzzle_load.build();
      popup.add = cache_popup;
    });

    it('shouldnd invoke prepare if already initialised', function (done) {
      var cache = retry_puzzle_load.prepare,
        cache_popup = popup.add;

      retry_puzzle_load.removeAllSubviews();
      popup.add = function () {};
      retry_puzzle_load.build();
      retry_puzzle_load.prepare = function () {
        done('error');
      };
      retry_puzzle_load.build();
      retry_puzzle_load.prepare = cache;
      popup.add = cache_popup;
      done();
    });

    it('close button should hide', function (done) {
      var cache = popup.add,
        opts = {
          type: 'shuffle'
        };

      popup.add = function (prop) {
        popup.add = cache;
        assert.strictEqual(prop.close, false);
        done();
      };
      retry_puzzle_load.build(opts);
    });

    it('should call history.setBusy', function (done) {
      var cache = popup.add,
        opts = {
          type: 'gameload'
        },
        cache_history = history.setBusy;

      popup.add = function (prop) {
        assert.strictEqual(prop.close, false);
      };
      history.setBusy = function () {
        history.setBusy = cache_history;
        done();
      };
      retry_puzzle_load.build(opts);
      popup.add = cache;
    });

    it('should call retry_content_small.setText', function (done) {
      var cache = retry_puzzle_load.retry_content_small.setText,
        add_cache = popup.add,
        prepare_cache;

      retry_puzzle_load.prepare();
      prepare_cache = retry_puzzle_load.prepare;
      retry_puzzle_load.prepare = function () {};
      popup.add = function () {};
      retry_puzzle_load.retry_content_small.setText = function () {
        retry_puzzle_load.retry_content_small.setText = cache;
        done();
      };
      retry_puzzle_load.build('gameload');
      popup.add = add_cache;
      retry_puzzle_load.prepare = prepare_cache;
    });

    it('should call retry_content.updateOpts', function (done) {
      var cache = retry_puzzle_load.retry_content.updateOpts,
        add_cache = popup.add,
        prepare_cache;

      retry_puzzle_load.prepare();
      prepare_cache = retry_puzzle_load.prepare;
      retry_puzzle_load.prepare = function () {};
      popup.add = function () {};
      retry_puzzle_load.retry_content.updateOpts = function () {
        retry_puzzle_load.retry_content.updateOpts = cache;
        done();
      };
      retry_puzzle_load.build('shuffle');
      popup.add = add_cache;
      retry_puzzle_load.prepare = prepare_cache;
    });

    it('should show home button if shuffle count ' +
      'is more', function (done) {
      var cache = retry_puzzle_load.btn_home.show,
        add_cache = popup.add;

      retry_puzzle_load.btn_home.show = function () {
        retry_puzzle_load.btn_home.show = cache;
        done();
      };
      popup.add = function () {};

      retry_puzzle_load.build('shuffle', 5);
      popup.add = add_cache;
    });
  });

  describe('retry()', function () {
    it('should call history.setbusy', function (done) {
      var cache = history.resetBusy;

      history.resetBusy = function () {
        history.resetBusy = cache;
        done();
      };
      retry_puzzle_load.retry();
    });

    it('should call popup.close', function (done) {
      var cache = popup.close;

      popup.close = function () {
        popup.close = cache;
        done();
      };
      retry_puzzle_load.retry();
    });

    it('should call popup.close', function (done) {
      var cache = popup.close;

      popup.close = function () {
        popup.close = cache;
        done();
      };
      retry_puzzle_load.retry();
    });
  });

  describe('home()', function () {
    it('should call popup.close', function (done) {
      var cache = popup.close;

      popup.close = function () {
        popup.close = cache;
        done();
      };
      retry_puzzle_load.home();
    });
  });
});
