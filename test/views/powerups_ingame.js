/* globals IngamePowerup, Puzzle, User, PowerupView, util*/

describe('IngamePowerup', function () {
    'use strict';

    var ingame_powerup, cache_unlocked, cache_get_product, user, puzzle;

    before(function () {
      jsio('import src.views.powerup_ingame as IngamePowerup');
      jsio('import src.models.puzzle as Puzzle');
      jsio('import DevkitHelper.style as style');
      jsio('import quest.models.user as User');
      jsio('import quest.modules.util as util');

      user = GC.app.user = new User();
      cache_unlocked = user.isUnlocked;
      user.isUnlocked = function () {
        return true;
      };
      cache_get_product = util.getProducts;
      util.getProducts = function () {
        var product = {
            shuffle: {
              cost: 50
            },
            hint: {
              cost: 100
            }
          };

        return product;
      };
    });

    after(function () {
      util.getProducts = cache_get_product;
    });

    beforeEach(function () {
      ingame_powerup = new IngamePowerup();
      puzzle = new Puzzle();
    });

    describe('build()', function () {
      it('should listen to change of free_shuffle in case' +
        'of shuffle powerup', function (done) {
        var cache = puzzle.on;

        puzzle.on = function (param) {
          puzzle.on = cache;
          assert.strictEqual(param, 'change:free_shuffle');
          done();
        };
        ingame_powerup.build('shuffle', puzzle);
      });
    });
    describe('setCount()', function () {
      it('should increase the count in case of shuffle powerup ', function () {
        var powerup_view, cache, cache_util, cache_get;

        jsio('import quest.views.powerup as PowerupView');
        jsio('import quest.modules.util as util');
        powerup_view = new PowerupView();
        cache = powerup_view.setCount;
        cache_util = util.getProducts;
        cache_get = puzzle.get;
        user.set('inventory_shuffle', 3);
        puzzle.get = function (param) {
          puzzle.get = cache_get;
          if (param === 'free_shuffle') {
            return 2;
          }
        };
        powerup_view.setCount = function (count) {
          powerup_view.setCount = cache;
          assert.strictEqual(4, count);
        };
        ingame_powerup.name = 'shuffle';
        ingame_powerup.build('shuffle', puzzle);
        ingame_powerup.setCount();
      });
    });
    describe('onRelease()', function () {
      it('should remove listeners for free_shuffle', function (done) {
        var cache,
          cache_user = user.removeListener,
          count = 0;

        ingame_powerup.build('shuffle', puzzle);
        cache = puzzle.removeListener;
        puzzle.removeListener = function (val) {
          puzzle.removeListener = cache;
          assert.strictEqual('change:free_shuffle', val);
        };
        user.removeListener = function (val) {
          user.removeListener = cache_user;
          assert.strictEqual('change:inventory_shuffle', val);
          ++count;
        };
        ingame_powerup.onRelease();
        if (count === 1) {
          done();
        }
      });
      it('should not remove listeners for powerups' +
        'other than shuffle', function (done) {
        var cache;

        ingame_powerup.build('hint', puzzle);
        cache = puzzle.removeListener;
        puzzle.removeListener = function () {
          puzzle.removeListener = cache;
          done('error');
        };
        ingame_powerup.onRelease();
        done();
      });
    });
  });
