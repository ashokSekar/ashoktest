/* global _, util_test, require, __dirname, global */
var path = require('path');

jsio('import util.underscore as _');
jsio('import test.lib.util as util_test');

beforeFile(function () {
  'use strict';

  var path_clean = [path.join(__dirname, '../src/'),
    path.join(__dirname, '../resources/'),
    path.join(__dirname, '../modules/')],
    path_exclude = [path.join(__dirname, '../modules/devkithelper/'),
      path.join(__dirname, '../modules/devkit-core/'),
      path.join(__dirname, '../modules/devkit-effects/'),
      path.join(__dirname, '../modules/facebook/')
      ],
    modules = _.keys(global.jsio.__modules);

  _.each(modules, function (file) {
    if (_.find(path_clean, function (prefix) {
      return file.startsWith(prefix);
    }) && !_.find(path_exclude, function (prefix) {
      return file.startsWith(prefix);
    })) {
      util_test.removeFromJSIOCache(file);
    }
  });
});
