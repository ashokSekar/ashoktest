'use strict';

var _ = require('lodash'),
  logger = require('../modules/debug')('ERROR'),
  test = require('../test/lib/private');

module.exports = {

  create: function (request, response) {
    var rules = {
      user: {type: 'string', canbe_null: true},
      device: {
        type: {type: 'string'},
        advertising_id: {type: 'string'},
        name: {type: 'string'},
        language: {type: 'string'},
        os: {type: 'string'},
        is_tablet: {type: 'bool'},
        install_date: {type: 'date'},
        appversion: {type: 'string'},
        store: {type: 'string'},
        jail_broken: {type: 'bool'}
      },
      errors: {
        is_array: true,
        timestamp: {type: 'date'},
        message: {type: 'string', required: true},
        url: {type: 'string'},
        line: {type: 'int'},
        col: {type: 'int'},
        count: {type: 'int'},
        last_occured: {type: 'date'}
      }
    },
    req_body, errors;

    test.prepare(this, {
      rules: rules
    });

    if (request.validate(rules)) {
      req_body = request.body;
      errors = req_body.errors;

      _.each(errors, function (error) {
        logger.error('DEVICE JS error:  %s, error: %s',
          JSON.stringify(req_body.device), JSON.stringify(error));
      });

      response.success(true);
    }
  }
};
