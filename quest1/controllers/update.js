'use strict';

var config = require('../resources/config'),
  fs = require('fs-extra-promise'),
  Class = require('js-class'),
  compareVersion = require('compare-version'),
  util = require('../modules/util'),
  user = require('../models/user'),
  Base = Class({ // jshint ignore:line

    get: function (request, res) {
      var rule = {
          platform: {
            required: true,
            type: 'string'
          },
          version: {
            required: true,
            type: 'string'
          }
        },
        validated = request.validate(null, null, rule),
        platform, latest_version, req_version,
        resp, info, updates;

      if (validated) {
        fs.readFileAsync(process.cwd() + '/' + config.update_available_path)
          .then(function (update_text) {
            updates = JSON.parse(update_text);
            platform = request.query.platform;
            req_version = request.query.version;
            latest_version = updates.latest[platform];

            if (compareVersion(latest_version, req_version) === 1 &&
              updates.info[platform][latest_version])
            {
              info = updates.info[platform][latest_version];
              resp = {
                data: {
                  message: info.message,
                  force_update: info.force_update
                }
              };
            }

            user.onUpdateAvailable(resp, request.query);
            res.success(resp);
          })
          .catch (function (err) {
            res.error(err);
          });
      }
    }
  });

module.exports = new
  (Class(Base, util.getGamePath('controllers/update')))();// jshint ignore:line
