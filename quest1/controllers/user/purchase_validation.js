'use strict';

var purchase_validator = require('../../modules/purchase_validator');

module.exports = {
  create: function (req, resp) {
    var rules = {
        user_id: {type: 'string', required: true},
        store: {type: 'string', required: true},
        purchase_details: {type: 'object', required: true}
      };

    if (req.validate(rules)) {
      purchase_validator.verifyPurchase(req.body)
        .then(function(status) {
          resp.success({
            status: status
          });
        })
        .catch(resp.error);
    }
  }
};
