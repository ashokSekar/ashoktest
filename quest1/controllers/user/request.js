'use strict';

var user = require('../../models/user');

module.exports = {
  create: function (req, resp) {
    var rules = {
        request: {type: 'string', required: true},
        to: {is_array: true, type: 'string', required: true},
        fb_action: {type: 'string'},
        sender: {type: 'string', required: true},
        type: {type: 'string', required: true}
      };

    if (req.validate(rules)) {
      user.addRequests(req.body)
        .then(resp.success, resp.error);
    }
  },

  update: function (req, res) {
    var rules = {
        request_id: {type: 'string', required: true},
        receiver: {type: 'string', required: true},
        type: {type: 'string', required: true},
        fb_action: {type: 'string'}
      };

    if (req.validate(rules)) {
      user.updateRequest(req.body)
        .then(res.success, res.error);
    }
  }
};
