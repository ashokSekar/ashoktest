'use strict';

var _ = require('lodash'),
  user = require('../../models/user'),
  test = require('../../test/lib/private'),
  request_ip = require('request-ip'),
  config = require('../../resources/config'),
  powerups = config.powerups,
  sync_rules = require('../../resources/sync_rules'),
  fs = require('fs');

module.exports = {
  create: function (request, resp) {
    var rules = sync_rules,
      isProductionIOS = function (request) {
        var device = request.body.device;

        if (device.name.indexOf('iPod') === 0) {
          device.type = 'IPod';
        }

        return device.type.indexOf('IP') === 0 &&
          request.headers['user-agent']
          .indexOf(config.game.package_name) !== -1;
      },
      isProductionAndroid = function (request) {
        var device = request.body.device;

        return device.type.indexOf('Android') === 0 ||
          device.type.indexOf('Amazon') === 0;
      },
      isProductionWeb = function (request) {
        var device = request.body.device;

        return device.type.indexOf('browser') === 0;
      };

    test.prepare(this, {
      rules: rules,
      isProductionIOS: isProductionIOS,
      isProductionAndroid: isProductionAndroid,
      isProductionWeb: isProductionWeb
    });

    if (!isProductionIOS(request) && !isProductionAndroid(request) &&
      !isProductionWeb(request)) {
      fs.appendFile(config.game.discard_data,
        '\n' + JSON.stringify(request.body), function () {});

      resp.error({
        type: 'Non Production'
      });

      return;
    }

    if (request.validate(rules, {user_id: {required: true, type: 'string'}})) {
      user.syncData(request.body, request_ip.getClientIp(request))
        .then(function (data) {
          if (data !== true && data.user) {
            data.user = _.pick(data.user, [
              'uid',
              'max_ms',
              'master_score',
              'paid_currency',
              'free_currency',
              'unlocked_powerups'
            ], _.concat(powerups.pregame, powerups.ingame,
              config.user_resp_prop));

            data = _.pick(data, [
              'friend_scores',
              'bonus_levels_played',
              'friend_positions',
              'score',
              'date',
              'sale',
              'cross_promotion',
              'rewards',
              'puzzle_patch',
              'requests',
              'user'
            ], config.sync_resp_prop);
          }

          return data;
        })
        .then(resp.success, resp.error);
    }
  }
};
