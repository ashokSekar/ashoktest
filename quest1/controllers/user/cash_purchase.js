'use strict';

var _ = require('lodash'),
  user = require('../../models/user'),
  sync_rules = require('../../resources/sync_rules');

module.exports = {
  create: function (req, resp) {
    var purchase_rules = _.clone(sync_rules.user.purchases),
      rules = {
        uid: {type: 'string', required: true},
        store: {type: 'string', required: true},
        purchase_details: _.merge(purchase_rules, {
          type: 'object',
          required: true,
          is_array: false
        })
      };

    if (req.validate(rules)) {
      user.verifyAndSavePurchase(req.body)
        .then(resp.success)
        .catch(resp.error);
    }
  }
};
