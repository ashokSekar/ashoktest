'use strict';

var kochava = require('../modules/kochava'),
  test = require('../test/lib/private');

module.exports = {
  index: function (request, response) {
    var query_rules = {
        kochava_id: {type: 'string', required: true},
        event_name: {type: 'string'},
        event_data: {type: 'string'},
        event_value: {type: 'string'},
        event_timestamp: {type: 'string'},
        device_ip: {type: 'string'},
        device_ua: {type: 'string'},
        idfa: {type: 'string'},
        adid: {type: 'string'},
        android_id: {type: 'string'},
        mac: {type: 'string'},
        imei: {type: 'string'},
        odin: {type: 'string'},
        network_name: {type: 'string'},
        campaign_id: {type: 'string'},
        campaign_name: {type: 'string'},
        tracker_name: {type: 'string'},
        tracker_id: {type: 'string'},
        site_id: {type: 'string'},
        creative_id: {type: 'string'},
        attribution_time: {type: 'string'},
        app_limit_track: {type: 'string'},
        device_limit_track: {type: 'string'},
        matched_by: {type: 'string'},
        click_id: {type: 'string'}
      };

    test.prepare(this, {
      rules: query_rules
    });

    if (request.validate(null, null, query_rules)) {
      kochava.insertEvent(request.query)
        .then(response.success, response.error);
    }
  }
};
