'use strict';

var bluebird = require('bluebird'),
  Class = require('js-class'),
  util = require('../modules/util'),
  db = require('../models/db/db'),
  Base = Class({ // jshint ignore:line
    get: function (request, res) {
      var rule = {
          puzzle_id: {
            required: true,
            type: 'int'
          }
        },
        validated = request.validate(null, rule),
        id = request.params.puzzle_id;

      if (!validated) {
        return bluebird.resolve();
      }
      return db.getPuzzle(id)
        .then(function (resp) {
          res.success(resp);
        })
        .catch(function (err) {
          res.error(err);
        });
    }
  });

module.exports = new
  (Class(Base, util.getGamePath('controllers/puzzle')))();// jshint ignore:line
