'use strict';

var fs = require('fs'),
  _ = require('lodash'),
  util = require('./util'),
  path = require('path'),
  mail_template = fs.readFileSync(path.join(__dirname,
    './templates/apperrors.html'), 'utf8'),
  error_template = fs.readFileSync(path.join(__dirname,
    './templates/error_template.html'), 'utf8'),
  detail_template = fs.readFileSync(path.join(__dirname,
    './templates/detail_template.html'), 'utf8'),
  detail_list = fs.readFileSync(path.join(__dirname,
    './templates/detail_list.html'), 'utf8'),
  config = require('../resources/config'),
  log_files = config.log_file.app,
  mailer = require('./mailer'),
  getErrorData = function (log_errors) {
    var i, curr_type,
      err_reg = /GMT\s(\S+)\sError/gi,
      arranged = {};

    for (i = 1; i < log_errors.length; i+=2) {
      curr_type = log_errors[i].split(err_reg)[1];
      if (!arranged[curr_type]) {
        arranged[curr_type] = [];
      }
      arranged[curr_type].push(log_errors[i+1]);
    }

    return arranged;
  },
  html, detailed, err_list, data, reg, opts;


_.each(log_files, function (log_file, idx) {
  opts = {
    subject: (idx === 0 ? 'App' : 'Worker') + ' Error log for: ' +
      config.game.app_name
  };
  html = '';
  detailed = '';
  reg = /(.+\d{4}\s\d{2}:\d{2}:\d{2}.+Error)/gi;
  data = fs.readFileSync(log_file, 'utf8').split(reg);
  data = getErrorData(data);
  _.each(data, function (errs, type) {
    err_list = '';
    html = html + util.render(error_template, {
      error: type,
      count: errs.length
    }, true);

    _.each(errs, function (curr) {
      err_list = err_list + util.render(detail_list, {
        info: curr
      }, true);
    });

    detailed = detailed + util.render(detail_template, {
      type: type,
      err_list: err_list
    }, false);
  });

  if (html !== '') {
    opts.html =  util.render(mail_template, {
      errors: html,
      app: config.game.app_name,
      detailed_errors: detailed
    }, false);
  } else {
    opts.text = 'No Errors!!';
  }

  mailer.send(opts);
});
