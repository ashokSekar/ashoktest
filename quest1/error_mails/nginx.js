'use strict';

var fs = require('fs'),
  path = require('path'),
  _ = require('lodash'),
  mail_template = fs.readFileSync(path.join(__dirname,
    './templates/nginxerrors.html'), 'utf8'),
  error_template = fs.readFileSync(path.join(__dirname,
    './templates/nginx_template.html'), 'utf8'),
  mailer = require('./mailer'),
  util = require('./util'),
  config = require('../resources/config'),
  log_file = config.log_file.nginx,
  data = fs.readFileSync(log_file, 'utf8').split('\n'),
  parsed_error = {},
  html = '',
  opts = {
    subject: 'Nginx Error for: ' + config.game.app_name
  },
  match_arr, reg_exp, curr_err, req_url, curr_obj;

data.splice(-1, 1);

_.each(data, function (curr_log) {
  reg_exp = /([0-9\/]+) ([0-9:]+) \[([a-z]+)\] (.+), client.*request: "\S+ \/(\S+) .*\/(\S+) .*host: "([a-z."]*)+/gi;
  match_arr = reg_exp.exec(curr_log);
  curr_err = match_arr[4];
  req_url = match_arr[5];

  if (!parsed_error[curr_err]) {
    parsed_error[curr_err] = {
      count: 0,
      urls: []
    };
  }

  curr_obj = parsed_error[curr_err];
  ++curr_obj.count;
  curr_obj.urls.push(req_url);
});

_.each(parsed_error, function (obj, err) {
  html = html + util.render(error_template, {
    error: err,
    urls: obj.urls.toString(),
    count: obj.count
  }, true);
});

if (html) {
  opts.html = util.render(mail_template, {
    errors: html,
    app: config.game.app_name
  }, false);
} else {
  opts.text = 'No Nginx Error logs';
}

mailer.send(opts);
