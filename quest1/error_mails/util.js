'use strict';

module.exports = {
  render: function (str, context, esc_html) {
    var re = /\{\{[ ]*([^{}\s]+)[ ]*\}\}/g;

    str = (str || '').toString();
    context = context || {};

    return str.replace(re, function (match, key) {
      var value;
      if (context.hasOwnProperty(key)) {
        value = context[key].toString();
        if (esc_html) {
          value = value.replace(/["'&<>]/g, function (char) {
            switch (char) {
              case '&':
                return '&amp;';
              case '<':
                return '&lt;';
              case '>':
                return '&gt;';
              case '"':
                return '&quot;';
              case '\'':
                return '&#039;';
              default:
                return char;
            }
          });
        }
        return value;
      }
      return match;
    });
  }
};
