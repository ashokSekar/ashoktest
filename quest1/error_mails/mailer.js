'use strict';

var nodemailer = require('nodemailer'),
  _ = require('lodash'),
  mail_config = require('../resources/config').email,
  transporter = nodemailer.createTransport();

module.exports = {
  send: function (opts) {
    transporter.sendMail(_.merge({
      from: mail_config.from,
      to: mail_config.to
    }, opts), function (err, info) {
      if (err) {
        console.log('Error: ', err);
      }
    });
  }
};
