'use strict';
var fs = require('fs'),
  path = require('path'),
  exec = require('child_process').exec,
  _ = require('lodash');

module.exports = function (grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    githooks: {
      all: {
        'pre-commit': 'jshint:all jscs cover'
      }
    },
    jshint: {
      options: {
        reporter: require('jshint-stylish'),
        jshintrc: './.jshintrc'
      },
      all: [
        '*.js',
        'resources/**/*.js',
        'lib/**/*.js',
        'routes/**/*.js',
        'models/**/*.js',
        'modules/**/*.js',
        'test/**/*.js'
      ]
    },
    mochaTest: {
      test: {
        src: ['test/**/*.js']
      },
      options: {
        reporter: 'spec',
        require: 'test/mocha-setup.js'
      }
    },
    jscs: {
      src: [
        '*.js',
        'resources/**/*.js',
        'lib/**/*.js',
        'routes/**/*.js',
        'models/**/*.js',
        'test/**/*.js'
      ],
      options: {
        config: '.jscsrc'
      }
    },
    supervisor: {
      target: {
        script: 'server.js'
      }
    },
    mocha_istanbul: {
      cover: {
        src: 'test/**/*.js',
        options: {
          coverage: true,
          require: 'test/mocha-setup.js',
          reportFormats: ['lcov', 'text']
        }
      }
    },
    availabletasks: {
      tasks: {
        options: {
          sort: ['lint', 'tasks'],
          filter: 'exclude',
          tasks: [
            'any-newer',
            'newer',
            'newer-clean',
            'newer-postrun',
            'availabletasks',
            'jshint',
            'cover'
          ],
          descriptions: {
            tasks: 'List all availble tasks',
            lint: 'Use JS Hint to test JavaScript code efficiancy',
            cover: 'Run coverage on tests',
            iscov: 'check if your code is covered or not'
          }
        }
      }
    }
  });

  grunt.event.on('coverage', function (lcov, done) {
    var base = path.basename(fs.realpathSync('.')),
      append_str = new Array(base.length).join(' '),
      rep_str, matches;

    lcov = fs.readFileSync('./coverage/result').toString()
      .replace(/\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?[m|K]/gi, '');
    matches = lcov.match(new RegExp(base + '[A-z, 0-9, /]*', 'g'));

    _.each(matches, function (str) {
      rep_str = str.replace(base, '.') + append_str;
      lcov = lcov.replace(str, rep_str);
    });
    fs.writeFileSync('./coverage/result', lcov);
    exec('git add coverage/result');
    done();
  });

  grunt.loadNpmTasks('grunt-githooks');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-available-tasks');
  grunt.loadNpmTasks('grunt-mocha-test');
  grunt.loadNpmTasks('grunt-newer');
  grunt.loadNpmTasks('grunt-jscs');
  grunt.loadNpmTasks('grunt-supervisor');
  grunt.loadNpmTasks('grunt-mocha-istanbul');

  grunt.registerTask('lint', ['jshint:all']);
  grunt.registerTask('test', ['mochaTest']);
  grunt.registerTask('cover', ['mocha_istanbul:cover']);
  grunt.registerTask('tasks', ['availabletasks']);
};
