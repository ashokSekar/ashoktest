var _ = require('lodash'),
  util = require('../modules/util');

module.exports = _.merge({
  user: {
    uid: {required: true, type: 'string'},
    fb_uid: {type: 'string'},
    fb_connect_time: {type: 'date', canbe_null: true},
    prev_fb_uid: {type: 'string', canbe_null: true},
    fb_disconnect_time: {type: 'date', canbe_null: true},
    gender: {type: 'string', canbe_null: true},
    first_name: {type: 'string', canbe_null: true},
    last_name: {type: 'string', canbe_null: true},
    dob: {type: 'date', canbe_null: true},
    location: {type: 'string', canbe_null: true},
    language: {type: 'string'},
    start_time: {type: 'date'},
    source: {type: 'string'},
    deauth_time: {type: 'date'},
    currency_paid: {type: 'int'},
    currency_free: {type: 'int'},
    curr_ms: {type: 'int'},
    max_ms: {type: 'int'},
    score: {type: 'int'},
    inventory_lives: {type: 'int'},
    ps_avg_session_length: {type: 'float'},
    ps_churn_prob: {type: 'float'},
    ps_since_last_played: {type: 'float'},
    ps_high_spender_prob: {type: 'float'},
    ps_no_of_purchases: {type: 'float'},
    ps_no_of_sessions: {type: 'float'},
    ps_session_percentile: {type: 'float'},
    ps_spend_percentile: {type: 'float'},
    ps_spend_probability: {type: 'float'},
    ps_total_spend_next28: {type: 'float'},
    game_sounds: {
      music: {type: 'bool'},
      effect: {type: 'bool'}
    },
    paid: {type: 'float'},
    purchases: {
      is_array: true,
      item: {type: 'string', required: true},
      time: {type: 'date'},
      quantity: {type: 'int'},
      currency_used: {
        free: {type: 'int'},
        paid: {type: 'int'}
      },
      currency_remaining: {type: 'int'},
      curr_ms: {type: 'int'},
      max_ms: {type: 'int'},
      instance_id: {type: 'int'}
    },
    track_free_currency: {
      //TODO: Remove later. Only for backward compatability,
      //can be removed after we no longer have users below v0.40
      is_array: true,
      time: {type: 'date', required: true},
      amount: {type: 'int', required: true},
      source: {type: 'string'},
      curr_ms: {type: 'int'}
    },
    track_free_goods: {
      is_array: true,
      time: {type: 'date', required: true},
      item: {type: 'string', required: true},
      amount: {type: 'int', required: true},
      source: {type: 'string'},
      curr_ms: {type: 'int'},
      instance_id: {type: 'int', canbe_null: true}
    },
    credits: {
      is_array: true,
      curr_ms: {type: 'int'},
      time: {type: 'date'},
      item: {type: 'string', required: true},
      usd_amount: {type: 'float'},
      amount: {type: 'float'},
      currency: {type: 'string'},
      receipt: {type: 'string'},
      source: {type: 'string'},
      max_ms: {type: 'int'}
    }
  },
  notifications: {
    is_array: true,
    notification_id: {type: 'string'},
    title: {type: 'string'},
    body: {type: 'string'},
    additional_data: {type: 'string'},
    clicked_time: {type: 'date'}
  },
  device: {
    type: {type: 'string'},
    advertising_id: {type: 'string', required: true},
    name: {type: 'string'},
    language: {type: 'string'},
    os: {type: 'string'},
    is_tablet: {type: 'bool'},
    install_date: {type: 'date'},
    design_version: {type: 'string'},
    appversion: {type: 'string'},
    store: {type: 'string'},
    jail_broken: {type: 'bool'}
  },
  puzzle: {
    is_array: true,
    id: {type: 'string', required: true},
    timestamp: {type: 'date'},
    time: {type: 'int'},
    score: {type: 'int', required: true},
    stars: {type: 'int', max: 3},
    bonus_level: {type: 'int'},
    progress: {type: 'int', max: 100},
    count_clear: {type: 'int'},
    app_version: {type: 'string'},
    design_version: {type: 'string'},
    instance_id: {type: 'int'},
    exit_type: {type: 'string'}
  }
}, util.getGamePath('resources/sync_rules'));
