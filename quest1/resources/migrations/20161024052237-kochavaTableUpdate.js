'use strict';

var Bluebird = require('bluebird'),
  alterColumn = function (db, col, spec) {
    return new Bluebird(function (resolve, reject) {
      db.changeColumn('kochava_events', col, spec, function (err) {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  };

exports.up = function(db, callback) {
  alterColumn(db, 'campaign_id', {
    type: 'string',
    length: 80
  })
  .then(alterColumn.bind(null, db, 'creative_id', {
    type: 'string',
    length: 80
  }))
  .then(alterColumn.bind(null, db, 'site_id', {
    type: 'string',
    length: 80
  }))
  .then(alterColumn.bind(null, db, 'tracker_id', {
    type: 'string',
    length: 80
  }))
  .finally(callback);
};

exports.down = function(db, callback) {
  alterColumn(db, 'campaign_id', {
    type: 'string',
    length: 40
  })
  .then(alterColumn.bind(null, db, 'creative_id', {
    type: 'string',
    length: 40
  }))
  .then(alterColumn.bind(null, db, 'site_id', {
    type: 'string',
    length: 40
  }))
  .then(alterColumn.bind(null, db, 'tracker_id', {
    type: 'string',
    length: 40
  }))
  .finally(callback);
};
