exports.up = function (db, callback) {
  'use strict';

  db.addColumn('user_device', 'rating', {
    type: 'tinyint',
    notNull: false,
    unsigned: true,
    defaultValue: 0
  }, callback);
};

exports.down = function (db, callback) {
  'use strict';

  db.removeColumn('user_device', 'rating', callback);
};
