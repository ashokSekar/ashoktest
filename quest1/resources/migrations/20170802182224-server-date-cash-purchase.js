'use strict';

exports.up = function(db, callback) {
  db.addColumn('cash_purchase', 'added_on', {
      type: 'datetime'
    }, callback);
};

exports.down = function(db, callback) {
  db.removeColumn('cash_purchase', 'added_on', callback);
};
