'use strict';

exports.up = function(db, callback) {
  db.createTable('kochava_events', {
    kochava_id: {type: 'string', primaryKey: true, length: 64},
    event_name: {type: 'string', primaryKey: false, length: 30},
    event_data: {type: 'string', primaryKey: false, length: 30},
    event_value: {type: 'string', primaryKey: false, length: 30},
    event_timestamp: {type: 'string', primaryKey: true, length: 15},
    device_ip: {type: 'string', primaryKey: false, length: 20},
    device_ua: {type: 'string', primaryKey: false, length: 80},
    idfa: {type: 'string', primaryKey: false, length: 64},
    adid: {type: 'string', primaryKey: false, length: 64},
    android_id: {type: 'string', primaryKey: false, length: 64},
    mac: {type: 'string', primaryKey: false, length: 64},
    imei: {type: 'string', primaryKey: false, length: 64},
    odin: {type: 'string', primaryKey: false, length: 64},
    network_name: {type: 'string', primaryKey: false, length: 40},
    campaign_id: {type: 'string', primaryKey: false, length: 40},
    campaign_name: {type: 'string', primaryKey: false, length: 40},
    tracker_name: {type: 'string', primaryKey: false, length: 40},
    tracker_id: {type: 'string', primaryKey: false, length: 40},
    site_id: {type: 'string', primaryKey: false, length: 40},
    creative_id: {type: 'string', primaryKey: false, length: 40},
    attribution_time: {type: 'string', primaryKey: false, length: 40},
    app_limit_track: {type: 'string', primaryKey: false, length: 40},
    device_limit_track: {type: 'string', primaryKey: false, length: 40},
    matched_by: {type: 'string', primaryKey: false, length: 40},
    click_id: {type: 'string', primaryKey: false, length: 40}
  }, callback);
};

exports.down = function(db, callback) {
  db.dropTable('kochava_events', callback);
};
