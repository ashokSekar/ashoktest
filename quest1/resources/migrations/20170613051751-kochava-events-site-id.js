'use strict';

exports.up = function(db, done) {
  db.changeColumn('kochava_events', 'site_id', {
      type: 'text'
    }, function (err) {
      done(err);
    }
  );
};

exports.down = function(db, done) {
  db.changeColumn('kochava_events', 'site_id', {
      type: 'string',
      length: 40
    }, function (err) {
      done(err);
    }
  );
};
