'use strict';

exports.up = function(db, callback) {
  db.changeColumn('track_free_goods', 'source', {
    type: 'string',
    length: 30,
    notNull: true
  }, callback);
};

exports.down = function(db, callback) {
  db.changeColumn('track_free_goods', 'source', {
    type: 'string',
    length: 16,
    notNull: true
  }, callback);
};
