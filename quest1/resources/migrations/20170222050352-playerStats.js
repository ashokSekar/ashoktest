'use strict';

var _ = require('lodash'),
  columns = [
    'avg_session_length',
    'churn_probability',
    'days_since_last_played',
    'high_spender_probability',
    'no_of_purchases',
    'no_of_sessions',
    'session_percentile',
    'spend_percentile',
    'spend_probability',
    'total_spend_next_28_days'
  ];

exports.up = function(db, callback) {
  var int_cols = [
      'days_since_last_played',
      'no_of_purchases',
      'no_of_sessions'
    ],
    getOpts = function (col_name) {
      var is_int = int_cols.indexOf(col_name) > -1,
        opts = {
          type: is_int ? 'mediumint' : 'float',
          notNull: true,
          defaultValue: -1,
          unsigned: false
        };

      if (!is_int) {
        opts.length = '7, 4';
      }

      return opts;
    };

  callback = _.after(columns.length, callback);

  _.each(columns, function (curr_col) {
    db.addColumn('user_device', curr_col, getOpts(curr_col), callback);
  });
};

exports.down = function(db, callback) {
  callback = _.after(columns.length, callback);

  _.each(columns, function (curr_col) {
    db.removeColumn('user_device', curr_col, callback);
  });
};
