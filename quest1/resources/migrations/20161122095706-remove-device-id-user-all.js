exports.up = function (db, callback) {
  'use strict';

  db.removeColumn('user_gs_stats_all', 'device_id', callback);
};

exports.down = function (db, callback) {
  'use strict';

  db.addColumn('user_gs_stats_all', 'device_id', {
    type: 'string',
    notNull: true,
    length: 36
  }, callback);
};
