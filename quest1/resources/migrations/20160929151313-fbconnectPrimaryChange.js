'use strict';

exports.up = function(db, callback) {
  db.runSql('alter table fb_connect drop primary key, add primary key ' +
    '(uid, fb_uid, connect_time)', callback);
};

exports.down = function(db, callback) {
  db.runSql('alter table fb_connect drop primary key, add primary key ' +
    '(uid, fb_uid)', callback);
};
