'use strict';

exports.up = function(db, callback) {
  db.createTable('push_notification_clicks', {
    uid: {type: 'string', notNull: true, primaryKey: true, length: 64},
    notification_id: {
      type: 'string',
      notNull: true,
      primaryKey: true,
      length: 40
    },
    opened_on: {type: 'datetime', notNull: true},
    segment: {type: 'string', length: 30},
    title: {type: 'string', length: 80},
    content: {type: 'text'}
  }, callback);
};

exports.down = function(db, callback) {
  db.dropTable('push_notification_clicks', callback);
};
