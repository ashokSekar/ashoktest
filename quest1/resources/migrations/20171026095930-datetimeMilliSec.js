'use strict';

exports.up = function(db, callback) {
  db.changeColumn('inventory_purchase', 'date_time', {
    type: 'datetime',
    length: 2,
    notNull: true
  }, callback);
};

exports.down = function(db, callback) {
  db.changeColumn('inventory_purchase', 'date_time', {
    type: 'datetime',
    notNull: true
  }, callback);
};
