'use strict';

exports.up = function(db, callback) {
  db.addColumn('user_gs_stats_all', 'bonus_level', {
    type: 'smallint',
    defaultValue: 0,
    unsigned: true
  }, callback);
};

exports.down = function(db, callback) {
  db.removeColumn('user_gs_stats_all', 'bonus_level', callback);
};
