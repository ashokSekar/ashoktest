exports.up = function (db, callback) {
  'use strict';

  db.addColumn('fb_user_game', 'fb_friend_count', {
    type: 'tinyint',
    notNull: true,
    unsigned: true,
    defaultValue: 0
  }, callback);
};

exports.down = function (db, callback) {
  'use strict';

  db.removeColumn('fb_user_game', 'fb_friend_count', callback);
};
