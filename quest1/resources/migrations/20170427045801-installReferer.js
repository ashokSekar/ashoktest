'use strict';

exports.up = function(db, callback) {
  db.addColumn('user_device', 'install_referrer', {
      type: 'text'
    }, callback);
};

exports.down = function(db, callback) {
  db.removeColumn('user_device', 'install_referrer', callback);
};
