exports.up = function (db, callback) {
  'use strict';

  var table_data = {
      id: {type: 'int', primaryKey: true,autoIncrement: true, notNull: true},
      name: {type: 'string', length: 50, notNull: true},
      platforms: {type: 'string', length: 50, notNull: true},
      start: {type: 'datetime', notNull: true},
      end: {type: 'datetime', notNull: true},
      image: {type: 'string', length: 30},
      multiplier: {type: 'int', length: 2}
    };

  db.createTable('sale_data', table_data, function () {
    db.addColumn('cash_purchase', 'sale_id', {
      type: 'int',
      defaultValue: 0
    }, callback);
  });
};

exports.down = function (db, callback) {
  'use strict';

  db.removeColumn('cash_purchase', 'sale_id', function () {
    db.dropTable('sale_data', callback);
  });
};
