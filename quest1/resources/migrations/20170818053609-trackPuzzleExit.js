'use strict';

exports.up = function(db, callback) {
  db.addColumn('user_gs_stats_all', 'exit_type', {
    type: 'string',
    length: 10
  }, callback);
};

exports.down = function(db, callback) {
  db.removeColumn('user_gs_stats_all', 'exit_type', callback);
};
