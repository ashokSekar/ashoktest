'use strict';

exports.up = function(db, callback) {
  db.createTable('user_bonus_game_stats', {
    fb_uid: {type: 'string', length: 20, notNull: true, primaryKey: true},
    id: {type: 'smallint',  notNull: true, unsigned: true, primaryKey: true},
    date_time: {type: 'datetime', notNull: true}
  }, callback);
};

exports.down = function(db, callback) {
  db.dropTable('user_bonus_game_stats', callback);
};
