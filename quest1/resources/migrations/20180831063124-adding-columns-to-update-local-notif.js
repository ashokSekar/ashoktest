'use strict';

exports.up = function(db, callback) {
    db.addColumn('push_notification_clicks', 'platform', {
    type: 'string',
    notNull: false,
  }, callback);
};

exports.down = function(db, callback) {
  db.dropTable('local_notification_clicks', callback);
};
