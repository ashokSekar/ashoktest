'use strict';

var _ = require('lodash'),
  promise = require('bluebird'),
  table_meta_data = {
    user_game: {
      uid: {type: 'string', notNull: true, primaryKey: true, length: 64},
      free_currency: {type: 'mediumint', unsigned: true},
      paid_currency: {type: 'mediumint', unsigned: true},
      max_ms: {
        type: 'smallint',
        unsigned: true,
        notNull: true,
        defaultValue: 1
      },
      curr_ms: {
        type: 'smallint',
        unsigned: true,
        notNull: true,
        defaultValue: 1
      },
      master_score: {
        type: 'int',
        unsigned: true,
        notNull: true
      },
      music_muted: {
        type: 'tinyint',
        unsigned: true,
        notNull: true
      },
      sound_muted: {
        type: 'tinyint',
        unsigned: true,
        notNull: true
      },
      amount_spent: {type: 'decimal', length: '8, 2', unsigned: true}
    },
    user_device: {
      device_id: {type: 'string', length: 36, notNull: true},
      uid: {type: 'string', length: 64, notNull: true, primaryKey: true},
      device_name: {type: 'string', length: 120},
      is_tablet: {type: 'tinyint', unsigned: true},
      os: {type: 'string', length: 15},
      jail_broken: {type: 'tinyint', notNull: true, unsigned: true},
      type: {type: 'string', length: 255},
      install_time: {type: 'datetime'},
      start_time: {type: 'datetime', notNull: true},
      current_version: {type: 'string', length: 8},
      installed_version: {type: 'string', length: 8},
      current_design_version: {type: 'string', length: 8},
      lives: {type: 'tinyint', notNull: true, defaultValue: 5, unsigned: true},
      language: {type: 'string', length: 6, notNull: true, defaultValue: 'en'},
      source: {type: 'string', length: 30},
      location: {type: 'string', length: 100},
      ip_address: {type: 'string', length: 39},
      last_update: {type: 'datetime'}
    },
    user_gs_stats: {
      fb_uid: {type: 'string', length: 20, notNull: true, primaryKey: true},
      ms_id: {type: 'string', length: 4, notNull: true, primaryKey: true},
      score: {type: 'mediumint', unsigned: true, defaultValue: 0},
      stars: {type: 'tinyint', notNull:true, unsigned: true, defaultValue: 0},
      progress: {type: 'tinyint', unsigned: true, notNull: true},
      time_taken: {
        type: 'smallint',
        unsigned: true,
        defaultValue: 0
      }
    },
    user_gs_stats_all: {
      uid: {type: 'string', notNull: true, primaryKey: true, length: 64},
      ms_id: {type: 'string', notNull: true, length: 4},
      date_time: {type: 'datetime', notNull: true, primaryKey: true},
      time_taken: {type: 'smallint', unsigned: true, notNull: true},
      score: {type: 'mediumint', unsigned: true, notNull: true},
      stars: {type: 'tinyint', unsigned: true, notNull: true},
      progress: {type: 'tinyint', unsigned: true, notNull: true},
      device_id: {type: 'string', length: 36, notNull: true},
      app_version: {type: 'string', length: 8},
      instance_id: {type: 'bigint', unsigned: true},
      design_version: {type: 'bigint', unsigned: true}
    },
    user_info: {
      fb_uid: {type: 'string', primaryKey: true, notNull: true, length: 20},
      gender: {type: 'string', length: 1},
      first_name: {type: 'string', length: 50},
      last_name: {type: 'string', length: 50},
      dob: {type: 'date'}
    },
    fb_user_game: {
      fb_uid: {type: 'string', notNull: true, primaryKey: true, length: 20},
      free_currency: {
        type: 'mediumint',
        unsigned: true,
        defaultValue: 0
      },
      paid_currency: {
        type: 'mediumint',
        unsigned: true,
        defaultValue: 0
      },
      max_ms: {
        type: 'smallint',
        unsigned: true,
        notNull: true,
        defaultValue: 1
      },
      curr_ms: {
        type: 'smallint',
        unsigned: true,
        notNull: true,
        defaultValue: 1
      },
      master_score: {
        type: 'int',
        unsigned: true,
        notNull: true,
        defaultValue: 0
      },
      music_muted: {
        type: 'tinyint',
        unsigned: true,
        notNull: true,
        defaultValue: 0
      },
      sound_muted: {
        type: 'tinyint',
        unsigned: true,
        notNull: true,
        defaultValue: 0
      },
      amount_spent: {type: 'decimal', length: '8, 2' ,unsigned: true}
    },
    track_free_goods: {
      uid: {type: 'string', length: '64', primaryKey: true, notNull: true},
      time: {type: 'datetime', primaryKey: true, notNull: true},
      amount: {type: 'mediumint', unsigned: true, notNull: true},
      source: {type: 'string', length: 16, notNull: true},
      ms: {type: 'mediumint', unsigned: true, notNull: true},
      instance_id: {type: 'bigint', unsigned: true},
      item: {type: 'string', length: 30}
    },
    requests_sent: {
      request_id: {
        type: 'string',
        length: '40',
        primaryKey: true,
        notNull: true
      },
      type: {type: 'string', length: '10'},
      sender: {type: 'string', length: '20', notNull: true},
      receiver: {type: 'string', length: '20', notNull: true, primaryKey: true},
      sent_time: {
        type: 'timestamp',
        notNull: true,
        defaultValue: new String('CURRENT_TIMESTAMP')// jshint ignore:line
      },
      fb_action: {type: 'string', length: '8'}
    },
    processed_requests: {
      request_id: {
        type: 'string',
        length: '40',
        primaryKey: true,
        notNull: true
      },
      type: {type: 'string', length: '10'},
      receiver: {type: 'string', length: '20', primaryKey: true, notNull: true},
      sender: {type: 'string', length: '20', notNull: true},
      sent_time: {type: 'datetime'},
      action: {type: 'string', length: '10', notNull: true},
      action_by: {type: 'string', length: '6'},
      action_time: {
        type: 'timestamp',
        notNull: true,
        defaultValue: new String('CURRENT_TIMESTAMP')// jshint ignore:line
      },
      fb_action: {type: 'string', length: '8'}
    },
    inventory_purchase: {
      uid: {type: 'string', length: '64', primaryKey: true, notNull: true},
      date_time: {type: 'datetime', primaryKey: true, notNull: true},
      item: {type: 'string', length: '30', notNull: true},
      quantity: {type: 'mediumint', unsigned: true, notNull: true},
      free_currency: {
        type: 'mediumint',
        notNull: true,
        unsigned: true,
        defaultValue: 0
      },
      paid_currency: {
        type: 'mediumint',
        notNull: true,
        unsigned: true,
        defaultValue: 0
      },
      remaining_currency: {
        type: 'mediumint',
        notNull: true,
        unsigned: true,
        defaultValue: 0
      },
      curr_ms: {
        type: 'smallint',
        notNull: true,
        unsigned: true,
        defaultValue: 1
      },
      max_ms: {
        type: 'smallint',
        notNull: true,
        unsigned: true,
        defaultValue: 1
      },
      instance_id: {type: 'bigint', unsigned: true}
    },
    fb_invites: {
      sender: {type: 'string', notNull: true, primaryKey: true, length: 20},
      receiver: {type: 'string', notNull: true, primartKey: true, length: 20},
      sent_time: {type: 'datetime', notNull: true, primartKey: true},
      accepted_time: {type: 'datetime'},
      sent_device: {type: 'string', length: 36, notNull: true},
      received_device: {type: 'string', length: 36},
      source: {type: 'string', length: 30, defaultValue: 'unknown'},
      request_id: {type: 'string', length: 60}
    },
    fb_connect: {
      uid: {type: 'string', length: '64', primaryKey: true, notNull: true},
      fb_uid: {type: 'string', notNull: true, primaryKey: true, length: 20},
      connect_time: {type: 'datetime', notNull: true, primartKey: true},
      disconnect_time: {type: 'datetime'}
    },
    cash_purchase: {
      uid: {type: 'string', length: 64, notNull: true, primaryKey: true},
      date_time: {type: 'datetime', notNull: true, primaryKey: true},
      ms: {
        type: 'smallint',
        unsigned: true
      },
      source: {
        type: 'string',
        notNull: true,
        defaultValue: 'unknown',
        length: 255
      },
      currency_bundle: {type: 'string', length: 20, notNull: true},
      local_amount: {
        type: 'decimal',
        length: '7, 2',
        unsigned: true,
        notNull: true,
        defaultValue: 0
      },
      local_currency: {type: 'string', length: 8},
      usd_amount: {type: 'decimal', length: '7, 2', defaultValue: 0},
      receipt: {type: 'text'}
    }
  };

exports.up = function(db, callback) {
  var createTable = promise.promisify(db.createTable.bind(db));

  promise.map(_.keys(table_meta_data), function(table) {
    return createTable(table, table_meta_data[table]);
  }).then(callback.bind(null, null))
  .catch(function(e) {
    console.log('Error in Migration', e);
  });
};

exports.down = function(db, callback) {
  var dropTable = promise.promisify(db.dropTable.bind(db));

  promise.map(_.keys(table_meta_data), function (table) {
    return dropTable(table);
  }).then(callback.bind(null, null))
  .catch(function(e) {
    console.log('Error in Migration', e);
  });
};
