var util = require('../modules/util'),
    sys_config = util.getGamePath('resources/sys_config'),
    db_opts = util.getGamePath('resources/database.json')[
      process.env.NODE_ENV || /* istanbul ignore next*/ 'development'],
    _ = require('lodash'),
    obj = {
      powerups: {
        pregame: [],
        ingame: []
      },
      inventories: _.concat(['lives', 'moves', 'time']),

      /* database */
      // no need to test for config
      db: {
        client: db_opts.driver,
        connection: _.pick(db_opts, 'host', 'port', 'user', 'password',
          'database', 'multipleStatements'),
        pool: {
          min: 2,
          max: 10
        }
      },

      /* log files */
      logger: {
        debug: '/var/log/quest/debug.log',
        error: '/var/log/quest/error.log'
      },

      /* Status codes for server */
      status: {
        OK: 200,
        ERROR: 500
      },

      puzzle_path: __dirname + '/data/puzzles/patches/',

      access_control_prod: {
        origin: '',
        headers: ''
      },

      access_control_dev: {
        origin: '*',
        headers: 'Origin, X-Requested-With, Content-Type, Accept'
      },

      /* life regeneration data */
      life: {
        max: 5,
        duration: 1200000
      },

      messages: {
        err_resp: 'Something went wrong'
      },

      puzzle_base_path: 'resources/data/puzzles/',

      /*
       * Should be overidden in game in following format
       * {
       *   id: 'sudokuquest',
       *   universal_link: 'https://sudokuquest.com/play',
       *   min_ms: 20,
       *   android: {
       *     package: 'com.hashcube.sudokuquest',
       *     name: 'Sudoku Quest',
       *     url: 'play_store_url',
       *     cash: 10
       *   },
       *   ios: {},
       *   amazon: {}
       * }
       */
      cross_promotion: null,

      rewards_min_version: '0',
      /*
       * Should be overidden in game in following format
       * We need to add a product named product_id+'disc'+amount
       * Ex: cash1disc80
       * [{
       *   id: 'reward_id',
       *   min_ms: 20,
       *   type: 'discount' // currently discount only will support
       *   product_id: 'cash1', // product id of the item
       *   paid_user: false // enable/disable for payed user
       *   amount: 80 // percentage of discount
       *   validity: 2 // validity in hours
       * }]
       */
      rewards: [],

      purchase_validation_url: {
        amazon: 'https://appstore-sdk.amazon.com/version/1.0/' +
         'verifyReceiptId/developer/'
      },

      update_available_path: 'resources/updates.json',

      user_resp_prop: [],
      sync_resp_prop: []
    };

obj = _.merge(obj, sys_config, util.getGamePath('resources/config'));
obj.inventories = _.concat(obj.inventories,
  obj.powerups.pregame, obj.powerups.ingame);

module.exports = obj;
