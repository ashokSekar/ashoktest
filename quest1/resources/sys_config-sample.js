module.exports = {
  server: {
    ip: 'localhost:3000'
  },

  /* Memcache server details */
  memcache: {
    servers: ['localhost:11211'],
    lifetime: 600,
    retries: 1,
    prefix: 'quest:'
  },

  /* Should be overidden with game specific data */
  game: {
    app_name: 'Quest',
    package_name: 'com.hashcube.game',
    discard_data: 'quest_discard.data',
    dir: '/'
  },

  // Facebook
  /* generating access token */
  /*
    run `node bin/app_accesstoken.js`
    after updating app id and app secret
  */
  fb: {
    appid: '',
    appsecret: '',
    access_token: ''
  },

  job: {
    prefix: 'quest_job_'
  },

  log_file: {
    app: [
      '/var/log/game/error.log',
      '/var/log/game/worker_error.log'
    ],
    nginx: '/var/log/nginx/game.error.log'
  },

  email: {
    from: 'admin@quest.com',
    to: 'dev@hashcube.com'
  },

  keys: {
    android: 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAokv1POBHQ',
    amazon: '2:5f_DUfYyMiIKZPDU-c9Bp_LmQAWPpSpWb6v8ovcrOVH-M70_0T6z'
  },

  puzzle_path: '/vol/mahjong_puzzles/patches/'
};
