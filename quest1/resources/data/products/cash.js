'use strict';

var util = require('../../../modules/util');

/**
Sample config -->
  cash2: {
    no_of_cash: 525,
    l10n: [{
      cost: 5,
      currency: 'USD'
    }]
  },
  cash3: {
    no_of_cash: 1100,
    l10n: [{
      cost: 10,
      currency: 'USD'
    }]
   },

 *make sure First l10n is always in USD
 */

module.exports = util.getGamePath('resources/data/products/cash');
