var fb = require('fb'),
  logger = require('./debug')('FB'),
  util = require('./util'),
  Class = require('js-class'),
  fb_config = require('../resources/config').fb,
  Base = Class({
  constructor: function () {
    'use strict';

    fb.options({
      appId: fb_config.appid,
      appSecret: fb_config.appsecret,
      accessToken: fb_config.access_token
    });
  },
  deleteRequest: function (id) {
    'use strict';

    fb.api(id, 'delete', function (resp) {
      if (!resp || resp.error) {
        logger.error(
          'deleteRequest id: %s, data: %s',
          id,
          JSON.stringify(resp)
        );
      } else {
        logger.log(
          'deleteRequest id: %s, data: %s',
          id,
          JSON.stringify(resp)
        );
      }
    });
  }
});

module.exports = new (Class(Base, util.getGamePath('modules/facebook')))();
