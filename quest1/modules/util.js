'use strict';
var _ = require('lodash'),
  path = require('path'),
  Class = require('js-class'),
  fs = require('fs'),
  cash, util, Base;

try {
  util = require(path.join(process.cwd(), '/modules/util'));
} catch (e) {
  util = {};
}

Base = Class({ //jshint ignore:line
  getMaxPuzzleData: function (old_data, new_data) {
    var compare_values = {
        progress: 1,
        score: 1,
        stars: 1,
        time_taken: -1
      };

    _.forEach(compare_values, function (val, key) {
      var compare = (old_data[key] - new_data[key]) * val;

      if (compare > 0) {
        return false;
      } else if (compare < 0) {
        old_data = _.pick(new_data, _.keys(old_data));
        return false;
      }
    });
    return old_data;
  },

  undefTrim: function (obj) {
    return _.omitBy(obj, function (val) {
      return _.isUndefined(val);
    });
  },

  getGamePath: function (game_path, base_cls) {
    var obj = {},
      file_path = path.join(process.cwd(), '/' + game_path);

    try {
      obj = require(file_path);
      if (base_cls) {
        obj.__base = base_cls;
      }
    } catch (e) {
      if (fs.existsSync(file_path + '.js')) {
        throw e;
      }

      obj = {};
    }
    return obj;
  },

  //TODO: fb_invites, processed_requests
  // this is a helper function, that always returns a Date object/null
  // This function is used, because if the date is passed as a string
  // to mysql, the timezone is lost and is entered as is, leads to wrong data
  // Other approaches were considered, reviver from JSON.parse but that
  // would have iterated through every key value and would have consumed lot of
  // time
  getDate: function (date) {

    if (typeof date === 'string') {
      return new Date(date);
    } else if (date === null || typeof date === 'object') {
      return date;
    } else {
      return new Date();
    }
  },

  getNoOfCash: function (item, multiplier_val) {
    var multiplier = multiplier_val || 1;

    return cash[item].no_of_cash * multiplier;
  },

  getCost: function (item) {
    return cash[item].l10n[0].cost;
  }
});

module.exports = new (Class(Base, util))(); //jshint ignore:line
cash = require('../resources/data/products/cash');
