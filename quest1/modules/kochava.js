'use strict';

var db = require('../models/db/db');

module.exports = {
  insertEvent: function (data) {
    return db.insertKochavaEvent(data)
      .then(function () {
        return true;
      });
  }
};
