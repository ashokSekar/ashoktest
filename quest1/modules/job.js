'use strict';

var amqp = require('amqplib'),
  test = require('../test/lib/private'),
  config = require('../resources/config'),
  debug = require('./debug')('JOB');

module.exports = {
  push: function (queue, data) {
    var connection, channel;

    queue = config.job.prefix + queue;

    amqp.connect()
      .then(function (conn) {
        connection = conn;
        return conn.createChannel();
      })
      .then(function (chann) {
        channel = chann;
        return channel.assertQueue(queue, {durable: true});
      })
      .then(function () {
        channel.sendToQueue(queue, new Buffer(JSON.stringify(data)));
        return channel.close();
      })
      .finally(function () {
        if (connection) {
          connection.close();
        }
      })
      .then(null, debug.error.bind(debug, 'RabitMQ: push'));
  }
};

test.prepare(module.exports, {
  debug: debug
});
