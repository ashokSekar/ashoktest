'use strict';

var Promise = require('bluebird'),
  http = require('http'),
  https = require('https'),
  url = require('url');

module.exports = function (ajax_url, opts) {
  var url_obj = url.parse(ajax_url),
    protocol = url_obj.protocol,
    curr_ctx = protocol && protocol.indexOf('https:') === 0 ? https : http;

  opts = opts || {};
  opts.host = url_obj.host;
  opts.path = url_obj.path;

  return new Promise(function (resolve, reject) {
    var body = '',
      req;

    req = curr_ctx.request(opts, function (res) {
      res.setEncoding('utf8');
      res.on('data', function (chunk) {
        body += chunk;
      });

      res.on('end', function () {
        if (opts.json) {
          try {
            body = JSON.parse(body);
          } catch (ex) {
            return reject(body);
          }
        }

        resolve({
          statusCode: res.statusCode,
          body: body
        });
      });
    });

    req.on('error', reject);
    req.end();
  });
};
