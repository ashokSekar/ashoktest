'use strict';

var logger = require('./debug')('PV'),
  crypto = require('crypto'),
  Class = require('js-class'),
  util = require('./util'),
  ajax = require('./ajax'),
  config = require('../resources/config'),
  Promise = require('bluebird'),
  test = require('../test/lib/private'),
  status = {
    valid: 'valid',
    invalid: 'invalid',
    reschedule: 'reschedule'
  },
  getPublicKey = function (public_key) {
    var key = public_key.match(new RegExp('.{0,' + 64 + '}', 'g')).join('\n'),
      pkey = '-----BEGIN PUBLIC KEY-----\n' + key +
        '-----END PUBLIC KEY-----\n';

    return pkey;
  },
  verifyAndroidPurchase = function (signed_data, signature) {
    var public_key = config.keys.android,
      decoded_public_key, verifier,
      parsed_signed_data = JSON.parse(signed_data);

    if (parsed_signed_data.orderId === undefined) {
      return Promise.resolve(status.invalid);
    }

    if (public_key) {
      decoded_public_key = getPublicKey(public_key);
      verifier = crypto.createVerify('SHA1');

      verifier.update(signed_data);
      return Promise.resolve(
        verifier.verify
          (
            decoded_public_key,
            signature,
            'base64'
          ) ? status.valid : status.invalid
      );
    }
    logger.error('Public key not available');
    return Promise.resolve(status.reschedule);
  },
  verifyKindlePurchase =  function (receipt) {
    var url = config.purchase_validation_url.amazon + config.keys.amazon +
      '/user/' + receipt.userid +
      '/receiptId/' + receipt.token,
      response = status.invalid;

    return ajax(url, {
      method: 'GET',
      json: true
    })
    .then(function (resp) {
      if (resp.statusCode === 200) {
        response = status.valid;
      } else if (resp.statusCode === 500 || resp.statusCode === 496) {
        response = status.reschedule;
      }
      return response;
    })
    .catch(function (err) {
      logger.error(
        'Error while verifying recipt with amazon with response: %s for: %s',
        JSON.stringify(err),
        JSON.stringify(receipt)
      );
      return status.reschedule;
    });
  },
  Base = Class({  //jshint ignore:line
    verifyPurchase: function (req) {
      var store = req.store,
        receipt = req.purchase_details.receipt;

      if (!receipt) {
        return Promise.resolve(status.invalid);
      }

      receipt = JSON.parse(receipt);

      if (store === 'android') {
        return verifyAndroidPurchase(
          receipt.purchaseData,
          receipt.dataSignature
        );
      } else if (store === 'kindle') {
        return verifyKindlePurchase(receipt);
      } else {
        logger.error('Unknown store: %s', store);
        return Promise.resolve(status.invalid);
      }
      // IOS purchases are automatically collect and validate from facebook
    }
  });

module.exports = new (Class(//jshint ignore:line
  Base, util.getGamePath('modules/purchase_validator')
))();

test.prepare(module.exports, {
  logger: logger,
  getPublicKey: getPublicKey,
  verifyAndroidPurchase: verifyAndroidPurchase,
  verifyKindlePurchase: verifyKindlePurchase,
  setAjax: function () {
    ajax = require('./ajax'); //jshint ignore:line
  },
  setValuesForAndroidVerification: function (def) {
    verifyAndroidPurchase = def;
  },
  setValuesForKindleVerification: function (def) {
    verifyKindlePurchase = def;
  }
});
