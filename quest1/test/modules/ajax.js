var http = require('http'),
  https = require('https'),
  assert = require('assert'),
  sinon = require('sinon'),
  req_obj = {
    on: function () {},
    end: function () {}
  },
  ajax;

describe('Ajax', function () {
  'use strict';

  before(function () {
    ajax = require('../../modules/ajax');
  });

  it('should use http module for request', function (done) {
    var http_get = sinon.stub(http, 'request', function () {
      http_get.restore();
      done();
      return req_obj;
    });

    ajax('mahjong.com');
  });

  it('should use https module for secure url', function (done) {
    var http_get = sinon.stub(https, 'request', function () {
      http_get.restore();
      done();
      return req_obj;
    });

    ajax('https://mahjong.com');
  });

  it('should reject on request error', function (done) {
    var cache = req_obj.on,
      http_get = sinon.stub(https, 'request', function () {
        http_get.restore();
        return req_obj;
      });

    req_obj.on = function (type, cb) {
      if (type === 'error') {
        req_obj.on = cache;
        cb('err_test');
      }
    };

    ajax('https://mahjong.com')
      .catch(function (err) {
        done(err === 'err_test' ? undefined : 'error');
      });
  });

  it('should resolve with body on response end', function (done) {
    var http_get = sinon.stub(https, 'request', function (opts, cb) {
      http_get.restore();

      cb({
        on: function (type, in_cb) {
          if (type === 'data') {
            in_cb('abc');
          } else if (type === 'end') {
            in_cb();
          }
        },
        setEncoding: function () {},
        statusCode: 10
      });
      return req_obj;
    });

    ajax('https://mahjong.com')
      .then(function (resp) {
        assert.strictEqual(resp.statusCode, 10);
        done(resp.body === 'abc' ? undefined : 'error');
      });
  });

  it('should return json response', function (done) {
    var http_get = sinon.stub(https, 'request', function (opts, cb) {
      http_get.restore();

      cb({
        on: function (type, in_cb) {
          if (type === 'data') {
            in_cb('{\"prop\": 1}');
          } else if (type === 'end') {
            in_cb();
          }
        },
        setEncoding: function () {},
        statusCode: 10
      });
      return req_obj;
    });

    ajax('https://mahjong.com', {json: true})
      .then(function (resp) {
        assert.deepEqual(resp, {
          statusCode: 10,
          body: {
            prop: 1
          }
        });
        done();
      });
  });

  it('should reject on json parse error', function (done) {
    var http_get = sinon.stub(https, 'request', function (opts, cb) {
      http_get.restore();

      cb({
        on: function (type, in_cb) {
          if (type === 'data') {
            in_cb('abc');
          } else if (type === 'end') {
            in_cb();
          }
        },
        setEncoding: function () {},
        statusCode: 10
      });
      return req_obj;
    });

    ajax('https://mahjong.com', {json: true})
      .catch(function () {
        done();
      });
  });
});
