var purchase_validator = require('../../modules/purchase_validator'),
  config = require('../../resources/config'),
  assert = require('assert'),
  sinon = require('sinon'),
  test_util = require('../lib/util'),
  crypto = require('crypto'),
  bluebird = require('bluebird'),
  status = {
    valid: 'valid',
    invalid: 'invalid',
    reschedule: 'reschedule'
  };

describe('purchase_validator', function () {
  'use strict';

  describe('verifyKindlePurchase', function () {
    before(function () {
      var ajax = require('../../modules/ajax'); // jshint ignore:line
    });

    it('should set proper values', function (done) {
      var receipt =
        {
          userid: '1234',
          token: 'kindle_token'
        },
        url = config.purchase_validation_url.amazon + config.keys.amazon +
          '/user/' + receipt.userid +
          '/receiptId/' + receipt.token;

      test_util.getFromCache('modules/ajax').exports = function (c_url, opts) {
        assert.strictEqual(opts.method, 'GET');
        assert.strictEqual(c_url, url);
        done();
        return bluebird.resolve({});
      };
      purchase_validator._setAjax();
      purchase_validator._verifyKindlePurchase(receipt);
    });

    it('should return valid', function (done) {
      var receipt = {
          userid: '1234',
          token: 'kindle_token'
        };

      test_util.getFromCache('modules/ajax').exports = function () {
        return bluebird.resolve({statusCode: 200});
      };
      purchase_validator._setAjax();
      purchase_validator._verifyKindlePurchase(receipt)
      .then(function (resp) {
        assert.strictEqual(resp, status.valid);
        done();
      })
      .catch(function () {
      });
    });

    it('should not return valid for non 200 response', function (done) {
      var receipt = {
          userid: '1234',
          token: 'kindle_token'
        };

      test_util.getFromCache('modules/ajax').exports = function () {
        return bluebird.resolve({statusCode: 400});
      };
      purchase_validator._setAjax();
      purchase_validator._verifyKindlePurchase(receipt)
      .then(function (resp) {
        assert.notEqual(resp, status.valid);
        done();
      })
      .catch(function () {
      });
    });

    it('should return reschedule for error response', function (done) {
      var receipt = {
          userid: '1234',
          token: 'kindle_token'
        };

      test_util.getFromCache('modules/ajax').exports = function () {
        return bluebird.reject();
      };
      purchase_validator._setAjax();
      purchase_validator._verifyKindlePurchase(receipt)
      .then(function (resp) {
        assert.strictEqual(resp, status.reschedule);
        done();
      })
      .catch(function () {
      });
    });

    it('should return reschedule for 500', function (done) {
      var receipt = {
          userid: '1234',
          token: 'kindle_token'
        };

      test_util.getFromCache('modules/ajax').exports = function () {
        return bluebird.resolve({statusCode: 500});
      };
      purchase_validator._setAjax();
      purchase_validator._verifyKindlePurchase(receipt)
      .then(function (resp) {
        assert.strictEqual(resp, status.reschedule);
        done();
      })
      .catch(function () {
      });
    });

    it('should return invalid for unknown reponses', function (done) {
      var receipt = {
          userid: '1234',
          token: 'kindle_token'
        };

      test_util.getFromCache('modules/ajax').exports = function () {
        return bluebird.resolve({statusCode: 999});
      };
      purchase_validator._setAjax();
      purchase_validator._verifyKindlePurchase(receipt)
      .then(function (resp) {
        assert.strictEqual(resp, status.invalid);
        done();
      })
      .catch(function () {
      });
    });

  });

  describe('verifyAndroidPurchase', function () {
    it('should return valid', function (done) {
      var verify_stub = sinon.stub(crypto, 'createVerify', function () {
        return {
          verify: function () {
            verify_stub.restore();
            return true;
          },
          update: function () {
            return true;
          }
        };
      });

      purchase_validator._verifyAndroidPurchase(
        '{"orderId": "123456"}',
        'signature',
        'public_key'
      ).then(function (resp) {
        assert.strictEqual(resp, status.valid);
        done();
      });
    });

    it('should return invalid', function (done) {
      var verify_stub = sinon.stub(
        crypto,
        'createVerify',
        function () {
          return {
            verify: function () {
              return false;
            },
            update: function () {
              verify_stub.restore();
              return true;
            }
          };
        });

      purchase_validator._verifyAndroidPurchase(
        '{"orderId": "123456"}',
        'signature'
      ).then(function (resp) {
        assert.strictEqual(resp, status.invalid);
        done();
      });
    });

    it('should return invalid if no order id', function (done) {
      var verify_stub = sinon.stub(
        crypto,
        'createVerify',
        function () {
          return {
            verify: function () {
              return false;
            },
            update: function () {
              verify_stub.restore();
              return true;
            }
          };
        });

      purchase_validator._verifyAndroidPurchase(
        '{"productId": "123456"}',
        'signature'
      ).then(function (resp) {
        assert.strictEqual(resp, status.invalid);
        done();
      });
    });

    it('should return reschedule if no public key', function (done) {
      var cached_key =  config.keys.android;
      config.keys.android = null;
      purchase_validator._verifyAndroidPurchase(
        '{"orderId": "123456"}',
        'signed_data',
        'signature'
      ).then(function (resp) {
        assert.strictEqual(resp, status.reschedule);
        config.keys.android = cached_key;
        done();
      });
    });
  });

  describe('verifyPurchase', function () {

    it('should return invalid if unknown device type sent', function (done) {
      var req =
        {
          store: 'unknown',
          purchase_details: {
            receipt: '{}'
          }
        },
        logger_stub = sinon.stub(
          purchase_validator._logger,
          'error',
          function (log, store) {
            logger_stub.restore();
            assert.strictEqual(log, 'Unknown store: %s');
            assert.strictEqual(store, req.store);
          }
        );

      purchase_validator.verifyPurchase(req)
        .then(function (val) {
          assert.strictEqual(val, status.invalid);
          done();
        });
    });

    it('should return invalid if no receipt', function (done) {
      var req = {
        store: 'unknown',
        purchase_details: {
          transaction_id: 'abc'
        }
      };

      purchase_validator.verifyPurchase(req)
        .then(function (val) {
          assert.strictEqual(val, status.invalid);
          done();
        });
    });

    it('should return invalid for device type android', function (done) {
      var req =
        {
          store: 'android',
          purchase_details: {
            receipt: '{\"purchaseData\":\"{\\\"orderId\\\":\\\"cash1\\\"}\"}'
          }
        },
        android_verify_cache = purchase_validator._verifyAndroidPurchase,
        kindle_verify_cache = purchase_validator._verifyKindlePurchase;

      purchase_validator._setValuesForAndroidVerification(function () {
        purchase_validator._setValuesForAndroidVerification(
          android_verify_cache
        );
        purchase_validator._setValuesForKindleVerification(
          kindle_verify_cache
        );
        return status.invalid;
      });
      purchase_validator._setValuesForKindleVerification(function () {
        done('Error');
      });

      assert.strictEqual(
        purchase_validator.verifyPurchase(req),
        status.invalid
      );
      done();
    });

    it('should return false if device type kindle', function (done) {
      var req =
        {
          store: 'kindle',
          purchase_details: {
            receipt: '{\"purchaseData\":\"{\\\"orderId\\\":\\\"cash1\\\"}\"}'
          }
        },
        kindle_verify_cache = purchase_validator._verifyKindlePurchase,
        android_verify_cache = purchase_validator._verifyAndroidPurchase;

      purchase_validator._setValuesForKindleVerification(function () {
        purchase_validator._setValuesForAndroidVerification(
          android_verify_cache
        );
        purchase_validator._setValuesForKindleVerification(
          kindle_verify_cache
        );
        return status.invalid;
      });
      purchase_validator._setValuesForAndroidVerification(function () {
        done('Error');
      });

      assert.strictEqual(
        purchase_validator.verifyPurchase(req),
        status.invalid
      );
      done();
    });

    it('should return valid if device type android', function (done) {
      var req =
        {
          store: 'android',
          purchase_details: {
            receipt: '{\"purchaseData\":\"{\\\"orderId\\\":\\\"cash1\\\"}\"}'
          }
        },
        android_verify_cache = purchase_validator._verifyAndroidPurchase,
        kindle_verify_cache = purchase_validator._verifyKindlePurchase;

      purchase_validator._setValuesForAndroidVerification(function () {
        purchase_validator._setValuesForAndroidVerification(
          android_verify_cache
        );
        purchase_validator._setValuesForKindleVerification(
          kindle_verify_cache
        );
        return status.valid;
      });
      purchase_validator._setValuesForKindleVerification(function () {
        done('Error');
      });

      assert.strictEqual(purchase_validator.verifyPurchase(req), status.valid);
      done();
    });

    it('should return true if device type kindle', function (done) {
      var req =
        {
          store: 'kindle',
          purchase_details: {
            receipt: '{\"purchaseData\":\"{\\\"orderId\\\":\\\"cash1\\\"}\"}'
          }
        },
        kindle_verify_cache = purchase_validator._verifyKindlePurchase,
        android_verify_cache = purchase_validator._verifyAndroidPurchase;

      purchase_validator._setValuesForKindleVerification(function () {
        purchase_validator._setValuesForAndroidVerification(
          android_verify_cache
        );
        purchase_validator._setValuesForKindleVerification(
          kindle_verify_cache
        );
        return status.valid;
      });
      purchase_validator._setValuesForAndroidVerification(function () {
        done('Error');
      });

      assert.strictEqual(purchase_validator.verifyPurchase(req), status.valid);
      done();
    });
  });

  describe('getPublicKey', function () {
    it('should return proper values', function (done) {
      var public_key = config.keys.android;
      assert.strictEqual(
        purchase_validator._getPublicKey(public_key),
        '-----BEGIN PUBLIC KEY-----\n' + public_key +
          '\n-----END PUBLIC KEY-----\n'
      );
      done();
    });
  });
});
