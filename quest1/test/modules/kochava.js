var promise = require('../lib/fakePromise').promise,
  db = require('../../models/db/db'),
  kochava = require('../../modules/kochava'),
  assert = require('assert'),
  sinon = require('sinon');

describe('kochava', function () {
  'use strict';
  var insert_event;

  beforeEach(function () {
    insert_event = sinon.stub(db, 'insertKochavaEvent');
  });

  afterEach(function () {
    insert_event.restore();
  });

  describe('insertEvent', function () {
    it('should call db function', function (done) {
      insert_event.onCall(0).returns(promise(1));
      kochava.insertEvent()
        .then(function (res) {
          assert.strictEqual(true, res);
          done();
        });
    });

    it('should fail if insert failure', function (done) {
      insert_event.onCall(0).returns(promise());
      kochava.insertEvent()
        .then(null, function (res) {
          assert.strictEqual(0, res.no);
          done();
        });
    });
  });
});
