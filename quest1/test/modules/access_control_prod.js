var access_control = require('../../modules/access_control_prod'),
  express = require('../lib/express'),
  config = require('../../resources/config').access_control_prod,
  assert = require('assert');

describe('Access Control', function () {
  'use strict';

  it('should set proper values', function (done) {
    var req = express.request({}),
      res = express.response(),
      header = res._header;

    access_control(req, res, function () {
      assert.equal(config.origin, header['Access-Control-Allow-Origin']);
      assert.equal(config.headers, header['Access-Control-Allow-Headers']);
      done();
    });
  });
});
