'use strict';

var job = require('../../modules/job'),
  sinon = require('sinon'),
  config = require('../../resources/config'),
  promise = require('../lib/fakePromise').promise,
  amqp = require('amqplib'),
  prefix = 'mocha_',
  amqp_stub;

describe('Job', function () {
  before(function () {
    config.job.prefix = prefix;
  });

  beforeEach(function () {
    amqp_stub =  sinon.stub(amqp, 'connect');
  });

  afterEach(function () {
    amqp_stub.restore();
  });

  describe('push()', function () {
    it('should call logger error', function (done) {
      var error_cache = job._debug.error;

      job._debug.error = function () {
        job._debug.error = error_cache;
        done();
      };

      amqp_stub.returns(promise());
      job.push();
    });

    it('should push job to queue', function (done) {
      var conn = promise({
        createChannel: function () {
          return promise({
            assertQueue: function () {
              return promise(2);
            },
            sendToQueue: function (queue) {
              done(queue === prefix + 'test' ? undefined : 'error');
            },
            close: function () {}
          });
        },
        close: function () {}
      });

      amqp_stub.returns(conn);
      job.push('test', {});
    });

    it('should close channel and connection', function (done) {
      var count = 0,
        conn = promise({
          createChannel: function () {
            return promise({
              assertQueue: function () {
                return promise(2);
              },
              sendToQueue: function () {
                ++count;
              },
              close: function () {
                return promise(++count);
              }
            });
          },
          close: function () {
            done(++count === 3 ? undefined : 'error');
          }
        });

      amqp_stub.returns(conn);
      job.push('test', {});
    });
  });
});
