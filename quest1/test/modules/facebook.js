var facebook = require('../../modules/facebook'),
  fb = require('fb'),
  config = require('../../resources/config').fb,
  assert = require('assert'),
  sinon = require('sinon'),
  debug = require('../../modules/debug')('FB');

describe('facebook', function () {
  'use strict';
  var fb_api, log, error;

  beforeEach(function () {
    fb_api = sinon.stub(fb, 'api');
    log = sinon.stub(debug, 'log');
    error = sinon.stub(debug, 'error');
  });

  afterEach(function () {
    fb_api.restore();
    log.restore();
    error.restore();
  });

  describe('init', function () {
    it('should set proper options', function () {
      var options = fb.options();

      assert.strictEqual(config.appid, options.appId);
      assert.strictEqual(config.appsecret, options.appSecret);
      assert.strictEqual(config.access_token, options.accessToken);
    });
  });

  describe('deleteRequest', function () {
    it('should log error', function (done) {
      var res = {
        error: 'failed to fetch'
      };

      error.onCall(0).returns(done());
      facebook.deleteRequest('test');
      fb_api.callArgWith(2, res);
    });

    it('should log success', function (done) {
      var res = {};

      log.onCall(0).returns(done());
      facebook.deleteRequest('test');
      fb_api.callArgWith(2, res);
    });
  });
});
