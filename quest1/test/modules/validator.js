var validator, validate,
  assert = require('assert'),
  _ = require('lodash');

before(function () {
  'use strict';

  validator = require('../../modules/validator');
  validate = validator.validate;
});

describe('Validator', function () {
  'use strict';

  describe('required', function () {
    it('should pass validation', function () {
      var valid = validate({test: 'apple'}, {test: {required: true}});

      assert.equal(valid.validation.validated, true);
      assert.deepEqual(valid.validation.errors, null);
    });

    it('should fail validation', function () {
      var valid = validate({}, {test: {required: true}});

      assert.equal(valid.validation.validated, false);
      assert.deepEqual(valid.validation.errors, {test: ['test is required']});
    });
  });

  describe('canbe_null', function () {
    it('should pass validation if property of type with canbe_null' +
      ' set and value is null', function () {
        var valid = validate({test: null},
          {test: {type: 'int', canbe_null: true}});

        assert.equal(valid.validation.validated, true);
      }
    );

    it('should fail validation if property of type with canbe_null' +
      ' not set and value is null', function () {
        var valid = validate({test: null}, {test: {type: 'int'}});

        assert.equal(valid.validation.validated, false);
      }
    );
  });

  describe('int' , function () {
    it('should pass validation for number', function () {
      var valid = validate({test: '12345'}, {test: {type: 'int'}});

      assert.equal(valid.validation.validated, true);
    });

    it('should not pass validation for part number', function () {
      var valid = validate({test: '12345adffaf'}, {test: {type: 'int'}});

      assert.equal(valid.validation.validated, false);
    });

    it('should not pass validation for string', function () {
      var valid = validate({test: 'afad3ad323'}, {test: {type: 'int'}});

      assert.equal(valid.validation.validated, false);
    });
  });

  describe('min', function () {
    it('should pass validation for string', function () {
      var valid = validate({test: 'Hola! banana'},
        {test: {min: 8}});

      assert.equal(valid.validation.validated, true);
      assert.deepEqual(valid.validation.errors, null);
    });

    it('should fail validation for null', function () {
      var valid = validate({test: null},
        {test: {min: 8}});

      assert.deepEqual(valid.validation.errors,
        {test: ['test should be atleast 8']});
      assert.equal(valid.validation.validated, false);
    });

    it('should fail validation for string', function () {
      var valid = validate({test: 'to'}, {test: {min: 4}});

      assert.equal(valid.validation.validated, false);
      assert.deepEqual(valid.validation.errors,
        {test: ['test should be atleast 4']});
    });

    it('should pass for equality in string', function () {
      var valid = validate({test: 'hello'}, {test: {min: 5}});

      assert.equal(valid.validation.validated, true);
      assert.deepEqual(valid.validation.errors, null);
    });

    it('should pass test for number value (integer)', function () {
      var valid = validate({test: 23}, {test: {min: 6}});

      assert.equal(valid.validation.validated, true);
      assert.deepEqual(valid.validation.errors, null);
    });

    it('should fail test for number value (integer)', function () {
      var valid = validate({test: 3}, {test: {min: 6}});

      assert.equal(valid.validation.validated, false);
      assert.deepEqual(valid.validation.errors,
        {test: ['test should be atleast 6']});
    });

    it('should pass for equality in number value (int)', function () {
      var valid = validate({test: 22}, {test: {min: 22}});

      assert.equal(valid.validation.validated, true);
      assert.deepEqual(valid.validation.errors, null);
    });

    it('should pass test for number value (floats)', function () {
      var valid = validate({test: 22.1}, {test: {min: 22}});

      assert.equal(valid.validation.validated, true);
      assert.deepEqual(valid.validation.errors, null);
    });

    it('should fail test for number value (floats)', function () {
      var valid = validate({test: 22.1}, {test: {min: 22.2}});

      assert.equal(valid.validation.validated, false);
      assert.deepEqual(valid.validation.errors,
        {test: ['test should be atleast 22.2']});
    });

    it('should pass for equality in number value (floats)', function () {
      var valid = validate({test: 22.2}, {test: {min: 22.2}});

      assert.equal(valid.validation.validated, true);
      assert.deepEqual(valid.validation.errors, null);
    });

    it('should pass for size of an object', function () {
      var valid = validate({test: {a: '1'}}, {test: {min: 1}});

      assert.equal(valid.validation.validated, true);
      assert.deepEqual(valid.validation.errors, null);
    });

    it('should fail test object size', function () {
      var valid = validate({test: {a: '1'}}, {test: {min: 2}});

      assert.equal(valid.validation.validated, false);
      assert.deepEqual(valid.validation.errors,
        {test: ['test should be atleast 2']});
    });
  });

  describe('max', function () {
    it('should pass validation for string', function () {
      var valid = validate({test: 'Hola! banana'},
        {test: {max: 15}});

      assert.equal(valid.validation.validated, true);
      assert.deepEqual(valid.validation.errors, null);
    });

    it('should fail validation for null', function () {
      var valid = validate({test: null},
        {test: {max: 15}});

      assert.deepEqual(valid.validation.errors,
        {test: ['test shouldn\'t exceed 15']});
      assert.equal(valid.validation.validated, false);
    });

    it('should fail validation for string', function () {
      var valid = validate({test: 'grande'}, {test: {max: 4}});

      assert.equal(valid.validation.validated, false);
      assert.deepEqual(valid.validation.errors,
        {test: ['test shouldn\'t exceed 4']});
    });

    it('should pass for equality in string', function () {
      var valid = validate({test: 'hello'}, {test: {max: 5}});

      assert.equal(valid.validation.validated, true);
      assert.deepEqual(valid.validation.errors, null);
    });

    it('should pass test for number value (integer)', function () {
      var valid = validate({test: 3}, {test: {max: 6}});

      assert.equal(valid.validation.validated, true);
      assert.deepEqual(valid.validation.errors, null);
    });

    it('should fail test for number value (integer)', function () {
      var valid = validate({test: 7}, {test: {max: 6}});

      assert.equal(valid.validation.validated, false);
      assert.deepEqual(valid.validation.errors,
        {test: ['test shouldn\'t exceed 6']});
    });

    it('should pass for equality in number value (int)', function () {
      var valid = validate({test: 22, type: 'int'}, {test: {max: 22}});

      assert.equal(valid.validation.validated, true);
      assert.deepEqual(valid.validation.errors, null);
    });

    it('should pass test for number value (floats)', function () {
      var valid = validate({test: 21.99}, {test: {max: 22}});

      assert.equal(valid.validation.validated, true);
      assert.deepEqual(valid.validation.errors, null);
    });

    it('should fail test for number value (floats)', function () {
      var valid = validate({test: 22.21}, {test: {max: 22.2}});

      assert.equal(valid.validation.validated, false);
      assert.deepEqual(valid.validation.errors,
        {test: ['test shouldn\'t exceed 22.2']});
    });

    it('should pass for equality in number value (floats)', function () {
      var valid = validate({test: 22.2}, {test: {max: 22.2}});

      assert.equal(valid.validation.validated, true);
      assert.deepEqual(valid.validation.errors, null);
    });

    it('should pass for size of an object', function () {
      var valid = validate({test: {a: '1'}}, {test: {max: 1}});

      assert.equal(valid.validation.validated, true);
      assert.deepEqual(valid.validation.errors, null);
    });

    it('should fail test object size', function () {
      var valid = validate({test: {a: '1', b: 2}}, {test: {max: 1}});

      assert.equal(valid.validation.validated, false);
      assert.deepEqual(valid.validation.errors,
        {test: ['test shouldn\'t exceed 1']});
    });

    it('should pass if request is empty', function () {
      var valid = validate(null);

      assert.strictEqual(true, valid.validation.validated);
    });
  });

  describe('regex()', function () {
    it('should pass test for regex', function () {
      var valid = validate({test: 'aaaaa'}, {test: {regex: /a{5}/}});

      assert.equal(valid.validation.validated, true);
      assert.deepEqual(valid.validation.errors, null);
    });

    it('should fail test for regex', function () {
      var valid = validate({test: 'aaaa'}, {test: {regex: /a{5}/}});

      assert.equal(valid.validation.validated, false);
      assert.deepEqual(valid.validation.errors,
        {test: ['test doesn\'t match given regex /a{5}/']});
    });
  });

  describe('Sanitize', function () {
    var values = {
      email: {
        valid: ['test@hashcube.com', 'me@me.in', 'abc.cc@aa.com', 'a_b@c.co'],
        invalid: ['peeE@rr', 'rrr@', 'r.ar@e', 'gmail.com', 'aabbee', 123]
      },
      numeric: {
        valid: [1, 23, 222],
        invalid: ['a', 'xx', '11224.1', 22.2]
      },
      int: {
        valid: [1, 2, 123, 4, -4, '123'],
        invalid: ['abc', 'vasls']
      },
      alpha: {
        valid: ['abc', 'XyZ', 'XYZ'],
        invalid: ['XY1', 122, '123']
      },
      object: {
        valid: [{}, {a: 'b'}, {a: {b: 'c'}}, {a: {b: []}}],
        invalid: ['a', 1, ['a']]
      },
      array: {
        valid: [['a'], [1], [{1: 2}]],
        invalid: ['a', 1, {a: 2}]
      },
      string: {
        valid: ['a', 'wwe', 'xxxv']
      },
      date: {
        valid: [new Date().toString(), new Date().toJSON(),
          new Date().toISOString(), new Date().toUTCString()],
        invalid: ['a']
      },
      null: {
        valid: [null, undefined],
        invalid: ['a', 1, [], {}]
      }
    };

    _.each(values, function (val, key) {
      describe(key, function () {
        _.each(val.valid, function (test_value) {
          it(test_value + ' should be a valid ' + key, function () {
            var valid = validate({test: test_value}, {test: {type: key}});

            assert.equal(true, valid.validation.validated);
            assert.deepEqual(valid.validation.errors, null);
          });
        });

        _.each(val.invalid, function (test_value) {
          it(test_value + ' should be a invalid ' + key, function () {
            var valid = validate({test: test_value}, {test: {type: key}});

            assert.equal(false, valid.validation.validated);
            assert.deepEqual(valid.validation.errors,
              {test: ['test is not a valid ' + key]});
          });
        });
      });
    });

    describe('undefined Sanitizer', function () {
      it('should throw an error', function () {
        assert
          .throws(
            validate.bind(null, {test: '1'}, {test: {type: 'unknown'}})
          );
      });
    });

    it('sanitize if sanitize type is passed', function () {
      var data = {test: '21233'},
        valid = validate(data, {test: {required: true, sanitize: 'int'}});

      assert.equal(true, valid.validation.validated);
      assert.strictEqual(data.test, 21233);
    });

    it('sanitize if sanitize if date passed as int', function () {
      var data = {test: '21233'},
        valid = validate(data, {test: {required: true, sanitize: 'date'}});

      assert.equal(true, valid.validation.validated);
      assert.strictEqual(JSON.stringify(data.test),
        JSON.stringify(new Date(21233)));
    });

    it('sanitize if date passed as float', function () {
      var data = {test: '212334.654'},
        valid = validate(data, {test: {required: true, sanitize: 'date'}});

      assert.equal(true, valid.validation.validated);
      assert.strictEqual(JSON.stringify(data.test),
        JSON.stringify(new Date(212334)));
    });
  });

  describe('Type', function () {
    var values = {
      email: {
        valid: ['test@hashcube.com', 'me@me.in', 'abc.cc@aa.com', 'a_b@c.co'],
        invalid: ['peeE@rr', 'rrr@', 'r.ar@e', 'gmail.com', 'aabbee', 123]
      },
      numeric: {
        valid: [1, 23, 222],
        invalid: ['a', 'xx', '11224.1', 22.2]
      },
      int: {
        valid: [1, 2, 123, 4, -4, '123'],
        invalid: ['abc', 'vasls']
      },
      alpha: {
        valid: ['abc', 'XyZ', 'XYZ'],
        invalid: ['XY1', 122, '123']
      },
      object: {
        valid: [{}, {a: 'b'}, {a: {b: 'c'}}, {a: {b: []}}],
        invalid: ['a', 1, ['a']]
      },
      array: {
        valid: [['a'], [1], [{1: 2}]],
        invalid: ['a', 1, {a: 2}]
      },
      string: {
        valid: ['a', 'wwe', 'xxxv']
      },
      bool: {
        valid: [true, false, 1, 0]
      },
      float: {
        valid: [1.1, 2.4, -1.5, 10.0],
        invalid: ['avc', 'aadv']
      },
      null: {
        valid: [null, undefined],
        invalid: ['a', 1, [], {}]
      }
    };

    _.each(values, function (val, key) {
      describe(key, function () {
        _.each(val.valid, function (test_value) {
          it(test_value + ' should be a valid ' + key, function () {
            var to_sanitize = key === 'bool' ? true : false,
              valid = validate({test: test_value},
                {test: {type: key, sanitize: to_sanitize}});

            assert.equal(true, valid.validation.validated);
            assert.deepEqual(valid.validation.errors, null);
          });
        });

        _.each(val.invalid, function (test_value) {
          it(test_value + ' should be a invalid ' + key, function () {
            var valid = validate({test: test_value},
              {test: {type: key}});

            assert.equal(false, valid.validation.validated);
            assert.deepEqual(valid.validation.errors,
              {test: ['test is not a valid ' + key]});
          });
        });
      });
    });

    describe('undefined type', function () {
      it('should throw an error', function () {
        assert
          .throws(
            validate.bind(null, {test: '1'},
              {test: {type: 'unknown', sanitize: false}})
          );
      });
    });
  });

  describe('array validation', function () {
    var data, rules;

    it('should throw validation error if property value is ' +
      'not array', function () {
        data = {puzzles: 'jhjh'};
        rules = {puzzles: {is_array: true}};

        assert.strictEqual(false, validate(data, rules).validation.validated);
      }
    );

    it('should succeed if value is array', function () {
      data = {puzzles: ['adsad', 'sasd']};
      rules.puzzles = {is_array: true, type: 'string'};

      assert.strictEqual(true, validate(data, rules).validation.validated);
    });

    it('should remove array element if it doesn\'t follow rules', function () {
      data = {puzzles: [{id: 'ssw'}, {id: 4}]};
      rules = {puzzles: {is_array: true, id: {type: 'int'}}};

      assert.strictEqual(true, validate(data, rules).validation.validated);
      assert.deepEqual([{id: 4}], validate(data, rules).data.puzzles);
    });

    it('should log proper error if array element doesn\'t' +
      ' follow rules', function (done) {
        var sync_data = {puzzles: [{id: 'ssw'}, {id: 4}]},
          cache = validator.logger.error;

        validator.logger.error = function () {
          validator.logger.error = cache;
          done();
        };

        rules = {puzzles: {is_array: true, id: {type: 'int'}}};

        assert.strictEqual(true, validate(data, rules).validation.validated);
        assert.deepEqual([{id: 4}], validate(sync_data, rules).data.puzzles);
      }
    );
  });

  describe('sub property validation', function () {
    var data, rules;

    it('should fail if sub property validation fails', function () {
      data = {user: {uid: 123, puzzles: {id: 'ssw'}}};
      rules = {user: {uid: {type: 'int'}, puzzles:
        {id: {type: 'int'}}}};

      assert.strictEqual(false, validate(data, rules).validation.validated);
    });

    it('should success if all property validated', function () {
      data = {user: {uid: 123, puzzles: [{id: 6}, {id: 4}]}};
      rules = {user: {uid: {type: 'int'}, puzzles:
        {is_array: true, id: {type: 'int'}}}};

      assert.strictEqual(true, validate(data, rules).validation.validated);
    });
  });

  describe('express function', function () {
    var data = {test: 'sds'},
      resp = {error: function () {}},
      rule = {test: {type: 'int'}};

    it('should add validate function to the passed request', function () {
      var req = {};

      validator.express(req, {});
      assert.strictEqual(true, _.isFunction(req.validate));
    });

    it('should validate request body', function () {
      var req = {body: data};

      validator.express(req, resp);
      assert.strictEqual(false, req.validate(rule));
    });

    it('should validate request params', function () {
      var req = {params: data};

      validator.express(req, resp);
      assert.strictEqual(false, req.validate(null, rule));
    });

    it('should validate request query', function () {
      var req = {query: data};

      validator.express(req, resp);
      assert.strictEqual(false, req.validate(null, null, rule));
    });

    it('should go to next', function (done) {
      var req = {query: data};

      validator.express(req, resp, done);
      assert.strictEqual(false, req.validate(null, null, rule));
    });

    it('should return empty for no errors', function () {
      var req = {query: data},
        cache = _.map;

      _.map = function () {
        cache = _.map;
        return {};
      };

      validator.express(req, resp);
      assert.strictEqual(true, req.validate(null, null, rule));
    });
  });
});
