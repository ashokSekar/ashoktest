var boot = require('../../modules/boot'),
  glob = require('glob'),
  sinon = require('sinon'),
  fs = require('fs'),
  assert = require('assert');

describe('boot', function () {
  'use strict';
  var result = {get: {}, post: {}},
    env = 'production',
    app = {
      _props: {},
      post: function (route, before, method) {
        if (!method) {
          method = before;
        } else {
          method.before = before;
        }
        result.post[route] = method;
      },
      get: function (route, before, method) {
        if (route === 'env') {
          return env;
        }

        if (!before && !method) {
          return this._props[route];
        }

        if (!method) {
          method = before;
        } else {
          method.before = before;
        }
        result.get[route] = method;
      },
      use: function () {}
    },
    root_path = __dirname,
    file = root_path + '/routetest',
    ext = file + '.js',
    exports_string = 'module.exports = ',
    quest_ctr = root_path +  '/controllers',
    quest_ctr_file = quest_ctr + '/test.js',
    proj_index = root_path + '/test',
    proj_ctr = proj_index + '/controllers',
    proj_ctr_file = proj_ctr + '/test.js',
    glob_stub;

  beforeEach(function () {
    glob_stub = sinon.stub(glob, 'sync');
  });

  after(function () {
    if (fs.existsSync(ext)) {
      fs.unlinkSync(ext);
    }
  });

  afterEach(function () {
    glob_stub.restore();
    result = {get: {}, post: {}};
    delete require.cache[__dirname + '/routetest.js'];
    delete require.cache[proj_ctr_file];
    delete require.cache[quest_ctr_file];
  });

  it('should test for index file', function () {
    var index = file.replace('routetest', 'index'),
      ext_idx = index + '.js',
      fs_stub = sinon.stub(fs, 'existsSync', function () {
        fs_stub.restore();
      });

    glob_stub.returns([index]);
    fs.writeFileSync(ext_idx, exports_string + '{get: function () {}}');
    env = 'development';
    boot(app);
    assert.equal(
      result.get['/modules/:modules_id'].toString(),
        'function () {}');
    env = 'production';
    fs.unlinkSync(ext_idx);
  });

  it('should test for quest only controllers', function () {
    fs.mkdirSync(quest_ctr);
    fs.mkdirSync(proj_index);
    fs.mkdirSync(proj_ctr);

    glob_stub.returns([quest_ctr_file]);
    fs.writeFileSync(quest_ctr_file, exports_string + '{get: function () {}}');
    boot(app);
    assert.equal(
      result.get['/test/:test_id'].toString(),
        'function () {}');
    fs.unlinkSync(quest_ctr_file);
    fs.rmdirSync(quest_ctr);
    fs.rmdirSync(proj_ctr);
    fs.rmdirSync(proj_index);
  });

  it('should test for game only controllers', function () {
    fs.mkdirSync(quest_ctr);
    fs.mkdirSync(proj_index);
    fs.mkdirSync(proj_ctr);
    glob_stub.onCall(0).returns([proj_ctr_file]);
    glob_stub.onCall(1).returns([]);
    fs.writeFileSync(proj_ctr_file, exports_string + '{get: function () {}}');
    boot(app);
    assert.equal(
      result.get['/test/:test_id'].toString(),
        'function () {}');
    fs.unlinkSync(proj_ctr_file);
    fs.rmdirSync(quest_ctr);
    fs.rmdirSync(proj_ctr);
    fs.rmdirSync(proj_index);
  });

  it('should test for multiple controllers same method', function () {
    fs.mkdirSync(quest_ctr);
    fs.mkdirSync(proj_index);
    fs.mkdirSync(proj_ctr);
    glob_stub.onCall(0).returns([proj_ctr_file]);
    glob_stub.onCall(1).returns([quest_ctr_file]);
    fs.writeFileSync(quest_ctr_file, exports_string + '{get: function () {}}');
    fs.writeFileSync(proj_ctr_file, exports_string + '{get: function () {}}');
    boot(app);
    assert.equal(
      result.get['/test/:test_id'].toString(),
        'function () {}');
    assert.equal(Object.keys(result.get).length, 1);
    fs.unlinkSync(proj_ctr_file);
    fs.unlinkSync(quest_ctr_file);
    fs.rmdirSync(quest_ctr);
    fs.rmdirSync(proj_ctr);
    fs.rmdirSync(proj_index);
  });

  it('should test for multiple controllers different method', function () {
    var string = 'var sup = require("' + quest_ctr_file + '");' +
      'var _ = require("lodash");\n' + exports_string +
      '_.extend(sup, {update: function () {}});';

    fs.mkdirSync(quest_ctr);
    fs.mkdirSync(proj_index);
    fs.mkdirSync(proj_ctr);
    glob_stub.onCall(0).returns([proj_ctr_file]);
    glob_stub.onCall(1).returns([quest_ctr_file]);
    fs.writeFileSync(quest_ctr_file, exports_string + '{get: function () {}}');
    fs.writeFileSync(proj_ctr_file, string);
    boot(app);
    assert.equal(
      result.get['/test/:test_id'].toString(),
        'function () {}');
    assert.equal(
     result.post['/test/:test_id'].toString(),
        'function () {}');
    fs.unlinkSync(proj_ctr_file);
    fs.unlinkSync(quest_ctr_file);
    fs.rmdirSync(quest_ctr);
    fs.rmdirSync(proj_ctr);
    fs.rmdirSync(proj_index);
  });

  it('should test for get', function () {
    glob_stub.returns([file]);
    fs.writeFileSync(ext, exports_string + '{get: function () {}}');
    boot(app);
    assert.equal(
      result.get['/modules/:modules_id/routetest/:routetest_id'].toString(),
        'function () {}');
  });

  it('should test for update', function () {
    glob_stub.returns([file]);
    fs.writeFileSync(ext, exports_string + '{update: function () {}}');
    boot(app);
    assert.equal(
      result.post['/modules/:modules_id/routetest/:routetest_id'].toString(),
        'function () {}');
  });

  it('should test for create', function () {
    glob_stub.returns([file]);
    fs.writeFileSync(ext, exports_string + '{create: function () {}}');
    boot(app);
    assert.equal(
      result.post['/modules/:modules_id/routetest'].toString(),
        'function () {}');
  });

  it('should test when controllers folder exists', function (done) {
    var fs_exist_stub = sinon.stub(fs, 'existsSync'),
      temp_file = process.cwd() + '/controllers/routetest.js',
      cache = glob.sync;

    fs.writeFileSync(temp_file, exports_string + '{create: function () {}}');
    glob.sync = function () {
      glob.sync = cache;
      done();
      return [temp_file];
    };
    fs_exist_stub.returns(true);
    glob_stub.onCall(0).returns([temp_file]);
    boot(app);
    fs_exist_stub.restore();
    fs.unlinkSync(temp_file);
  });

  it('should test for batch', function () {
    glob_stub.returns([file]);
    fs.writeFileSync(ext, exports_string + '{batch: function () {}}');
    boot(app);
    assert.equal(
      result.post['/modules/:modules_id/routetest/batch'].toString(),
        'function () {}');
  });

  it('should test for index', function () {
    glob_stub.returns([file]);
    fs.writeFileSync(ext, exports_string + '{index: function () {}}');
    boot(app);
    assert.equal(
      result.get['/modules/:modules_id/routetest'].toString(),
        'function () {}');
  });

  it('should test for before', function () {
    glob_stub.returns([file]);
    fs.writeFileSync(ext, exports_string +
      '{get: function () {}, before: function () {"test"}}');
    boot(app);
    assert.equal(
      result.get['/modules/:modules_id/routetest/:routetest_id']
        .before.toString(), 'function () {"test"}');
  });

  it('should throw unrecognised route error', function () {
    glob_stub.returns([file]);
    fs.writeFileSync(ext, exports_string + '{unknown: function () {}}');
    assert.throws(boot.bind(null, app), function (err) {
      return 'unrecognized route: routetest.unknown' === err.message;
    });
  });

});
