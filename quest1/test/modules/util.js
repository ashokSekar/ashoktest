'use strict';
var fs = require('fs'),
  assert = require('assert'),
  sinon = require('sinon'),
  cache = process.cwd,
  util, cash;

describe('util', function () {
  before(function() {
    process.cwd = function () {
      process.cwd = cache;
      return '/';
    };
    util = require('../../modules/util');
    cash = require('../../resources/data/products/cash');
    cash.cash1 =  {
      no_of_cash: 100,
      l10n: [{
        cost: 1,
        currency: 'USD'
      }]
    };
    cash.cash2 = {
      no_of_cash: 525,
      l10n: [{
        cost: 5,
        currency: 'USD'
      }]
    };
  });
  describe('getMaxPuzzleData()', function () {
    it('should modify max_puzzle data with data with greater ' +
      'progress value', function () {
        var resp = util.getMaxPuzzleData({score: 1, stars: 2, progress: 10},
          {score: 4, stars: 1, progress: 9});

        assert.deepEqual({score: 1, stars: 2, progress: 10}, resp);
      }
    );

    it('should return data from new puzzle if old score is zero', function () {
      var resp = util.getMaxPuzzleData({
          progress: 10,
          moves_used: 8,
          time_taken: 6,
          score: 10
        },
        {progress: 10, moves_used: 10, time_taken: 5, score: 0});

      assert.deepEqual({
        progress: 10,
        moves_used: 8,
        time_taken: 6,
        score: 10
      }, resp);
    });

    it('should return data from old puzzle for if both progress is same and ' +
      'new score is less than old one', function () {
        var resp = util.getMaxPuzzleData({
            progress: 30,
            moves_used: 10,
            time_taken: 8,
            score: 20
          },
          {progress: 30, moves_used: 11, time_taken: 12, score: 5});

        assert.deepEqual({
          progress: 30,
          moves_used: 10,
          time_taken: 8,
          score: 20
        }, resp);
      }
    );

    it('should return new puzzle if new moves used are lesser', function () {
      var resp = util.getMaxPuzzleData({
          progress: 10,
          moves_used: 12,
          time_taken: 6,
          score: 10
        },
        {progress: 10, moves_used: 10, time_taken: 5, score: 10});

      assert.deepEqual({
        progress: 10,
        moves_used: 10,
        time_taken: 5,
        score: 10
      }, resp);
    });

    it('shouldnt change old puzzle if new moves used are more', function () {
      var resp = util.getMaxPuzzleData({
          progress: 10,
          moves_used: 8,
          time_taken: 6,
          score: 10
        },
        {progress: 10, moves_used: 10, time_taken: 7, score: 10});

      assert.deepEqual({
        progress: 10,
        moves_used: 8,
        time_taken: 6,
        score: 10
      }, resp);
    });

    it('should return new puzzle if new time taken is lesser', function () {
      var resp = util.getMaxPuzzleData({
          progress: 10,
          moves_used: 12,
          time_taken: 6,
          score: 10
        },
        {progress: 10, moves_used: 12, time_taken: 5, score: 10});

      assert.deepEqual({
        progress: 10,
        moves_used: 12,
        time_taken: 5,
        score: 10
      }, resp);
    });

    it('shouldnt change old puzzle if new time taken is more', function () {
      var resp = util.getMaxPuzzleData({
          progress: 10,
          moves_used: 8,
          time_taken: 6,
          score: 10
        },
        {progress: 10, moves_used: 8, time_taken: 9, score: 10});

      assert.deepEqual({
        progress: 10,
        moves_used: 8,
        time_taken: 6,
        score: 10
      }, resp);
    });

    it('shouldnt change old data if new progress is less and non zero ' +
      'properties', function () {
        var resp = util.getMaxPuzzleData({progress: 10, uid: 123, abc: 4},
          {progress: 9, uid: 2, abc: 5});

        assert.deepEqual({progress: 10, uid: 123, abc: 4}, resp);
      }
    );
  });

  describe('undefTrim', function () {
    it('should omit undefined data', function () {
      var data = util.undefTrim({
        test: undefined,
        word: 'Welcome'
      });
      assert.deepEqual({word: 'Welcome'}, data);
    });
  });

  describe('getGamePath', function () {
    it('should return empty obj when no file present', function () {
      assert.deepEqual({}, util.getGamePath('test'));
    });

    it('should throw error on import fail of existing file', function (done) {
      var exists = sinon.stub(fs, 'existsSync');

      try {
        exists.returns(true);
        util.getGamePath('test');
      } catch (e) {
        done();
      }

      exists.restore();
    });

    it('should return file obj when file present', function () {
      var file_name = 'test.js',
        obj = {
          key: 'test'
        };

      fs.writeFileSync(file_name, 'module.exports = ' + JSON.stringify(obj) +
        ';');
      assert.deepEqual(obj, util.getGamePath(file_name));
      fs.unlinkSync(file_name);
    });

    it('should return file obj with passed base class', function () {
      var file_name = 'test.js',
        obj = {
          key: 'test'
        },
        A = function () {};

      fs.writeFileSync(file_name, 'module.exports = ' + JSON.stringify(obj) +
        ';');
      obj.__base = A;
      assert.deepEqual(obj, util.getGamePath(file_name, A));
      fs.unlinkSync(file_name);
    });
  });

  describe('getDate', function () {
    it('should return date object of string ifinput is string', function () {
      var date = '2017-04-21T13:34:01.420Z';

      assert.strictEqual(new Date(date).getTime(),
        util.getDate(date).getTime());
    });

    it('should return null, if input date is null', function () {
      assert.strictEqual(null, util.getDate(null));
    });

    it('should return date object as is, if input date is date', function () {
      var date = new Date('2017-04-21T13:34:01.420Z');

      assert.strictEqual(date.getTime(), util.getDate(date).getTime());
    });

    it('should return current date, if input date is false', function () {
      assert.strictEqual(new Date().getTime(), util.getDate(false).getTime());
    });

  });

  describe('getNoOfCash()', function () {
    it('should return getNoOfCash', function () {
      var item = 'cash1';

      assert.strictEqual(cash[item].no_of_cash, util.getNoOfCash(item));
    });
  });

  describe('getCost()', function () {
    it('should return getCost', function () {
      var item = 'cash1';

      assert.strictEqual(cash[item].l10n[0].cost, util.getCost(item));
    });
  });
});
