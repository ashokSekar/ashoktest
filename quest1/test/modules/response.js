var responseHandler,
  debug,
  util_test = require('../lib/util'),
  mock = require('mock-require'),
  assert = require('assert');

describe('Response handler', function () {
  'use strict';

  var req = {},
    next = function () {};

  before(function () {
    debug = require('../../modules/debug')('RESPONSE');
    responseHandler = require('../../modules/response');
  });

  afterEach(function () {
    mock.stopAll();
  });

  describe('handler', function () {
    var resp = {
        end: function () {}
      };

    it('should register logger and call next', function () {
      assert.deepEqual(resp.error, undefined);
      responseHandler(req, resp, next);
      assert.notDeepEqual(undefined, resp.end);
    });

    it('end should invoke original end function', function (done) {
      resp.end = function () {
        done();
      };

      responseHandler(req, resp, next);
      resp.end();
    });
  });

  describe('response error', function () {
    it('should set error status (500)', function (done) {
      var resp = {
        status: function (code) {
          done(code === 500 ? null: 'error');
          return this;
        },
        json: function () {
        }
      };

      responseHandler(req, resp, next);
      resp.error({});
    });

    it('should respond with error message', function (done) {
      var resp = {
        status: function () {
          return this;
        },
        json: function (data) {
          done(data === 'Something went wrong' ? null: 'error');
        }
      };

      responseHandler(req, resp, next);
      resp.error({});
    });

    it('should log error with body and query as data for error ' +
      'resp', function (done) {
        var resp = {
            status: function () {
              return this;
            },
            json: function () {
            }
          };

        mock('../../modules/debug', function (type) {
          return {
            error: function (err, data, query) {
              assert.deepEqual({}, err);
              assert.strictEqual('body', data);
              assert.strictEqual('query', query);
              done(type.startsWith('RESPONSE') ? null : 'error');
              mock.stop('../../modules/debug');
            }
          };
        });

        util_test.removeFromCache('modules/response');
        debug = require('../../modules/debug');
        responseHandler = require('../../modules/response');

        req.body = 'body';
        req.query = 'query';
        responseHandler(req, resp, next);
        resp.error({});
      }
    );

    it('should log error if any error happened on response', function (done) {
      var resp = {
          status: function () {
            return this;
          },
          json: function () {
            throw new Error('resp_err_a');
          }
        },
        count = 0;

      mock('../../modules/debug', function (err) {
        return {
          error: function () {
            if (++count < 2) {
              return;
            }
            done(err.startsWith('RESPONSE') ? null : 'error');
            mock.stop('../../modules/debug');
          }
        };
      });

      util_test.removeFromCache('modules/response');
      debug = require('../../modules/debug');
      responseHandler = require('../../modules/response');

      responseHandler(req, resp, next);

      resp.error({});
    });
  });

  describe('response success', function () {
    it('should set ok status (200)', function (done) {
      var resp = {
        status: function (code) {
          done(code === 200 ? null: 'error');
          return this;
        },
        json: function () {
        }
      };

      responseHandler(req, resp, next);
      resp.success('hello');
    });
    it('should respond with given data', function (done) {
      var resp = {
        status: function () {
          return this;
        },
        json: function (data) {
          done(data === 'hello' ? null: 'error');
        }
      };

      responseHandler(req, resp, next);
      resp.success('hello');
    });
    it('should log error if any error happened', function (done) {
      var resp = {
          status: function () {
            return this;
          },
          json: function () {
            throw new Error('resp_err');
          }
        };

      mock('../../modules/debug', function (err) {
        return {
          error: function () {
            done(err.startsWith('RESPONSE') ? null : 'error');

            mock.stop('../../modules/debug');
          }
        };
      });

      util_test.removeFromCache('modules/response');
      debug = require('../../modules/debug');
      responseHandler = require('../../modules/response');

      responseHandler(req, resp, next);
      resp.success('hello');
    });
  });

});
