var _ = require('lodash'),
  path = require('path');

beforeFile(function() {
  'use strict';

  var all_modules = require.cache,
    modules = _.keys(all_modules),
    clean = [
      path.join(__dirname, '../controllers'),
      path.join(__dirname, '../models'),
      path.join(__dirname, '../modules'),
      path.join(__dirname, '../resources'),
      path.join(__dirname, '../server.js')
    ];

  _.each(modules, function (file) {
    if (_.find(clean, function (prefix) {
      return file.startsWith(prefix);
    })) {
      delete all_modules[file];
    }
  });
});
