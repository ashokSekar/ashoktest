'use strict';
var assert = require('assert'),
  bluebird = require('bluebird'),
  sinon = require('sinon'),
  fs = bluebird.promisifyAll(require('fs')),
  _ = require('lodash'),
  promise = require('../../lib/fakePromise').promise,
  knex_mock = require('mock-knex'),
  cached_function = {}, db,
  tracker, logger_stub, config;

describe('DB', function () {
  before(function () {
    db = require('../../../models/db/db');
    config = require('../../../resources/config');
    logger_stub = sinon.stub(db._logger, 'error');
    db._setFunction('global.assert', 'require(\'assert\')');
    knex_mock.mock(db._knex);
    tracker = knex_mock.getTracker();

    config.powerups = {
      pregame: ['pow_a'],
      ingame: ['pow_b']
    };
  });

  beforeEach(function (done) {
    cached_function = {
      fetchAndUpdateUser: db._fetchAndUpdateUser,
      fetchUserByFBId: db._fetchUserByFBId,
      updateUserCache: db._updateUserCache,
      insertEachValue: db._insertEachValue,
      fetchCompletedUserPuzzleData: db._fetchCompletedUserPuzzleData,
      fetchFriendData: db._fetchFriendData,
      fetchAddBonusPlayed: db._fetchAddBonusPlayed
    };
    tracker.install();
    done();
  });
  afterEach(function (done) {
    _.each(cached_function, function (val, key) {
      db._setFunction(key, val);
    });
    tracker.uninstall();
    logger_stub.restore();
    done();
  });

  describe('errorHandler', function() {
    it('should resolve for dupplicate entry ', function (done) {
      logger_stub.returns(true);
      db._errorHandler()({code: 'ER_DUP_ENTRY'})
        .then(function (res) {
          assert.deepEqual(res, null);
          done();
        });
    });

    it('should resolve for non duplicate entries', function (done) {
      logger_stub.returns(true);
      db._errorHandler()({code: ''})
        .catch(function(err) {
          assert.deepEqual(err, {code: ''});
          done();
        });
    });
  });

  describe('getUserRecord()', function () {
    it('should get fetch data from db if no fb_id', function (done) {
      tracker.on('query', function (query) {
        assert.deepEqual(query.method, 'first');
        query.response({
          uid: 'uid',
          go_red: 10
        });
      });
      db.getUserRecord('uid')
        .then(function (resp) {
          assert.deepEqual(resp.game.uid, 'uid');
          assert.deepEqual(resp.game.go_red, 10);
          done();
        });
    });

    it('should return empty user data if user not present in ' +
      'db', function (done) {
      tracker.on('query', function (query) {
        assert.deepEqual(query.method, 'first');
        query.response();
      });
      db.getUserRecord('uid')
        .then(function (user) {
          done(user.info);
        });
    });

    it('should return user record from cache', function (done) {
      var memcache_data = {
          info: {
            uid: 'aaa',
            fb_uid: '1234',
            gender: 'M',
            first_name: 'Test',
            last_name: 'User'
          }
        },
        update_cache = sinon.stub(db._cache, 'getValue', function () {
          update_cache.restore();
          return bluebird.resolve(memcache_data);
        });

      tracker.on('query', function (query) {
        assert.deepEqual(query.method, 'first');
        query.response({
          uid: 'uid',
          device_name: 'IPhone'
        });
      });
      db.getUserRecord('aaa', '1234')
        .then(function (data) {
          assert.deepEqual(data.info.uid, memcache_data.info.uid);
          assert.deepEqual(data.info.fb_uid, memcache_data.info.fb_uid);
          assert.deepEqual(data.info.gender, memcache_data.info.gender);
          assert.deepEqual(data.device.device_name, 'IPhone');
          done();
        }, function () {
          done('error');
        });
    });

    it('should not call cache.getVal if no fb user ' +
      'exists', function (done) {
      var update_cache = sinon.stub(db._cache, 'getValue', function () {
        update_cache.restore();
        return bluebird.reject();
      });

      tracker.on('query', function (query) {
        assert.deepEqual(query.method, 'first');
        query.response();
      });
      db.getUserRecord('aa', '1234')
        .then(function (data) {
          if (!data.device) {
            done();
          }
        }, function () {
          done('error');
        });
    });

    it('should return user data with device null if info and game is ' +
      'present and device is not for fb user', function (done) {
      db._setFunction('fetchUserByFBId', function () {
        return bluebird.resolve({
          info: '123'
        });
      });
      tracker.on('query', function (query) {
        assert.deepEqual(query.method, 'first');
        query.response();
      });
      db.getUserRecord(123, 'aaa')
        .then(function (user) {
          assert.strictEqual(undefined, user.device);
          done(user.info ? undefined : 'error');
        }, function () {
          done('error');
        });
    });
  });

  describe('insertBonusStats()', function () {
    it('should insert passed data and update cache', function (done) {
      var curr = 0,
        del_cache = sinon.stub(db._cache, 'delValue', function (id, data) {
          del_cache.restore();
          assert.strictEqual(id, 'fb_id');
          assert.strictEqual(data, 'bonus_levels');
          curr++;

          return bluebird.resolve();
        });

      db._setFunction('fetchAddBonusPlayed', function () {});

      tracker.on('query', function (query) {
        query.response([{
          affectedRows: 1
        }]);
      });
      db.insertBonusStats([{
          fb_uid: 'fb_id'
        }])
        .then(function (count) {
          assert.strictEqual(1, curr);
          done(count === 1 ? undefined : 'error');
        });
    });

    it('shouldnt update cache if not inserted', function (done) {
      var del_cache = sinon.stub(db._cache, 'delValue', function () {
        done('error');
      });

      tracker.on('query', function (query) {
        assert.strictEqual(true, query.sql.indexOf('insert ignore') > -1);
        query.response([{
          affectedRows: 0
        }]);
      });
      db.insertBonusStats([{
          fb_uid: 'fb_id'
        }])
        .then(function (count) {
          del_cache.restore();
          done(count === 0 ? undefined : 'error');
        });
    });
  });

  describe('getCompletedBonusLevels()', function () {
    it('should return data from cache', function (done) {
      var get_cache = sinon.stub(db._cache, 'getValue', function (id, data) {
        get_cache.restore();
        assert.strictEqual(id, 'fb_id');
        assert.strictEqual(data, 'bonus_levels');

        return bluebird.resolve(123);
      });

      db.getCompletedBonusLevels('fb_id')
        .then(function (val) {
          assert.strictEqual(val, 123);
          done();
        });
    });

    it('should fetch and return if cache miss', function (done) {
      var get_cache = sinon.stub(db._cache, 'getValue', function (id, data) {
        get_cache.restore();
        assert.strictEqual(id, 'fb_id');
        assert.strictEqual(data, 'bonus_levels');

        return bluebird.reject(null);
      });

      db.getCompletedBonusLevels('fb_id')
        .then(function (val) {
          assert.strictEqual(val, 123);
          done();
        });

      db._setFunction('fetchAddBonusPlayed', function () {
        return bluebird.resolve('xyz');
      });

      db.getCompletedBonusLevels('fb_id')
        .then(function (val) {
          done(val === 'xyz' ?undefined : 'error');
        });
    });
  });

  describe('fetchAddBonusPlayed()', function () {
    it('should fetch puzzle ids and cache and return', function (done) {
      var add_cache = sinon.stub(db._cache, 'addValue', function (id, data) {
        add_cache.restore();
        assert.strictEqual(id, 'fb_id');
        assert.deepEqual(data, [1,2,3]);
      });

      tracker.on('query', function (query) {
        query.response([{
            id: 1
          }, {
            id: 2
          }, {
            id: 3
          }]);
      });

      db._fetchAddBonusPlayed('fb_id')
        .then(function (data) {
          assert.deepEqual([1,2,3], data);
          done();
        });
    });
  });

  describe('fetchAndUpdateUser()', function () {
    it('should fetch user record and update cache for fb ' +
      'user', function (done) {
      var update_cache = sinon.stub(db._cache, 'addValue', function (id, data) {
        update_cache.restore();
        assert.strictEqual(data.info.gender, 'M');
        assert.strictEqual(data.game.go_red, 6);
        done(id === 'bbb' ? undefined : 'error');
      });

      db._setFunction('fetchUserByFBId', function () {
        return bluebird.resolve({
        info: {
          fb_uid: 'bbb',
          gender: 'M'
        },
        game: {
          fb_uid: 'bbb',
          go_red: 6
        }});
      });
      db._fetchAndUpdateUser('bbb');
    });

    it('shouldnt update cache if given fb user not present', function (done) {
      var update_cache = sinon.stub(db._cache, 'addValue', function () {
        done('error');
      });
      db._setFunction('fetchUserByFBId', function () {
        return bluebird.resolve();
      });
      db._fetchAndUpdateUser('bbb')
        .then(function () {
          update_cache.restore();
          done();
        });
    });
  });

  describe('fetchUserByFBId()', function () {
    it('should return null if no user', function (done) {

      tracker.on('query', function (query) {
        assert.deepEqual(query.method, 'first');
        query.response();
      });
      db._fetchUserByFBId('23')
        .then(done);
    });

    it('should return user data', function (done) {
      tracker.on('query', function (query) {
        assert.deepEqual(query.method, 'first');
        query.response({
          fb_uid: '123',
          gender: 'F',
          go_red: 8
        });
      });
      db._fetchUserByFBId('123')
        .then(function (user) {
          assert.strictEqual('F', user.info.gender);
          assert.strictEqual(8, user.game.go_red);
          done();
        });
    });
  });

  describe('updateUserCache()', function () {
    it('should update user cache with data from db', function (done) {
      var data = {
        info: {
          fb_uid: '223',
          gender: 'F'
        }},
        cache_stub_add = sinon.stub(db._cache, 'addValue', function () {
          cache_stub_add.restore();
          return bluebird.resolve();
        }),
        cache_stub_get = sinon.stub(db._cache, 'getValue', function () {
          cache_stub_get.restore();
          return bluebird.resolve(data);
        });

      db._setFunction('fetchUserByFBId', function () {
        return bluebird.resolve({
          info: '223'
        });
      });
      db._updateUserCache('223', null)
        .then(db._cache.getValue.bind(null,'223', 'user_record'))
          .then(function (rec) {
            assert.strictEqual(rec.info.gender, data.info.gender);
            done();
          }, done);
    });

    it('should update cache with passed value', function (done) {
      var data = {a: 2},
        cache_stub_add = sinon.stub(db._cache, 'addValue', function () {
          cache_stub_add.restore();
          return bluebird.resolve();
        }),
        cache_stub_get = sinon.stub(db._cache, 'getValue', function () {
          cache_stub_get.restore();
          return bluebird.resolve(data);
        });

      db._updateUserCache('fb_id', data)
        .then(db._cache.getValue.bind(null, 'fb_id', 'user_record'))
          .then(function (rec) {
            assert.deepEqual(rec, data);
            done();
          }, done);
    });
  });

  describe('checkUpdateScoreCache()', function () {

    it('should call cache update for fb user with param', function (done) {
      var cache_stub = sinon.stub(db._cache,
        'addOrUpdateScoreCache', function (param, update) {
        cache_stub.restore();
        assert.deepEqual({
          fb_uid: 'fb_id',
          progress: 100}, param);
        assert.strictEqual(2, update);
        return promise('success');
      });

      db._checkUpdateScoreCache({
          fb_uid: 'fb_id',
          progress: 100
        }, 2)
        .then(function (resp) {
          done(resp === 'success' ? undefined : 'error');
        }, done);
    });

    it('shouldnt call cache update for fb user if progress' +
      ' is not 100', function (done) {
        var cache_stub = sinon.stub(db._cache,
          'addOrUpdateScoreCache', function () {
          cache_stub.restore();
          return bluebird.reject('cache update invoked');
        });

        db._checkUpdateScoreCache({
            progress: 99,
            fb_uid: 'fb_id'
          }, 2)
          .then(function (resp) {
            done(resp);
          }, done);
      }
    );
  });
  describe('getUserGame()', function () {
    it('should return game data of non_fb user', function (done) {
      tracker.on('query', function (query) {
        assert.deepEqual(query.method, 'first');
        query.response({
          uid: 'abc',
          go_red: 3,
          gummy_popper: 5
        });
      });
      db.getUserGame('abc')
        .then(function (data) {
          done(data && data.go_red === 3 ? undefined : 'error');
        });
    });

    it('should return fb users game from cache', function (done) {
      var memcache_data = {
        info: {
          fb_uid: '1234',
          gender: 'M',
          first_name: 'Test',
          last_name: 'User'
        },
        game: {
          fb_uid: '1234',
          master_score: 200,
          lives: 2,
          double_trouble: 2,
          eight_way: 2,
          color_splash: 3,
          club_reds: 4,
          jelly_buster: 6,
          music_muted: 0,
          sound_muted: 0
        }
      },
      cache_stub = sinon.stub(db._cache, 'getValue', function () {
        cache_stub.restore();
        return bluebird.resolve(memcache_data);
      });
      db.getUserGame(null, '1234')
        .then(function (game) {
          assert.deepEqual(game, memcache_data.game);
          done();
        });
    });

    it('should return fb user game data from db for cache ' +
      'miss', function (done) {
      var cache_stub = sinon.stub(db._cache, 'addValue', function () {
        cache_stub.restore();
        bluebird.resolve();
      });

      db._setFunction('fetchUserByFBId', function () {
        return bluebird.resolve({
          game: {
            fb_uid: '3456',
            jelly_buster: 235
          }
        });
      });
      db.getUserGame(null, '3456')
        .then(function (data) {
          done(data.jelly_buster === 235 ? undefined : 'error');
        });
    });
  });

  describe('updateUserDevice()', function () {
    it('it should update proper device data', function (done) {
      tracker.on('query', function (query) {
        assert.deepEqual(query.method, 'update');
        assert.deepEqual(query.bindings.length, 6);
        done();
      });
      db.updateUserDevice({
        uid: 'abc',
        device_id: '2',
        current_design_version: '28',
        lives: 4,
        ip_address: 'lmn',
        rating: 5
      });
    });
  });

  describe('insertEachValue()', function () {
    var Knex_error = function (code) {
      this.code = code;
    };

    Knex_error.protoype = Error.prototype;

    it('should insert all records', function (done) {
      var data = [
        {fb_uid: '122'},
        {fb_uid: '234'}];

      tracker.on('query', function (query) {
        assert.deepEqual(query.method, 'insert');
        query.response([]);
      });
      db._insertEachValue(data, 'user_info')
        .then(function (duplicate) {
          assert.deepEqual(duplicate, []);
          done();
        });
    });

    it('should return entries with error code duplicate entry',
      function (done) {
      var data = [
        {fb_uid: '122'},
        {fb_uid: '122'}];

      tracker.on('query', function (query, step) {
        [
          function () {
            assert.deepEqual(query.method, 'insert');
            query.response(bluebird.resolve());
          },
          function () {
            throw new Knex_error('ER_DUP_ENTRY');
          }
        ][step - 1]();
      });
      db._insertEachValue(data, 'user_info')
        .then(function (duplicate) {
          assert.deepEqual(duplicate, [{fb_uid: '122'}]);
          done();
        });
    });

    it('should reject with other error apart from duplicate entry',
    function (done) {
      var data = [
        {fb_uid: '122'},
        {fb_uid: 9}];

      tracker.on('query', function (query, step) {
        [
          function () {
            assert.deepEqual(query.method, 'insert');
          },
          function () {
            throw new Knex_error('WRONG TYPE');
          }
        ][step - 1]();
      });
      db._insertEachValue(data, 'user_info')
        .catch(function () {
        done();
      });
    });
  });

  describe('deleteRecord()', function () {
    it('should delete record from table with passed ' +
      'property', function (done) {
        tracker.on('query', function (query) {
          assert.strictEqual(query.method, 'del');
          assert.deepEqual([1], query.bindings);
          assert.strictEqual('delete from `user_info` where `uid` = ?',
            query.sql);
          query.response(true);
        });

        db._deleteRecord('user_info', 'uid', 1)
          .then(function (res) {
            done(res === true ? undefined : 'error');
          });
      }
    );
  });

  describe('insertRecordsDupIgnore()', function () {
    var Knex_error = function (code) {
      this.code = code;
    };

    Knex_error.protoype = Error.prototype;

    it('should insert all records ignoring duplicate', function (done) {
      var data = [
        {fb_uid: '122'},
        {fb_uid: '234'}];

      tracker.on('query', function (query) {
        assert.deepEqual(query.method, 'insert');
        query.response([]);
      });
      db.insertRecordsDupIgnore(data, 'user_info')
        .then(function (duplicate) {
          assert.deepEqual(duplicate, true);
          done();
        });
    });

    it('should return duplicate entry with error duplicate code when there is' +
      'duplicate entry in duplicate ignore', function (done) {
      var data = [
        {fb_uid: '122'},
        {fb_uid: '122'}];

      tracker.on('query', function (query) {
        assert.deepEqual(query.method, 'insert');
        throw new Knex_error('ER_DUP_ENTRY');
      });
      db._setFunction('insertEachValue', function () {
        return bluebird.resolve([{fb_uid: '122'}]);
      });
      db.insertRecordsDupIgnore(data, 'user_info')
        .then(function (duplicate) {
          assert.deepEqual(duplicate, [{fb_uid: '122'}]);
          done();
        });
    });

    it('should reject with error duplicate code when there is' +
      'duplicate entry in ignore', function (done) {
      var data = [
        {fb_uid: '122'},
        {fb_uid: 9}];

      tracker.on('query', function (query) {
        assert.deepEqual(query.method, 'insert');
        throw new Knex_error('WRONG TYPE');
      });
      db.insertRecordsDupIgnore(data, 'user_info')
        .catch(function () {
          done();
        });
    });
  });

  describe('getUserByFBId()', function () {
    it('should return user by facebook uid', function (done) {
      var cache_stub = sinon.stub(db._cache, 'getValue', function () {
        cache_stub.restore();
        return bluebird.reject();
      });

      db._setFunction('fetchUserByFBId', function () {
        return bluebird.resolve({
          info: {
            fb_uid: 123456,
            gender: 'M',
            first_name: 'Test',
            last_name: 'User'
          },
          game: {
            fb_uid: 123456,
            go_red: 3
          }
        });
      });
      db.getUserByFBId(123456)
        .then(function (user) {
          var user_info = user.info,
            user_game = user.game;

          assert.strictEqual(user_info.fb_uid, 123456);
          assert.strictEqual(user_info.first_name, 'Test');
          assert.strictEqual(user_info.gender, 'M');
          assert.strictEqual(user_game.fb_uid, 123456);
          assert.strictEqual(user_game.go_red, 3);
          done();
        });
    });

    it('should return user by facebook uid from memcache', function (done) {
      var cache_stub = sinon.stub(db._cache, 'getValue', function () {
        cache_stub.restore();
        return bluebird.resolve({
          info: {
            fb_uid: '123456',
            gender: 'M',
            first_name: 'Test',
            last_name: 'User'
          },
          game: {
            fb_uid: '123456',
            gummy_popper: 5
          }
        });
      });

      db.getUserByFBId('123456')
        .then(function (user) {
          var user_info = user.info,
            user_game = user.game;

          assert.strictEqual(user_info.fb_uid, '123456');
          assert.strictEqual(user_info.first_name, 'Test');
          assert.strictEqual(user_info.gender, 'M');
          assert.strictEqual(user_game.fb_uid, '123456');
          assert.strictEqual(user_game.gummy_popper, 5);
          done();
        });
    });

    it('should return null as user doesn\'t exist', function (done) {
      var cache_stub = sinon.stub(db._cache, 'getValue', function () {
        cache_stub.restore();
        return bluebird.reject();
      });

      db._setFunction('fetchUserByFBId', function () {
        return bluebird.resolve(null);
      });
      db.getUserByFBId(123457)
        .then(function (res) {
          done(res);
        });
    });
  });

  describe('updateUserInfo()', function () {
    it('should fail if cache update fails', function (done) {
      var update_cache = sinon.stub(db._cache, 'addValue', function (fb_id) {
        assert.strictEqual('oo', fb_id);
        update_cache.restore();
        return bluebird.reject({code: 22});
      });

      tracker.on('query', function (query) {
        query.response({});
      });

      db.updateUserInfo({
        fb_uid: 'oo',
        first_name: 'Test_User'
      })
        .catch(function (data) {
          done(data.code === 22 ? undefined : 'error');
        });
    });

    it('should update user info table of given fb_uid', function (done) {
      tracker.on('query', function (query) {
        query.response({});
      });

      db._setFunction('updateUserCache', function () {
        return bluebird.resolve({
          info:{
            fb_uid: 'oo',
            last_name: 'Test_Name'
          }
        });
      });
      db.updateUserInfo({
        fb_uid: 'oo',
        dob: new Date('2010-10-10'),
        last_name: 'Test_Name'
      })
      .then(function (user) {
        done(user.info.last_name === 'Test_Name' ? undefined : 'error');
      }, function () {
        done('error');
      });
    });

    it('should update only data provided', function (done) {
      tracker.on('query', function (query) {
        query.response({});
      });

      db._setFunction('updateUserCache', function () {
        return bluebird.resolve({
          info:{
            fb_uid: 'oo',
            first_name: 'Test_FN'
          }
        });
      });

      db.updateUserInfo({
        fb_uid: 'oo',
        first_name: 'Test_FN'
      })
        .then(function (user) {
          var info = user.info;

          done(info.first_name === 'Test_FN' && !info.last_name ?
            undefined : 'error');
        }, function () {
          done('error');
        });
    });

    it('should update user data cache if fb id present', function (done) {
      var stub_cache = sinon.stub(db._cache, 'addValue', function (fb_id) {
        stub_cache.restore();
        done(fb_id === 'oo' ? undefined : 'error');
      });
      tracker.on('query', function (query) {
        query.response({});
      });
      db.updateUserInfo({
        fb_uid: 'oo',
        first_name: 'en_eu',
        dob: new Date('2010-10-10')
      });
    });
  });

  describe('updateUserGame()', function () {
    it('should fail if cache update fails for fb user', function (done) {
      var update_cache = sinon.stub(db._cache, 'addValue', function (fb_id) {
        assert.strictEqual('123', fb_id);
        update_cache.restore();
        return bluebird.rejected({code: 239});
      });

      tracker.on('query', function (query) {
        query.response({});
      });

      db.updateUserGame({
        fb_uid: '123'
      })
        .catch(function (data) {
          done(data.code === 239 ? undefined : 'error');
        });
    });

    it('should update user_game table for non fb user', function (done) {
      tracker.on('query', function (query) {
        if (query.method === 'update') {
          query.response({});
        } else {
          query.response({
            uid: 'pio',
            gummy_popper: 4,
            go_red: 5
          });
        }
      });
      db.updateUserGame({
        uid: 'pio',
        gummy_popper: 4,
        go_red: 5
      })
        .then(db._fetchFirstRow.bind(null, 'user_game', {uid: 'pio'}))
          .then(function (user_game) {
            done(user_game.gummy_popper === 4 ? undefined : 'error');
          }, function () {
          done('error');
        });
    });

    it('should throw error if given non existing col name as ' +
      'param', function (done) {
      tracker.on('query', function (query) {
        query.response('error');
      });

      db.updateUserGame({uid: 'pio', non_exists: 20})
        .then(function (res) {
          done(res === 'error'? undefined : 'error');
        });
    });

    it('should update user data cache for fb connected user', function (done) {
      var stub_cache = sinon.stub(db._cache, 'addValue', function (fb_id) {
        stub_cache.restore();
        done(fb_id === '123' ? undefined : 'error');
      });

      tracker.on('query', function (query) {
        query.response({});
      });

      db.updateUserGame({
        fb_uid: '123',
        go_red: 6
      });
    });
  });

  describe('getFriendHighscores()', function () {
    it('should get friend data in memcache', function (done) {
      var stub_cache = sinon.stub(db._cache, 'getFriendData', function () {
        stub_cache.restore();
        return bluebird.resolve({
          ms_data:[{
            fb_uid: '1234',
            ms_id: 'abcd',
            score: 4320
          }, {
            fb_uid: '1234',
            ms_id: 'efgh',
            score: 1430
          }]
        });
      });

      tracker.on('query', function (query) {
        query.response([{fb_uid:'1234'}]);
      });

      db.getFriendHighscores(['1234'])
      .then(function (ms_data) {
        assert.deepEqual(ms_data, [{
          fb_uid: '1234',
          ms_id: 'abcd',
          score: 4320
        }, {
          fb_uid: '1234',
          ms_id: 'efgh',
          score: 1430
        }]);
        done();
      });
    });

    it('should return empty object if passed ids are not present' +
      ' in db', function (done) {
      tracker.on('query', function (query) {
        query.response([]);
      });

      db.getFriendHighscores(['8910', '111213'])
        .then(function (data) {
          assert.deepEqual([], data);
          done();
        });
    });

    it('should return empty object for null uid', function (done) {
      db.getFriendHighscores(null)
        .then(function (data) {
          if (_.isEmpty(data)) {
            done();
          } else {
            done('data returned');
          }
        });
    });

    it('should get data for non cached user from db',
      function (done) {
      var stub_cache = sinon.stub(db._cache, 'getFriendData', function () {
        stub_cache.restore();
        return bluebird.resolve({});
      });

      tracker.on('query', function (query) {
        query.response([{fb_uid:'1234'}, {fb_uid: '2345'}]);
      });
      db._setFunction ('fetchFriendData', function () {
        return bluebird.resolve([
          {
            fb_uid: '1234',
            progress: 100,
            score: 200,
            stars: 3,
            ms_id: 'llli'
          },
          {
            fb_uid: '1234',
            progress: 100,
            score: 300,
            stars: 3,
            ms_id: 'lllj'
          },
          {
            fb_uid: '2345',
            progress: 100,
            score: 300,
            stars: 3,
            ms_id: 'llli'
          }
        ]);
      });
      db.getFriendHighscores(['1234', '2345'])
        .then(function (res) {
          done(res.length === 3 ? undefined : 'error');
        });
    });

    it('should create data from cache and db', function (done) {
      var stub_cache = sinon.stub(db._cache, 'getFriendData', function () {
        stub_cache.restore();
        return bluebird.resolve({
          ms_data: [
          {
            fb_uid: '1234',
            ms_id: 'abcd',
            score: 4320
          }, {
            fb_uid: '1234',
            ms_id: 'efgh',
            score: 1430
          },
          {
            fb_uid: '2345',
            ms_id: 'abcd',
            score: 3230
          }, {
            fb_uid: '2345',
            ms_id: 'hijk',
            score: 30230
          }],
          non_cached_ids: ['4567']
        });
      });

      tracker.on('query', function (query) {
        query.response([{fb_uid:'1234'}, {fb_uid: '2345'}, {fb_uid:'3456'}]);
      });
      db._setFunction ('fetchFriendData', function (ms_data, non_cached_ids) {
        assert.deepEqual(ms_data,[{
          fb_uid: '1234',
          ms_id: 'abcd',
          score: 4320
        },
        {
          fb_uid: '1234',
          ms_id: 'efgh',
          score: 1430
        },
        {
          fb_uid: '2345',
          ms_id: 'abcd',
          score: 3230
        },
        {
          fb_uid: '2345',
          ms_id: 'hijk',
          score: 30230
        }]);

        assert.deepEqual(non_cached_ids, ['4567']);
        bluebird.resolve();
      });
      db.getFriendHighscores(['1234', '2345', '3456'])
        .then(done);
    });
  });

  describe('fetchFriendData()', function () {
    it('should not add to data if no non cached ids passed ', function (done) {
      db._fetchFriendData([
        {
          fb_uid: '1234',
          score: 4320,
          ms_id: 'abcd'
        }, {
          fb_uid: '2345',
          score: 3230,
          ms_id: 'abcd'
        }
      ], [], [])
        .then(function (ms_data) {
          assert.deepEqual(ms_data, [
            {
              fb_uid: '1234',
              score: 4320,
              ms_id: 'abcd'
            }, {
              fb_uid: '2345',
              score: 3230,
              ms_id: 'abcd'
            }
          ]);
          done();
        });
    });

    it('should return null data if record ' +
      'doesnt exist in user_gs_stats', function (done) {
      tracker.on('query', function (query) {
        query.response([]);
      });
      db._fetchFriendData([], ['1234', '2345'], [])
        .then(function (ms_data) {
          assert.deepEqual(ms_data, []);
          done();
        });
    });

    it('should return data if one record ' +
      'doesnt exist in user_gs_stats', function (done) {
      tracker.on('query', function (query) {
        query.response([{
          fb_uid: '1234',
          ms_id: 'abcd',
          progress: 100,
          score: 3230
        }, {
          fb_uid: '1234',
          ms_id: 'hijk',
          progress: 100,
          score: 30230
        }]);
      });
      db._fetchFriendData([], ['1234', '3456', '4567'])
        .then(function (ms_data) {
          assert.deepEqual(ms_data, [{
            fb_uid: '1234',
            ms_id: 'abcd',
            score: 3230,
            progress: 100
          }, {
            fb_uid: '1234',
            ms_id: 'hijk',
            score: 30230,
            progress: 100
          }]);
          done();
        });
    });

    it('should return data if called correctly', function (done) {
      tracker.on('query', function (query) {
        query.response([
          {
            fb_uid: '1234',
            ms_id: 'abcd',
            score: 4320,
            stars: 0,
            progress: 100
          },
          {
            fb_uid: '1234',
            ms_id: 'efgh',
            score: 1430,
            stars: 0,
            progress: 100
          },
          {
            fb_uid: '2345',
            ms_id: 'abcd',
            score: 3230,
            stars: 0,
            progress: 100
          },
          {
            fb_uid: '2345',
            ms_id: 'hijk',
            score: 30230,
            stars: 0,
            progress: 100
          }
        ]);
      });
      db._fetchFriendData([], ['1234', '2345'])
        .then(function (ms_data) {
          assert.deepEqual(ms_data, [
            {
              fb_uid: '1234',
              ms_id: 'abcd',
              score: 4320,
              stars: 0,
              progress: 100
            },
            {
              fb_uid: '1234',
              ms_id: 'efgh',
              score: 1430,
              stars: 0,
              progress: 100
            },
            {
              fb_uid: '2345',
              ms_id: 'abcd',
              score: 3230,
              stars: 0,
              progress: 100
            },
            {
              fb_uid: '2345',
              ms_id: 'hijk',
              score: 30230,
              stars: 0,
              progress: 100
            }
          ]);
          done();
        });
    });

    it('should return records with progress is 100', function (done) {
      tracker.on('query', function (query) {
        query.response([{
          fb_uid: '1234',
          ms_id: 'abc',
          score: 2000,
          stars: 0,
          progress: 100
        }]);
      });
      db._fetchFriendData([], ['1234'])
        .then(function (ms_data) {
          assert.strictEqual(1, ms_data.length);
          assert.deepEqual(ms_data, [{
            fb_uid: '1234',
            ms_id: 'abc',
            score: 2000,
            stars: 0,
            progress: 100
          }]);
          done();
        });
    });
  });

  describe('addOrUpdateMaxPuzzle()', function () {
    var puzzle_max = {
      fb_uid: '1234',
      ms_id: 'def'
    };

    it('should create new record for that user puzzle if not' +
      ' exists', function (done) {
        tracker.on('query', function (query, step) {
          [
            function () {
              query.response();
            },
            function () {
              assert.strictEqual(query.method, 'insert');
              assert.strictEqual(true, query.sql
                .indexOf('user_gs_stats') >= 0 ? true: false);
              assert.strictEqual(query.bindings.length,
                _.keys(puzzle_max).length);
              done();
            }
          ][step - 1]();
        });

        db.addOrUpdateMaxPuzzle(puzzle_max);
      });

    it('should update existing user puzzle with max from ' +
      'passed data', function (done) {
        tracker.on('query', function (query, step) {
          [
            function () {
              query.response(puzzle_max);
            },
            function () {
              assert.strictEqual(query.method, 'update');
              assert.strictEqual(true, query.sql
                .indexOf('user_gs_stats') >= 0 ? true: false);
              query.response();
            }
          ][step - 1]();
        });

        db._setFunction('checkUpdateScoreCache', function () {
          return bluebird.resolve();
        });

        db.addOrUpdateMaxPuzzle(puzzle_max)
          .then(function (data) {
            assert.deepEqual(data, puzzle_max);
            done();
          });
      });

    it('should fail if unknown column data is passed', function (done) {
      tracker.on('query', function () {
        throw new Error();
      });

      db.addOrUpdateMaxPuzzle(puzzle_max)
        .then(function () {
          done('error');
        })
        .catch(function () {
          done();
        });
    });

    it('should call getCompletedUserPuzzleData if memcached ' +
      'is empty', function (done) {
      var cache_getUser = db.getCompletedUserPuzzleData;

      tracker.on('query', function (query, step) {
          [
            function () {
              query.response();
            },
            function () {
              assert.strictEqual(query.method, 'insert');
              assert.strictEqual(true, query.sql
                .indexOf('user_gs_stats') >= 0 ? true: false);
              query.response();
            }
          ][step - 1]();
        });

      db._setFunction('checkUpdateScoreCache', function () {
        return bluebird.reject('not_found');
      });

      db.getCompletedUserPuzzleData = function (fb_uid) {
        db.getCompletedUserPuzzleData = cache_getUser;
        assert.strictEqual('1234', fb_uid);
        done();
      };
      db.addOrUpdateMaxPuzzle(puzzle_max);
    });
  });

  describe('getCompletedUserPuzzleData()', function () {
    var fb_uid = 'test_fb_id';

    it('should give score value for completed puzzles', function (done) {
      tracker.on('query', function (query) {
        query.response([
          {
            fb_uid: fb_uid,
            ms_id: 'abcd',
            score: 1234,
            stars: 2,
            progress: 100
          }, {
            fb_uid: fb_uid,
            ms_id: 'efgh',
            score: 2345,
            stars: 3,
            progress: 100
          }
        ]);
      });
      db.getCompletedUserPuzzleData(fb_uid)
        .then(function (data) {
          assert.strictEqual(2, data.length);
          assert.strictEqual(data[0].ms_id, 'abcd');
          assert.strictEqual(data[0].score, 1234);
          assert.strictEqual(data[0].stars, 2);
          assert.strictEqual(data[1].ms_id, 'efgh');
          assert.strictEqual(data[1].score, 2345);
          assert.strictEqual(data[1].stars, 3);
          done();
        });
    });

    it('should get scores from memcache', function (done) {
      var cache_getValue = sinon.stub(db._cache, 'getValue', function () {
        cache_getValue.restore();
        return promise([
          {
            fb_uid: fb_uid,
            ms_id: 'abcd',
            score: 1234,
            stars: 2,
            progress: 100
          }, {
            fb_uid: fb_uid,
            ms_id: 'efgh',
            score: 2345,
            stars: 3,
            progress: 100
          }
        ]);
      });

      db.getCompletedUserPuzzleData(fb_uid)
        .then(function (data) {
          assert.strictEqual(data[0].ms_id, 'abcd');
          assert.strictEqual(data[0].score, 1234);
          assert.strictEqual(data[0].stars, 2);
          assert.strictEqual(data[1].ms_id, 'efgh');
          assert.strictEqual(data[1].score, 2345);
          assert.strictEqual(data[1].stars, 3);
          done();
        });
    });

    it('should get score when not cached', function (done) {
      var cache_getValue = sinon.stub(db._cache, 'getValue', function () {
          cache_getValue.restore();
          return bluebird.reject();
        }),
        cache_addValue = sinon.stub(db._cache, 'addValue', function () {
          cache_addValue.restore();
          return bluebird.resolve();
        });

      db._setFunction('fetchCompletedUserPuzzleData', function (fb_id) {
        assert.strictEqual(fb_id, 'test_fb_id');
        return bluebird.resolve([
          {
            fb_uid: 'test_fb_id',
            ms_id: 'abcd',
            score: 1234,
            stars: 2,
            progress: 100
          }, {
            fb_uid: 'test_fb_id',
            ms_id: 'efgh',
            score: 2345,
            stars: 3,
            progress: 100
          }
        ]);
      });

      db.getCompletedUserPuzzleData(fb_uid)
        .then(function (data) {
          assert.strictEqual(2, data.length);
          assert.strictEqual(data[0].ms_id, 'abcd');
          assert.strictEqual(data[0].score, 1234);
          assert.strictEqual(data[0].stars, 2);
          assert.strictEqual(data[1].ms_id, 'efgh');
          assert.strictEqual(data[1].score, 2345);
          assert.strictEqual(data[1].stars, 3);
          done();
        });
    });
  });

  describe('insertPuzzleAll', function () {
    it('should invoke inserting to table', function () {
      tracker.on('query', function (query) {
        assert.strictEqual(query.method, 'insert');
        assert.strictEqual(true, query.sql
          .indexOf('user_gs_stats_all') >= 0 ? true: false);
        query.response({});
      });

      db.insertPuzzleAll({});
    });
  });

  describe('insertKochavaEvent()', function () {
    it('should populate kochava events', function (done) {
      tracker.on('query', function (query) {
        assert.strictEqual(query.method, 'insert');
        assert.strictEqual(true, query.sql
          .indexOf('kochava_events') >= 0 ? true: false);
        query.response({});
        done();
      });

      db.insertKochavaEvent({});
    });
  });

  describe('insertUser()', function () {
    it('should populate useri_game and user_device', function (done) {
      tracker.on('query', function (query, step) {
        [
          function () {
            assert.strictEqual(query.method, 'insert');
            assert.strictEqual(true, query.sql
              .indexOf('user_device') >= 0 ? true: false);
            query.response({});
          },
          function () {
            assert.strictEqual(query.method, 'insert');
            assert.strictEqual(true, query.sql
              .indexOf('user_game') >= 0 ? true: false);
            done();
            query.response({});
          }
        ][step - 1]();
      });

      db.insertUser({}, {});
    });
  });

  describe('insertFbUser()', function () {
    it('should insert user data and fb tables', function (done) {
      tracker.on('query', function (query, step) {
        [
          function () {
            assert.strictEqual(query.method, 'insert');
            assert.strictEqual(true, query.sql
              .indexOf('user_info') >= 0 ? true: false);
            query.response({});
          },
          function () {
            assert.strictEqual(query.method, 'insert');
            assert.strictEqual(true, query.sql
              .indexOf('fb_user_game') >= 0 ? true: false);
            done();
            query.response({});
          }
        ][step - 1]();
      });

      db._setFunction('updateUserCache', function () {
        return bluebird.resolve({});
      });

      db.insertFbUser({}, {});
    });
  });

  describe('insertSale()', function () {
    it('should insert sale data into db', function () {
      tracker.on('query', function (query) {
        assert.strictEqual(query.method, 'raw');
        query.response({});
      });

      db.insertSale({});
    });
    it('should delete cache value for platform/s', function () {
      var cache_stub_delete;
      tracker.on('query', function (query) {
        assert.strictEqual(query.method, 'raw');
        query.response({});
      });
      cache_stub_delete = sinon.stub(db._cache, 'delValue', function (data) {
        assert.strictEqual(data, 'ios');
        cache_stub_delete.restore();
        return bluebird.resolve();
      });

      db.insertSale({platform: ['ios']});
    });
  });

  describe('savePurchases', function () {
    it('should invoke inserting to table', function () {
      tracker.on('query', function (query) {
        assert.strictEqual(query.method, 'insert');
        assert.strictEqual(true, query.sql
          .indexOf('inventory_purchase') >= 0 ? true: false);
        query.response({});
      });

      db.savePurchases({});
    });
  });

  describe('saveCredits', function () {
    it('should invoke inserting to table', function () {
      tracker.on('query', function (query) {
        assert.strictEqual(query.method, 'insert');
        assert.strictEqual(true, query.sql
          .indexOf('cash_purchase') >= 0 ? true: false);
        query.response({});
      });

      db.saveCredits({});
    });
  });

  describe('trackFreeGoods', function () {
    it('should invoke inserting to table', function () {
      tracker.on('query', function (query) {
        assert.strictEqual(query.method, 'insert');
        assert.strictEqual(true, query.sql
          .indexOf('track_free_goods') >= 0 ? true: false);
        query.response({});
      });

      db.trackFreeGoods({});
    });

    it('should push to cache if there is fb_uid', function (done) {
      var data = {},
        fb_uid = 1234;

      tracker.on('query', function (query) {
        assert.strictEqual(query.method, 'insert');
        assert.strictEqual(true, query.sql
          .indexOf('track_free_goods') >= 0 ? true: false);
        query.response({});
      });

      var insert_req = sinon.stub(db._cache, 'push', function () {
        insert_req.restore();
        done();
      });

      db.trackFreeGoods(data, fb_uid);
    });
  });

  describe('insertRequests', function () {
    it('should insert array of values into requestsSent', function (done) {
      var
        req_obj = [
          {request_id: 1, sender: 8, receiver: 4, type: 'gift'},
          {request_id: 2, sender: 7, receiver: 4, type: 'gift'}
        ],
        insert_req = sinon.stub(db._cache, 'insertRequests', function (req) {
          insert_req.restore();
          assert.deepEqual(req, req_obj);
          done();
          return promise({});
        });

      tracker.on('query', function (query) {
        assert.strictEqual(query.method, 'insert');
        query.response();
      });

      db.insertRequests([
        {request_id: 1, sender: 8, receiver: 4, type: 'gift'},
        {request_id: 2, sender: 7, receiver: 4, type: 'gift'}
      ]);
    });
  });

  describe('updateRequest()', function () {
    it('should move the request to processed table', function (done) {
      var
        req_obj = {
          request_id: 1,
          sender: 2,
          receiver: 3,
          type: 'gift',
          action: 'accept'
        },
        cache_del = sinon.stub(db._cache, 'deleteRequest', function () {
          cache_del.restore();
          return promise(req_obj);
        });

      tracker.on('query', function (query, step) {
        [
          function () {
            assert.strictEqual(query.method, 'first');
            query.response({});
          },
          function () {
            query.response();
          },
          function () {
            assert.strictEqual(query.method, 'del');
            query.response(req_obj);
          }
        ][step - 1]();
      });

      db.updateRequest(req_obj)
        .then(function (data) {
          assert.deepEqual(req_obj, data);
          done();
        });
    });
  });

  describe('getRequests', function () {
    var cache_getValue, cache_addValue;

    beforeEach(function () {
      cache_getValue = sinon.stub(db._cache, 'getValue');
      cache_addValue = sinon.stub(db._cache, 'addValue');
    });

    afterEach(function () {
      cache_getValue.restore();
      cache_addValue.restore();
    });

    it('should be able to get data from memcache if available',
      function (done) {
      var req_obj = [
        {request_id: 1, sender: 8, receiver: 4, type: 'gift'},
        {request_id: 2, sender: 7, receiver: 4, type: 'gift'}
      ];

      cache_getValue
        .withArgs(4, 'requests')
        .returns(promise(req_obj));

      db.getRequests(4)
      .then(function (data) {
        assert.deepEqual(data, req_obj);
        done(data.length === 2 ? null : 'Error: wrong data');
      });
    });

    it('should invoke fetchRequest if data not in memcache', function (done) {
      var req_obj = [
          {request_id: 1, sender: 8, receiver: 4, type: 'gift'},
          {request_id: 2, sender: 7, receiver: 4, type: 'gift'}
        ];

      tracker.on('query', function (query) {
        assert.deepEqual(query.bindings, [4, 'invite']);
        query.response(req_obj);
      });

      cache_getValue.returns(promise());

      db.getRequests(4)
      .then(function (data) {
        assert.deepEqual(data, req_obj);
        done(data.length === 2 ? null : 'Error: wrong data');
      });
    });

    it('should add to cache if data not in memcache', function (done) {
      var req_obj = [
          {request_id: 1, sender: 8, receiver: 4, type: 'gift'},
          {request_id: 2, sender: 7, receiver: 4, type: 'gift'}
        ],
        cache;

      tracker.on('query', function (query) {
        assert.deepEqual(query.bindings, [4, 'invite']);
        query.response(req_obj);
      });

      cache_getValue.returns(promise());
      cache_addValue.restore();

      cache = sinon.stub(db._cache, 'addValue', function (key, value, type) {
        assert.strictEqual(4, key);
        assert.deepEqual(value, req_obj);
        assert.strictEqual(type, 'requests');
        done();
        return promise({});
      });

      db.getRequests(4)
      .then(function (data) {
        assert.deepEqual(data, req_obj);
      });
    });
  });

  describe('removeOldRequests', function () {
    it('should delete requests from same user', function (done) {
      var ctr = 0,
        req_obj = [
          {request_id: 1, sender: 4, receiver: 8, type: 'gift', fb_action: 't'},
          {request_id: 2, sender: 4, receiver: 8, type: 'gift', fb_action: 't'},
          {request_id: 3, sender: 4, receiver: 8, type: 'gift', fb_action: 'y'}
        ],
        cache = sinon.stub(db, 'updateRequest', function (request) {
          assert.strictEqual(request.action, 'delete');

          if (++ctr === req_obj.length) {
            done();
            cache.restore();
          }
        });

      tracker.on('query', function (query) {
        assert.strictEqual(query.method, 'select');
        query.response(req_obj);
      });

      db.removeOldRequests({
        request_id: 3,
        sender: 4,
        receiver: 8,
        type: 'gift',
        fb_action: 't'
      })
      .catch(function (err) {
        done(err);
      });
    });

    it('should return empty array if no requests with same sender' +
      ' and receiver present', function (done) {
      tracker.on('query', function (query) {
        assert.strictEqual(query.method, 'select');
        query.response([]);
      });

      db.removeOldRequests({
        request_id: 3,
        sender: 4,
        receiver: 8,
        type: 'gift',
        fb_action: 't'
      })
      .then(function (data) {
        assert.deepEqual([], data);
        done();
      })
      .catch(function () {
        done('err');
      });
    });

    it('should return all the deleted requests if requests with same sender' +
      ' and receiver present', function (done) {
      var ctr = 0,
        req_obj = [
          {request_id: 1, sender: 4, receiver: 8, type: 'gift', fb_action: 't'},
          {request_id: 2, sender: 4, receiver: 8, type: 'gift', fb_action: 't'},
          {request_id: 3, sender: 4, receiver: 8, type: 'gift', fb_action: 'y'}
        ],
        cache = sinon.stub(db, 'updateRequest', function (request) {
          assert.strictEqual(request.action, 'delete');

          if (++ctr === req_obj.length) {
            cache.restore();
          }
        });

      tracker.on('query', function (query) {
        assert.strictEqual(query.method, 'select');
        query.response(req_obj);
      });

      db.removeOldRequests({
        request_id: 3,
        sender: 4,
        receiver: 8,
        type: 'gift',
        fb_action: 't'
      })
      .then(function (requests) {
        assert.strictEqual(_.isEqual(requests, req_obj), true);
        done();
      })
      .catch(function (err) {
        done(err);
      });
    });
  });

  describe('insertFbConnect()', function () {
    it('should insert fb_connect record for uid', function (done) {
      var obj = {
          key: 'test'
        };

      tracker.on('query', function (query) {
        assert.strictEqual(query.method, 'insert');
        assert.deepEqual(query.bindings, ['test']);
        query.response();
        done();
      });

      db.insertFbConnect(obj);
    });

    it('should clean powerup cache', function (done) {
      var obj = {
          key: 'test'
        },
        del_cache = sinon.stub(db._cache, 'delValue', function () {
          del_cache.restore();
          done();
        });

      tracker.on('query', function (query) {
        assert.strictEqual(query.method, 'insert');
        assert.deepEqual(query.bindings, ['test']);
        query.response();
      });

      db.insertFbConnect(obj);
    });
  });

  describe('updateFbConnect()', function () {
    it('should update disconnect time of last open fb connect for that uid',
      function (done) {
        tracker.on('query', function (query) {
          assert.strictEqual(query.method, 'update');
          query.response({
            uid: 'abc',
            fb_uid: '123',
            connect_time: '2016-04-10'
          });
        });

        db.updateFbConnect('abc', '123', new Date('2016-04-10'))
          .then(function (res) {
            done(res.connect_time === '2016-04-10' ? undefined : 'error');
          });
      });
  });

  describe('getVersionPatch()', function () {
    var cache_push, cache_getValue, cache_addValue,
      cache_fetchVersionPatch;

    beforeEach(function () {
      cache_push = sinon.stub(db._cache, 'push');
      cache_getValue = sinon.stub(db._cache, 'getValue');
      cache_addValue = sinon.stub(db._cache, 'addValue');
      cache_fetchVersionPatch = sinon.stub(db, 'fetchVersionPatch');
    });

    afterEach(function () {
      cache_push.restore();
      cache_getValue.restore();
      cache_addValue.restore();
      cache_fetchVersionPatch.restore();
    });

    it('Should fetch version data if it exists in memcache', function (done) {
      var memcache_data = {
          version: '4',
          app_version: '0.50',
          1: {
            id: 'first'
          },
          2: {
            id: 'second'
          },
          3: {
            id: 'third'
          }
        };

      cache_getValue.returns(promise(memcache_data));

      db.getVersionPatch('0.50', '3')
        .then(function (data) {
          assert.deepEqual(data, memcache_data);
          done();
        })
        .catch(done);
    });

    it('should get data from file if memcache doesnt have id', function (done) {
      var patch_data = {
          version: '4',
          app_version: '0.50',
          1: {
            id: 'first'
          },
          2: {
            id: 'second'
          },
          3: {
            id: 'third'
          }
        };

      cache_getValue.returns(promise());
      cache_fetchVersionPatch.returns(promise(patch_data));

      db.getVersionPatch('0.50', '3')
        .then(function (data) {
          assert.deepEqual(data, patch_data);
          done();
        });
    });

    it('should get false if memcache doesnt have id', function (done) {
      cache_getValue.returns(promise());
      cache_fetchVersionPatch.returns(promise(false));

      db.getVersionPatch('0.50', '3')
        .then(function (data) {
          assert.strictEqual(data, false);
          done();
        });
    });
  });

  describe('fetchVersionPatch()', function () {
    it('should check if error is ENOENT', function (done) {
      var cache_readFile = sinon.stub(fs, 'readFileAsync', function (location) {
        var err = {
          code: 'ENOENT'
        };

        cache_readFile.restore();
        assert.strictEqual(config.puzzle_path + '0.52.json',
          location);

        return bluebird.reject(err);
      });

      db.fetchVersionPatch('0.52')
        .then(function (data) {
          assert.strictEqual('EMPTY', data);
          done();
        }, function (err) {
          done(err);
        });
    });

    it('should reject for other error', function (done) {
      var cache_readFile = sinon.stub(fs, 'readFileAsync', function (location) {
        var err = {
          code: 'WOH'
        };

        cache_readFile.restore();
        assert.strictEqual(config.puzzle_path + '0.52.json',
          location);

        return bluebird.reject(err);
      });

      db.fetchVersionPatch('0.52')
        .then(function (data) {
          assert.strictEqual('EMPTY', data);
          done();
        }, function (err) {
          done(err.code === 'WOH' ? undefined : 'err');
        });
    });

    it('should return correct data', function (done) {
      var cache_readFile = sinon.stub(fs, 'readFileAsync', function (location) {
        var data = '{"test": "test1"}';

        cache_readFile.restore();
        assert.strictEqual(config.puzzle_path + '0.52.json',
          location);

        return promise(data);
      });

      db.fetchVersionPatch('0.52')
        .then(function (data) {
          assert.strictEqual(data.test, 'test1');
          done();
        }, function (err) {
          done(err);
        });
    });

    it('should add to cache for correct data', function (done) {
      var test = '{"test": "test1"}',
        cache_readFile = sinon.stub(fs, 'readFileAsync',
        function (location) {
          cache_readFile.restore();
          assert.strictEqual(config.puzzle_path + '0.52.json',
            location);

          return promise(test);
        }),
        cache_add = sinon.stub(db._cache, 'addValue',
          function (app_version, data) {
            cache_add.restore();
            assert.strictEqual(app_version, '0.52');
            assert.deepEqual(data, JSON.parse(test));
          });

      db.fetchVersionPatch('0.52')
        .then(function (data) {
          assert.strictEqual(data.test, 'test1');
          done();
        }, function (err) {
          done(err);
        });
    });

    it('should update design_patches for correct data', function (done) {
      var test = '{"test": "test1"}',
        cache_readFile = sinon.stub(fs, 'readFileAsync',
        function (location) {
          cache_readFile.restore();
          assert.strictEqual(config.puzzle_path + '0.52.json',
            location);

          return promise(test);
        }),
        cache_push = sinon.stub(db._cache, 'push',
          function (dp, app_version) {
            cache_push.restore();
            assert.strictEqual('design_patches', dp);
            assert.strictEqual(app_version, '0.52');
          });

      db.fetchVersionPatch('0.52')
        .then(function (data) {
          assert.strictEqual(data.test, 'test1');
          done();
        }, function (err) {
          done(err);
        });
    });

    it('should reject for parse error', function (done) {
      var cache_readFile = sinon.stub(fs, 'readFileAsync', function (location) {
        var data = '"test": "test1"';

        cache_readFile.restore();
        assert.strictEqual(config.puzzle_path + '0.52.json',
          location);

        return promise(data);
      });

      db.fetchVersionPatch('0.52')
        .then(function () {
          done('err');
        }, function () {
          done();
        });
    });
  });
  describe('fetchCurrentSale()', function () {
    it('should return only upcoming and running sale data from db' +
      'if not available in cache', function () {
      var response = [{id: 123,
        name: 'Most amazing sale',
        platform: 'android',
        start: '2015-09-09 00:00:00',
        end: '2022-09-09 00:00:00',
        multiplier: 2}];
      tracker.on('query', function (query) {
        query.response(response);
      });
      db.fetchCurrentSale('android')
      .then(function (data) {
        assert.strictEqual(data, response);
      });
    });
  });
  describe('getCurrentSale()', function () {
    it('should return sale data in cache' +
    'and within data range', function (done) {
      var response = [{id: 123,
        name: 'Most amazing sale',
        platform: 'android',
        start: '2015-09-09 00:00:00',
        end: '2022-09-09 00:00:00',
        multiplier: 2}],
        cache_getValue = sinon.stub(db._cache, 'getValue', function () {
        cache_getValue.restore();
        return promise(response);
      });
      db.getCurrentSale('android').then(function (res) {
        assert.strictEqual(true, _.isEqual(res, response[0]));
        done();
      });
    });
    it('should return undefined in cache' +
    'and not within data range', function (done) {
      var response = [{id: 123,
        name: 'Most amazing sale',
        platform: 'android',
        start: '1945-09-09 00:00:00',
        end: '2007-09-09 00:00:00',
        multiplier: 2}],
        cache_getValue = sinon.stub(db._cache, 'getValue', function () {
        cache_getValue.restore();
        return promise(response);
      });
      db.getCurrentSale('android').then(function (res) {
        assert.strictEqual(undefined, res);
        done();
      });
    });
  });
  describe('getCurrentFbConnect()', function () {
    it('should return currrently open fb_connect for uid', function (done) {
      tracker.on('query', function (query) {
        assert.strictEqual(query.bindings.length, 3);
        assert.strictEqual(query.method, 'first');
        query.response({
          uid: 'abc',
          fb_uid: '123',
          connect_time: '2016-04-10'
        });
      });

      db.getCurrentFbConnect('123', 'abc')
        .then(function (res) {
          assert.strictEqual(res.disconnect_time, undefined);
          done(res.connect_time === '2016-04-10' ? undefined : 'error');
        });
    });

    it('should return currrently open fb_connect for fb_id', function (done) {
      tracker.on('query', function (query) {
        assert.strictEqual(query.bindings.length, 2);
        assert.strictEqual(query.method, 'first');
        query.response({
          uid: 'abc',
          fb_uid: '123',
          connect_time: '2016-04-10'
        });
      });

      db.getCurrentFbConnect('123')
        .then(function (res) {
          assert.strictEqual(res.disconnect_time, undefined);
          done(res.connect_time === '2016-04-10' ? undefined : 'error');
        });
    });

  });

  describe('getLastFbConnect()', function () {
    it('should return last disconected fb_connect for fb_id', function (done) {
      tracker.on('query', function (query) {
        assert.strictEqual(query.method, 'first');
        query.response({
          uid: 'abc',
          fb_uid: '123',
          connect_time: '2016-04-10',
          disconnect_time: '2016-04-11'
        });
      });

      db.getLastFbConnect('123')
        .then(function (res) {
          assert.strictEqual(res.disconnect_time, '2016-04-11');
          done(res.connect_time === '2016-04-10' ? undefined : 'error');
        });
    });
  });

  describe('getNonTrackedDevicePuzzles()', function () {
    var gs_stats_all = [{
        uid: 'abc',
        instance_id: 123456,
        ms_id: 'rrr',
        date_time: '2016-03-25'
      }, {
        uid: 'abc',
        instance_id: 9876,
        ms_id: 'lll',
        date_time: '2016-03-28'
      }];

    it('should return all game data if no closed fb_connect ' +
      'available', function (done) {
        tracker.on('query', function (query, step) {
          [
            function () {
              query.response();
            },
            function () {
              query.response(gs_stats_all);
            }
          ][step - 1]();
        });

        db.getNonTrackedDevicePuzzles('abc', '1234')
          .then(function (res) {
            done(res.length === 2 ? undefined : 'error');
          });
      }
    );

    it('should return game data after last fb disconnect time if closed ' +
      'fb_connect is available', function (done) {
        tracker.on('query', function (query, step) {
          [
            function () {
              assert.strictEqual('first', query.method);
              query.response({
                uid: 'abc',
                fb_uid: '1234',
                connect_time: '2016-03-24',
                disconnect_time: '2016-03-26'
              });
            },
            function () {
              assert.strictEqual(true, query.sql
                .indexOf('date_time') >= 0 ? true : false);
              query.response([gs_stats_all[1]]);
            }
          ][step - 1]();
        });

        db.getNonTrackedDevicePuzzles('abc', '1234')
          .then(function (res) {
            assert.strictEqual(res.length, 1);
            done(res[0].ms_id === 'lll' ? undefined : 'error');
          });
      }
    );
  });

  describe('getLastDisconnectTime', function () {
    it('should get last disconnect time', function (done) {
      tracker.on('query', function (query) {
        assert.strictEqual('first', query.method);
        query.response({
          time: '2014-08-10 00:00:05'
        });
      });
      db.getLastDisconnectTime('uid', '2008-08-10 00:00:00')
        .then (function (resp) {
          assert.strictEqual(resp, '2014-08-10 00:00:05');
          done();
        });
    });

    it('should get return 0 if last disconnect time is null', function (done) {
      tracker.on('query', function (query) {
        query.response({
          time: null
        });
      });
      db.getLastDisconnectTime('uid', '2008-08-10 00:00:00')
        .then (function (resp) {
          assert.strictEqual(resp, 0);
          done();
        });
    });
  });

  describe('getUsedCurrencyFrom', function () {
    var transactions = [{
          free_currency: 0,
          paid_currency: 10
        }];

    it('should get free currency, paid curency and quantity', function (done) {
      tracker.on('query', function (query) {
        query.response(transactions);
      });
      db.getUsedCurrencyFrom('uid', 'time')
        .then(function (resp) {
          assert.deepEqual(resp, transactions);
          done();
        });
    });
  });

  describe('getPurchasesFrom', function () {
    it('should get purchases', function (done) {
      tracker.on('query', function (query) {
        if (query.step === 1) {
          query.response([{
            currency_bundle: 'cash1',
            sale_id: 1
          }]);
        } else {
          query.response([{
            id: 1,
            multiplier: 2
          }]);
        }
      });
      db.getPurchasesFrom('uid', 'time')
        .then(function (resp) {
          assert.deepEqual(resp, [{currency_bundle: 'cash1', sale_id: 1,
            multiplier: 2}]);
          done();
        });
    });
    it('should not query db if no unique sales', function (done) {
      tracker.on('query', function (query) {
        if (query.step === 1) {
          query.response([{
            currency_bundle: 'cash1',
            sale_id: undefined
          }]);
        } else if (query.step === 2) {
          done('err');
        }
      });
      db.getPurchasesFrom('uid', 'time')
        .then(function () {
          done();
        });
    });
  });

  describe('getFreeCurrencyFrom', function () {
    it('should get free currency', function (done) {
      tracker.on('query', function (query) {
        query.response({
          amount: 10
        });
      });
      db.getFreeCurrencyFrom('uid', 'time')
        .then(function (resp) {
          assert.deepEqual(resp, 10);
          done();
        });
    });
    it('should get return 0, if null', function (done) {
      tracker.on('query', function (query) {
        query.response({
          amount: null
        });
      });
      db.getFreeCurrencyFrom('uid', 'time')
        .then(function (resp) {
          assert.deepEqual(resp, 0);
          done();
        });
    });
  });

  describe('getUnlockedPowerups', function () {
    it('should return unlocked powerups', function (done) {
      var get_cache = sinon.stub(db._cache, 'getValue', function () {
        get_cache.restore();

        return bluebird.reject();
      });
      tracker.on('query', function (query) {
        if (query.sql.indexOf('fb_connect') > -1) {
          query.response([{
            uid: ''
          }]);
        } else {
          query.response([{
            item: 'pow_a',
            source: 'first_time_bonus'
          },
          {
            item: 'pow_b',
            source: 'xyz'
          }]);
        }
      });
      db.getUnlockedPowerups('uid')
        .then(function (resp) {
          assert.deepEqual(resp, ['pow_a']);
          done();
        });
    });

    it('should return value from cache', function (done) {
      var get_cache = sinon.stub(db._cache, 'getValue', function () {
        get_cache.restore();

        return bluebird.resolve(['pow_b']);
      });
      tracker.on('query', function (query) {
        if (query.sql.indexOf('fb_connect') > -1) {
          query.response(false);
        } else {
          query.response(['pow_a']);
        }
      });
      db.getUnlockedPowerups('uid')
        .then(function (resp) {
          assert.deepEqual(resp, ['pow_b']);
          done();
        });
    });
  });

  describe('read', function () {
    var cache, file_proper,
      file_imp;

    beforeEach(function (done) {
      cache = config.puzzle_base_path;
      config.puzzle_base_path = 'test/';
      file_proper = config.puzzle_base_path + 'temp.json';
      file_imp = config.puzzle_base_path + 'temp_2.json';

      // Create a new file
      fs.writeFileAsync(file_proper, '{"key": "test"}', 'utf8')
        .then(function () {
          return fs.writeFileAsync(file_imp, '', 'utf8')
          .then(function () {
            return;
          })
          .catch(function (err) {
            return err;
          });
        })
        .then(done);
    });

    afterEach(function (done) {
      cache = config.puzzle_base_path;
      fs.unlinkAsync(file_proper)
        .then(function () {
          return fs.unlinkAsync(file_imp)
            .catch(function (err) {
              return err;
            });
        })
        .then(done);
    });

    it('File exists and data exists', function (done) {
      var data = db.read('temp');

      data.then(function (data) {
        assert.deepEqual(data, {
          key: 'test'
        });
        done(data ? null : 'no data');
      });
    });

    it('File exists and data exists, add to cache',
      function (done) {
      var obj = {
          key: 'test'
        },
        cache_add = sinon.stub(db._cache, 'addValue',
        function (key, value, type) {
          cache_add.restore();
          assert.strictEqual(key, 'temp');
          assert.deepEqual(value, obj);
          assert.strictEqual('puzzle', type);
          done();
        }),
        data = db.read('temp');

      data.then(function (data) {
        assert.deepEqual(data, obj);
      });
    });

    it('File doesn\'t exist', function (done) {
      var data = db.read('nofile');
      data.then(function (data) {
        done(data);
      }, function (err) {
        done(err.code === 'ENOENT' ? null : err.cause);
      });
    });

    it('File exists but no data', function (done) {
      var data = db.read('temp_2');

      data.then(function (data) {
        done(data);
      }, function () {
        done();
      });
    });

    it('file exists but cannot parse', function (done) {
      var data = db.read('temp_2');

      fs.writeFileAsync(file_imp, 'w');
      data.then(function (data) {
        done(data);
      }, function () {
        done();
      });
    });
  });

  describe('getPuzzle', function () {
    it('should read data if not available in cache', function (done) {
      var cache = db.read,
        cache_getValue = sinon.stub(db._cache, 'getValue', function () {
        cache_getValue.restore();
        return promise();
      });

      db.read = function (id) {
        db.read = cache;
        assert.strictEqual(1, id);
      };

      db.getPuzzle('1')
        .then(function () {
          done('err');
        })
        .catch(function () {
          done();
        });
    });

    it('should read if available in cache', function (done) {
      var cache = db.read,
        cache_getValue = sinon.stub(db._cache, 'getValue', function () {
          cache_getValue.restore();
          return promise({
            key: 'val'
          });
        });

      db.read = function () {
        done('err');
      };

      db.getPuzzle('1')
        .then(function () {
          db.read = cache;
          done();
        })
        .catch(function () {
          done('err');
        });
    });
  });

  describe ('fetchCompletedUserPuzzleData', function () {
    it('should return puzzle data only with progress as 100', function (done) {
      tracker.on('query', function (query) {
        query.response({
          fb_uid: '1234',
          ms_id: '3',
          score: '123322',
          stars: 3
        });
      });

      db._fetchCompletedUserPuzzleData('1234')
        .then(function (data) {
          assert.deepEqual(data, {
            fb_uid: '1234',
            ms_id: '3',
            score: '123322',
            stars: 3
          });
          done();
        });
    });

    it('should reject when fb_uid is null', function (done) {
      tracker.on('query', function () {
        throw new Error();
      });

      db._fetchCompletedUserPuzzleData()
        .then(function () {
          done('error');
        }, function () {
          done();
        });
    });
  });

  describe('getFriendPositions()', function () {
    it('should return empty array for null uids', function (done) {
      db.getFriendPositions()
        .then(function (data) {
          assert.deepEqual(data, []);
          done();
        });
    });

    it('should return empty array for empty uids', function (done) {
      db.getFriendPositions([])
        .then(function (data) {
          assert.deepEqual(data, []);
          done();
        });
    });

    it('should return values from cache', function (done) {
      var i = -1,
        ms = [5, 6],
        cache_getValue = sinon.stub(db._cache, 'getValue', function () {
        i++;
        return promise({
          game: {
            max_ms: ms[i]
          }
        });
      });

      db.getFriendPositions(['1234', '2345'])
        .then(function (data) {
          cache_getValue.restore();
          assert.deepEqual(data, {
            5: ['1234'],
            6: ['2345']
          });
          done();
        });
    });

    it('should return values from cache and db', function (done) {
      var cache_getValue = sinon.stub(db._cache, 'getValue', function () {
        cache_getValue.restore();
        return promise({
          game: {
            max_ms: 5
          }
        });
      });

      tracker.on('query', function (query) {
        query.response({
          fb_uid: '54321',
          max_ms: 6
        });
      });
      db.getFriendPositions(['4321', '54321'])
        .then(function (data) {
          assert.deepEqual(data, {
            5: ['4321'],
            6: ['54321']
          });
          done();
        });
    });

    it('should return undefined values for non existent ids', function (done) {
      var cache_getValue = sinon.stub(db._cache, 'getValue', function () {
        cache_getValue.restore();
        return promise({
          game: {
            max_ms: 5
          }
        });
      }),
      fb_uid = ['54321', 'aabbcc'],
      i = -1;

      tracker.on('query', function (query) {
        i++;
        query.response({
          fb_uid: fb_uid[i],
          max_ms: 6
        });
      });
      db.getFriendPositions(['4321', '54321', 'aabbcc', 'ccddaa'])
        .then(function (data) {
          assert.deepEqual(data, {
            5: ['4321'],
            6: ['54321', 'aabbcc', undefined]
          });
          done();
        });
    });

    it('should empty array when there is no data in memcache and db',
      function (done) {
      var cache_getValue = sinon.stub(db._cache, 'getValue', function () {
        cache_getValue.restore();
        return promise();
      });

      tracker.on('query', function (query) {
        query.response();
      });
      db.getFriendPositions(['4321'])
        .then(function (data) {
          assert.deepEqual(data, {});
          done();
        });
    });

    it('should reject invalid data', function (done) {
      var cache_getValue = sinon.stub(db._cache, 'getValue', function () {
        cache_getValue.restore();
        return promise();
      });

      tracker.on('query', function (query) {
        assert.deepEqual(query.bindings.length, 2);
        done();
      });
      db.getFriendPositions([{}]);
    });
  });

  describe('fetchDeviceInfo()', function () {
    it('should not select * if no value for fetch all', function (done) {

      tracker.on('query', function (query) {
        assert.deepEqual(query.method, 'first');
        assert.deepEqual(query.sql.startsWith('select *'), false);
        query.response();
      });
      db._fetchDeviceInfo('23')
        .then(done);
    });

    it('should select * if no value for fetch all', function (done) {

      tracker.on('query', function (query) {
        assert.deepEqual(query.method, 'first');
        assert.deepEqual(query.sql.startsWith('select *'), true);
        query.response();
      });
      db._fetchDeviceInfo('23', true)
        .then(done);
    });
  });

  describe('deleteFbUser()', function () {
    it('should delete table records for fb user', function (done) {
      var cache_del = sinon.stub(db._cache,
        'deleteUserData', function (uid, res) {
          assert.strictEqual(123, uid);
          assert.deepEqual([1, 1, 1, 1, 1, 1, 1, 1, 1], res);
          cache_del.restore();
          return true;
        });

      db._setFunction('deleteRecord', function () {
        return 1;
      });

      db.deleteFbUser(123)
        .then(function (val) {
          done(val === true ? undefined : 'error');
        });
    });
  });

  describe('cacheUnlockedPowerups()', function () {
    it('should return proper values', function (done) {
      var data = [
        {
          item: 'pow_a',
          source: 'first_time_bonus'
        },
        {
          item: 'pow_b',
          source: 'xyz'
        }
      ];

      assert.deepEqual(db._cacheUnlockedPowerups(data), ['pow_a']);
      done();
    });

    it('should push proper values to cache', function (done) {
      var ret_val = ['pow_a'],
        fb_uid = 123456,
        data = [
          {
            item: 'pow_a',
            source: 'first_time_bonus'
          },
          {
            item: 'pow_b',
            source: 'xyz'
          }
        ],
        push_cache = sinon.stub(db._cache, 'push', function (fb_id, val, key) {
          push_cache.restore();
          assert.strictEqual(fb_id, fb_uid);
          assert.deepEqual(val, ret_val);
          assert.strictEqual(key, 'unlocked_powerups');
          done();
        });

      assert.deepEqual(db._cacheUnlockedPowerups(data, fb_uid), ret_val);
    });
  });

  describe('insertPushNotifications()', function () {
    it('should invoke inserting to table', function () {
      tracker.on('query', function (query) {
        assert.strictEqual(query.method, 'insert');
        assert.strictEqual(true, query.sql
          .indexOf('push_notification_clicks') >= 0 ? true: false);
        query.response({});
      });

      db.insertPushNotifications({});
    });
  });
});
