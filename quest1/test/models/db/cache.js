'use strict';
var assert = require('assert'),
  _ = require('lodash'),
  bluebird = require('bluebird'),
  cache;

describe('cache', function () {
  var cache_get, cache_set;

  before(function () {
    cache = require('../../../models/db/cache');
  });

  beforeEach(function () {
    cache_get = cache._memcached.get;
    cache_set = cache._memcached.set;
  });

  afterEach(function () {
    cache._memcached.set = cache_set;
    cache._memcached.get = cache_get;
  });

  describe('getValue', function () {
    it('should get values from  memcache', function (done) {
      cache._memcached.get = function (type_id) {
        if (type_id === 'quest:user_record:1234') {
          return bluebird.resolve({
            uid: 'aaa',
            fb_uid: '1234'
          });
        } else {
          return bluebird.reject();
        }
      };
      cache.getValue('1234', 'user_record')
        .then(function (data) {
          assert.strictEqual(data.uid, 'aaa');
          assert.strictEqual(data.fb_uid, '1234');
          done();
        }, function (err) {
          done('error:' + err);
        });
    });

    it('should return with reject if data is not' +
      ' present memcache', function (done) {
      cache._memcached.get = function () {
        return bluebird.resolve(null);
      };
      cache.getValue('14445', 'user_record')
        .then(function () {
          done('got data');
        }, function () {
          done();
        });
    });
  });

  describe('delValue()', function () {
    it('should delete values from memcache', function (done) {
      cache._memcached.del = function (type_id) {
        if (type_id === 'quest:user_record:1234') {
          return bluebird.resolve(true);
        } else {
          return bluebird.reject();
        }
      };

      cache.delValue('1234', 'user_record')
        .then(function (res) {
          done(res === true ? undefined : 'error');
        }, function (err) {
          done('error:' + err);
        });
    });

    it('should reject with error', function (done) {
      cache._memcached.del = function (type_id) {
        if (type_id === 'quest:user_record:1234') {
          return bluebird.reject('err');
        } else {
          return bluebird.resolve(true);
        }
      };

      cache.delValue('1234', 'user_record')
        .then(function () {
          done('error');
        })
        .catch(function (err) {
          done(err === 'err' ? undefined : 'error');
        });
    });
  });

  describe('addValue', function () {
    it('should add values to  memcache', function (done) {
      cache._memcached.get = function (type_id) {
        if (type_id === 'quest:user_record:1234') {
          return bluebird.resolve({
            uid: 'aaa',
            fb_uid: '1234'
          });
        } else {
          return bluebird.reject();
        }
      };
      cache._memcached.set = function (type_id, data) {
        if (type_id === 'quest:user_record:1234' && JSON.stringify(data) ===
          JSON.stringify({uid: 'aaa', fb_uid: '1234'})) {
          return bluebird.resolve();
        } else {
          return bluebird.reject();
        }
      };
      cache.addValue('1234', {
        uid: 'aaa',
        fb_uid: '1234'
      }, 'user_record')
        .then(function () {
          cache._memcached.get('quest:user_record:1234')
            .then(function (data) {
              assert.strictEqual(data.uid, 'aaa');
              assert.strictEqual(data.fb_uid, '1234');
              done();
            }, function (err) {
              done('error get' + err);
            });
        }, function (err) {
          done('error addValue' + err);
        });
    });

    it('should update the exitsing values of memcache', function (done) {
      cache._memcached.get = function (type_id) {
        if (type_id === 'quest:user_record:1111') {
          return bluebird.resolve({
            uid: 'aaa',
            fb_uid: '1111'
          });
        } else {
          return bluebird.reject();
        }
      };
      cache._memcached.set = function (type_id, data) {
        if (type_id === 'quest:user_record:1111' && JSON.stringify(data) ===
          JSON.stringify({uid: 'aaa', fb_uid: '1234'})) {
          return bluebird.resolve();
        } else {
          return bluebird.reject();
        }
      };
      cache.addValue('1111', {
          uid: 'aaa',
          fb_uid: '1234'
        }, 'user_record')
        .then(function() {
          cache._memcached.get('quest:user_record:1111')
            .then(function (data) {
              assert.strictEqual(data.uid, 'aaa');
              assert.strictEqual(data.fb_uid, '1111');
              done();
            }, function (err) {
              done('error get' + err);
            });
        }, function (err) {
          done('error addValue' + err);
        });
    });
  });

  describe('push()', function () {
    it('should update cache after pushing the value', function (done) {
      var cache_get = cache.getValue,
        cache_set = cache.addValue;

      cache.getValue = function () {
        cache.getValue = cache_get;
        return bluebird.resolve([1, 2, 3]);
      };

      cache.addValue = function (key, data, type) {
        cache.addValue = cache_set;
        assert.deepEqual([1, 2, 3, 4], data);
        assert.strictEqual(type, 'req');
        done(key === 'id' ? undefined : 'error');

        return bluebird.resolve();
      };

      cache.push('id', 4, 'req');
    });

    it('should add value as array if key doesnt exists', function (done) {
      var cache_get = cache.getValue,
        cache_set = cache.addValue;

      cache.getValue = function () {
        cache.getValue = cache_get;
        return bluebird.reject();
      };

      cache.addValue = function (key, data, type) {
        cache.addValue = cache_set;
        assert.deepEqual([3, 4], data);
        assert.strictEqual(type, 'req');
        done(key === 'id' ? undefined : 'error');

        return bluebird.resolve();
      };

      cache.push('id', [3, 4], 'req');
    });
  });

  describe('addOrUpdateScoreCache()', function () {
    it('should add score for ms to cache', function (done) {
      var cache_get = cache.getValue,
        cache_add = cache.addValue;

      cache.getValue = function () {
        cache.getValue = cache_get;

        return bluebird.resolve([{
          ms_id: 'aaa',
          stars: 3
        }]);
      };

      cache.addValue = function (fbid, data, type) {
        cache.addValue = cache_add;
        assert.deepEqual([{
            ms_id: 'aaa',
            stars: 3
          }, {
            ms_id: 'bbb',
            stars: 1
          }], data);
        done(type === 'highscores' ? undefined : 'error');
      };

      cache.addOrUpdateScoreCache({
        ms_id: 'bbb',
        stars: 1
      });
    });

    it('should update max score to cache', function (done) {
      var cache_get = cache.getValue,
        cache_add = cache.addValue;

      cache.getValue = function () {
        cache.getValue = cache_get;

        return bluebird.resolve([{
          ms_id: 'aaa',
          stars: 3
        }]);
      };

      cache.addValue = function (fbid, data, type) {
        cache.addValue = cache_add;
        assert.deepEqual([{
            ms_id: 'aaa',
            stars: 3
          }, {
            ms_id: 'bbb',
            stars: 1
          }], data);
        done(type === 'highscores' ? undefined : 'error');
      };

      cache.addOrUpdateScoreCache({
        ms_id: 'bbb',
        stars: 1
      }, true);
    });

    it('should update score for existing puzzle', function (done) {
      var cache_get = cache.getValue,
        cache_add = cache.addValue;

      cache.getValue = function () {
        cache.getValue = cache_get;

        return bluebird.resolve([{
          ms_id: 'aaa',
          stars: 3
        }]);
      };

      cache.addValue = function (fbid, data, type) {
        cache.addValue = cache_add;
        assert.deepEqual([{
            ms_id: 'aaa',
            stars: 1
          }], data);
        done(type === 'highscores' ? undefined : 'error');
      };

      cache.addOrUpdateScoreCache({
        ms_id: 'aaa',
        stars: 1
      }, true);
    });

    it('it should add puzzle to cache if not cached', function (done) {
      var cache_get = cache.getValue,
        cache_add = cache.addValue;

      cache.getValue = function () {
        cache.getValue = cache_get;

        return bluebird.reject('not_found');
      };

      cache.addValue = function (fbid, data, type) {
        cache.addValue = cache_add;
        assert.deepEqual([{
            ms_id: 'aaa',
            stars: 2
          }], data);
        done(type === 'highscores' ? undefined : 'error');
      };

      cache.addOrUpdateScoreCache({
        ms_id: 'aaa',
        stars: 2
      }, true);
    });

    it('it should throw error if any error', function (done) {
      var cache_get = cache.getValue;

      cache.getValue = function () {
        cache.getValue = cache_get;

        return bluebird.reject('random_error');
      };

      cache.addOrUpdateScoreCache({
          ms_id: 'aaa',
          stars: 2
        }, true)
        .catch(function (err) {
          done(err === 'random_error' ? undefined : 'error');
        });
    });
  });

  describe('getFriendData()', function () {
    it('should get cached friends highscore data', function (done) {
      var ms_data = {
          123: [{ms_id: 1}],
          456: [{ms_id: 2}],
          789: [{ms_id: 3}]
        },
        memcached = cache._memcached,
        cache_multi = memcached.getMulti;

      memcached.getMulti = function (ids) {
        var res = [];

        memcached.getMulti = cache_multi;
        assert.strictEqual(ids.length, 3);
        _.forEach(ms_data, function (dat, id) {
          res[cache._getKey(id, 'highscores')] = dat;
        });

        return bluebird.resolve(res);
      };

      cache.getFriendData(_.keys(ms_data))
        .then(function (data) {
          assert.deepEqual(data.ms_data,
            _.reduce(ms_data, function (res, curr) {
              return _.concat(res, curr);
            }, [])
          );
          assert.strictEqual(data.non_cached_ids.length, 0);
          done();
        });
    });

    it('should return cached data of available friends and return non ' +
      'available users as non_cached_ids', function (done) {
        var ms_data = {
            123: [{ms_id: 1}],
            456: [{ms_id: 2}]
          },
          memcached = cache._memcached,
          cache_multi = memcached.getMulti;

        memcached.getMulti = function (ids) {
          var res = [];

          memcached.getMulti = cache_multi;
          assert.strictEqual(ids.length, 3);
          _.forEach(ms_data, function (dat, id) {
            res[cache._getKey(id, 'highscores')] = dat;
          });

          return bluebird.resolve(res);
        };

        cache.getFriendData(['123', '456', '789'])
          .then(function (data) {
            assert.deepEqual(data.ms_data,
              _.reduce(ms_data, function (res, curr) {
                return _.concat(res, curr);
              }, [])
            );
            assert.deepEqual(data.non_cached_ids, ['789']);
            done();
          });
      }
    );

    it('should return empty data and all fbids for error', function (done) {
      var memcached = cache._memcached,
        cache_multi = memcached.getMulti;

      memcached.getMulti = function () {
        memcached.getMulti = cache_multi;
        return bluebird.reject('err_data');
      };

      cache.getFriendData(['123', '456', '789'])
        .then(function (data) {
          assert.deepEqual([], data.ms_data);
          assert.deepEqual(['123', '456', '789'], data.non_cached_ids);
          done();
        });
    });
  });

  describe('deleteRequest()', function () {
    it('should delete request from users requests', function (done) {
      var cache_get = cache.getValue,
        cache_add = cache.addValue;

      cache.getValue = function () {
        cache.getValue = cache_get;

        return bluebird.resolve([{
            request_id: 'req1',
            sender: 123
          }, {
            request_id: 'req2',
            sender: 456
          }
        ]);
      };

      cache.addValue = function (receiver, data, type) {
        cache.addValue = cache_add;
        assert.deepEqual([{
            request_id: 'req2',
            sender: 456
          }], data);
        assert.strictEqual(receiver, 789);
        done(type === 'requests' ? undefined : 'error');
      };

      cache.deleteRequest('req1', 789)
        .catch(done);
    });

    it('should return false if requests not found for the ' +
      'user', function (done) {
        var cache_get = cache.getValue;

        cache.getValue = function () {
          cache.getValue = cache_get;

          return bluebird.reject('err');
        };

        cache.deleteRequest('req1', 789)
          .then(function (res) {
            done(res === false ? undefined : 'error');
          });
      }
    );
  });

  describe('insertRequests()', function () {
    it('should add request to cache for coresponding ' +
      'receiver', function (done) {
        var cache_push = cache.push,
          count = 0;

        cache.push = function (receiver, data, type) {
          assert.strictEqual('requests', type);
          if (++count === 2) {
            cache.push = cache_push;
            assert.deepEqual(data, {
              receiver: 2,
              sender: 8
            });
            done();
          } else {
            assert.deepEqual(data, {
              receiver: 1,
              sender: 8
            });
          }

          assert.strictEqual(count, receiver);

          return bluebird.resolve();
        };

        cache.insertRequests([{
            receiver: 1,
            sender: 8
          }, {
            receiver: 2,
            sender: 8
          }]);
      }
    );

    it('should fail even one request is failed', function (done) {
      var cache_push = cache.push,
        count = 0;

      cache.push = function () {
        if (++count === 2) {
          cache.push = cache_push;
          return bluebird.reject();
        }

        return bluebird.resolve();
      };

      cache.insertRequests([{}, {}])
        .catch(done.bind(null, undefined));
    });
  });

  describe('deleteUserData()', function () {
    it('should delete user data from cache', function (done) {
      var cache_del = cache.delValue,
        count = 0;

      cache.delValue = function () {
        return bluebird.resolve(++count);
      };

      cache.deleteUserData(123)
        .then(function (res) {
          assert.deepEqual([1, 2, 3, 4], res);
          cache.delValue = cache_del;
          done();
        });
    });
  });
});

