var sinon = require('sinon'),
  _ = require('lodash'),
  promise = require('../lib/fakePromise').promise,
  purchase_validator = require('../../modules/purchase_validator'),
  assert = require('assert'),
  util, config, db, job, fb,
  cash, user;

describe('user', function () {
  'use strict';

  var db_get_user, db_update_userinfo, db_insert_puzzle_all,
    db_update_usergame, db_update_device, db_insert_user,
    db_add_update_maxpuzzle, db_save_credit, db_save_purchase,
    db_get_requests, db_non_track_puzzles, db_get_fb_connect,
    db_get_user_by_fbid, db_ins_requests, db_insert_fb_connect,
    db_remove_old_reqs, fb_delete_req, db_update_requests,
    db_get_puzzle_data, job_push, db_get_user_game,
    db_track_free_goods, db_get_friend_scores, db_insert_fb_user,
    db_update_fb_disconnect, db_get_friend_positions, db_get_sale_data,
    db_get_version_patch, db_get_used_currency, db_get_purchases_from,
    db_get_free_currency_from, db_get_last_disconnect_time, purchase_val,
    db_get_bonus_puzzles, db_insert_bonus_stats, db_get_unlocked_powerups;

  before(function () {
    config = require('../../resources/config');
    cash = require('../../resources/data/products/cash');
    cash.cash1 =  {
        no_of_cash: 100,
        l10n: [{
          cost: 1,
          currency: 'USD'
        }]
      };
    cash.cash2 = {
        no_of_cash: 525,
        l10n: [{
          cost: 5,
          currency: 'USD'
        }]
      };

    config.powerups.ingame = ['hint'];
    config.inventories = ['hint'];

    db = require('../../models/db/db');
    job = require('../../modules/job');
    fb = require('../../modules/facebook');
    util = require('../../modules/util');
    user = require('../../models/user');

    // Below line is just to ensure coverage of getDb function
    user.getDb();

    db_get_user = sinon.stub(db, 'getUserRecord');
    db_update_userinfo = sinon.stub(db, 'updateUserInfo');
    db_update_usergame = sinon.stub(db, 'updateUserGame');
    db_update_device = sinon.stub(db, 'updateUserDevice');
    db_insert_user = sinon.stub(db, 'insertUser');
    db_insert_puzzle_all = sinon.stub(db, 'insertPuzzleAll');
    db_add_update_maxpuzzle = sinon.stub(db, 'addOrUpdateMaxPuzzle');
    db_save_credit = sinon.stub(db, 'saveCredits');
    db_save_purchase = sinon.stub(db, 'savePurchases');
    db_get_user_by_fbid = sinon.stub(db, 'getUserByFBId');
    db_get_friend_scores = sinon.stub(db, 'getFriendHighscores');
    db_get_friend_positions = sinon.stub(db, 'getFriendPositions');
    db_get_puzzle_data = sinon.stub(db, 'getCompletedUserPuzzleData');
    db_ins_requests = sinon.stub(db, 'insertRequests');
    db_update_requests = sinon.stub(db, 'updateRequest');
    db_remove_old_reqs = sinon.stub(db, 'removeOldRequests');
    fb_delete_req = sinon.stub(fb, 'deleteRequest');
    db_get_requests = sinon.stub(db, 'getRequests');
    db_non_track_puzzles = sinon.stub(db, 'getNonTrackedDevicePuzzles');
    db_get_fb_connect = sinon.stub(db, 'getCurrentFbConnect');
    db_insert_fb_connect = sinon.stub(db, 'insertFbConnect');
    db_insert_fb_user = sinon.stub(db, 'insertFbUser');
    db_get_user_game = sinon.stub(db, 'getUserGame');
    db_update_fb_disconnect = sinon.stub(db, 'updateFbConnect');
    job_push = sinon.stub(job, 'push');
    db_track_free_goods = sinon.stub(db, 'trackFreeGoods');
    db_get_version_patch = sinon.stub(db, 'getVersionPatch');
    db_get_sale_data = sinon.stub(db, 'getCurrentSale');
    db_get_used_currency = sinon.stub(db, 'getUsedCurrencyFrom');
    db_get_purchases_from = sinon.stub(db, 'getPurchasesFrom');
    db_get_free_currency_from = sinon.stub(db, 'getFreeCurrencyFrom');
    db_get_last_disconnect_time = sinon.stub(db, 'getLastDisconnectTime');
    purchase_val = sinon.stub(purchase_validator, 'verifyPurchase');
    db_get_bonus_puzzles = sinon.stub(db, 'getCompletedBonusLevels');
    db_insert_bonus_stats = sinon.stub(db, 'insertBonusStats');
    db_get_unlocked_powerups = sinon.stub(db, 'getUnlockedPowerups');
  });

  after(function () {
    db_update_userinfo.restore();
    db_update_usergame.restore();
    db_update_device.restore();
    db_insert_user.restore();
    db_get_friend_scores.restore();
    db_get_friend_positions.restore();
    db_get_user.restore();
    db_insert_puzzle_all.restore();
    db_add_update_maxpuzzle.restore();
    db_save_credit.restore();
    db_save_purchase.restore();
    db_get_user_by_fbid.restore();
    db_get_puzzle_data.restore();
    db_ins_requests.restore();
    db_update_requests.restore();
    db_remove_old_reqs.restore();
    fb_delete_req.restore();
    db_get_requests.restore();
    db_non_track_puzzles.restore();
    db_get_fb_connect.restore();
    db_insert_fb_connect.restore();
    db_insert_fb_user.restore();
    db_get_user_game.restore();
    db_update_fb_disconnect.restore();
    job_push.restore();
    db_track_free_goods.restore();
    db_get_version_patch.restore();
    db_get_sale_data.restore();
    db_get_used_currency.restore();
    db_get_purchases_from.restore();
    db_get_free_currency_from.restore();
    db_get_last_disconnect_time.restore();
    purchase_val.restore();
    db_get_bonus_puzzles.restore();
    db_insert_bonus_stats.restore();
  });

  describe('init', function () {
    it('should initialise knex with config connection', function (done) {
      var knex = db._knex;

      assert.strictEqual(knex.client.connectionSettings.host,
        config.db.connection.host);
      assert.strictEqual(knex.client.connectionSettings.user,
        config.db.connection.user);
      assert.strictEqual(knex.client.connectionSettings.password,
        config.db.connection.password);
      assert.strictEqual(knex.client.connectionSettings.database,
        config.db.connection.database);
      done();
    });
  });

  describe('syncData()', function () {
    var add_update_user, get_version_patch, get_sale_data, get_sale_reward;

    before(function () {
      add_update_user = sinon.stub(user, 'addOrUpdateUserData');
      get_version_patch = sinon.stub(user, 'getVersionPatch');
      get_sale_data = sinon.stub(user, 'getCurrentSale');
      db_get_puzzle_data.returns(promise(1));
      get_sale_reward = sinon.stub(user, 'getSaleAndRewards');
    });

    after(function () {
      add_update_user.restore();
      get_version_patch.restore();
      get_sale_data.restore();
      get_sale_reward.restore();
    });

    it('should fail user sync if user update db call fails', function (done) {

      add_update_user.returns(promise());
      user.syncData({user: {}, device: {}})
        .then(function () {
          done('error');
        })
        .catch(function (error) {
          assert.strictEqual(error.no, 0);
          assert.strictEqual(error.stack, 'Over here');
          done();
        });
    });

    it('should invoke user add/update db method with proper ' +
      'variables', function (done) {
        var data = {
          user: {start_time: 'date', friend_fbids: 'fbs'},
          device: {}
        }, user_cache = user.postSync;

        db_get_friend_scores.returns(promise(1));

        add_update_user.withArgs(data.user, data.device)
          .returns(promise(2));

        get_sale_reward.returns(promise({}));
        user.syncData(data)
          .then(function () {
            user.postSync = user_cache;
            done();
          })
          .catch(function (err) {
            done('error:' + err);
          });
      }
    );

    it('should return patch if not facebook connected', function (done) {
      add_update_user.returns(promise(1));
      get_version_patch.returns(promise({
        version: '5',
        1: {
          ms_id: 'hello'
        }
      }));

      user.syncData({user: {uid: 1}, device: {}})
        .then(function (status) {
          assert.strictEqual('5', status.puzzle_patch.version);
          assert.strictEqual('hello', status.puzzle_patch[1].ms_id);
          done();
        });
    });

    it('should\'nt call postSync if user is\'n fb connected', function (done) {
      var sync_cache = user.postSync,
        update_user_cache = user.addOrUpdateUserData,
        post_sync_called = false;

      user.postSync = function () {
        user.postSync = sync_cache;
        post_sync_called = true;
      };

      get_version_patch.returns(promise({
        version: '5',
        1: {
          ms_id: 'hello'
        }
      }));

      user.addOrUpdateUserData = function () {
        user.addOrUpdateUserData = update_user_cache;
        return promise(1);
      };

      user.syncData({user: {uid: 1}, device: {}})
        .then(function (status) {
          assert.strictEqual('5', status.puzzle_patch.version);
          assert.strictEqual('hello', status.puzzle_patch[1].ms_id);
          assert.strictEqual(false, post_sync_called);
          user.postSync = sync_cache;
          done();
        });
    });

    it('should get empty patch is\'n fb connected', function (done) {
      var sync_cache = user.postSync,
        post_sync_called;

      user.postSync = function () {
        post_sync_called = true;
      };

      get_version_patch.returns(promise(false));

      user.syncData({user: {uid: 1}, device: {}})
        .then(function (res) {
          assert.deepEqual({
            puzzle_patch: false,
            cross_promotion: null,
            rewards: null,
            sale: null
          }, _.omit(res, 'date'));
          assert.strictEqual(post_sync_called);
          user.postSync = sync_cache;
          done();
        });
    });

    it('should return true if user is not facebook connected', function () {
      var update_user_cache = user.addOrUpdateUserData;

      user.addOrUpdateUserData = function () {
        user.addOrUpdateUserData = update_user_cache;
        return promise(1);
      };
      get_sale_reward.returns(promise({
        sale: null,
        rewards: null
      }));
      add_update_user.returns(promise(1));
      get_version_patch.returns(promise(null));
      user.syncData({user: {uid: 1}, device: {}})
        .then(function (res) {
          assert.deepEqual({
            puzzle_patch: null,
            cross_promotion: null,
            sale: null,
            rewards: null
          }, _.omit(res, 'date'));
        });
    });

    it('should set client ip as users ip and pass user to ' +
      'addOrUpdateUserData', function (done) {
        var stub_update_user;

        add_update_user.restore();
        get_version_patch.returns(promise(false));

        stub_update_user = sinon.stub(user, 'addOrUpdateUserData',
          function (dat) {
            stub_update_user.restore();
            assert.deepEqual({
              uid: 1,
              ip_address: 'ip',
              friend_fbids: 'fbs'
            }, dat);

            return promise(1);
          }
        );

        user.syncData({
            user: {uid: 1, friend_fbids: 'fbs'},
            device: {}
          }, 'ip')
          .then(function () {
            done();
          }, function () {
            done('error');
          });
      }
    );

    it('should set proper job data for old data', function (done) {
      var purchases = [{currency_used: {paid: 100}}],
        credits = [{item: 'cash4'}],
        free_currencies = [{source: 'level_up'}],
        clock = sinon.useFakeTimers(new Date(2011,9,1).getTime()),
        curr_date = new Date(),
        user_cache = user.postSync;

      add_update_user.restore();

      get_version_patch.returns(promise(false));
      db_non_track_puzzles.returns(promise([]));
      db_get_friend_scores.returns(promise([]));
      db_get_friend_positions.returns(promise({}));
      get_sale_data.returns(promise({}));
      add_update_user = sinon.stub(user, 'addOrUpdateUserData', function () {
        return promise('uid');
      });

      user.postSync = function (data) {
        user.postSync = user_cache;

        assert.deepEqual(data, {
          uid: 'uid',
          fb_id: '1234',
          score: 1,
          date: curr_date.toString(),
          bonus_levels_played: [],
          friend_positions: {},
          friend_scores: [],
          puzzle_patch: false,
          cross_promotion: null,
          rewards: null,
          sale: null,
          job_data: {
            uid: 'uid',
            fb_uid: '1234',
            puzzle_data: {
              gs_stats_all: [],
              bonus_stats: []
            },
            purchases: purchases,
            credits: credits,
            free_goods: free_currencies,
            notifications: []
          }
        });
        clock.restore();
        done();
        return promise(2);
      };

      user.syncData({
        user: {
          uid: 'uid',
          fb_uid: '1234',
          credits: credits,
          purchases: purchases,
          friends: {},
          track_free_currency: free_currencies
        },
        notifications: [],
        device: {}
      });
    });

    it('should sort friend positions correctly', function (done) {
      var purchases = [{currency_used: {paid: 100}}],
        credits = [{item: 'cash4'}],
        clock = sinon.useFakeTimers(new Date(2011,9,1).getTime()),
        curr_date = new Date(),
        free_currencies = [{source: 'level_up'}],
        user_cache = user.postSync;

      add_update_user.restore();

      get_version_patch.returns(promise(false));
      db_non_track_puzzles.returns(promise([]));
      db_get_friend_scores.returns(promise([]));
      db_get_friend_positions.returns(promise({
          5: ['1234'],
          6: ['2345'],
          7: ['3456']
        }));
      add_update_user = sinon.stub(user, 'addOrUpdateUserData', function () {
        return promise('uid');
      });

      user.postSync = function (data) {
        user.postSync = user_cache;
        assert.deepEqual(data, {
          uid: 'uid',
          fb_id: '1234',
          score: 1,
          friend_positions: {
            5: ['1234'],
            6: ['2345'],
            7: ['3456']
          },
          friend_scores: [],
          bonus_levels_played: [],
          date: curr_date.toString(),
          puzzle_patch: false,
          cross_promotion: null,
          rewards: null,
          sale: null,
          job_data: {
            uid: 'uid',
            fb_uid: '1234',
            puzzle_data: {
              gs_stats_all: [],
              bonus_stats: []
            },
            purchases: purchases,
            credits: credits,
            free_goods: free_currencies,
            notifications: []
          }
        });
        done();
        clock.restore();
        return promise(2);
      };

      user.syncData({
        user: {
          uid: 'uid',
          fb_uid: '1234',
          credits: credits,
          purchases: purchases,
          friends: {},
          track_free_currency: free_currencies
        },
        notifications: [],
        device: {}
      });
    });
  });
  describe('getVersionPatch()', function () {
    it('should return false if patch not available', function (done) {
      db_get_version_patch.returns(promise(false));

      user.getVersionPatch('0.50', 4)
      .then(function (resp) {
        assert.strictEqual(resp, false);
        done();
      })
      .catch(function () {
        done('err');
      });
    });

    it('should return false if patch version same as' +
      'user verison', function (done) {
      db_get_version_patch.returns(promise({
        version: 4
      }));

      user.getVersionPatch('0.50', 4)
      .then(function (resp) {
        assert.strictEqual(resp, false);
        done();
      })
      .catch(function () {
        done('err');
      });
    });

    it('should return patch data if available for app ' +
      'verison', function (done) {
      var obj = {
        key: 'test'
      };

      db_get_version_patch.returns(promise(obj));

      user.getVersionPatch('0.50', 4)
      .then(function (resp) {
        assert.strictEqual(resp, obj);
        done();
      })
      .catch(function () {
        done('err');
      });
    });

    it('should return false for exception in getVersionPatch', function (done) {
      db_get_version_patch.returns(promise());

      user.getVersionPatch('0.50', 4)
      .then(function () {
        done('err');
      })
      .catch(function (resp) {
        assert.strictEqual(false, resp);
        done();
      });
    });
  });

  describe('updateUserData()', function () {
    it('should resolve when all three params are resolved', function (done) {
      db_update_usergame.returns(promise({}));
      db_update_userinfo.returns(promise({}));
      db_update_device.returns(promise({}));

      user._updateUserData(true, true, true)
        .then(function () {
          done();
        })
        .catch(function (err) {
          done('err:' + err);
        });
    });

    it('should fail when one of the three param fail', function (done) {
      db_update_usergame.returns(promise({}));
      db_update_userinfo.returns(promise());
      db_update_device.returns(promise({}));

      user._updateUserData(true, true, true)
        .then(function () {
          done('err');
        })
        .catch(function () {
          done();
        });
    });

    it('should pass when the required params are passed ' +
      'even if one missing params fail', function (done) {
      var cache = db.updateUserInfo;

      db_update_userinfo.restore();

      db.updateUserInfo = function () {
        done('err');
      };

      db_update_usergame.returns(promise({}));
      db_update_device.returns(promise({}));

      user._updateUserData(null, true, true)
        .then(function () {
          db.updateUserInfo = cache;
          done();
        })
        .catch(function () {
          done('err');
        });
    });
  });

  describe('updateBonusPuzzles()', function () {
    it('should invoke db insert for bonus puzzles', function (done) {
      db_insert_bonus_stats.returns(promise(true));
      user._updateBonusPuzzles([{}, {}])
        .then(function (val) {
          done(val === true ? undefined : 'error');
        });
    });

    it('shouldnt invoke db insert if array empty', function (done) {
      db_insert_bonus_stats.returns(promise(true));
      user._updateBonusPuzzles([])
        .then(function (val) {
          done(val !== true ? undefined : 'error');
        });
    });
  });

  describe('updateData()', function () {
    it('should update user_game', function (done) {
      var master_data = {powerup: 10},
        curr_user_data = {powerup: 10},
        user_game = {powerup: 5};
      user.updateData(master_data, curr_user_data, user_game);

      assert.deepEqual(user_game, {powerup: 20});
      done();
    });
  });

  describe('addOrUpdateUserData()', function () {
    it('should call insert user method with proper ' +
      'parameter', function (done) {
        var clock = sinon.useFakeTimers(new Date(2011,9,1).getTime()),
          game = {
            uid: 'uidone',
            paid_currency: 0,
            free_currency: 0,
            music_muted: true,
            sound_muted: true,
            fb_friend_count: 2,
            club_reds: 2,
            double_trouble: 3,
            color_splash: 1,
            jelly_buster: 2,
            eight_way: 2
          },
          device = {
            device_id: 'deviceid',
            uid: 'uidone',
            start_time: new Date(),
            last_update: new Date(),
            current_version: '0.34',
            install_referrer: 'hashcube.com'
          },
          data = {
            user: {
              uid: 'uidone',
              install_time: new Date(),
              start_time: new Date(),
              friend_fbids: ['1234','1311'],
              track_free_currency: [],
              purchases: [],
              credits: [],
              inventory_club_reds: 2,
              inventory_double_trouble: 3,
              inventory_color_splash: 1,
              inventory_jelly_buster: 2,
              inventory_eight_way: 2,
              rating_done: 4,
              fb_disconnect_time: null
            },
            device: {advertising_id: 'deviceid', appversion: '0.34'}
          };

        data.device.is_mobile = true;
        db_get_user.returns(promise({}));
        db_insert_user.withArgs(device, game).returns(promise(1));
        user.addOrUpdateUserData(data.user, data.device, [])
          .then(function (resp) {
            clock.restore();
            delete data.device.is_mobile;
            done(resp === false ? undefined : 'error');
          }, function () {
            clock.restore();
            done('Promise rejected');
          });
      }
    );

    it('should call insert user method device start_time ' +
      'set as users start time', function (done) {
        var info = {
            uid: 'uidone',
            language: 'en'
          },
          clock = sinon.useFakeTimers(new Date(2011,9,1).getTime()),
          game = {
            uid: 'uidone',
            paid_currency: 0,
            free_currency: 0,
            music_muted: true,
            sound_muted: true,
            fb_friend_count: 0
          },
          device = {
            device_id: 'deviceid',
            uid: 'uidone',
            start_time: 'date',
            last_update: new Date(),
            current_version: '0.34'
          },
          data = {
            user: {uid: 'uidone', start_time: 'date', fb_disconnect_time: null},
            device: {advertising_id: 'deviceid', appversion: '0.34'}
          };

        data.device.is_mobile = false;
        db_get_user.returns(promise({}));
        db_insert_user.withArgs(info, game, device).returns(promise(1));
        user.addOrUpdateUserData(data.user, data.device, 0)
          .then(function (resp) {
            clock.restore();
            delete data.device.is_mobile;
            done(resp === false ? undefined : 'error');
          }, function () {
            clock.restore();
            done('Promise rejected');
          });
      }
    );

    it('should update user data if user with uid already exists',
      function (done) {
        var user_data = {
            max_ms: 5,
            uid: 'uidone',
            fb_uid: 123,
            game_sounds: {music: false, effects: true}
          },
          device = {advertising_id: 'sdsd', appversion: '0.34'};

        db_get_user.returns(promise({
          info: {fb_uid: 123},
          game: {max_ms: 1, music_muted: true},
          device: {uid: 'uid'}
        }));
        db_update_userinfo.returns(promise(1));
        db_update_usergame.returns(promise(1));
        db_update_device.returns(promise(2));
        db_get_fb_connect.returns(promise({}));

        user.addOrUpdateUserData(user_data, device)
          .then(function (resp) {
            done(resp === false ? undefined : 'error');
          }, function () {
            done('error');
          });
      }
    );

    it('should update disconnect time of fb connect', function (done) {
      var now = new Date(),
        user_data = {
          max_ms: 5,
          uid: 'uidone',
          prev_fb_uid: '122',
          fb_disconnect_time: now,
          game_sounds: {music: false, effects: true}
        },
        device = {advertising_id: 'sdsd', appversion: '0.34'};

      db_get_user.returns(promise({
        device: {uid: 'uidone'},
        game: {max_ms: 1, music_muted: true}
      }));

      db_update_usergame.restore();
      db_update_usergame = sinon.stub(db, 'updateUserGame', function (params) {
        db_update_usergame.restore();
        db_update_usergame = sinon.stub(db, 'updateUserGame');
        assert.strictEqual(params.max_ms, 5);

        return promise(1);
      });
      db_update_device.returns(promise(2));
      db_update_fb_disconnect.restore();
      db_update_fb_disconnect = sinon.stub(db, 'updateFbConnect',
        function (uid, fbid, time) {
          assert.strictEqual('uidone', uid);
          assert.strictEqual('122', fbid);
          assert.strictEqual(now.getTime(), time.getTime());
          done();

          return promise(1);
        }
      );

      user.addOrUpdateUserData(user_data, device)
        .then(function (resp) {
          assert.strictEqual(false, resp);
        }, function () {
          done('error');
        });
    });

    it('should reject because of javascript error', function (done) {
      var user_data = {
          max_ms: 5,
          uid: 'uidone',
          fb_uid: 123,
          game_sounds: {music: false, effects: true}
        },
        device = {advertising_id: 'sdsd', appversion: '0.34'};

      db_get_user.returns(promise(null));
      db_update_userinfo.returns(promise(1));
      db_update_usergame.returns(promise(1));
      db_update_device.returns(promise(2));

      user.addOrUpdateUserData(user_data, device)
        .then(function () {
          done('error');
        }, function (err) {
          assert.strictEqual('TypeError', err.name);
          assert.strictEqual('Cannot read property ' +
            '\'info\' of null', err.message);
          done();
        });
    });

    it('should invoke insertFbUser if fb user connected for first ' +
      'time', function (done) {
        var user_data = {
            max_ms: 5,
            uid: 'uidone',
            fb_uid: 123,
            fb_disconnect_time: null,
            game_sounds: {music: false, effects: true}
          },
          device = {advertising_id: 'sdsd', appversion: '0.34'};

        db_get_user.returns(promise({device: {}}));
        db_update_userinfo.returns(promise(1));
        db_update_usergame.returns(promise(1));
        db_update_device.returns(promise(2));
        db_insert_fb_connect.returns(promise(1));
        db_get_user_game.returns(promise({go_red: 3}));
        db_insert_fb_user.returns(promise({}));

        user.addOrUpdateUserData(user_data, device)
          .then(function (resp) {
            done(resp === true ? undefined : 'error');
          }, function () {
            done('error');
          });
      }
    );

    it('should insert fbconnect and update fb user first sync after connect',
      function (done) {
        var user_data = {
            max_ms: 5,
            uid: 'uidone',
            fb_uid: 123,
            fb_disconnect_time: null,
            game_sounds: {music: false, effects: true}
          },
          device = {advertising_id: 'sdsd', appversion: '0.34'};

        db_get_user.returns(promise({
            info: {first_name: 'Name'},
            game: {go_red: 6, max_ms: 10}
          }));

        db_insert_user.returns(promise(1));
        db_update_userinfo.returns(promise(1));
        db_update_usergame.returns(promise(1));
        db_update_device.returns(promise(2));
        db_get_fb_connect.returns(promise(null));
        db_insert_fb_connect.returns(promise(1));
        db_get_user_game.returns(promise({go_red: 3}));

        user.addOrUpdateUserData(user_data, device)
          .then(function (resp) {
            done(resp === true ? undefined : 'error');
          }, function () {
            done('error');
          });
      }
    );

    it('should update user data for existing nonfb user', function (done) {
      var user_data = {
          max_ms: 5,
          uid: 'uidone',
          fb_disconnect_time: null,
          game_sounds: {music: false, effects: true}
        },
        device = {advertising_id: 'sdsd', appversion: '0.34'};

      db_get_user.returns(promise({
        device: {uid: 'uidone'},
        game: {max_ms: 1, music_muted: true}
      }));

      db_update_usergame.restore();
      db_update_usergame = sinon.stub(db, 'updateUserGame', function (params) {
        db_update_usergame.restore();
        assert.strictEqual(params.max_ms, 5);
        return promise(1);
      });
      db_update_device.returns(promise(2));

      user.addOrUpdateUserData(user_data, device)
        .then(function (resp) {
          done(resp === false ? undefined : 'error');
        }, function () {
          done('error');
        });
    });

    it('should insert user if fb_uid is not associated', function (done) {
      var data = {
        user: {uid: 'uidone', start_time: 'date', fb_uid: 123,
          max_ms: 5, fb_disconnect_time: null},
        device: {advertising_id: 'deviceid', appversion: '0.34'}
      };

      db_get_user.returns(promise({}));
      db_get_user_by_fbid.returns(promise(null));
      db_insert_user.returns(promise(1));
      db_insert_fb_connect.returns(promise(1));
      db_get_user_game.returns(promise({}));
      db_insert_fb_user.restore();
      db_insert_fb_user = sinon.stub(db, 'insertFbUser', function (info, game) {
        assert.deepEqual({
          fb_uid: 123,
          paid_currency: 0,
          free_currency: 0,
          hint: 0,
          max_ms: 5,
          music_muted: true,
          sound_muted: true,
          amount_spent: 0,
          fb_friend_count: 0
        }, _.omit(game, 'last_update'));

        return promise(1);
      });
      user.addOrUpdateUserData(data.user, data.device)
        .then(function (id) {
          done(id === true ? null: 'error');
        }, done);
    });

    it('should update user_game with existing data and current ' +
      'transactions', function (done) {
        var data = {
          user: {
            uid: 'test_uid_a',
            max_ms: 2,
            paid: 10.2,
            purchases: [{
              item: 'double_trouble',
              quantity: 3,
              currency_used: {
                free: 10,
                paid: 0
              }
            }],
            fb_disconnect_time: null,
            track_free_currency: [{amount: 15}],
            credits: [{item: 'cash1'}]
          },
          device: {advertising_id: 'device_a', appversion: '0.34'}
        };

        db_get_user.returns(promise({
          device: {},
          game: {
            double_trouble: 2,
            free_currency: 10,
            paid_currency: 20,
            amount_spent: 4
          }
        }));

        db_update_userinfo.returns(promise(1));
        db_update_usergame.restore();
        db_update_usergame = sinon.stub(db, 'updateUserGame', function (data) {
          assert.deepEqual({
            uid: 'test_uid_a',
            paid_currency: 120,
            free_currency: 0,
            hint: 0,
            max_ms: 2,
            music_muted: true,
            sound_muted: true,
            amount_spent: 5
          }, _.omit(data, 'last_update'));
          return promise(1);
        });

        db_update_device.returns(promise(2));

        user.addOrUpdateUserData(data.user, data.device, 5)
          .then(function (reload) {
            done(reload === false ? undefined : 'error');
          }, done);
      });

    it('should update fb game with correct values for first sync ' +
      'after connect and prev_fb_id != fb_id', function (done) {
        var user_data = {
            max_ms: 5,
            uid: 'uidone',
            fb_uid: 123,
            score: 10000,
            game_sounds: {music: false, effects: true},
            fb_disconnect_time: null,
            first_name: 'Name',
            last_name: 'testB',
            purchases: [{
              item: 'double_trouble',
              quantity: 1,
              currency_used: {
                free: 10,
                paid: 0
              }
            }],
            track_free_currency: [{amount: 15}],
            credits: [{item: 'cash1'}]
          },
          db_user_data = {
            free_currency: 10,
            paid_currency: 20,
            amount_spent: 4,
            master_score: 9999,
            max_ms: 7
          },
          device = {advertising_id: 'sdsd', appversion: '0.34'};

        db_get_user.returns(promise({
            device: {},
            info: {first_name: 'Name', last_name: 'testB'},
            game: db_user_data
          }));

        db_update_device.returns(promise(2));
        db_get_fb_connect.returns(promise(null));
        db_insert_fb_connect.returns(promise(1));
        db_update_userinfo.restore();
        db_update_usergame.restore();
        db_update_userinfo = sinon.stub(db, 'updateUserInfo', function (param) {
          assert.strictEqual('testB', param.last_name);

          return promise(1);
        });
        db_update_usergame = sinon.stub(db, 'updateUserGame', function (param) {
          assert.deepEqual({
            paid_currency: cash[user_data.credits[0].item].no_of_cash +
               db_user_data.paid_currency,
            free_currency: db_user_data.free_currency -
               user_data.purchases[0].currency_used.free,
            max_ms: Math.max(user_data.max_ms, db_user_data.max_ms),
            amount_spent: db_user_data.amount_spent + _.size(user_data.credits),
            master_score: Math.max(user_data.score, db_user_data.master_score)
          }, _.pick(param, ['paid_currency', 'free_currency', 'max_ms',
          'double_trouble', 'amount_spent', 'master_score']));

          return promise(1);
        });
        user.addOrUpdateUserData(user_data, device)
          .then(function (resp) {
            done(resp === true ? undefined : 'error');
          })
          .catch(function (err) {
            done('error:' + err);
          });
      });

    it('should update fb game with correct values for first sync' +
      ' after connect and prev_fb_id == fb_id', function (done) {
        var user_data = {
            max_ms: 5,
            uid: 'uidone',
            prev_fb_uid: 123,
            fb_uid: 123,
            score: 10000,
            fb_disconnect_time: null,
            game_sounds: {music: false, effects: true},
            first_name: 'Name',
            last_name: 'testB',
            purchases: [{
              item: 'double_trouble',
              quantity: 1,
              currency_used: {
                free: 10,
                paid: 0
              }
            }],
            track_free_currency: [{amount: 15}],
            credits: [{item: 'cash1'}]
          },
          db_user_data = {
            free_currency: 10,
            paid_currency: 20,
            amount_spent: 4,
            master_score: 9999,
            max_ms: 7
          },
          free_currency = 10,
          transactions = {
            free_currency: 0, paid_currency: 50
          },
          purchases = {
            currency_bundle: 'cash1'
          },
          device = {advertising_id: 'sdsd', appversion: '0.34'};

        db_get_user.returns(promise({
            device: {},
            info: {first_name: 'Name', last_name: 'testB'},
            game: db_user_data
          }));

        db_update_device.returns(promise(2));
        db_get_fb_connect.returns(promise(null));
        db_insert_fb_connect.returns(promise(1));
        db_get_last_disconnect_time.returns(promise(0));
        db_get_used_currency.returns(promise([transactions]));
        db_get_purchases_from.returns(promise([purchases]));
        db_get_free_currency_from.returns(promise(free_currency));
        db_update_userinfo.restore();
        db_update_usergame.restore();
        db_update_userinfo = sinon.stub(db, 'updateUserInfo', function (param) {
          assert.strictEqual('testB', param.last_name);

          return promise(1);
        });
        db_update_usergame = sinon.stub(db, 'updateUserGame', function (param) {
          assert.deepEqual({
            paid_currency: cash[user_data.credits[0].item].no_of_cash +
               db_user_data.paid_currency - transactions.paid_currency +
               cash[purchases.currency_bundle].no_of_cash,
            free_currency: db_user_data.free_currency + free_currency -
              user_data.purchases[0].currency_used.free,
            max_ms: Math.max(user_data.max_ms, db_user_data.max_ms),
            amount_spent: db_user_data.amount_spent + _.size(user_data.credits),
            master_score: Math.max(user_data.score, db_user_data.master_score)
          }, _.pick(param, ['paid_currency', 'free_currency', 'max_ms',
          'double_trouble', 'amount_spent', 'master_score']));

          return promise(1);
        });
        user.addOrUpdateUserData(user_data, device)
          .then(function (resp) {
            done(resp === true ? undefined : 'error');
          })
          .catch(function (err) {
            done('error:' + err);
          });
      });

    it('should pass null to get date if fb_disconnect_time is' +
      'undefined', function (done) {
        var user_data = {
            max_ms: 5,
            uid: 'uidone',
            fb_uid: 123,
            fb_disconnect_time: undefined,
            game_sounds: {music: false, effects: true}
          },
          get_date_cache = util.getDate,
          device = {uid: 'uidone', advertising_id: 'sdsd'},
          getDate = sinon.stub(util, 'getDate',
          function (fb_disconnect_time) {
            getDate.restore();
            util.getDate = get_date_cache;
            assert.strictEqual(null, fb_disconnect_time);
            done();
          });

        db_get_user.returns(promise({
          device: device,
          game: {max_ms: 1, music_muted: true}
        }));

        db_update_usergame.restore();
        db_update_usergame = sinon.stub(db,
          'updateUserGame',
          function (params) {
            db_update_usergame.restore();
            db_update_usergame = sinon.stub(db, 'updateUserGame');
            assert.strictEqual(params.max_ms, 5);

            return promise(1);
          });
        db_update_device.returns(promise(2));
        user.addOrUpdateUserData(user_data, device);
      });

    it('should pass same value to get date if fb_disconnect_time is not' +
      'undefined', function (done) {
        var user_data = {
            max_ms: 5,
            uid: 'uidone',
            fb_uid: 123,
            fb_disconnect_time: '10:00 AM',
            game_sounds: {music: false, effects: true}
          },
          get_date_cache = util.getDate,
          device = {uid: 'uidone', advertising_id: 'sdsd'},
          getDate = sinon.stub(util, 'getDate',
          function (fb_disconnect_time) {
            getDate.restore();
            util.getDate = get_date_cache;
            assert.strictEqual(
              user_data.fb_disconnect_time,
              fb_disconnect_time
            );
            done();
          });

        db_get_user.returns(promise({
          device: device,
          game: {max_ms: 1, music_muted: true}
        }));

        db_update_usergame.restore();
        db_update_usergame = sinon.stub(db,
          'updateUserGame',
          function (params) {
            db_update_usergame.restore();
            db_update_usergame = sinon.stub(db, 'updateUserGame');
            assert.strictEqual(params.max_ms, 5);

            return promise(1);
          });
        db_update_device.returns(promise(2));
        user.addOrUpdateUserData(user_data, device);
      });
  });

  describe('updateCashPurchase()', function () {
    it('should update user game', function (done) {
      db_get_user_game.returns(promise({
        paid_currency: 300,
        amount_spent: 3
      }));
      db_save_credit.returns(promise(true));
      db_update_usergame.restore();
      db_update_usergame = sinon.stub(db, 'updateUserGame', function (params) {
        db_update_usergame.restore();
        assert.strictEqual(400, params.paid_currency);
        assert.strictEqual(4, params.amount_spent);
        done();
      });

      user._updateCashPurchase({item: 'cash1'});
    });

    it('should not update user game if save failed', function () {
      db_get_user_game.returns(promise({
        paid_currency: 300,
        amount_spent: 3
      }));
      db_save_credit.returns(promise(false));
      db_update_usergame.restore();
      db_update_usergame = sinon.stub(db, 'updateUserGame', function () {
        db_update_usergame.restore();
        assert.strictEqual(true, false);
      });

      user._updateCashPurchase({item: 'cash1'});
    });

    it('should save cash purchases', function (done) {
      db_get_user_game.returns(promise({
        paid_currency: 300,
        amount_spent: 3
      }));
      db_save_credit.restore();
      db_save_credit = sinon.stub(db, 'saveCredits', function () {
        db_save_credit.restore();
        done();
        return promise(true);
      });

      user._updateCashPurchase({item: 'cash1'});
    });
  });

  describe('puzzleUpdateAndGetMax()', function () {
    it('should return send max of send puzzle data', function (done) {
      var puzzles = [{
        id: 1,
        score: 100,
        stars: 1,
        time: 0,
        progress: 80,
        timestamp: 200,
        instance_id: 'id1'
      }, {
        id: 1,
        score: 150,
        time: 0,
        stars: 2,
        progress: 100,
        timestamp: 300,
        app_version: 'app_v',
        instance_id: 'id2',
        exit_type: 'restart'
      }];

      db_get_puzzle_data.returns(promise(null));
      db_add_update_maxpuzzle.returns(promise(1));
      user.puzzleUpdateAndGetMax(puzzles, 'uid_test', 'fb_id',
        'app_d')
        .then(function (resp) {

          var resp_puzzle = resp.puzzle_data;

          assert.deepEqual([{ms_id: 1, score: 150, stars: 2}],
            resp.puzzle_highest);

          assert.strictEqual(resp_puzzle.gs_stats_all.length, 2);

          assert.deepEqual(resp_puzzle.gs_stats_all[0], {
            uid: 'uid_test',
            ms_id: 1,
            date_time: 200,
            time_taken: 0,
            score: 100,
            stars: 1,
            progress: 80,
            app_version: 'app_d',
            bonus_level: 0,
            design_version: null,
            instance_id: 'id1',
            exit_type: null
          });

          assert.deepEqual(resp_puzzle.gs_stats_all[1], {
            uid: 'uid_test',
            ms_id: 1,
            date_time: 300,
            time_taken: 0,
            score: 150,
            stars: 2,
            bonus_level: 0,
            progress: 100,
            design_version: null,
            app_version: 'app_v',
            instance_id: 'id2',
            exit_type: 'restart'
          });
          done();
        }, done);
    });

    it('shouldnt update max puzzle for bonus level', function (done) {
      var puzzles = [{
        id: 1,
        score: 100,
        stars: 1,
        time: 0,
        progress: 100,
        timestamp: 200,
        instance_id: 'id1'
      }, {
        id: 1,
        score: 150,
        time: 0,
        stars: 2,
        progress: 100,
        timestamp: 300,
        bonus_level: 2,
        app_version: 'app_v',
        instance_id: 'id2',
        exit_type: 'restart'
      }];

      db_get_puzzle_data.returns(promise(null));
      db_add_update_maxpuzzle.returns(promise(1));
      user.puzzleUpdateAndGetMax(puzzles, 'uid_test', 'fb_id',
        'app_d')
        .then(function (resp) {

          var resp_puzzle = resp.puzzle_data;

          assert.deepEqual([{ms_id: 1, score: 100, stars: 1}],
            resp.puzzle_highest);

          assert.strictEqual(resp_puzzle.gs_stats_all.length, 2);

          assert.deepEqual(resp_puzzle.gs_stats_all[0], {
            uid: 'uid_test',
            ms_id: 1,
            date_time: 200,
            time_taken: 0,
            score: 100,
            stars: 1,
            progress: 100,
            app_version: 'app_d',
            bonus_level: 0,
            design_version: null,
            instance_id: 'id1',
            exit_type: null
          });

          assert.deepEqual(resp_puzzle.gs_stats_all[1], {
            uid: 'uid_test',
            ms_id: 1,
            date_time: 300,
            time_taken: 0,
            score: 150,
            stars: 2,
            bonus_level: 2,
            progress: 100,
            design_version: null,
            app_version: 'app_v',
            instance_id: 'id2',
            exit_type: 'restart'
          });
          done();
        }, done);
    });

    it('shouldnt return played bonus level id if progress ' +
      '< 100', function (done) {
        var puzzles = [{
          id: 1,
          score: 150,
          time: 0,
          stars: 2,
          progress: 99,
          timestamp: 300,
          bonus_level: 1,
          app_version: 'app_v',
          instance_id: 'id2',
          exit_type: 'restart'
        }];

        db_get_puzzle_data.returns(promise(null));
        db_add_update_maxpuzzle.returns(promise(1));
        user.puzzleUpdateAndGetMax(puzzles, 'uid_test', 'fb_id',
          'app_d')
          .then(function (resp) {
            var resp_puzzle = resp.puzzle_data;

            assert.deepEqual(resp_puzzle.bonus_stats, []);
            assert.deepEqual([], resp.bonus_played);

            done();
          }, done);
      }
    );

    it('should return played bonus level id for fb user', function (done) {
      var puzzles = [{
        id: 1,
        score: 150,
        time: 0,
        stars: 2,
        progress: 100,
        timestamp: 300,
        bonus_level: 1,
        app_version: 'app_v',
        instance_id: 'id2',
        exit_type: 'restart'
      }];

      db_get_puzzle_data.returns(promise(null));
      db_add_update_maxpuzzle.returns(promise(1));
      user.puzzleUpdateAndGetMax(puzzles, 'uid_test', 'fb_id',
        'app_d')
        .then(function (resp) {
          var resp_puzzle = resp.puzzle_data;

          assert.deepEqual(resp_puzzle.bonus_stats, [{
            fb_uid: 'fb_id',
            id: 1,
            date_time: 300
          }]);

          assert.deepEqual([1], resp.bonus_played);
          done();
        }, done);
    });

    it('should return already played bonus levels for user', function (done) {
      var puzzles = [{
        id: 1,
        score: 150,
        time: 0,
        stars: 2,
        progress: 100,
        timestamp: 300,
        app_version: 'app_v',
        instance_id: 'id2',
        exit_type: 'restart'
      }];

      db_get_bonus_puzzles.returns(promise([2]));
      db_get_puzzle_data.returns(promise(null));
      db_add_update_maxpuzzle.returns(promise(1));
      user.puzzleUpdateAndGetMax(puzzles, 'uid_test', 'fb_id',
        'app_d')
        .then(function (resp) {
          var resp_puzzle = resp.puzzle_data;

          assert.deepEqual(resp_puzzle.bonus_stats, []);
          assert.deepEqual([2], resp.bonus_played);

          done();
        }, done);
    });

    it('should return send design version', function (done) {
      var puzzles = [{
        id: 1,
        score: 100,
        time: 0,
        stars: 1,
        progress: 80,
        timestamp: 200,
        design_version: 3,
        instance_id: 'id1'
      }, {
        id: 1,
        score: 150,
        time: 0,
        stars: 2,
        progress: 100,
        timestamp: 300,
        design_version: 2,
        app_version: 'app_v',
        instance_id: 'id2'
      }];

      db_get_puzzle_data.returns(promise([]));
      db_add_update_maxpuzzle.returns(promise(1));
      user.puzzleUpdateAndGetMax(puzzles, 'uid_test', null,
        'app_d')
        .then(function (resp) {

          var resp_puzzle = resp.puzzle_data;

          assert.deepEqual(null, resp.puzzle_highest);
          assert.deepEqual(resp_puzzle.gs_stats_all[0], {
            uid: 'uid_test',
            ms_id: 1,
            date_time: 200,
            time_taken: 0,
            score: 100,
            stars: 1,
            progress: 80,
            bonus_level: 0,
            design_version: 3,
            app_version: 'app_d',
            instance_id: 'id1',
            exit_type: null
          });
          assert.deepEqual(resp_puzzle.gs_stats_all[1], {
            uid: 'uid_test',
            ms_id: 1,
            date_time: 300,
            time_taken: 0,
            score: 150,
            stars: 2,
            bonus_level: 0,
            progress: 100,
            design_version: 2,
            app_version: 'app_v',
            instance_id: 'id2',
            exit_type: null
          });
          assert.strictEqual(resp_puzzle.gs_stats_all.length, 2);
          done();
        }, done);
    });

    it('should return max puzzle data from db', function (done) {
      db_get_puzzle_data.returns(promise({test: 'success'}));
      user.puzzleUpdateAndGetMax(null, null, 1)
        .then(function (resp) {
          assert.deepEqual(resp.puzzle_highest, {test: 'success'});
          done();
        })
        .catch(function (err) {
          done('err:' + err);
        });
    });

    it('should return max puzzle data from request and db', function (done) {
      var puzzles = [
          {id: 'aaa', score: 3000, stars: 3, progress: 100, time: 0},
          {id: 'bbb', score: 500, stars: 2, progress: 100, time: 0},
          {id: 'ddd', score: 1000, stars: 3, progress: 100, time: 0}
        ],
        count = 0;

      db_get_puzzle_data.returns(promise([
        {ms_id: 'ccc', score: 200, stars: 2, time: 0},
        {ms_id: 'bbb', score: 700, stars: 2, time: 0},
        {ms_id: 'ddd', score: 500, stars: 2, time: 0}
      ]));

      db_add_update_maxpuzzle.restore();
      db_add_update_maxpuzzle = sinon.stub(db,
        'addOrUpdateMaxPuzzle', function (puzzle) {
          if (++count === 3) {
            db_add_update_maxpuzzle.restore();
          }

          if (count === 1) {
            assert.deepEqual({
              ms_id: 'aaa',
              fb_uid: 'fb_id',
              score: 3000,
              stars: 3,
              progress: 100,
              time_taken: 0
            }, puzzle);
          }

          return promise(1);
        });

      user.puzzleUpdateAndGetMax(puzzles, 'uid', 'fb_id')
        .then(function (resp) {
          var puzz_data = resp.puzzle_data;

          assert.strictEqual(4, resp.puzzle_highest.length);
          assert.strictEqual(3, puzz_data.gs_stats_all.length);
          done();
        }, function () {
          done('error');
        });
    });

    it('should recalculate max puzzle data of user and save for ' +
      'fb user', function (done) {
        var puzzles = [],
          count = 0;

        db_get_puzzle_data.returns(promise([
          {ms_id: 'ccc', score: 200, stars: 2, time: 0},
          {ms_id: 'bbb', score: 700, stars: 2, time: 0},
          {ms_id: 'ddd', score: 500, stars: 2, time: 0}
        ]));

        db_non_track_puzzles.returns(promise([{
          ms_id: 'aaa',
          score: 3000,
          stars: 3,
          progress: 100,
          time_taken: 0
        }, {
          ms_id: 'bbb',
          score: 500,
          stars: 2,
          progress: 100,
          time_taken: 0
        }, {
          ms_id: 'ddd',
          score: 1000,
          stars: 3,
          progress: 100,
          time_taken: 0
        }]));
        db_add_update_maxpuzzle.restore();
        db_add_update_maxpuzzle = sinon.stub(db,
          'addOrUpdateMaxPuzzle', function (puzzle) {
            if (++count === 3) {
              db_add_update_maxpuzzle.restore();
            }

            if (count === 1) {
              assert.deepEqual({
                fb_uid: 'fb_id',
                ms_id: 'aaa',
                score: 3000,
                stars: 3,
                progress: 100,
                time_taken: 0
              }, puzzle);
            }

            return promise(1);
          });

        user.puzzleUpdateAndGetMax(puzzles, 'uid', 'fb_id', 'ver', true)
          .then(function (resp) {
            db_add_update_maxpuzzle.restore();
            db_add_update_maxpuzzle = sinon.stub(db, 'addOrUpdateMaxPuzzle');
            assert.strictEqual(4, resp.puzzle_highest.length);
            done();
          }, function () {
            done('error');
          });
      }
    );

    it('should send max from db', function (done) {
      var puzzles = [{id: 'aaa', star: 4}];

      db_get_puzzle_data.returns(promise([
        {ms_id: 'aaa', score: 200, stars: 2}
      ]));
      db_add_update_maxpuzzle.returns(promise(1));

      user.puzzleUpdateAndGetMax(puzzles, 'uid', 'fbid')
        .then(function (resp) {
          assert.deepEqual(resp.puzzle_highest,
            [{ms_id: 'aaa', score: 200, stars: 2}]);
          done();
        });
    });

    it('should send max from current sync puzzle data whose progress' +
      ' is 100', function (done) {
        var puzzles = [{id: 'aaa', stars: 3, progress: 100, score: 5000},
          {id: 'bbb', stars: 2, progress: 90}];

        db_get_puzzle_data.returns(promise([
          {ms_id: 'ccc', score: 200, stars: 2}
        ]));
        db_add_update_maxpuzzle.returns(promise(1));

        user.puzzleUpdateAndGetMax(puzzles, 'uid', 'fbid')
          .then(function (resp) {
            assert.deepEqual(resp.puzzle_highest,
              [{ms_id: 'ccc', score: 200, stars: 2},
              {ms_id: 'aaa', score: 5000, stars: 3}]);
            done();
          });
      }
    );

    it('should merge data from game specific items', function (done) {
      var cache_getGsStatsTemplate = sinon.stub(user, 'getGsStatsTemplate',
          function () {
            cache_getGsStatsTemplate.restore();
            return {
              key: 'test'
            };
          }),
        cache_getMaxPuzzleData = sinon.stub(util, 'getMaxPuzzleData',
          function (data) {
            cache_getMaxPuzzleData.restore();

            assert.deepEqual(data, {
              ms_id: 1,
              score: 0,
              stars: 0,
              progress: 0,
              time_taken: 0,
              key: 'test'
            });
            done();
            return {};
          });

      db_non_track_puzzles.returns(promise([{
        key: 'test',
        ms_id: 1
      }]));

      user.puzzleUpdateAndGetMax([], 'uid', 'fbid', '0.50', 'pp')
      .catch(function () {
        done('err');
      });
    });

    it('should reject for error in getNonTrackedDevicePuzzles',
      function (done) {
      db_non_track_puzzles.returns(promise());
      db_add_update_maxpuzzle.returns(promise(1));

      user.puzzleUpdateAndGetMax([], 'uid', 'fbid', '0.50', 'pp')
        .then(function () {
          done('err');
        })
        .catch(function (err) {
          assert.deepEqual(err, {
            no: 0,
            stack: 'Over here'
          });
          done();
        });
    });
  });

  describe('saveCredits()', function () {
    it('should call save credits to db with proper params', function (done) {
      var credits = [
          {
            curr_ms: 1,
            time: new Date(2),
            item: 3,
            amount: 4,
            currency: 5,
            usd_amount: 3,
            receipt: 6,
            source: 'direct',
            sale_id: null
          }, {
            curr_ms: 2,
            time: new Date(3),
            item: 4,
            amount: 5,
            usd_amount: 3,
            currency: 6,
            receipt: 7,
            source: 'unknown',
            sale_id: null
          }
        ],
        util_date = sinon.stub(util, 'getDate', function () {
          return new Date(4);
        });

      db_save_credit.restore();
      db_save_credit = sinon.stub(db, 'saveCredits');
      db_save_credit.withArgs([{
        uid: 1,
        ms: 1,
        date_time: new Date(4),
        currency_bundle: 3,
        usd_amount: 3,
        local_amount: 4,
        local_currency: 5,
        receipt: 6,
        source: 'direct',
        added_on: new Date(4),
        sale_id: null
      }, {
        uid: 1,
        ms: 2,
        date_time: new Date(4),
        usd_amount: 3,
        currency_bundle: 4,
        local_amount: 5,
        local_currency: 6,
        receipt: 7,
        source: 'unknown',
        added_on: new Date(4),
        sale_id: null
      }])
      .returns(promise(2));

      user.saveCredits(credits, 1)
        .then(function () {
          util_date.restore();
          done();
        })
        .catch(function () {
          done('error');
        });
    });

    it('should call saveCredits with current date if date not ' +
      'passed', function (done) {
        db_save_credit.restore();
        db_save_credit = sinon.stub(db, 'saveCredits', function (data) {
          done(data[0].date_time instanceof Date ? undefined : 'error');
        });

        user.saveCredits([{
          curr_ms: 2,
          item: 4,
          amount: 5,
          usd_amount: 3,
          currency: 6,
          receipt: 7,
          source: 'unknown',
          sale_id: null
        }]);
      }
    );

    it('should return empty object for no data', function (done) {
      db_save_credit.restore();
      db_save_credit = sinon.stub(db, 'saveCredits', function () {
        return promise([]);
      });

      user.saveCredits()
      .then(function (data) {
        assert.deepEqual(data, []);
        done();
      });
    });
  });

  describe('savePurchases()', function () {
    it('should call save purchases with proper params', function (done) {
      var purchases = [
          {
            item: 'test1',
            time: new Date(1),
            quantity: 1,
            currency_used: {},
            instance_id: 'id1'
          }, {
            item: 'test2',
            time: new Date(2),
            quantity: 2,
            currency_used: {},
            instance_id: 'id2'
          }
        ],
        clone = _.clone(purchases);

      db_save_purchase.withArgs([{
        uid: 1,
        item: 'test1',
        date_time: new Date(1),
        quantity: 1,
        free_currency: 0,
        paid_currency: 0,
        remaining_currency: 0,
        curr_ms: 1,
        max_ms: 1,
        instance_id: 'id1'
      }, {
        uid: 1,
        item: 'test2',
        date_time: new Date(2),
        quantity: 2,
        free_currency: 0,
        paid_currency: 0,
        remaining_currency: 0,
        curr_ms: 1,
        max_ms: 1,
        instance_id: 'id2'
      }]).returns(promise(1));

      user.savePurchases(clone, 1)
        .then(function () {
          done();
        }, function () {
          done('error');
        });
    });

    it('should call savePurchase with current date if date not ' +
      'passed', function (done) {
        db_save_purchase.restore();
        db_save_purchase = sinon.stub(db, 'savePurchases', function (data) {
          done(data[0].date_time instanceof Date ? undefined : 'error');
        });

        user.savePurchases([{
          item: 'test1',
          quantity: null,
          currency_used: {},
          instance_id: 'id1'
        }]);
      }
    );
    it('should return empty object for no data', function (done) {
      db_save_purchase.restore();
      db_save_purchase = sinon.stub(db, 'savePurchases', function () {
        return promise({});
      });

      user.savePurchases()
      .then(function (data) {
        assert.deepEqual(data, []);
        done();
      });
    });
  });

  describe('postSync()', function () {
    it('should get proper data', function (done) {
      var data = {
        user: {
          info: {
            first_name: 'Test',
            last_name: 'User',
            info: {},
            uid: '1234'
          }, game: {
            paid_currency: 1000,
            free_currency:500,
            max_ms: 2,
            master_score: 37
          }
        },
        requests: [],
        fb_id: 'fb_uid',
        job_data: {}
      };

      db_get_user_by_fbid.withArgs('fb_uid')
        .returns(promise(data.user));

      db_get_unlocked_powerups.withArgs('fb_uid')
        .returns(promise([]));

      user.postSync(data)
        .then(function (sync_data) {
          var expected_user = {
              free_currency: 500,
              paid_currency: 1000,
              max_ms: 2,
              master_score: 37,
              unlocked_powerups: []
            },
            actual_user = sync_data.user;
          assert.deepEqual(expected_user, actual_user);
          done();
        });
    });

    it('should have corresponding request data if fb ' +
      'connected', function (done) {
        var user_data = {
          info: {
            first_name: 'Test',
            last_name: 'User',
            uid: '1234'
          },
          game: {
            paid_currency: 1000,
            free_currency: 500,
            max_ms: 2,
            master_score: 37
          }
        };

        db_get_user_by_fbid.withArgs(209)
          .returns(promise(user_data));
        db_get_requests.withArgs(209)
          .returns(promise([1, 2, 3]));
        db_get_unlocked_powerups.withArgs(209)
          .returns(promise([]));

        user.postSync({
            fb_id: 209,
            job_data: {}
          })
          .then(function (sync_data) {
            var expected_data = {
              requests: [1,2,3],
              user: {
                max_ms: 2,
                master_score: 37,
                paid_currency: 1000,
                free_currency: 500,
                unlocked_powerups: []
              }
            };

            assert.deepEqual(sync_data, expected_data);
            done();
          });
      }
    );
  });

  describe('addRequests', function () {
    it('should call insertRequest with proper data', function (done) {
      var data = {
        request: 1,
        type: 'something',
        sender: 122,
        fb_action: 'send',
        to: [123, 345]
      },
      cache = user.removeOldRequests;

      db_ins_requests.withArgs([
          {
            request_id: 1,
            sender: 122,
            receiver: 123,
            type: 'something',
            fb_action: 'send'
          },
          {
            request_id: 1,
            sender: 122,
            receiver: 345,
            type: 'something',
            fb_action: 'send'
          }
        ])
      .returns(promise(1));
      user.removeOldRequests = function () {
        return promise(1);
      };
      user.addRequests(data)
        .then(function () {
          user.removeOldRequests = cache;
          done();
        });
    });

    it('should reject if error occurs', function (done) {
      var data = {
        request: 1,
        type: 'something',
        sender: 122,
        fb_action: 'send',
        to: [123, 345]
      },
      cache = user.removeOldRequests;

      user.removeOldRequests = function () {
        return promise();
      };

      user.addRequests(data)
        .then(function () {
          done('err');
        })
        .catch(function (err) {
          user.removeOldRequests = cache;
          assert.deepEqual(err, {
            no: 0,
            stack: 'Over here'
          });
          done();
        });
    });
  });

  describe('removeOldRequests', function () {
    it('should call db.removeOldRequests', function (done) {
      db_remove_old_reqs
        .returns(promise([
          {request_id: 1, sender: 2, receiver: 4, type: 'gift'},
          {request_id: 1, sender: 2, receiver: 8, type: 'gift'}
        ]));

      fb_delete_req.onCall(1).returns(done());
      user.removeOldRequests({request_id: 2, sender: 2, receiver: 4});
    });
  });

  describe('updateRequest()', function () {
    it('should create array from passed data', function (done) {
      var req = {request_id: 1, sender: 4, receiver: 8};

      db_update_requests.returns(promise(1));

      user.updateRequest(req)
        .then(function (data) {
          assert.deepEqual(true, data);
          done();
        });
    });

    it('should call facebook deleteRequest', function (done) {
      var req = {request_id: 1, sender: 4, receiver: 8},
        fb_cache = fb.deleteRequest;

      db_update_requests.returns(promise(1));

      fb.deleteRequest = function () {
        fb.deleteRequest = fb_cache;
        done();
      };

      user.updateRequest(req);
    });

    it('should not call fb deleteRequest', function (done) {
      var req = {request_id: 1, sender: 4, receiver: 8},
        fb_cache = fb.deleteRequest;

      db_update_requests.returns(promise());

      fb.deleteRequest = function () {
        fb.deleteRequest = fb_cache;
        done('err');
      };

      user.updateRequest(req)
        .then(function () {
        }, function () {
          done();
        });
    });
  });

  describe('updatePuzzles()', function () {
    it('should call insertPuzzleAll', function (done) {
      db_insert_puzzle_all.restore();
      db_insert_puzzle_all = sinon.stub(db, 'insertPuzzleAll', function () {
        done();
        return promise(1);
      });

      user._updatePuzzles([{}, {}]);
    });

    it('should call addOrUpdateMaxPuzzle', function (done) {
      var count = 0;

      db_add_update_maxpuzzle.restore();
      db_add_update_maxpuzzle = sinon.stub(db, 'addOrUpdateMaxPuzzle',
        function () {
          if (++count === 2) {
            done();
          }
          return promise(1);
        }
      );

      user._updatePuzzles([{}, {}], true);
    });

    it('should return false if any update fails', function (done) {
      var count = 0;

      db_add_update_maxpuzzle.restore();
      db_add_update_maxpuzzle = sinon.stub(db, 'addOrUpdateMaxPuzzle',
        function () {
          ++count;
          if (count === 2) {
            done('error');
          } else if (count === 1) {
            done();
          }
          return promise();
        }
      );

      user._updatePuzzles([{}, {}], true)
      .catch(function (err) {
        assert.deepEqual(err, {
          no: 0,
          stack: 'Over here'
        });
      });
    });
  });

  describe('insertPuzzleData', function () {
    it('should invoke puzzle inserts', function (done) {
      db_insert_puzzle_all.restore();
      db_add_update_maxpuzzle.restore();
      db_get_puzzle_data.restore();
      var count = 0,
        db_all, db_max;

      db_insert_puzzle_all.restore();
      db_add_update_maxpuzzle.restore();
      db_get_puzzle_data.restore();

      db_all = sinon.stub(db, 'insertPuzzleAll', function () {
        ++count;
        db_all.restore();
        done();
        return promise(1);
      });
      db_max = sinon.stub(db, 'addOrUpdateMaxPuzzle', function () {
        done('error: addOrUpdateMaxPuzzle called');
      });

      user.insertPuzzleData({
        gs_stats_all: [{}],
        gs_stats: [{}]
      });
    });

    it('shouldnt invoke getUserPuzzle if no fbid', function (done) {
      db_get_puzzle_data.returns(promise(50));

      user.insertPuzzleData({}, 'uid')
        .then(function (data) {
          done(data !== 50 ? undefined : 'error');
        });
    });
  });

  describe('trackFreeGoods()', function () {
    it('should resolve with response from db for new data', function (done) {
      db_track_free_goods.withArgs([{
          uid: 'uid',
          item: null,
          ms: 1,
          time: new Date(1),
          amount: 10,
          source: 's',
          instance_id: 123
        }, {
          uid: 'uid',
          item: 'cash',
          ms: 2,
          time: new Date(2),
          amount: 200,
          source: 'q',
          instance_id: null
        }])
        .returns(promise(1));

      user.trackFreeGoods([{
          curr_ms: 1,
          item: null,
          time: new Date(1),
          amount: 10,
          source: 's',
          instance_id: 123
        }, {
          curr_ms: 2,
          item: 'cash',
          time: new Date(2),
          amount: 200,
          source: 'q',
          instance_id: undefined
        }],
        'uid', 'did')
        .then(function (res) {
          done(res === 1 ? undefined : 'error');
        });
    });

    it('should resolve for empty data', function (done) {
      user.trackFreeGoods([])
      .then(function (data) {
        assert.deepEqual(data, null);
        done();
      });
    });
  });

  describe('getCurrentUserGameData()', function () {
    it('should calculate powerups and currencies in currenct ' +
      'transaction', function () {
        var user_data = {
            purchases: [{
              item: 'hint',
              quantity: 4,
              currency_used: {
                free: 20,
                paid: 40
              }
            }, {
              item: 'go_red',
              quantity: 5,
              currency_used: {
                free: 10,
                paid: 30
              }
            }],
            credits: [{
              item: 'cash1'
            }],
            track_free_goods: [{
              item: 'lives',
              amount: 5
            }, {
              item: 'hint',
              amount: 4
            }]
          },
          puzzles = [{
          }, {
            hint: 1
          }],
          curr_data;

        config.powerups.pregame = ['hint'];
        curr_data = user._getCurrentUserGameData(user_data, puzzles);

        assert.deepEqual(curr_data, {
          free_currency: -30,
          paid_currency: 30,
          hint: 7,
          amount_spent: 1
        });
      }
    );

    it('should calculate powerups and currencies in currenct ' +
      'db entries', function () {
        var user_data = {
            purchases: [{
              item: 'hint',
              quantity: 5,
              free_currency: 10,
              paid_currency: 30
            }],
            credits: [{
              currency_bundle: 'cash1'
            }],
            track_free_goods: [{
              item: 'hint',
              amount: 4
            }]
          },
          puzzles = [{
            hint: 0
          }, {
            hint: 0
          }],
          curr_data = user._getCurrentUserGameData(user_data, puzzles);

        assert.deepEqual(curr_data, {
          free_currency: -10,
          paid_currency: 70,
          hint: 9,
          amount_spent: 1
        });
      }
    );
  });

  describe('getFreeGoodsCount', function () {
    it('it should do early return for empty lives', function () {
      var obj = user._getFreeGoodsCount([
        {item: 'lives'}
      ]);

      assert.deepEqual({
        cash: 0,
        hint: 0
      }, obj);
    });

    it('it return proper amount for goods', function () {
      var obj = user._getFreeGoodsCount([{
        item: 'hint',
        amount: 4
      }]);

      assert.deepEqual({
        cash: 0,
        hint: 4
      }, obj);
    });
  });

  describe('getUsedPowerups', function () {
    it('it should return count after adjusting powerups', function () {
      var obj;

      config.powerups.pregame = ['hint'];

      obj = user._getUsedPowerups([
        {hint: 2}
      ]);

      assert.deepEqual({
        hint: 1
      }, obj);
    });
  });

  describe('getCreditData', function () {
    it('it should return credits data adjusted by use', function () {
      var obj = user._getCreditData([
        {item: 'cash2'},
        {item: 'cash2'}
      ]);

      assert.deepEqual({
        currency: 1050,
        spent: 10
      }, obj);
    });
  });

  describe('getPurchaseData', function () {
    it('it should return purchase data after processing', function () {
      var obj = user._getPurchaseData([{
        item: 'hint',
        quantity: 5,
        free_currency: 10,
        paid_currency: 30
      }]);

      assert.deepEqual({
        currency_used: {
          free: 10,
          paid: 30
        },
        powerup_count: {
          hint: 5
        }
      }, obj);
    });
  });

  describe('revertGameSyncUpdate()', function () {
    it('should update user game by reducing duplicate added ' +
      'powerups', function (done) {
        db_get_user_game.returns(promise({
          free_currency: 300,
          paid_currency: 0
        }));

        db_update_usergame.restore();
        db_update_usergame = sinon.stub(db, 'updateUserGame', function (data) {
          assert.strictEqual(data.free_currency, 320);
          assert.strictEqual(data.paid_currency, 40);
          done();
        });

        return user.revertGameSyncUpdate({
          purchases: [{
            item: 'double_trouble',
            quantity: 4,
            free_currency: 20,
            paid_currency: 40
          }
        ]});
      }
    );

    it('shouldnt update if no difference in data', function (done) {
      db_update_usergame.restore();

      db_update_usergame = sinon.stub(db, 'updateUserGame', function () {
        done('Invoked');
      });

      user.revertGameSyncUpdate({})
        .then(function () {
          done();
        });
    });
  });

  describe('getCurrentSale', function () {
    it('should return the difference b/w start and end time', function (done) {
      var cache = Date.now;

      db_get_sale_data.returns(promise({
        id: 1,
        name: 'Test sale',
        start: '2017-08-25',
        end: '2018-08-25',
        platforms: 'android'
      }));
      Date.now = function () {
        Date.now = cache;
        return 1507295250514;
      };
      user.getCurrentSale('android')
        .then(function (data) {
          assert.strictEqual(data.duration, 27859949);
        });
      done();
    });

    it('should not set duration ' +
      'if no data is returned from db', function (done) {
      db_get_sale_data.returns(promise(null));
      user.getCurrentSale('android')
        .then(function (data) {
          assert.strictEqual(data, null);
        });
      done();
    });
  });
  describe('getCrossPromotion()', function () {
    it('should return cross prommotion data for passed store', function () {
      var android = {
        url: 'store_url',
        package: 'com.hc.sq',
        name: 'Sudoku Quest',
        universal_link: 'https://universal'
      },
      dvce = {
        store: 'android'
      },
      usr = {
        max_ms: 35
      };

      config.cross_promotion = {
        id: 'test_sq',
        min_ms: 30,
        android: android
      };
      assert.deepEqual({
        url: 'store_url',
        package: 'com.hc.sq',
        name: 'Sudoku Quest',
        id: 'test_sq',
        universal_link: 'https://universal'
      }, user.getCrossPromotion(dvce, usr));
      config.cross_promotion = null;
    });

    it('should return null for passed store is disabled', function () {
      var android = {
        url: 'store_url',
        package: 'com.hc.sq',
        name: 'Sudoku Quest'
      },
      dvce = {
        store: 'ios'
      },
      usr = {
        max_ms: 32
      };

      config.cross_promotion = {
        id: 'test_sq',
        min_ms: 30,
        android: android
      };

      config.cross_promotion = {
        id: 'test_sq',
        min_ms: 30,
        android: android
      };
      assert.strictEqual(null, user.getCrossPromotion(dvce, usr));
      config.cross_promotion = null;
    });
  });

  describe('getRewards()', function () {
    it('should return proper rewards', function () {
      var reward_data = [{
          id: 'xyz1',
          min_ms: 10,
          paid_user: false
        },
        {
          id: 'xyz2',
          min_ms: 15,
          paid_user: true
        },
        {
          id: 'xyz3',
          min_ms: 10,
          paid_user: true
        },
        {
          id: 'xyz4',
          min_ms: 12,
          paid_user: false
        }],
        data = {
          user: {
            max_ms: 12,
            paid: 100
          },
          rewards_given: ['xyz2']
        };

      config.rewards = reward_data;
      assert.deepEqual(reward_data[2], user.getRewards(data.user, '1.0'));
      config.reward_data = [];
    });

    it('shouldnt return rewards if version less than min reward ' +
      'version', function () {
        var reward_data = [{
            id: 'xyz1',
            min_ms: 10,
            paid_user: false
          },
          {
            id: 'xyz2',
            min_ms: 15,
            paid_user: true
          },
          {
            id: 'xyz3',
            min_ms: 10,
            paid_user: true
          },
          {
            id: 'xyz4',
            min_ms: 12,
            paid_user: false
          }],
          data = {
            user: {
              max_ms: 12,
              paid: 100
            },
            rewards_given: ['xyz2']
          };

        config.rewards = reward_data;
        config.rewards_min_version = '1.2';
        assert.deepEqual(null, user.getRewards(data.user, '1.0'));
        config.reward_data = [];
      }
    );
  });

  describe('getSaleAndRewards()', function () {
    var get_sale_data, get_reward_data;

    before(function () {
      get_sale_data = sinon.stub(user, 'getCurrentSale');
      get_reward_data = sinon.stub(user, 'getRewards');
    });

    after(function () {
      get_sale_data.restore();
      get_reward_data.restore();
    });
    it('should return proper rewards and sale', function (done) {
      get_sale_data.returns(promise(null));
      get_reward_data.returns({});

      user.getSaleAndRewards({}, {})
        .then(function (data) {
          assert.deepEqual(data, {sale: null, rewards: {}});
          done();
        });
    });

    it('should return null for reward if sale is on', function (done) {
      get_sale_data.returns(promise({}));
      get_reward_data.returns({});

      user.getSaleAndRewards({}, {})
        .then(function (data) {
          assert.deepEqual(data, {sale: {}, rewards: null});
          done();
        });
    });
  });

  describe('updateCurrency', function () {
    it('calculate new currency values when available currency more than' +
      'used currency', function (done) {
      var user_game = {
        uid: 'AndroidPhone-1490763681112-26ad4e75-287e-40ce-9587-d9871816ad81',
        fb_uid: '10209792695310451',
        paid_currency: 4475,
        free_currency: 400
      },
        new_user_game = _.clone(user_game),
        free_currency = 10,
        transactions = {
          free_currency: 0, paid_currency: 50
        },
        purchases = {
          currency_bundle: 'cash1'
        };

      new_user_game.free_currency = user_game.free_currency -
        transactions.free_currency + free_currency;
      new_user_game.paid_currency = user_game.paid_currency -
        transactions.paid_currency + cash[purchases.currency_bundle].no_of_cash;

      db_get_last_disconnect_time.returns(promise(0));
      db_get_used_currency.returns(promise([transactions]));
      db_get_purchases_from.returns(promise([purchases]));
      db_get_free_currency_from.returns(promise(free_currency));

      user._updateCurrency(user_game, 'hc1', 1, 1)
        .then(function (resp) {
          assert.deepEqual(user_game, new_user_game);
          done(resp);
        });
    });

    it('currency not to go above 0 when used is more than available' +
      'used currency', function (done) {
      var user_game = {
        uid: 'AndroidPhone-1490763681112-26ad4e75-287e-40ce-9587-d9871816ad81',
        fb_uid: '10209792695310451',
        paid_currency: 25,
        free_currency: 400
      },
        new_user_game = _.clone(user_game),
        free_currency = 10,
        transactions = {
          free_currency: 500, paid_currency: 550
        },
        purchases = {
          currency_bundle: 'cash1'
        };

      new_user_game.free_currency = 0;
      new_user_game.paid_currency = 0;

      db_get_last_disconnect_time.returns(promise(0));
      db_get_used_currency.returns(promise([transactions]));
      db_get_purchases_from.returns(promise([purchases]));
      db_get_free_currency_from.returns(promise(free_currency));

      user._updateCurrency(user_game, 'hc1', 1, 1)
        .then(function (resp) {
          assert.deepEqual(user_game, new_user_game);
          done(resp);
        });
    });

    it('no change when prev fb id and curr fb are different', function (done) {
      var user_game = {
        uid: 'AndroidPhone-1490763681112-26ad4e75-287e-40ce-9587-d9871816ad81',
        fb_uid: '10209792695310451',
        paid_currency: 4475,
        free_currency: 400
      },
        new_user_game = _.clone(user_game),
        free_currency = 10,
        transactions = {
          free_currency: 0, paid_currency: 50
        },
        purchases = {
          currency_bundle: 'cash1'
        };

      db_get_last_disconnect_time.returns(promise(0));
      db_get_used_currency.returns(promise([transactions]));
      db_get_purchases_from.returns(promise([purchases]));
      db_get_free_currency_from.returns(promise(free_currency));

      user._updateCurrency(user_game, 'hc1', 1, 2)
        .then(function (resp) {
          assert.deepEqual(user_game, new_user_game);
          done(resp);
        });
    });
  });

  describe('verifyAndSavePurchase', function () {
    it('should return proper values', function (done) {
      purchase_val.returns(promise('valid'));
      user._updateCashPurchase = function () {
        return promise(true);
      };

      user.verifyAndSavePurchase({purchase_details: {receipt: '{}'}})
        .then(function() {
          purchase_val.restore();
          done();
        });
    });

    it('should not save cash purchase if already saved', function () {
      purchase_val.returns(promise('valid'));
      user._updateCashPurchase = function () {
        assert.strictEqual(true, false);
        return promise(true);
      };

      user.verifyAndSavePurchase({purchase_details: {
        receipt: '{}',
        synced: true
      }})
      .then(function() {
      });
    });
  });
  describe('getNotifications()', function () {
    it('should build proper notificattion data', function () {
      var data = {
        id: 'notif',
        time: new Date(),
        title: 'title',
        content: 'content_data'
      };

      assert.deepEqual([{
        uid: 'uid',
        notification_id: data.id,
        opened_on: data.time,
        segment: 'abc_segment',
        title: data.title,
        content: data.content
      }], user._getNotifications('uid', [{
        notification_id: data.id,
        clicked_time: data.time,
        title: data.title,
        body: data.content,
        additional_data: '{\"segment_name\": \"abc_segment\"}'
      }]));
    });

    it('should set segment name null', function () {
      var data = {
        id: 'notif',
        time: new Date(),
        title: 'title',
        content: 'content_data'
      };

      assert.deepEqual([{
        uid: 'uid',
        notification_id: data.id,
        opened_on: data.time,
        segment: null,
        title: data.title,
        content: data.content
      }], user._getNotifications('uid', [{
        notification_id: data.id,
        clicked_time: data.time,
        title: data.title,
        body: data.content
      }]));
    });

    it('should return empty', function () {
      assert.deepEqual([], user._getNotifications('uid', []));
    });
  });

  describe('pushSyncJobs()', function () {
    it('should push sync_inserts omiting notifications', function (done) {
      job_push.restore();
      job_push = sinon.stub(job, 'push', function (job_name, data) {
        assert.strictEqual('sync_inserts', job_name);
        assert.deepEqual({
          uid: 1,
          puzzles: []
        }, data);
        done();
      });

      user._pushSyncJobs({
        uid: 1,
        puzzles: [],
        notifications: []
      });
    });

    it('should push sync_notifications with notifications and ' +
      'uid', function (done) {
        var count = 0;

        job_push.restore();
        job_push = sinon.stub(job, 'push', function (job_name, data) {
          if (++count === 2) {
            assert.strictEqual('sync_notifications', job_name);
            assert.deepEqual({
              uid: 1,
              notifications: ['abc']
            }, data);
            done();
          }
        });

        user._pushSyncJobs({
          uid: 1,
          puzzles: [],
          notifications: ['abc']
        });
      }
    );
  });

  describe('insertNotifications()', function () {
    it('should invoke db insert', function (done) {
      var insert_push = sinon.stub(db, 'insertPushNotifications',
        function () {
          return promise(2);
        });

      user.insertNotifications(1, ['abc'])
        .then(function (res) {
          insert_push.restore();
          done(res === 2 ? undefined : 'error');
        });
    });

    it('shouldnt invoke db insert', function (done) {
      var insert_push = sinon.stub(db, 'insertPushNotifications',
        function () {
          done('error');
        });

      user.insertNotifications(1, [])
        .then(function (res) {
          insert_push.restore();
          done(res === true ? undefined : 'error');
        });
    });
  });
});
