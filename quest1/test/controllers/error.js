var error_controller,
  response_handler,
  validator, logger,
  sinon = require('sinon'),
  assert = require('assert'),
  express = require('../lib/express');

describe('Error Controller', function () {
  'use strict';

  var req = {validate: function () {}},
    resp = {error: function () {}},
    rules, stub_logger;

  before(function () {
    express = require('../lib/express');
    error_controller = require('../../controllers/error');
    response_handler = require('../../modules/response');
    validator = require('../../modules/validator');
    logger = require('../../modules/debug')('ERROR');

    error_controller.create(req, resp);
    rules = error_controller._rules;

    stub_logger = sinon.stub(logger, 'error');
  });

  after(function () {
    stub_logger.restore();
  });

  describe('create()', function () {
    it('should send 500 if validation fails', function (done) {
      var req = {
          body: {
            user: 'uid',
            device: {
              adverising_id: 'asasa',
              install_date: 'hg'
            }
          }
        },
        res = express.response(function (status) {
          done(status === 500 ? undefined : 'wrong status');
        });

      response_handler(req, res, function () {});
      validator.express(req, res);
      error_controller.create(req, res);
    });

    it('should send 200 if validation succes', function (done) {
      var req = {
          body: {
            user: 'uid',
            device: {
              adverising_id: 'asasa'
            },
            errors: []
          }
        },
        res = express.response(function (status) {
          assert.strictEqual(200, status);
        }, function (data) {
          done(data === true ? undefined : 'wrong data');
        });

      response_handler(req, res, function () {});
      validator.express(req, res);
      error_controller.create(req, res);
    });

    it('should log errors in request properly', function (done) {
      var date = Date.now(),
        req = {
          body: {
            user: 'uid',
            device: {
              adverising_id: 'asasa'
            },
            errors: [{
              timestamp: date,
              message: 'Reference Error',
              url: 'src/Application.js',
              line: 201,
              col: 30,
              count: 1
            }]
          }
        },
        res = express.response(function (status) {
          assert.strictEqual(200, status);
        }, function (data) {
          done(data === true ? undefined : 'wrong data');
        });

      stub_logger.restore();
      stub_logger = sinon.stub(logger, 'error', function (err, opts) {
        stub_logger.restore();
        stub_logger = sinon.stub(logger, 'error');
        assert.strictEqual('CLIENT_ERR', opts.title);
        assert.strictEqual('uid', opts.msg);
        assert.deepEqual({
          timestamp: new Date(date),
          message: 'Reference Error',
          url: 'src/Application.js',
          line: 201,
          col: 30,
          count: 1,
          last_occured: undefined
        }, opts.data);
      });
      response_handler(req, res, function () {});
      validator.express(req, res);
      error_controller.create(req, res);
    });
  });
});
