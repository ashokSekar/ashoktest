var update, express,  response_handler,
  validator, update_data, assert, config;

describe('Update Controller', function () {
  'use strict';

  before(function () {
    express = require('../lib/express');
    response_handler = require('../../modules/response');
    validator = require('../../modules/validator');
    update = require('../../controllers/update');
    update_data = require('../../resources/updates.json');
    assert = require('assert');
    config = require('../../resources/config');
  });

  describe('get()', function () {
    it('should send 500 if validation fails', function (done) {
      var values = {
          query: {
            platform: 'android'
          }
        },
      req = express.request(values),
      res = express.response(function (status) {
        done(status === 500 ? undefined :  'wrong status');
      });
      response_handler(req, res, function () {});
      validator.express(req, res);
      update.get(req, res);
    });

    it('should check for new release if validation pass', function (done) {
      var values = {
          query: {
            platform: 'android',
            version: '1.0.1'
          }
        },

      req = express.request(values),
      res = express.response(function () {
        done();
      });
      response_handler(req, res, function () {});
      validator.express(req, res);
      update.get(req, res);
    });

    it('should return release details if ' +
      'version is higher', function (done) {
      var values =
        {
          query: {
            platform: 'android',
            version: '1.0.1'
          }
        },
        cache_parse = JSON.parse,
        req, res;

      JSON.parse = function () {
        JSON.parse = cache_parse;
        return {
          latest: {
            ios: '0.0.1',
            android: '1.0.2',
            amazon: '0.0.1'
          },
          info: {
            ios: {},
            android: {
              '1.0.2': {
                message: 'We upgraded the base IDE',
                force_update: false
              }
            },
            amazon: {}
          }
        };
      };

      req = express.request(values);
      res = express.response(null, function (data) {
        assert.notEqual(data.data, null);
        assert.notEqual(data.data, undefined);
        done();
      });
      update_data.latest.android = '10.0.1';
      update_data.info.android = {'10.0.1': {}};
      response_handler(req, res, function () {});
      validator.express(req, res);
      update.get(req, res);
    });

    it('should sent null if there is req version is latest', function (done) {
      var values = {
        query: {
          platform: 'android',
          version: '1.0.1'
        }
      },
      req = express.request(values),
      res = express.response(null, function (data) {
        assert.strictEqual(data, undefined);
        done();
      });

      update_data.latest.android = '1.0.0';
      update_data.info.android = {'1.0.1': {}};
      response_handler(req, res, function () {});
      validator.express(req, res);
      update.get(req, res);
    });

    it('should sent null if there is two versions are equal', function (done) {
      var values = {
        query: {
          platform: 'android',
          version: '1.0.1'
        }
      },
      req = express.request(values),
      res = express.response(null, function (data) {
        assert.strictEqual(data, undefined);
        done();
      });

      update_data.latest.android = '1.0.1';
      update_data.info.android = {'1.0.1': {}};
      response_handler(req, res, function () {});
      validator.express(req, res);
      update.get(req, res);
    });

    it('should sent error if update conf not exist', function (done) {
      var values = {
        query: {
          platform: 'android',
          version: '1.0.1'
        }
      },
      conf_cache = config.update_available_path,
      req = express.request(values),
      res = express.response(null, function (data) {
        config.update_available_path = conf_cache;
        assert.strictEqual(data, 'Something went wrong');
        done();
      });

      config.update_available_path = 'it/is/an/unknown/path';
      update_data.latest.android = '1.0.1';
      update_data.info.android = {'1.0.1': {}};
      response_handler(req, res, function () {});
      validator.express(req, res);
      update.get(req, res);
    });
  });
});
