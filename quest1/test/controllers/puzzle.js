var puzzle, express, db,
  response_handler, validator,
  promise = require('../lib/fakePromise').promise,
  sinon = require('sinon');

describe('Puzzle Controller', function () {
  'use strict';

  var db_getPuzzle;

  before(function () {

    express = require('../lib/express');
    response_handler = require('../../modules/response');
    db = require('../../models/db/db');
    validator = require('../../modules/validator');
    puzzle = require('../../controllers/puzzle');

    db_getPuzzle = sinon.stub(db, 'getPuzzle');
  });

  after(function () {
    db_getPuzzle.restore();
  });

  describe('get()', function () {
    it('should send 500 if validation fails', function (done) {
      var values = {
          params: {puzzle_id: 'abc'}
        },
        req = express.request(values),
        res = express.response(function (status) {
          done(status === 500 ? undefined :  'wrong status');
        });

      // append middleware
      response_handler(req, res, function () {});
      validator.express(req, res);
      puzzle.get(req, res);
    });

    it('should return send since validation passes', function (done) {
      var values = {
          params: {puzzle_id: '1'}
        },
        req = express.request(values),
        res = express.response(function (status) {
          console.log(status);
          done(status === 200 ? undefined : 'wrong status');
        });

      db_getPuzzle.returns(promise({}));

      response_handler(req, res, function () {});
      // append middleware
      validator.express(req, res);

      puzzle.get(req, res);
    });

    it('should return puzzle data of puzzle 2', function (done) {
      var values = {
          params: {puzzle_id: '2'}
        },
        req = express.request(values),
        res = express.response(null, function (data) {
          done(data.id === values.id ? undefined: 'Wrong data');
        });

      db_getPuzzle.returns(promise(values));

      response_handler(req, res, function () {});
      validator.express(req, res);
      puzzle.get(req, res);
    });

    it('should throw error coz id 0 doesn\'t exist', function (done) {
      var values = {
          params: {puzzle_id: '0'}
        },
        req = express.request(values),
        res = express.response(function (status) {
          done(status === 500 ? undefined : 'wrong status ' + status);
        });

      db_getPuzzle.returns(promise());

      response_handler(req, res, function () {});
      validator.express(req, res);
      puzzle.get(req, res);
    });
  });
});
