var kochava_controller,
  response_handler,
  validator, kochava,
  bluebird = require('bluebird'),
  assert = require('assert'),
  express = require('../lib/express');

describe('Kochava Controller', function () {
  'use strict';

  var req = {validate: function () {}},
    resp = {error: function () {}},
    rules;

  before(function () {
    express = require('../lib/express');
    kochava_controller = require('../../controllers/kochava');
    response_handler = require('../../modules/response');
    validator = require('../../modules/validator');
    kochava = require('../../modules/kochava');

    kochava_controller.index(req, resp);
    rules = kochava_controller._rules;
  });

  describe('index()', function () {
    it('should send 500 if validation fails', function (done) {
      var req = {
          body: {
          },
          query: {
            id: 5
          }
        },
        res = express.response(function (status) {
          done(status === 500 ? undefined : 'wrong status');
        });

      response_handler(req, res, function () {});
      validator.express(req, res);
      kochava_controller.index(req, res);
    });

    it('should send 200 if validation succes', function (done) {
      var cache = kochava.insertEvent,
        req = {
          body: {},
          query: {
            kochava_id: '123',
            event_name: 'test'
          }
        },
        res = express.response(function (status) {
          done(status === 200 ? undefined : 'wrong data');
        });

      kochava.insertEvent = function () {
        cache = kochava.insertEvent;
        return bluebird.resolve(1);
      };

      response_handler(req, res, function () {});
      validator.express(req, res);
      kochava_controller.index(req, res);
    });

    it('should insert the event', function () {
      var cache = kochava.insertEvent,
        req = {
          body: {},
          query: {
            kochava_id: '123',
            event_name: 'test'
          }
        },
        res = express.response(function (status) {
          assert.strictEqual(200, status);
        });

      kochava.insertEvent = function (data) {
        cache = kochava.insertEvent;
        assert.deepEqual(data, req.query);
        return bluebird.resolve(1);
      };

      response_handler(req, res, function () {});
      validator.express(req, res);
      kochava_controller.index(req, res);
    });
  });
});
