var fb_req = require('../../../controllers/user/request'),
  user = require('../../../models/user'),
  validator = require('../../../modules/validator'),
  response_handler = require('../../../modules/response'),
  express = require('../../lib/express'),
  promise = require('../../lib/fakePromise').promise,
  sinon = require('sinon');

describe('Facebook Requests', function () {
  'use strict';

  var init, add_reqs, update_reqs;

  beforeEach(function () {
    init = sinon.stub(user, 'constructor');
    add_reqs = sinon.stub(user, 'addRequests');
    update_reqs = sinon.stub(user, 'updateRequest');
  });

  afterEach(function () {
    init.restore();
    add_reqs.restore();
    update_reqs.restore();
  });

  describe('create', function () {
    it('should send 200 if no validation error', function (done) {
      var req = {
          body: {
            request: 1,
            to: [4, 8],
            sender: 20,
            type: 'life'
          }
        },
        res = express.response(function (state) {
          done(state === 200 ? null : 'error occurred');
        });

      response_handler(req, res, function () {});
      validator.express(req, res);
      add_reqs.returns(promise(1));
      fb_req.create(req, res);
    });

    it('should send 500 if there is a validation error', function (done) {
      var req = {
          body: {
            request: '1223',
            to: '[4, 8]',
            sender: '20',
            type: 'life'
          }
        },
        res = express.response(function (state) {
          done(state === 500 ? null : 'error occurred');
        });

      response_handler(req, res, function () {});
      add_reqs.returns(promise(1));
      validator.express(req, res);
      fb_req.create(req, res);
    });
  });

  describe('update', function () {
    it('should send 200 if no validation error', function (done) {
      var req = {
          body: {
            request_id: '1234',
            receiver: '1234',
            type: '4321',
            fb_action: '4321'
          }
        },
        res = express.response(function (state) {
          done(state === 200 ? null : 'error occurred');
        });

      response_handler(req, res, function () {});
      validator.express(req, res);
      update_reqs.returns(promise(1));
      fb_req.update(req, res);
    });

    it('should send 500 if there is a validation error', function (done) {
      var req = {
          body:{
            data: 'test'
          }
        },
        res = express.response(function (state) {
          done(state === 500 ? null : 'error occurred');
        });

      response_handler(req, res, function () {});
      validator.express(req, res);
      update_reqs.returns(promise(1));
      fb_req.update(req, res);
    });
  });
});
