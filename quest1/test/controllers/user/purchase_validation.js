var pv_req = require('../../../controllers/user/purchase_validation'),
  validator = require('../../../modules/validator'),
  response_handler = require('../../../modules/response'),
  express = require('../../lib/express'),
  promise = require('../../lib/fakePromise').promise,
  sinon = require('sinon'),
  purchase_validator = require('../../../modules/purchase_validator'),
  assert = require('assert'),
  status = {
    valid: 'valid',
    invalid: 'invalid',
    reschedule: 'reschedule'
  };

describe('Purchase validation requests', function () {
  'use strict';

  describe('create', function () {
    it('should send 200 with valid for currect values', function (done) {
      var req = {
          body: {
            user_id: 'Android-123456',
            store: 'android',
            purchase_details: {}
          }
        },
        verifyPurchase = sinon.stub(
          purchase_validator,
          'verifyPurchase'
        ),
        res = express.response(function (state) {
          verifyPurchase.restore();
          assert.strictEqual(200, state);
        }, function (resp) {
          assert.strictEqual(resp.status, status.valid);
          done();
        });

      response_handler(req, res, function () {});
      validator.express(req, res);
      verifyPurchase.returns(promise(status.valid));
      pv_req.create(req, res);
    });

    it('should send 500 if there is a validation error', function (done) {
      var req = {
          body: {
            request: '1223',
            to: '[4, 8]',
            sender: '20',
            type: 'life'
          }
        },
        verifyPurchase = sinon.stub(
          purchase_validator,
          'verifyPurchase'
        ),
        res = express.response(function (state) {
          verifyPurchase.restore();
          done(state === 500 ? null : 'error occurred');
        });

      response_handler(req, res, function () {});
      verifyPurchase.returns(promise(1));
      validator.express(req, res);
      pv_req.create(req, res);
    });

    it('should send 200 with invalid status if invalid', function (done) {
      var req = {
          body: {
            user_id: 'Android-123456',
            store: 'android',
            purchase_details: {}
          }
        },
        verifyPurchase = sinon.stub(
          purchase_validator,
          'verifyPurchase'
        ),
        res = express.response(function (state) {
          verifyPurchase.restore();
          assert.strictEqual(state, 200);
        }, function (resp) {
          assert.strictEqual(resp.status, status.invalid);
          done();
        });

      response_handler(req, res, function () {});
      verifyPurchase.returns(promise(status.invalid));
      validator.express(req, res);
      pv_req.create(req, res);
    });

    it('should send 200 with invalid status if reschedule', function (done) {
      var req = {
          body: {
            user_id: 'Android-123456',
            store: 'android',
            purchase_details: {}
          }
        },
        verifyPurchase = sinon.stub(
          purchase_validator,
          'verifyPurchase'
        ),
        res = express.response(function (state) {
          verifyPurchase.restore();
          assert.strictEqual(state, 200);
        }, function (resp) {
          assert.strictEqual(resp.status, status.reschedule);
          done();
        });

      response_handler(req, res, function () {});
      verifyPurchase.returns(promise(status.reschedule));
      validator.express(req, res);
      pv_req.create(req, res);
    });
  });
});
