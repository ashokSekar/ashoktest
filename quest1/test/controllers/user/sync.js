var user, express, config,
  validator, response_handler,
  fs = require('fs'),
  sinon = require('sinon'),
  assert = require('assert'),
  sync, logger, sync_rules,
  promise = require('../../lib/fakePromise').promise;

describe('Sync Controller', function () {
  'use strict';

  var req = {
      validate: function () {},
      body: {
        device: {
          type: 'AndroidPhone',
          name: '7.5'
        }
      }
    },
    resp = {error: function () {}},
    sync_data, stub_logger, post_sync;

  before(function () {
    config = require('../../../resources/config');

    config.game = {
      package_name: 'com.hashcube.game',
      discard_data: 'quest_discard.data'
    };

    express = require('../../lib/express');
    validator = require('../../../modules/validator');
    user = require('../../../models/user');
    response_handler = require('../../../modules/response');

    logger = require('../../../modules/debug')('SYNC');
    sync = require('../../../controllers/user/sync');

    sync.create(req, {});
    sync_rules = sync._rules;

    stub_logger = sinon.stub(logger, 'error');
    sync_data = sinon.stub(user, 'syncData');
    post_sync = sinon.stub(user, 'postSync');
    stub_logger.returns(1);
  });

  after(function () {
    stub_logger.restore();
    sync_data.restore();
    post_sync.restore();

    fs.unlinkSync('./' + config.game.discard_data);
  });

  describe('create()', function () {
    it('should send error response if not production', function (done) {
      var req = {body: {
          user: {uid: 'qasqw'},
          device: {
            advertising_id: 'sads',
            type: 'Adroid',
            name: 'asa'
          }
        }},
        res = express.response(function (status) {
          done(status === 500 ? undefined : 'error');
        });

      response_handler(req, res, function () {});
      validator.express(req, res);
      sync.create(req, res);
    });

    it('should invoke error with proper data', function (done) {
      var req = {body: {
          user: {uid: 'qasqw'},
          device: {
            advertising_id: 'sads',
            type: 'Adroid',
            name: 'asa'
          }
        }},
        res = express.response(function () {});

      response_handler(req, res, function () {});
      validator.express(req, res);
      res.error = function (err) {
        done(err.type === 'Non Production' ? undefined : 'error');
      };
      sync.create(req, res);
    });

    it('should write data to proper file', function (done) {
      var req = {body: {
          user: {uid: 'qasqw'},
          device: {
            advertising_id: 'sads',
            type: 'Adroid',
            name: 'saasa'
          }
        }},
        res = express.response(function () {}),
        cache_fs = fs.appendFile;

      response_handler(req, res, function () {});
      validator.express(req, res);
      fs.appendFile = function (file) {
        fs.appendFile = cache_fs;
        done(file === config.game.discard_data ? undefined : 'error');
      };
      sync.create(req, res);
    });

    it('should send 500 if validation fails', function (done) {
      var req = {
          body: {
            user: {fb_uid: 'jhghrtyss'},
            device: {
              adverising_id: 'asasa',
              type: 'AndroidPhone',
              name: 'aada'
            }
          }
        },
        res = express.response(function (status) {
          done(status === 500 ? undefined : 'wrong status');
        });

      response_handler(req, res, function () {});
      validator.express(req, res);
      sync.create(req, res);
    });

    it('should throw validation error if params with user_id is not ' +
      'present in request', function (done) {
        var req = {body: {
            user: {uid: 'qasqw'},
            device: {
              advertising_id: 'sads',
              type: 'Android',
              name: 'asdf'
            }
          }},
          res = express.response(function (status) {
            done(status === 500 ? undefined : 'error');
          });

        response_handler(req, res, function () {});
        validator.express(req, res);
        sync.create(req, res);
      }
    );

    it('should pass validation and send success response', function (done) {
      var request = {body: {
          user: {uid: '897tghg'},
          device: {
            advertising_id: 'kjjhy5678u',
            type: 'AndroidPhone',
            name: 'asgr'
          }
        },
        headers: {
          'x-forwarded-for': 'ip'
        },
        params: {
          user_id: 'asasd'
        }},
          resp = express.response(function (status) {
          assert.strictEqual(200, status);
        }, function (data) {
          assert.strictEqual(data.user.uid, '1234');
          assert.strictEqual(data.user.max_ms, 2);
          assert.strictEqual(data.user.master_score, 37);
          assert.strictEqual(data.user.paid_currency, 1000);
          assert.strictEqual(data.user.free_currency, 500);
          assert.deepEqual(data.requests, [1,2,3]);
          done();
        });

      response_handler(request, resp, function () {});
      sync_data.withArgs(request.body, 'ip')
        .returns(promise({
            uid: 'test_uid',
            fb_id: 209,
            requests: [1,2,3],
            puzzles: {},
            user: {
              first_name: 'Test',
              last_name: 'User',
              uid: '1234',
              max_ms: 2,
              master_score: 37,
              paid_currency: 1000,
              free_currency: 500
            }
          }));
      validator.express(request, resp);
      sync.create(request, resp);
    });

    it('should send success response  for patch', function (done) {
      var request = {body: {
          user: {uid: '897'},
          device: {
            advertising_id: 'kjjhy5678u',
            type: 'AndroidPhone',
            name: 'asgr'
          }
        },
        headers: {
          'x-forwarded-for': 'ip'
        },
        params: {
          user_id: 'asasd'
        }},
          resp = express.response(function (status) {
          assert.strictEqual(200, status);
        }, function (data) {
          assert.strictEqual(data.puzzle_patch.app_version, '0.51');
          assert.strictEqual(data.puzzle_patch.version, '5');
          done();
        });

      response_handler(request, resp, function () {});
      sync_data.withArgs(request.body, 'ip')
        .returns(promise({
            puzzle_patch: {
              version: '5',
              app_version: '0.51'
            }
          }));

      validator.express(request, resp);
      sync.create(request, resp);
    });
  });

  describe('isProductionIOS()', function () {
    var req = {
        body: {
          device: {
            type: 'browser',
            name: '5.2'
          }
        },
        headers: {}
      },
      device = req.body.device,
      headers = req.headers;

    it('should return false for android device', function () {
      device.type = 'AndroidPhone';
      assert.strictEqual(false, sync._isProductionIOS(req));
    });

    it('should return true for web device', function () {
      device.type = 'browser';
      assert.strictEqual(true, sync._isProductionWeb(req));
    });

    it('should return false for ios device & non production', function () {
      device.type = 'IPhone';
      headers['user-agent'] = 'com.hashcube.bq_dev';
      assert.strictEqual(false, sync._isProductionIOS(req));
    });

    it('should return true for iphone device production build', function () {
      device.type = 'IPhone';
      headers['user-agent'] = config.game.package_name;
      assert.strictEqual(true, sync._isProductionIOS(req));
    });

    it('should return true for ipad device production build', function () {
      device.type = 'IPad';
      headers['user-agent'] = config.game.package_name;
      assert.strictEqual(true, sync._isProductionIOS(req));
    });

    it('should return true for ipod device production build and change ' +
      'device type to IPod', function () {
        device.type = 'browser';
        device.name = 'iPod';
        headers['user-agent'] = config.game.package_name;
        assert.strictEqual(true, sync._isProductionIOS(req));
        assert.strictEqual('IPod', device.type);
      }
    );
  });

  describe('isProductionAndroid()', function () {
    var req = {
        body: {
          device: {type: 'browser'}
        }
      },
      device = req.body.device;

    it('should return false for ios device', function () {
      device.type = 'IPhone';
      assert.strictEqual(false, sync._isProductionAndroid(req));
    });

    it('should return false for browser', function () {
      device.type = 'browser';
      assert.strictEqual(false, sync._isProductionAndroid(req));
    });

    it('should return true for android phone', function () {
      device.type = 'AndroidPhone';
      assert.strictEqual(true, sync._isProductionAndroid(req));
    });

    it('should return true for android tab', function () {
      device.type = 'AndroidTab';
      assert.strictEqual(true, sync._isProductionAndroid(req));
    });

    it('should return true for amazon phone', function () {
      device.type = 'AmazonPhone';
      assert.strictEqual(true, sync._isProductionAndroid(req));
    });

    it('should return true for amazon tab', function () {
      device.type = 'AmazonTab';
      assert.strictEqual(true, sync._isProductionAndroid(req));
    });
  });

  describe('validateUserInfo', function () {
    var user_rules;

    beforeEach(function () {
      user_rules = sync_rules.user;
    });

    it('should fail with validation errors', function () {
      var user_info = {
          fb_uid: 'jhytguy',
          aurrency_free: 'asfdds',
          currency_paid: 'aas',
          curr_ms: 'sdfsa',
          max_ms: 'sdasd',
          tart_time: 'd3rs',
          score: 'dfsd',
          dob: 'ioyut',
          inventory_lives: 'sdsd',
          deauth_time: 'sdfd',
          paid: 'sds',
          last_life: 'sds'
        },
        req = {body: user_info};

      validator.express(req, resp);
      assert.strictEqual(false, req.validate(user_rules));
    });

    it('shouldnt fail the validation', function () {
      var user_info = {
        uid: 'uyrt',
        fb_uid: 234567,
        state: 'ker',
        currency_paid: 200,
        gender: 'm',
        location: 'Blr, India',
        curr_ms: 2,
        currency_free: 100,
        first_name: 'nam',
        language: 'en',
        max_ms: 2,
        last_name: 'lname',
        start_time: '2015-02-03',
        score: 98768,
        dob: '2015-02-01',
        source: 'df',
        inventory_lives: 5,
        country: 'ind',
        deauth_time: '2015-03-01',
        paid: 2.4,
        last_life: '2015-02-27',
        game_sounds: {music: true, effect: true}
      },
      req = {body: user_info};

      validator.express(req, resp);
      assert.strictEqual(true, req.validate(user_rules));
    });

    it('should fail if uid is not present', function () {
      var info = {fb_uid: 24356},
        req = {body: info};

      validator.express(req, resp);
      assert.strictEqual(false, req.validate(user_rules));
    });
  });

  describe('validatePuzzleData', function () {
    var puzzle_rules;

    beforeEach(function () {
      puzzle_rules = {puzzle: sync_rules.puzzle};
    });

    it('should pass validation and empty the array', function () {
      var puz_data = [{
          date_time: 'hjgf',
          time_taken: 'ase',
          moves: 'sdd',
          progress: 'sds',
          score: 'asda',
          stars: 'sds',
          club_reds: 'sda',
          jelly_buster: 'sds',
          lock_breaker: 'asq',
          ingredient_filler: 'sasa',
          go_red: 'asa',
          gummy_popper: 'asf'
        }],
        req = {body: {puzzle: puz_data}},
        valid;

      validator.express(req, resp);
      valid = req.validate(puzzle_rules);
      assert.strictEqual(true, valid);
      assert.strictEqual(0, req.body.puzzle.length);
    });

    it('should pass validation and remove the wrong element', function () {
      var data = [{
          id: 'utyrt',
          date_time: 1426488383984,
          time_taken: 60,
          moves: 26,
          progress: 100,
          score: 786,
          stars: 3,
          limit: 1
        }, {
          date_time: 'hjgf',
          time_taken: 'ase',
          moves: 'sdd',
          progress: 'sds',
          score: 'asda',
          stars: 'sds'
        }],
        req = {body: {puzzle: data}}, valid;

      validator.express(req, resp);
      valid = req.validate(puzzle_rules);
      assert.strictEqual(true, valid);
      assert.strictEqual(1, req.body.puzzle.length);
    });

    it('should fail validation', function () {
      var data = {id: 'asdasd'},
        req = {body: {puzzle: data}};

      validator.express(req, resp);
      assert.strictEqual(false, req.validate(puzzle_rules));
    });
  });

  describe('validateDevice()', function () {
    var device_rules;

    beforeEach(function () {
      device_rules = sync_rules.device;
    });

    it('should fail validation', function () {
      var data = {
          is_tablet: 'sdds'
        },
        req = {body: data};

      validator.express(req, resp);
      assert.strictEqual(false, req.validate(device_rules));
    });

    it('should pass validation', function () {
      var data = {
        advertising_id: 'asdsad',
        name: 'sdsd',
        language: 'sdsds',
        os: 'asdsd',
        appversion: 'sds',
        store: 'sdfef',
        is_tablet: true
      },
      req = {body: data};

      validator.express(req, resp);
      assert.strictEqual(true, req.validate(device_rules));
    });
  });

  describe('validateCredits', function () {
    var credit_rules;

    beforeEach(function () {
      credit_rules = {credits: sync_rules.user.credits};
    });

    it('should pass validation and empty the array', function () {
      var data = [{
          curr_ms: 'asas',
          time: 'asasa',
          amount: 'asas',
          receipt: 'asas',
          max_ms: 'wasdsasd'
        }],
        req = {body: {credits: data}},
        valid;

      validator.express(req, resp);
      valid = req.validate(credit_rules);
      assert.strictEqual(true, valid);
      assert.strictEqual(0, req.body.credits.length);
    });

    it('should pass validation and remove wrong data', function () {
      var data = [
          {
            curr_ms: 3,
            time: '2015-02-01 10:10:10',
            amount: 78.9,
            receipt: 9978656,
            max_ms: 3,
            transaction_id: 'asas',
            item: 'jhhg'
          }, {
            curr_ms: 'abcd',
            time: 'asd',
            amount: 'dasda',
            receipt: 'sadsad',
            max_ms: 'dsada',
            transaction_id: 213,
            item: 12312
          }
        ],
        req = {body: {credits: data}},
        valid;

      validator.express(req, resp);
      valid = req.validate(credit_rules);
      assert.strictEqual(true, valid);
      assert.strictEqual(1, req.body.credits.length);
    });

    it('should fail validation', function () {
      var data = {curr_ms: 3},
      req = {body: {credits: data}};

      validator.express(req, resp);
      assert.strictEqual(false, req.validate(credit_rules));
    });
  });

  describe('validateFreeCurrencies', function () {
    var free_curr_rules;

    beforeEach(function () {
      free_curr_rules = {
        free_currencies: sync_rules.user.track_free_currency
      };
    });

    it('should pass validation and empty the array', function () {
      var data = [{
          curr_ms: 'asas',
          time: 'asasa',
          amount: 'asas'
        }],
        req = {body: {free_currencies: data}},
        valid;

      validator.express(req, resp);
      valid = req.validate(free_curr_rules);
      assert.strictEqual(true, valid);
      assert.strictEqual(0, req.body.free_currencies.length);
    });

    it('should pass validation and remove wrong data', function () {
      var data = [
          {
            curr_ms: 3,
            time: '2015-02-01 10:10:10',
            amount: 1000,
            source: 'jhhg'
          },
          {
            curr_ms: 'abcd',
            time: 'asd',
            amount: 'dasda',
            source: 766
          }
        ],
        req = {body: {free_currencies: data}},
        valid;

      validator.express(req, resp);
      valid = req.validate(free_curr_rules);
      assert.strictEqual(true, valid);
      assert.strictEqual(1, req.body.free_currencies.length);
    });

  });

  describe('validateInventoryPurchases', function () {
    var trans_rules;

    beforeEach(function () {
      trans_rules = {purchases: sync_rules.user.purchases};
    });

    it('should pass validation and empty the array', function () {
      var data = [{
          time: 'sdd',
          quantity: 'asa',
          currency_used: {free: 9, paid: 10},
          currency_remaining: 'asa',
          curr_ms: 'aa',
          max_ms: 'sa'
        }],
        req = {body: {purchases: data}},
        valid;

      validator.express(req, resp);
      valid = req.validate(trans_rules);
      assert.strictEqual(true, valid);
      assert.strictEqual(0, req.body.purchases.length);
    });

    it('should pass validation and remove wrong element', function () {
      var data = [{
        time: 1426491017882,
        quantity: 78,
        currency_used: {free: 9, paid: 10},
        remaining_currency: 998,
        curr_ms: 4,
        max_ms: 4,
        item: 'cash09'
      }, {
        time: 'sdd',
        quantity: 'asa',
        currency_used: {free: 9, paid: 10},
        currency_remaining: 'asa',
        curr_ms: 'aa',
        max_ms: 'sa'
      }],
        req = {body: {purchases: data}}, valid;

      validator.express(req, resp);
      validator.express(req, resp);
      valid = req.validate(trans_rules);
      assert.strictEqual(true, valid);
      assert.strictEqual(1, req.body.purchases.length);
    });

    it('should fail validation', function () {
      var data = {item: 'asdasd'},
        req = {body: {purchases: data}};

      validator.express(req, resp);
      assert.strictEqual(false, req.validate(trans_rules));
    });
  });
});
