var cash_purchase = require('../../../controllers/user/cash_purchase'),
  validator = require('../../../modules/validator'),
  response_handler = require('../../../modules/response'),
  express = require('../../lib/express'),
  promise = require('../../lib/fakePromise').promise,
  sinon = require('sinon'),
  user = require('../../../models/user'),
  assert = require('assert'),
  status = {
    valid: 'valid',
    invalid: 'invalid',
    reschedule: 'reschedule'
  },
  purchase_details = {
    item: 'cash1',
    time: 10000000,
    quantity: 1,
    currency_used: {
      free: 100,
      paid: 1000
    },
    currency_remaining: 100000,
    curr_ms: 10,
    max_ms: 10,
    instance_id: 100
  };

describe('Purchase validation requests', function () {
  'use strict';

  describe('create', function () {
    it('should send 200 with valid for currect values', function (done) {
      var req = {
          body: {
            uid: 'Android-123456',
            store: 'android',
            purchase_details: purchase_details
          }
        },
        verifyAndSavePurchase = sinon.stub(
          user,
          'verifyAndSavePurchase'
        ),
        res = express.response(function (state) {
          verifyAndSavePurchase.restore();
          assert.strictEqual(200, state);
        }, function (resp) {
          assert.strictEqual(resp.validation_status, status.valid);
          done();
        });

      response_handler(req, res, function () {});
      validator.express(req, res);
      verifyAndSavePurchase.returns(promise({
        validation_status: 'valid',
        save_status: true
      }));
      cash_purchase.create(req, res);
    });

    it('should send 500 if there is a validation error', function (done) {
      var req = {
          body: {
            request: '1223',
            to: '[4, 8]',
            sender: '20',
            type: 'life'
          }
        },
        verifyAndSavePurchase = sinon.stub(
          user,
          'verifyAndSavePurchase'
        ),
        res = express.response(function (state) {
          verifyAndSavePurchase.restore();
          done(state === 500 ? null : 'error occurred');
        });

      response_handler(req, res, function () {});
      verifyAndSavePurchase.returns(promise(1));
      validator.express(req, res);
      cash_purchase.create(req, res);
    });

    it('should send 200 with invalid status if invalid', function (done) {
      var req = {
          body: {
            uid: 'Android-123456',
            store: 'android',
            purchase_details: purchase_details
          }
        },
        verifyAndSavePurchase = sinon.stub(
          user,
          'verifyAndSavePurchase'
        ),
        res = express.response(function (state) {
          verifyAndSavePurchase.restore();
          assert.strictEqual(200, state);
        }, function (resp) {
          assert.strictEqual(resp.validation_status, status.invalid);
          done();
        });

      response_handler(req, res, function () {});
      validator.express(req, res);
      verifyAndSavePurchase.returns(promise({
        validation_status: 'invalid',
        save_status: true
      }));
      cash_purchase.create(req, res);
    });

    it('should send 200 with invalid status if reschedule', function (done) {
      var req = {
          body: {
            uid: 'Android-123456',
            store: 'android',
            purchase_details: purchase_details
          }
        },
        verifyAndSavePurchase = sinon.stub(
          user,
          'verifyAndSavePurchase'
        ),
        res = express.response(function (state) {
          verifyAndSavePurchase.restore();
          assert.strictEqual(200, state);
        }, function (resp) {
          assert.strictEqual(resp.validation_status, status.reschedule);
          done();
        });

      response_handler(req, res, function () {});
      validator.express(req, res);
      verifyAndSavePurchase.returns(promise({
        validation_status: 'reschedule',
        save_status: true
      }));
      cash_purchase.create(req, res);
    });
  });
});
