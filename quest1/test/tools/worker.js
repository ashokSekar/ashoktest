'use strict';

var sinon = require('sinon'),
  assert = require('assert'),
  user = require('../../models/user'),
  config = require('../../resources/config'),
  promise = require('../lib/fakePromise').promise,
  worker = require('../../tools/worker'),
  bluebird = require('bluebird'),
  amqp = require('amqplib');

describe('Worker', function () {

  describe('syncInserts', function () {
    var puzzle_ins, credits_ins, good_ins, purchases_ins,
      getStub = function (func) {
        var stb = sinon.stub(user, func);

        stb.returns(bluebird.resolve());
        return stb;
      };

    before(function () {
      puzzle_ins = getStub('insertPuzzleData');
      credits_ins = getStub('saveCredits');
      good_ins = getStub('trackFreeGoods');
      purchases_ins = getStub('savePurchases');
    });

    after(function () {
      puzzle_ins.restore();
      credits_ins.restore();
      good_ins.restore();
      purchases_ins.restore();
    });

    it('should insert puzzle data', function (done) {
      puzzle_ins.restore();
      puzzle_ins = sinon.stub(user, 'insertPuzzleData', function (val) {
        puzzle_ins.restore();
        puzzle_ins = getStub('insertPuzzleData');
        done(val === 'puzzle' ? undefined : 'error');
        return bluebird.resolve();
      });

      worker._syncInserts({
        uid: 1,
        puzzle_data: 'puzzle'
      });
    });

    it('should insert credit data', function (done) {
      credits_ins.restore();
      credits_ins = sinon.stub(user, 'saveCredits', function (val) {
        credits_ins.restore();
        credits_ins = getStub('saveCredits');
        done(val === 'credits' ? undefined : 'error');
        return bluebird.resolve();
      });

      worker._syncInserts({
        uid: 1,
        credits: 'credits'
      });
    });

    it('should insert free goods', function (done) {
      good_ins.restore();
      good_ins = sinon.stub(user, 'trackFreeGoods', function (val) {
        good_ins.restore();
        good_ins = getStub('trackFreeGoods');
        done(val === 'goods' ? undefined : 'error');
        return bluebird.resolve();
      });

      worker._syncInserts({
        uid: 1,
        free_goods: 'goods'
      });
    });

    it('should insert purchases', function (done) {
      purchases_ins.restore();
      purchases_ins = sinon.stub(user, 'savePurchases', function (val) {
        purchases_ins.restore();
        purchases_ins = getStub('savePurchases');
        done(val === 'purchases' ? undefined : 'error');
        return bluebird.resolve();
      });

      worker._syncInserts({
        uid: 1,
        purchases: 'purchases'
      });
    });

    it('should revert duplicate data', function (done) {
      var revert = sinon.stub(user, 'revertGameSyncUpdate',
        function (data, uid) {
          revert.restore();
          assert.strictEqual(1, uid);
          assert.deepEqual(data.purchases, [1, 2]);
          assert.deepEqual(data.puzzles, [3, 4]);
          assert.deepEqual(data.credits, [5, 6]);
          assert.deepEqual(data.track_free_goods, [7, 8]);
          done();
          return bluebird.resolve();
        });

      puzzle_ins.restore();
      credits_ins.restore();
      good_ins.restore();
      purchases_ins.restore();

      purchases_ins = sinon.stub(user, 'savePurchases', function () {
        purchases_ins.restore();
        purchases_ins = getStub('savePurchases');
        return bluebird.resolve([1, 2]);
      });
      puzzle_ins = sinon.stub(user, 'insertPuzzleData', function () {
        puzzle_ins.restore();
        puzzle_ins = getStub('insertPuzzleData');
        return bluebird.resolve([3, 4]);
      });
      credits_ins = sinon.stub(user, 'saveCredits', function () {
        credits_ins.restore();
        credits_ins = getStub('saveCredits');
        return bluebird.resolve([5, 6]);
      });
      good_ins = sinon.stub(user, 'trackFreeGoods', function () {
        good_ins.restore();
        good_ins = getStub('trackFreeGoods');
        return bluebird.resolve([7, 8]);
      });

      worker._syncInserts({
        uid: 1,
        purchases: []
      });
    });
  });

  describe('syncNotifications()', function () {
    it('should invoke user.insertNotifications', function (done) {
      var insert_notif = sinon.stub(user, 'insertNotifications',
        function (uid, data) {
          insert_notif.restore();
          assert.strictEqual(uid, 1);
          assert.deepEqual(data, ['abc']);
          done();
        });

      worker._syncNotifications({
        uid: 1,
        notifications: ['abc']
      });
    });
  });

  describe('getChannel', function () {
    it('should create channel and return if not present', function (done) {
      var conn = sinon.stub(amqp, 'connect', function () {
        conn.restore();

        return bluebird.resolve({
          createChannel: function () {
            return bluebird.resolve('channel');
          },
          close: function () {}
        });
      });

      worker._setVal('amqp_channel', null);
      worker._getChannel()
        .then(function(val) {
          done(val === 'channel' ? undefined : 'error');
        });
    });

    it('should return already existing channel', function (done) {
      worker._setVal('global.bluebird', 'require(\'bluebird\')');
      worker._setVal('amqp_channel', 'bluebird.resolve(\'existing_channel\')');
      worker._getChannel()
        .then(function(val) {
          done(val === 'existing_channel' ? undefined : 'error');
        });
    });
  });

  describe('handleJob', function () {
    var buffer = {
      content: '{"job": "puzzles"}'
    };

    it('should parse data from buffer and invoke cb', function (done) {
      var conn = sinon.stub(amqp, 'connect', function () {
        conn.restore();

        return bluebird.resolve({
          createChannel: function () {
            return bluebird.resolve({
              ack: function () {
                done();
              }
            });
          },
          close: function () {}
        });
      });

      worker._setVal('amqp_channel', null);
      worker._handleJob(function () {
        return bluebird.resolve();
      }, buffer);
    });

    it('should requeue if error', function (done) {
      var conn = sinon.stub(amqp, 'connect', function () {
        conn.restore();

        return bluebird.resolve({
          createChannel: function () {
            return bluebird.resolve({
              nack: function () {
                done();
              }
            });
          },
          close: function () {}
        });
      });

      worker._setVal('amqp_channel', null);
      worker._handleJob(function (data) {
        assert.deepEqual(data, {
          job: 'puzzles'
        });

        return bluebird.reject();
      }, buffer);
    });
  });

  describe('start()', function () {
    it('should invoke sync_inserts, sync_notifications', function (done) {
      var instance = worker._instance,
        cache = instance.run,
        count = 0;

      instance.run = function (job) {
        if (++count === 2) {
          instance.run = cache;
          return done(job === 'sync_notifications' ? undefined : 'error');
        }
        assert.strictEqual(job, 'sync_inserts');
      };

      instance.start();
    });
  });

  describe('run()', function () {
    it('should listen to passed job', function (done) {
      var conn = sinon.stub(amqp, 'connect'),
        count;

      conn.returns(bluebird.resolve({
        createChannel: function () {
          return promise({
            assertQueue: function () {
              return promise(2);
            },
            prefetch: function (val) {
              count = val;
              return promise(1);
            },
            consume: function (param) {
              conn.restore();
              if (param === 'pref_test_run') {
                done(count === 100 ? undefined : 'error');
              }
            }
          });
        },
        close: function () {}
      }));

      config.job.prefix = 'pref_';
      worker._setVal('amqp_channel', null);
      worker._instance.run('test_run');
    });
  });
});
