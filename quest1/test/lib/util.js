var _ = require('lodash');

module.exports = {
  removeFromCache: function (file_name, test_remove) {
    'use strict';

    var to_remove,
      cache_modules = require.cache;

    to_remove = _.find(_.keys(cache_modules), function (key) {
      return key.indexOf(file_name) !== -1 &&
      (test_remove ? key.indexOf('/test/') >= 0 :
        key.indexOf('/test/') === -1);
    });

    if (to_remove) {
      delete cache_modules[to_remove];
    }
  },
  getFromCache: function (file_name, test_remove) {
    'use strict';

    var to_get,
      cache_modules = require.cache;

    to_get = _.find(_.keys(cache_modules), function (key) {
      return key.indexOf(file_name) !== -1 &&
        (test_remove ? key.indexOf('/test/') >= 0 :
        key.indexOf('/test/') === -1);
    });

    return cache_modules[to_get];
  }
};
