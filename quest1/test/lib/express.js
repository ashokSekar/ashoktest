var _ = require('lodash');

module.exports = {
  request: function  (values) {
    'use strict';
    values.query  = values.query || {};
    values.body = values.body || {};
    values.params = values.params || {};

    return {
      query: values.query,
      body: values.body,
      params: values.params,
      param: function (param) {
        var query = values.query[param],
          body = values.body[param],
          param_val = values.params[param];

        if (query) {
          return query;
        } else if (body) {
          return body;
        } else {
          return param_val;
        }
      }
    };
  },
  response: function (status_callback, data_callback) {
    'use strict';

    return {
      _header: {},
      status: function (status) {
        if (status_callback) {
          status_callback(status);
        }
        return this;
      },
      header: function (key, data) {
        if (_.isString(key)) {
          this._header[key] = data;
        } else if (_.isObject(key)) {
          this._header = _.merge(this._header, key);
        } else {
          throw new TypeError('Undefined type for key');
        }
      },
      json: function (data) {
        if (data_callback) {
          data_callback(data);
        }
        return this;
      }
    };
  }
};
