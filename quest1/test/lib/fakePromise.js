'use strict';
var bluebird  = require('bluebird');
// functions to mock promise in test cases;

module.exports.promise = function (data) {
  if (typeof data !== 'undefined') {
    return bluebird.resolve(data);
  } else {
    return bluebird.reject({
      no: 0,
      stack: 'Over here'
    });
  }
};
