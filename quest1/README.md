Quest Server
============

1. Install npm dependency
2. Install rabbitMQ, memcache
3. Create sys_config and database.json from sample
4. Migrate up
5. Set up pre-commit hooks by copying/symlink from tools


Setup as a module
=================
1. Create empty project
2. Add npm dependecy of quest
3. Add appropriate database library to use with knex
4. Add server.js file
5, Setup quest from node modules
6. Do appropriate migration(s)
7. You can override quest config by creating files in same folder struct
  Eg:
    If you want to override sync_rules.js in resources.
    Create a file sync_rules.js in resources of game.
    Add code like:

    module.exports = {
      // Item to override
    }

    Now whenever quest config is imported it will merge with game config
  Same approach for function overrriding of other modules

8. Start server.js

Adding Sale Info
=================
Run node tools/insert_sale with following parameters
1. Sale name
2. Platform/Platforms: android,ios,kindle,web(for testing)
3. Start date:Example: 2017-08-25
4. End date:Example: 2017-08-27
5. Optional Image name
6. Multiplier: How much to multiply the quantity
