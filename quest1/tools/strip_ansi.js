#!/usr/bin/env node
'use strict';

var fs = require('fs'),
  ansi_regex = require('ansi-regex')(),
  argv = process.argv.slice(2),
  input = argv[0],
  write = function (data) {
    // Writes the output to console replacing the ANSI escape sequences with ''
    process.stdout.write(data.replace(ansi_regex, ''));
  };

// If a file is passed as an argument then write the output to console
if (input) {
  write(fs.readFileSync(input, 'utf8'));
} else {
  // Else if the data is coming from a stream
  process.stdin.setEncoding('utf8');
  process.stdin.on('data', write);
}
