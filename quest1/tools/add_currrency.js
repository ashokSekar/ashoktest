'use strict';

var bluebird = require('bluebird'),
  db = require('./../models/db/db'),
  argv = process.argv,
  fb_id = argv[2],
  amount = parseInt(argv[3], 10),
  source = argv[4] || 'hc_reward_ticket',
  validate = function () {
    var err = '';

    if (!fb_id) {
      err += 'Enter valid fb_id,';
    }

    if (!amount) {
      err += ' Enter valid amount';
    }

    return err ? bluebird.reject(err) : bluebird.resolve(true);
  },
  getUID = function () {
    return db.getCurrentFbConnect(fb_id)
      .then(function (rec) {
        return rec || db.getLastFbConnect(fb_id);
      })
      .then(function (data) {
        if (!data) {
          return bluebird.reject('No fb_connect records found for this user');
        }

        return data.uid;
      });
  },
  curr_free = 0,
  max_ms;

validate()
  .then(db.getUserByFBId.bind(db, fb_id))
  .then(function (user) {
    if (!user || !user.info || !user.game) {
      return bluebird.reject('User not found');
    }
    curr_free = user.game.free_currency;
    max_ms = user.game.max_ms;

    console.log('Current free_currency: ', curr_free);
    return getUID();
  })
  .then(function (uid) {
    return db.trackFreeGoods({
      uid: uid,
      item: 'cash',
      ms: max_ms,
      time: new Date(),
      amount: amount,
      source: source,
      instance_id: null
    });
  })
  .then(function () {
    console.log('Updated free_currency: ', curr_free + amount);
    return db.updateUserGame({
      fb_uid: fb_id,
      free_currency: curr_free + amount
    });
  })
  .then(function () {
    console.log('Cash updated successfully!!');
  })
  .catch(function (err) {
    console.log('Error: ', err);
  })
  .finally(function () {
    process.exit();
  });
