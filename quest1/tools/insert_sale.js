'use strict';
var data = {},
  _ = require('lodash'),
  platforms = ['ios', 'android', 'kindle', 'web'],
  db = require('../models/db/db.js'),
  args = process.argv.slice(2);

try {
  if (args.length === 6) {
    data.sale_name = args[0];
    data.platform = _.map(args[1].split(','), _.trim);
    data.start = args[2];
    data.end = args[3];
    data.image = args[4];
    data.multiplier = args[5];

    if (_.difference(data.platform, platforms).length > 0) {
      throw 'Unknown platform';
    }
    db.insertSale(data)
    .then(function () {
      process.exit();
    });
  } else {
    throw 'Unmatched arguments';
  }
} catch (err) {
  console.log(err);
  process.exit();
}
