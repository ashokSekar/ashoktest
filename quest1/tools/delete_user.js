'use strict';

var bluebird = require('bluebird'),
  fb_id = parseInt(process.argv[2], 10),
  db = require('./../models/db/db'),
  validate = function () {
    return !fb_id ? bluebird.reject('Ener valid fb id') :
      db.getUserRecord(null, fb_id)
        .then(function (user) {
          return user.info ? true :
            bluebird.reject('No user found with Fb id: ' + fb_id);
        });
  };

validate()
  .then(function () {
    return db.deleteFbUser(fb_id);
  })
  .then(function () {
    console.log('User data cleared successfully!!');
  })
  .catch(function (err) {
    console.log('Error: ', err);
  })
  .finally(function () {
    process.exit();
  });

