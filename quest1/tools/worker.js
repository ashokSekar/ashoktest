'use strict';

var amqp_channel, worker,
  _ = require('lodash'),
  jsClass = require('js-class'),
  amqp = require('amqplib'),
  logger = require('../modules/debug')('WORKER'),
  util = require('../modules/util'),
  config = require('../resources/config'),
  user = require('./../models/user'),
  test = require('../test/lib/private'),
  syncInserts = function (sync_data) {
    var uid = sync_data.uid,
      dup_data = {};

    return user.insertPuzzleData(sync_data.puzzle_data)
      .then(function (dup_puzzles) {
        dup_data.puzzles  = _.isArray(dup_puzzles) ? dup_puzzles : [];

        return user.saveCredits(sync_data.credits, uid);
      })
      .then(function (dup_credits) {
        dup_data.credits = _.isArray(dup_credits) ? dup_credits : [];

        return user.trackFreeGoods(sync_data.free_goods, uid, sync_data.fb_uid);
      })
      .then(function (dup_free_goods) {
        dup_data.track_free_goods = _.isArray(dup_free_goods) ?
          dup_free_goods : [];

        return user.savePurchases(sync_data.purchases, uid);
      })
      .then(function (dup_purchases) {
        dup_data.purchases = _.isArray(dup_purchases) ? dup_purchases : [];

        return user.revertGameSyncUpdate(dup_data, uid, sync_data.fb_uid);
      });
  },
  syncNotifications = function (data) {
    return user.insertNotifications(data.uid, data.notifications);
  },

  getChannel = function () {
    if (!amqp_channel) {
      amqp_channel = amqp.connect()
        .then(function (conn) {
          process.once('SIGINT', conn.close.bind(conn));

          return conn.createChannel();
        });
    }

    return amqp_channel;
  },
  handleJob = function (cb, data) {
    var job_data = JSON.parse(data.content.toString()),
      postJob = function (ack) {
        getChannel()
          .then(function (channel) {
            if (ack) {
              channel.ack(data);
            } else {
              // requeing if failed
              channel.nack(data, false, true);
            }
          });
      };

    cb(job_data)
      .then(postJob.bind(null, true), function (err) {
        postJob();
        logger.error('%s: %s', err, data);
      });
  },
  Base = jsClass({
    start: function () {
      this.run('sync_inserts', syncInserts);
      this.run('sync_notifications', syncNotifications);
    },

    // Note: cb should return promise
    run: function (job_name, cb) {
      job_name = config.job.prefix + job_name;

      getChannel()
        .then(function (channel) {
          return channel.assertQueue(job_name, {durable: true})
            // No of messages awaiting for acknowledgment. here 100. Server
            // wont push more messages if given no of message is awaiting ack
            .then(channel.prefetch.bind(channel, 100, false))
            .then(function () {
              return channel.consume(job_name, handleJob.bind(null, cb));
            })
            .then(null, logger.error.bind(logger, '%s'));
        });
    }
  });

worker = new (jsClass(Base, util.getGamePath('tools/worker', Base)))();
worker.start();

test.prepare(module.exports, {
  syncInserts: syncInserts,
  syncNotifications: syncNotifications,
  getChannel: getChannel,
  handleJob: handleJob,
  setVal: function (var_name, value) {
    eval(var_name + '=' + value); //jshint ignore:line
  },
  instance: worker
});
