'use strict';

//All Error handling regarding values coming from users client side
//has to be handled in the game code end. No error handling is done
//in the DB end

var knex_instance, Base,
  _ = require('lodash'),
  bluebird = require('bluebird'),
  fs = bluebird.promisifyAll(require('fs')),
  memcache = require('./cache'),
  knex = require('knex'),
  path = require('path'),
  util = require('../../modules/util'),
  config = require('../../resources/config'),
  Class = require('js-class'),
  cache = require('./cache'),
  logger = require('../../modules/debug')('DB'),
  test = require('../../test/lib/private'),
  puzzle_cache_props = ['fb_uid', 'ms_id', 'score', 'stars', 'progress',
    'time_taken'],
  errorHandler = function (method, data) {
    return function (error) {
      if (error.code === 'ER_DUP_ENTRY') {
        logger.error('Causing', error.code, 'issue in ', method, 'with', data);
        return bluebird.resolve(null);
      } else {
        logger.error('Causing issue in ', method, 'with', data);
        return bluebird.reject(error);
      }
    };
  },
  checkUpdateScoreCache = function (data, update) {
    if (data.progress !== 100) {
      return bluebird.resolve();
    }
    return cache.addOrUpdateScoreCache(data, update);
  },
  insertRecord = function (table, params) {
    return knex_instance(table)
      .insert(params)
      .catch(errorHandler('insertRecord', params));
  },
  fetchUserGame = function (uid) {

    return knex_instance('user_game')
      .where('uid', uid)
      .first()
      .catch(errorHandler('fetchUserGame', uid));
  },
  fetchDeviceInfo = function (uid, is_fetch_all) {
    return knex_instance('user_device')
      .where('uid', uid)
      .select(is_fetch_all ? '*' : 'device_id')
      .first();
  },
  fetchFirstRow = function (table, data) {
    return knex_instance(table)
      .where(data)
      .first();
  },
  fetchUserByFBId = function (fb_uid) {
    var user = {},
      resp = null;

    return fetchFirstRow('user_info', {fb_uid: fb_uid})
      .then(function (data) {
        if (data) {
          user.info = data;
          resp = fetchFirstRow('fb_user_game', {fb_uid: fb_uid})
            .then(function (game) {
              user.game = game;
              return user;
            });
        }
        return resp;
      })
      .catch(errorHandler('fetchUserByFBId', fb_uid));
  },
  fetchFbMaxMs = function (fbid) {
    return knex_instance('fb_user_game')
      .first('max_ms', 'fb_uid')
      .where('fb_uid', fbid)
      .catch(errorHandler('fetchFbMaxMs', fbid));
  },
  updateUserCache = function (fb_uid, data) {
    if (data) {
      return cache.addValue(fb_uid, data, 'user_record');
    }
    return fetchUserByFBId(fb_uid)
      .then(function (data) {
        return cache.addValue(fb_uid, data, 'user_record');
      });
  },
  fetchAndUpdateUser = function (fbuid) {
    return fetchUserByFBId(fbuid)
      .then(function (user) {
        if (user) {
          updateUserCache(fbuid, user);
        }
        return user;
      });
  },
  fetchCompletedUserPuzzleData = function (fb_uid) {
    return knex_instance('user_gs_stats')
      .where({fb_uid: fb_uid, progress: 100})
      .select(puzzle_cache_props)
      .catch(errorHandler('fetchUserPuzzleData', fb_uid));
  },
  fetchAddHighScores = function (fb_uid) {
    return fetchCompletedUserPuzzleData(fb_uid)
      .then(function (data) {
        cache.addValue(fb_uid, data, 'highscores');
        return data;
      });
  },
  fetchAddBonusPlayed = function (fb_uid) {
    return knex_instance('user_bonus_game_stats')
      .where('fb_uid', fb_uid)
      .pluck('id')
      .then(function (data) {
        cache.addValue(fb_uid, data, 'bonus_levels');

        return data;
      })
      .catch(errorHandler('fetchAddBonusPlayed', fb_uid));
  },
  fetchFriendData = function (ms_data, non_cached_ids) {
    var user_score_data = {};

    if (non_cached_ids &&  non_cached_ids.length > 0) {
      return knex_instance('user_gs_stats')
        .whereIn('fb_uid', non_cached_ids)
        .where('progress', 100)
        .then(function (result) {
          if (!result || result.length === 0) {
            return ms_data;
          }

          user_score_data = _.groupBy(result, function (n) {
            return n.fb_uid;
          });
          _.each(non_cached_ids, function (fb_id) {
            var score_data = user_score_data[fb_id];

            if (score_data) {
              cache.addValue(fb_id, _.map(score_data, function (curr) {
                  return _.pick(curr, puzzle_cache_props);
                }), 'highscores');
            }
          });

          _.each(result, function (res) {
            ms_data.push(_.pick(res, puzzle_cache_props));
          });

          return ms_data;
        })
        .catch(errorHandler('fetchFriendData', non_cached_ids));
    } else {
      return bluebird.resolve(ms_data);
    }
  },
  copyRequest = function (params) {
    var action_by = params.by || 'app';

    return knex_instance('requests_sent')
      .where('request_id', '=', params.request_id)
      .andWhere('receiver', '=', params.receiver)
      .first()
      .then(function (request) {
        request.action = params.action;
        request.action_by = action_by;
        return knex_instance('processed_requests').insert(request);
      })
      .catch(errorHandler('copyRequests', params));
  },
  deleteRequest = function (params) {
    var request_id = params.request_id,
      receiver_id = params.receiver;

    return knex_instance('requests_sent')
      .where('receiver', '=', receiver_id)
      .andWhere('request_id', '=', request_id)
      .del()
      .then(cache.deleteRequest.bind(cache, request_id, receiver_id))
      .catch(errorHandler('deleteRequest', params));
  },
  fetchRequests = function (id) {
    return knex_instance('requests_sent')
      .where('receiver', '=', id)
      .andWhere('type', '!=', 'invite')
      .catch(errorHandler('fetchRequests', id));
  },
  fetchAddReq = function (receiver_id, cache_type) {
    return fetchRequests(receiver_id)
      .then(function (data) {
        cache.addValue(receiver_id, data, cache_type);
        return data;
      });
  },
  insertEachValue = function (data, table) {
    var dup_records = [];

    return bluebird.map(data, function (data_element) {
      return knex_instance(table)
        .insert(data_element)
        .catch(function (err) {
          if (err.code === 'ER_DUP_ENTRY') {
            dup_records.push(data_element);
          }
          return errorHandler('insertEachValue_' + table,
            data_element)(err);
        });
    })
    .then(function () {
      return dup_records;
    });
  },
  deleteRecord = function (table, col, val) {
    return knex_instance(table)
      .where(col, val)
      .del();
  },
  cacheUnlockedPowerups = function (free_goods_records, fb_uid) {
    var powerups = config.powerups,
      powerups_all = _.concat(powerups.pregame, powerups.ingame),
      unlocked_powerups = [];

    _.each(free_goods_records, function (free_good) {
      var item = free_good.item;

      if (free_good.source === 'first_time_bonus' &&
        _.indexOf(powerups_all, item) >= 0) {
        unlocked_powerups.push(item);
      }
    });
    cache.push(fb_uid, unlocked_powerups, 'unlocked_powerups');
    return unlocked_powerups;
  };

Base = Class({ //jshint ignore:line
  constructor: function () {
    knex_instance = knex(config.db);
  },

  insertRecordsDupIgnore: function (records, table) {
    return knex_instance(table)
      .insert(records)
      .return(true)
      .catch(function (error) {
        if (error.code === 'ER_DUP_ENTRY') {
          return insertEachValue(records, table);
        }
        return errorHandler('insertRecordsDupIgnore_' + table,
          records)(error);
      });
  },

  getUserRecord: function (uid, fb_uid) {
    var cache_type = 'user_record',
      game_promise;

    game_promise = !fb_uid ? fetchUserGame(uid)
      .then(function (game) {
        return game ? {game: game} : null;
      }) :
      cache.getValue(fb_uid, cache_type)
        .catch(fetchAndUpdateUser.bind(null, fb_uid));

    return bluebird.all([game_promise, fetchDeviceInfo(uid, false)])
      .then(function (data) {
        var user_data = data[0] || {};

        user_data.device = data[1];
        return user_data;
      });
  },
  getUserByFBId: function (fb_uid) {
    //TODO Will be redundant once we don't query for non-fb users
    return cache.getValue(fb_uid, 'user_record')
      .catch(fetchAndUpdateUser.bind(null, fb_uid));
  },

  getFriendPositions: function (uids) {
    if (!uids || uids.length === 0) {
      return bluebird.resolve({});
    }

    return bluebird.map(uids, function (fbid) {
      return cache.getValue(fbid, 'user_record')
        .then(function (val) {
          return {
            fb_uid: fbid,
            max_ms: val.game.max_ms
          };
        })
        .catch(fetchFbMaxMs.bind(null, fbid));
    })
    .then(function (val) {
      var position_data = {};

      _.each(val, function (data) {
        var max_ms;

        if (data) {
          max_ms = data.max_ms;
          if (!position_data[max_ms]) {
            position_data[max_ms] = [];
          }
          position_data[max_ms].push(data.fb_uid);
        }
      });

      return position_data;
    })
    .catch(errorHandler('getFriendPositions_fb_user_game', uids));
  },

  getUserGame: function (uid, fb_uid) {
    return !fb_uid ? fetchUserGame(uid) :
      cache.getValue(fb_uid, 'user_record')
        .catch(fetchAndUpdateUser.bind(null, fb_uid))
        .then(function (user) {
          return user.game;
        });
  },

  updateUserDevice: function (user_device) {
    return knex_instance('user_device')
      .where('uid', user_device.uid)
      .update(_.pick(user_device, 'device_id', 'current_design_version',
        'current_version', 'lives', 'ip_address', 'last_update', 'rating',
        'install_referrer'))
      .catch(errorHandler('updateUserDevice', user_device));
  },

  updateUserInfo: function (params) {
    /**
     * params {}
     * acceptable properties for params:
     * fb_uid, gender, first_name, last_name, dob
     **/
    var fbid = params.fb_uid;

    return knex_instance('user_info')
      .where('fb_uid', fbid)
      .update(params)
      .then(updateUserCache.bind(null, fbid, null))
      .catch(errorHandler('updateUserInfo', params));
  },

  updateUserGame: function (params) {
    /**
     * params {}
     * acceptable properties for params:
     * uid, free_currency, paid_currency, max_ms, curr_ms, master_score,
     * music_muted, sound_muted, amount_spent, fb_friends_count, fb_uid
     **/
    var fbid = params.fb_uid,
      uid = params.uid,
      pre = fbid ? 'fb_' : '';

    return knex_instance(pre + 'user_game')
      .where(pre + 'uid', fbid || uid)
      .update(params)
      .then(function (resp) {
        return fbid ? updateUserCache(fbid) : resp;
      })
      .catch(errorHandler('updateUserGame', params));
  },

  getCompletedUserPuzzleData: function (fb_uid) {
    return cache.getValue(fb_uid, 'highscores')
      .catch(fetchAddHighScores.bind(this, fb_uid));
  },

  getCompletedBonusLevels: function (fb_uid) {
    return cache.getValue(fb_uid, 'bonus_levels')
      .catch(fetchAddBonusPlayed.bind(this, fb_uid));
  },

  getFriendHighscores: function (uids) {
    var ms_data = [],
      non_cached_ids = [];

    if (uids) {
      return knex_instance('user_info')
        .whereIn('fb_uid', uids)
        .select('fb_uid')
        .bind(this)
        .then(function (id_data) {
          return id_data.length > 0 ?
            cache.getFriendData(_.map(id_data, 'fb_uid'))
              .then(function (data) {
                ms_data = data.ms_data;
                non_cached_ids = data.non_cached_ids;
              }) : false;
        })
        .then(function () {
          return fetchFriendData(ms_data, non_cached_ids);
        })
        .catch(errorHandler('getFriendHighscores', uids));
    } else {
      return bluebird.resolve(ms_data);
    }
  },

  // For fb connected user, fb_uid is must
  addOrUpdateMaxPuzzle: function (params) {
    /**
     * params {}
     * accepted properties:
     * fb_uid, ms_id, score, stars, time_taken
     **/
    var fb_id = params.fb_uid,
      query_cond = {
        fb_uid: fb_id,
        ms_id: params.ms_id
      },
      cache_data = _.pick(params, puzzle_cache_props),
      curr_max;

    return knex_instance('user_gs_stats')
      .where(query_cond).first()
      .bind(this)
      .then(function (data) {
        var promise;

        if (data) {
          curr_max = _.pick(data, puzzle_cache_props);
          params = util.getMaxPuzzleData(data, params);
          cache_data = _.pick(params, puzzle_cache_props);

          promise = knex_instance('user_gs_stats')
              .where(query_cond)
              .update(params)
              .then(checkUpdateScoreCache.bind(null, cache_data, true));
        } else {
          promise = insertRecord('user_gs_stats', params)
            .then(checkUpdateScoreCache.bind(null, cache_data, false));
        }

        return promise;
      })
      .then(function () {
        return params;
      })
      .catch(function (err) {
        if (err === 'not_found') {
          return this.getCompletedUserPuzzleData(fb_id);
        }
        throw err;
      })
      .catch(errorHandler('addOrUpdateMaxPuzzle', params));
  },

  insertPuzzleAll: function (puzzles_data) {
    /**
     * params {}
     * accepted properties:
     * uid, ms_id, date_time, time_taken,
     * score, stars, device_id, app_version, design_version
     **/

    return this.insertRecordsDupIgnore(puzzles_data, 'user_gs_stats_all');
  },

  insertBonusStats: function (bonus_stats) {
    // Since knex have no support for insert ignore using knex raw.
    // if we change knex client from mysql response might change.

    return knex_instance.raw(knex_instance('user_bonus_game_stats')
      .insert(bonus_stats)
      .toString().replace(/^insert/i, 'insert ignore'))
      .then(function (resp) {
        var count = resp && resp[0] &&
          resp[0].affectedRows,
          fb_uid = bonus_stats[0].fb_uid;

        if (count > 0) {
          cache.delValue(fb_uid, 'bonus_levels')
            .then(fetchAddBonusPlayed.bind(this, fb_uid));
        }

        return count;
      })
      .catch(errorHandler('insertBonusStats', bonus_stats));
  },

  insertKochavaEvent: function (event_data) {
    /**
      event_name, event_data, event_value, event_timestamp
      device_ip, device_ua, idfa, adid, android_id, mac, imei
      odin, kochava_id,network_name, campaign_id, campaign_name
      tracker_name, tracker_id, site_id, creative_id,
      attribution_time, app_limit_track, device_limit_track,
      matched_by, click_id
    **/

    return insertRecord('kochava_events', event_data);
  },

  insertUser: function (user_device, user_game) {
    /**
     * params: user_device {}, user_game {}
     * and acceptable properties are:
     ****************************************************
     * user_game: uid, free_currency, paid_currency, max_ms, curr_ms,
     *   master_score, lives, music_muted, sound_muted, amount_spent
     *
     * device_info: device_id, type, uid, device_name, current_version,
     *   installed_version, current_design_version, is_tablet, os, jail_broken,
     *   install_time, location, source, language, start_time, source,
     *   deauth_time, ip_address
     */

    var db_ops;

    user_device.installed_version = user_device.current_version;
    db_ops = [
      knex_instance('user_device')
        .insert(user_device),
      knex_instance('user_game')
        .insert(user_game)
      ];

    return bluebird.all(db_ops)
      .catch(errorHandler('insertUser', {
        user_device: user_device,
        user_game: user_game
      }));
  },

  insertFbUser: function (info, game) {
    var db_ops = [
      knex_instance('user_info')
        .insert(info),
      knex_instance('fb_user_game')
        .insert(game)
      ];

    return bluebird.all(db_ops)
      .then(updateUserCache.bind(null, info.fb_uid, null))
      .catch(errorHandler('insertFbUser', game));
  },

  insertSale: function (data) {
    return knex_instance.raw('insert into sale_data' +
      '(name, platforms, start, end, image, multiplier) values' +
      '("' +
      data.sale_name + '","' + data.platform + '","' +
      data.start + '","' + data.end + '","' +
      data.image + '",' + data.multiplier + ');')
      .then(function () {
        // Deleting platform values
        var platforms = data.platform;

        return bluebird.map(platforms, function (platform) {
          return cache.delValue(platform, 'sale');
        });
      })
      .catch(errorHandler('insertSale', data));
  },

  savePurchases: function (purchases) {
    return this.insertRecordsDupIgnore(purchases, 'inventory_purchase');
  },

  saveCredits: function (credits) {
    return this.insertRecordsDupIgnore(credits, 'cash_purchase');
  },

  trackFreeGoods: function (free_goods_records, fb_uid) {
    return this.insertRecordsDupIgnore(free_goods_records, 'track_free_goods')
      .then(function (resp) {
        if (fb_uid) {
          cacheUnlockedPowerups(free_goods_records, fb_uid);
        }
        return resp;
      });
  },

  insertRequests: function (params) {
    return knex_instance('requests_sent')
      .insert(params)
      .then(cache.insertRequests.bind(cache, params))
      .catch(errorHandler('insertRequests', params));
  },

  insertPushNotifications: function (notifications) {
    return this.insertRecordsDupIgnore(notifications,
      'push_notification_clicks');
  },

  updateRequest: function (request) {
    return copyRequest(request)
      .then(deleteRequest.bind(null, request))
      .catch(errorHandler('updateRequest', request));
  },

  getRequests: function (receiver_id) {
    var cache_type = 'requests';

    return cache.getValue(receiver_id, cache_type)
      .catch(fetchAddReq.bind(null, receiver_id,
        cache_type));
  },

  removeOldRequests: function (params) {
    var response;

    return knex_instance('requests_sent')
      .where('request_id', '<>', params.request_id)
      .andWhere('sender', '=', params.sender)
      .andWhere('receiver', '=', params.receiver)
      .andWhere('type', '=', params.type)
      .andWhere('fb_action', '=', params.fb_action)
      .select()
      .bind(this)
      .then(function (requests) {
        response = requests;
        if (requests.length === 0) {
          return requests;
        }

        return bluebird.map(requests, function (request) {
          request.action = 'delete';
          return this.updateRequest(request);
        }.bind(this));
      })
      .then(function () {
        return response;
      })
      .catch(errorHandler('removeOldRequests', params));
  },

  insertFbConnect: function (params) {
    cache.delValue(params.fb_uid, 'unlocked_powerups');
    return insertRecord('fb_connect', params);
  },

  updateFbConnect: function (uid, fb_id, disconnect_time) {
    return knex_instance('fb_connect')
      .where('uid', uid)
      .andWhere('fb_uid', fb_id)
      .whereNull('disconnect_time')
      .update({disconnect_time: disconnect_time});
  },

  getVersionPatch: function (app_version) {
    return cache.getValue(app_version, 'version_patch')
      .catch(this.fetchVersionPatch.bind(this,
        app_version));
  },

  fetchVersionPatch: function (app_version) {
    var path = config.puzzle_path;

    return fs.readFileAsync(path + app_version + '.json')
      .then(function (data) {
        data = JSON.parse(data);
        cache.addValue(app_version, data,
          'version_patch');
        cache.push('design_patches', app_version,
          'patch_version');
        return data;
      })
      .catch(function (err) {
        if (err && err.code === 'ENOENT') {
          return 'EMPTY';
        }
        return bluebird.reject(err);
      });
  },

  fetchCurrentSale: function (platform) {
    return knex_instance('sale_data')
      .where('platforms', 'like', ['%' + platform + '%'])
      .andWhere(knex_instance.raw('now() < end'))
      .then(function (sale_data) {
        cache.push(platform, sale_data, 'sale');
        return sale_data;
      })
      .catch(errorHandler('fetchCurrentSale', platform));
  },

  getCurrentSale: function (platform) {
    return cache.getValue(platform, 'sale')
      .catch(this.fetchCurrentSale.bind(this, platform))
      .then(function (sale_data_arr) {
        return _.find(sale_data_arr, function (sale_data) {
          var curr_date = new Date().getTime(),
            start_time = new Date(sale_data.start).getTime(),
            end_time = new Date(sale_data.end).getTime();

          return curr_date >= start_time  && curr_date <= end_time;
        });
      });
  },
  getCurrentFbConnect: function (fb_id, uid) {
    var promise = knex_instance('fb_connect')
      .where('fb_uid', fb_id)
      .whereNull('disconnect_time');

    if (uid) {
      promise.where('uid', uid);
    }

    return promise.orderBy('connect_time', 'asc')
      .first()
      .catch(errorHandler('getCurrentFbConnect', uid));
  },

  getLastFbConnect: function (fb_id) {
    return knex_instance('fb_connect')
      .where('fb_uid', fb_id)
      .whereNotNull('disconnect_time')
      .orderBy('connect_time', 'asc')
      .first()
      .catch(errorHandler('getLastFbConnect', fb_id));
  },

  getNonTrackedDevicePuzzles: function (uid, fb_id) {
    return knex_instance('fb_connect')
      .where('uid', uid)
      .andWhere('fb_uid', fb_id)
      .whereNotNull('disconnect_time')
      .orderBy('connect_time', 'desc')
      .select('disconnect_time')
      .first()
      .then(function (data) {
        var puzzle_promise = knex_instance('user_gs_stats_all')
          .where('uid', uid);

        if (data) {
          puzzle_promise = puzzle_promise
            .where('date_time', '>=', data.disconnect_time);
        }

        return puzzle_promise
          .select('ms_id', 'score', 'stars', 'progress', 'time_taken',
            'bonus_level', 'date_time');
      })
      .catch(errorHandler('getNonTrackedDevicePuzzles', {
        fb_id: fb_id,
        uid: uid
      }));
  },

  getLastDisconnectTime: function(uid, fb_id) {
    return knex_instance('fb_connect')
      .where('uid', uid)
      .andWhere('fb_uid', fb_id)
      .max('disconnect_time as time')
      .first()
      .then(function(data) {
        return data.time === null ? 0: data.time;
      })
      .catch(errorHandler('getLastDisconnectTime', {
        fb_id: fb_id,
        uid: uid
      }));
  },

  getUsedCurrencyFrom: function(uid, time) {
    return knex_instance('inventory_purchase')
      .where('uid', uid)
      .andWhere('date_time', '>=', time)
      .select('free_currency', 'paid_currency')
      .then(function(data) {
        return data;
      })
      .catch(errorHandler('getUsedCurrencyFrom', {
        uid: uid,
        time: time
      }));
  },

  getPurchasesFrom: function(uid, time) {
    var ret_data = [];

    return knex_instance('cash_purchase')
      .where('uid', uid)
      .andWhere('date_time', '>=', time)
      .select('currency_bundle', 'sale_id')
      .then(function(data_arr) {
        var unique_sale_ids = [];

        ret_data = data_arr;
        _.each(data_arr, function (curr) {
          var sale_id = curr.sale_id;

          if (sale_id > 0 && unique_sale_ids.indexOf(sale_id) === -1) {
            unique_sale_ids.push(sale_id);
          }
        });

        if (unique_sale_ids.length === 0) {
          return bluebird.resolve([]);
        } else {
          return knex_instance('sale_data')
            .whereIn('id', unique_sale_ids)
            .select('id', 'multiplier');
        }
      })
      .then(function (sale_data) {
        var sale_info = {};

        _.each(sale_data, function (data) {
          sale_info[data.id] = data.multiplier;
        });

        /* For each transaction getting the multiplier value
          as there can be several sale running since last connection
        */
        _.each(ret_data, function (data) {
          data.multiplier = sale_info[data.sale_id];
        });

        return ret_data;
      })
      .catch(errorHandler('getPurchasesFrom', {
        uid: uid,
        time: time
      }));
  },

  getFreeCurrencyFrom: function(uid, time) {
    return knex_instance('track_free_goods')
      .where('uid', uid)
      .andWhere('time', '>=', time)
      .andWhere('item', 'cash')
      .sum('amount as amount')
      .first()
      .then(function(data) {
        return data.amount === null ? 0 : data.amount;
      })
      .catch(errorHandler('getFreeCurrencyFrom', {
        uid: uid,
        time: time
      }));
  },

  getUnlockedPowerups: function (fb_uid) {
    var cache_type = 'unlocked_powerups';

    return cache.getValue(fb_uid, cache_type)
      .catch(function () {
        return knex_instance('fb_connect')
        .where('fb_uid', fb_uid)
        .pluck('uid')
        .then(function (uids) {
          return knex_instance('track_free_goods')
            .whereIn('uid', uids)
            .andWhere('source', 'first_time_bonus')
            .select('item', 'source')
            .then(function (items) {
              return cacheUnlockedPowerups(items);
            });
        });
      });
  },

  read: function (id) {
    var base_path = path.join(process.cwd(),
      config.puzzle_base_path),
      data;

    try {
      data = require(base_path + id + '.json');
      memcache.addValue(id, data, 'puzzle');
    } catch (err) {
      return bluebird.reject(err);
    }

    return bluebird.resolve(data);
  },

  getPuzzle: function (id) {
    return cache.getValue(id, 'puzzle')
      .catch(this.read.bind(this, id));
  },

  deleteFbUser: function (fb_uid) {
    var tables = [
        ['user_gs_stats', 'fb_uid'],
        ['user_info', 'fb_uid'],
        ['fb_user_game', 'fb_uid'],
        ['fb_connect', 'fb_uid'],
        ['requests_sent', 'sender'],
        ['requests_sent', 'receiver'],
        ['processed_requests', 'sender'],
        ['processed_requests', 'receiver'],
        ['user_bonus_game_stats', 'fb_uid']
      ];

    return bluebird.map(tables, function (curr) {
        return deleteRecord(curr[0], curr[1], fb_uid);
      })
      .then(cache.deleteUserData.bind(cache, fb_uid));
  }
});

module.exports = new
  (Class(Base, util.getGamePath('models/db/db', Base)))(); //jshint ignore:line

test.prepare(module.exports, {
  knex: knex_instance,
  cache: cache,
  logger: logger,
  fetchFirstRow: fetchFirstRow,
  errorHandler: errorHandler,
  fetchUserGame: fetchUserGame,
  checkUpdateScoreCache: checkUpdateScoreCache,
  insertRecord: insertRecord,
  insertEachValue: insertEachValue,
  fetchDeviceInfo: fetchDeviceInfo,
  fetchUserByFBId: fetchUserByFBId,
  setFunction: function (function_name, function_def) {
    eval(function_name + '=' + function_def); //jshint ignore:line
  },
  updateUserCache: updateUserCache,
  fetchCompletedUserPuzzleData: fetchCompletedUserPuzzleData,
  fetchFriendData: fetchFriendData,
  copyRequest: copyRequest,
  deleteRequest: deleteRequest,
  fetchRequests: fetchRequests,
  fetchAndUpdateUser: fetchAndUpdateUser,
  fetchAddBonusPlayed: fetchAddBonusPlayed,
  deleteRecord: deleteRecord,
  cacheUnlockedPowerups: cacheUnlockedPowerups
});
