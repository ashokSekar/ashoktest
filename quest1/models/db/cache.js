'use strict';

var Memcached = require('memcache-promise'),
  _ = require('lodash'),
  Class = require('js-class'),
  bluebird = require('bluebird'),
  Bluebird = bluebird,
  test = require('../../test/lib/private'),
  util = require('../../modules/util'),
  config = require('../../resources/config'),
  game_prefix = config.memcache.prefix,
  lifetime = config.memcache.lifetime,
  memcached = new Memcached(config.memcache.servers, config.memcache.options),
  getKey = function (id, type) {
    return game_prefix + type + ':' + id;
  },
  Base = Class({ //jshint ignore:line
    getValue: function (id, type) {

      // Converting memcache's q promise to bluebird
      return new Bluebird(function (resolve, reject) {
        memcached.get(getKey(id, type))
          .then(function (res) {
            if (res) {
              resolve(res);
            } else {
              reject('not_found');
            }
          })
          .catch(reject);
      });
    },

    addValue: function (id, data, type) {

      // Converting memcache's q promise to bluebird
      return new Bluebird(function (resolve, reject) {
        memcached.set(getKey(id, type), data, lifetime)
          .then(resolve)
          .catch(reject);
      });
    },

    push: function (key, value, type) {
      if (!_.isArray(value)) {
        value = [value];
      }

      return this.getValue(key, type)
        .bind(this)
        .then(function (data) {
          return this.addValue(key, _.union(data, value), type);
        })
        .catch(this.addValue.bind(this, key, value, type));
    },

    delValue: function (id, type) {

      // Converting memcache's q promise to bluebird
      return new Bluebird(function (resolve, reject) {
        memcached.del(getKey(id, type))
          .then(resolve)
          .catch(reject);
      });
    },

    addOrUpdateScoreCache: function (puzzle, update) {
      var fb_id = puzzle.fb_uid;

      return this.getValue(fb_id, 'highscores')
        .bind(this)
        .then(function (data) {
          var index;

          if (update) {
            index = _.findIndex(data, function (entry) {
              return entry.ms_id === puzzle.ms_id;
            });

            if (index !== -1) {
              data[index] = puzzle;
            } else {
              data.push(puzzle);
            }
          } else {
            data.push(puzzle);
          }

          return this.addValue(fb_id, data, 'highscores');
        })
        .catch(function (err) {
          if (err === 'not_found') {
            return this.addValue(fb_id, [puzzle], 'highscores');
          }

          throw err;
        });
    },

    getFriendData: function (fb_ids) {
      var ms_data = [],
        non_cached_ids = [],
        mem_ids = [];

      _.each(fb_ids, function (curr_id) {
        mem_ids.push(getKey(curr_id, 'highscores'));
      });

      // Converting memcache's  q promise to bluebird
      return new Bluebird(function (resolve) {
        memcached.getMulti(mem_ids)
          .then(function (data) {
            _.each(mem_ids, function (mem_id) {
              var curr_id = mem_id.split('highscores:')[1],
                curr_data = data[mem_id];

              if (curr_data) {
                ms_data = _.concat(ms_data, curr_data);
              } else {
                non_cached_ids.push(curr_id);
              }
            });

            resolve({
              ms_data: ms_data,
              non_cached_ids: non_cached_ids
            });
          })
          .catch(resolve.bind(null, {
            ms_data: ms_data,
            non_cached_ids: fb_ids
          }));
      });
    },

    deleteRequest: function (request_id, receiver_id) {
      return this.getValue(receiver_id, 'requests')
        .bind(this)
        .then(function (requests) {
          requests = _.filter(requests, function (current) {
            return current.request_id !== request_id;
          });

          return this.addValue(receiver_id, requests, 'requests');
        }, function () {
          return false;
        });
    },

    insertRequests: function (requests) {

      return bluebird.map(requests, function (current) {
        return this.push(current.receiver, current, 'requests');
      }.bind(this));
    },

    deleteUserData: function (uid) {
      return bluebird.map(['user_record', 'highscores',
        'requests', 'bonus_levels'], function (curr_type) {
          return this.delValue(uid, curr_type);
        }.bind(this));
    }
  });

module.exports = new (Class(Base, //jshint ignore:line
  util.getGamePath('models/db/cache', Base)))();

test.prepare(module.exports, {
  memcached: memcached,
  getKey: getKey
});
