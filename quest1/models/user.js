'use strict';

var Base,
  _ = require('lodash'),
  compareVersion = require('compare-version'),
  Class = require('js-class'),
  facebook = require('../modules/facebook'),
  bluebird = require('bluebird'),
  config = require('../resources/config'),
  db = require('./db/db'),
  job = require('../modules/job'),
  logger = require('../modules/debug')('USER'),
  util = require('../modules/util'),
  test = require('../test/lib/private'),
  purchase_validator = require('../modules/purchase_validator'),
  powerups = config.powerups,
  powerups_all = _.concat(powerups.pregame, powerups.ingame),
  powerups_count_init = _.reduce(powerups_all, function (result, curr_type) {
    result[curr_type] = 0;
    return result;
  }, {}),
  updatePuzzles = function (puzzles, max) {
    if (!puzzles || puzzles.length <= 0) {
      return bluebird.resolve();
    }

    if (!max) {
      puzzles.date_time = util.getDate(puzzles.date_time);
      _.map(puzzles, function (puzzle) {
        puzzle.date_time = util.getDate(puzzle.date_time);
        return puzzle;
      });
      return db.insertPuzzleAll(puzzles);
    } else {
      return bluebird.map(puzzles, function (max_puzzle) {
        return db.addOrUpdateMaxPuzzle(max_puzzle);
      });
    }
  },
  updateBonusPuzzles = function (bonus_stats) {
    if (!bonus_stats || bonus_stats.length <= 0) {
      return bluebird.resolve();
    } else {
      _.map(bonus_stats, function (bonus) {
        bonus.date_time = util.getDate(bonus.date_time);
      });
      return db.insertBonusStats(bonus_stats);
    }
  },
  updateUserData = function (user_info, user_game, user_device) {
    var db_ops = [db.updateUserGame(user_game)];

    if (user_info) {
      db_ops.push(db.updateUserInfo(user_info));
    }

    if (user_device) {
      db_ops.push(db.updateUserDevice(user_device));
    }

    return bluebird.all(db_ops);
  },
  getFreeGoodsCount = function (free_goods) {
    var free_goods_count = _.merge({
        cash: 0
      }, powerups_count_init),
      curr_type;

    _.each(free_goods, function (curr_good) {
      if (curr_good.item === 'lives') {
        return;
      }

      curr_type = _.findKey(free_goods_count, function (count, type) {
        return type === curr_good.item;
      });

      free_goods_count[curr_type] = free_goods_count[curr_type] +
        curr_good.amount;
    });

    return free_goods_count;
  },
  getUsedPowerups = function (puzzles) {
    var powerups_used = _.clone(powerups_count_init),
      pregame_powerups = config.powerups.pregame,
      curr_count = 0;

    _.each(puzzles, function (curr_puzzle) {
      _.each(powerups_used, function (count, type) {
        curr_count = curr_puzzle[type];
        curr_count = curr_count && _.indexOf(pregame_powerups, type) !== -1 ?
          1 : curr_count;

        /* (curr_count || 0) is because for old installs, new powerups wont
         * be there. To avoid NaN value for those cases
         */
        powerups_used[type] = count + (curr_count || 0);
      });
    });

    return powerups_used;
  },
  getCreditData = function (credits) {
    var data = {
        currency: 0,
        spent: 0
      },
      curr_bundle;

    _.each(credits, function (current) {
      curr_bundle = current.item || current.currency_bundle;

      data.currency = data.currency +
        util.getNoOfCash(curr_bundle, current.multiplier);
      data.spent = data.spent + util.getCost(curr_bundle);
    });

    return data;
  },
  getPurchaseData = function (purchases) {
    var inventory_purchase = _.reduce(config.inventories,
        function (result, curr_type) {
          result[curr_type] = 0;
          return result;
        }, {}
      ),
      currency_used = {
        free: 0,
        paid: 0
      },
      curr_type, curr_currency;

    _.each(purchases, function (curr_purchase) {
      curr_currency = curr_purchase.currency_used;
      curr_type = _.findKey(inventory_purchase, function (count, type) {
        return type === curr_purchase.item;
      });

      inventory_purchase[curr_type] = inventory_purchase[curr_type] +
        curr_purchase.quantity;
      currency_used.free = currency_used.free + (curr_currency ?
        curr_currency.free : curr_purchase.free_currency);
      currency_used.paid = currency_used.paid + (curr_currency ?
        curr_currency.paid : curr_purchase.paid_currency);
    });

    return {
      powerup_count: _.pick(inventory_purchase, powerups_all),
      currency_used: currency_used
    };
  },
  getCurrentUserGameData = function (user, puzzles) {
    var purchase_data = getPurchaseData(user.purchases),
      powerup_purchase = purchase_data.powerup_count,
      currency_used = purchase_data.currency_used,
      credit_data = getCreditData(user.credits),
      used_powerups = getUsedPowerups(puzzles),
      free_data = getFreeGoodsCount(user.track_free_goods),
      data = {};

    _.each(powerup_purchase, function (count, type) {
      data[type] = count + free_data[type] - used_powerups[type];
    });

    data.free_currency = free_data.cash -
      currency_used.free;
    data.paid_currency = credit_data.currency - currency_used.paid;
    data.amount_spent = credit_data.spent;

    return data;
  },
  updateCurrency = function (user_game, uid, fbuid, prev_fbuid) {
    // TODO: Change logic to get values from all fb connected
    // device data.

    // Get max disconnect time
    // Get all purchases from disconnect time to now
    // Get all inventory_purchases from disconnect time to now
    // Current currency + purchases - inventory_purchases
    //    will be new currency

    if (prev_fbuid === fbuid) {

      return db.getLastDisconnectTime(uid, fbuid)
        .then(function(time) {
          return bluebird.all([
            db.getUsedCurrencyFrom(uid, time),
            db.getPurchasesFrom(uid, time),
            db.getFreeCurrencyFrom(uid, time)
          ]);
        })
        .spread(function(transactions, purchases, received_free) {
          var used_free = 0,
            used_paid = 0,
            bought_paid = 0,
            free, paid;

          _.each(transactions, function(val) {
            used_free += val.free_currency;
            used_paid += val.paid_currency;
          });

          bought_paid += _.sumBy(purchases, function(val) {
            return util.getNoOfCash(val.currency_bundle, val.multiplier);
          });

          free = user_game.free_currency -
            used_free + received_free;
          paid = user_game.paid_currency -
            used_paid + bought_paid;

          user_game.free_currency = free > 0 ? free: 0;
          user_game.paid_currency = paid > 0 ? paid: 0;
        });
    }
    return bluebird.resolve();
  },

  updateCashPurchase = function (credit, uid, fb_uid) {
    var item = credit.item;

    return db.saveCredits([getCreditObject(credit, uid)])
      .then(function(result) {
        if (result === true) {
          return db.getUserGame(uid, fb_uid);
        }
        return bluebird.reject();
      })
      .then(function(user_game) {
        user_game.paid_currency = user_game.paid_currency +
          util.getNoOfCash(item, credit.multiplier);
        user_game.amount_spent = user_game.amount_spent +
          util.getCost(item);
        return db.updateUserGame(user_game);
      })
      .catch(function () {
        return false;
      });
  },

  getCreditObject = function (credit, uid) {
    return {
      uid: uid,
      ms: credit.curr_ms,
      date_time: util.getDate(credit.time),
      currency_bundle: credit.item,
      local_amount: credit.amount,
      local_currency: credit.currency,
      usd_amount: credit.usd_amount,
      receipt: credit.receipt,
      source: credit.source,
      added_on: util.getDate(),
      sale_id: credit.sale_id
    };
  },

  getNotifications = function (uid, notifications) {
    var notif_arr = [];

    _.forEach(notifications, function (curr_notif) {
      var additional_data = curr_notif.additional_data || {};

      if (_.isString(additional_data)) {
        additional_data = JSON.parse(additional_data);
      }

      notif_arr.push({
        uid: uid,
        notification_id: curr_notif.notification_id,
        opened_on: util.getDate(curr_notif.clicked_time),
        segment: additional_data.segment_name || null,
        title: curr_notif.title,
        content: curr_notif.body
      });
    });

    return notif_arr;
  },
  pushSyncJobs = function (data) {
    job.push('sync_inserts', _.omit(data, 'notifications'));

    if (data.notifications && data.notifications.length > 0) {
      job.push('sync_notifications', _.pick(data, 'uid', 'notifications'));
    };
  };

Base = Class({ // jshint ignore:line

  getDb: function () {
    return db;
  },

  getJobData: function (puzzle, user) {
    return {
      puzzle_data: puzzle.puzzle_data,
      purchases: user.purchases,
      credits: user.credits,
      free_goods: user.track_free_currency || user.track_free_goods
    };
  },

  syncData: function (data, ip_address) {
    var user = data.user,
      puzzles = data.puzzle,
      device = data.device,
      notifications = data.notifications,
      uid = user.uid,
      fb_id = user.fb_uid,
      curr_date = new Date(),
      app_version = device.appversion,
      design_version = device.design_version,
      position_data = {},
      friend_uids = _.keys(user.friends),
      puzzle_highscore, job_data, bonus_levels;

    user.ip_address = ip_address;
    device.is_mobile = data.is_mobile;

    return this.addOrUpdateUserData(user, device, puzzles)
      .then(this.puzzleUpdateAndGetMax.bind(this, puzzles, uid, fb_id,
        app_version))
      .then(function (data) {
        puzzle_highscore = data.puzzle_highest;
        bonus_levels = data.bonus_played;
        job_data = _.merge({
          uid: uid,
          fb_uid: fb_id,
          notifications: notifications
        }, this.getJobData(data, user));
        return true;
      }.bind(this))
      .then(function () {
        return bluebird.all([
          this.getVersionPatch(app_version, design_version),
          this.getCrossPromotion(device, user),
          this.getSaleAndRewards(device, user)
        ]);
      }.bind(this))
      .spread(function (patch, cross_promotion, sale_rewards) {
        var sale = sale_rewards.sale,
          rewards = sale_rewards.rewards;

        if (fb_id) {
          return db.getFriendPositions(friend_uids)
            .then(function (positions) {
              position_data = positions;

              return db.getFriendHighscores(friend_uids);
            })
            .then(function (scores) {
              return this.postSync({
                uid: uid,
                fb_id: fb_id,
                bonus_levels_played: bonus_levels,
                score: puzzle_highscore,
                friend_scores: scores,
                friend_positions: position_data,
                puzzle_patch: patch,
                cross_promotion: cross_promotion,
                rewards: rewards,
                date: curr_date.toString(),
                sale: sale,
                job_data: job_data
              });
            }.bind(this));
        } else {
          pushSyncJobs(job_data);
          return {
              cross_promotion: cross_promotion,
              puzzle_patch: patch,
              rewards: rewards,
              sale: sale,
              date: curr_date.toString()
            };
        }
      }.bind(this))
      .catch(function (error) {
        logger.error('user sync %s', error);
        return bluebird.reject(error);
      });
  },
  getCrossPromotion: function (device, user) {
    var curr_config = config.cross_promotion,
      data = null,
      store = device.store,
      max_ms = user.max_ms;

    if (curr_config && curr_config.id && curr_config[store] &&
      curr_config.min_ms <= max_ms) {
      data = _.merge({
        id: curr_config.id,
        universal_link: curr_config.universal_link
      }, curr_config[store]);
    }
    return data;
  },
  getRewards: function (user, app_version) {
    var rewards = config.rewards,
      min_version = config.rewards_min_version,
      comp = compareVersion(app_version, min_version),
      max_ms = user.max_ms,
      rewards_given = user.rewards_given || [],
      paid_user = user.paid > 0;

    if (_.isNumber(comp) && comp >= 0) {
      return _.find(rewards, function (reward) {
        return max_ms >= reward.min_ms &&
          (reward.paid_user || !paid_user) &&
          _.indexOf(rewards_given, reward.id) < 0;
      });
    }

    return null;
  },
  getSaleAndRewards: function (device, user) {
    var app_version = device.appversion;

    return this.getCurrentSale(device.store)
      .then(function (sale) {
        var rewards = null;

        if (!sale) {
          rewards = this.getRewards(user, app_version);
        }
        return {
          sale: sale,
          rewards: rewards
        };
      }.bind(this));
  },
  getCurrentSale: function (store) {
    return db.getCurrentSale(store)
      .then(function (sale_data) {
        // Checking difference b/w start and end data from server side
        if (sale_data) {
          sale_data.duration =
            Math.floor((new Date(sale_data.end).getTime() - Date.now()) / 1000);
          return _.pick(sale_data, 'id', 'name', 'duration',
                'image', 'multiplier');
        }
        return sale_data;
      });
  },
  getVersionPatch: function (app_version, user_version) {
    return db.getVersionPatch(app_version)
      .then(function (patch) {
        return (!patch ||
          patch === 'EMPTY' ||
          patch.version === user_version) ? false : patch;
      })
      .catch(function () {
        return bluebird.reject(false);
      });
  },

  // Needed for overriding in puzzleUpdateAndGetMax
  getPuzzle: function (puzzle, uid, version) {
    return {
      uid: uid,
      ms_id: puzzle.id,
      bonus_level: puzzle.bonus_level || 0,
      date_time: puzzle.timestamp,
      time_taken: puzzle.time,
      score: puzzle.score,
      stars: puzzle.stars,
      progress: puzzle.progress || 0,
      app_version: puzzle.app_version || version,
      design_version: puzzle.design_version || null,
      instance_id: puzzle.instance_id,
      exit_type: puzzle.exit_type || null
    };
  },

  // override to add more property to gs_stats
  getGsStatsTemplate: function () {
    return {};
  },

  puzzleUpdateAndGetMax: function (puzzles, uid, fb_uid, version, update_max) {
    var all_puzzles = [],
      max_puzzles = {},
      bonus_stats = {},
      processPuzzle = function (puzzle, client) {
        var puzzl_id = puzzle.id || puzzle.ms_id,
          puzzle_data = !client ? puzzle : this.getPuzzle(puzzle, uid, version),
          bonus_level = puzzle_data.bonus_level,
          puzzle_max;

        if (fb_uid) {
          if (!bonus_level) {
            puzzle_max = max_puzzles[puzzl_id] || _.merge({
              ms_id: puzzl_id,
              score: 0,
              stars: 0,
              progress: 0,
              time_taken: 0
            }, this.getGsStatsTemplate());

            puzzle_max = util.getMaxPuzzleData(puzzle_max,
              puzzle_data);
            puzzle_max.fb_uid = fb_uid;
            max_puzzles[puzzl_id] = puzzle_max;
          } else if (puzzle_data.progress >= 100) {
            puzzle_max = bonus_stats[bonus_level] || {
              fb_uid: fb_uid,
              id: bonus_level,
              date_time: puzzle_data.date_time
            };
            bonus_stats[bonus_level] = puzzle_max;
          }
        }

        if (client) {
          all_puzzles.push(puzzle_data);
        }
      }.bind(this),
      calculateHighestAndResolve = function (data, bonus_played) {
        var ms_list = _.keys(max_puzzles),
          added_ms_list = [],
          curr_ms, curr_max;

        if (ms_list.length === 0) {
          return bluebird.resolve({
            puzzle_highest: data,
            bonus_played: bonus_played,
            puzzle_data: {
              gs_stats_all: all_puzzles,
              bonus_stats: _.values(bonus_stats)
            }
          });
        }

        _.each(data, function (curr_puzzle) {
          curr_ms = curr_puzzle.ms_id;
          curr_max = max_puzzles[curr_ms];

          if (_.includes(ms_list, curr_ms)) {
            added_ms_list.push(curr_ms);
            if (curr_max.progress === 100 &&
              curr_max.score > curr_puzzle.score) {
              curr_puzzle.score = curr_max.score;
              curr_puzzle.stars = curr_max.stars;
            }
          }
        });

        if (added_ms_list.length !== ms_list.length) {
          data = data || [];
          _.mapValues(max_puzzles, function (puzzle, ms_id) {
            if (!_.includes(added_ms_list, ms_id) && puzzle.progress === 100) {
              data.push(_.pick(puzzle, 'ms_id', 'score', 'stars'));
            }
          });
        }
        updatePuzzles(_.values(max_puzzles), true);

        return bluebird.resolve({
          puzzle_highest: data,
          bonus_played: bonus_played,
          puzzle_data: {
            gs_stats_all: all_puzzles,
            bonus_stats: _.values(bonus_stats)
          }
        });
      },
      puzzle_promise = null;

    if (puzzles && puzzles.length > 0) {
      _.each(puzzles, function (puzzle) {
        processPuzzle(puzzle, true);
      });
    }

    if (!fb_uid) {
      return calculateHighestAndResolve(null);
    } else {
      puzzle_promise = update_max ? db.getNonTrackedDevicePuzzles(uid, fb_uid) :
        bluebird.resolve(null);
      return puzzle_promise
        .then(function (device_puzzles) {
          _.each(device_puzzles, function (puzzle) {
            processPuzzle(puzzle);
          });

          return bluebird.all([
            db.getCompletedUserPuzzleData(fb_uid),
            db.getCompletedBonusLevels(fb_uid)
          ]);
        })
        .spread(function (max_puzzles, bonus_played) {
          var curr_bonus_arr = _.map(_.values(bonus_stats), 'id');

          return calculateHighestAndResolve(max_puzzles,
            _.union(curr_bonus_arr, bonus_played));
        });
    }
  },

  insertPuzzleData: function (data) {
    return bluebird.all([
        updatePuzzles(data.gs_stats_all),
        updateBonusPuzzles(data.bonus_stats)
      ])
      .spread(function (all_resp) {
        return all_resp;
      });
  },

  // Override to get data based on user_info table
  getUserInfo: function (user) {
    return util.undefTrim({
      fb_uid: user.fb_uid,
      gender: user.gender,
      first_name: user.first_name,
      last_name: user.last_name,
      dob: util.getDate(user.dob)
    });
  },

  // Override to get data based on user_game table
  getUserGame: function (user, extra) {
    var game_sounds = user.game_sounds || {};

    return util.undefTrim(_.merge({
      uid: user.uid,
      fb_uid: user.fb_uid,
      paid_currency: user.currency_paid,
      free_currency: user.currency_free,
      max_ms: user.max_ms,
      curr_ms: user.curr_ms,
      master_score: user.score,
      music_muted: !game_sounds.music,
      sound_muted: !game_sounds.effect,
      amount_spent: user.paid,
      fb_friend_count: _.keys(user.friends).length
    }, extra));
  },

  // Override to get data based on user_device table
  getUserDevice: function (user, device) {
    return util.undefTrim({
      device_id: device.advertising_id,
      type: device.type,
      uid: user.uid,
      device_name: device.name,
      is_tablet: device.is_tablet,
      os: device.os,
      jail_broken: device.jail_broken,
      install_time: util.getDate(device.install_date),
      start_time: util.getDate(user.start_time),
      current_design_version: device.design_version,
      current_version: device.appversion,
      location: user.location,
      language: user.language || 'en',
      source: user.source,
      ip_address: user.ip_address,
      lives: user.inventory_lives,
      last_update: new Date(),
      rating: parseInt(user.rating_done) || 0,
      avg_session_length: parseFloat(user.ps_avg_session_length) || -1,
      churn_probability: parseFloat(user.ps_churn_prob) || -1,
      days_since_last_played: parseFloat(user.ps_since_last_played) || -1,
      high_spender_probability: parseFloat(user.ps_high_spender_prob) || -1,
      no_of_purchases: parseFloat(user.ps_no_of_purchases) || -1,
      no_of_sessions: parseFloat(user.ps_no_of_sessions) || -1,
      session_percentile: parseFloat(user.ps_session_percentile) || -1,
      spend_percentile: parseFloat(user.ps_spend_percentile) || -1,
      spend_probability: parseFloat(user.ps_spend_probability) || -1,
      total_spend_next_28_days: parseFloat(user.ps_total_spend_next28) || -1,
      install_referrer: device.install_referrer || 'unknown'
    });
  },

  updateData: function (master_data, curr_game_data, user_game) {
    master_data = master_data || {};
    _.each(curr_game_data, function (count, type) {
      user_game[type] = (master_data[type] || 0) + count;
    });
  },

  addOrUpdateUserData: function (user, device, puzzles) {
    var uid = user.uid,
      fbuid = user.fb_uid,
      max_ms = user.max_ms,
      master_score = user.score,
      prev_fbuid = user.prev_fb_uid,
      disconnect_time = util.getDate(user.fb_disconnect_time || null),
      curr_game_data = getCurrentUserGameData(user, puzzles),
      user_info = this.getUserInfo(user),
      user_game = this.getUserGame(user),
      user_device = this.getUserDevice(user, device),
      user_fbconnect = {
        uid: uid,
        fb_uid: fbuid,
        connect_time: util.getDate(user.fb_connect_time)
      },
      reload_max_puzzle = false,
      getGameData = function (fb) {
        var omit_props = fb ? ['uid'] :
          ['fb_uid', 'fb_friend_count'];

        return _.omit(user_game, omit_props);
      },
      updateData = this.updateData.bind(this);

    return db.getUserRecord(uid, fbuid)
      .then(function (user) {
        var info = user.info,
          game = user.game,
          device = user.device,
          user_promise;

        // First time user
        user_promise = !device ? db.insertUser(user_device, getGameData()) :
          bluebird.resolve(null);

        if (!disconnect_time) {
          updateData(game, curr_game_data, user_game);
        }

        if (fbuid && !info) {
          // Firsttime fb user
          user_promise = user_promise
            .then(db.insertFbConnect.bind(null, user_fbconnect))
            .then(db.getUserGame.bind(null, uid, null))
            .then(function (nonfb_game) {
              updateData(nonfb_game, curr_game_data, user_game);
              reload_max_puzzle = true;

              return db.insertFbUser(user_info, getGameData(true));
            });
        } else if (fbuid) {
          // Existing fb user

          user_promise = user_promise
            .then(db.getCurrentFbConnect.bind(db, fbuid, uid))
            .then(function (fb_connect) {
              user_game.max_ms = Math.max(max_ms, game.max_ms);
              user_game.master_score = Math.max(master_score,
                game.master_score);

              return fb_connect ? false : db.insertFbConnect(user_fbconnect)
                .then(updateCurrency.bind(null, user_game, uid,
                  fbuid, prev_fbuid))
                .then(function() {
                  _.each(powerups_all, function (prop) {
                    user_game[prop] = Math.max(game[prop], user_game[prop]);
                  });
                  return true;
                });
            })
            .then(function (puzzle_reload) {
              reload_max_puzzle = puzzle_reload;

              return updateUserData(user_info, getGameData(true),
                device ? user_device : null);
            });
        } else if (device) {
          // Existing non fb user

          user_promise = user_promise
            .then(updateUserData.bind(null, null, getGameData(), user_device));
        }

        if (disconnect_time && prev_fbuid) {
          // Update disconnect data. Delete prev_fb_uid from client side

          user_promise = user_promise
            .then(db.updateFbConnect.bind(null, uid, prev_fbuid,
              disconnect_time));
        }

        return user_promise;
      })
      .then(function () {
        return reload_max_puzzle;
      });
  },

  savePurchases: function (purchases, uid) {
    var purchases_arr = [];

    if (purchases && purchases.length > 0) {
      _.each(purchases, function (current) {
        var currency = current.currency_used;

        purchases_arr.push({
          uid: uid,
          item: current.item,
          date_time: util.getDate(current.time),
          quantity: current.quantity || 0,
          free_currency: currency.free || 0,
          paid_currency: currency.paid || 0,
          remaining_currency: current.currency_remaining || 0,
          curr_ms: current.curr_ms || 1,
          max_ms: current.max_ms || 1,
          instance_id: current.instance_id
        });
      });
    }

    return db.savePurchases(purchases_arr);
  },

  saveCredits: function (credits, uid) {
    var credits_arr = [];

    if (credits && credits.length > 0) {
      _.each(credits, function (current) {
        credits_arr.push(getCreditObject(current, uid));
      });
    }

    return db.saveCredits(credits_arr);
  },

  trackFreeGoods: function (free_goods, uid, fb_uid) {
    var free_goods_data = [],
      val;

    if (free_goods.length > 0) {
      _.each(free_goods, function (current) {
        val = {
            uid: uid,
            item: current.item || null,
            ms: current.curr_ms,
            time: util.getDate(current.time),
            amount: current.amount,
            source: current.source,
            instance_id: current.instance_id || null
          };
        free_goods_data.push(val);
      });
      return db.trackFreeGoods(free_goods_data, fb_uid);
    }
    return bluebird.resolve(null);
  },

  postSync: function (data) {
    var fbuid = data.fb_id;

    delete data.fb_id;

    return bluebird.all([
      db.getUserByFBId(fbuid),
      db.getRequests(fbuid),
      db.getUnlockedPowerups(fbuid)
    ])
    .spread(function (user, requests, unlocked_powerups) {
      data.user = user.game;
      data.user.unlocked_powerups = unlocked_powerups;
      data.requests = requests;
      pushSyncJobs(data.job_data);
      delete data.job_data;
      return data;
    });
  },

  addRequests: function (data) {
    var requests = [],
      type = data.type,
      sender = data.sender,
      request_id = data.request,
      fb_action = data.fb_action,
      receivers = data.to;

    return bluebird.map(receivers, function (receiver) {
      var obj = {
        receiver: receiver,
        sender: sender,
        type: type,
        request_id: request_id,
        fb_action: fb_action
      };

      requests.push(obj);
      return this.removeOldRequests(obj)
        .then(db.insertRequests.bind(null, requests));
    }.bind(this))
    .finally(function () {
      return requests;
    });
  },

  removeOldRequests: function (req) {
    return db.removeOldRequests(req)
      .then(function (requests) {
        _.each(requests, function (request) {
          facebook.deleteRequest(request.request_id + '_' + request.receiver);
        });
        return requests;
      });
  },

  updateRequest: function (request) {
    var id = request.request_id,
      receiver = request.receiver;

    request.by = 'user';
    request.action = 'accept';
    return db.updateRequest(request)
      .then(facebook.deleteRequest.bind(null, id + '_' + receiver))
      .then(function () {
        return true;
      });
  },

  // This function to revert game data update of sync for duplicate entry bug.
  revertGameSyncUpdate: function (dup_data, uid, fb_uid) {
    var curr_game_data = getCurrentUserGameData(
        _.omit(dup_data, 'puzzles'), dup_data.puzzles),
      updateGame = function (master_data) {
        _.each(curr_game_data, function (count, type) {
          master_data[type] = master_data[type] - count;
        });

        return db.updateUserGame(master_data);
      },
      promise;

    if (_.find(curr_game_data, function (val) {
        return val !== 0;
      })) {
      promise = db.getUserGame(uid, fb_uid)
        .then(updateGame);
    }

    return promise || bluebird.resolve(true);
  },

  verifyAndSavePurchase: function(req) {
    var cash_purchase = req.purchase_details;

    return bluebird.all([
      purchase_validator.verifyPurchase(req),
      cash_purchase.synced ? bluebird.resolve(true) :
        updateCashPurchase(req.purchase_details, req.uid, req.fb_uid)
    ])
    .spread(function(validation_status, synced) {
      return {
        validation_status: validation_status,
        synced: synced
      };
    });
  },

  insertNotifications: function (uid, notifications) {
    var notif_data = getNotifications(uid, notifications),
      promise;

    if (notif_data.length > 0) {
      promise = db.insertPushNotifications(notif_data);
    }

    return promise || bluebird.resolve(true);
  },

  onUpdateAvailable: function () {
  }
});

module.exports = new
  (Class(Base, util.getGamePath('models/user', Base)))(); //jshint ignore:line

test.prepare(module.exports, {
  updatePuzzles: updatePuzzles,
  getCurrentUserGameData: getCurrentUserGameData,
  getPurchaseData: getPurchaseData,
  updateUserData: updateUserData,
  getFreeGoodsCount: getFreeGoodsCount,
  getUsedPowerups: getUsedPowerups,
  getCreditData: getCreditData,
  updateCurrency: updateCurrency,
  updateCashPurchase: updateCashPurchase,
  updateBonusPuzzles: updateBonusPuzzles,
  getNotifications: getNotifications,
  pushSyncJobs: pushSyncJobs
});
