'use strict';

var bonus_map = require('../models/bonus_map');

bonus_map.reload()
  .then(function () {
    console.log('Reload success');
  })
  .catch(function (err) {
    console.log('Error: ', err);
  })
  .finally(function () {
    process.exit();
  });
