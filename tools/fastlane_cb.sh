#! /bin/bash

cd resources/puzzles
if ([ $FASTLANE_PLATFORM_NAME == "android" ] && ([ $FASTLANE_LANE_NAME == "deploy_production" ] ||
    [ $FASTLANE_LANE_NAME == "amazon_production" ])) || \
    ([ $FASTLANE_PLATFORM_NAME == "ios" ] && [ $FASTLANE_LANE_NAME == "production" ])
then
    git tag -a v$HASHCUBE_VERSION -m "Version update - v$HASHCUBE_VERSION"
    git push --tags
fi
