#!/usr/bin/env bash

SCRIPT_PATH=$(dirname "$(readlink -f "$0")")
cd "$SCRIPT_PATH/.."
node node_modules/quest/error_mails/app.js
node node_modules/quest/error_mails/nginx.js
