Mahjongg Quest Puzzle Generator

Basic Algo

1. Load all tile data in an array. Each item in the array indicates
  a tile position like [x, y, z]
  invoke: loadPuzzle

2. Get a list of faces to assign to these tiles. Ensure that the pairs are shuffled
  like : [f3, f3, f1, f1, f7, f7 ...etc]
  invoke: randomizeFaces

3. We now call a helper function (assignFaces) which attempts to generate a solvable
  puzzles (generateSolvableGame), if an attempt fails, there is a fresh attempt.

4. Core algo:
  * reset all stale data (from previous attempt)
  * gather all tiles from layer 0 (bottommost), store in flatten
  * iterating over each tiles of flatten, randomly pick a few tiles and set an attribute
  free = true for them. ensure that if a tile set free, no other tile can be set free in
  that same line of the same group (onlyFreeInLine).
    Eg: - Assuming [] is a tiles with
      -- [T] having a tile with free true,
      -- [F] having free false

      initially :
        [F][F] [F][F][F]
        [F][F] [F][F][F]

      finally :
        [F][T] [F][T][F]
        [F][T] [T][F][F]

    This `free` attribute allows the generator to pick that tile to assign faces
  * loop from i = 0 to i = tile_count
      if (i is even)
       reset last_position // needed to remove position based allocation dependency

      pos = selectCoordinates(last_position) // will return a position to place the tile
      pos2 = selectCoordinates(last_position) // return a position, maybe same as
      // `pos` or different

      // if last_position is not null, call selectCoordinates with these
      // coordinates to get appropriate position.

      if either pos or pos2 is false,
        restart puzzle goto start
      else
        compare pos & pos2, pick the one which has higher z-position (increase difficulty)
        to pos

      call placeTile to place face at i to pos
      set last_position = pos

      next iteration
    return true
