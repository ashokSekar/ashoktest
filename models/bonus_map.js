'use strict';

var fs = require('fs-extra-promise'),
  compareVersion = require('compare-version'),
  _ = require('lodash'),
  config = require('quest/resources/config'),
  cache = require('quest/models/db/cache'),
  cache_id = 'data',
  cache_type = 'bonus_map',
  getMapData = function (platform_arr, all_data) {
    var resp = [];

    _.each(platform_arr, function (curr_data) {
      var curr_id = curr_data[0],
        start_str = curr_data[1],
        end_str = curr_data[2],
        start_date = new Date(start_str),
        end_date = new Date(end_str),
        end_time = end_date.getTime(),
        start_time = start_date.getTime(),
        curr_map_data;

      if (end_time > Date.now()) {
        curr_map_data = _.clone(all_data[curr_id]);
        curr_map_data.id = curr_id + start_time;
        curr_map_data.start_time = start_time;
        curr_map_data.end_time = end_time;

        resp.push(curr_map_data);
      }
    });

    return resp;
  },
  cacheData = function () {
    var bonus_data = {};

    return fs.readFileAsync(process.cwd() + '/' + config.bonus_map_data_path)
      .then(function (data) {
        data = JSON.parse(data);

        _.each(data.platform, function (curr_info, platform) {
          bonus_data[platform] = getMapData(curr_info, data.data);
        });

        bonus_data.min_ms = data.min_ms;

        return cache.addValue(cache_id, bonus_data, cache_type);
      })
      .return(bonus_data);
  },
  global_min_ms;

module.exports = {
  getCurrentData: function (platform, max_ms, curr_version) {
    return cache.getValue(cache_id, cache_type)
      .catch(function (err) {
        if (err === 'not_found') {
          return cacheData();
        }

        throw err;
      })
      .then(function (bonus_data) {
        global_min_ms = bonus_data.min_ms;

        return _.find(bonus_data[platform], function (curr) {
          var now = Date.now(),
            curr_max_ms = curr.min_ms || global_min_ms;

          return curr.start_time <= now && curr.end_time > now &&
            max_ms >= curr_max_ms;
        });
      })
      .then(function (valid_data) {
        var type = valid_data ? valid_data.type : null,
          min_version, comp;

        min_version = type ? config.bonus_map_min_version[type] : null;
        comp = compareVersion(curr_version, min_version);

        if (valid_data && (!min_version || _.isNumber(comp) && comp >= 0)) {
          valid_data = _.merge({
            time_left: Math.floor((valid_data.end_time -
              Date.now()) / 1000)
          }, _.pick(valid_data, ['id', 'type', 'levels', 'popup_bg']));
        } else {
          valid_data = null;
        }

        return valid_data;
      })
      .catch(function () {
        return null;
      });
  },

  reload: function () {
    return cache.delValue(cache_id, cache_type)
      .then(this.getCurrentData.bind(this));
  }
};
