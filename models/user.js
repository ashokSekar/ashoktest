'use strict';

var  compareVersion = require('compare-version'),
  bluebird = require('bluebird'),
  config = require('../resources/config'),
  util = require('quest/modules/util'),
  _ = require('lodash'),
  bonus_map = require('./bonus_map');

module.exports = {
  getPuzzle: function (puzzle, uid, version) {
    /*jshint maxcomplexity:17 */

    var base_puzzle = this.__base.prototype.getPuzzle.call(
      this, puzzle, uid, version);

    base_puzzle.type = puzzle.type || null;
    base_puzzle.taps_taken = puzzle.tap || 0;
    base_puzzle.hint = puzzle.hint;
    base_puzzle.shuffle = puzzle.shuffle;
    base_puzzle.tile_set = puzzle.tile_set || 'classic';

    // To support upto version v0.1.2 adding (|| 0)
    base_puzzle.free_shuffle = puzzle.free_shuffle || 0;
    base_puzzle.undo = puzzle.undo || 0;

    base_puzzle.magnet = puzzle.magnet || 0;
    base_puzzle.swoosh = puzzle.swoosh || 0;
    base_puzzle.substitute = puzzle.substitute || 0;
    base_puzzle.swap = puzzle.swap || 0;
    base_puzzle.half_hint = puzzle.half_hint || 0;

    base_puzzle.open_slot = puzzle.open_slot || 0;
    base_puzzle.joker = puzzle.joker || 0;
    base_puzzle.auto_hint = puzzle.auto_hint || 0;

    base_puzzle.load_attempts = puzzle.load_attempts || 0;
    base_puzzle.tiles_placings = puzzle.tiles_placings || 0;
    base_puzzle.loading_elapsed = puzzle.loading_elapsed || 0;

    return base_puzzle;
  },

  getJobData: function (puzzle, user) {
    var job_data =  this.__base.prototype.getJobData(puzzle, user);

    job_data.puzzle_data.bonus_map_puzzles = puzzle.puzzle_data
      .bonus_map_puzzles;

    return job_data;
  },

  updateData: function (master_data, curr_game_data, user_game) {
    master_data = master_data || {};
    user_game.tile_sets = _.union(
      master_data.tile_sets ? master_data.tile_sets.split(',') : [],
      user_game.tile_sets.split(',')).join();

    this.__base.prototype.updateData(master_data, curr_game_data, user_game);
  },

  getRewards: function (user, app_version) {
    var starterpack_bug = config.starterpack_bug,
      starterpack_id = starterpack_bug.id,
      starter_pack_data = _.find(config.rewards, function (curr) {
        return curr.id === starterpack_id;
      }),
      rewards_given = user.rewards_given || [];

    // For users who got starterpack, wont be gettig gameover popup because
    // of bug. Hence repushing another starter pack reward with more validity.
    if (_.indexOf(starterpack_bug.versions, app_version) > -1) {
      starter_pack_data = _.clone(starter_pack_data);
      starter_pack_data.id = starterpack_bug.new_id;
      starter_pack_data.validity = starterpack_bug.validity;

      return _.indexOf(rewards_given, starterpack_id) > -1 ?
        starter_pack_data : null;
    } else {
      return this.__base.prototype.getRewards(user, app_version);
    }
  },

  getUserGame: function (user) {
    return this.__base.prototype.getUserGame(user, {
      shuffle: user.inventory_shuffle,
      hint: user.inventory_hint,
      magnet: user.inventory_magnet || 0,
      swoosh: user.inventory_swoosh || 0,
      substitute: user.inventory_substitute || 0,
      swap: user.inventory_swap || 0,
      half_hint: user.inventory_half_hint || 0,
      open_slot: user.inventory_open_slot || 0,
      joker: user.inventory_joker || 0,
      auto_hint: user.inventory_auto_hint || 0,
      tile_sets: user.tile_sets ? user.tile_sets.join() : ''
    });
  },

  puzzleUpdateAndGetMax: function (puzzles, uid, fb_uid, version, update_max) {
    var normal_puzzles = [],
      bonus_map_puzzles = [],
      curr_reduced, curr_event, curr_limit;

    _.each(puzzles, function (curr) {
      curr_event = curr.bonus_map;

      if (curr_event) {
        curr_reduced = curr.reduced_family || 1;
        curr_limit = curr.limit_given || 0;

        curr = this.getPuzzle(curr, uid, version);
        curr.reduced_family = curr_reduced;

        // For previous versions, no id was sending from client
        curr.event_id = _.isString(curr_event) ? curr_event : null;
        curr.limit_given = curr_limit;

        bonus_map_puzzles.push(curr);
      } else {
        normal_puzzles.push(curr);
      }
    }.bind(this));

    return this.__base.prototype.puzzleUpdateAndGetMax.call(this,
      normal_puzzles, uid, fb_uid, version, update_max)
      .then(function (data) {
        /* istanbul ignore next */
        data.puzzle_data = data.puzzle_data || {};
        data.puzzle_data.bonus_map_puzzles = bonus_map_puzzles;

        return data;
      });
  },

  syncData: function (data, ip_address) {
    var final_resp;

    return this.__base.prototype.syncData.call(this, data, ip_address)
      .then(function (resp) {
        var user = resp.user || data.user || {},
          device = data.device;

        final_resp = resp;

        return bonus_map.getCurrentData(device.store, user.max_ms,
          device.appversion);
      })
      .then(function (bonus_map) {
        final_resp.bonus_map = bonus_map;

        return final_resp;
      });
  },

  postSync: function (data) {
    return this.__base.prototype.postSync(data)
      .then(function (resp) {
        var user = resp.user;

        //shuffle is not saving in track free goods. so adding
        if (user.unlocked_powerups && user.max_ms > 5) {
          user.unlocked_powerups.push('shuffle');
        }
        user.tile_sets = user.tile_sets.split(',');
        return resp;
      });
  },

  insertBonusMapPuzzles: function (puzzles) {
    if (!puzzles || puzzles.length <= 0) {
      return bluebird.resolve();
    }

    _.each(puzzles, function (puzzle) {
      puzzle.date_time = util.getDate(puzzle.date_time);
    });

    return this.__base.prototype.getDb().insertBonusMapPuzzles(puzzles);
  },

  insertPuzzleData: function (data) {
    return bluebird.all([
        this.__base.prototype.insertPuzzleData(data),
        this.insertBonusMapPuzzles(data.bonus_map_puzzles)
      ])
      .spread(function () {
        var duplicates = [];

        _.each(arguments, function (curr) {
          /* istanbul ignore else */
          if (_.isArray(curr)) {
            duplicates = _.concat(duplicates, curr);
          }
        });

        return duplicates;
      });
  },

  getCrossPromotion: function (device, user) {
    var curr_config = config.cross_promotion,
      store = device.store,
      app_version =  device.appversion,
      fbuid = user.fb_uid;

    if (fbuid && curr_config && curr_config[store] &&
      compareVersion(
        curr_config[store].min_version_fb_user,
        app_version
      ) >= 0)
    {
      return null;
    } else {
      return this.__base.prototype.getCrossPromotion(device, user);
    }
  },

  onUpdateAvailable: function (update_resp, req) {
    if (update_resp && req &&
      _.includes(config.force_update_versions, req.version)) {
      update_resp.force_update = true;
    }
  }
};
